import	{	TranslationTable	} from './e2e.commons'
import Chainable = Cypress.Chainable;
import { SettingValue } from '@rcc/common'

export const buttonSelector: string = 'button, a, [role="button"]' 	// Extend selector list if necessary

export function findElementWithLabel(labelHash: TranslationTable, selector: string = '*'): Chainable<JQuery>{
	return 	translate(labelHash)
			.then( tapLog(`Looking for element (${selector})  with label %s`) )
			.then( (label: string) => cy.contains(selector, label) )
}

export function findElementWithExactLabel(labelHash: TranslationTable, selector : string = '*'): Chainable<JQuery> {
	return translate(labelHash)
		.then(tapLog(`Looking for element (${selector})  with label %s`))
		.then((label: string) => cy.contains(selector, new RegExp('^' + label + '$', 'g')))
}

export function assertInputValue(valueHash: TranslationTable): Chainable {
	return translate(valueHash).then(
		(value: string) => cy.get('input').should('have.value', value)
	)
}

export function findElementWithAriaLabel(labelHash: TranslationTable,  selector: string = '*'): Chainable<JQuery>{
	return 	translate(labelHash)
			.then( tapLog('Looking for element with aria-label %s') )
			.then( (label: string) => cy.get(`${selector}[aria-label="${label}"]`) )
}

export function findElementWithName(nameHash: TranslationTable,  selector: string = '*') : Chainable<JQuery> {
	return 	translate(nameHash)
			.then( tapLog('Looking for element with name %s') )
			.then( (name: string) => cy.get(`[name="${name}"]`) )
			.then( elements => elements.filter(selector) )
}

export function findElementWithValue(valueHash: TranslationTable,  selector: string = '*'): Chainable<JQuery> {
	return translate(valueHash)
		.then( tapLog('Looking for element with value %s') )
		.then( (value: string) => cy.get(selector).should('have.value', value) )
}

/** Buttons with label */
export function findButtonWithLabel(labelHash: TranslationTable): Chainable<JQuery> {
	return 	findElementWithLabel(labelHash, buttonSelector)
}

export function clickButtonWithLabel(labelHash: TranslationTable): Chainable<JQuery> {
	return 	findButtonWithLabel(labelHash).click()
}

/** Buttons with aria label */
export function clickButtonWithAriaLabel(labelHash: TranslationTable, selector: string = buttonSelector): Chainable<JQuery> {
	return 	findElementWithAriaLabel(labelHash, selector)
			.click()
}

export function checkElementWithAriaLabelIsNotVisible(labelHash: TranslationTable): Chainable<JQuery> {
	return 	translate(labelHash)
		.then( tapLog('Looking for element with aria-label %s, and that it does not exist') )
		.then( (label: string) => cy.get(`[aria-label="${label}"]:visible`).should('not.exist'))
}

/**
 * Opens the HCP app in the browser.
 *
 * @returns A chainable object
 */

/**
 * Taps into a Chainable and logs a message referencing the current content
 * of the chain. `message` can contain a replacement pattern either '%s' or '%n'
 * with n being a digit. The pattern will be replaced by the stringified version
 * of the content. If no pattern is present the content will be concatenated.
 */
export function tapLog<T>(messageTemplate: string) : (x:T) => Chainable<T> {

	return (x:T) => {
		const	stringifiedContent 	: string
									= JSON.stringify(x)

		let 	message 			: string
									= messageTemplate.replace(/%[s0-9]/, stringifiedContent )

		if(message === messageTemplate) message = message + ` "${stringifiedContent}"`

		return cy.log(message).then( () => cy.wrap(x) )
	}

}

/**
 * Gets the language from the environment variable and with it picks the proper
 * translations off the provided translation table.
 */
export function translate(translationTable: TranslationTable): Chainable<string> {

	const language 		: string
						= (Cypress.env('language') as string)

	const translation 	: string
						= translationTable[language]

	return 	cy.log(`Using translation (${language}): ${translation}`)
			.then( () => translation )
}


/**
 * Find an element with the respective label matching the provided css selector.
 *
 * @param {labelHash} TranslationTable - Hash of labels for each language.
 * (e.g {en: 'my label'})
 *
 * @returns A Chainable JQuery object
 */


/**
 * Clicks the button with the arial Label marking it as the main menu button.
 *
 * @returns A Chainable JQuery object
 */
export function clickMainMenuButton(): Chainable<JQuery> {
	return 	clickButtonWithAriaLabel({ en: 'Main menu', de: 'Hauptmenü' }, 'button')
}

/**
 *Check if the page title is as expected.
 */
export function assertTitle(titleHash: TranslationTable) : Chainable<string> {
	return	translate(titleHash)
			.then( tapLog('Checking if title matches %s') )
			.then( title => cy.title().should('eq',title) )
}




















/** deprecated: */


/**
 * Takes a translation string and a fixture, and returns the translation string
 * from the fixture.
 * @deprecated
 * @param fixture - The fixture object that contains the translation strings.
 * @param {string} translationString - The string you want to translate.
 * @returns the value of the key in the fixture object.
 */
export function e2eTranslate(fixture: Record<string, unknown>, translationString: string):  string | undefined{

	console.warn('e2eTranslate is deprecated, please use translate().')
	const [first, ...rest] = translationString.split('.')
	if (rest.length === 0) return fixture[first] as string | undefined
	return e2eTranslate(fixture[first] as Record<string, unknown>, rest.join('.'))

}

/**
 * Takes a string like `"foo.bar.baz"` and a translation table like `{foo: {bar:
 * {baz: "Hello world!"}}}` and returns `"Hello world!"`
 * @param {string} translationString - The string to resolve.
 * @param translationTable - The translation table that you want to resolve the
 * translation string from.
 * @returns A string or null or undefined
 */

export function resolveTranslationString(
	translationString: string,
	translationTable: Record<string, unknown>
): string | null | undefined {
	if(!translationTable) return undefined
	const [first, ...rest] = translationString.split('.')
	if (rest.length === 0) return translationTable[first] as string | undefined
	return resolveTranslationString(rest.join('.'), translationTable[first] as Record<string, unknown>)
}

/**
 * Takes a translation string, looks up the current language, loads the
 * translation table for that language, and returns the translated string
 * @param {string} translationString - The string to translate.
 * @returns A Chainable (Cypress specific type) object that resolves to a string.
 */


// REMOVE ME
export function _translate(translationString: string): Chainable<string> {
	return 	getLanguage()
			.then((currentLanguage: string) => {
				cy.log(`Translating ${translationString} into ${currentLanguage}.`)
				const translationFile: string = `translations/${currentLanguage}.json`
				cy.log(`Looking for translation file: ${translationFile}`)
				return cy.fixture(translationFile).should('not.be.undefined')
			})
			.then((translationTable: Record<string,string>) => {
				cy.log(`Searching translationTable for translationString: '${translationString}'.`)
				return cy.wrap(resolveTranslationString(translationString, translationTable)).should('not.be.undefined').then((result) => {
					if(typeof result === 'string')
						return result
					else
						throw new Error('Resolved translation is not a string')

				})
			})
}

/**
 * Reads the lang attribute of the <html> element.
 * @returns A Chainable (Cypress specific type) that resolves to the value of the lang attribute of
 * the <html> element.
 */
export function getLanguage(): Chainable<string> {
	return 	cy.log('Reading lang attribute of <html> element')
			.then(() => cy.get('html[lang]'))
			.then(element => cy.wrap(element.attr('lang')).should('not.be.undefined'))
			.then((attrValue) => {
				if(typeof attrValue === 'string')
					return attrValue
				else
					throw new Error('Lang attribute is undefined')

			})
}

// TODO selectPullDown(), separate openMonitoring(), separate rccClick() function


/**
 * clicks on the back button.
 */
export function pressBackButton(): Chainable {
	return cy.get('ion-back-button').should('be.visible').click()
}

export function pressConfirmButton(): Chainable<JQuery> {
	return cy.get('.submit-button').should('be.visible').click()
}

export function log(message: string): Chainable<null> {
	return cy.log(message)
}

export function clearLocalStorage(): void {
	cy.window().then(window => {
	window.localStorage.clear()
	})
}

export function scrollToElement(element: string) : Chainable<JQuery> {
	return cy.get(element).scrollIntoView()
}

export function getNumberOfElements(element: string) : Chainable<number> {
	return cy.get(element).its('length')
}

export function resetLanguage(languageCode: string) : Chainable {
	return 	cy
			.window()
			.then(window => {

				let 	resolve : (...args:unknown[]) => void = undefined
				let		reject 	: (...args:unknown[]) => void = undefined
				const 	promise : Promise<unknown>
								= new Promise( (solve, ject) => { resolve = solve; reject = ject })

				const dbRequest: IDBOpenDBRequest = window.indexedDB.open('rccDatabase', 1)

				dbRequest.onerror = reject

				dbRequest.onsuccess = (successEvent: Event) => {
					if (successEvent.target == null)
						return

					const dataBase: IDBDatabase = (successEvent.target as IDBOpenDBRequest).result

					const transaction: IDBTransaction = dataBase.transaction('rccStorage', 'readwrite')

					transaction.onerror = () => {
						throw new Error('Error opening transaction')
					}

					const store: IDBObjectStore = transaction.objectStore('rccStorage')

					const settings: IDBRequest<string> = store.get('rcc-settings') as IDBRequest<string>

					settings.onerror = reject

					settings.onsuccess = (settingsSuccessEvent: Event) => {
						if (settingsSuccessEvent.target == null)
							return

						const settingsData: string = (settingsSuccessEvent.target as IDBRequest<string>).result
						const settingsObject: SettingValue[] = JSON.parse(settingsData) as SettingValue[]

						const newSettingsObject: SettingValue[] = updateActiveLanguage(settingsObject, languageCode)

						const newSettingsData: string = JSON.stringify(newSettingsObject)

						const putRequest: IDBRequest = store.put(newSettingsData, 'rcc-settings')

						putRequest.onerror 		= reject
						putRequest.onsuccess 	= resolve
					}
				}

				return cy.wrap(promise)
			})
}

export function resetTransmissionTypes() : Chainable {

	return 	cy
			.window()
			.then(window => {

				let 	resolve : (...args:unknown[]) => void = undefined
				let		reject 	: (...args:unknown[]) => void = undefined
				const 	promise : Promise<unknown>
								= new Promise( (solve, ject) => { resolve = solve; reject = ject })

				const dbRequest: IDBOpenDBRequest = window.indexedDB.open('rccDatabase', 1)

				dbRequest.onerror = () => {
					throw new Error('Error opening IndexDB database')
				}

				dbRequest.onsuccess = (successEvent: Event) => {

					if (successEvent.target == null)
						return

					const dataBase: IDBDatabase = (successEvent.target as IDBOpenDBRequest).result

					const transaction: IDBTransaction = dataBase.transaction('rccStorage', 'readwrite')

					transaction.onerror = reject

					const store: IDBObjectStore = transaction.objectStore('rccStorage')

					const settings: IDBRequest<string> = store.get('rcc-settings') as IDBRequest<string>

					settings.onerror = () => {
						throw new Error('Error getting settings')
					}

					settings.onsuccess = (settingsSuccessEvent: Event) => {

						if (settingsSuccessEvent.target == null)
							return

						const settingsData: string = (settingsSuccessEvent.target as IDBRequest<string>).result
						const settingsObject: SettingValue[] = JSON.parse(settingsData) as SettingValue[]

						const newSettingsObject: SettingValue[] = updateTransmissionTypes(settingsObject)

						const newSettingsData: string = JSON.stringify(newSettingsObject)

						const putRequest: IDBRequest = store.put(newSettingsData, 'rcc-settings')

						putRequest.onerror		= reject
						putRequest.onsuccess 	= resolve

					}
				}

				return cy.wrap(promise)
			})

}

function updateTransmissionTypes(settingsObject: SettingValue[]): SettingValue[] {

	for (const entry of settingsObject)
		if (entry.id === 'transmission-send') {
			entry.value = 'COMBINED_TRANSMISSION_SERVICE'
			break
		}

	for (const entry of settingsObject)
		if (entry.id === 'transmission-receive') {
			entry.value = 'COMBINED_TRANSMISSION_SERVICE'
			break
		}

	return settingsObject

}

function updateActiveLanguage(settingsObject: SettingValue[], languageCode: string): SettingValue[] {
	for (const entry of settingsObject)
		if (entry.id === 'activeLanguage') {
			entry.value = languageCode
			break
		}

	return settingsObject
}

export function assertSendTransmissionType(type: string): Chainable<boolean> {

	return getSendTransmissionType()
		.then((transmissionType) => cy.wrap(type === transmissionType))

}

function getSendTransmissionType(): Chainable<string> {
	return cy.window()
		.then((window): Cypress.Chainable<string> =>
			cy.wrap(new Cypress.Promise<string>((resolve, reject) => {

			const dbRequest: IDBOpenDBRequest = window.indexedDB.open('rccDatabase', 1)

			dbRequest.onerror = () => {
				reject('Error opening IndexDB database')
			}

			dbRequest.onsuccess = (successEvent: Event) => {

				if (!successEvent.target) {
					reject('Error: successEvent target is null')
					return
				}

				const dataBase: IDBDatabase = (successEvent.target as IDBOpenDBRequest).result

				const transaction: IDBTransaction = dataBase.transaction('rccStorage', 'readwrite')

				transaction.onerror = () => {
					reject('Error opening transaction')
				}

				const store: IDBObjectStore = transaction.objectStore('rccStorage')

				const settings: IDBRequest<string> = store.get('rcc-settings') as IDBRequest<string>

				settings.onerror = () => {
					reject('Error getting settings')
				}

				settings.onsuccess = (settingsSuccessEvent: Event) => {
					if (!settingsSuccessEvent.target) {
						reject('Settings target is null')
						return
					}

					const settingsData: string = (settingsSuccessEvent.target as IDBRequest<string>).result
					const settingsObject: SettingValue[] = JSON.parse(settingsData) as SettingValue[]

					for (const entry of settingsObject)
						if (entry.id === 'transmission-send') {
							resolve(entry.value as string)
							return
						}


					reject('Could not find transmission-send in settings')
				}
			}
		})))
}

export function assertReceiveTransmissionType(type: string): Chainable<boolean> {
	return getReceiveTransmissionType().then((transmissionType) => cy.wrap(type === transmissionType))
}

function getReceiveTransmissionType(): Chainable<string> {

	return cy.window().then((window): Cypress.Chainable<string> => cy.wrap(new Cypress.Promise<string>((resolve, reject) => {

		const dbRequest: IDBOpenDBRequest = window.indexedDB.open('rccDatabase', 1)

		dbRequest.onerror = () => {
			reject('Error opening IndexDB database')
		}

		dbRequest.onsuccess = (successEvent: Event) => {

			if (!successEvent.target) {
				reject('Error: successEvent target is null')
				return
			}

			const dataBase: IDBDatabase = (successEvent.target as IDBOpenDBRequest).result

			const transaction: IDBTransaction = dataBase.transaction('rccStorage', 'readwrite')

			transaction.onerror = () => {
				reject('Error opening transaction')
			}

			const store: IDBObjectStore = transaction.objectStore('rccStorage')

			const settings: IDBRequest<string> = store.get('rcc-settings') as IDBRequest<string>

			settings.onerror = () => {
				reject('Error getting settings')
			}

			settings.onsuccess = (settingsSuccessEvent: Event) => {
				if (!settingsSuccessEvent.target) {
					reject('Settings target is null')
					return
				}

				const settingsData: string = (settingsSuccessEvent.target as IDBRequest<string>).result
				const settingsObject: SettingValue[] = JSON.parse(settingsData) as SettingValue[]

				for (const entry of settingsObject)
					if (entry.id === 'transmission-receive') {
						resolve(entry.value as string)
						return
					}


				reject('Could not find transmission-send in settings')
			}
		}
	})))

}


export function findSendDataTransmissionPullDown() : Chainable<JQuery> {
	// TODO find a better way to select the pull down
	return cy.get('rcc-pull-down-select').eq(1)
}

export function findReceiveDataTransmissionPullDown() : Chainable<JQuery> {
	// TODO find a better way to select the pull down
	return cy.get('rcc-pull-down-select').eq(2)
}

// #region modals

export function waitForModal(): Chainable<JQuery> {
    /* we need to wait until all javascript files are loaded
     * this takes a lot of time
     * but only when running in cypress. No clue why.
	 * we wait for up to three minute
     */
    return cy.get('ion-modal:visible', {timeout: 180000})
}

export function getCurrentlyOpenModal() {
	return waitForModal().last()
}

// #endregion

export function findSubmitButton() : Chainable {
	return cy.get(buttonSelector).get('[type="submit"]')
}
