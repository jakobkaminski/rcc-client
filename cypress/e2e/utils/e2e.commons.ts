export interface TranslationTable{
	[key: string]: string
}

export interface KeyValuePair {
	key: string
	value: unknown
}
