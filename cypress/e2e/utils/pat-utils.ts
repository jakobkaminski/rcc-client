import Chainable = Cypress.Chainable;
import { TranslationTable } from './e2e.commons'
import { assertTitle } from './e2e-utils'


export function enterPATRoot(): Chainable {
	return cy.visit(Cypress.config('baseUrl') as string)
}

export function assertPATTitle(titleHash: TranslationTable): Chainable<string> {
	const titleSuffixHash: TranslationTable = {
		en: ' · Recovery Cat',
		de: ' · Recovery Cat'
	}
	const fullTitleHash: TranslationTable
		= {}
	for (const lang in titleSuffixHash)
		if (Object.prototype.hasOwnProperty.call(titleSuffixHash, lang))
			fullTitleHash[lang] = titleHash[lang] + titleSuffixHash[lang]
	return assertTitle(fullTitleHash)
}

export function findSendDataTransmissionPullDown() : Chainable<JQuery> {
	// TODO find a better way to select the pull down
	return cy.get('rcc-pull-down-select').eq(0)
}

export function findReceiveDataTransmissionPullDown() : Chainable<JQuery> {
	// TODO find a better way to select the pull down
	return cy.get('rcc-pull-down-select').eq(1)
}
