import * as path from 'path'
import * as fs from 'fs'
import Chainable = Cypress.Chainable;

export function getMostRecentFile(dir: string): string {
	const files: string[] = orderRecentFiles(dir)
	return path.join(dir, files[0])
}

function orderRecentFiles(dir: string): string[] {
	return fs.readdirSync(dir).filter((file : string)  =>
		fs.lstatSync(path.join(dir, file)).isFile()
	).map(file => ({
		name: file,
		time: fs.statSync(path.join(dir, file)).mtime.getTime()
	})).sort((a, b) => b.time - a.time).map(file => file.name)
}

export function isFileNewerThanDate(filename: string, dateToCompare: Date): Chainable<boolean> {

	const matches : RegExpMatchArray | null = filename.match(/rcc-backup-\d{4}-\d{2}-\d{2}-(\d+)\.txt$/)
	if (!matches)
		throw new Error(`Filename ${filename} does not have a valid timestamp`)


	const fileTimestamp : number = parseInt(matches[1], 10)
	const fileDate: Date = new Date()
	fileDate.setTime(fileTimestamp)

	cy.log(`Comparing ${fileDate.toISOString()} with ${dateToCompare.toISOString()}`)

	return cy.wrap(fileDate > dateToCompare)
}

