import {
	assertReceiveTransmissionType,
	assertSendTransmissionType,
	clickMainMenuButton, findElementWithAriaLabel, findElementWithExactLabel,
	findElementWithLabel,
	resetLanguage,
	resetTransmissionTypes
} from '../../../../utils/e2e-utils'
import {
	enterPATRoot,
	findReceiveDataTransmissionPullDown,
	findSendDataTransmissionPullDown
} from '../../../../utils/pat-utils'
import { assertHCPTitle, findBackButton, findLanguagePullDown } from '../../../../utils/hcp-utils'

describe('E_002 PAT Settings', () => {

	beforeEach(() => {

		resetLanguage('de')
		Cypress.env('language', 'de')

		resetTransmissionTypes()

		enterPATRoot()

		clickMainMenuButton().wait(5)

	})

	it('should be possible to open the settings menu (E_002.1)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click().wait(500)

		assertHCPTitle({
			en: 'Settings',
			de: 'Einstellungen'
		})

	})

	it('should be possible to open the language menu (E_002.2)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Language',
			de: 'Sprache'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Language settings',
			de: 'Spracheinstellungen'
		})

	})

	it('should be possible to change the language using the dropdown (E_002.3)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Language',
			de: 'Sprache'
		}).click().wait(5)

		findLanguagePullDown()
			.click()
			.wait(5)

		findElementWithLabel({
			en: 'English',
			de: 'Englisch'
		}).click().wait(5)

		Cypress.env('language', 'en')

		findElementWithLabel({
			en: 'Language setting',
			de: 'Spracheinstellungen',
		})

		findBackButton().last()
			.click().wait(5)

		findBackButton().last()
			.click().wait(5)

		findElementWithLabel({
			en: 'Welcome to Recovery Cat Pilot, you are using version',
			de: 'Willkommen bei Recovery Cat Pilot, Du nutzt Version'
		})

	})

	it('should be possible to open the data transmission menu (E_002.4)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Data transmission',
			de: 'Datenübertragung'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Send transmissions',
			de: 'Datenversand'
		}).scrollIntoView().should('be.visible')

		findElementWithLabel({
			en: 'Receive transmissions',
			de: 'Empfangsoptionen'
		}).scrollIntoView().should('be.visible')

	})

	it('should be possible to choose the data transmission options (E_001.5 and E_001.6)', () => {

		assertSendTransmissionType('COMBINED_TRANSMISSION_SERVICE').should('be.true')
		assertReceiveTransmissionType('COMBINED_TRANSMISSION_SERVICE').should('be.true')

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Data transmission',
			de: 'Datenübertragung'
		}).click().wait(5)

		findSendDataTransmissionPullDown().click().wait(5)

		findSendDataTransmissionPullDown().within(() => {

			findElementWithExactLabel({
				en: 'WebRTC',
				de: 'WebRTC'
			}).first().click().wait(5)

		})

		findReceiveDataTransmissionPullDown().click().wait(5)

		findReceiveDataTransmissionPullDown().within(() => {

			findElementWithExactLabel({
				en: 'WebRTC',
				de: 'WebRTC'
			}).click().wait(5)

		})

		assertSendTransmissionType('RTC_TRANSMISSION_SERVICE').should('be.true')
		assertReceiveTransmissionType('RTC_TRANSMISSION_SERVICE').should('be.true')

	})

	it('should be possible to open the info modals (E_001.7)', () => {

		findElementWithLabel({
			en: 'Settings',
			de: 'Einstellungen'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Data transmission',
			de: 'Datenübertragung'
		}).click().wait(5)

		findElementWithAriaLabel({
			en: 'Information',
			de: 'Information'
		}).eq(0).click().wait(5)

		findElementWithLabel({
			en: 'Configuration of data transfer',
			de: 'Konfiguration der Datenübertragung'
		})

		findElementWithLabel({
			en: 'Okay',
			de: 'Okay'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Configuration of data transfer',
			de: 'Konfiguration der Datenübertragung'
		}).should('not.exist')

		findElementWithAriaLabel({
			en: 'Information',
			de: 'Information'
		}).eq(1).click().wait(5)

		findElementWithLabel({
			en: 'Configuration of data transfer',
			de: 'Konfiguration der Datenübertragung'
		})

		findElementWithLabel({
			en: 'Okay',
			de: 'Okay'
		}).click().wait(5)

		findElementWithLabel({
			en: 'Configuration of data transfer',
			de: 'Konfiguration der Datenübertragung'
		}).should('not.exist')

	})

})

