import { assertPATTitle, enterPATRoot } from '../../../../utils/pat-utils'
import { clickMainMenuButton, findElementWithAriaLabel, findElementWithLabel } from '../../../../utils/e2e-utils'
import {assertNewFileWasDownloaded, findBackButton, findQrCode} from "../../../../utils/hcp-utils";

describe('F_004 Backup and Restore Initiative F', () => {

	beforeEach(() => {

		enterPATRoot()

		clickMainMenuButton().wait(500)

		findElementWithLabel({
			en: 'Backup & Restore',
			de: 'Backup & Wiederherstellung'
		}).click().wait(500)

	})

	it('should be possible to open the Backup and Restore menu (F_004.1)', () => {

		assertPATTitle({ en: 'Backup & Restore', de: 'Backup & Wiederherstellung' })

	})

	it('should have all the required buttons (F_004.1)', () => {

		findElementWithLabel({
			en: 'Create Backup',
			de: 'Backup erstellen'
		}).scrollIntoView().should('be.visible')

		findElementWithLabel({
			en: 'Restore Backup',
			de: 'Backup wiederherstellen'
		}).scrollIntoView().should('be.visible')

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().should('be.visible')

		findBackButton()

	})

	it('should be possible to download the backup (F_004.2', () => {

		const date: Date = new Date()

		findElementWithLabel({
			en: 'Create Backup',
			de: 'Backup erstellen'
		}).scrollIntoView().click().wait(500)

		findElementWithLabel({
			en: 'confirm',
			de: 'Bestätigen'
		}).click().wait(2000)

		assertNewFileWasDownloaded(date)

	})

	it('should be possible to transmit the backup (F_004.4)', () => {

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click().wait(500)

		findQrCode().should('be.visible')

	})

	it('should be possible to cancel the backup transfer (F_004.5)', () => {

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click().wait(500)

		findQrCode().should('be.visible')

		findElementWithLabel({
			en: 'Cancel',
			de: 'Abbrechen'
		}).click().wait(500)

		findQrCode().should('not.exist')

	})

	it('should show a retry button if the backup transfer fails (F_004.6)', () => {

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click().wait(500)

		findQrCode().should('be.visible')

		cy.wait(65000)

		findElementWithLabel({
			en: 'Transmission failed.',
			de: 'Übertragung fehlgeschlagen.'
		}).should('be.visible')

	})

})

