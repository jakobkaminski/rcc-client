import { enterHCPRoot } from '../../../../utils/hcp-utils'
import { clickMainMenuButton, findElementWithLabel } from '../../../../utils/e2e-utils'

describe('F_001 Delete Data', () => {

	beforeEach(() => {

		enterHCPRoot()

		clickMainMenuButton().wait(500)

		// TODO load a backup

	})

	it('should show the delete all data button in the menu (F_001.1)', () => {

		findElementWithLabel({
			en: 'Delete all data',
			de: 'Daten löschen'
		}).scrollIntoView().should('be.visible')

	})

	it('should be possible to delete all data', function () {

		findElementWithLabel({
			en: 'Delete all data',
			de: 'Daten löschen'
		}).scrollIntoView().click().wait(500)

		findElementWithLabel({
			en: 'confirm',
			de: 'Bestätigen'
		}).click().wait(500)

		// TODO check that the data is deleted
	})

})



