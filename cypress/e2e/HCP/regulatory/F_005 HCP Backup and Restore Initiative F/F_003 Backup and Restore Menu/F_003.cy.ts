import {
	assertHCPTitle,
	assertNewFileWasDownloaded,
	enterHCPRoot,
	findBackButton,
	findQrCode
} from '../../../../utils/hcp-utils'
import { clickMainMenuButton, findElementWithAriaLabel, findElementWithLabel } from '../../../../utils/e2e-utils'

describe('F_003 Backup and Restore Menu', () => {

	beforeEach(() => {

		enterHCPRoot()

		clickMainMenuButton().wait(500)

		findElementWithLabel({
			en: 'Backup & Restore',
			de: 'Backup & Wiederherstellung'
		}).click().wait(500)

	})


	it('should be possible to open the Backup and Restore menu (F_003.1)', () => {

		assertHCPTitle({ en: 'Backup & Restore', de: 'Backup & Wiederherstellung' })

	})

	it('should have all the required buttons (F_003.1)', () => {

		findElementWithLabel({
			en: 'Create Backup',
			de: 'Backup erstellen'
		}).scrollIntoView().should('be.visible')

		findElementWithLabel({
			en: 'Restore Backup',
			de: 'Backup wiederherstellen'
		}).scrollIntoView().should('be.visible')

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().should('be.visible')

		findBackButton()
	})

	it('should be possible to download the backup (F_003.2', () => {

		const date: Date = new Date()

		findElementWithLabel({
			en: 'Create Backup',
			de: 'Backup erstellen'
		}).scrollIntoView().click().wait(500)

		findElementWithLabel({
			en: 'confirm',
			de: 'Bestätigen'
		}).click().wait(2000)

		assertNewFileWasDownloaded(date)
	})

	it('should be possible to transfer the backup (F_003.4)', () => {

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click().wait(500)

		findQrCode().should('be.visible')

	})

	it('should be possible to cancel the backup transfer (F_003.5)', () => {

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click().wait(500)

		findQrCode().should('be.visible')

		findElementWithLabel({
			en: 'Cancel',
			de: 'Abbrechen'
		}).click().wait(500)

		findQrCode().should('not.exist')

	})

	it('should show a retry button if the qr-code transfer fails (F_003.7)', () => {

		findElementWithLabel({
			en: 'Transmit Backup',
			de: 'Backup übertragen'
		}).scrollIntoView().click().wait(500)

		findQrCode().should('be.visible')

		cy.wait(65000)

		findElementWithLabel({
			en: 'Transmission failed.',
			de: 'Übertragung fehlgeschlagen.'
		}).should('be.visible')

	})

})
