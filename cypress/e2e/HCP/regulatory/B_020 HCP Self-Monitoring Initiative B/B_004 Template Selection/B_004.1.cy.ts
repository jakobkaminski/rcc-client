import {
	clickCancelButton,
	findDeleteButtonOfQuestion,
	clickMonitoringSetupButton, findQuestionnaireTitleEditButton,
	clickTemplatePullDownSelect,
	enterHCPRoot, findDepressionTemplateOption, findQuestionnaireTitle, findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import { findElementWithExactLabel, findElementWithLabel } from '../../../../utils/e2e-utils'

describe('B_004.1 Template Selected', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	describe('Back Button', () => {

		it('should return to the Homescreen selection screen', function () {
			clickCancelButton()

			findElementWithLabel({
				en: 'Welcome to Recovery Cat Pilot, you are using version',
				de: 'Willkommen bei Recovery Cat Pilot, Du nutzt Version'
			})
		})
	})

	describe('Template selection', () => {

		it('should show all templates in the pull down', function () {
			clickTemplatePullDownSelect()

			findDepressionTemplateOption()

			// wait for questions to be added
			cy.wait(700)

			findElementWithLabel({
				en: 'Template - Bipolar Affective Disorder',
				de: 'Vorlage - Bipolare affektive Störung'
			})

			findElementWithLabel({
				en: 'Template - Psychose',
				de: 'Vorlage - Psychose'
			})

			findElementWithLabel({
				en: 'Template - Schizoaffektive Störungen',
				de: 'Vorlage - Schizoaffektive Störungen'
			})

		})

		// TODO add checks for the other tmaplates and all questions?
		it('should be possible to select a template (e.g. Depression) from a RC curated list', function () {
			clickTemplatePullDownSelect()

			findDepressionTemplateOption().click()

			// wait for questions to be added
			cy.wait(700)

			// check to see if a question from the template is displayed
			// TODO improve this check (e.g. check the category)
			findElementWithLabel({
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			})

			findElementWithLabel({
				en: 'Follow-up Preview',
				de: 'Follow-up Vorschau'
			}).should('not.be.disabled')
		})

		it('should not be possible to change the template after it has been selected', function () {

			clickTemplatePullDownSelect()

			findDepressionTemplateOption()
				.click()

			// wait for questions to be added
			cy.wait(700)

			findTemplatePullDownSelect()
				.should('not.exist')

		})

		it('should be possible to edit the questionnaire name after a template has been set', () => {
			clickTemplatePullDownSelect()

			findDepressionTemplateOption()
				.click()

			// wait for questions to be added
			cy.wait(700)

			findQuestionnaireTitleEditButton()
				.click()

			findQuestionnaireTitle()
				.clear()
				.type('Test Questionnaire')

			findQuestionnaireTitle()
				.should('have.value', 'Test Questionnaire')

		})

	})

	describe('Question interaction', () => {

		it('should be possible to delete a question', () => {
			clickTemplatePullDownSelect()

			findDepressionTemplateOption()
				.click()

			// wait for questions to be added
			cy.wait(700)

			// check to see if a question from the template is displayed
			findElementWithLabel({
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			})

			findDeleteButtonOfQuestion({
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			}).as('deleteButton')
			cy.get('@deleteButton').scrollIntoView()
			cy.get('@deleteButton').click()


			findElementWithExactLabel({
				en: 'yes',
				de: 'ja'
			}).click()

			// check to see if the question is no longer displayed
			findElementWithLabel({
				en: 'How well were you able to manage your usual daily tasks today?',
				de: 'Konnten Sie Ihren Alltag heute so gut wie sonst bewältigen?'
			}).should('not.exist')

		})

	})

})
