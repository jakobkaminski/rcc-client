import {
	assertHCPTitle,
	clickCancelButton,
	clickMonitoringSetupButton,
	clickTemplatePullDownSelect,
	enterHCPRoot,
	findDepressionTemplateOption, findEarlyWarningSignsQuestionCategoryButton,
	findMedicationQuestionCategoryButton,
	findResourceQuestionCategoryButton, findSideEffectsQuestionCategoryButton,
	findSymptomQuestionCategoryButton, findWithdrawalSymptomsQuestionCategoryButton
} from '../../../../utils/hcp-utils'
import { findElementWithLabel } from '../../../../utils/e2e-utils'


describe('B_002 Initializing New Self-Monitoring Question Set', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	describe('Cancel button', () => {

		it('should return to homescreen', () => {

			clickCancelButton().then(() => {

				findElementWithLabel({
					en: 'Welcome to Recovery Cat Pilot, you are using version',
					de: 'Willkommen bei Recovery Cat Pilot, Du nutzt Version'
				})

			})
		})

	})

	describe('Category display', () => {

		it('should display the symptoms category', function () {
			findSymptomQuestionCategoryButton()
		})

		it('should display the medication category', function () {
			findMedicationQuestionCategoryButton()
		})

		it('should display the resources category', function () {
			findResourceQuestionCategoryButton()
		})

		it('should display the warning signs category', function () {
			findEarlyWarningSignsQuestionCategoryButton()
		})

		it('should display the side effects category', function () {
			findSideEffectsQuestionCategoryButton()
		})

		it('should display the withdrawal category', function () {
			findWithdrawalSymptomsQuestionCategoryButton()
		})

	})

	describe('Form validation', () => {

		it('should not be possible to continue without setting at least one question', function () {
			findElementWithLabel({
				en: 'Follow-up Preview',
				de: 'Follow-up Vorschau'
			}).should('be.disabled')
		})

	})

	describe('Question Templates', () => {

		it('should show the template selection pull down', function () {
			clickTemplatePullDownSelect()

			// find an example template in the list
			findDepressionTemplateOption().should('be.visible')
		})

	})

})
