import {
	clickMainMenuButton, findElementWithLabel,
} from '../../../../utils/e2e-utils'

import {
	enterHCPRoot,
	clickScanQRCodeButton,
	clickMonitoringSetupButton,
	assertNavigationMenuIsOpen,
	assertHCPLogoIsVisible,
	assertDateDisplayIsVisible, findQrCodeScanner,
} from '../../../../utils/hcp-utils'


describe('B_001 HCP Homescreen', () => {

	beforeEach(() => {
		enterHCPRoot()
	})

	describe('Homescreen', () => {

		it('should contain the recovery cat logo', function () {
			assertHCPLogoIsVisible()
		})

		it('should contain todays date display', () => {
			assertDateDisplayIsVisible()
		})
	})

	describe('Self monitoring button', () => {

		it('should open self monitoring setup', () => {

			clickMonitoringSetupButton()
				.wait(50)

			findElementWithLabel(
				{
					en: 'Choose template and/or add questions',
					de: 'Vorlage auswählen und/oder Fragen hinzufügen'
				}
			).should('be.visible')

		})

	})

	describe('Scan QR code button', () => {

		it('should open qr code scan', () => {

			clickScanQRCodeButton().wait(500)
			findQrCodeScanner()

		})

	})

	describe('Request Data button', () => {

		it('should open the reequest data screen', () => {

			findElementWithLabel({
				en: 'Receive data',
				de: 'Daten empfangen'
			})

		})
	})

	describe('Main menu button', () => {

		it('should open main menu', () => {
			clickMainMenuButton()
				.then(() => {
					assertNavigationMenuIsOpen()
				})
		})

	})
})
