import {
	assertAllDaysAreChecked,
	clickMonitoringSetupButton,
	clickSaturdayScheduleCheckBox,
	clickSundayScheduleCheckBox,
	enterHCPRoot,
	findCustomQuestionButton,
	findDepressionTemplateOption,
	findDefaultScheduleButton,
	findScheduleButtonWithLabel,
	findSymptomQuestionCategoryButton,
	findTemplatePullDownSelect,
	findQuestionTypePulldown,
} from '../../../../utils/hcp-utils'
import {
	findButtonWithLabel,
	findElementWithLabel,
	findSubmitButton,
	getCurrentlyOpenModal,
} from '../../../../utils/e2e-utils'

describe('B_006 Scheduling Questions', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	describe('template selected (B_004.1)', () => {

		beforeEach(() => {
			findTemplatePullDownSelect()
				.click()

			findDepressionTemplateOption()
				.click()
		})

		it('B_006.2 should be possible top set a base schedule for the whole questionaire', () => {

			findDefaultScheduleButton()
				.click()

			getCurrentlyOpenModal().within(() => {
				assertAllDaysAreChecked()

				clickSaturdayScheduleCheckBox()
				clickSundayScheduleCheckBox()

				findSubmitButton().click()
			})

			findScheduleButtonWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			})
		})

		it('B_006.6 should be possible to set a custom schedule for a custom question', () => {

			findSymptomQuestionCategoryButton()
				.click()

			findCustomQuestionButton()
				.click()

			// #region custom queston modal
			getCurrentlyOpenModal().within(() => {

				findQuestionTypePulldown()
					.click()

				findElementWithLabel({
					en: 'Yes/No question (0/1)',
					de: 'Ja/Nein Frage (0/1)'
				}).click()

				cy.get('rcc-edit-text[formcontrolname="questionTitle"]').within(() => {
					cy.get('button.editButton').focus().click()

					cy.get('input')
						.click()
						.focus()
						.type('This is a test question')

					cy.get('button.doneButton').click()
				}).scrollIntoView()

				findButtonWithLabel({
					de:	'Individuellen Zeitplan setzen',
					en:	'Set individual schedule'
				}).click()
			})
			// #endregion

			// #region schedule modal
			getCurrentlyOpenModal().within(() => {
				findElementWithLabel({
					en: 'Individual Schedule',
					de: 'Individueller Zeitplan Auswahl'
				})

				// all days are selected by default
				assertAllDaysAreChecked()

				clickSaturdayScheduleCheckBox()
				clickSundayScheduleCheckBox()

				findSubmitButton().click()
			})
			// #endregion

			// #region custom question modal
			getCurrentlyOpenModal().within(() => {
				findElementWithLabel({
					en: 'Mo, Tu, We, Th, Fr',
					de: 'Mo, Di, Mi, Do, Fr'
				}).should('exist')

				findSubmitButton().click()
			})
			// #endregion

			findElementWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			}).should('exist')
		})

	})

	describe('No template selected (B_004.2)', () => {

		it('B_006.2 should be possible top set a base schedule for the whole questionaire', () => {
			findDefaultScheduleButton()
				.click()

			// #region schedule modal
			getCurrentlyOpenModal().within(() => {
				assertAllDaysAreChecked()

				clickSaturdayScheduleCheckBox()
				clickSundayScheduleCheckBox()

				findSubmitButton().click()
			})
			// #endregion

			findScheduleButtonWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			})
		})

		it('B_006.6 should be possible to set a custom schedule for a custom question',  () => {
			findSymptomQuestionCategoryButton()
				.click()

			findCustomQuestionButton()
				.click()

			// #region custom question modal
			getCurrentlyOpenModal().within(() => {
				findQuestionTypePulldown()
					.click()

				findElementWithLabel({
					en: 'Yes/No question (0/1)',
					de: 'Ja/Nein Frage (0/1)'
				}).click()

				cy.get('rcc-edit-text[formcontrolname="questionTitle"]').within(() => {
					cy.get('button.editButton').focus().click()

					cy.get('input')
						.click()
						.focus()
						.type('This is a test question')

					cy.get('button.doneButton').click()
				}).scrollIntoView()

				findElementWithLabel(
					{
						de:	'Individuellen Zeitplan setzen',
						en:	'Set individual schedule'
					},
					'button'
				).click()
			})
			// #endregion

			// #region schedule modal
			getCurrentlyOpenModal().within(() => {
				findElementWithLabel({
					en: 'Individual Schedule',
					de: 'Individueller Zeitplan Auswahl'
				})

				// all days are selected by default

				assertAllDaysAreChecked()

				clickSaturdayScheduleCheckBox()
				clickSundayScheduleCheckBox()

				findSubmitButton().click()
			})
			// #endregion

			// #region custom question modal
			getCurrentlyOpenModal().within(() => {
				findElementWithLabel({
					en: 'Mo, Tu, We, Th, Fr',
					de: 'Mo, Di, Mi, Do, Fr'
				}).should('exist')

				findSubmitButton().click()
			})
			// #endregion

			findElementWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			}).should('exist')
		})
	})
})
