import {
	clickMonitoringSetupButton,
	clickTemplatePullDownSelect,
	enterHCPRoot, findAndIncrementReminder,
	findDefaultReminderButton,
	findDepressionTemplateOption,
	findHoursInTimePicker,
	findMinutesInTimePicker,
	findToggleButton,
} from '../../../../utils/hcp-utils'
import { findElementWithLabel } from '../../../../utils/e2e-utils'

describe('B_007 Reminder Set-up', () => {

	beforeEach(() => {

		enterHCPRoot()
		clickMonitoringSetupButton()

	})

	describe('B_007.1 Reminder Set-up with template selected', () => {

		beforeEach(() => {

			clickTemplatePullDownSelect().wait(500)

			findDepressionTemplateOption()
				.scrollIntoView()
				.click()
				.wait(700)

		})

		it('B_007.1 should set the reminder to a default 12:00', function () {

			findDefaultReminderButton()
				.scrollIntoView()
				.should('be.visible')

		})

		it('B_007.1 should be possible to change the reminder time', function () {

			findAndIncrementReminder()

			findElementWithLabel({
				en: 'Reminder, 01:01 PM',
				de: 'Erinnerung, 13:01 Uhr'
			})
				.scrollIntoView()
				.should('be.visible')
		})

		it('B_007.1 should be possible to turn off the reminder', function () {

			findDefaultReminderButton()
				.scrollIntoView()
				.should('be.visible')
				.click()

			findToggleButton()
				.click()

			findHoursInTimePicker()
				.should('have.attr', 'aria-disabled', 'true')

			findMinutesInTimePicker()
				.should('have.attr', 'aria-disabled', 'true')

			cy.get('rcc-submit-button')
				.click()

			findElementWithLabel({
				en: 'No reminder',
				de: 'keine Erinnerung'
			})
		})

	})

	describe('B_007.2 Reminder Set-up with no template selected', () => {

		it('B_007.2 should set the reminder to a default 12:00', function () {

			findDefaultReminderButton()
				.scrollIntoView()
				.should('be.visible')

		})

		it('B_007.2 should be possible to change the reminder time', function () {

			findAndIncrementReminder()

			findElementWithLabel({
				en: 'Reminder, 01:01 PM',
				de: 'Erinnerung, 13:01 Uhr'
			})
				.scrollIntoView()
				.should('be.visible')
		})

		it('B_007.2 should  be possible to turn off the reminder', function () {

			findDefaultReminderButton()
				.click()

			findToggleButton()
				.click()

			findHoursInTimePicker()
				.should('have.attr', 'aria-disabled', 'true')

			findMinutesInTimePicker()
				.should('have.attr', 'aria-disabled', 'true')

			cy.get('rcc-submit-button')
				.click()

			findElementWithLabel({
				en: 'No reminder',
				de: 'keine Erinnerung'
			})
		})

	})

})
