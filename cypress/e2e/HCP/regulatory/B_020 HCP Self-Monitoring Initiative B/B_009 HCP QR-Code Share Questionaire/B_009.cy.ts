import {
	clickMonitoringSetupButton,
	enterHCPRoot,
	findDepressionTemplateOption, findQrCode,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import { findElementWithLabel } from '../../../../utils/e2e-utils'

describe('B_009 HCP QR-Code Share Questionaire', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	it('should be possible to open the qr-code page to transmit the questionnaire', function () {

		findTemplatePullDownSelect()
			.click().wait(500)

		findDepressionTemplateOption()
			.click().wait(500)

		findElementWithLabel({
			en: 'Follow-up Preview',
			de: 'Follow-up Vorschau',
		}).click()

		findElementWithLabel({
			en: 'Share Follow-up',
			de: 'Follow-up teilen',
		}).click()

		findQrCode().should('exist')

	})

})
