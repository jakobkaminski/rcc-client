import {
	clickMonitoringSetupButton,
	enterHCPRoot,
	findDepressionTemplateOption,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import { findElementWithLabel } from '../../../../utils/e2e-utils'

xdescribe('B_016 Notes Functionality', () => {

	beforeEach(() => {

		enterHCPRoot()
		clickMonitoringSetupButton()

		findTemplatePullDownSelect()
			.click()

		findDepressionTemplateOption()
			.click().wait(1500)

	})

	it('should show the daily note', () => {

		findElementWithLabel({
			en: 'Follow-up Preview',
			de: 'Follow-up Vorschau',
		}).scrollIntoView().click()

		findElementWithLabel({
			en: 'What else happened today?',
			de: 'Was ist heute sonst noch passiert?'
		}).scrollIntoView().should('be.visible')

	})

})
