import {
	clickMonitoringSetupButton,
	enterHCPRoot,
	findDepressionTemplateOption, findDosageInput, findMedicationDropDownSelect,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import {
	findElementWithAriaLabel,
	findElementWithLabel,
	findElementWithName,
} from '../../../../utils/e2e-utils'

describe('B_015 Add and Edit Medication', () => {

	beforeEach(() => {

		enterHCPRoot()

		clickMonitoringSetupButton()

	})

	describe('with Template selected (B_015.1', () => {

		beforeEach(() => {
			findTemplatePullDownSelect()
				.click()

			findDepressionTemplateOption()
				.scrollIntoView()
				.click()

			findElementWithLabel({
				en: 'Query Medication',
				de: 'Medikation abfragen'
			}).click().wait(500)

		})

		it('should display the medication modal with all elements', function () {

			findElementWithAriaLabel({
				en: 'title',
				de: 'Titel'
			}).should('be.visible')

			findMedicationDropDownSelect()
				.should('exist')

			findDosageInput().should('be.visible')

			// TODO check for unit

		})

		it('should be possible to select a medication from the list (B_015.1)', () => {

			findMedicationDropDownSelect()
				.click()
				.wait(500)

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.click()

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.should('exist')

		})

		it('should be possible to set a dosage (B_015.2)', function () {

			findDosageInput().within(() => {
				cy.get('button[class*="editButton"]').click()
			})

			findDosageInput().type('20mg')

			findDosageInput().within(() => {
				cy.get('button[class*="doneButton"]').click()
			})

			findDosageInput().within(() => {
				cy.get('input').should('have.value', '20mg')
			})

		})

		it('should be possible to change the question wording (B_015.3)', function () {

			// TODO differentiate between template title and question title (issue created)
			findElementWithName({
				en: 'Edit Title',
				de: 'Titel bearbeiten'
			}).last().click()

			findElementWithAriaLabel({
				en: 'title',
				de: 'Titel'
			}).last().clear().type('TestQuestion')

			// TODO same identifier as above (issue created)
			findElementWithName(
				{
					en: 'Finish editing title',
					de: 'Titel bestätigen'
				}
			).last().click()

			findElementWithAriaLabel({
				en: 'title',
				de: 'Titel'
			}).last().should('have.value', 'TestQuestion ')

		})

		it('should be possible to add the question to the monitoring', function () {

			// select medication
			findMedicationDropDownSelect()
				.click()
				.wait(500)

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.click()

			// set dosage
			findDosageInput().within(() => {
				cy.get('button[class*="editButton"]').click()
			})

			findDosageInput().type('20mg')

			findDosageInput().within(() => {
				cy.get('button[class*="doneButton"]').click()
			})

			// set question title
			findElementWithName({
				en: 'Edit Title',
				de: 'Titel bearbeiten'
			}).last().click()

			findElementWithAriaLabel({
				en: 'title',
				de: 'Titel'
			}).last().clear().type('TestQuestion')

			// TODO same identifier as above (issue created)
			findElementWithName(
				{
					en: 'Finish editing title',
					de: 'Titel bestätigen'
				}
			).last().click()

			// submit
			cy.get('rcc-submit-button').last().click()

			// TODO Fails for now, bug in the app
			findElementWithLabel({
				en: 'TestQuestion (Citalopram, 20mg)',
				de: 'TestQuestion (Citalopram, 20mg)'
			})

		})

	})

	describe('without Template selected (B_015.4', () => {

		beforeEach(() => {

			findElementWithLabel({
				en: 'Query Medication',
				de: 'Medikation abfragen'
			}).click()

		})

		it('should display the medication modal with all elements', function () {

			findElementWithAriaLabel({
				en: 'title',
				de: 'Titel'
			}).should('be.visible')

			findMedicationDropDownSelect()
				.should('exist')

			findDosageInput().should('be.visible')

			// TODO check for unit

		})

		it('should be possible to select a medication from the list (B_015.1)', () => {

			findMedicationDropDownSelect()
				.click()
				.wait(500)

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.click()

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.should('exist')

		})

		it('should be possible to set a dosage (B_015.2)', function () {

			findDosageInput().within(() => {
				cy.get('button[class*="editButton"]').click()
			})

			findDosageInput().type('20mg')

			findDosageInput().within(() => {
				cy.get('button[class*="doneButton"]').click()
			})

			findDosageInput().within(() => {
				cy.get('input').should('have.value', '20mg')
			})

		})

		it('should be possible to change the question wording (B_015.3)', function () {

			// TODO differentiate between template title and question title (issue created)
			findElementWithName({
				en: 'Edit Title',
				de: 'Titel bearbeiten'
			}).last().click()

			findElementWithAriaLabel({
				en: 'title',
				de: 'Titel'
			}).last().clear().type('TestQuestion')

			// TODO same identifier as above (issue created)
			findElementWithName(
				{
					en: 'Finish editing title',
					de: 'Titel bestätigen'
				}
			).last().click()

			findElementWithAriaLabel({
				en: 'title',
				de: 'Titel'
			}).last().should('have.value', 'TestQuestion ')

		})

		it('should be possible to add the question to the monitoring', function () {

			// select medication
			findMedicationDropDownSelect()
				.click()
				.wait(500)

			findElementWithLabel({
				en: 'Citalopram',
				de: 'Citalopram'
			})
				.scrollIntoView()
				.click()

			// set dosage
			findDosageInput().within(() => {
				cy.get('button[class*="editButton"]').click()
			})

			findDosageInput().type('20mg')

			findDosageInput().within(() => {
				cy.get('button[class*="doneButton"]').click()
			})

			// set question title
			findElementWithName({
				en: 'Edit Title',
				de: 'Titel bearbeiten'
			}).last().click()

			findElementWithAriaLabel({
				en: 'title',
				de: 'Titel'
			}).last().clear().type('TestQuestion')

			// TODO same identifier as above (issue created)
			findElementWithName(
				{
					en: 'Finish editing title',
					de: 'Titel bestätigen'
				}
			).last().click()

			// submit
			cy.get('rcc-submit-button').last().click()

			// TODO Fails for now, bug in the app
			findElementWithLabel({
				en: 'TestQuestion (Citalopram, 20mg)',
				de: 'TestQuestion (Citalopram, 20mg)'
			})

		})

	})

})
