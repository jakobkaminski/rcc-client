import {
	findAnswerOptionValue,
	clickMonitoringSetupButton,
	clickSaturdayScheduleCheckBox,
	clickSundayScheduleCheckBox,
	enterHCPRoot,
	findAnswerOptionInputField,
	findCustomQuestionButton,
	findLabelInsideMonitoringModal,
	findSymptomQuestionCategoryButton,
	findToggleButton,
	findScrollBackButton,
	findCloseButton,
	findDepressionTemplateOption,
	findTemplatePullDownSelect
} from '../../../../utils/hcp-utils'
import {
	findElementWithAriaLabel, findElementWithExactLabel,
	findElementWithLabel, findElementWithName,
} from '../../../../utils/e2e-utils'

describe('B_008 Add and Edit Custom Questions', () => {

	beforeEach(() => {
		enterHCPRoot()
		clickMonitoringSetupButton()
	})

	describe('with template selected (B_004.1)', function () {

		beforeEach(() => {
			findTemplatePullDownSelect()
				.scrollIntoView().click()

			findDepressionTemplateOption()
				.scrollIntoView()
				.scrollIntoView().click()
		})

		it('B_008 should be possible to open the custom question modal', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findElementWithExactLabel({
				en: 'Symptom question catalogue',
				de: 'Symptom Fragenkatalog'
			}).should('be.visible')

			findCustomQuestionButton()
				.scrollIntoView().click()

			findElementWithExactLabel({
				en: 'Symptoms',
				de: 'Symptome'
			}).should('exist')

			findElementWithExactLabel({
				en: 'Edit Question',
				de: 'Frage bearbeiten'
			}).should('be.visible')


		})

		it('B_008.1 should be possible to select the type of question', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// Quantity question (0-3)
			findElementWithLabel({
				en: 'Quantity question (0-3)',
				de: 'Mengen Frage (0-3)'
			}).scrollIntoView().click().wait(500)

			// TODO better way to identify the different answer fields
			findAnswerOptionValue({
				en: 'no',
				de: 'nein'
			}, 0).should('exist')

			findAnswerOptionValue({
				en: 'somewhat',
				de: 'etwas'
			}, 1).should('exist')

			findAnswerOptionValue({
				en: 'moderately',
				de: 'mäßig'
			}, 2).should('exist')

			findAnswerOptionValue({
				en: 'strongly',
				de: 'stark'
			}, 3).should('exist')

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// Yes/No question (0/1)
			findElementWithLabel({
				en: 'Yes/No question (0/1)',
				de: 'Ja/Nein Frage (0/1)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValue({
				en: 'yes',
				de: 'ja'
			},  0).should('exist')

			findAnswerOptionValue({
				en: 'no',
				de: 'nein'
			}, 1).should('exist')

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// 4-Choice question (A-D)
			findElementWithLabel({
				en: '4-Choice question (A-D)',
				de: '4-Choice-Frage (A-D)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValue({
				en: 'A',
				de: 'A'
			}, 0).should('exist')

			findAnswerOptionValue({
				en: 'B',
				de: 'B'
			}, 1).should('exist')

			findAnswerOptionValue({
				en: 'C',
				de: 'C'
			}, 2).should('exist')

			findAnswerOptionValue({
				en: 'D',
				de: 'D'
			}, 3).should('exist')

		})

		it('B_008.2 should be possible to set the answer option for a question', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			findElementWithLabel({
				en: 'Yes/No question (0/1)',
				de: 'Ja/Nein Frage (0/1)'
			}).scrollIntoView().click().wait(500)

			// TODO better way to identify the different answer fields (no aria-label)
			findAnswerOptionInputField(0).within(() => {
				cy.get('button').eq(0).scrollIntoView().click()
				cy.get('input').first().clear().type('Custom answer option 1')
				cy.get('button').eq(1).scrollIntoView().click()
			})

			// TODO selects the second answer
			findAnswerOptionInputField(1).within(() => {
				cy.get('button').eq(2).scrollIntoView().click()
				cy.get('input').eq(1).clear().type('Custom answer option 2')
				cy.get('button').eq(3).scrollIntoView().click()
			})

		})

		it('B_008.3 should be possible to edit the question wording', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// TODO same identifier as template name controls
			findElementWithName(
				{
					en: 'Edit title',
					de: 'Titel bearbeiten'
				}
			).last().scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Title',
					de: 'Titel'
				}
			).last().type('This is a test Question')

			// TODO same identifier as template name controls
			findElementWithName(
				{
					en : 'Finish editing title',
					de: 'Titel bestätigen'
				}
			).last().scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Title',
					de: 'Titel'
				}
			).last().should('have.value', 'This is a test Question')
		})

		it('B_008 should be possible to add the question to the list of questions', function () {
			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			findElementWithLabel({
				en: 'Yes/No question (0/1)',
				de: 'Ja/Nein Frage (0/1)'
			}).scrollIntoView().click().wait(500)

			findElementWithName(
				{
					en: 'Edit title',
					de: 'Titel bearbeiten'
				}
			).last().scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Title',
					de: 'Titel'
				}
			).last().type('This is a test Question')

			findElementWithName(
				{
					en : 'Finish editing title',
					de: 'Titel bestätigen'
				}
			).last().scrollIntoView().click()

			// TODO better way to identify the different answer fields (no aria-label)
			findAnswerOptionInputField(0).within(() => {
				cy.get('button').eq(0).scrollIntoView().click()
				cy.get('input').first().clear().type('Custom answer option 1')
				cy.get('button').eq(1).scrollIntoView().click()
			})

			// TODO selects the second answer
			findAnswerOptionInputField(1).within(() => {
				cy.get('button').eq(2).scrollIntoView().click()
				cy.get('input').eq(1).clear().type('Custom answer option 2')
				cy.get('button').eq(3).scrollIntoView().click()
			})

			findToggleButton()
				.scrollIntoView().click()
				.wait(1500)

			findElementWithLabel({
				en: 'Individual Schedule',
				de: 'Individueller Zeitplan Auswahl'
			}).wait(500)

			clickSaturdayScheduleCheckBox()
			clickSundayScheduleCheckBox()

			cy.get('rcc-submit-button').eq(2).should('exist').scrollIntoView().click()
			// TODO find a better way of identifying the submit button

			cy.get('rcc-submit-button').eq(1).should('exist').scrollIntoView().click().wait(1000)

			findScrollBackButton().eq(0).click({ force: true }).wait(1500)

			findElementWithLabel({
				en: 'This is a test Question',
				de: 'This is a test Question'
			}).should('exist')

			findElementWithLabel({
				en: 'Custom answer option 1',
				de: 'Custom answer option 1'
			}).should('exist')

			findElementWithLabel({
				en: 'Custom answer option 2',
				de: 'Custom answer option 2'
			}).should('exist')

			findElementWithLabel({
				en: 'Mo, Tu, We, Th, Fr',
				de: 'Mo, Di, Mi, Do, Fr'
			})
		})

		it('B_008 should be possible to return to the list of questions', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			findCloseButton()
				.scrollIntoView().click().wait(500)

			findElementWithLabel({
				en: 'Custom questions',
				de: 'Frage bearbeiten',
			}).should('not.exist')

			findCloseButton()
				.scrollIntoView().click()
		})

	})

	describe('No template selected (B_004.2)', () => {

		it('B_008 should be possible to open the custom question modal without selecting a template', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findLabelInsideMonitoringModal({
				en: 'Symptom question catalogue',
				de: 'Symptom Fragenkatalog'
			})

			findCustomQuestionButton()
				.scrollIntoView().click()

			// TODO because modals can't be differentiated it finds two
			findLabelInsideMonitoringModal({
				en: 'Symptoms',
				de: 'Symptome'
			})

			findLabelInsideMonitoringModal({
				en: 'Edit Question',
				de: 'Frage bearbeiten'
			})


		})

		it('B_008.4 should be possible to select the type of question', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// Quantity question (0-3)
			findElementWithLabel({
				en: 'Quantity question (0-3)',
				de: 'Mengen Frage (0-3)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValue({
				en: 'no',
				de: 'nein'
			}, 0).should('exist')

			findAnswerOptionValue({
				en: 'somewhat',
				de: 'etwas'
			}, 1).should('exist')

			findAnswerOptionValue({
				en: 'moderately',
				de: 'mäßig'
			}, 2).should('exist')

			findAnswerOptionValue({
				en: 'strongly',
				de: 'stark'
			}, 3).should('exist')

			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// Yes/No question (0/1)
			findElementWithLabel({
				en: 'Yes/No question (0/1)',
				de: 'Ja/Nein Frage (0/1)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValue({
				en: 'yes',
				de: 'ja'
			},  0).should('exist')

			findAnswerOptionValue({
				en: 'no',
				de: 'nein'
			}, 1).should('exist')

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			// 4-Choice question (A-D)
			findElementWithLabel({
				en: '4-Choice question (A-D)',
				de: '4-Choice-Frage (A-D)'
			}).scrollIntoView().click().wait(500)

			findAnswerOptionValue({
				en: 'A',
				de: 'A'
			}, 0).should('exist')

			findAnswerOptionValue({
				en: 'B',
				de: 'B'
			}, 1).should('exist')

			findAnswerOptionValue({
				en: 'C',
				de: 'C'
			}, 2).should('exist')

			findAnswerOptionValue({
				en: 'D',
				de: 'D'
			}, 3).should('exist')

		})

		it('B_008.5 should be possible to set the answer option for a question', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			findElementWithLabel({
				en: 'Yes/No question (0/1)',
				de: 'Ja/Nein Frage (0/1)'
			}).scrollIntoView().click().wait(500)

			// TODO better way to identify the different answer fields (no aria-label)
			findAnswerOptionInputField(0).within(() => {
				cy.get('button').eq(0).scrollIntoView().click()
				cy.get('input').first().clear().type('Custom answer option 1')
				cy.get('button').eq(1).scrollIntoView().click()
			})

			// TODO selects the second answer
			findAnswerOptionInputField(1).within(() => {
				cy.get('button').eq(2).scrollIntoView().click()
				cy.get('input').eq(1).clear().type('Custom answer option 2')
				cy.get('button').eq(3).scrollIntoView().click()
			})

		})

		it('B_008.6 should be possible to edit the question wording', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// TODO same identifier as template name controls
			findElementWithName(
				{
					en: 'Edit title',
					de: 'Titel bearbeiten'
				}
			).scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Title',
					de: 'Titel'
				}
			).type('This is a test Question')


			findElementWithAriaLabel(
				{
					en: 'Title',
					de: 'Titel'
				}
			).should('have.value', 'This is a test Question')
		})

		it('B_008 should be possible to add the question to the list of questions without selecting a template', function () {
			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			// findQuestionTypePulldown()
			// .scrollIntoView().click().wait(500)

			// TODO needs to be changed back once the aria label above has been added again
			cy.get('rcc-pull-down-select')
				.last()
				.scrollIntoView().click()

			findElementWithLabel({
				en: 'Yes/No question (0/1)',
				de: 'Ja/Nein Frage (0/1)'
			}).scrollIntoView().click().wait(500)

			findElementWithName(
				{
					en: 'Edit title',
					de: 'Titel bearbeiten'
				}
			).scrollIntoView().click()

			findElementWithAriaLabel(
				{
					en: 'Title',
					de: 'Titel'
				}
			).type('This is a test Question')

			// TODO better way to identify the different answer fields (no aria-label)
			findAnswerOptionInputField(0).within(() => {
				cy.get('button').eq(0).scrollIntoView().click()
				cy.get('input').first().clear().type('Custom answer option 1')
			})

			// TODO selects the second answer
			findAnswerOptionInputField(1).within(() => {
				cy.get('button').eq(2).scrollIntoView().click()
				cy.get('input').eq(1).clear().type('Custom answer option 2')
			})

			findToggleButton()
				.scrollIntoView().click()
				.wait(1500)

			findElementWithLabel({
				en: 'Individual Schedule',
				de: 'Individueller Zeitplan Auswahl'
			}).wait(500)

			clickSaturdayScheduleCheckBox()
			clickSundayScheduleCheckBox()

			cy.get('rcc-submit-button').eq(2).should('exist').scrollIntoView().click()
			// TODO find a better way of identifying the submit button

			cy.get('rcc-submit-button').eq(1).should('exist').scrollIntoView().click().wait(2000)

			findScrollBackButton().eq(0).click({ force: true }).wait(1500)

			findElementWithLabel({
				en: 'This is a test Question',
				de: 'This is a test Question'
			}).scrollIntoView().should('be.visible')

			findElementWithLabel({
				en: 'Custom answer option 1',
				de: 'Custom answer option 1'
			}).should('be.visible')

			findElementWithLabel({
				en: 'Custom answer option 2',
				de: 'Custom answer option 2'
			}).should('be.visible')

		})

		it('B_008 should be possible to return to the list of questions without selecting a template', function () {

			findSymptomQuestionCategoryButton()
				.scrollIntoView().click().wait(500)

			findCustomQuestionButton()
				.scrollIntoView().click().wait(500)

			findCloseButton()
				.scrollIntoView().click().wait(500)

			findElementWithLabel({
				en: 'Custom questions',
				de: 'Frage bearbeiten',
			}).should('not.exist')

			findCloseButton()
				.scrollIntoView().click()
		})

	})

})
