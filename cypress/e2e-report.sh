#!/bin/bash

cypress_command="$1"
variant="$2"
output_dir="$3"

if [[ "$variant" != "PAT" && "$variant" != "HCP" ]]; then
  echo "Invalid variant. Please use PAT or HCP."
  exit 1
fi

if [ ! -d "$output_dir" ]; then
  mkdir -p "$output_dir"
  echo "Invalid output directory. Created directory: $output_dir"
fi

timestamp=$(date +"%Y-%m-%d_%H-%M-%S")

# capture cypress output into file
{ eval $cypress_command | tee cypress/reports/cypress-output.txt; }
cypress_exit_code=${PIPESTATUS[0]}  # capture the exit code of the cypress_command

if [ "$cypress_exit_code" -eq 0 ]; then
  echo "Cypress run completed successfully."
else
  echo "Cypress run failed with exit code $cypress_exit_code."
fi

cypress_output="cypress/reports/cypress-output.txt"

# Extract the result table
awk '/Run Finished/ {flag=1} flag' "$cypress_output" > cypress/reports/extracted_content.txt

# Here i had to convert the file to markdown in order to preserve the format of the table
echo '```' > cypress/reports/extracted_content.md
cat cypress/reports/extracted_content.txt >> cypress/reports/extracted_content.md
echo '```' >> cypress/reports/extracted_content.md

# intermediate step to convert to html
pandoc -s cypress/reports/extracted_content.md -o cypress/reports/cypress-output.html

pdf_file_name="${output_dir}/cypress-output_${variant}_${timestamp}.pdf"

# finally convert to pdf
wkhtmltopdf --enable-local-file-access cypress/reports/cypress-output.html "$pdf_file_name"

rm cypress/reports/extracted_content.txt cypress/reports/extracted_content.md cypress/reports/cypress-output.html cypress/reports/cypress-output.txt

exit $cypress_exit_code
