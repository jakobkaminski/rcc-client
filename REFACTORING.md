# Refactoring

This document is meant to guide refactoring attempts / processes. - Everything up to debate. The below sections tell the current state of agreement.

Create a merge request in order to propose changes to this document.

'Refactoring' in this document refers to any changes to improve the code without adding actual functionality: Increasing readability, maintainability, extensibility or performance, reducing complexity or redundancy. What these things actually mean will probably vary highly depending on whom you ask, so they are are up to discussion. Current state of agreement is meant to be tracked in the goals section.

The Process on how we go about the refactoring in general is subject of the process section. Also up to debate and that section tracks the current state of agreement.

## Goals

These goals should give you ideas on what to look out for in order to improve the code.
They also serve as justification for changing that thing that has been annoying you for so long.

Feel free to add, discuss, complain :)

* Get rid of Ionic
* Decouple portions of code
* Abstract code portions to reduce redundancy[^1]
* ~~Improving documentation~~[^2]
* ~~Adding Unit tests~~[^2]
* ~~Adding E2E tests (beyond those already in the software requirements)~~[^2]
* Splitting code into smaller self-contained pieces.
* Update Naming for more expressiveness
* Update Naming for more coherence
* splitting up code into smaller single purpose functions
* improving consistency
* Explanatory comments (when all else fails)
* Improve code readability
* Lessen repetition ( do not repeat yourself )
* Enable another feature
* Enable specific E2E test
* Improve accessibility
* Make things faster to build
* Make things faster to run
* Get rid of an external dependency
* Reduce (reasons for) warnings in js console

...?`

[^1] (needs a rule of thumb where to stop :D)
[^2] Not part of refactoring / maybe needs its own process?

## Process

Refactoring is meant to be part of every sprint, either by planning a project or completing refactoring issues.

### Refactoring Ideas/Notes

This part is about anything, that is not a full fledged refactoring proposal. If you come across some oddity
or annoyance or anything that is worth improving, that is too complex to solve or update on the fly, take a note:

* Create an issue roughly describing what you found and believe needs improvement.
* Add label 'refactoring' and 'draft'.
* Add to Milestone %7+
* Close when taken up by a Refactoring project or when out-of-date / no longer relevant.

### Refactoring Proposals/Planning

These are actually planned out refactoring projects; can be as small as updating a single pipe or component or as
big as revamping all the modals infrastructure). The idea here is, to add – if needed – multiple sub tasks/issues
to this parent issue and discuss everything with the team.

* Create an issue in %32+
* Add label 'refactoring' and 'draft' and 'project'
* Point out what needs improvement and which of the above goals you are aiming at.
* Announce in team channels / standup / sprint meeting.
* Discuss with team (in comments or meetings)
* Add subtasks as needed
* Get Approval in Sprint meeting
* Remove 'draft' label
* Close when all sub issues are done.


### When to refactor?

* Refactoring should be part of every sprint.
* At times we should have refactoring dedicated sprints as well (Any suggestion on how often or what should trigger something like that?)



