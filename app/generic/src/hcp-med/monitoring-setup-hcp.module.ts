import { NgModule } from '@angular/core'
import { RccMonitoringSetupModule } from '@rcc/features'
import { DAILY_NOTES_ID } from '@rcc/features/curated/curated-questions/curated-default-questions'

@NgModule({
    imports: [
        RccMonitoringSetupModule.forRoot({
            questionCategories: [
                'symptoms',
                'medication',
                'resources',
                'early_warning_signs',
                'side_effects',
                'withdrawal_symptoms',
                'daily_notes'
            ],
            hiddenOnEditCategories: [
                'daily_notes'
            ],
            hiddenInCatalogue: [
                DAILY_NOTES_ID
            ],
            defaultQuestionsByCategory: new Map(
                [['daily_notes', [DAILY_NOTES_ID]]]
            ),
            addTemplateQuestionsTo: 'symptoms'
        }),
    ]
})
export class RccMonitoringSetupHcpModule {}
