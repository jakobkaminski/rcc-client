import	{	Component, OnInit		}	from '@angular/core'
import { RccTitleService } from '@rcc/common'

@Component({
	templateUrl:	'./terms-and-conditions-page.component.html',
	styleUrls:		['./legal-common.scss'],
})
export class TermsAndConditionsPageComponent implements OnInit{

	public constructor(private rccTitleService: RccTitleService){}

	public ngOnInit(): void {
		this.rccTitleService.setTitle('LEGAL.TERMS.MENU_ENTRY')
	}}
