import	{	NgModule							}	from '@angular/core'
import	{
			LogoVariants,
			provideLogoVariants,
			provideTranslationMap,
			RccTitleService
		}											from '@rcc/common'


import en from './i18n/en.json'
import de from './i18n/de.json'

// Repose logo is currently the same as PAT
export const ReposeLogoVariants: LogoVariants = {
	full: 'assets/svg/rcc-logo-pat-full.svg',
	minimal: 'assets/svg/rcc-logo-pat-minimal.svg'
}

@NgModule({

	providers: 	[
					provideTranslationMap(null, { en,de }),
					provideLogoVariants(ReposeLogoVariants),
				],

})

export class BaseReposeModule {
	public constructor(private rccTitleService: RccTitleService) {
		this.rccTitleService.setTitleSuffix('PAGE_TITLE_PREFIX')
	}
}
