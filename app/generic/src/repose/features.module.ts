import	{	NgModule						}	from '@angular/core'

import	{
			RccServiceWorkerModule,
			QrCodeModule,
			ScheduledNotificationModule,
			RccTitleModule,

			// Main menu entries:
			HomePageMainMenuEntryModule,
			SettingsMainMenuEntryModule,
			QrCodeMainMenuEntryModule,

			// Settings
			RccTransmissionSettingsModule,
		}										from '@rcc/common'

import	{
			QrCodeScannerServiceModule,

			IndexedDbModule,

			SymptomCheckViewModule,
			SymptomCheckQueryModule,

			ImportSymptomCheckStoreServiceModule,
			ImportQuestionStoreServiceModule,

			CombinedTransmissionModule,

			JournalServiceModule,

			ZipModule,

			BasicQueryWidgetsModule,
			DayQueryRemindersModule,

			// Main menu entries:
			BackupMainMenuEntryModule,

			// Homepage entries:
			JournalServiceHomePageEntryModule,

			// Basics:
			BasicReportRepresentationModule,
			BasicReportPreparatorModule,
			BasicDataViewWidgetsModule,
			BasicSymptomCheckRepresentationModule,

			// Fallbacks:
			FallbackDataViewWidgetsModule,

			// Settings:
			IcsRemindersServiceModule,

			// Setting entry groups
			RccReminderSettingsEntryGroupModule,
			RccLanguageSettingsEntryGroupModule,
			RtcTransmissionModule,
			RccZammadModule,
			RccUserSupportDefaultDebugInfoModule,
		}										from '@rcc/features'

import {
			ReposeModule,
			ReposeQueryModule,
			ReposeQueryHomePageEntryModule,
			ReposeOnboardingModule,
			ReposeOnboardingMainMenuEntryModule,
			ReposeSporadicSymptomCheckStoreModule,
			ReposePeriodicSymptomCheckStoreModule
		}										from '@rcc/repose'

import	{	BaseReposeModule				}	from '../repose/base-repose.module'
import	{	RccLegalModule					}	from '../repose/legal'
import	{
			UpdateModule,
			UpdatesMainMenuEntryModule
		}										from '@rcc/features/updates'

@NgModule({
	imports: [
		BaseReposeModule,
		RccLegalModule,


		IndexedDbModule,
		RccTitleModule,
		UpdateModule,

		// Repose modules
		ReposeModule,
		ReposeOnboardingModule,
		ReposeQueryModule,
		ReposePeriodicSymptomCheckStoreModule,
		ReposeSporadicSymptomCheckStoreModule,

		ScheduledNotificationModule,

		RccServiceWorkerModule,
		QrCodeModule,
		QrCodeScannerServiceModule,
		ZipModule,

		RccZammadModule.forRoot(
			{ url:'https://zammad.recoverycat.de' }
		),
		RccUserSupportDefaultDebugInfoModule,


		// Main menu entries
		HomePageMainMenuEntryModule.addEntry({ position: .5 }),
		ReposeOnboardingMainMenuEntryModule.addEntry({ position: 1 }),
		QrCodeMainMenuEntryModule.addEntry({ position: 1 }),
		SettingsMainMenuEntryModule.addEntry({ position: 2 }),
		BackupMainMenuEntryModule.addEntry({ position: 3 }),
		UpdatesMainMenuEntryModule.addEntry({ position: -2 }),


		// Homepage entries
		ReposeQueryHomePageEntryModule.addEntry({ position: 1 }),
		JournalServiceHomePageEntryModule.addEntry({ position: 2 }),

		JournalServiceModule,

		BasicQueryWidgetsModule,
		DayQueryRemindersModule,

		SymptomCheckViewModule,
		SymptomCheckQueryModule,

		ImportSymptomCheckStoreServiceModule,
		ImportQuestionStoreServiceModule,

		// should use a different way to configure:
		CombinedTransmissionModule.forRoot(
			'wss://signaling.recoverycat.de',
			[
				'stun:stun.recoverycat.de'
			]
		),
		RtcTransmissionModule.forRoot(
			'wss://signaling.recoverycat.de',
			[
				'stun:stun.recoverycat.de'
			]
		),

		// Basics
		BasicReportRepresentationModule,
		BasicReportPreparatorModule,
		BasicDataViewWidgetsModule,
		BasicSymptomCheckRepresentationModule,

		// Fallbacks
		FallbackDataViewWidgetsModule,

		// Settings
		RccTransmissionSettingsModule,
		IcsRemindersServiceModule,

		// Setting entry groups
		RccReminderSettingsEntryGroupModule,
		RccLanguageSettingsEntryGroupModule,

	],
})
export class RccFeaturesModule{}
