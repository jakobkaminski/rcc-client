import	{	Component				}	from '@angular/core'
import	{	gotoSupportFormAction	}	from '@rcc/features/user-support'
import	{	Action					}	from '@rcc/common'

@Component({
	templateUrl:	'./support-page.component.html',
	styleUrls:		['./legal-common.scss'],
})
export class SupportPageComponent {

	public gotoSupportFormAction : Action = gotoSupportFormAction
	
}
