import	{	NgModule							}	from '@angular/core'
import	{	RouterModule, Routes				}	from '@angular/router'
import	{
			MainMenuEntry,
			provideMainMenuMetaEntry,
			provideTranslationMap,
			SharedModule
		}											from '@rcc/common'
import	{	ImprintPageComponent				}	from './imprint-page.component'
import	{	SupportPageComponent				}	from './support-page.component'
import	{	RccScrollToTopButtonComponent		}	from '@rcc/themes/bengal/scroll-to-top-button'


import en from './i18n/en.json'
import de from './i18n/de.json'

const mainMenuMetaEntries 	: 	MainMenuEntry[]
							=	[
									{
										label: 	'LEGAL.IMPRINT.MENU_ENTRY',
										path:	'imprint',
										icon:	undefined
									},
									{
										label:	'LEGAL.SUPPORT.MENU_ENTRY',
										path:	'support',
										icon:	undefined
									}
								]


const routes				: Routes
							=	[
									{
										path: 		'imprint',
										component: 	ImprintPageComponent
									},
									{
										path: 		'support',
										component:	SupportPageComponent
									}
								]

@NgModule({
	declarations: [
		ImprintPageComponent,
		SupportPageComponent
	],
	imports:[
		SharedModule,
		RouterModule.forChild(routes),
		RccScrollToTopButtonComponent
	],
	providers:[
		provideTranslationMap('LEGAL', { en,de }),
		...mainMenuMetaEntries.map(provideMainMenuMetaEntry)
	]
})
export class RccLegalModule {}
