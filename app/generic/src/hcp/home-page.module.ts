import	{	NgModule							}	from '@angular/core'

import	{
		}											from '@rcc/common'

import	{
			RccMonitoringSetupHomePageEntryModule,
			RccCombinedSessionRequestHomepageEntryModule,
			OpenSessionHomePageEntryModule,
		}											from '@rcc/features'

@NgModule({
	imports : [
		RccMonitoringSetupHomePageEntryModule
		.addEntry({ position: 1 }),

		RccCombinedSessionRequestHomepageEntryModule
		.addEntry({ position: 2 }),

		OpenSessionHomePageEntryModule
	]
})
export class HomePageModule {}
