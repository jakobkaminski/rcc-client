import	{	NgModule						}	from '@angular/core'
import	{	RccHcpUserManualModule			}	from '@rcc/features'
import	{	RccLegalModule					}	from '../hcp/legal'


@NgModule({
	imports: [
		RccHcpUserManualModule,
		RccLegalModule,
	]
})
export class NonMedicalFeaturesModule {}
