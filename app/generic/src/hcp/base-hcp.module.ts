import	{	NgModule							}	from '@angular/core'
import	{
			provideTranslationMap,
			provideLogoVariants,
			LogoVariants,
		} 											from '@rcc/common'
import	{	RccTitleService						}	from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

export const HCPLogoVariants: LogoVariants = {
	full: 'assets/svg/rcc-logo-hcp-full.svg',
	minimal: 'assets/svg/rcc-logo-hcp-minimal.svg'
}

@NgModule({

	providers:		[
						provideTranslationMap(null, { en,de }),
						provideLogoVariants(HCPLogoVariants),
					]
})

export class 	BaseHCPModule {
	public constructor(private rccTitleService: RccTitleService) {
		this.rccTitleService.setTitleSuffix('PAGE_TITLE_PREFIX')
	}
}
