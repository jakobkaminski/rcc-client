import	{	NgModule						}	from '@angular/core'

import {
			RccServiceWorkerModule,
			QrCodeModule,
			RccTitleModule,

			// Settings
			RccTransmissionSettingsModule,
		}										from '@rcc/common'

import	{

			QrCodeScannerServiceModule,
			IndexedDbModule,
			PDFModule,
			ZipModule,
			FhirModule,
			JsonModule,

			BackupModule,

			// Sessions
			SessionHomePageModule,

			CustomSymptomCheckStoreServiceModule,

			// Curated Questions
			CuratedSymptomCheckStoreServiceModule,

			CuratedQuestionStoreServiceModule,

			// Transmissions
			CombinedTransmissionModule,

			// Medication:
			MedicationModule,

			// Basics:
			BasicReportRepresentationModule,
			BasicReportPreparatorModule,
			BasicQueryWidgetsModule,	// For previews
			BasicQuestionEditWidgetsModule,
			BasicDataViewWidgetsModule,
			BasicSymptomCheckRepresentationModule,

			ComplexDataViewWidgetsModule,

			// Fallbacks:
			FallbackDataViewWidgetsModule,
			FallbackQueryWidgetsModule,


			// Setting entry groups
			RccReminderSettingsEntryGroupModule,
			RccLanguageSettingsEntryGroupModule,

			RccMonitoringSetupQuestionWidgetsModule,
			RtcTransmissionModule,

			RccUsageLoggingModule,
			MatomoModule,

			ReportStackedViewModule,
			RccUsageLoggingUsertypeModule,
			RccUserSupportDefaultDebugInfoModule,
			RccZammadModule,
			RccRuntimeStartupContractModalModule,
			UpdateModule,

			RccRuntimeStartupBannerModule

		}										from '@rcc/features'

import	{	BaseHCPModule					}	from '../hcp/base-hcp.module'

import 	{ 	RccMonitoringSetupHcpModule 	} 	from '../hcp/monitoring-setup-hcp.module'

import	{	MainMenuModule					}	from '../hcp/main-menu.module'
import	{	HomePageModule					}	from '../hcp/home-page.module'

import	{
			MonitoringSetupCustomTemplatesModule
		}										from '@rcc/features/monitoring-setup-custom-templates/monitoring-setup-custom-templates.module'
import	{	environment						} 	from '../environments/environment'


@NgModule({
	imports: [
		BaseHCPModule,

		MainMenuModule,
		HomePageModule,

		IndexedDbModule,
		RccMonitoringSetupHcpModule,
		RccMonitoringSetupQuestionWidgetsModule,
		RccTitleModule,

		RccServiceWorkerModule,
		QrCodeModule,

		QrCodeScannerServiceModule,
		PDFModule,
		ZipModule,
		FhirModule,
		JsonModule,
		RccZammadModule.forRoot(
			{ url: 'https://zammad.recoverycat.de' }
		),
		RccUserSupportDefaultDebugInfoModule,

		// Settings:
		RccTransmissionSettingsModule,



		// Sessions
		SessionHomePageModule,


		ReportStackedViewModule,

		CustomSymptomCheckStoreServiceModule,
		CuratedSymptomCheckStoreServiceModule,
		MonitoringSetupCustomTemplatesModule,

		CuratedQuestionStoreServiceModule,

		// Medication:
		MedicationModule,

		// should use a different way to configure:
		CombinedTransmissionModule.forRoot(
			'wss://signaling.recoverycat.de',
			[
				'stun:stun.recoverycat.de'
			]
		),
		RtcTransmissionModule.forRoot(
			'wss://signaling.recoverycat.de',
			[
				'stun:stun.recoverycat.de'
			]
		),

		BackupModule,

		// Basics:
		BasicReportRepresentationModule,
		BasicReportPreparatorModule,
		BasicQueryWidgetsModule,	// For Previews
		BasicQuestionEditWidgetsModule,
		BasicDataViewWidgetsModule,
		BasicSymptomCheckRepresentationModule,

		ComplexDataViewWidgetsModule,



		// Fallbacks:

		FallbackDataViewWidgetsModule,
		FallbackQueryWidgetsModule,

		// Setting entry groups
		RccReminderSettingsEntryGroupModule,
		RccLanguageSettingsEntryGroupModule,

		// data logging
		RccUsageLoggingModule,
		MatomoModule.forRoot({
			url: environment.matomo.url,
		}),

		// logging types
		RccUsageLoggingUsertypeModule,

		RccRuntimeStartupBannerModule,
		RccRuntimeStartupContractModalModule,

		UpdateModule,
	],
})

export class RccBaseFeaturesModule { }
