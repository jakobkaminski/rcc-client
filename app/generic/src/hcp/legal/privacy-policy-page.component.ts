import	{	Component, OnInit		}	from '@angular/core'
import	{	RccTitleService			}	from '@rcc/common'

@Component({
	templateUrl:	'./privacy-policy-page.component.html',
	styleUrls:		['./legal-common.scss'],
})
export class PrivacyPolicyPageComponent implements OnInit{

	public constructor(private rccTitleService: RccTitleService){}

	public ngOnInit(): void {
		this.rccTitleService.setTitle('LEGAL.PRIVACY_POLICY.MENU_ENTRY')
	}}
