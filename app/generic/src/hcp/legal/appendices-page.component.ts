import	{	Component, OnInit		}	from '@angular/core'
import	{	RccTitleService			}	from '@rcc/common'

@Component({
	templateUrl:	'./appendices-page.component.html',
	styleUrls:		['./legal-common.scss'],
})
export class AppendicesPageComponent implements OnInit{

	public constructor(private rccTitleService: RccTitleService){}

	public ngOnInit(): void {
		this.rccTitleService.setTitle('LEGAL.TERMS.APPENDICES_PAGE_TITLE')
	}
}
