import	{	NgModule						}	from '@angular/core'
import	{	RccBaseFeaturesModule			}	from '../hcp/base-features.module'
import	{	NonMedicalFeaturesModule		}	from '../hcp/non-medical-features.module'


@NgModule({
	imports: [
		RccBaseFeaturesModule,
		NonMedicalFeaturesModule,
	],
})

export class RccFeaturesModule { }
