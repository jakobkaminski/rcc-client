import	{	NgModule							}	from '@angular/core'

import	{
			QrCodeHomePageEntryModule,
		}											from '@rcc/common'

import	{
			DayQueryHomePageEntryModule,
			CameraSwitchHomePageEntryModule,
			ReportStackedViewHomePageEntryModule,
		}											from '@rcc/features'

@NgModule({
	imports : [
		QrCodeHomePageEntryModule.addEntry({ position: 3 }),
		DayQueryHomePageEntryModule.addEntry({ position: 1 }),
		CameraSwitchHomePageEntryModule.addEntry({ position: -2 }),
		ReportStackedViewHomePageEntryModule.addEntry({ position: 2 }),

	]
})

export class HomePageModule {}
