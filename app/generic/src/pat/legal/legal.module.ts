import	{	NgModule							}	from '@angular/core'
import	{	RouterModule, Routes				}	from '@angular/router'
import	{
			MainMenuEntry,
			provideMainMenuMetaEntry,
			provideTranslationMap,
			SharedModule
		}											from '@rcc/common'
import	{	PrivacyPolicyPageComponent			}	from './privacy-policy-page.component'
import	{	TermsAndConditionsPageComponent		}	from './terms-and-conditions-page.component'
import	{	AppendicesPageComponent				}	from './appendices-page.component'
import	{	ImprintPageComponent				}	from './imprint-page.component'
import	{	SupportPageComponent				}	from './support-page.component'
import	{	RccUserManualSharedModule			}	from '@rcc/features/user-manual/user-manual-shared/user-manual-shared.module'
import	{	RccScrollToTopButtonComponent		}	from '@rcc/themes/bengal/scroll-to-top-button'

import en from './i18n/en.json'
import de from './i18n/de.json'

const mainMenuMetaEntries 	: 	MainMenuEntry[]
							=	[
									{
										label: 	'LEGAL.IMPRINT.MENU_ENTRY',
										path:	'imprint',
										icon:	undefined
									},

									{
										label: 	'LEGAL.TERMS.MENU_ENTRY',
										path:	'terms-and-conditions',
										icon:	undefined
									},

									{
										label: 	'LEGAL.PRIVACY_POLICY.MENU_ENTRY',
										path:	'privacy-policy',
										icon:	undefined
									},
								]


const routes				: Routes
							=	[
									{
										path: 		'privacy-policy',
										component: 	PrivacyPolicyPageComponent
									},
									{
										path: 		'imprint',
										component: 	ImprintPageComponent
									},
									{
										path: 		'terms-and-conditions',
										component: 	TermsAndConditionsPageComponent
									},
									{
										path: 		'terms-and-conditions/appendices',
										component: 	AppendicesPageComponent
									},
									{
										path: 		'support',
										component:	SupportPageComponent
									}
								]

@NgModule({
	declarations: [
		ImprintPageComponent,
		PrivacyPolicyPageComponent,
		TermsAndConditionsPageComponent,
		AppendicesPageComponent,
		SupportPageComponent
	],
	imports:[
		SharedModule,
		RouterModule.forChild(routes),
		RccUserManualSharedModule,
		RccScrollToTopButtonComponent
	],
	providers:[
		provideTranslationMap('LEGAL', { en,de }),
		...mainMenuMetaEntries.map(provideMainMenuMetaEntry)
	]
})
export class RccLegalModule {}
