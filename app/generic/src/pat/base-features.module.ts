import	{	NgModule										}	from '@angular/core'

import	{

			RccServiceWorkerModule,
			QrCodeModule,
			ScheduledNotificationModule,
			RccTitleModule,

			// Settings
			RccTransmissionSettingsModule,

		}														from '@rcc/common'

import	{
			QrCodeScannerServiceModule,

			IndexedDbModule,

			SymptomCheckViewModule,
			SymptomCheckQueryModule,

			ImportSymptomCheckStoreServiceModule,
			ImportQuestionStoreServiceModule,

			CombinedTransmissionModule,

			JournalServiceModule,

			BasicQueryWidgetsModule,

			DayQueryModule,
			DayQueryRemindersModule,


			FulFillSessionRequestModule,

			// Entries
			EntryShareServiceModule,

			// Basics:
			BasicReportRepresentationModule,
			BasicReportPreparatorModule,
			BasicDataViewWidgetsModule,
			BasicSymptomCheckRepresentationModule,

			ComplexDataViewWidgetsModule,

			// Fallbacks:
			FallbackDataViewWidgetsModule,


			// Setting entry groups
			RccReminderSettingsEntryGroupModule,
			RccLanguageSettingsEntryGroupModule,
			RtcTransmissionModule,
			IcsRemindersServiceModule,

			ReportStackedViewModule,
			RccUserSupportDefaultDebugInfoModule,
			RccZammadModule,

			RccRuntimeStartupBannerModule

		}										from '@rcc/features'

import	{	BasePatModule					}	from '../pat/base-pat.module'

import	{	MainMenuModule					}	from '../pat/main-menu.module'
import	{	HomePageModule					}	from '../pat/home-page.module'

import	{
			UpdateModule,
		}										from '@rcc/features/updates'


@NgModule({
	imports: [
		BasePatModule,

		MainMenuModule,
		HomePageModule,

		IndexedDbModule,
		RccTitleModule,

		ScheduledNotificationModule,
		RccZammadModule.forRoot(
			{ url: 'https://zammad.recoverycat.de' }
		),
		RccUserSupportDefaultDebugInfoModule,


		RccServiceWorkerModule,
		QrCodeModule,

		UpdateModule,

		QrCodeScannerServiceModule,

		JournalServiceModule,

		BasicQueryWidgetsModule,
		DayQueryModule,
		DayQueryRemindersModule,

		FulFillSessionRequestModule,

		SymptomCheckViewModule,
		SymptomCheckQueryModule,

		ImportSymptomCheckStoreServiceModule,
		ImportQuestionStoreServiceModule,

		// should use a different way to configure:
		CombinedTransmissionModule.forRoot(
			'wss://signaling.recoverycat.de',
			[
				'stun:stun.recoverycat.de'
			]
		),
		RtcTransmissionModule.forRoot(
			'wss://signaling.recoverycat.de',
			[
				'stun:stun.recoverycat.de'
			]
		),


		// BackupModule,

		// Entries
		EntryShareServiceModule,


		// Basics:
		BasicReportRepresentationModule,
		BasicReportPreparatorModule,
		BasicDataViewWidgetsModule,
		BasicSymptomCheckRepresentationModule,

		ComplexDataViewWidgetsModule,


		// Fallbacks:

		FallbackDataViewWidgetsModule,

		// Settings
		RccTransmissionSettingsModule,
		IcsRemindersServiceModule,

		// Setting entry groups
		RccReminderSettingsEntryGroupModule,
		RccLanguageSettingsEntryGroupModule,

		// statistics

		ReportStackedViewModule,

		RccRuntimeStartupBannerModule,

		// example data:
		// ExampleChartStoresModule,

	],
})
export class RccBaseFeaturesModule{}
