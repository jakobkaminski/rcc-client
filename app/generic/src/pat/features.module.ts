
import	{	NgModule								}	from '@angular/core'
import	{	RccBaseFeaturesModule					}	from '../pat/base-features.module'
import	{	NonMedicalFeaturesModule				}	from '../pat/non-medical-features.module'


@NgModule({
	imports: [
		RccBaseFeaturesModule,
		NonMedicalFeaturesModule
	],
})
export class RccFeaturesModule{}
