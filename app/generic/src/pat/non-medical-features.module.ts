import { NgModule } from '@angular/core'
import { RccPatUserManualModule } from '@rcc/features'
import { RccLegalModule } from '../pat/legal'

@NgModule({
	imports: [
		RccPatUserManualModule,
		RccLegalModule,
	]
})
export class NonMedicalFeaturesModule {}
