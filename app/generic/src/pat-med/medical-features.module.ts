import	{
			NgModule
		}											from '@angular/core'

import	{
			MedicalProductModule,
		}											from '@rcc/common'

import	{
			GlossaryMainMenuEntryModule,
			RccResourcesAndCrisisPlanMainMenuEntryModule,
			RegulatoryInfoPageMetaMenuEntryModule,
			BasicGlossaryEntryRepresentationModule,
			RccPatMedUserManualModule,
			RccResourcesAndCrisisPlanModule,

			AverageTrendComputedValueModule,
			BooleanCountComputedValueModule,
			AnswerCountComputedValueModule,
		}											from '@rcc/features'
import	{	RccLegalModule						}	from '../pat-med/legal'



@NgModule({
	imports: [
		MedicalProductModule,
		BasicGlossaryEntryRepresentationModule,
		RccPatMedUserManualModule,
		RccLegalModule,
		RccResourcesAndCrisisPlanModule,

		// Main menu entries
		GlossaryMainMenuEntryModule.addEntry({ position: 3.5 }),
		RegulatoryInfoPageMetaMenuEntryModule.addEntry({ position: 1 }),
		RccResourcesAndCrisisPlanMainMenuEntryModule.addEntry({ position: 5 }),

		// ComputedValues
		AverageTrendComputedValueModule,
		BooleanCountComputedValueModule,
		AnswerCountComputedValueModule,

	]
})
export class PatMedicalFeaturesModule{}
