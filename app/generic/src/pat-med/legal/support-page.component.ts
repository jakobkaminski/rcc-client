import	{	Component, OnInit		}	from '@angular/core'
import	{	gotoSupportFormAction	}	from '@rcc/features/user-support'
import	{	Action, RccTitleService	}	from '@rcc/common'

@Component({
	templateUrl:	'./support-page.component.html',
	styleUrls:		['./legal-common.scss'],
})
export class SupportPageComponent implements OnInit {
	public constructor(private rccTitleService: RccTitleService){}


	public ngOnInit(): void {
		this.rccTitleService.setTitle('LEGAL.SUPPORT.MENU_ENTRY')
	}

	public gotoSupportFormAction : Action = gotoSupportFormAction

}
