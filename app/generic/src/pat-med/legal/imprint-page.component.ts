import	{	Component, OnInit		}	from '@angular/core'
import	{	RccTitleService			}	from '@rcc/common'

@Component({
	templateUrl:	'./imprint-page.component.html',
	styleUrls:		['./legal-common.scss'],
})
export class ImprintPageComponent implements OnInit{

	public constructor(private rccTitleService: RccTitleService){}

	public ngOnInit(): void {
		this.rccTitleService.setTitle('LEGAL.IMPRINT.MENU_ENTRY')
	}
}
