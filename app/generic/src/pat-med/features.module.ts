
import	{	NgModule									}	from '@angular/core'

import	{	RccBaseFeaturesModule as PatBaseFeatureModule	}	from '../pat/base-features.module'
import	{	PatMedicalFeaturesModule					}	from '../pat-med/medical-features.module'


@NgModule({
	imports: [
		PatBaseFeatureModule,
		PatMedicalFeaturesModule
	],
})
export class RccFeaturesModule{}
