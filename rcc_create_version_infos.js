import { execSync } from 'child_process';
import { writeFileSync } from 'fs';
import path from 'path';


// Call this script with node - or `npm run generate-info` - to generate the `build_info.json` file
// which will be stored in the root folder of this project.
// `build_info.json` is used by the `RccBuildInfoService` and contains info about the current build
// (git commit, build environment, …)
//
// sadly we need environment and flavor as arguments as we can not read them from the config

/**
 * Execute a command and log error if one occurs
 * @param {*} command to be executed
 * @returns a string with the stdout of the command
 */
function execute(command) {
  try {
    return execSync(command).toString().trim();
  } catch (error) {
    console.error(error);
  }
};

/**
 * generate the build info
 */
function generateBuildInfo() {
  // we can not hook into the build process and therefore need environment and flavor
  // handed to the scirpt as args
  const args        = process.argv;
  const environment = args[2] || 'development';
  const flavor      = args[3] || 'unknown';

  // get the relevant information from git
  const commit      = execute('git rev-parse HEAD');
  const describe    = execute('git describe --dirty');
  const short       = execute('git rev-parse --short HEAD');
  const tag         = execute('git describe --tags --abbrev=0 --dirty');

  const tagWithoutDirty = tag.replace('-dirty', '');
  const tagDate     = execute(`git log -1 --format=%aI ${tagWithoutDirty}`);

	console.log(tagDate)

  // create JSON object
  const json = {
    environment,
    flavor,
    commit,
    describe,
    short,
    tag,
	  tagDate,
  };

  // save json to files
  const rootDirfile = path.resolve('build_info.json');
  const fileContents = JSON.stringify(json, null, 2);

  writeFileSync(rootDirfile, fileContents);
  console.log(`Saved following info to ${rootDirfile}\n${fileContents}`);
}

generateBuildInfo()
