import path from 'path'

export default {
  module: {
    rules: [
      {
        test: /\.(pdf)$/i,
        type: 'asset/resource'
      },
      {
        test: /.(md)$/i,
        use: ['html-loader', path.resolve('loaders/markdown-loader.js')]
      }
    ],
  },
}
