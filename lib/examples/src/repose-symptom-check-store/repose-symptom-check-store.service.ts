import 	{ 	Injectable 			}	from '@angular/core'

import	{
			ItemStorage,
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
		}							from '@rcc/core'


@Injectable()
export class ReposeExampleSymptomCheckStore extends SymptomCheckStore {

	public constructor() {
		super(staticStorage)
	}
}

const staticStorage : ItemStorage<SymptomCheck, SymptomCheckConfig> = { getAll: () => Promise.resolve(configs) }

const configs : SymptomCheckConfig[] = [
	// Sporadic SC
	{
		meta:			{
							label: 				'Repose example symptom check',
							defaultSchedule:	{
													startDate: '2023-06-01',
													interval: [9]
												},
							creationDate:		'2023-06-20T13:00:00.0Z',
						},
		questions:		['BSI53_1', 'BSI53_2', 'BSI53_3'],
		presentation: 	{
							slides:	[
										{
											title:		{ translations: { en: 'slide title',  de: 'slide titel' } },
											message:	{ translations: { en: 'This symptom check is sporadic',  de: 'Dieser Symptom Check ist sporadisch' } }
										},
										'BSI53_2',
										'BSI53_3',
										'BSI53_1'
									]
						}
	},

	// Periodic SC
	{
		meta:			{
							label: 				'Repose example symptom check 2',
							defaultSchedule:	[
													[],
													[]
												],
							creationDate:		'2023-06-22T15:10:00.0Z'
						},
		questions:		['PVSS21_1', 'PVSS21_2', 'PVSS21_3'],
		presentation: 	{
							slides:	[
										{
											title:		{ translations: { en: 'slide title',  de: 'slide titel' } },
											message:	{ translations: { en: 'This symptom check is periodic',  de: 'Dieser Symptom Check ist periodisch' } }
										},
										'PVSS21_1',
										'PVSS21_2',
										'PVSS21_3'
									]
						}
	},
]
