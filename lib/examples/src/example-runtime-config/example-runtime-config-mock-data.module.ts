import	{
			NgModule,
			Injectable,
		}									from '@angular/core'


import	{
			RccPublicRuntimeConfigModule,
			RccPublicRuntimeConfigService,
		}									from '@rcc/common'



const mockedConfig	:	unknown
					= 	{
							test: {
								myFirstValue: 'my first mocked value'
							},
							matomo: {
								'site-id': '99'
							},
							startup:{
								banner:{
									color:'secondary',
									markdown:{
										translations:{
											'de': '__fett__ normal<br>Zeilenumbruch',
											'en': '__bold__ normal<br>line break'
										}
									}
								},
								'contract-modal': {
									content: {
										translations: {
											de: `# Das ist eine Testversion

Um die App zu nutzen, müssen Sie den [Allgemeinen Geschäftsbedingungen](/terms-and-conditions) und den [Datenschutzhinweisen](/privacy-policy) zustimmen.

Abweichend zu den Allgemeinen Geschäftsbedingungen gilt in diesem [Angebot der Testversion und der entsprechenden Leistungsbeschreibung](http://www.example.com) eine eingeschränkte Nutzung für eine:n Nutzer:in: **Mit der Testversion können für maximal 10 Patient:innen Therapie Follow-ups erstellt werden, und die Nutzung ist begrenzt bis 30.04.2024.**

Im Rahmen der Testversion bitten wir um die Teilnahme an einer Nutzer:innenumfrage.

_Ich habe die Allgemeinen Geschäftsbedingungen, Datenschutzhinweisen, das Angebot der Testversion und die Leistungsbeschreibung gelesen und erkläre mich mit diesen Bedingungen einverstanden._`,

											en: '# This is a test version'
										}
									},
									userConfirmationKey: 'mock-contract-agreement',
									excludedRoutes: ['/terms-and-conditions', '/privacy-policy']
								}
							}
						}

@Injectable()
export class MockRccPublicRuntimeConfigService extends RccPublicRuntimeConfigService{

	protected async loadConfig() : Promise<unknown> {

		return await Promise.resolve(mockedConfig)
	}

}


@NgModule({
	imports:[
		RccPublicRuntimeConfigModule,
	],
	providers : [
		{
			provide:	RccPublicRuntimeConfigService,
			useClass:	MockRccPublicRuntimeConfigService
		},
	]
})
export class MockRccPublicRuntimeConfigModule {

	public constructor(
		public rccPublicRuntimeConfigService: RccPublicRuntimeConfigService
	){
		console.info('Mocking RccPublicRuntimeConfigService using this config', mockedConfig)
	}
}
