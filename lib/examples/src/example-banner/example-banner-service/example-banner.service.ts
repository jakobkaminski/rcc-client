import { Injectable } from '@angular/core'
import { RccBannerService } from '@rcc/common/banners/banner-service/banner.service'

@Injectable()
export class ExampleBannerService {
	public constructor(rccBannerService: RccBannerService) {
		const bannerDelaySeconds: number = 2.5
		setTimeout(() => {
			rccBannerService.next({
				color: 'primary',
				markdown:{
					translations: {
						'de': 'Das is __fett__',
						'en': 'This is __bold__'
					}
				}
			})
		}, bannerDelaySeconds * 1000)
	}
}
