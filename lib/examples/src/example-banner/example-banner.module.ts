import { NgModule } from '@angular/core'
import { ExampleBannerService } from './example-banner-service/example-banner.service'
import { RccBannerService } from '@rcc/common/banners/banner-service/banner.service'


@NgModule({
	providers: [
		ExampleBannerService,
	],
})
export class ExampleBannerModule {
	public constructor(rccBannerService: RccBannerService) {
		const bannerDelaySeconds: number = 2.5
		setTimeout(() => {
			rccBannerService.next({
				color: 'primary',
				markdown:{
					translations: {
						'de': '__Dies ist eine Testversion.__ <br/> Hier können Sie die <a href="mailto:psy@recovery.cat?subject=Anfrage%20zu%20Recovery%20Cat%20(HCP)">Vollversion </a>anfragen.',
						'en': '__This is a test version.__ <br/> You can request the <a href="mailto:psy@recovery.cat?subject=Inquiry%20about%20Recovery%20Cat%20(HCP)">full version here</a>.'
					}
				}
			})
		}, bannerDelaySeconds * 1000)
	}
}
