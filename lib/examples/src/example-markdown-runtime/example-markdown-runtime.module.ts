import { NgModule } from '@angular/core'
import { RccModalController } from '@rcc/common'
import { ExampleMarkdownRuntimeModalComponent } from './example-markdown-runtime-modal/example-markdown-runtime-modal.component'

@NgModule({
	imports: [
	]
})
export class ExampleMarkdownRuntimeModule {
	public constructor(rccModalController: RccModalController) {
		setTimeout(() => {
			void rccModalController.present(ExampleMarkdownRuntimeModalComponent)
		}, 500)
	}
}
