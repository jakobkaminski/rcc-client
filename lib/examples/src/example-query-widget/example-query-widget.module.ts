/**
 * This is a quick example of how to add query widget (a widget used to answer questions) to the recovery cat app.
 * Everything was put into one file for demonstration purposes.
 *
 * This example includes:
 * * a query widget
 *
 * In order for this module to do anything, it has to be imported into th AppModule (already done).
 *
 * This example builds on the question example (ExampleQuestionsModule).
 *
 * The example question(s) of ExampleQuestionsModule a should show up in the QuestionnaireService.
 * If you click to answer them, the following widget should be rendered.
 */


import 	{
			Component,
			NgModule,
		}											from '@angular/core'

import	{
			QuestionnaireServiceModule,
			QueryControl
		}											from '@rcc/features'
import 	{
			SharedModule,
			WidgetComponent,
			provideWidget
		} 											from '@rcc/common'




@Component({
	template:   `
					<p>---The example widget starts here---</p>

					<section>
						<h3>This is the question data:</h3>
						<pre>{{this.queryControl.question|json}}</pre>
					</section>

					<ion-item lines = "full">
						<ion-input
							[type]			= "queryControl.question.type === 'string' ? 'text' : 'number'"
							[formControl] 	= "queryControl.answerControl"
							[debounce] 		= "500"
						></ion-input>
					</ion-item>

					<p>---The example widget ends here---</p>
				`
})
export class ExampleWidgetComponent extends WidgetComponent<QueryControl> {

	public static controlType = QueryControl

	public static widgetMatch( queryControl: QueryControl ): number {

		return 	queryControl.question.tags?.includes('rcc-example-question')
				?	2
				:	-1

	}

	public constructor(
		public queryControl: QueryControl
	){
		super(queryControl)




	}

}





@NgModule({
	imports: [
		SharedModule,
		QuestionnaireServiceModule,
	],
	providers:[
		provideWidget(ExampleWidgetComponent)
	],
	declarations:[
		ExampleWidgetComponent
	],
	exports: [
		ExampleWidgetComponent
	]
})
export class ExampleQueryWidgetsModule {}
