import	{
			NgModule
		}										from '@angular/core'

import	{
			RccPublicRuntimeTextResourceModule,
			RccPublicRuntimeTextResourceService,
			requestPublicRuntimeTextResource
		}										from '@rcc/common'


@NgModule({
	imports:[
		RccPublicRuntimeTextResourceModule
	],
	providers : [
		requestPublicRuntimeTextResource({
			name:					'my_example_text_%lang.md',
			description:			'Language dependent example.',
			languageIndependent: 	false,
			required:				false
		}),

		requestPublicRuntimeTextResource({
			name:					'my_example_emoji.txt',
			description:			'Language independent example.',
			languageIndependent: 	true,
			required:				false
		})

	]
})
export class ExampleRuntimeResourceModule {

	public constructor(
		rccPublicRuntimeTextResourceService: RccPublicRuntimeTextResourceService
	){
		void	rccPublicRuntimeTextResourceService.get('my_example_emoji.txt')
				.then( value => console.info('Value for ExampleRuntimeResourceModule, "my_example_emoji.txt":', value) )

		void	rccPublicRuntimeTextResourceService.getTranslations('my_example_text_%lang.md')
				.then( value => console.info('Value for ExampleRuntimeResourceModule, "my_example_text_%lang.md":', value) )
	}
}
