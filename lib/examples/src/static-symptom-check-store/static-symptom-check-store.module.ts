import 	{	NgModule 						} 	from '@angular/core'
import	{
			DevModule,
			provideTranslationMap,
		}										from '@rcc/common'

import	{
			SymptomCheckMetaStoreServiceModule,
		}										from '@rcc/features'

import	{	StaticSymptomCheckStoreService			}	from './static-symptom-check-store.service'


import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([StaticSymptomCheckStoreService]),
		DevModule.note('StaticSymptomCheckStoreServiceModule'),
	],
	providers: [
		StaticSymptomCheckStoreService,
		provideTranslationMap('STATIC_SYMPTOM_CHECK_STORE', { en, de })
	]
})
export class StaticSymptomCheckStoreServiceModule{}
