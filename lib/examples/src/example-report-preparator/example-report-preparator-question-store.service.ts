import  {   Injectable			}	from '@angular/core'

import  {
			QuestionConfig,
			QuestionStore
		}							from '@rcc/core'




@Injectable()
export class ExampleReportPreparatorQuestionStoreService extends QuestionStore {

	public readonly name = 'EXAMPLE_REPORT_PREPARATOR.QUESTION_STORE_NAME'

	public constructor(){
		super(staticStorage)
	}
}

const staticStorage = { getAll: () => Promise.resolve(configs) }

const options		=	[
							{ value:0  , translations: { en: 'terrible', 	de: 'schrecklich' } },
							{ value:1  , translations: { en: 'bad',  		de: 'schlecht' } },
							{ value:2  , translations: { en: 'okay',  		de: 'okay' } },
							{ value:3  , translations: { en: 'splendid',  	de: 'prächtig' } }
						]
const tags			=	['#my-physical-state']


const configs:QuestionConfig[] = [

		{
			id:       'physical-state-000',
			type:     'integer',
			meaning:  'How are your joints?',
			translations: {
				'en': 'How are your joints?',
				'de': 'Wie geht es Ihren Gelenken?'
			},
			options,
			tags
		},


		{
			id:       'physical-state-001',
			type:     'integer',
			meaning:  'How is your back?',
			translations: {
				'en': 'How is your back?',
				'de': 'Wie geht es Ihrem Rücken?'
			},
			options,
			tags
		},


		{
			id:       'physical-state-002',
			type:     'integer',
			meaning:  'How is your head?',
			translations: {
				'en': 'How is your head?',
				'de': 'Wie geht es Ihrem Kopf?'
			},
			options,
			tags
		},


		{
			id:       'example-breakfast',
			type:     'boolean',
			translations: {
				'en': 'Did you have breakfast today?',
				'de': 'Haben Sie heute schon gefrühstückt?'
			},
			tags
		}


]
