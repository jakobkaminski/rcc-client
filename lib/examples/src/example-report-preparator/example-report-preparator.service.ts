import 	{
			NgModule,
			Injectable
		}								from '@angular/core'

import	{
			Question,
			Report,
			Schedule
		}								from '@rcc/core'

import	{
			Dataset,
			ReportPreparator,
			QuestionnaireService
		}								from '@rcc/features'





@Injectable()
export class ExampleReportPreparatorService extends ReportPreparator{

	public constructor(
		public questionnaireService: QuestionnaireService
	){
		super()
	}



	public async prepare(report: Report): Promise<Dataset[]> {


		await this.questionnaireService.ready

		// getting all questions tagged with: #my-physical-state
		const questions 		= 	this.questionnaireService.items.filter( question => question.tags?.includes('#my-physical-state') )

		// we define a virtual questions with all the info for a proper labeling:
		const virtualQuestion  	= 	new Question({
										id:			'my-virtual-question-overall-physical-state',
										type:		'decimal',
										meaning:	'Overall physical state [generated]',
										min:		1,
										max:		4,
										translations: {
											'en': 'Overall physical state [generated]',
											'de': 'Körperlicher Zustand ingesamt [automatisch]'
										},
										tags: ['#my-physical-state-overall'],
									})



		const dailyValues : Record<string, unknown[]>	= {}

		// Collect all answers, for all questions, for every day.
		// At some days one of the questions is answered multiple times and another one is not answered at all;
		// we don't mind this is just an example.


		report.entries.forEach( entry => {

			const question = questions.find( q => q.id === entry.questionId)

			if(!question) return null

			const date_str = entry.targetDay

			dailyValues[date_str] = dailyValues[date_str] || []
			dailyValues[date_str].push(entry.answer)

		})

		// transfer the values into a data set:
		const dataset	=	{
								question: virtualQuestion,
								// virtual values:
								datapoints:	Object.entries(dailyValues)
											.map( ([isoDateString, values]: any) => ({
												// take the average of all values for one day:
												date:	isoDateString,
												value: 	values.length
														?	values.reduce( (a:number,b:number) => a+b, 0) / values.length
														:	0
											}))
							}


		console.log('|||||', report, dataset)

		return [dataset]

	}
}
