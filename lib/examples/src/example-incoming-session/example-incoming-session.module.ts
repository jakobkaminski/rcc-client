import	{
			NgModule,
		}										from '@angular/core'

import	{
			SymptomCheckConfig
		}										from '@rcc/core'

import	{
			IncomingDataService
		}										from '@rcc/common'

import	{
			OpenSessionModule,
			OpenSessionStoreService,
		}										from '@rcc/features'


import	{
			ExampleReportPreparatorModule,
			ExampleReportPreparatorQuestionStoreService,
			ExampleReportPreparatorReportStoreService
		}										from '../example-report-preparator'



const symptomCheckConfig : SymptomCheckConfig = {
													meta:		{
																	label: 				'Example: Dr. Who',
																	defaultSchedule:	[
																							[],
																							[]
																						],
																	creationDate:		'2020-02-28T16:13:00.0Z'
																},
													questions:	[
																	'physical-state-000',
																	'physical-state-001',
																	'physical-state-002'
																]
												}


@NgModule({
	imports:[
		ExampleReportPreparatorModule,
		OpenSessionModule
	]
})
export class ExampleIncomingSessionModule {

	public constructor(
		exampleReportPreparatorReportStoreService		: ExampleReportPreparatorReportStoreService,
		exampleReportPreparatorQuestionStoreService		: ExampleReportPreparatorQuestionStoreService,
		incomingDataService								: IncomingDataService,
		openSessionStoreService							: OpenSessionStoreService
	){
		Promise.all([
			exampleReportPreparatorReportStoreService.ready
		])
		.then( () => {
			incomingDataService.next({
				symptomCheck: 	symptomCheckConfig,
				report:			exampleReportPreparatorReportStoreService.items[0].config,
				questions:		[]
			})
		})
		.catch( (e) => console.log(e))

	}

}
