import { Component } from '@angular/core'
import { RccTranslationService } from '@rcc/common'
import { Observable, map } from 'rxjs'

import de from '../i18n/de.md'
import en from '../i18n/en.md'

const templates: Record<string, string> = { en, de } as Record<string, string>

@Component({
	templateUrl	: './example-markdown-template-page-view.component.html',
	selector	: 'example-markdown',
})
export class ExampleMarkdownTemplatePageViewComponent {
	public constructor (
		private rccTranslationService: RccTranslationService,
	) {}

	protected markdownTemplate: Observable<string> = this.rccTranslationService.activeLanguageChange$.pipe(
		map((value) => templates[value])
	)
}
