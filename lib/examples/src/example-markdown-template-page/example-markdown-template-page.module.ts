import { NgModule } from '@angular/core'
import { ExampleMarkdownTemplateRouteModule } from './example-markdown-template-route-module/example-markdown-template-route.module'
import { ExampleMarkdownTemplateMenuEntryModule } from './example-markdown-template-menu-entry-module/example-markdown-template-menu-entry.module'
import { RccMarkdownTemplateComponent } from '@rcc/common/ui-components/markdown/markdown-template/markdown-template.component'
import { ExampleMarkdownTemplatePageViewComponent } from './example-markdown-template-page-view/example-markdown-template-page-view.component'
import { CommonModule } from '@angular/common'
import { RccThemeModule } from '@rcc/themes/active'

@NgModule({
	imports: [
		ExampleMarkdownTemplateRouteModule,
		ExampleMarkdownTemplateMenuEntryModule,
		RccMarkdownTemplateComponent,
		CommonModule,
		RccThemeModule
	],
	declarations: [
		ExampleMarkdownTemplatePageViewComponent,
	]
})
export class ExampleMarkdownTemplatePageModule {

}
