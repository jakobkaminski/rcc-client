import 	{	Injectable					}	from '@angular/core'
import	{
			HierarchicalColor,
			isHierarchicalColor,
			CategoricalColor,
			ColorVariants,
		}									from './colors.commons'


export type ColorCategory = HierarchicalColor | CategoricalColor | 'misc'

/**
 * This service is meant to retrieve color values or css variables from the
 * active skin. For Each color referred to by name – hierarchical
 * (e.g. 'primary') or categorical (e.g. 'create'), this service retrieves the
 * corresponding set of css variables or set of computed values. "Set" meaning
 * that every named color comes with a set of 8 colors defined by the
 * {@link ColorVariants} interface.
 *
 */
@Injectable({
	providedIn: 'root'
})
export class RccColorService {

	/**
	 * Fallback colors are meant to be used if something went wrong - colors
	 * are missing or malformed.
	 */

	protected fallbackColorMain						: string = '48, 56, 79'
	protected fallbackColorMainHighlight 			: string = '32, 49, 96'
	protected fallbackColorMainContrast 			: string = '255,255,255'
	protected fallbackColorMainHighlightContrast 	: string = '255,255,255'

	protected fallbackColorSupp 					: string = '130, 145, 189'
	protected fallbackColorSuppHighlight 			: string = '100, 131, 219'
	protected fallbackColorSuppContrast 			: string = '0,0,0'
	protected fallbackColorSuppHighlightContrast 	: string = '0,0,0'





	/**
	 * Wraps the name of a css variable in a css var function adding a default
	 * value if present.
	 */
	public wrapVar(cssVar: string, defaultValue?: string): string {
		return `var(${cssVar},${defaultValue})`
	}

	/**
	 * Wraps the name of a css variable in a css rgb function. Only makes sense
	 * if the css-variable hold an RGB triple, like "32,126,255":
	 * ```
	 * -rcc-primary-rgb-main: 32,126,255
	 * ```
	 */
	public wrapRgb(cssVar: string): string {
		return `rgb(${cssVar})`
	}

	private getVariable(colorString: string, suffix: string): string {
		return `--rcc-${colorString}-rgb-${suffix}`
	}

	private getVariableFallbacks(): ColorVariants {
		return {
			main:					this.fallbackColorMain,
			mainHighlight:			this.fallbackColorMainHighlight,
			mainContrast:			this.fallbackColorMainContrast,
			mainHighlightContrast:	this.fallbackColorMainHighlight,

			supp:					this.fallbackColorSupp,
			suppHighlight:			this.fallbackColorSuppHighlight,
			suppContrast:			this.fallbackColorSuppContrast,
			suppHighlightContrast:	this.fallbackColorSuppHighlightContrast,
		}
	}

	private getVariableVariants(colorString: string): ColorVariants {
		return {
			main:					this.getVariable(colorString, 'main'),
			mainHighlight:			this.getVariable(colorString, 'main-highlight'),
			mainContrast:			this.getVariable(colorString, 'main-contrast'),
			mainHighlightContrast:	this.getVariable(colorString, 'main-highlight-contrast'),

			supp:					this.getVariable(colorString, 'supp'),
			suppHighlight:			this.getVariable(colorString, 'supp-highlight'),
			suppContrast:			this.getVariable(colorString, 'supp-contrast'),
			suppHighlightContrast:	this.getVariable(colorString, 'supp-highlight-contrast'),
		}
	}

	private getWrappedRgbVariants(colorString: string): ColorVariants {
		const variables	: ColorVariants = this.getVariableVariants(colorString)

		return {
			main: 					this.wrapVar(variables.main,					this.fallbackColorMain),
			mainHighlight:			this.wrapVar(variables.mainHighlight, 			this.fallbackColorMainHighlight),
			mainContrast:			this.wrapVar(variables.mainContrast, 			this.fallbackColorMainContrast),
			mainHighlightContrast:	this.wrapVar(variables.mainHighlightContrast,	this.fallbackColorMainHighlightContrast),

			supp:					this.wrapVar(variables.supp, 					this.fallbackColorSupp),
			suppHighlight:			this.wrapVar(variables.suppHighlight, 			this.fallbackColorSuppHighlight),
			suppContrast:			this.wrapVar(variables.suppContrast, 			this.fallbackColorSuppContrast),
			suppHighlightContrast:	this.wrapVar(variables.suppHighlightContrast,	this.fallbackColorSuppHighlightContrast)
		}
	}

	/**
	 * Retrieves all color variants associated with the given hierarchy item.
	 */
	public getHierarchicalRgbVariants(color: HierarchicalColor): ColorVariants {

		const colorString 		: string 	= String(color)
		return this.getWrappedRgbVariants(colorString)
	}

	/**
	 * Retrieves all colors associated with a given category (can be any string)
	 */
	public getCategoricalRgbVariants(color: ColorCategory): ColorVariants {

		const colorString : string = String(color)
		return this.getWrappedRgbVariants(`category-${colorString}`)
	}

	/**
	 * Retrieves all colors associated with a hierarchy item or category.
	 */
	public getColorVariants(color: ColorCategory): ColorVariants & {rgb: ColorVariants}{

		const rgbVariants 	: ColorVariants = 	isHierarchicalColor(color)
												?	this.getHierarchicalRgbVariants(color)
												:	this.getCategoricalRgbVariants(color)

		return	{
					main:					this.wrapRgb(rgbVariants.main),
					mainHighlight:			this.wrapRgb(rgbVariants.mainHighlight),
					mainContrast:			this.wrapRgb(rgbVariants.mainContrast),
					mainHighlightContrast:	this.wrapRgb(rgbVariants.mainHighlightContrast),

					supp:					this.wrapRgb(rgbVariants.supp),
					suppHighlight:			this.wrapRgb(rgbVariants.suppHighlight),
					suppContrast:			this.wrapRgb(rgbVariants.suppContrast),
					suppHighlightContrast:	this.wrapRgb(rgbVariants.suppHighlightContrast),

					rgb: rgbVariants
				}
	}

	/**
	 * Retrieves all colors associated with a hierarchy item or category.
	 * Ensure that all values have been resolved and no css variables appear.
	 * This is useful, if another js library needs actual color values.
	 */
	public getComputedColorVariants(color: ColorCategory, element: HTMLElement ): ColorVariants {

		let computedStyles					: 	CSSStyleDeclaration	= undefined
		let colorVariants 					: 	ColorVariants		= undefined
		let colorFallbacks					: 	ColorVariants		= undefined
		let colorVariantsEntries			: 	[string, string][]	= undefined
		let computedColorVariantsEntries	: 	[string, string][]	= undefined
		let updatedColorVariants			: 	ColorVariants		= undefined

		computedStyles						= 	getComputedStyle(element)
		colorVariants 						= 	this.getVariableVariants(color)
		colorFallbacks						=	this.getVariableFallbacks()
		colorVariantsEntries				=	Object.entries(colorVariants) as [string, string][]
		computedColorVariantsEntries		= 	colorVariantsEntries.map( ([key, value]) => {

													let computedValue : string	= 	undefined

													computedValue = 	value.slice(0,2) === '--'
																		?	computedStyles.getPropertyValue(value)
																		:	value

													return [key, computedValue || colorFallbacks[key as keyof ColorVariants]]
												})

												// We know all the key, because we have not changes them,
												// but it's very hard to have typescript infer the type,
												// so we just tell it :)
		updatedColorVariants				=	Object.fromEntries(computedColorVariantsEntries) as unknown as ColorVariants

		return updatedColorVariants

	}
}
