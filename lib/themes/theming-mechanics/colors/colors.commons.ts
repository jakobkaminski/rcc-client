import	{
			assert,
			isErrorFree
		}						from '@rcc/core'

export interface ColorVariants {

	main					: string // name of a css color variable or color value,
	supp					: string // name of a css color variable or color value,

	mainHighlight			: string // name of a css color variable or color value,
	suppHighlight			: string // name of a css color variable or color value,

	mainContrast			: string // name of a css color variable or color value,
	suppContrast			: string // name of a css color variable or color value,

	mainHighlightContrast	: string // name of a css color variable or color value,
	suppHighlightContrast	: string // name of a css color variable or color value,

}



export type HierarchicalColor 		= 'primary' | 'secondary' | 'tertiary' | 'quaternary' | 'quinary' | 'senary'
export type CategoricalColor 		= 'create'| 'share' | 'receive' | 'analyze' | 'misc'

export function assertHierarchicalColor(x:unknown) : asserts x is HierarchicalColor {

	const ranks			: unknown[]
						= ['primary', 'secondary', 'tertiary', 'quaternary', 'quinary', 'senary']

	assert(typeof x === 'string', 	'assertHierarchicalColor: x must be a string')
	assert(ranks.includes(x), 		`assertHierarchicalColor: x must be a one of ${ranks.join(', ')}`)

}

export function assertCategoricalColor(x:unknown) : asserts x is HierarchicalColor {

	const categories	: unknown[]
						= ['create', 'share', 'receive', 'analyze', 'misc']

	assert(typeof x === 'string', 	'assertCategoricalColor: x must be a string')
	assert(categories.includes(x), 	`assertCategoricalColor: x must be a one of ${categories.join(', ')}`)

}

export function isHierarchicalColor(x: unknown): x is HierarchicalColor {
	return isErrorFree( () => assertHierarchicalColor(x) )
}

export function isCategoricalColor(x: unknown): x is CategoricalColor {
	return isErrorFree( () => assertCategoricalColor(x) )
}
