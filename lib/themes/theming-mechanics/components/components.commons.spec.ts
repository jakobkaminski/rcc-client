import	{ Type } from '@angular/core'


export interface ComponentUnitTests {
	baseComponentClass	: unknown,
	unitTests			: (componentClass: Type<unknown> ) => void
}
