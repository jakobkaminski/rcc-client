import	{	Type 							}	from '@angular/core'
import	{	RccSegmentBaseComponent			}	from './segment-base.component'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccSegmentTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccRadioSelectBaseComponent)', () => {
		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccSegmentBaseComponent)
	})
}

export const RccSegmentTests: ComponentUnitTests = {
	baseComponentClass: RccSegmentBaseComponent,
	unitTests:			rccSegmentTests,
}
