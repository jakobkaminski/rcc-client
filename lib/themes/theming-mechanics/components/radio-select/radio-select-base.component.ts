import { Component, Input } from '@angular/core'
import { ControlValueAccessor, ReactiveFormsModule } from '@angular/forms'
import { Required } from '@rcc/common/decorators'

export interface RccSelectOption<T> {
  value: T,
  label?: string,
  translations?: Record<string, string>
}

@Component({
	selector	: 'rcc-radio-select',
	template	: '',
	imports		: [ReactiveFormsModule],
	standalone	: true
})
export class RccRadioSelectBaseComponent<T> implements ControlValueAccessor {
	protected value: T
	public writeValue(value: T): void {
		this.value = value
	}

	protected onChange: (value: T) => void = () => undefined
	public registerOnChange(fn: (value: T) => void): void {
		this.onChange = fn
	}

	protected onTouched: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	protected isDisabled: boolean
	public setDisabledState(isDisabled: boolean): void {
		this.isDisabled = isDisabled
	}

	@Input()
	public options: RccSelectOption<T>[]

	@Input() @Required
	public groupLabel: string

	@Input()
	public describedBy: string
}
