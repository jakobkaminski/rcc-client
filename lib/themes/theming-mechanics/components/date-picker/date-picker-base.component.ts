import { Component, Input } from '@angular/core'
import { ControlValueAccessor } from '@angular/forms'
import { DayLabel } from './date-picker.commons'

/**
 * Base component for the date picker. See bengal themes {@link RccDatePickerComponent}
 * for more details on the properties.
 */
@Component({
    selector: '',
    template: ''
})
export abstract class RccDatePickerBaseComponent implements ControlValueAccessor {

    @Input()
    public format?: string = undefined
    @Input()
    public placeholder: string = '---'
    @Input()
    public highlightInterval: number = 0
    @Input()
    public earliest: string|Date|undefined
    @Input()
    public latest: string|Date|undefined
    @Input()
    public invalidDays: (string|Date)[] = []
    @Input()
    public labels: DayLabel[] = []

    protected onChange: (date: Date|string) => void
    protected onTouched: () => void
    protected isDisabled: boolean = false

    public abstract writeValue(value: Date|string): void

    public registerOnChange(fn: (date: Date|string) => void): void {
        this.onChange = fn
    }
    public registerOnTouched(fn: () => void): void {
        this.onTouched = fn
    }
    public setDisabledState(isDisabled: boolean): void {
       this.isDisabled = isDisabled
    }

}
