import	{	Type 								}	from '@angular/core'
import  { 	RccDatePickerBaseComponent 	        } 	from './date-picker-base.component'
import 	{	ComponentUnitTests					}	from '../components.commons.spec'


// All Units test for RccDatePickerComponentTests:
function rccDatePickerTests(componentClass: Type<unknown>) : void {


	it('extends the base component class (RccDaysInWeekSelectorBaseComponent)', () => {

		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccDatePickerBaseComponent)
	})

}



export const RccDatePickerComponentTests : ComponentUnitTests = {

	baseComponentClass: 	RccDatePickerBaseComponent,
	unitTests:				rccDatePickerTests

}
