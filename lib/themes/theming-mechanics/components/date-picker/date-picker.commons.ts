/**
 * Used to apply labels to days in {@link RccDatePickerComponent}.
 *
 * @property {string | Date} date the date of the day that will be labeled
 * @property {content} string the actual label
 * @property {boolean | undefined} highlight wether the labeled day should be
 *  highlighted.
 */
export interface DayLabel {
    date: string | Date;
    content: string;
    highlight?: boolean;
}

export const throwNoFormatStringError: () => void = () => {
    throw Error('RccDatePickerComponent: When string representations of dates are used a format string must be set.')
}
