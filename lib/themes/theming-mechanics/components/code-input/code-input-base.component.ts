import { Component, Input } from '@angular/core'
import { ControlValueAccessor } from '@angular/forms'

type CodeInputMode = 'number'

@Component({
	selector: 'rcc-code-input',
	template: ''
})
export class RccCodeInputBaseComponent implements ControlValueAccessor {
	protected value: string
	public writeValue(value: string): void {
		this.value = value
	}

	protected onChange: (value: string) => void = () => undefined
	public registerOnChange(fn: (value: string) => void): void {
		this.onChange = fn
	}

	protected onTouched: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	@Input()
	public mode: CodeInputMode = 'number'

	@Input()
	public length: number

	@Input()
	public label: string
}
