import {
	Directive,
	Input,
	OnChanges,
	SimpleChanges
} 					from '@angular/core'
import {
    RccEditTextBaseComponent
}                   from '../edit-text/edit-text-base.component'

@Directive()
export abstract class RccEditTextBaseLabelDirective<T extends RccEditTextBaseComponent> implements OnChanges {
    @Input() public editLabel: string = ''
    @Input() public doneLabel: string = ''

    public constructor(private readonly host: T) {}

    public ngOnChanges(changes: SimpleChanges): void {
        const editLabel: string = changes['editLabel']?.currentValue as string
        if(
            typeof editLabel === 'string'
            && 'editButtonLabel' in this.host
            && typeof this.host.editButtonLabel === 'string'
        )
            this.host.editButtonLabel = editLabel
    
		const doneLabel: string = changes['doneLabel']?.currentValue as string
        if(
            typeof doneLabel === 'string'
            && 'doneButtonLabel' in this.host
            && typeof this.host.doneButtonLabel === 'string'
        )
            this.host.doneButtonLabel = doneLabel
    }
}
