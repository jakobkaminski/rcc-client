import { Component, Input } from '@angular/core'
import { ControlValueAccessor } from '@angular/forms'

@Component({
	selector: 'rcc-text-input',
	template: ''
})
export class RccTextInputBaseComponent implements ControlValueAccessor {
	protected value: string
	public writeValue(value: string): void {
		this.value = value
	}

	protected onChange: (value: string) => void = () => undefined
	public registerOnChange(fn: (value: string) => void): void {
		this.onChange = fn
	}

	protected onTouched: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	protected isDisabled: boolean = false
	public setDisabledState(isDisabled: boolean): void {
		this.isDisabled = isDisabled
	}

	@Input()
	public label: string
}
