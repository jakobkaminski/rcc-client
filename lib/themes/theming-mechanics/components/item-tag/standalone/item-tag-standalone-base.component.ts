import { Component } from '@angular/core'
import { RccItemTagComponent } from '../item-tag.component'


@Component({
	selector: 		'rcc-standalone-item-tag',
	template: 	    '',
	styleUrls:		[],
})
export class RccStandaloneItemTagBaseComponent extends RccItemTagComponent {}
