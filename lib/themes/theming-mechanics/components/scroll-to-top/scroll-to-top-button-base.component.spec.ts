import	{	Type					}	from '@angular/core'
import	{	RccScrollToTopButtonBaseComponent	}	from './scroll-to-top-button-base.component'
import	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccScrollToTopTests(componentClass: Type<unknown>): void {
it('extends the base component class (RccScrollToTopButtonBaseComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccScrollToTopButtonBaseComponent)
	})
}

export const RccScrollToTopButtonBaseComponentTests: ComponentUnitTests = {
	baseComponentClass	: RccScrollToTopButtonBaseComponent,
	unitTests			: rccScrollToTopTests,
}
