import	{
			Component,
			ElementRef
		}								from	'@angular/core'
import	{	HandlerAction	}			from	'@rcc/common/actions'


@Component({
	selector:	'rcc-scroll-to-top-button',
	template:	'',
})

export class RccScrollToTopButtonBaseComponent {

	public scrollUpAction : HandlerAction = {
		label:		'THEME.SCROLL',
		icon:		'up',
		handler:	 () => this.scrollToTop()
	}

	public scrollElement : HTMLElement

	public constructor(private elementRef: ElementRef) {}

	public scrollToTop(): void {

		const scrollToTopButtonElement	: HTMLElement
										= this.elementRef.nativeElement as HTMLElement

		if (!this.scrollElement)
			this.scrollElement 	= scrollToTopButtonElement.closest('rcc-full-page')
									? 	scrollToTopButtonElement.closest('rcc-full-page')
									: 	scrollToTopButtonElement.closest('rcc-reduced-full-page')

		this.scrollElement.scrollTo({ top: 0, behavior: 'smooth' })

	}

}
