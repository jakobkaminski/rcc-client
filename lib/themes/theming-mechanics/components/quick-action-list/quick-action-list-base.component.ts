import	{
			Component,
			Input,
			ChangeDetectorRef,
			SimpleChanges,
			OnChanges,
			OnDestroy,
			Output,
			EventEmitter
		}								from '@angular/core'
import	{	sortByKeyFn, 		}		from '@rcc/core'

import	{
			Subscription
		}								from 'rxjs'
import	{
			Action,
			ActionService,
			hasNotification,
		}								from '@rcc/common/actions'
import	{
			ColorVariants,
			RccColorService,
			HierarchicalColor,
			CategoricalColor
		}								from '@rcc/themes/theming-mechanics/colors'

@Component({
	selector:			'rcc-quick-action-list',
	template:			'',
})

export abstract class RccQuickActionListBaseComponent implements OnChanges, OnDestroy {


	@Input()
	public editable					: boolean = true

	@Output('editClick')
	public editClickEmitter			: EventEmitter<void> = new EventEmitter<void>()

	public get editIconRequired()	: boolean { return this.editable && this.editClickEmitter.observed }

	@Input()
	public actions					: Action[]

	@Input()
	public action					: Action

	@Input()
	public label					: string

	@Input()
	public color 					: HierarchicalColor | CategoricalColor = 'primary'

	public colorVariants			: ColorVariants	| null	= null

	public notificationValue 		: boolean | number 		= false

	protected notificationSub		: Subscription | null	= null

	public constructor(
		protected actionService		: ActionService,
		protected changeDetectorRef	: ChangeDetectorRef,
		protected rccColorService	: RccColorService
	){
		this.onColorChange()

	}

	public get activeActions(): Action[] {
		return 	this.actions
				.filter(
					(action: Action) => typeof action.position === 'function'
										?	['number', 'undefined'].includes(typeof action.position() )
										:	['number', 'undefined'].includes(typeof action.position )
				)
				.sort( sortByKeyFn('position') )
	}

	protected onColorChange() : void {
		this.colorVariants = this.rccColorService.getColorVariants(this.color || this.action.category)
	}

	protected onActionChange() : void {

		this.notificationValue 	= null

		if(this.notificationSub) 			this.notificationSub.unsubscribe()

		if(!this.action) 					return undefined
		if(!hasNotification(this.action)) 	return undefined

		this.notificationSub 	= this.action.notification.subscribe( value => this.notificationValue = value)

	}

	public ngOnChanges(changes: SimpleChanges): void {

		if(changes.color) this.onColorChange()

		if(changes.action){
			this.onActionChange()
			this.onColorChange()
		}
	}

	public ngOnDestroy() : void {
		this.notificationSub?.unsubscribe()
	}


}
