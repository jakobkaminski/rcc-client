import	{	Type 								}	from '@angular/core'
import  { 	RccDaysInWeekSelectorBaseComponent 	} 	from './days-in-week-selector-base.component'
import 	{	ComponentUnitTests					}	from '../components.commons.spec'


// All Units test for RccDaysInWeekSelectorComponentTest:
function rccDaysInWeekSelectorTests(componentClass: Type<unknown>) : void {


	it('extends the base component class (RccDaysInWeekSelectorBaseComponent)', () => {

		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccDaysInWeekSelectorBaseComponent)
	})

}



export const RccDaysInWeekSelectorComponentTest : ComponentUnitTests = {

	baseComponentClass: 	RccDaysInWeekSelectorBaseComponent,
	unitTests:				rccDaysInWeekSelectorTests

}
