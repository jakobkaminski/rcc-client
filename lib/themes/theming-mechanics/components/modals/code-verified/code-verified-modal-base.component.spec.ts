import	{	Component, Type						}	from '@angular/core'
import	{	RccCodeVerifedModalBaseComponent	}	from './code-verified-modal-base.component'
import	{	ComponentUnitTests					}	from '../../components.commons.spec'

function rccCodeVerifiedModalTests(componentClass: Type<Component>): void {
	it('extends the base component class (RccCodeVerifiedModalBaseComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccCodeVerifedModalBaseComponent)
	})
}

export const RccCodeVerifiedModalTests: ComponentUnitTests = {
	baseComponentClass	: RccCodeVerifedModalBaseComponent,
	unitTests			: rccCodeVerifiedModalTests,
}
