import	{	Type 							}	from '@angular/core'
import	{	RccToggleBaseComponent			}	from '@rcc/themes'
import 	{	ComponentUnitTests				}	from '../components.commons.spec'


// All Units test for RccToggleBaseComponent:
function rccToggleTests(componentClass: Type<unknown>) : void {


	it('extends the base component class (RccToggleBaseComponent)', () => {

		const proto : unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccToggleBaseComponent)
	})

}



export const RccToggleBaseComponentTests : ComponentUnitTests = {

	baseComponentClass: 	RccToggleBaseComponent,
	unitTests:				rccToggleTests

}
