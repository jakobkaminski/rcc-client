import	{
			Component,
			ChangeDetectorRef,
			Input,
		}										from '@angular/core'
import	{	ControlValueAccessor			}	from '@angular/forms'

import	{
			Action,
			ActionService,
		}										from '@rcc/common/actions'

import	{
			ColorVariants,
			RccColorService,
		}										from '../../colors'

@Component({
	selector:		'rcc-toggle',
	template:		'',
	standalone:		true,
})
export class RccToggleBaseComponent implements ControlValueAccessor {

	public onToggleChange(): void {
		this.checked = !this.checked
		this.onChange(this.checked)
		this.onTouched()
	}

	@Input()
	public usage			: string

	@Input()
	public action			: Action | null

	@Input()
	public checked			: boolean = false

	public isButtonEnabled	: boolean = true

	public colorVariants	: ColorVariants	| null	= null

	public constructor(
		protected actionService		: ActionService,
		protected changeDetectorRef	: ChangeDetectorRef,
		protected rccColorService	: RccColorService
	){
	}

	public writeValue(value: boolean): void {
		this.checked = value
	}

	protected onChange: (value: boolean) => void = () => undefined
	public registerOnChange(fn: (value: boolean) => void): void {
		this.onChange = fn
	}

	protected onTouched: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}
}
