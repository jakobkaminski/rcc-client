import	{	Component, Type					}	from '@angular/core'
import	{	RccMainMenuBodyBaseComponent	}	from '@rcc/themes'
import	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccMainMenuBodyTests(componentClass: Type<Component>): void {
	it('extends the base component class (RccMainMenuBodyBaseComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccMainMenuBodyBaseComponent)
	})
}

export const RccMainMenuBodyComponentTests: ComponentUnitTests = {
	baseComponentClass	: RccMainMenuBodyBaseComponent,
	unitTests			: rccMainMenuBodyTests
}
