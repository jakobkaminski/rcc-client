import	{	Component, Type					}	from '@angular/core'
import	{	RccItemListBaseComponent		}	from './item-list-base.component'
import	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccItemListTests(componentClass: Type<Component>): void {
it('extends the base component class (RccItemListComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccItemListBaseComponent)
	})
}

export const RccItemListComponentTests: ComponentUnitTests = {
	baseComponentClass	: RccItemListBaseComponent,
	unitTests			: rccItemListTests,
}
