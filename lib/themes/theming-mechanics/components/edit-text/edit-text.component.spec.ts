import	{	Type						}	from '@angular/core'
import	{	RccEditTextBaseComponent	}	from './edit-text-base.component'
import 	{	ComponentUnitTests			}	from '../components.commons.spec'

function rccEditTextTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccEditTextBaseComponent)', () => {
		const proto: unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccEditTextBaseComponent)
	})
}

export const RccEditTextComponentTests: ComponentUnitTests = {
	baseComponentClass:	RccEditTextBaseComponent,
	unitTests:			rccEditTextTests,
}
