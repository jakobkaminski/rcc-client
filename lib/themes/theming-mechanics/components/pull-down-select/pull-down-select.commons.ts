export interface Option<T> {
	value: T
	label?: string
	translations?: Record<string, string>
}

/**
 * This determines when the onChange method is called:
 * 'type'   :   onChange is called whenever the input fields value changes
 *              or an option is selected
 * 'select' :   onChange is only called when an option is selected
 */
export type OnChangeOnOption = 'type' | 'select';

/**
 * A filter function is used to filter options for the pull down select. It takes
 * the value user entered in the input and an array of options, then returns
 * filtered options. See below for examples.
 * @param {string} filterFor the string that is used as the search term.
 * @param {Option<T>[]} options the options to filter
 * @returns {Option<T>[]} the filtered options
 */
export type FilterFn<T> = (filterFor: string, options: Option<T>[]) => Option<T>[]

/**
 * A compare function compares two options and should return true if they are the
 * same. This will be relatively simple in most cases (when option values are primitives),
 * but might need more involved comparison when values are objects.
 * @param {Option<T>} a the first option to compare
 * @param {Option<T>} b the second option to compare
 * @returns {boolean} true when the options are the same
 */
export type CompareFn<T> = (a: Option<T>, b: Option<T>) => boolean

// default functions:

/**
 * This is a simple filter function that just looks for the string in the label of the options
 */
export function labelFilterFn<T>(filterFor: string, options: Option<T>[]): Option<T>[] {
    return [...options].filter(
        option => option.label
                    .toLocaleLowerCase()
                    .indexOf(filterFor.toLocaleLowerCase()) !== -1
    )
}

/**
 * This is a simple compare function that compares options by value. Please keep in mind that this
 * only works for primitive types like strings, numbers, booleans and so on, but will most likely
 * fail when comparing objects as strict identity comparison is used. Please provide your own
 * compare function for those cases. This also might produce false positives if values are not unique.
 */
export function valueCompareFn<T>(a: Option<T>, b: Option<T>): boolean {
    return a.value === b.value
}
