import	{	Component, Type					}	from '@angular/core'
import	{	RccPullDownSelectBaseComponent	}	from '@rcc/themes'
import	{	ComponentUnitTests				}	from '../components.commons.spec'

function rccPullDownSelectTests(componentClass: Type<Component>): void {
	it('extends the base component class (RccPullDownSelectComponent)', () => {
		const prototype: unknown = componentClass.prototype as unknown

		expect(prototype).toBeInstanceOf(RccPullDownSelectBaseComponent)
	})
}

export const RccPullDownSelectComponentTests: ComponentUnitTests = {
	baseComponentClass	: RccPullDownSelectBaseComponent,
	unitTests			: rccPullDownSelectTests,
}
