import	{	Type						}	from '@angular/core'
import	{	RccTimeSelectBaseComponent	}	from './time-select-base.component'
import 	{	ComponentUnitTests			}	from '../components.commons.spec'

function rccTimeSelectTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccTimeSelectBaseComponent', () => {
		const proto: unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccTimeSelectBaseComponent)
	})
}

export const RccTimeSelectComponentTests: ComponentUnitTests = {
	baseComponentClass:	RccTimeSelectBaseComponent,
	unitTests:			rccTimeSelectTests,
}
