import { Component, Input } from '@angular/core'
import { ControlValueAccessor } from '@angular/forms'

@Component({
	template: '',
})
export abstract class RccTimeSelectBaseComponent implements ControlValueAccessor {
	@Input()
	public labelText: string = ''

	protected disabled: boolean = false

	protected value: string = '00:00'

	public writeValue(value: string): void {
		this.value = value
	}

	public registerOnChange(fn: (event: string) => void): void {
		this.onChange = fn
	}

	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	public setDisabledState(isDisabled: boolean): void {
		this.disabled = isDisabled
	}

	protected onChange: (value: string) => void

	protected onTouched: () => void
}
