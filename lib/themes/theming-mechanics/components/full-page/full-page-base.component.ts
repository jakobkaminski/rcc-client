import	{
			Component,
			ViewEncapsulation,
			Input
		}								from '@angular/core'

import	{
			HierarchicalColor,
			CategoricalColor
		}								from '../../colors'

/**
 * This component represents the structure of a full page.
 * It makes use of content projection; slots you can use are:
 *
 * * headingContent
 * * mainContent
 * * footerContent
 *
 * A full page can have a banner with markdown content.
 */
@Component({
	selector:	'rcc-full-page',
	template:	'',
	encapsulation:	ViewEncapsulation.None
})
export class RccFullPageBaseComponent {

	/**
	 * Markdown to be rendered on the full page as a banner.
	 */
	@Input()
	public bannerMarkdown 	: string

	/**
	 * Background color of the banner.
	 */
	@Input()
	public bannerColor		: HierarchicalColor | CategoricalColor


}
