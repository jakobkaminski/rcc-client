import	{
			Component,
			Input,
		}								from '@angular/core'
import	{
			ControlValueAccessor,
			FormControl,
		}								from '@angular/forms'
import	{	QueryControl			}	from '@rcc/features'

@Component({
	selector: 'rcc-textarea',
	template: '',
})
export abstract class RccTextareaBaseComponent implements ControlValueAccessor {
	protected value: string
	public writeValue(value: string): void {
		this.value = value
		this.formControl.setValue(value)
	}

	protected onChange: (value: string) => void
	public registerOnChange(fn: (value: string) => void): void {
		this.onChange = fn
	}

	protected onTouched: () => void
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	protected isDisabled: boolean
	public setDisabledState(isDisabled: boolean): void {
		this.isDisabled = isDisabled

		if( isDisabled) this.formControl.disable()
		if(!isDisabled) this.formControl.enable()
	}

	@Input()
	public inputLabel: string

	@Input()
	public queryControl: QueryControl

	protected formControl: FormControl<string> = new FormControl<string>('')

	protected abstract focusOnInput(): void
}
