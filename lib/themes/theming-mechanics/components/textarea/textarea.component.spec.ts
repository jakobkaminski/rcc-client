import	{	Type						}	from '@angular/core'
import	{	RccTextareaBaseComponent	}	from './textarea-base.component'
import 	{	ComponentUnitTests			}	from '../components.commons.spec'

function rccTextareaTests(componentClass: Type<unknown>): void {
	it('extends the base component class (RccTextareaBaseComponent)', () => {
		const proto: unknown = componentClass.prototype as unknown

		expect(proto).toBeInstanceOf(RccTextareaBaseComponent)
	})
}

export const RccTextareaComponentTests: ComponentUnitTests = {
	baseComponentClass:	RccTextareaBaseComponent,
	unitTests:			rccTextareaTests,
}
