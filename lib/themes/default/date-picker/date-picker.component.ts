import  {
            Component,
            OnDestroy,
            OnInit,
            forwardRef
        }                               from '@angular/core'
import  {
            RccDatePickerBaseComponent
        }                               from '../..'
import  {
            ControlValueAccessor,
            FormControl,
            NG_VALUE_ACCESSOR,
            ReactiveFormsModule
        }                               from '@angular/forms'
import  {
            DefaultThemeCommonModule
        }                               from '../default-theme-common.module'
import  {
            format,
            fromISO8601CalendarDate,
            parse,
            toISO8601CalendarDate
        }                               from '@rcc/core'
import  {
            throwNoFormatStringError
        }                               from '@rcc/themes/theming-mechanics/components/date-picker/date-picker.commons'
import  {
            Subject,
            map,
            takeUntil
        }                               from 'rxjs'

@Component({
    selector: 'rcc-date-picker',
    templateUrl: './date-picker.component.html',
    standalone: true,
    imports: [DefaultThemeCommonModule, ReactiveFormsModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccDatePickerComponent),
            multi: true
        }
    ]
})
export class RccDatePickerComponent extends RccDatePickerBaseComponent implements ControlValueAccessor, OnDestroy, OnInit {

    protected dateControl   : FormControl<string|null>  = new FormControl<string|null>(null)

    private unsubscribe$    : Subject<void>             = new Subject<void>()

    public constructor() {
        super()
    }

    public ngOnInit(): void {
        this.dateControl
            .valueChanges
            .pipe(
                map((val:string|null) => val !== null
                                            ? fromISO8601CalendarDate(val)
                                            : null
                ),
                map((val: Date|null) => typeof this.format === 'string' && val !== null
                                            ? format(val, this.format)
                                            : val
                ),
                takeUntil(this.unsubscribe$)
            )
            .subscribe((val: Date|string|null) => {
                if(typeof this.onChange === 'function')
                    this.onChange(val)
            })
    }

    public ngOnDestroy(): void {
        this.unsubscribe$.next()
        this.unsubscribe$.complete()
    }

    public writeValue(value: string | Date | null): void {
        if(typeof value === 'string')
            if(typeof this.format === 'string')
                value = parse(value, this.format)
            else
                throwNoFormatStringError()
        
        if(value instanceof Date)
            value = toISO8601CalendarDate(value)

        this.dateControl.setValue(value)
    }

    
}
