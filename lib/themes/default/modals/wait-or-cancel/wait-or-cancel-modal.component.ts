import { Component } from '@angular/core'
import { RccWaitOrCancelModalBaseComponent } from '@rcc/themes/theming-mechanics'
import { DefaultThemeCommonModule } from '../../default-theme-common.module'

@Component({
	templateUrl	: './wait-or-cancel-modal.component.html',
	styleUrls	: ['./wait-or-cancel-modal.component.scss'],
	standalone	: true,
	imports		: [DefaultThemeCommonModule],
	selector	: 'rcc-wait-or-cancel-modal',
})
export class RccWaitOrCancelModalComponent<T> extends RccWaitOrCancelModalBaseComponent<T> {
	
}
