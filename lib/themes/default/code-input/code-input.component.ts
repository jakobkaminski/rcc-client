import	{
			Component,
			forwardRef,
		}									from '@angular/core'
import	{	RccCodeInputBaseComponent	}	from '../../theming-mechanics'
import	{	DefaultThemeCommonModule	}	from '../default-theme-common.module'
import	{	NG_VALUE_ACCESSOR			}	from '@angular/forms'

@Component({
	selector: 'rcc-code-input',
	templateUrl: './code-input.component.html',
	standalone: true,
	imports: [DefaultThemeCommonModule],
	providers:	[
			{
				provide: NG_VALUE_ACCESSOR,
				useExisting: forwardRef(() => RccCodeInputComponent),
				multi: true
			}
		]
})
export class RccCodeInputComponent extends RccCodeInputBaseComponent {
	protected onInput(event: Event): void {
		const input: HTMLInputElement = event.target as HTMLInputElement
		this.onChange(input.value)
	}
}
