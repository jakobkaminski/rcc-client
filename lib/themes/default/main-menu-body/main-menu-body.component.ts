import	{
			Component,
			Inject,
			Optional,
			ViewChild
		}										from '@angular/core'
import	{ 	RouterModule 					} 	from '@angular/router'
import	{ 	IonMenu 						} 	from '@ionic/angular'
import	{
			MainMenuEntry,
			MAIN_MENU_CONFIG,
			MAIN_MENU_ENTRIES,
			MAIN_MENU_ENTRIES_META
		}										from '@rcc/common/main-menu'
import	{	ActionService 					} 	from '@rcc/common/actions'
import	{ 	RccMainMenuBodyBaseComponent 	} 	from '@rcc/themes/theming-mechanics'
import	{ 	DefaultThemeCommonModule 		} 	from '../default-theme-common.module'

@Component({
	selector:		'rcc-main-menu-body',
	templateUrl:	'./main-menu-body.component.html',
	standalone:		true,
	imports:		[
						DefaultThemeCommonModule,
						RouterModule
					]
})
export class RccMainMenuBodyComponent extends RccMainMenuBodyBaseComponent {

	@ViewChild(IonMenu) private menu: IonMenu

	public constructor(
		@Optional() @Inject(MAIN_MENU_ENTRIES)
		public entries			: MainMenuEntry[],

		@Optional() @Inject(MAIN_MENU_ENTRIES_META)
		public metaEntries		: MainMenuEntry[],

		@Optional() @Inject(MAIN_MENU_CONFIG)
		public config	: Record<string, unknown>,

		actionService						: ActionService,
	) {
		super(entries, metaEntries, config, actionService)
	}
}
