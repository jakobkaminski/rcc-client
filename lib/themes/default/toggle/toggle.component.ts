import	{
			Component
		}										from '@angular/core'

import	{	CommonModule					}	from '@angular/common'

import	{	TranslationsModule				}	from '@rcc/common/translations'
import	{	UiComponentsModule				}	from '@rcc/common/ui-components'

import	{	RccToggleBaseComponent			}	from '../../theming-mechanics'

@Component({
	selector:		'rcc-toggle',
	templateUrl:	'./toggle.component.html',
	styleUrls:		['./toggle.component.scss'],
	standalone:		true,
	imports:[
		CommonModule,
		UiComponentsModule,
		TranslationsModule,
	]
})

export class RccToggleComponent extends RccToggleBaseComponent {}
