import 	{
			NgModule,
			Type
		}									from '@angular/core'
import	{
			RccActionButtonComponent
		}									from './action-button/action-button.component'
import	{
			RccButtonComponent
		}									from './button/button.component'
import	{

			RccFullPageComponent
		}									from './full-page/full-page.component'
import	{
			RccHomePageGreetingComponent
		}									from './home-page-greeting/home-page-greeting.component'

import	{
			RccMainMenuBodyComponent
		}								from './main-menu-body/main-menu-body.component'
import	{
			RccRadioSelectComponent
		}								from './radio-select/radio-select.component'
import	{
			RccTextareaComponent
		}								from './textarea/textarea.component'
import	{
			RccPullDownSelectComponent
		}								from './pull-down-select/pull-down-select.component'
import 	{
			RccDaysInWeekSelectorComponent
		} 								from './days-in-week-selector/days-in-week-selector.component'
import	{
			RccQuickActionListComponent
		}								from './quick-action-list/quick-action-list.component'
import	{
			RccToggleComponent
		}									from './toggle/toggle.component'
import	{	RccTimeSelectComponent		}	from './time-select/time-select.component'
import 	{
			RccSubmitButtonComponent

		}									from './submit-button/submit-button.component'
import	{	RccEditTextComponent		}	from './edit-text/edit-text.component'
import	{	RccEditTextLabelDirective	}	from './edit-text-label/edit-text-label.directive'
import 	{ 	RccDatePickerComponent		}	from './date-picker/date-picker.component'
import	{	RccSegmentComponent			}	from './segment/segment.component'
import	{	RccCodeInputComponent		}	from './code-input/code-input.component'
import	{	RccTextInputComponent		}	from './text-input/text-input.component'
import	{	RccScrollToTopButtonComponent	}	from './scroll-to-top-button/scroll-to-top-button.component'

// imports from bengal so that default still compiles
import	{
			RccStandaloneItemTagComponent,
			RccListItemTagComponent,
		}								 	from '../bengal/item-tag'
import	{	RccReducedFullPageComponent	}	from '../bengal/reduced-full-page/reduced-full-page.component'
import	{	RccItemListComponent		}	from '../bengal/item-list/item-list.component'




const components : Type<unknown>[] = [
	RccActionButtonComponent,
	RccButtonComponent,
	RccCodeInputComponent,
	RccDatePickerComponent,
	RccDaysInWeekSelectorComponent,
	RccEditTextComponent,
	RccEditTextLabelDirective,
	RccFullPageComponent,
	RccHomePageGreetingComponent,
	RccItemListComponent,
	RccListItemTagComponent,
	RccMainMenuBodyComponent,
	RccPullDownSelectComponent,
	RccQuickActionListComponent,
	RccRadioSelectComponent,
	RccReducedFullPageComponent,
	RccSegmentComponent,
	RccSubmitButtonComponent,
	RccStandaloneItemTagComponent,
	RccTextareaComponent,
	RccTextInputComponent,
	RccTimeSelectComponent,
	RccToggleComponent,
	RccScrollToTopButtonComponent
]

@NgModule({
	imports: [
		...components,
	],
	exports:[
		...components
	]
})
export class RccThemeModule {}
