import { Component, ElementRef, forwardRef, ViewChild } from '@angular/core'
import { NG_VALUE_ACCESSOR } from '@angular/forms'
import { RccTimeSelectBaseComponent } from '../..'

@Component({
	selector	: 'rcc-time-select',
	templateUrl	: './time-select.component.html',
	standalone	: true,
	providers	: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccTimeSelectComponent),
            multi: true,
        },
    ],
})
export class RccTimeSelectComponent extends RccTimeSelectBaseComponent {
	private _timeInput: ElementRef<HTMLInputElement>
	@ViewChild('timeInput')
	private set timeInput (value: ElementRef<HTMLInputElement>) {
		this._timeInput = value
		if (value != null)
			this._timeInput.nativeElement.value = this.value
	}
	private get timeInput(): ElementRef<HTMLInputElement> {
		return this._timeInput
	}

	public override writeValue(value: string): void {
		if (this.timeInput != null)
			this.timeInput.nativeElement.value = value
		
		this.value = value
	}

	protected handleChange(event: Event): void {
		const target: HTMLInputElement = event.target as HTMLInputElement
		this.onChange(target.value)
	}
}
