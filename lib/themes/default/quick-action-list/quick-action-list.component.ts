import	{
			Component,
		}								from '@angular/core'
import	{	CommonModule		}		from '@angular/common'
import	{	RccIconComponent	}		from '@rcc/common/src/ui-components/icons'
import	{
			RccCardComponent
		}								from '@rcc/common/src/ui-components/card'
import	{
			RccQuickActionListBaseComponent,
		}								from '@rcc/themes/theming-mechanics'
import	{
			TranslationsModule
		}								from '@rcc/common/translations'

@Component({
	selector:			'rcc-quick-action-list',
	templateUrl:		'./quick-action-list.component.html',
	styleUrls:			['quick-action-list.component.css'],
	standalone:			true,
	imports:			[
								RccIconComponent,
								CommonModule,
								TranslationsModule,
								RccCardComponent
						],
})

export class RccQuickActionListComponent extends RccQuickActionListBaseComponent {}
