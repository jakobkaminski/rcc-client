import {
	Directive,
	Input,
} 					from '@angular/core'
import { RccEditTextBaseLabelDirective } from '../..'
import { RccEditTextComponent } from '../edit-text/edit-text.component'

@Directive({
    selector: 'rcc-edit-text[editLabel], rcc-edit-text[doneLabel]',
	standalone: true
})
export class RccEditTextLabelDirective extends RccEditTextBaseLabelDirective<RccEditTextComponent> {
    @Input() public editLabel: string = ''
    @Input() public doneLabel: string = ''

    public constructor(host: RccEditTextComponent) {
        super(host)
    }
}
