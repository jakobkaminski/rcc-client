import { CommonModule } from '@angular/common'
import { Component, ElementRef, forwardRef, ViewChild, Input } from '@angular/core'
import { NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms'
import { RccIconComponent } from '@rcc/common/src/ui-components/icons'
import { RccEditTextBaseComponent } from '../../theming-mechanics/components/edit-text/edit-text-base.component'

@Component({
	selector:		'rcc-edit-text',
	templateUrl:	'./edit-text.component.html',
	styleUrls:		['./edit-text.component.scss'],
	standalone:		true,
	imports:		[CommonModule, ReactiveFormsModule, RccIconComponent],
	providers	: [
		{       provide: NG_VALUE_ACCESSOR,
				useExisting: forwardRef(() => RccEditTextComponent),
				multi: true
		}
	],
})
export class RccEditTextComponent extends RccEditTextBaseComponent {

	@ViewChild('textArea')
	private textArea: ElementRef<HTMLTextAreaElement>

	private _onChange: (text: string) => void
	public onChange(): void {
		this._onChange(this.textArea.nativeElement.value)
	}
	@Input()
	public readOnly: boolean


	public onTouched: () => void

	public disabled: boolean = false
	public questionTitle: string = ''

	public override writeValue(obj: string): void {
		this.questionTitle = obj
	}
	public override registerOnChange(fn: (text: string) => void): void {
		this._onChange = fn
	}
	public override registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}
	public override setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled
	}

	protected focusOnInput(): void {
		this.textArea.nativeElement.focus()
	}
}
