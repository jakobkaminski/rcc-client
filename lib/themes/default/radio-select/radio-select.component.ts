import	{	Component, forwardRef					}	from '@angular/core'
import	{	NG_VALUE_ACCESSOR, ReactiveFormsModule			}	from '@angular/forms'
import	{	uniqueId					}	from '@rcc/core'
import	{	RccRadioSelectBaseComponent	}	from '../../theming-mechanics/components'
import	{	DefaultThemeCommonModule	}	from '../default-theme-common.module'

@Component({
	templateUrl	: './radio-select.component.html',
	selector	: 'rcc-radio-select',
	standalone	: true,
	imports		: [ReactiveFormsModule, DefaultThemeCommonModule],
	providers	: [
		{       provide: NG_VALUE_ACCESSOR,
				useExisting: forwardRef(() => RccRadioSelectComponent),
				multi: true
		}
	],
})
export class RccRadioSelectComponent<T> extends RccRadioSelectBaseComponent<T> {
	private uniqueIdPart: string = uniqueId('rcc-radio-select')

	protected id(index: number): string {
		return `${this.uniqueIdPart}_${index}`
	}
}
