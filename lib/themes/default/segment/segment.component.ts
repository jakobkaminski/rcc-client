import { Component, forwardRef } from '@angular/core'
import { RccSegmentBaseComponent } from '../../theming-mechanics/components/segment/segment-base.component'
import { DefaultThemeCommonModule } from '../default-theme-common.module'
import { NG_VALUE_ACCESSOR } from '@angular/forms'

@Component({
	templateUrl	: './segment.component.html',
	standalone	: true,
	selector	: 'rcc-segment',
	imports		: [DefaultThemeCommonModule],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => RccSegmentComponent),
			multi: true
		}
	],
})
export class RccSegmentComponent extends RccSegmentBaseComponent {

}
