import { Component, ElementRef, ViewChild, forwardRef } from '@angular/core'
import { NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms'
import { RccTextareaBaseComponent } from '@rcc/themes/theming-mechanics/components/textarea/textarea-base.component'

@Component({
	selector:		'rcc-textarea',
	templateUrl:	'./textarea.component.html',
	styleUrls:		['./textarea.component.scss'],
	standalone:		true,
	imports:		[ReactiveFormsModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccTextareaComponent),
            multi: true
        }
    ],
})
export class RccTextareaComponent extends RccTextareaBaseComponent {
	@ViewChild('textInput')
	private textInput: ElementRef<HTMLTextAreaElement>

	protected focusOnInput(): void {
		this.textInput.nativeElement.focus()
	}

	protected onTextInputChange(event: Event): void {
		const target: HTMLTextAreaElement = event.target as HTMLTextAreaElement

		this.onChange(target.value)
	}
}
