
import	{
			Component,
			Input
		}											from	'@angular/core'
import	{	RccStandaloneItemTagBaseComponent	}	from	'../../../theming-mechanics/components/item-tag/standalone/item-tag-standalone-base.component'
import	{	CommonModule						}	from	'@angular/common'
import	{	RccIconComponent					}	from	'@rcc/common/ui-components/icons'
import	{	RccActionButtonComponent			}	from	'../../action-button/action-button.component'
import	{	RccColorCategoryDirective			}	from	'@rcc/common/ui-components/color-category'
import	{	Action								}	from	'@rcc/common/actions'
import	{	ColorCategory						}	from '@rcc/themes/theming-mechanics'


@Component({
	selector: 		'rcc-standalone-item-tag',
	templateUrl: 	'./item-tag-standalone.component.html',
	styleUrls:		['./item-tag-standalone.component.css'],
	standalone:		true,
	imports:		[
		CommonModule,
		RccIconComponent,
		RccActionButtonComponent,
		RccColorCategoryDirective,
	],
})
/**
 * The RccStandaloneItemTag can render Items
 * If a colorCategory is provided it will be used, but
 * if no colorCategory is provided and the mode is 'simple' the action's category will be used
 * otherwise no colorCategory will be applied
 */
export class RccStandaloneItemTagComponent extends RccStandaloneItemTagBaseComponent {

	protected complexAction: Action = {
		label:		'THEME.ACTIONS.COMPLEX',
		icon:		'next_thin',
		handler:	() => this.onClick(),
	}

	private _colorCategory: ColorCategory
	@Input()
	public set colorCategory(value: ColorCategory) {
		this._colorCategory = value
	}
	public get colorCategory(): ColorCategory {
		if(this._colorCategory)
			return this._colorCategory
		if(this.mode === 'simple')
			return this.actions.at(0)?.category || 'misc'
		return 'misc'
	}
}
