import	{
			Component,
			ElementRef
		}											from	'@angular/core'
import	{
			TranslationsModule,
		}											from	'@rcc/common/translations'
import	{	RccColorCategoryDirective			}	from	'@rcc/common/ui-components/color-category'
import	{	RccIconComponent					}	from	'@rcc/common/src/ui-components/icons'
import	{	RccScrollToTopButtonBaseComponent	}	from	'@rcc/themes/theming-mechanics/components/scroll-to-top/scroll-to-top-button-base.component'
import	{	RccActionButtonComponent			}	from	'../action-button/action-button.component'

@Component({
	selector:		'rcc-scroll-to-top-button',
	templateUrl:	'./scroll-to-top-button.component.html',
	styleUrls:		['./scroll-to-top-button.component.css'],
	standalone:		true,
	imports:		[
						TranslationsModule,
						RccIconComponent,
						RccColorCategoryDirective,
						RccActionButtonComponent
					],
})

export class RccScrollToTopButtonComponent extends RccScrollToTopButtonBaseComponent{

	public constructor(elementRef: ElementRef) {
		super(elementRef)
	}

}
