import { DOCUMENT } from '@angular/common'
import { Component, Inject, forwardRef } from '@angular/core'
import { uniqueId } from '@rcc/core'
import { DefaultThemeCommonModule } from '@rcc/themes/default/default-theme-common.module'
import { RccRadioSelectBaseComponent } from '../..'
import { NG_VALUE_ACCESSOR } from '@angular/forms'

@Component({
	templateUrl	: './radio-select.component.html',
	styleUrls	: ['radio-select.component.scss'],
	selector	: 'rcc-radio-select',
	standalone	: true,
	imports		: [DefaultThemeCommonModule],
	providers	: [
		{       provide: NG_VALUE_ACCESSOR,
				useExisting: forwardRef(() => RccRadioSelectComponent),
				multi: true
		}
	],
})
export class RccRadioSelectComponent<T> extends RccRadioSelectBaseComponent<T> {
	public constructor(
		@Inject(DOCUMENT)
		private readonly document: Document
	) {
		super()
	}

	private uniqueIdPart: string = uniqueId('radio-select')

	protected id(index: number): string {
		return `${this.uniqueIdPart}_${index}`
	}

	protected isTabbable(value: T, index: number): boolean {
		const isAnyValueSelected: boolean = this.options.find((option) => option.value === this.value) != null

		if (!isAnyValueSelected) return index === 0

		return this.isSelected(value)
	}

	protected isSelected(value: T): boolean {
		return this.value === value
	}

	protected handleKeydown(event: KeyboardEvent, index: number): void {
		if (event.key === ' ')
			this.selectOption(this.options[index].value)

		if (event.key === 'ArrowDown' || event.key === 'ArrowRight')
			this.moveToNext(index)

		if (event.key === 'ArrowUp' || event.key === 'ArrowLeft')
			this.moveToPrevious(index)
	}

	protected selectOption(value: T): void {
		this.value = value
		this.onChange(value)
		this.onTouched()
	}

	private moveToNext(index: number): void {
		const shouldRollover: boolean = index + 1 >= this.options.length
		const nextIndex: number = shouldRollover ? 0 : index + 1

		this.moveToIndex(nextIndex)
	}

	private moveToPrevious(index: number): void {
		const shouldRollover: boolean = index === 0
		const nextIndex: number = shouldRollover ? this.options.length - 1 : index - 1

		this.moveToIndex(nextIndex)
	}

	private moveToIndex(nextIndex: number): void {
		this.selectOption(this.options[nextIndex].value)
		const id: string = this.id(nextIndex)
		this.document.getElementById(id)?.focus()
	}
}
