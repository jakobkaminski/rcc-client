import	{	Component						}	from '@angular/core'
import	{	CommonModule,					}	from '@angular/common'
import	{
			TranslationsModule,
		}										from '@rcc/common/translations'
import	{
			RccColorCategoryDirective,
			RccIconComponent,
		}										from '@rcc/common/ui-components'
import	{	RccReducedFullPageBaseComponent	}	from '@rcc/themes'
import	{	RccActionButtonComponent		}	from '../action-button/action-button.component'

@Component({
	selector	:	'rcc-reduced-full-page',
	templateUrl	:	'./reduced-full-page.component.html',
	standalone	:	true,
	styleUrls	:	['./reduced-full-page.component.scss'],
	imports		:	[
						TranslationsModule,
						RccIconComponent,
						RccColorCategoryDirective,
						CommonModule,
						RccActionButtonComponent
					]
})
export class RccReducedFullPageComponent extends RccReducedFullPageBaseComponent {}
