import	{
			Component,
			ElementRef,
			ViewEncapsulation
		}									from '@angular/core'
import	{
			NgIf
		}									from '@angular/common'
import	{
			RouterModule
		}									from '@angular/router'
import	{
			TranslationsModule,
		}									from '@rcc/common/translations'
import	{	MainMenuService				}	from '@rcc/common/main-menu'
import	{
			RccColorCategoryDirective,
			RccIconComponent,
			RccLogoComponent,
			RccMarkdownComponent
		}									from '@rcc/common/ui-components'
import	{	RccFullPageBaseComponent	}	from '@rcc/themes'

@Component({
	selector:		'rcc-full-page',
	templateUrl:	'./full-page.component.html',
	standalone:		true,
	styleUrls:		['./full-page.component.scss'],
	encapsulation:	ViewEncapsulation.None,
	imports	:		[
						RouterModule,
						RccLogoComponent,
						TranslationsModule,
						RccIconComponent,
						RccColorCategoryDirective,
						RccMarkdownComponent,
						NgIf
					]
})
export class RccFullPageComponent extends RccFullPageBaseComponent {
	public constructor (
		private readonly mainMenuService	: MainMenuService,
		public readonly elementRef			: ElementRef<HTMLElement>
	) {
		super()
	}

	protected onMenuClick(): void {
		this.mainMenuService.setIsMenuOpen(true)
	}
}
