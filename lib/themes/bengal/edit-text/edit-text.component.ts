import { CommonModule } from '@angular/common'
import { AfterViewInit, Component, ElementRef, forwardRef, ViewChild, Input } from '@angular/core'
import { NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms'
import { RccIconComponent } from '@rcc/common/src/ui-components/icons'
import { RccEditTextBaseComponent } from '../../theming-mechanics/components/edit-text/edit-text-base.component'

@Component({
	selector:		'rcc-edit-text',
	templateUrl:	'./edit-text.component.html',
	styleUrls:		['./edit-text.component.scss'],
	standalone:		true,
	imports:		[CommonModule, ReactiveFormsModule, RccIconComponent],
	providers	: [
		{       provide: NG_VALUE_ACCESSOR,
				useExisting: forwardRef(() => RccEditTextComponent),
				multi: true
		}
	],
})
export class RccEditTextComponent extends RccEditTextBaseComponent implements AfterViewInit {
	public editButtonLabel: string = ''
	@ViewChild('editButton') private editButton: ElementRef<HTMLButtonElement>
	public doneButtonLabel: string = ''
	@ViewChild('doneButton') private doneButton: ElementRef<HTMLButtonElement>

	@Input()
	public readOnly: boolean

	public ngAfterViewInit(): void {
		this.editButton.nativeElement.name = this.editButtonLabel
		this.doneButton.nativeElement.name = this.doneButtonLabel
	}

	private _questionTitle: string = ''
	protected get questionTitle(): string {
		return this._questionTitle
	}

	private _onChange: (text: string) => void
	public onChange(): void {
		const value: string = this.textArea.nativeElement.value
		this._onChange(value)
		this._questionTitle = value
	}

	public onTouched: () => void

	public writeValue(obj: string): void {
		this._questionTitle = obj
	}

	public registerOnChange(fn: (text: string) => void): void {
		this._onChange = fn
	}

	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	public override setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled
	}

	@ViewChild('textArea')
	private textArea: ElementRef<HTMLTextAreaElement>

	protected edit: boolean = false
	protected disabled: boolean

	protected focusOnInput(): void {
		this.textArea.nativeElement.focus()
	}
	protected editText(): void {
		this.edit = true
		this.startEdit.emit()
		this.focusOnInput()
	}

	protected doneEditing(): void {
		this.edit = false
	}

	protected get renderValue(): string {
		if (this.displayValue == null)
			return this.questionTitle
		if (this.questionTitle === '')
			return this.questionTitle
	
		return this.edit ? this.questionTitle : this.displayValue
	}

	protected handleKeydown(event: KeyboardEvent): void {
		if (event.key === 'Enter' && !event.shiftKey) {
			this.onChange()
			this.onTouched()
		}
	}
}
