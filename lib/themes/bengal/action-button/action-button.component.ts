import	{
			Component,
		}										from '@angular/core'

import	{	CommonModule					}	from '@angular/common'
import	{	RouterLink						}	from '@angular/router'

import	{
			RccActionButtonBaseComponent
		}										from '@rcc/themes'

import	{	TranslationsModule				}	from '@rcc/common/translations'
import	{
			UiComponentsModule,
		}										from '@rcc/common/ui-components'

@Component({
	selector:		'rcc-action-button',
	templateUrl:	'./action-button.component.html',
	styleUrls:		['./action-button.component.css'],
	standalone:		true,
	imports:[
		CommonModule,
		UiComponentsModule,
		TranslationsModule,
		RouterLink,
	],
})
export class RccActionButtonComponent extends RccActionButtonBaseComponent {

	public get disabled(): boolean { return this.action.disabled && this.action.disabled() }

	public get cardBackground(): boolean { return this.showCard && !this.hideCard }

	public get showCard(): boolean { return this.buttonSize === 'full' || this.buttonSize === 'none' }
}
