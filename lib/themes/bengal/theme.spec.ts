import 	* as themeExports 	from './index'
import	{ testComponents }	from '@rcc/themes/spec'


describe( 'Bengal theme components', () => {
	testComponents(themeExports)
})
