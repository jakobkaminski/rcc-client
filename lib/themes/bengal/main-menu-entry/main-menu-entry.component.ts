import	{
			Component,
			EventEmitter,
			Input,
			Output,
		}									from 	'@angular/core'
import	{	RouterModule				}	from	'@angular/router'
import	{	UiComponentsModule			}	from	'@rcc/common/ui-components'
import	{
			Action,
			ActionService,
			PathAction,
			isPathAction
		}									from	'@rcc/common/actions'
import	{	DefaultThemeCommonModule	}	from	'@rcc/themes/default/default-theme-common.module'

@Component({
	selector:		'rcc-main-menu-entry',
	templateUrl:	'./main-menu-entry.component.html',
	styleUrls:		['./main-menu-entry.component.scss'],
	standalone:	 	true,
	imports	: 		[
						DefaultThemeCommonModule,
						RouterModule,
						UiComponentsModule
					]
})
export class RccMainMenuEntryComponent {

	public constructor(private readonly actionService: ActionService) {}

	@Input() public action: Action

	@Output() public entryClicked: EventEmitter<void> = new EventEmitter<void>()

	protected isLink(action: Action): action is PathAction {
		return isPathAction(action)
	}

	public onClick(): void {
		void this.actionService
			.execute(this.action)
			.finally(() => this.entryClicked.emit())
	}
}
