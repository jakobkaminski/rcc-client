import 	{
			DOCUMENT
		} 										from '@angular/common'
import 	{
			AfterViewChecked,
			AfterViewInit,
			Component,
			ElementRef,
			forwardRef,
			Inject,
			OnDestroy,
			ViewChild,
		}						 				from '@angular/core'
import 	{
			FormControl,
			FormsModule,
			NG_VALUE_ACCESSOR,
			ReactiveFormsModule
		} 										from '@angular/forms'
import 	{
			uniqueId
		} 										from '@rcc/core'
import 	{
			DefaultThemeCommonModule
		} 										from '@rcc/themes/default/default-theme-common.module'
import 	{
			RccPullDownSelectBaseComponent,
		} 										from '../../theming-mechanics'
import 	{
			combineLatest,
			Connectable,
			connectable,
			debounceTime,
			delay,
			distinctUntilChanged,
			filter, first, map,
			Observable,
			shareReplay,
			startWith,
			Subject, take,
			takeUntil,
			tap
		} 										from 'rxjs'
import 	{
			Option
		} 										from '@rcc/themes/theming-mechanics/components/pull-down-select/pull-down-select.commons'
import 	{
			RccTranslationService
		} 										from '@rcc/common'

const observerMarginBuffer : number = 20

@Component({
	templateUrl	:	'./pull-down-select.component.html',
	styleUrls	: ['./pull-down-select.component.scss'],
	selector	: 'rcc-pull-down-select',
	standalone	: true,
	imports		: [DefaultThemeCommonModule, FormsModule, ReactiveFormsModule],
	providers:	[
			{
				provide: NG_VALUE_ACCESSOR,
				useExisting: forwardRef(() => RccPullDownSelectComponent),
				multi: true
			}
		]
})
export class RccPullDownSelectComponent<T> extends RccPullDownSelectBaseComponent<T> implements AfterViewChecked, AfterViewInit, OnDestroy {
	@ViewChild('combobox')
	private combobox: ElementRef<HTMLInputElement>

	@ViewChild('listbox')
	private listbox: ElementRef<HTMLDivElement>

	protected expanded			: 	boolean 			= false
	protected showAbove			: 	boolean 			= false
	protected listboxId			: 	string 				= uniqueId('pull-down-select__listbox')
	
	private uniqueIdPart		: 	string 				= uniqueId('pull-down-select')
	private highlightedIndex	: 	number 				= undefined
	private observer			: 	IntersectionObserver
	private unsubscribe$		: 	Subject<void> 		= new Subject<void>()

	// Due to the timeline of the angular life-cycles, we don't have a way of knowing
	// what the height of the popup (and as such what margin the observer will need)
	// at the time it's set. As Such we use this variable to track if the height has
	// changed during the change detection cycle, and if so (and it's a non-zero value,
	// meaning the pop up-content has been initialized), we can create the observer
	private observerMargin		: 	number 				= 0
	
	protected typeaheadControl	: 	FormControl<string> = new FormControl<string>('')

	// #region handle option filtering

	private typeaheadValueChange$	: 	Observable<string>
									=  	this.typeaheadControl
											.valueChanges
											.pipe(
												filter(() => this.allowTextInput),
												startWith(''),
												debounceTime(200),
												distinctUntilChanged(),
												tap((tv) => this.setTypedValue(tv))
											)

	private optionsValueChange$		: 	Connectable<Option<T>[]>
									=	connectable(this._options$
											.pipe(
												tap((options) => {
													if(this.onChangeMode === 'select')
														return

													// when onChangeMode is set to 'type'
													// only string values can be safely
													// supported.
													if(options.find((option) => typeof option.value !== 'string'))
														throw Error(
															'RccPullDownSelectComponent: Only option values of type string \
															 are allowed when onChangeMode is set to \'type\''
														)
												}),
												shareReplay(1)
											))

	protected filteredOptions$ 		: 	Observable<Option<T>[]>
									= 	combineLatest([
											this.typeaheadValueChange$,
											this.optionsValueChange$
										])
										.pipe(
											map(([tv, options]: [string, Option<T>[]]) => 	this.filterFn === null
																							?	options
																							:	this.filterFn(tv, options ?? [])
											),
											shareReplay(1),
											takeUntil(this.unsubscribe$),
										)

	// #endregion

	public constructor(
		@Inject(DOCUMENT)
		private readonly document: Document,
		private readonly rccTranslationService : RccTranslationService
	) {
		super()
	}

	public override setDisabledState(isDisabled: boolean): void {
		this.isDisabled = isDisabled
		if (isDisabled) this.typeaheadControl.disable()
		else this.typeaheadControl.enable()
	}

	// #region ng lifecycle hooks

	public ngAfterViewInit(): void {
		// prevent short flashing of the list box when
		// the component initializes
		this.resizeListbox()

		// update the list box size when number of options changes
		this.filteredOptions$
			.pipe(
				map((options) => options.length),
				distinctUntilChanged(),
				delay(0),
				takeUntil(this.unsubscribe$)
			)
			.subscribe(() => this.resizeListbox())

		// connect optionsValueChange$ so it only starts emiting
		// values after all the inputs have been set´
		this.optionsValueChange$.connect()
	}

	public ngAfterViewChecked(): void {
		// This function is called by every change detection, so we want to do an
		// early return to prevent unnecessary code being executed
		if (this.observerMargin === this.listbox?.nativeElement.clientHeight)
			return

		this.observerMargin = this.listbox?.nativeElement.clientHeight

		const rootMargin : number = this.observerMargin + this.combobox.nativeElement.clientHeight + observerMarginBuffer
		
		this.observer?.disconnect()
		this.observer = new IntersectionObserver(() => {
								this.showAbove = this.shouldShowAbove()
							}, {
								rootMargin: `0px 0px -${rootMargin}px 0px`
							})
		this.observer.observe(this.combobox.nativeElement)
	}

	public ngOnDestroy(): void {
		this.unsubscribe$.next()
		this.unsubscribe$.complete()
	}

	// #endregion

	public writeValue(value: T): void {
		// do not preset when value is 'empty'
		if(value === '' || value === null || typeof value === 'undefined') return
		
		this.optionsValueChange$
			.pipe(
				// setting options via async pipe might
				// result in options being null, which will
				// result in options.find being undefined
				filter((options) => !!options),
				first(),
				delay(0),
				map((options) => {
					const selectedOption : Option<T> = options.find(option => this.compareFn(option, { value }))
					
					if (typeof selectedOption === 'undefined' && typeof value === 'string')
						return { value } as Option<T>

					return selectedOption
				}),
			).subscribe((option) => {
				this.value = option.value
				
				this.typeaheadControl.setValue(
					this.rccTranslationService.translate(option)
				)
			})
	}

	// #region template interpolation

	protected get activeDescendant(): string | undefined {
		return this.expanded ? this.optionId(this.highlightedIndex) : undefined
	}

	protected isHighlighted(index: number): boolean {
		return this.highlightedIndex === index
	}

	protected isSelected(option: Option<T>): boolean {
		return this.compareFn(option, { value: this.value })
	}

	protected optionId(index: number): string {
		return `${this.uniqueIdPart}_${index}`
	}

	// #endregion

	// #region events

	protected blur(): void {
		if(typeof this.onTouched === 'function')
			this.onTouched()

		// in select mode remove input that is not
		// a selected option
		if(this.onChangeMode === 'select')
			if(typeof this.value !== 'undefined')
				this.writeValue(this.value)
			else
				this.typeaheadControl.setValue('')
		
		this.close()
	}

	protected close(): void {
		this.expanded = false
		this.highlightedIndex = undefined

		this.resizeListbox()
	}

	protected open(): void {
		this.expanded = true
		this.showAbove = this.shouldShowAbove()

		if(this.combobox)
			this.combobox.nativeElement.select()

		this.resizeListbox()
	}

	protected selectValue(option: Option<T>): void {
		if(typeof this.onChange === 'function')
			this.onChange(option.value)

		this.value = option.value
		this.typeaheadControl.setValue(
			this.rccTranslationService.translate(option)
		)

		this.close()
	}

	protected handleKeydown(event: KeyboardEvent): void {
		switch(event.key) {
			case 'Enter':
				if(this.expanded)
					event.preventDefault()
				this.filteredOptions$
					.pipe(
						take(1),
						map((options) => {
							const selectedOption : Option<T> = options[this.highlightedIndex ?? -1]

							if(!selectedOption && this.onChangeMode === 'type')
								return this.typeaheadControl.value

							return selectedOption
						}),
						filter((option: string | Option<T>) => !(typeof option === 'undefined'))
					)
					.subscribe((option: string | Option<T>) => {
						if(typeof option === 'string') {
							this.setTypedValue(option)
							this.close()
						} else
							this.selectValue(option)
					})
				break
			case 'ArrowDown':
				event.preventDefault()
				if (this.expanded)
					this.cycleOptions()
				else
					this.open()
				break
			case 'ArrowUp':
				event.preventDefault()
				if (this.expanded)
					this.cycleOptions(-1)
				else
					this.open()
				break
			case 'Escape':
			case 'Tab':
				this.close()
				break
			default:
				if(!this.expanded) this.open()
		}
	}

	// #endregion

	protected shouldShowAbove(): boolean {
		const comboboxPosition	: number	= this.combobox.nativeElement.getBoundingClientRect().y
		const comboboxHeight	: number	= this.combobox.nativeElement.clientHeight
		const popupHeight		: number	= this.listbox.nativeElement.clientHeight
		const pageHeight		: number	= this.document.body.clientHeight

		const enoughSpaceBelow	: boolean
								= comboboxPosition + comboboxHeight + popupHeight + observerMarginBuffer <= pageHeight

		const enoughSpaceAbove	: boolean
								= comboboxPosition > popupHeight

		return !enoughSpaceBelow && enoughSpaceAbove
	}

	private resizeListbox(): void {
		if(this.expanded) {
			this.listbox.nativeElement.style.height = 'auto'
			this.listbox.nativeElement.parentElement.style.height = `${this.listbox.nativeElement.clientHeight}px`
		} else
			this.listbox.nativeElement.style.height = this.listbox.nativeElement.parentElement.style.height = '0px'
	}

	private cycleOptions(n: number = 1): void {
		if(typeof this.highlightedIndex !== 'number') {
			this.highlightedIndex = 0
			return
		}

		this.filteredOptions$
			.pipe(
				take(1),
				map(options => options.length)
			)
			.subscribe((numberOfOptions) => {
				if(numberOfOptions < 1)
					this.highlightedIndex = undefined
				else
					// this wraps negative or to high values around so that highlightedIndex is allways a valid value
					this.highlightedIndex = ((this.highlightedIndex + n) % numberOfOptions + numberOfOptions) % numberOfOptions
			})
	}

	private setTypedValue(tv: string): void {
		if(this.onChangeMode === 'type') {
			this.writeValue(tv as T)
			if(typeof this.onChange === 'function')
				this.onChange(tv as T)
		}
	}
}
