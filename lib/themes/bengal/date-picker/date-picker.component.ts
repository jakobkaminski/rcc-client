import  {
            Component,
            ElementRef,
            OnChanges,
            OnInit,
            SimpleChanges,
            forwardRef
        }                                   from '@angular/core'
import  {
            RccDatePickerBaseComponent
        }                                   from '../..'
import  {
            parse,
            format,
            startOfMonth,
            endOfMonth,
            isSameDay,
            daysAfter,
            daysBefore,
            getDayOfWeek,
            today as getToday,
            isWithinInterval,
            isDayBefore,
            isDayAfter
        }                                   from '@rcc/core'
import  {
            BehaviorSubject,
            distinctUntilChanged
        }                                   from 'rxjs'
import  {
            ControlValueAccessor,
            NG_VALUE_ACCESSOR
        }                                   from '@angular/forms'
import  {
            DefaultThemeCommonModule
        }                                   from '@rcc/themes/default/default-theme-common.module'
import  {
            DayLabel,
            throwNoFormatStringError
        }                                   from '@rcc/themes/theming-mechanics/components/date-picker/date-picker.commons'

interface Day {
    date            : Date;
    formatted       : string;
    isSelected      : boolean;
    inInterval      : boolean;
    firstInInterval : boolean;
    lastInInterval  : boolean;

    isToday         : boolean;
    disabled        : boolean;
    weekday         : number;
    label?          : DayLabel;
}

type DaySelection   = Pick<Day, 'isSelected'|'inInterval'|'firstInInterval'|'lastInInterval'>

type DateInterval   = { start: Date, end: Date }


/**
 * Date picker component with several options to display additional information.
 * Use {@link colorCategory} to set color scheme.
 *
 * @property {string | undefined} format input/output format for the picked date.
 *  For example 'dd.MM.yyyy'. See date-fns documentation for supported format strings.
 *  If no format is set date picker will output Date objects.
 * @property {string} displayFormat format in which the picked date will be
 *  displayed in the collapsed date picker field. Default is 'dd.MM.yyyy'
 * @property {string} placeholder text that is displayed in the collapsed date picker
 *  field when no date has been picked yet.
 * @property {number} highlightInterval set to a number > 1 or < -1 show an interval
 *  as selected. The interval will be of the given length including the selected day
 *  (a value of 7 will display a whole week as selected, starting with the picked
 *  day, a value of -7 will also display a whole week, but ending with the picked
 *  day). This is for display purposes only, the picker will still take/return a single
 *  date as selected.
 * @property {string | Date | undefined} earliest set the earliest possible day to
 *  be picked. Days before that will be disabled, but can still be set as an input
 *  value. Will throw an error when a string value is given but `format` is not set.
 * @property {string | Date | undefined} latest sets the latest possible day to be
 *  picked. See `earliest` for more information.
 * @property {(string|Date)[]} invalidDays use this to define days that should be
 *  disabled (for instance national holidays). If string values are used `format`
 *  must be set or date picker will throw an error.
 * @property {DayLabel[]} labels set this to add labels (for instance answered
 *  questions) to days. See {@link DayLabel} for more detail. Make sure that `format`
 *  is set if string values are used for `DayLabel.date`. Not doing this will result
 *  in an error.
 *
 * @example
 * ```html
 * <rcc-date-picker
 *      colorCategory="create"
 *      [formControl]="dateControl"
 *      format="yyyy-MM-dd"
 *      [highlightInterval]="14"
 *      earliest="2023-05-01"
 *      latest="2023-05-31"
 *      [invalidDays]="[
 *          '2023-05-01',
 *          '2023-05-18',
 *          '2023-05-29'
 *      ]"
 *      [labels]="[
 *          {
 *              date: '2023-05-05',
 *              content: '<3',
 *              highlight: true
 *          }
 *      ]"
 * ></rcc-date-picker>
 * ```
 * This will result in an orange (for hcp) date picker that returns dates in
 * format 'yyyy-MM-dd'. Valid dates are limited to all days of May 2023 excluding
 * german national holidays. When a date is picked an interval of two weeks
 * will be highlighted, starting with the picked date. The 5th of May is labeled
 * with a heart and highlighted for some reason.
 */
@Component({
    selector:       'rcc-date-picker',
    templateUrl:    './date-picker.component.html',
    styleUrls:      ['./date-picker.component.scss'],
    standalone:     true,
    imports:        [DefaultThemeCommonModule],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => RccDatePickerComponent),
            multi: true
        }
    ]
})
export class RccDatePickerComponent extends RccDatePickerBaseComponent implements ControlValueAccessor, OnInit, OnChanges {

    private _value$             : BehaviorSubject<Date|null>
                                = new BehaviorSubject<Date|null>(null)

    protected days              : Day[]     = []
    protected expanded          : boolean   = false
    protected currentDate       : Date      = new Date()
    protected firstDayOffset    : number    = 0

    // #region getters & setters

    protected set value(val: string|Date) {
        if(typeof val === 'string')
            if(typeof this.format === 'string')
                this._value$.next(parse(val, this.format))
            else
                throwNoFormatStringError()
        else
            this._value$.next(val)
    }

    protected get value(): Date {
        return this._value$.getValue()
    }

    protected get sanitizedHighlightInterval(): number {
        // only highlight an interval if the interval is more than one day
        // an interval of one day is just the selected day
        if(this.highlightInterval >= -1 && this.highlightInterval <= 1)
            return 0
        
        // subsctract one from the given value to reflect that the
        // selected day is included in the interval
        if(this.highlightInterval > 0)
            return this.highlightInterval - 1
        // for negative intervals add one day for the same reason
        return this.highlightInterval + 1
    }

    // #endregion

    
    public constructor(private readonly elementRef: ElementRef<HTMLElement>) {
        super()
    }


    // #region ng lifecycle hooks

    public ngOnInit(): void {
        this.subscribeForValueChanges()
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if(
            (changes['format'] ||
            changes['highlightInterval'] ||
            changes['earliest'] ||
            changes['latest'] ||
            changes['invalidDays'] ||
            changes['labels']) &&
            this.days.length
        )
            this.generateDays(this.days[0].date)
    }

    // #endregion


    public writeValue(value: string | Date): void {
        this.value = value
    }


    // #region controls

    protected open(): void {
        this.generateDays(this._value$.getValue() ?? new Date())

        this.expanded = true

        this.toggleEventListeners(true)


        setTimeout(() => {
            const dayBtns           : HTMLButtonElement[]
                                    = Array.from(
                                        this.elementRef
                                            .nativeElement
                                            .querySelectorAll('.day')
                                        )

            const ctrlBtns          : HTMLButtonElement[]
                                        
                                    = Array.from(
                                        this.elementRef
                                            .nativeElement
                                            .querySelectorAll('.btn-prev, .btn-next')
                                    )

            // Focus the first day that is not disabled.
            // When no days are available, try the other
            // controls
            const firstAvailable    : HTMLButtonElement
                                    = [...dayBtns, ...ctrlBtns].find(button => !button.disabled)

            if(firstAvailable)
                firstAvailable.focus()
        })
    }

    protected close(): void {
        this.expanded = false

        this.toggleEventListeners(false)
    }

    protected nextMonth(): void {
        if(this.days.length)
            this.generateDays(
                daysAfter(
                    this.days.at(-1).date,
                    1
                )
            )
    }

    protected prevMonth(): void {
        if(this.days.length)
            this.generateDays(
                daysBefore(
                    this.days[0].date,
                    1
                )
            )
    }

    private handleKeyboardEvents : (e: KeyboardEvent) => void = (e) => {
        switch(e.key) {
            case 'Escape':
                this.close()
                e.preventDefault()
                break
            case 'ArrowUp':
                this.traverseDays(-7)
                e.preventDefault()
                break
            case 'ArrowDown':
                this.traverseDays(7)
                e.preventDefault()
                break
            case 'ArrowLeft':
                this.traverseDays(-1)
                e.preventDefault()
                break
            case 'ArrowRight':
                this.traverseDays(1)
                e.preventDefault()
                break
        }
    }

    private traverseDays(n: number = 1): void {
        const activeElement     : Element
                                = document.activeElement

        const picker            : HTMLElement
                                = this.elementRef.nativeElement

        if(picker.contains(activeElement) && activeElement.classList.contains('day')) {

            const dayElements   : HTMLButtonElement[]
                                = Array.from(picker.getElementsByClassName('day') as HTMLCollectionOf<HTMLButtonElement>)

            const current       : number
                                = dayElements.findIndex(item => item === activeElement)

            dayElements[current + n]?.focus()

        } else
            (picker.getElementsByClassName('day')[0] as HTMLButtonElement)?.focus()
    }

    protected dayClicked(e: MouseEvent): void {
        const target: HTMLElement = e.target as HTMLElement
        const button: HTMLButtonElement = target.closest('button')
        if (button == null) return

        const formattedValue : string = button.dataset['day']
        if(formattedValue) {
            const d : Date = parse(formattedValue, 'yyyyMMdd')
            // clicking / hitting enter on an already
        // selected date will close the dialog
            if(isSameDay(this.value, d))
                this.close()
            else {
                this.value = d
                this.generateDays(this.days[0].date)
                this.close()
            }
        }
    }

    // #endregion


    // #region generate days

    private generateDays(forDate: Date): void {
        
        const d                     : Day[] = []
        const startOfNextMonth      : Date  = daysAfter(endOfMonth(forDate), 1)
        const today                 : Date  = getToday()

        const highlightedInterval   : DateInterval | null
                                    = this._value$.getValue() && this.sanitizedHighlightInterval !== 0
                                        ? this.getHighlightedInterval()
                                        : null

        let current                 : Date  = startOfMonth(forDate)

        this.currentDate            = current
        this.firstDayOffset         = getDayOfWeek(current)

        while(!isSameDay(current, startOfNextMonth)) {
            d.push({
                ...this.isDaySelected(current, highlightedInterval),

                date            : new Date(current),
                formatted       : format(current, 'yyyyMMdd'),
                isToday         : isSameDay(current, today),
                disabled        : this.isDayDisabled(current),
                weekday         : getDayOfWeek(current),
                label           : this.getDayLabel(current),
            })

            current = daysAfter(current, 1)
        }

        this.days = d
    }

    private getHighlightedInterval(): DateInterval {
        const selectedDay : Date = this._value$.getValue()

        if(this.sanitizedHighlightInterval > 0)
            return {
                start   : selectedDay,
                end     : daysAfter(
                            selectedDay,
                            this.sanitizedHighlightInterval
                        )
            }
        else
            // invert the interval for negative interval lengths:
            return {
                start   : daysAfter(
                            selectedDay,
                            this.sanitizedHighlightInterval
                        ),
                end     : selectedDay
            }
    }

    private isDaySelected(day: Date, interval: DateInterval | null): DaySelection {
        const selectedDay   : Date | null   = this._value$.getValue()

        let isSelected      : boolean       = false
        let inInterval      : boolean       = false
        let firstSelected   : boolean       = false
        let lastSelected    : boolean       = false

        if(selectedDay !== null) {
            isSelected      = isSameDay(day, selectedDay)

            inInterval      = interval
                                ?   isWithinInterval(day, interval) ||
                                    isSameDay(day, interval.start) ||
                                    isSameDay(day, interval.end)
                                :   false

            firstSelected   = inInterval
                                ?   isSameDay(day, interval.start)
                                :   false
                                
            lastSelected    = inInterval
                                ?   isSameDay(day, interval.end)
                                :   false
        }

        return {
            isSelected,
            inInterval,
            firstInInterval: firstSelected,
            lastInInterval: lastSelected
        }
    }

    private isDayDisabled(day: Date): boolean {
        if(!this.earliest && !this.latest && !this.invalidDays.length)
            return false

        if(typeof this.earliest !== 'undefined' && isDayBefore(day, this.toDate(this.earliest)))
            return true

        if(typeof this.latest !== 'undefined' && isDayAfter(day, this.toDate(this.latest)))
            return true

        if(this.invalidDays.length)
            return !!this.invalidDays.find(
                (invalidDay: string|Date) => isSameDay(day, this.toDate(invalidDay))
            )

        return false
    }

    private getDayLabel(day: Date): DayLabel | undefined {
        if(!this.labels.length)
            return

        return this.labels
            .find((l: DayLabel) =>
                isSameDay(day, this.toDate(l.date))
            )
    }

    // #endregion


    // #region event handling

    private subscribeForValueChanges(): void {
        this._value$
            .pipe(
                distinctUntilChanged((previous: Date, current: Date) =>
                    previous !== null && current !== null
                        ? isSameDay(previous, current)
                        : false
                ),
            )
            .subscribe((value: Date|null) => {
                if(value === null)
                    return

                const date: string | Date = this.format
                                                ? format(value, this.format)
                                                : value

                if(typeof this.onChange === 'function')
                    this.onChange(date)
            })
    }

    private toggleEventListeners(set: boolean = true): void {
        if(set) {
            document.body.addEventListener('focus', this.onFocusChange, { capture: true })
            document.body.addEventListener('keydown', this.handleKeyboardEvents, { capture: true })
        } else {
            document.body.removeEventListener('focus', this.onFocusChange, { capture: true })
            document.body.removeEventListener('keydown', this.handleKeyboardEvents, { capture: true })
        }
    }

    private onFocusChange : (e: FocusEvent) => void = (e) => {
        // trap focus in the date picker as long as it is open
        const picker : HTMLElement = this.elementRef.nativeElement

        if(e.target && !picker.contains(e.target as Node)) {
            e.preventDefault()
            e.stopPropagation()

            const focusables: HTMLButtonElement[] = Array.from(picker.getElementsByTagName('button')).filter(button => !button.disabled)

            // check if the first button is the one loosing focus
            // if that is the case wrap around to the last one
            if(focusables[0] === e.relatedTarget)
                focusables.at(-1)?.focus()
            else
                focusables[0]?.focus()
        }
    }

    // #endregion

    

    // #region util fns

    private toDate(date: string|Date): Date {
        if(date instanceof Date) return date

        if(typeof date === 'string' && typeof this.format !== 'string')
            throwNoFormatStringError()

        return parse(date, this.format)
    }

    protected trackFn(_: number, day: Day): string {
        return day.formatted
    }

    // #endregion
}
