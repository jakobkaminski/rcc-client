import	{	NgModule					} from '@angular/core'
// import	{	ItemModule					} from '@rcc/common'
import	{	itemSelectModalProvider		} from './item-select-modal'

@NgModule({
	imports:[
	],
	providers:[
		itemSelectModalProvider
	]
})
export class BengalInjectionsModule {}
