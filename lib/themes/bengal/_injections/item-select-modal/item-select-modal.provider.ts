import	{	Provider							}	from '@angular/core'
import	{	provideItemSelectModal				}	from '@rcc/common/items/select'
import	{	BengalItemSelectModalComponent		}	from './item-select-modal.component'

export const itemSelectModalProvider 	: Provider
										= provideItemSelectModal(BengalItemSelectModalComponent)
