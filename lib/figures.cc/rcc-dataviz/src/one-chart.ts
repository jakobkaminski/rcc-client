import type { TimeLocaleDefinition } from "d3-time-format";
import YesNoChart from "./components/charts/yes-no-chart";
import Controls from "./controls";

import { DIMENSIONS_DEFAULT, TickDataScaleX } from "./models";
import type { ApplicationState, RawDataString, RawDataBoolean } from "./models";
import {
  parseTime,
  generateConsumableDataByTimePeriod,
  isArrayOfConsumableDataForYesNo,
  isArrayOfConsumableDataForGroupCharts,
} from "./util";

import "./style.css";

// load the raw data
import yesNoJson from "./content/RCC-Datasets-2023-04-25__yesNo.json";

const rawData = yesNoJson[0] as unknown as RawDataBoolean;
const notes = yesNoJson[2] as unknown as RawDataString;

const localeDE: TimeLocaleDefinition = {
  dateTime: "%A, der %e. %B %Y, %X",
  date: "%d.%m.%Y",
  time: "%H:%M:%S",
  periods: ["AM", "PM"],
  days: [
    "Sonntag",
    "Montag",
    "Dienstag",
    "Mittwoch",
    "Donnerstag",
    "Freitag",
    "Samstag",
  ],
  shortDays: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
  months: [
    "Januar",
    "Februar",
    "März",
    "April",
    "Mai",
    "Juni",
    "Juli",
    "August",
    "September",
    "Oktober",
    "November",
    "Dezember",
  ],
  shortMonths: ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"],
};

// APPLICATION STATE
// get min and max date, pick a start date and calculate days ahead
const datapointDates: Date[] = [rawData, notes]
  .map((dataSet) => dataSet.datapoints)
  .flat()
  .flat()
  .map((d) => parseTime(d.date) as Date);

const minDateYesNo = new Date(
  Math.min(...datapointDates.map((d) => d.getTime()))
);
const maxDateYesNo = new Date(
  Math.max(...datapointDates.map((d) => d.getTime()))
);
const startDate = minDateYesNo;

const initialState: ApplicationState = {
  language: "de",
  timePeriod: "year",
  startDate,
  minDate: minDateYesNo,
  maxDate: maxDateYesNo,
  daysAhead: 366,
};
const endDate = new Date(startDate.getTime());
endDate.setDate(startDate.getDate() + initialState.daysAhead);

// PROCESS DATA
const consumableData = generateConsumableDataByTimePeriod({
  dataPoints: rawData.datapoints,
  notes: notes,
  startDate,
  daysAhead: initialState.daysAhead,
  timePeriod: initialState.timePeriod,
  chartType: "yesNo",
});

// RENDER
const datavizContainer = document.querySelector<HTMLDivElement>("#dataviz")!;

const commonChartProps = {
  startDate,
  endDate,
  timePeriod: initialState.timePeriod,
  language: initialState.language,
  onTick: (event: Event, tickData: TickDataScaleX) => {
    console.log(event, tickData);
  },
  dimensions: DIMENSIONS_DEFAULT,
  locales: { de: localeDE },
};

let chart: YesNoChart;
if (
  isArrayOfConsumableDataForYesNo(consumableData) ||
  isArrayOfConsumableDataForGroupCharts(consumableData)
) {
  chart = new YesNoChart({
    data: consumableData,
    options: rawData.question.options,
    ...commonChartProps,
  });
  datavizContainer.appendChild(chart.svg.node()!);
  chart.render();
} else {
  console.error(
    "Error: consumableData is not an array of ConsumableDataForYesNo or ConsumableDataForGroupCharts"
  );
}

const usedLanguages = yesNoJson.reduce((languages, currentRawData) => {
  const languagesForCurrentRawData = Object.keys(
    currentRawData.question.translations
  );
  languagesForCurrentRawData.forEach((language) => {
    if (!languages.includes(language)) {
      languages.push(language);
    }
  });
  return languages;
}, [] as string[]);
const controls = new Controls(initialState, { languages: usedLanguages });

controls.onStateChange((newState) => {
  const { startDate, daysAhead, timePeriod, language } = newState;

  const consumableData = generateConsumableDataByTimePeriod({
    dataPoints: rawData.datapoints,
    notes: notes,
    startDate,
    daysAhead: daysAhead,
    timePeriod: timePeriod,
    chartType: "yesNo",
  });

  if (
    isArrayOfConsumableDataForYesNo(consumableData) ||
    isArrayOfConsumableDataForGroupCharts(consumableData)
  ) {
    chart.updateData(consumableData);
    chart.updateLanguage(language);
    chart.updateTimePeriod(timePeriod);
  } else {
    console.error(
      "Error: consumableData for YesNo chart is not an array of ConsumableDataForYesNo or ConsumableDataForGroupCharts"
    );
  }
});
