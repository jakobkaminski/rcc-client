import { timeParse } from "d3-time-format";
import type { TimeLocaleDefinition } from "d3-time-format";
import { ScalePower } from "d3-scale";
import type {
  RawDataString,
  DatapointInteger,
  DatapointBoolean,
  ConsumableDataForSingleBars,
  TickDataScaleX,
  TimePeriod,
  ConsumableDataWithValue,
  DatapointBasics,
  ConsumableDataForGroupCharts,
  ChartType,
  OptionInteger,
  ValueWithOption,
  DatapointIntegerOrIntegerArray,
} from "../models/index";
import {
  isArrayOfDatapointBoolean,
  isArrayOfDatapointInteger,
  isArrayOfDatapointIntegerArray,
  isNumber,
  isNumberArray,
} from "./type-guards";

export * from "./type-guards";

// locale definitions for time formatting (de and en)
export const localeEN: TimeLocaleDefinition = {
  dateTime: "%x, %X",
  date: "%-m/%-d/%Y",
  time: "%-I:%M:%S %p",
  periods: ["AM", "PM"],
  days: [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ],
  shortDays: ["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"],
  months: [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ],
  shortMonths: ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"],
};

// how to parse the date string from the raw data
export const parseTime = timeParse("%Y-%m-%d"); // %Y is 4-digit year, %m is 0-padded month, %d is 0-padded day

const getDatesWithinTimeFrame = (
  startDate: Date,
  timePeriod: TimePeriod
): Date[] => {
  switch (timePeriod) {
    case "year":
      // from the start date, get the month
      const startMonth = startDate.getMonth();

      // from the start date, generate a new datapoint for each month ahead
      const datesWithinTimeFrame: Date[] = [];
      for (let i = 0; i < 12; i++) {
        const date = new Date(startDate.getTime());

        date.setMonth(startMonth + i);
        datesWithinTimeFrame.push(date);
      }

      return datesWithinTimeFrame;
    case "month":
      // from the start date, get the day
      const startDay = startDate.getDate();

      // get the same day next month
      const nextMonth = new Date(startDate.getTime());
      nextMonth.setMonth(nextMonth.getMonth() + 1);

      // get the amount of days between the start date and the next month
      const daysBetween = getDaysBetweenDates(startDate, nextMonth);

      // from the start date, generate a new datapoint for each day ahead
      const datesWithinTimeFrameMonth: Date[] = [];
      for (let i = 0; i < daysBetween; i++) {
        const date = new Date(startDate.getTime());

        date.setDate(startDay + i);
        datesWithinTimeFrameMonth.push(date);
      }

      return datesWithinTimeFrameMonth;
    case "week":
      // from the start date, get the day
      const startDayWeek = startDate.getDate();

      // from the start date, generate a new datapoint for each day ahead
      const datesWithinTimeFrameWeek: Date[] = [];
      for (let i = 0; i < 7; i++) {
        const date = new Date(startDate.getTime());

        date.setDate(startDayWeek + i);
        datesWithinTimeFrameWeek.push(date);
      }

      return datesWithinTimeFrameWeek;
    default:
      return [];
  }
};
// TODO: - change generateConsumableDailyDataWithNotes function to return a DatapointBasics array
//       and then use this function to generate the ConsumableDataWithValue array in a separate function.
//       - Create a function to just return datapoints with empty values for days not covered by the raw data
//       - Create a function to cross-reference the notes with the datapoints
//       - Create a function to generate the ConsumableDataWithValue array from the datapoints with notes
/**
 * Process the raw data into consumable data for the chart. Reduces the amount of data points
 * to the given time frame and also generates new data points with empty values for days not
 * covered by the raw data.
 *
 * Afterwards, the consumable data points are cross-referenced with the notes.
 *
 * @param dataPoints raw data points array
 * @param notes array of notes
 * @param startDate the date to start from
 * @param daysAhead how many days to go ahead
 * @returns
 */
export const generateConsumableDailyDataWithNotes = <T extends DatapointBasics>(
  dataPoints: T[],
  notes: RawDataString,
  startDate: Date,
  daysAhead: number
): ConsumableDataWithValue[] => {
  // from the start date, generate a new datapoint for each day ahead
  const datesWithinTimeFrame: Date[] = [];
  for (let i = 0; i <= daysAhead; i++) {
    const date = new Date(startDate.getTime());

    date.setDate(startDate.getDate() + i);
    datesWithinTimeFrame.push(date);
  }

  // for the dates within the time frame, generate a new datapoint for each date.
  // if there is no datapoint for a date, generate a new one with value null
  const datapointsWithinTimeFrame = datesWithinTimeFrame.map((date) => {
    // check if there is an existing datapoint for the date
    const existingDatapointForDate = dataPoints.find(
      (dp) => parseTime(dp.date)?.getTime() === date.getTime()
    );

    if (existingDatapointForDate) {
      // check if there is a note for the date (has to be a string with length > 0)
      const hasNote =
        existingDatapointForDate.note &&
        existingDatapointForDate.note.length > 0;

      return {
        date: parseTime(existingDatapointForDate.date)!,
        value: existingDatapointForDate.value,
        note: hasNote ? existingDatapointForDate.note : null,
      };
    } else {
      return {
        date,
        value: null,
        note: null,
      };
    }
  });

  // cross check the notes with the datapoints
  // if there is a note for a date that is not given in the datapoint, update the datapoint with the note
  const datapointsWithinTimeFrameWithNotes = datapointsWithinTimeFrame.map(
    (dp) => {
      const note = notes.datapoints.find(
        (n) => parseTime(n.date)?.getTime() === dp.date.getTime()
      );

      if (note) {
        return {
          ...dp,
          note: `${note.value}`,
        };
      } else {
        return { ...dp };
      }
    }
  );

  return datapointsWithinTimeFrameWithNotes;
};

export const generateMultipleChoiceConsumableData = (
  consumableData: ConsumableDataWithValue[],
  multipleChoiceOptions: OptionInteger[]
): ConsumableDataForGroupCharts[] => {
  if (
    consumableData.find(
      (datum) =>
        !(
          isNumberArray(datum.value) ||
          isNumber(datum.value) ||
          datum.value === null
        )
    )
  ) {
    throw new Error(
      "The consumable data contains a value that is not a number, number array or null"
    );
  }

  // for each datum in the consumable data, generate a new datum for each multiple choice option
  const ConsumableDataForGroupCharts: ConsumableDataForGroupCharts[] =
    consumableData.map((datum) => {
      if (datum.value === null) {
        return {
          date: datum.date,
          values: null,
          note: datum.note,
        };
      } else {
        const existingValues = isNumberArray(datum.value)
          ? datum.value
          : ([datum.value] as number[]);

        // for each multiple choice option, generate data for a bar within the grouped bars
        const multipleChoiceData: ValueWithOption[] = multipleChoiceOptions.map(
          (option) => {
            return {
              yScaleValue: option.value,
              option: existingValues.includes(option.value)
                ? option.value
                : null,
            };
          }
        );

        return {
          date: datum.date,
          note: datum.note,
          values: multipleChoiceData,
        };
      }
    });

  return ConsumableDataForGroupCharts;
};

export const generateAveragedYearlyIntegerData = (
  dataPoints: DatapointInteger[],
  startDate: Date
): ConsumableDataForSingleBars[] => {
  // from the start date, generate a new datapoint for each month ahead
  const datesWithinTimeFrame: Date[] = getDatesWithinTimeFrame(
    startDate,
    "year"
  );

  // for the dates within the time frame, generate a new datapoint for each date.
  // if there is no datapoint for a date, generate a new one with value null
  const datapointsWithinTimeFrame: ConsumableDataForSingleBars[] =
    datesWithinTimeFrame.map((currentDate) => {
      // get month for date
      const currentMonth = currentDate.getMonth();
      const currentYear = currentDate.getFullYear();

      // get all datapoints for the month
      const datapointsForMonth = dataPoints.filter(
        (dp) =>
          parseTime(dp.date)?.getMonth() === currentMonth &&
          parseTime(dp.date)?.getFullYear() === currentYear
      );

      // get the average value for the month. if there are no datapoints, set the value to null
      const aggregatedValue =
        datapointsForMonth.length > 0
          ? datapointsForMonth.reduce((acc, dp) => acc + dp.value, 0) /
            datapointsForMonth.length
          : null;

      return {
        date: currentDate,
        value: aggregatedValue,
        note: null,
      };
    });

  return datapointsWithinTimeFrame;
};

export const generateSummedYearlyBooleanData = (
  dataPoints: DatapointBoolean[],
  startDate: Date
): ConsumableDataForGroupCharts[] => {
  // from the start date, generate a new datapoint for each month ahead
  const datesWithinTimeFrame: Date[] = getDatesWithinTimeFrame(
    startDate,
    "year"
  );

  // for the dates within the time frame, generate a new datapoint for each date.
  // if there is no datapoint for a date, generate a new one with value null
  const datapointsWithinTimeFrame: ConsumableDataForGroupCharts[] =
    datesWithinTimeFrame.map((currentDate) => {
      // get month for date
      const currentMonth = currentDate.getMonth();
      const currentYear = currentDate.getFullYear();

      // get all datapoints for the month
      const datapointsForMonth = dataPoints.filter(
        (dp) =>
          parseTime(dp.date)?.getMonth() === currentMonth &&
          parseTime(dp.date)?.getFullYear() === currentYear
      );

      // get the sum of values for the month. if there are no datapoints, set the value to 0
      const summedMonthValueYes =
        datapointsForMonth.length > 0
          ? datapointsForMonth.reduce(
              (acc, dp) => acc + (dp.value === true ? 1 : 0),
              0
            )
          : 0;
      const summedMonthValueNo =
        datapointsForMonth.length - summedMonthValueYes;

      return {
        date: currentDate,
        values:
          datapointsForMonth.length > 0
            ? [
                { yScaleValue: summedMonthValueNo, option: false },
                {
                  yScaleValue: summedMonthValueYes,
                  option: true,
                },
              ]
            : null,
        note: null,
      };
    });

  return datapointsWithinTimeFrame;
};

export const generateSummedYearlyMultipleChoiceData = (
  dataPoints: DatapointIntegerOrIntegerArray[],
  options: OptionInteger[],
  startDate: Date
): ConsumableDataForGroupCharts[] => {
  // from the start date, generate a new datapoint for each month ahead
  const datesWithinTimeFrame: Date[] = getDatesWithinTimeFrame(
    startDate,
    "year"
  );

  // for the dates within the time frame, generate a new datapoint for each date.
  // if there is no datapoint for a date, generate a new one with value null
  const datapointsWithinTimeFrame: ConsumableDataForGroupCharts[] =
    datesWithinTimeFrame.map((currentDate) => {
      // get month for date
      const currentMonth = currentDate.getMonth();
      const currentYear = currentDate.getFullYear();

      // get all datapoints for the month
      const datapointsForMonth = dataPoints.filter(
        (dp) =>
          parseTime(dp.date)?.getMonth() === currentMonth &&
          parseTime(dp.date)?.getFullYear() === currentYear
      );

      // get the sum of values for the month. if there are no datapoints, set the values to 0
      const summedMonthValues: ValueWithOption[] =
        datapointsForMonth.length > 0
          ? options.map((option) => {
              const summedMonthValue =
                datapointsForMonth.length > 0
                  ? datapointsForMonth.reduce((acc, dp) => {
                      const values = Array.isArray(dp.value)
                        ? dp.value
                        : [dp.value];
                      return acc + (values.includes(option.value) ? 1 : 0);
                    }, 0)
                  : 0;
              return {
                yScaleValue: summedMonthValue,
                option: option.value,
              };
            })
          : [];

      return {
        date: currentDate,
        values: summedMonthValues,
        note: null,
      };
    });

  return datapointsWithinTimeFrame;
};

interface GenerateConsumableDataProps {
  dataPoints: DatapointBasics[];
  startDate: Date;
  timePeriod: TimePeriod;
  chartType: ChartType;
  notes?: RawDataString;
  daysAhead?: number;
  options?: OptionInteger[];
}
export const generateConsumableDataByTimePeriod = ({
  dataPoints, // raw data points to generate the consumable data from
  startDate,
  timePeriod,
  chartType,
  notes,
  daysAhead,
  options,
}: GenerateConsumableDataProps) => {
  // throw error if timePeriod is week or month but daysAhead or notes are not given
  if (
    (timePeriod === "week" || timePeriod === "month") &&
    (!daysAhead || !notes)
  ) {
    throw new Error(
      "daysAhead and notes are required for timePeriods 'week' and 'month'"
    );
  }

  // throw error if chartType is multipleChoice but options are not given
  if (chartType === "multipleChoice" && !options) {
    throw new Error("options are required for chartType 'multipleChoice'");
  }

  // generate the data
  let data: ConsumableDataWithValue[] | ConsumableDataForGroupCharts[] = [];

  switch (timePeriod) {
    case "week":
      if (notes && daysAhead) {
        data = generateConsumableDailyDataWithNotes(
          dataPoints,
          notes,
          startDate,
          daysAhead
        );
        if (chartType === "multipleChoice" && options) {
          data = generateMultipleChoiceConsumableData(data, options);
        }
      }
      break;

    case "month":
      if (notes && daysAhead) {
        data = generateConsumableDailyDataWithNotes(
          dataPoints,
          notes,
          startDate,
          daysAhead
        );
        if (chartType === "multipleChoice" && options) {
          data = generateMultipleChoiceConsumableData(data, options);
        }
      }
      break;

    case "year":
      // for chart type "quantities" and integer data, generate averaged yearly data points
      if (
        isArrayOfDatapointInteger(dataPoints) &&
        (chartType === "quantities" || chartType === "slider")
      ) {
        data = generateAveragedYearlyIntegerData(
          dataPoints,
          startDate
        ) as ConsumableDataForSingleBars[];
      }

      // for chart type "multipleChoice" and integer data, generate summed yearly data points
      if (
        (isArrayOfDatapointInteger(dataPoints) ||
          isArrayOfDatapointIntegerArray(dataPoints)) &&
        chartType === "multipleChoice"
      ) {
        data = generateSummedYearlyMultipleChoiceData(
          dataPoints,
          options as OptionInteger[],
          startDate
        );
      }

      // for boolean data, the raw data points are turned into a consumable data point with numeric values for yes and no
      if (isArrayOfDatapointBoolean(dataPoints)) {
        data = generateSummedYearlyBooleanData(dataPoints, startDate);
      }

      break;
  }

  return data;
};

/**
 * Returns a new Date object that is one week ahead of the given date.
 * @param  {Date} date
 */
export const getOneWeekAhead = (date: Date): Date => {
  const newDate = new Date(date.getTime());
  newDate.setDate(date.getDate() + 6);

  return newDate;
};

/**
 * Returns a new Date object that is one month ahead of the given date.
 * @param  {Date} date
 */
export const getOneMonthAhead = (date: Date): Date => {
  const newDate = new Date(date.getTime());
  newDate.setMonth(date.getMonth() + 1);

  return newDate;
};

/**
 * Returns a new Date object that is one year ahead of the given date.
 * @param date
 */
export const getOneYearAhead = (date: Date): Date => {
  const newDate = new Date(date.getTime());
  newDate.setFullYear(date.getFullYear() + 1);

  return newDate;
};

/**
 * Returns the number of days between two dates.
 * @param  {Date} date1
 * @param  {Date} date2
 */
export const getDaysBetweenDates = (date1: Date, date2: Date): number => {
  // Get timestamps for both dates
  const date1Timestamp = date1.getTime();
  const date2Timestamp = date2.getTime();

  // Get the difference between the two timestamps
  const diffTime = Math.abs(date2Timestamp - date1Timestamp);

  // Convert the difference to days and return
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
  return diffDays;
};

/**
 * like date.toISOString(), but respects the timezone offset
 * @param date
 */
export const getIsoStringWithTimeZoneOffsett = (date: Date) => {
  // timezone offset here in milliseconds
  var tzoffset = new Date().getTimezoneOffset() * 60000;

  // get the local "ISO" date string, but with the timezone offset
  var localISOTime = new Date(date.getTime() - tzoffset).toISOString();

  // return the iso string without time information
  return localISOTime.slice(0, 10);
};

/**
 * This function decides which tick data to show for the given time period.
 * For example, if the time period is "week", tick is shown for every day.
 * If the time period is "month", every second tick data is shown.
 *
 * @param timePeriod the selected time period
 * @param d tick data
 */
export const tickDataByTimePeriod = (
  timePeriod: TimePeriod,
  startDate: Date,
  d: TickDataScaleX
) => {
  let tickData: TickDataScaleX[] = [];
  switch (timePeriod) {
    case "week":
      tickData = [d];
      break;

    // only show every second day for month view
    case "month":
      // only show every second day starting from start date
      const isEverySecondDay = getDaysBetweenDates(startDate, d.date) % 2 === 0;

      // const isEven = d.date.getDate() % 2 === 0; // only show even days
      tickData = isEverySecondDay ? [d] : [];
      break;

    case "year":
      tickData = [d];
      break;

    default:
      tickData = [d];
      break;
  }
  return tickData;
};

export const splitIntoLines = (input: string, wordLength: number) => {
  const words = input.split(/\s+/g); // split on whitespace

  let line: string[] = []; // current line
  let lines: string[] = []; // all lines

  // loop through words and add them to the current line until the line length is too long
  words.forEach((word) => {
    // if the word is longer than the word length, split it up
    if (word.length > wordLength) {
      // split the word into chunks of the word length
      const chunks = word.match(new RegExp(`.{1,${wordLength}}`, "g"));

      // add the chunks to the line. Suffix each chunk with a hyphen, except the last one
      chunks?.forEach((chunk, index) => {
        const isLastChunk = index === chunks.length - 1;
        const _chunk = isLastChunk ? chunk : `${chunk}-`;

        // if the line is too long, add it to the lines and start a new line
        if (line.join(" ").length + chunk.length > wordLength) {
          lines.push(line.join(" "));
          line = [_chunk];
        }
        // otherwise, add the chunk to the line
        else {
          line.push(_chunk);
        }
      });
    } else if (line.join(" ").length + word.length > wordLength) {
      lines.push(line.join(" "));
      line = [word];
    } else {
      line.push(word);
    }
  });

  // add the last line
  lines.push(line.join(" "));

  return lines;
};

/**
 * This function calculates the width of the marks based on the number of marks, the gap size and
 * the stage width. The width of the marks is capped at MARK_SIZE_MAX and MARK_SIZE_MIN
 *
 *
 * @param  {number} stageWidth the width of the stage
 * @param  {number} numMarks the number of marks to be displayed
 * @param  {ScaleLinear} gapSizeScale the scale that maps the number of marks to the gap size
 */
export const calculateMarkWidth = (
  stageWidth: number,
  numMarks: number,
  gapSizeScale: (numMarks: number) => number,
  options?: {
    maxWidth?: number;
    minWidth?: number;
  }
) => {
  const gapSize = gapSizeScale(numMarks);

  let scaledMarkWidth = (stageWidth - gapSize * (numMarks - 1)) / numMarks;

  if (options?.minWidth && scaledMarkWidth < options.minWidth) {
    scaledMarkWidth = options.minWidth;
  }
  if (options?.maxWidth && scaledMarkWidth > options.maxWidth) {
    scaledMarkWidth = options.maxWidth;
  }

  return scaledMarkWidth;
};

/**
 * Power scales transform the values by raising them to a given power. This way, values
 * slowly increase/decrease at the beginning and then quickly towards the end.
 *
 * 🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄●●⏺
 * or
 * ⏺⏺⏺⏺⏺⏺⏺⏺●●🞄
 *
 * This function
 * reverses the power scale, so that values quickly increase/decrease at the beginning and
 * slowly towards the end.
 *
 * 🞄🞄●●⏺⏺⏺⏺⏺⏺⏺⏺
 * or
 * ⏺●●🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄🞄
 * @param  {ScalePower<number, number>} scale the power scale that should be reversed
 */
export const reversedPowScale = (scale: ScalePower<number, number>) => {
  const domainEnd = scale.domain()[1];
  const domainStart = scale.domain()[0];
  const sum = domainEnd + domainStart;
  return (x: number) => scale(sum - x);
};

/**
 * Creates a number of numeric steps between a min and max value and returns them as an array, including min and max.
 *
 * @param  {number} min the min boundary
 * @param  {number} max the max boundary
 * @param  {number} steps how many steps to take between min and max
 *
 * @example
 * stepsBetweenMinMax(0, 100, 3 ) // [0, 25, 50, 75, 100]
 * stepsBetweenMinMax(1, 10, 2 ) // [1, 4, 7, 10]
 * stepsBetweenMinMax(1, 10, 8 ) // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
 */
export const stepsBetweenMinMax = (min: number, max: number, steps: number) => {
  const stepSize = (max - min) / (steps + 1);
  const result: number[] = [];

  for (let i = 0; i < steps + 2; i++) {
    result.push(min + stepSize * i);
  }
  return result;
};
