/**
 * @fileoverview Main entry point for the application. This file is responsible
 * for loading the raw data, processing it, and rendering the charts.
 */
import type { TimeLocaleDefinition } from "d3-time-format";
import quantitiesChart from "./components/charts/quantities-chart";
import YesNoChart from "./components/charts/yes-no-chart";
import MultipleChoiceChart from "./components/charts/multiple-choice-chart";
import SliderChart from "./components/charts/slider-chart";

import Controls from "./controls";
import {
  COLORMAP_DEFAULT,
  COLORS_DEFAULT,
  ChartStyleProps,
  DIMENSIONS_DEFAULT,
  FONT_DEFAULT,
  RawDataIntegerSlider,
  TickDataScaleX,
} from "./models";
import type {
  RawData,
  ApplicationState,
  RawDataInteger,
  RawDataString,
  RawDataBoolean,
  ConsumableDataForSingleBars,
  ConsumableDataForYesNo,
  ConsumableDataForGroupCharts,
  ChartType,
  RawDataIntegerMulti,
} from "./models";
import {
  parseTime,
  generateConsumableDataByTimePeriod,
  isArrayOfConsumableDataForSingleBars,
  isArrayOfConsumableDataForYesNo,
  isArrayOfConsumableDataForGroupCharts,
} from "./util";
import "./style.css";

// load the raw data
import quantitiesJson from "./content/RCC-Datasets-2023-04-18__quantities.json";
import yesNoJson from "./content/RCC-Datasets-2023-04-25__yesNo.json";
import multipleChoiceMultiAnswers_MCMA_Json from "./content/RCC-Datasets-2023-04-18__multipleChoice_multiAnswers.json";
import multipleChoiceSingleAnswers_MCSA_Json from "./content/RCC-Datasets-2023-04-18__multipleChoice_singleAnswers.json";
import sliderJson from "./content/RCC-Datasets-2023-04-18__slider.json";

const localeDE: TimeLocaleDefinition = {
  dateTime: "%A, der %e. %B %Y, %X",
  date: "%d.%m.%Y",
  time: "%H:%M:%S",
  periods: ["AM", "PM"],
  days: [
    "Sonntag",
    "Montag",
    "Dienstag",
    "Mittwoch",
    "Donnerstag",
    "Freitag",
    "Samstag",
  ],
  shortDays: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa"],
  months: [
    "Januar",
    "Februar",
    "März",
    "April",
    "Mai",
    "Juni",
    "Juli",
    "August",
    "September",
    "Oktober",
    "November",
    "Dezember",
  ],
  shortMonths: ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"],
};
const localeFR: TimeLocaleDefinition = {
  // French
  dateTime: "%A %e %B %Y à %X",
  date: "%d/%m/%Y",
  time: "%H:%M:%S",
  periods: ["AM", "PM"],
  days: [
    "dimanche",
    "lundi",
    "mardi",
    "mercredi",
    "jeudi",
    "vendredi",
    "samedi",
  ],
  shortDays: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
  months: [
    "janvier",
    "février",
    "mars",
    "avril",
    "mai",
    "juin",
    "juillet",
    "août",
    "septembre",
    "octobre",
    "novembre",
    "décembre",
  ],
  shortMonths: ["J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"],
};

const DEBUG = false;

const alternateChartStyles: ChartStyleProps = {
  color: {
    primary: "#756BB1",
    inactive: COLORS_DEFAULT.greyBack,
    scale: new Map([
      [0, "#756BB1"],
      [1, "#9E9AC8"],
      [2, "#BCBDDC"],
      [3, "#DADAEB"],
    ]),
  },
  note: {
    backgroundColor: "#FF7F00",
    font: {
      family: "Arial",
      size: 8.5,
      color: "white",
      weight: 700,
      lineHeight: 9,
    },
  },
  font: {
    family: "Arial",
    size: 8.5,
    color: "#333",
    weight: 500,
    lineHeight: 9,
  },
};

const rcChartStyles: ChartStyleProps = {
  color: {
    primary: COLORS_DEFAULT.greenDefault,
    inactive: COLORS_DEFAULT.greyBack,
    scale: COLORMAP_DEFAULT,
  },
  note: {
    backgroundColor: COLORS_DEFAULT.purpleDisabled,
    font: {
      family: FONT_DEFAULT.family,
      weight: FONT_DEFAULT.weigth,
      size: FONT_DEFAULT.size,
      color: COLORS_DEFAULT.textLight,
      lineHeight: FONT_DEFAULT.lineHeight,
    },
  },
  font: {
    family: FONT_DEFAULT.family,
    weight: FONT_DEFAULT.weigth,
    size: FONT_DEFAULT.size,
    color: COLORS_DEFAULT.textLight,
    lineHeight: FONT_DEFAULT.lineHeight,
  },
};

interface RawDataWithSettings {
  rawData: RawData;
  notes: RawDataString;
  chartType: ChartType;
  styles: ChartStyleProps;
}
const rawDataWithSettings: RawDataWithSettings[] = [
  {
    rawData: yesNoJson[0] as unknown as RawDataBoolean,
    notes: yesNoJson[2] as unknown as RawDataString,
    chartType: "yesNo",
    styles: rcChartStyles,
  },
  {
    rawData: sliderJson[0] as RawDataIntegerSlider,
    notes: sliderJson[1] as RawDataString,
    chartType: "slider",
    styles: rcChartStyles,
  },
  {
    rawData: quantitiesJson[0] as RawDataInteger,
    notes: quantitiesJson[2] as RawDataString,
    chartType: "quantities",
    styles: alternateChartStyles,
  },
  {
    rawData: multipleChoiceSingleAnswers_MCSA_Json[0] as RawDataInteger,
    notes: multipleChoiceSingleAnswers_MCSA_Json[1] as RawDataString,
    chartType: "multipleChoice",
    styles: rcChartStyles,
  },
  {
    rawData: multipleChoiceMultiAnswers_MCMA_Json[0] as RawDataIntegerMulti,
    notes: multipleChoiceMultiAnswers_MCMA_Json[1] as RawDataString,
    chartType: "multipleChoice",
    styles: rcChartStyles,
  },
];

// APPLICATION STATE
// get min and max date, pick a start date and calculate days ahead
const datapointDates: Date[] = rawDataWithSettings
  .map((dataSet) => dataSet.rawData.datapoints)
  .flat()
  .flat()
  .map((d) => parseTime(d.date) as Date);

const minDateYesNo = new Date(
  Math.min(...datapointDates.map((d) => d.getTime()))
);
const maxDateYesNo = new Date(
  Math.max(...datapointDates.map((d) => d.getTime()))
);
const startDate = minDateYesNo;

const initialState: ApplicationState = {
  language: "de",
  timePeriod: "week",
  startDate,
  minDate: minDateYesNo,
  maxDate: maxDateYesNo,
  daysAhead: 6,
};
const endDate = new Date(startDate.getTime());
endDate.setDate(startDate.getDate() + initialState.daysAhead);

// PROCESS DATA
const consumableData = rawDataWithSettings.map((dataSet) => {
  return generateConsumableDataByTimePeriod({
    dataPoints: dataSet.rawData.datapoints,
    notes: dataSet.notes,
    startDate,
    daysAhead: initialState.daysAhead,
    timePeriod: initialState.timePeriod,
    chartType: dataSet.chartType,
    options: (dataSet.rawData as RawDataInteger).question.options,
  });
}) as [
  ConsumableDataForSingleBars[],
  ConsumableDataForYesNo[],
  ConsumableDataForGroupCharts[],
  ConsumableDataForGroupCharts[]
];

// RENDER
const datavizContainer = document.querySelector<HTMLDivElement>("#dataviz")!;

const charts = consumableData.map((dataSet, dataSetIndex) => {
  let chart;

  const commonChartProps = {
    startDate,
    endDate,
    timePeriod: initialState.timePeriod,
    language: initialState.language,
    styles: rawDataWithSettings[dataSetIndex].styles,
    onTick: (event: Event, tickData: TickDataScaleX) => {
      console.log(event, tickData);
    },
    dimensions: { ...DIMENSIONS_DEFAULT, width: 266 },
    debug: DEBUG,
    locales: { de: localeDE, fr: localeFR },
  };
  switch (rawDataWithSettings[dataSetIndex].chartType) {
    case "quantities":
      chart = new quantitiesChart({
        data: dataSet as ConsumableDataForSingleBars[],
        options: (rawDataWithSettings[dataSetIndex].rawData as RawDataInteger)
          .question.options,
        ...commonChartProps,
      });
      break;
    case "yesNo":
      chart = new YesNoChart({
        data: dataSet as
          | ConsumableDataForYesNo[]
          | ConsumableDataForGroupCharts[],
        options: (rawDataWithSettings[dataSetIndex].rawData as RawDataBoolean)
          .question.options,
        ...commonChartProps,
      });
      break;

    case "multipleChoice":
      chart = new MultipleChoiceChart({
        data: dataSet as ConsumableDataForGroupCharts[],
        options: (rawDataWithSettings[dataSetIndex].rawData as RawDataInteger)
          .question.options,
        ...commonChartProps,
      });
      break;

    case "slider":
      chart = new SliderChart({
        data: dataSet as ConsumableDataForSingleBars[],
        min: (rawDataWithSettings[dataSetIndex].rawData as RawDataIntegerSlider)
          .question.min,
        max: (rawDataWithSettings[dataSetIndex].rawData as RawDataIntegerSlider)
          .question.max,
        ...commonChartProps,
      });
      break;

    default:
      chart = new quantitiesChart({
        data: dataSet as ConsumableDataForSingleBars[],
        options: (rawDataWithSettings[dataSetIndex].rawData as RawDataInteger)
          .question.options,
        startDate,
        endDate,
        timePeriod: initialState.timePeriod,
        language: initialState.language,
        onTick: (event, tickData) => {
          console.log(event, tickData);
        },

        dimensions: {
          width: 266,
          margin: {
            top: 1,
            right: 1,
            bottom: 30,
            left: 54,
          },
        },
        debug: DEBUG,
      });
      break;
  }
  datavizContainer.appendChild(chart.svg.node()!);
  chart.render();

  return chart;
});

// CONTROLS
// get used languages from data
const usedLanguages = rawDataWithSettings.reduce(
  (languages, currentRawData) => {
    const languagesForCurrentRawData = Object.keys(
      currentRawData.rawData.question.translations
    );
    languagesForCurrentRawData.forEach((language) => {
      if (!languages.includes(language)) {
        languages.push(language);
      }
    });
    return languages;
  },
  [] as string[]
);

const controls = new Controls(initialState, { languages: usedLanguages });
controls.onStateChange((newState) => {
  const { startDate, daysAhead, timePeriod, language } = newState;

  const consumableData = rawDataWithSettings.map((dataSet) => {
    return generateConsumableDataByTimePeriod({
      dataPoints: dataSet.rawData.datapoints,
      notes: dataSet.notes,
      startDate,
      daysAhead: daysAhead,
      timePeriod: timePeriod,
      chartType: dataSet.chartType,
      options: (dataSet.rawData as RawDataInteger).question.options,
    });
  }) as [
    ConsumableDataForSingleBars[],
    ConsumableDataForYesNo[],
    ConsumableDataForGroupCharts[],
    ConsumableDataForGroupCharts[]
  ];

  consumableData.forEach((dataSet, dataSetIndex) => {
    const chart = charts[dataSetIndex];
    if (chart instanceof quantitiesChart) {
      if (isArrayOfConsumableDataForSingleBars(dataSet)) {
        chart.updateData(dataSet);
        chart.updateLanguage(language);
        chart.updateTimePeriod(timePeriod);
      } else {
        console.error(
          "Error: consumableData for VMC chart is not an array of ConsumableDataForSingleBars"
        );
      }
    } else if (chart instanceof SliderChart) {
      if (isArrayOfConsumableDataForSingleBars(dataSet)) {
        chart.updateData(dataSet);
        chart.updateLanguage(language);
        chart.updateTimePeriod(timePeriod);
      } else {
        console.error(
          "Error: consumableData for VMC chart is not an array of ConsumableDataForSingleBars"
        );
      }
    } else if (chart instanceof YesNoChart) {
      if (
        isArrayOfConsumableDataForYesNo(dataSet) ||
        isArrayOfConsumableDataForGroupCharts(dataSet)
      ) {
        chart.updateData(dataSet);
        chart.updateLanguage(language);
        chart.updateTimePeriod(timePeriod);
      } else {
        console.error(
          "Error: consumableData for YesNo chart is not an array of ConsumableDataForYesNo"
        );
      }
    } else if (chart instanceof MultipleChoiceChart) {
      if (isArrayOfConsumableDataForGroupCharts(dataSet)) {
        chart.updateData(dataSet);
        chart.updateLanguage(language);
        chart.updateTimePeriod(timePeriod);
      } else {
        console.error(
          `Error: consumableData for MC chart is not an array of
          ConsumableDataForGroupCharts or ConsumableDataForGroupCharts`
        );
      }
    }
  });
});
