import { ScaleLinear, scaleLinear } from "d3-scale";
import BaseChartWithOptions from "../base-chart-with-options";
import YAxis from "../../chart-elements/y-axis/index";
import YLegend from "../../chart-elements/y-legend/index";
import GroupedBars from "../../chart-elements/grouped-bars/index";

import {
  MARK_SIZE_MAX,
  GRAPH_CLASSNAMES,
  ConsumableDataForGroupCharts,
  BaseChartWithOptionsConfig,
} from "../../../models/index";
import type { OptionInteger } from "../../../models/index";

interface MultipleChoiceConfig
  extends BaseChartWithOptionsConfig<
    ConsumableDataForGroupCharts,
    OptionInteger
  > {}
export default class MultipleChoice extends BaseChartWithOptions<
  ConsumableDataForGroupCharts,
  OptionInteger
> {
  private scaleYOptions: ScaleLinear<number, number>;
  private scaleYSummedYearly: ScaleLinear<number, number>;
  private yAxisOptions: YAxis;
  private yLegendSummedYearly: YLegend;
  private groupedBars: GroupedBars;

  constructor(props: MultipleChoiceConfig) {
    super(props);

    // generate y-scale from question options
    const optionValues = this.options.map((o) => o.value);
    this.scaleYOptions = scaleLinear()
      .domain([Math.min(...optionValues), Math.max(...optionValues)])
      .range([this.stageHeight - MARK_SIZE_MAX / 2, MARK_SIZE_MAX / 2]);

    this.scaleYSummedYearly = scaleLinear();
    this.updateScaleYSummedYearly(this.data);

    this.stage.attr(
      "transform",
      `translate(${this.margin.left}, ${this.margin.top + this.stageHeight})`
    );

    this.yAxisOptions = new YAxis({
      parent: this.yAxisGroup,
      fontStyles: this.styles.font,
      debug: this.debug,
    });
    this.yLegendSummedYearly = new YLegend({
      parent: this.yAxisGroup,
      fontStyles: this.styles.font,
    });

    this.groupedBars = new GroupedBars({
      parent: this.stage,
      debug: this.debug,
    });
  }

  public render(): void {
    super.render();

    // week view
    if (this.timePeriod === "week" || this.timePeriod === "month") {
      // clear the bars and y-legend
      this.groupedBars.clear();
      this.yLegendSummedYearly.clear();

      // render the dots and y-axis

      this.stage.attr(
        "transform",
        `translate(${this.margin.left}, ${this.margin.top + this.stageHeight})`
      );
      this.renderDots(this.data);
      this.yAxisOptions.render({
        ticks: this.options.map((o) => ({
          label: o.translations,
          value: o.value,
        })),
        language: this.language,
        scaleY: this.scaleYOptions,
        stageHeight: this.stageHeight,
      });
    }

    // year view
    if (this.timePeriod === "year") {
      this.updateScaleYSummedYearly(this.data);
      // clear the dots and y-axis
      this.clearDots();
      this.yAxisOptions.clear();

      // render the bars
      this.stage.attr(
        "transform",
        `translate(${this.margin.left}, ${this.margin.top + this.stageHeight})`
      );
      this.groupedBars.render({
        data: this.data,
        scaleX: this.scaleX,
        scaleY: this.scaleYSummedYearly,
        markWidth: this.markWidth,
        colorMap: this.styles.color.scale,
      });
      this.yLegendSummedYearly.render({
        ticks: this.options.map((o) => ({
          label: o.translations,
          color: this.styles.color.scale.get(o.value)!,
        })),
        language: this.language,
        stageHeight: this.stageHeight,
      });
    }
  }

  public updateOptions(options: OptionInteger[]) {
    this.options = options;
    this.render();
  }

  private updateScaleYSummedYearly(data: ConsumableDataForGroupCharts[]) {
    const values = data.reduce((acc, d) => {
      if (d.values) {
        const nonNullValues = d.values
          .filter((v) => v.yScaleValue !== null)
          .map((v) => v.yScaleValue) as number[];
        acc.push(...nonNullValues);
      }

      return acc;
    }, [] as number[]);

    this.scaleYSummedYearly = scaleLinear()
      .domain([0, Math.max(...values)])
      .range([0, this.stageHeight]);
  }

  private renderDots(data: ConsumableDataForGroupCharts[]): void {
    const { color } = this.styles;

    const circleRadius = this.markWidth / 2;

    // separate data with values from data without values
    const nonNullValueData = data.filter((d) => d.values !== null);
    const nullValueData = data.filter((d) => d.values === null);

    // draw group to hold all circles
    const stage = this.svg.select(`.${GRAPH_CLASSNAMES.STAGE}`);

    const dotsEmpty = stage
      .selectAll(`.${GRAPH_CLASSNAMES.CIRCLE_GROUP_EMPTY}`)
      .data(nullValueData)
      .join("g")
      .classed(GRAPH_CLASSNAMES.CIRCLE_GROUP_EMPTY, true)
      .attr("transform", (d) => `translate(${this.scaleX(d.date)}, 0)`);

    // draw a circle for each data point WITHOUT a values property
    dotsEmpty
      .selectAll(`.${GRAPH_CLASSNAMES.CIRCLE_EMPTY}`)
      .data(() => this.options.map((o) => ({ value: o.value })))
      .join(
        (enter) => enter.append("circle"), // append on enter
        (update) => update, // update (no change)
        (exit) => exit.remove() // remove on exit
      )
      .classed(GRAPH_CLASSNAMES.CIRCLE_EMPTY, true)
      .attr("cx", 0)
      .attr("cy", (d) => -this.scaleYOptions(d.value))
      .attr("r", circleRadius)
      .attr("fill", "white")
      .attr("stroke", color.inactive)
      .attr("stroke-width", 1);

    const dotsData = stage
      .selectAll(`.${GRAPH_CLASSNAMES.CIRCLE_GROUP_DATA}`)
      .data(nonNullValueData)
      .join("g")
      .classed(GRAPH_CLASSNAMES.CIRCLE_GROUP_DATA, true)
      .attr("transform", (d) => `translate(${this.scaleX(d.date)}, 0)`);

    // draw a circle for each data point WITH a value
    dotsData
      .selectAll(`.${GRAPH_CLASSNAMES.CIRCLE_DATA}`)
      .data(
        (d) =>
          d.values?.map((v) => ({
            value: v.yScaleValue!,
            color: v.option != null
              ? this.styles.color.scale.get(v.option)!
              : this.styles.color.inactive,
          })) ?? []
      )
      .join(
        (enter) => enter.append("circle"), // append on enter
        (update) => update, // update (no change)
        (exit) => exit.remove() // remove on exit
      )
      .classed(GRAPH_CLASSNAMES.CIRCLE_DATA, true)
      .attr("cx", 0)
      .attr("cy", (d) => -this.scaleYOptions(d.value))
      .attr("r", circleRadius)
      .attr("fill", (d) => {
        if (d.color === null) {
          return "red";
        } else {
          return d.color;
        }
      });
  }

  private clearDots(): void {
    const stage = this.svg.select(`.${GRAPH_CLASSNAMES.STAGE}`);
    stage.selectAll(`.${GRAPH_CLASSNAMES.CIRCLE_GROUP_DATA}`).remove();
    stage.selectAll(`.${GRAPH_CLASSNAMES.CIRCLE_GROUP_EMPTY}`).remove();
  }
}
