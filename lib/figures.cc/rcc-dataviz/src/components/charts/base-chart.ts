import { create } from "d3-selection";
import type { Selection } from "d3-selection";
import { scaleTime } from "d3-scale";
import type { ScaleTime } from "d3-scale";
import XAxis from "../chart-elements/x-axis";

import {
  ASPECT_WIDESCREEN,
  COLORS_DEFAULT,
  GRAPH_CLASSNAMES,
  GAP_SIZE_SCALE,
  MARK_SIZE_MIN,
  MARK_SIZE_MAX,
  FONT_DEFAULT,
  ChartStyleProps,
  COLORMAP_DEFAULT,
  LocaleDefinitions,
} from "../../models";
import type {
  TimePeriod,
  Margin,
  ConsumableDataBase,
  TickDataScaleX,
  BaseChartConfig,
} from "../../models";
import { calculateMarkWidth, localeEN } from "../../util";

/**
 * @fileoverview The base chart class that all other charts extend.
 *
 * The base chart creates the containing svg element (.chart) as well
 * as groups for the y-axis/y-legend (.y-axis), for the x-axis (.x-axis)
 * and for the visual elements (.stage, for bars, bar groups and circles).
 *
 * The y-axis and stage groups are empty, while the .x-axis group always
 * shows the x-axis for all charts.
 *
 * The stage width and height are calculated based on the available width
 * and height minus the margins. The margins are for elements that need to
 * be shown outside of the stage, e.g. headlines, labels, the y-axis or the x-axis.
 *
 *      .CHART
 *     ┌──────────────────────────────────────────────────────┐
 *     │ .Y-AXIS          .STAGE                              │
 *     │┌──────────────┐ ┌───────────────────────────────────┐│
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     ││              │ │                                   ││
 *     │●──────────────┘ ●───────────────────────────────────┘│
 *     │                  .X-AXIS (always shown)              │
 *     │                  ●──────────────────────────────────┐│
 *     │                  │ Di   Mi   Do   Fr   Sa   So   Mo ││
 *     │                  └──────────────────────────────────┘│
 *     └──────────────────────────────────────────────────────┘
 *
 *      ● = group origin
 */

export default class BaseChart<
  CONSUMABLE_DATA_TYPE extends ConsumableDataBase
> {
  public svg: Selection<SVGSVGElement, undefined, null, undefined>;
  protected stage: Selection<SVGGElement, undefined, null, undefined>;
  protected xAxisGroup: Selection<SVGGElement, undefined, null, undefined>;
  protected yAxisGroup: Selection<SVGGElement, undefined, null, undefined>;
  protected debugLayer: Selection<SVGGElement, undefined, null, undefined>;
  protected debug: boolean;
  protected data: CONSUMABLE_DATA_TYPE[];
  protected width: number;
  protected height: number;
  protected margin: Margin;
  protected stageWidth: number;
  protected stageHeight: number;
  protected scaleX: ScaleTime<number, number>;
  protected noteCallback: (event: Event, tickData: TickDataScaleX) => void;
  protected language: string;
  protected timePeriod: TimePeriod;
  protected xAxis: XAxis;
  protected markWidth: number = 0;
  protected styles: ChartStyleProps;
  protected locales: LocaleDefinitions;

  constructor({
    data,
    startDate,
    endDate,
    timePeriod,
    language = "de",
    onTick: noteCallback,
    dimensions = {
      width: 300,
      margin: { top: 10, right: 10, bottom: 20, left: 30 },
    },
    styles,
    debug = false,
    locales = { en: localeEN },
  }: BaseChartConfig<CONSUMABLE_DATA_TYPE>) {
    this.data = data;
    this.timePeriod = timePeriod;
    this.language = language;
    this.noteCallback = noteCallback;
    this.debug = debug;

    this.locales = locales;

    const {
      width,
      height = width / ASPECT_WIDESCREEN,
      margin: m,
    } = dimensions as {
      width: number;
      height: number;
      margin: Margin;
    };

    this.width = width;
    this.height = height;
    this.margin = m;
    this.stageWidth = width - m.left - m.right;
    this.stageHeight = height - m.top - m.bottom;

    // set the styles if not provided
    const _styles: ChartStyleProps = {
      color: {
        primary: styles?.color?.primary || COLORS_DEFAULT.greenDefault,
        inactive: styles?.color?.inactive || COLORS_DEFAULT.greyBack,
        scale: (styles?.color?.scale || COLORMAP_DEFAULT) as Map<
          number,
          string
        >,
      },
      note: {
        backgroundColor:
          styles?.note?.backgroundColor || COLORS_DEFAULT.purpleDisabled,
        font: {
          family: styles?.note?.font?.family || FONT_DEFAULT.family,
          weight: styles?.note?.font?.weight || FONT_DEFAULT.weigth,
          size: styles?.note?.font?.size || FONT_DEFAULT.size,
          color: styles?.note?.font?.color || COLORS_DEFAULT.textLight,
          lineHeight: styles?.note?.font?.lineHeight || FONT_DEFAULT.lineHeight,
        },
      },
      font: {
        family: styles?.font?.family || FONT_DEFAULT.family,
        weight: styles?.font?.weight || FONT_DEFAULT.weigth,
        size: styles?.font?.size || FONT_DEFAULT.size,
        color: styles?.font?.color || COLORS_DEFAULT.textLight,
        lineHeight: styles?.font?.lineHeight || FONT_DEFAULT.lineHeight,
      },
    };
    this.styles = _styles;

    // create the main svg container
    this.svg = create("svg")
      .classed(GRAPH_CLASSNAMES.CHART, true)
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [0, 0, width, height])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;");

    // create the group for the x-axis
    this.xAxisGroup = this.svg
      .append("g")
      .classed(GRAPH_CLASSNAMES.AXIS_X, true)
      .attr("transform", `translate(${m.left}, ${m.top + this.stageHeight})`);

    this.xAxis = new XAxis({
      parent: this.xAxisGroup,
      noteCallback: this.noteCallback,
      debug: this.debug,
      styles: this.styles,
      locales: this.locales,
    });

    // create the group for the y-axis
    this.yAxisGroup = this.svg
      .append("g")
      .classed(GRAPH_CLASSNAMES.AXIS_Y, true)
      .attr("transform", `translate(0, ${m.top + this.stageHeight})`);

    // create the stage for the bars / dots / bargroups
    this.stage = this.svg
      .append("g")
      .classed(GRAPH_CLASSNAMES.STAGE, true)
      .attr("transform", `translate(${m.left}, ${m.top + this.stageHeight})`);

    this.scaleX = scaleTime<number, number>()
      .domain([startDate, endDate])
      .range([0, this.stageWidth]);

    this.updateScaleXDomain();
    this.updateMarkWidth();
    this.updateScaleXRange();

    this.debugLayer = this.svg.append("g").classed("debug", true);
    if (debug) {
      this.debugLayer
        .append("rect")
        .classed("debug-bounds", true)
        .attr("width", this.width)
        .attr("height", this.height)
        .attr("fill", "none")
        .attr("stroke", "red")
        .attr("stroke-width", 1);

      this.debugLayer
        .append("rect")
        .classed("debug-stage", true)
        .attr("width", this.stageWidth)
        .attr("height", this.stageHeight)
        .attr("fill", "none")
        .attr("stroke", "blue")
        .attr("stroke-width", 1)
        .attr("transform", `translate(${m.left}, ${m.top})`);
    }
  }

  public render(): void {
    // render the x-axis for every graph extending this class
    this.xAxis.render({
      ticks: this.data.map((d) => {
        return {
          date: d.date,
          note: d.note,
        };
      }),
      scaleX: this.scaleX,
      timePeriod: this.timePeriod,
      language: this.language,
    });
  }
  public updateData(data: CONSUMABLE_DATA_TYPE[]) {
    this.data = data;
    this.updateScaleXDomain();
    this.updateMarkWidth();
    this.updateScaleXRange();

    this.render();
  }
  public updateLanguage(language: string) {
    this.language = language;
    this.render();
  }
  public updateTimePeriod(timePeriod: TimePeriod) {
    this.timePeriod = timePeriod;
    // this.updateScaleX();
    this.render();
  }
  public resize(newHeight: number) {
    // update height and stageHeight
    this.height = newHeight;
    this.stageHeight = this.height - this.margin.top - this.margin.bottom;

    // update svg
    this.svg
      .attr("height", this.height)
      .attr("viewBox", [0, 0, this.width, this.height]);

    // update x-axis
    this.svg
      .select(`.${GRAPH_CLASSNAMES.AXIS_X}`)
      .attr(
        "transform",
        `translate(${this.margin.left}, ${this.margin.top + this.stageHeight})`
      );

    // update stage
    this.svg
      .select(`.${GRAPH_CLASSNAMES.STAGE}`)
      .attr(
        "transform",
        `translate(${this.margin.left}, ${this.margin.top + this.stageHeight})`
      );

    // update debug views
    if (this.debug) {
      this.debugLayer
        .select(".debug-bounds")
        .attr("width", this.width)
        .attr("height", this.height);

      this.debugLayer
        .select(".debug-stage")
        .attr("width", this.stageWidth)
        .attr("height", this.stageHeight)
        .attr(
          "transform",
          `translate(${this.margin.left}, ${this.margin.top})`
        );
    }
  }

  protected updateScaleXDomain() {
    const dateTimes = this.data.map((d) => d.date.getTime());
    const newMinDate = new Date(Math.min(...dateTimes));
    const newMaxDate = new Date(Math.max(...dateTimes));
    this.scaleX.domain([newMinDate, newMaxDate]);
  }
  protected updateMarkWidth() {
    this.markWidth = calculateMarkWidth(
      this.stageWidth,
      this.data.length,
      GAP_SIZE_SCALE,
      { minWidth: MARK_SIZE_MIN, maxWidth: MARK_SIZE_MAX }
    );
  }
  protected updateScaleXRange() {
    this.scaleX.range([
      this.markWidth / 2,
      this.stageWidth - this.markWidth / 2,
    ]);
  }
}
