import { ScaleLinear, scaleLinear } from "d3-scale";
import LegendY from "../../chart-elements/y-legend/index";
import GroupedBars from "../../chart-elements/grouped-bars/index";
import BaseChartWithOptions from "../base-chart-with-options";

import {
  isArrayOfConsumableDataForYesNo,
  isArrayOfConsumableDataForGroupCharts,
} from "../../../util/index";
import {
  ConsumableDataForYesNo,
  ConsumableDataForGroupCharts,
  GRAPH_CLASSNAMES,
  ASPECT_COMPRESSED,
  ASPECT_WIDESCREEN,
  MARK_SIZE_MAX,
  OptionBoolean,
  BaseChartWithOptionsConfig,
} from "../../../models/index";
import type { TimePeriod } from "../../../models/index";

interface YesNoChartConfig
  extends BaseChartWithOptionsConfig<
    ConsumableDataForYesNo | ConsumableDataForGroupCharts,
    OptionBoolean
  > {
  ratio?: number;
}
export default class YesNoChart extends BaseChartWithOptions<
  ConsumableDataForYesNo | ConsumableDataForGroupCharts,
  OptionBoolean
> {
  private scaleY: ScaleLinear<number, number>;
  private legendY: LegendY;
  private groupedBars: GroupedBars;
  private yesNoColorMap: Map<boolean, string>;

  constructor(props: YesNoChartConfig) {
    super(props);

    // this chart changes the aspect ratio based on the time period. It is narrow for week and month views, and wide for year view.
    const {
      ratio = this.timePeriod === "year"
        ? ASPECT_WIDESCREEN
        : ASPECT_COMPRESSED,
    } = props;

    // size the graph initially based on the time period
    this.resize(this.width / ratio);

    // initialize the y scale (for year view bar heights)
    this.scaleY = scaleLinear();

    // initialize the y legend (for year view color legend)
    this.legendY = new LegendY({
      parent: this.yAxisGroup,
      fontStyles: this.styles.font,
    });

    this.groupedBars = new GroupedBars({
      parent: this.stage,
      debug: this.debug,
    });

    // initialize the color map for yes/no values
    this.yesNoColorMap = new Map([
      [true, this.styles.color.primary],
      [false, this.styles.color.inactive],
    ]);

    // if the data is an array of multiple integers (for bar groups), update the y scale based on the data
    if (isArrayOfConsumableDataForGroupCharts(this.data)) {
      this.updateScaleY(this.data);
    }
  }

  // override the base graph render method
  public render(): void {
    super.render();

    if (isArrayOfConsumableDataForYesNo(this.data)) {
      // remove the groups of bars and the y-legend
      this.groupedBars.clear();
      this.legendY.clear();

      // render the dots
      this.renderDots(this.data);
    }

    if (isArrayOfConsumableDataForGroupCharts(this.data)) {
      // remove the circles
      this.svg
        .select(`.${GRAPH_CLASSNAMES.STAGE}`)
        .selectAll("circle")
        .remove();

      // render the bar groups and y-legend
      this.groupedBars.render({
        data: this.data,
        scaleX: this.scaleX,
        scaleY: this.scaleY,
        markWidth: this.markWidth,
        colorMap: this.yesNoColorMap,
      });
      this.legendY.render({
        ticks: this.options.map((o) => ({
          color: this.yesNoColorMap.get(o.value)!,
          label: o.translations,
        })),
        stageHeight: this.stageHeight,
        language: this.language,
      });
    }
  }

  // override the base graph resize method
  public updateTimePeriod(timePeriod: TimePeriod) {
    switch (timePeriod) {
      case "year":
        this.resize(this.width / ASPECT_WIDESCREEN);
        if (isArrayOfConsumableDataForGroupCharts(this.data)) {
          this.updateScaleY(this.data);
        }
        break;

      default:
        this.resize(this.width / ASPECT_COMPRESSED); // resize to compressed aspect ratio
        break;
    }
    super.updateTimePeriod(timePeriod);
  }

  private updateScaleY(data: ConsumableDataForGroupCharts[]) {
    const values = data.reduce((acc, d) => {
      if (d.values) {
        const nonNullValues = d.values
          .filter((v) => v.yScaleValue !== null)
          .map((v) => v.yScaleValue) as number[];
        acc.push(...nonNullValues);
      }

      return acc;
    }, [] as number[]);

    this.scaleY = scaleLinear()
      .domain([0, Math.max(...values)])
      .range([0, this.stageHeight]);
  }

  private renderDots(data: ConsumableDataForYesNo[]) {
    const { color } = this.styles;

    const circleRadius = this.markWidth / 2;

    // separate data with values from data without values
    const nonNullValueData = data.filter((d) => d.value !== null);
    const nullValueData = data.filter((d) => d.value === null);

    // draw group to hold all circles
    const stage = this.svg.select(`.${GRAPH_CLASSNAMES.STAGE}`);

    const circlesGroup = stage
      .selectAll(`.${GRAPH_CLASSNAMES.CIRCLES_GROUP}`)
      .data([null])
      .join("g")
      .classed(GRAPH_CLASSNAMES.CIRCLES_GROUP, true)
      .attr("transform", `translate(0, ${-MARK_SIZE_MAX / 2})`);

    // draw a circle for each data point WITHOUT a value
    circlesGroup
      .selectAll(`.${GRAPH_CLASSNAMES.CIRCLE_EMPTY}`)
      .data(nullValueData)
      .join(
        (enter) => enter.append("circle"), // append on enter
        (update) => update, // update (no change)
        (exit) => exit.remove() // remove on exit
      )
      .classed(GRAPH_CLASSNAMES.CIRCLE_EMPTY, true)
      .attr("cx", (d) => this.scaleX(d.date))
      .attr("cy", 0)
      .attr("r", circleRadius)
      .attr("fill", "white")
      .attr("stroke", color.inactive)
      .attr("stroke-width", 1);

    // draw a circle for each data point WITH a value
    circlesGroup
      .selectAll(`.${GRAPH_CLASSNAMES.CIRCLE_DATA}`)
      .data(nonNullValueData)
      .join(
        (enter) => enter.append("circle"), // append on enter
        (update) => update, // update (no change)
        (exit) => exit.remove() // remove on exit
      )
      .classed(GRAPH_CLASSNAMES.CIRCLE_DATA, true)
      .attr("cx", (d) => this.scaleX(d.date))
      .attr("cy", 0)
      .attr("r", circleRadius)
      .attr("fill", (d) => (d.value === true ? color.primary : color.inactive));
  }
}
