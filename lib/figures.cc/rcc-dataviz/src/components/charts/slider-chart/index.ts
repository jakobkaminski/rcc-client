import { ScaleLinear, scaleLinear } from "d3-scale";
import BaseChart from "../base-chart";
import YAxis from "../../chart-elements/y-axis";
import SingleBars from "../../chart-elements/single-bars";

import { AXIS_Y_VERTICAL_PADDING, TickDataScaleY } from "../../../models";
import type {
  ConsumableDataForSingleBars,
  BaseChartConfig,
} from "../../../models";
import { stepsBetweenMinMax } from "../../../util";

/**
 * @fileoverview The slider chart
 *     ┌──────────────────────────────────────────────────────┐
 *     │ 100                ◠    ◠    ◠    ◠    ◠    ◠    ◠   │
 *     │                   |░|  |░|  |░|  | |  | |  |█|  |░|  │
 *     │                   |░|  |░|  |░|  | |  | |  |█|  |░|  │
 *     │ 75                |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ 50                |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |░|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ 25                |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │                   |█|  |░|  |█|  | |  | |  |█|  |░|  │
 *     │ 0                 |█|  |█|  |█|  | |  | |  |█|  |█|  │
 *     │                    ◡    ◡    ◡    ◡    ◡    ◡    ◡   │
 *     │                   Di   Mi   Do   Fr   Sa   So   Mo   │
 *     └──────────────────────────────────────────────────────┘
 */
interface SliderChartConfig
  extends BaseChartConfig<ConsumableDataForSingleBars> {
  min: number;
  max: number;
}
export default class SliderChart extends BaseChart<ConsumableDataForSingleBars> {
  private scaleY: ScaleLinear<number, number>;
  private scaleYPadded: ScaleLinear<number, number>;
  private yAxis: YAxis;
  private singleBars: SingleBars;
  private ticks: TickDataScaleY[];

  constructor(props: SliderChartConfig) {
    super(props);
    const { min, max } = props;
    const numbersToShow = stepsBetweenMinMax(min, max, 3);

    this.ticks = numbersToShow.map((n) => {
      const label: { [key: string]: string } = {};
      Object.keys(this.locales).forEach((key) => {
        label[key] = n.toString();
      });
      return {
        value: n,
        label,
      };
    });

    // initialize the y-scale (used for the y-axis and the bars)
    this.scaleY = scaleLinear().domain([min, max]).range([0, this.stageHeight]);

    this.scaleYPadded = this.scaleY
      .copy()
      .range([
        AXIS_Y_VERTICAL_PADDING,
        this.stageHeight - AXIS_Y_VERTICAL_PADDING,
      ]);

    // initialize the y-axis (labels for the y-scale)
    this.yAxis = new YAxis({
      parent: this.yAxisGroup,
      fontStyles: this.styles.font,
      debug: this.debug,
    });

    this.singleBars = new SingleBars({
      parent: this.stage,
      debug: this.debug,
    });
  }

  public render(): void {
    super.render();

    this.yAxis.render({
      ticks: this.ticks,
      language: this.language,
      scaleY: this.scaleYPadded,
      stageHeight: this.stageHeight,
    });

    this.singleBars.render({
      data: this.data,
      scaleX: this.scaleX,
      scaleY: this.scaleY,
      markWidth: this.markWidth,
      color: this.styles.color,
    });
  }

  public updateData(data: ConsumableDataForSingleBars[]): void {
    super.updateData(data);
    this.render();
  }
}
