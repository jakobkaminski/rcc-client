import { OptionBasics } from "../../models";
import type {
  ConsumableDataBase,
  BaseChartWithOptionsConfig,
} from "../../models";
import BaseChart from "./base-chart";

/**
 * @fileoverview This class extends the base chart to also take options as a property. A lot of
 * charts use the options to create the y-scale, so it makes sense to create a reusable class
 * for them.
 */

export default class BaseChartWithOptions<
  CONSUMABLE_DATA_TYPE extends ConsumableDataBase,
  OPTIONS_TYPE extends OptionBasics
> extends BaseChart<CONSUMABLE_DATA_TYPE> {
  protected options: OPTIONS_TYPE[];

  constructor(
    props: BaseChartWithOptionsConfig<CONSUMABLE_DATA_TYPE, OPTIONS_TYPE>
  ) {
    super(props);
    this.options = props.options;
  }
}
