import type { ScaleTime, ScaleLinear } from "d3-scale";
import type { Selection } from "d3-selection";
import { ColorProps, GRAPH_CLASSNAMES } from "../../../models";
import type { ConsumableDataForSingleBars } from "../../../models";

interface SingleBarsProps {
  parent: Selection<SVGGElement, undefined, null, undefined>;
  debug?: boolean;
}
interface SingleBarsRenderProps {
  data: ConsumableDataForSingleBars[];
  scaleX: ScaleTime<number, number>;
  scaleY: ScaleLinear<number, number>;
  markWidth: number;
  color: ColorProps;
}
export default class SingleBars {
  private parent: Selection<SVGGElement, undefined, null, undefined>;
  private uniqueId: string;

  constructor(props: SingleBarsProps) {
    this.parent = props.parent;

    // generate a unique id for this collection of single bars used by the clip path
    this.uniqueId = Math.random().toString(16).slice(2);
  }

  public render({
    data,
    scaleX,
    scaleY,
    markWidth,
    color,
  }: SingleBarsRenderProps) {
    const scaledBarWidth = markWidth;

    // separate data with values from data without values
    const nonNullValueData = data.filter((d) => d.value !== null) as {
      value: number;
      date: Date;
      note?: string;
    }[];
    const nullValueData = data.filter((d) => d.value === null) as {
      value: null;
      date: Date;
      note?: string;
    }[];

    // draw a bar for each data point without a value
    this.parent
      .selectAll(`.${GRAPH_CLASSNAMES.BAR_EMPTY}`)
      .data(nullValueData)
      .join("rect")
      .classed(GRAPH_CLASSNAMES.BAR_EMPTY, true)
      .attr("x", (d) => scaleX(d.date) - scaledBarWidth / 2)
      .attr("y", -Math.max(...scaleY.range()))
      .attr("width", scaledBarWidth)
      .attr("height", Math.max(...scaleY.range()))
      .attr("rx", scaledBarWidth / 2)
      .attr("ry", scaledBarWidth / 2)
      .attr("fill", "white")
      .attr("stroke", color.inactive)
      .attr("stroke-width", 1);

    // draw a group for each data point with a value
    const barsWithData = this.parent
      .selectAll(`.${GRAPH_CLASSNAMES.BAR_DATA}`)
      .data(nonNullValueData)
      .join("g")
      .classed(GRAPH_CLASSNAMES.BAR_DATA, true)
      .attr("transform", (d) => `translate(${scaleX(d.date)}, 0)`);

    // background bars
    barsWithData
      .selectAll(`.${GRAPH_CLASSNAMES.BAR_DATA_BACKGROUND}`)
      .data((d) => [d])
      .join("rect")
      .classed(GRAPH_CLASSNAMES.BAR_DATA_BACKGROUND, true)
      .attr("x", -scaledBarWidth / 2)
      .attr("y", () => {
        return -Math.max(...scaleY.range());
      })
      .attr("width", scaledBarWidth)
      .attr("height", () => {
        return Math.max(...scaleY.range());
      })
      .attr("rx", scaledBarWidth / 2)
      .attr("ry", scaledBarWidth / 2)
      .attr("fill", color.inactive);

    // full range bar used as mask for the data bar
    barsWithData
      .selectAll(`.${GRAPH_CLASSNAMES.BAR_DATA_MASK}`)
      .data((d, i) => {
        // add an index to the data so that the clip path can be uniquely identified
        return [{ ...d, index: i }];
      })
      .join("clipPath")
      .attr("id", (d) => {
        return `clip-${this.uniqueId}-${d.index}`;
      })
      .append("rect")
      .classed(GRAPH_CLASSNAMES.BAR_DATA_MASK, true)
      .attr("x", -scaledBarWidth / 2)
      .attr("y", () => {
        return -Math.max(...scaleY.range());
      })
      .attr("width", scaledBarWidth)
      .attr("height", () => {
        return Math.max(...scaleY.range());
      })
      .attr("rx", scaledBarWidth / 2)
      .attr("ry", scaledBarWidth / 2)
      .attr("fill", color.inactive);

    // bars displaying the data
    barsWithData
      .selectAll(`.${GRAPH_CLASSNAMES.BAR_DATA_VALUE}`)
      .data((d, i) => {
        // add an index to the data so that the clip path can be uniquely identified
        return [{ ...d, index: i }];
      })
      .join("rect")
      .classed(GRAPH_CLASSNAMES.BAR_DATA_VALUE, true)
      .attr("x", -scaledBarWidth / 2)
      .attr("y", (d) => {
        return -scaleY(d.value);
      })
      .attr("width", scaledBarWidth)
      .attr("height", (d) => {
        // if the bar is shorter than the width of the bar, make it the width of the bar
        // this is to prevent the bar from being squished. The overlap is handled by the clip path.
        const barHeightDesignated = scaleY(d.value);

        const barHeightAdjusted =
          barHeightDesignated < scaledBarWidth
            ? scaledBarWidth
            : barHeightDesignated;

        return barHeightAdjusted;
      })
      .attr("rx", scaledBarWidth / 2)
      .attr("ry", scaledBarWidth / 2)
      .attr("fill", color.primary)
      .attr("clip-path", (d) => `url(#clip-${this.uniqueId}-${d.index})`);
  }

  public clear() {
    this.parent.selectAll(`.${GRAPH_CLASSNAMES.BAR_EMPTY}`).remove();
    this.parent.selectAll(`.${GRAPH_CLASSNAMES.BAR_DATA}`).remove();
  }
}
