import type { Selection } from "d3-selection";
import {
  GRAPH_CLASSNAMES,
  LEGEND_Y_DOT_SIZE,
  LEGEND_Y_DOT_MARGIN_BOTTOM,
  FontStyleProps,
} from "../../../models";
import type { TickDataLegendY } from "../../../models";

interface LegendYProps {
  parent: Selection<SVGGElement, undefined, null, undefined>;
  fontStyles: FontStyleProps;
}
interface LegendYRenderProps {
  ticks: TickDataLegendY[];
  stageHeight: number;
  language: string;
}
export default class LegendY {
  private parent: Selection<SVGGElement, undefined, null, undefined>;
  private fontStyles: FontStyleProps;

  constructor({ parent, fontStyles }: LegendYProps) {
    this.parent = parent;
    this.fontStyles = fontStyles;
  }

  public render({ ticks, stageHeight, language }: LegendYRenderProps) {
    const yStep = stageHeight / 4;
    const positionedTicks = ticks.reverse().map((tick, tickIndex) => {
      return {
        ...tick,
        y: yStep * tickIndex,
      };
    });

    const LegendYTicks = this.parent
      .selectAll(`.${GRAPH_CLASSNAMES.LEGEND_Y_TICK}`)
      .data(positionedTicks)
      .join(
        (enter) => enter.append("g"), // append on enter
        (update) => update, // update (no change)
        (exit) => exit.remove() // remove on exit
      )
      .classed(GRAPH_CLASSNAMES.LEGEND_Y_TICK, true)
      .attr("transform", (d) => `translate(0, ${-d.y})`);

    // dots
    LegendYTicks.selectAll("circle")
      .data((d) => [d]) // draw circle only for ticks with notes and only for week and month view
      .join(
        (enter) => enter.append("circle"), // append circle on enter
        (update) => update, // update circle on update (no change)
        (exit) => exit.remove() // remove circle on exit
      )
      .attr("cx", LEGEND_Y_DOT_SIZE / 2)
      .attr("cy", -LEGEND_Y_DOT_SIZE / 2)
      .attr("r", LEGEND_Y_DOT_SIZE / 2)
      .attr("fill", (d) => d.color);

    LegendYTicks.selectAll("text")
      .data((d) => [d])
      .join(
        (enter) => enter.append("text"), // append text on enter
        (update) => update, // update text on update (no change)
        (exit) => exit.remove() // remove text on exit
      )
      .classed("label", true)
      .attr("font-family", this.fontStyles.family)
      .attr("font-size", this.fontStyles.size)
      .attr("font-weight", this.fontStyles.weight)
      .attr("fill", this.fontStyles.color)
      .attr("x", 0)
      .attr("y", -LEGEND_Y_DOT_SIZE - LEGEND_Y_DOT_MARGIN_BOTTOM)
      .attr("text-anchor", "start")
      .text((d) => {
        return d.label[language] || d.label["en"] || d.label["de"];
      });
  }

  public clear() {
    this.parent.selectAll(`.${GRAPH_CLASSNAMES.LEGEND_Y_TICK}`).remove();
  }
}
