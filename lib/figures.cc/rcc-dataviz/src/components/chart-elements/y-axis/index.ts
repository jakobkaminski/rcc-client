import type { ScaleLinear } from "d3-scale";
import type { Selection } from "d3-selection";
import { GRAPH_CLASSNAMES } from "../../../models";
import type { TickDataScaleY, FontStyleProps } from "../../../models";
import { splitIntoLines } from "../../../util";

interface YAxisProps {
  parent: Selection<SVGGElement, undefined, null, undefined>;
  fontStyles: FontStyleProps;
  debug?: boolean;
}
interface YAxisRenderProps {
  ticks: TickDataScaleY[];
  language: string;
  scaleY: ScaleLinear<number, number>;
  stageHeight: number;
}
export default class YAxis {
  private parent: Selection<SVGGElement, undefined, null, undefined>;
  private fontStyles: FontStyleProps;
  private debug: boolean;

  constructor({ parent, fontStyles, debug = false }: YAxisProps) {
    this.parent = parent;
    this.fontStyles = fontStyles;
    this.debug = debug;
  }

  public render({ ticks, language, scaleY, stageHeight }: YAxisRenderProps) {
    const rangeDiff = scaleY.range()[0] - scaleY.range()[1];
    const padding = (stageHeight - rangeDiff) / 2;

    if (this.debug) {
      this.parent
        .selectAll(`.axis-y-debug`)
        .data([null])
        .join("rect")
        .classed("axis-y-debug", true)
        .attr("x", 3)
        .attr("y", -stageHeight)
        .attr("width", 59)
        .attr("height", stageHeight)
        .attr("fill", "red")
        .attr("opacity", 0.05);
      this.parent
        .selectAll(`.axis-y-debug-rect`)
        .data([null])
        .join("rect")
        .classed("axis-y-debug-rect", true)
        .attr("x", 3)
        .attr("y", -stageHeight + padding)
        .attr("width", 59)
        .attr("height", stageHeight - padding * 2)
        .attr("fill", "none")
        .attr("stroke", "red");
    }

    const yAxisTicks = this.parent
      .selectAll(`.${GRAPH_CLASSNAMES.AXIS_Y_TICK}`)
      .data(ticks)
      .join("g")
      .classed(GRAPH_CLASSNAMES.AXIS_Y_TICK, true);

    yAxisTicks
      .selectAll(`.${GRAPH_CLASSNAMES.LABEL}`)
      .data((d) => {
        // split the label into lines of max 10 characters. Break words after 10 characters.
        const label = d.label[language] || d.label[Object.keys(d.label)[0]];
        const lines = splitIntoLines(label, 10);

        return lines.map((line, lineIndex) => ({
          label: line,
          value: d.value,
          offset: lineIndex * this.fontStyles.lineHeight,
        }));
      })
      .join(
        (enter) => enter.append("text"), // append text on enter
        (update) => update, // update text on update (no change)
        (exit) => exit.remove() // remove text on exit
      )
      .classed(GRAPH_CLASSNAMES.LABEL, true)
      .attr("x", 0)
      .attr("y", (d) => -scaleY(d.value) + d.offset)
      .attr("dy", "0.35em")
      .attr("text-anchor", "start")
      .attr("font-family", this.fontStyles.family)
      .attr("font-size", this.fontStyles.size)
      .attr("font-weight", this.fontStyles.weight)
      .attr("fill", this.fontStyles.color)
      .text((d) => d.label);

    if (this.debug) {
      yAxisTicks
        .selectAll("rect")
        .data((d) => [d])
        .join(
          (enter) => enter.append("rect"), // append rect on enter
          (update) => update, // update rect on update (no change)
          (exit) => exit.remove() // remove rect on exit
        )
        .attr("x", 0)
        .attr("y", (d) => -scaleY(d.value))
        .attr("width", 3)
        .attr("height", 1)
        .attr("fill", "blue");
    }
  }

  public clear() {
    this.parent.selectAll(`.${GRAPH_CLASSNAMES.AXIS_Y_TICK}`).remove();
  }
}
