import 	{
			isErrorFree,
			assert,
			assertProperty
		}								from '../utils'

import	{
			RccWebSocketConfig,
			assertRccWebSocketConfig
		}								from '../rcc-websocket'


export interface RccRtcConfig extends RccWebSocketConfig {
	stunServers		: string[]
}

export function assertRccRtcConfig(x:unknown ) : asserts x is RccRtcConfig {

	assertRccWebSocketConfig(x)

	assertProperty(x, 'stunServers')
	assert(Array.isArray(x.stunServers), 		'assertRccRtcConfig: .stunServers must be a string[].')

	x.stunServers.every( (url:string) => assert(typeof url === 'string', `assertRccRtcConfig: .stunServers must be a string[]; got ${typeof url}`) )

}


export function isRccRtcConfig(x : unknown) : x is RccRtcConfig {
	return isErrorFree( () => assertRccRtcConfig(x) )
}
