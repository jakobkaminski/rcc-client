import	{
			assert,
			isErrorFree
		}							from '../../utils'

import	{	Item				}	from '../item.class'
import	{
			Entry,
			EntryConfig,
			assertEntryConfig
		}							from '../entries/entry.class'

export type ReportConfig = [string|null, string|null, EntryConfig[]] // label, date, entries



export function assertReportConfig(x: unknown ): asserts x is ReportConfig {

	assert( Array.isArray(x),								'isReportConfig() config must be an array.', x)

	assert(typeof x[0] === 'string' || x[0] === null, 		'isReportConfig() [0] must be a string or null.', x)
	assert(typeof x[1] === 'string' || x[1] === null, 		'isReportConfig() [1] must be a string or null.', x)
	assert(Array.isArray(x[2]), 							'isReportConfig() [2] must be an EntryConfig[].', x)

	x[2].every( (x:unknown) => assertEntryConfig(x) )

}


export function isReportConfig(x: unknown) : x is ReportConfig {
	return isErrorFree( () => assertReportConfig(x) )
}





export class Report extends Item<ReportConfig>{

	public entries 	: Entry[]
	public date		: Date
	public label	: string

	public static acceptsAsConfig = isReportConfig

	public static from(entries: Entry[]) : Report {
		return new Report( [null, new Date().toISOString(), entries.map( entry => entry.config)] )
	}


	public constructor(config: ReportConfig){
		super(config)
	}

	public set config(config: ReportConfig){


		if(Array.isArray(config[0]))
			// legacy
			this.config = [null,null, config as any[] ]
		else {

			assertReportConfig(config)

			this.label		= config[0]
			this.date 		= new Date(config[1])
			this.entries	= Array.from(config[2]).map( (config: EntryConfig) => new Entry(config) )

			console.log(config[1], this.date)
		}
	}

	public get config(): ReportConfig{
		return [this.label, this.date.toISOString(), this.entries.map( (entry: Entry) => entry.config )]
	}

	public get questionIds(): string[]{
		const ids 			= this.entries.map( entry => entry.questionId )
		const unique_ids	= Array.from(new Set(ids))
		return unique_ids
	}

	// todo add filter functions
}
