import	{
			ScheduleConfig,
			isScheduleConfig,
			assertScheduleConfig
		}								from '../schedules'

import	{
			isErrorFree,
			has,
			assert,
			assertProperty
		}								from '../../utils'
import	{	TranslationList			}	from '../questions'

export interface SymptomCheckMetaConfig {
	label?				: string			// Some label
	defaultSchedule?	: ScheduleConfig	// default schedule, legacy: rrule
	creationDate?		: string			// Some date format
	paused?				: boolean
	reminder?			: string			// HH:MM
}

export interface QuestionRelationDto {
	id:				string 				// Question id
	schedule?:		ScheduleConfig
	category?:		string
}

/**
 * Config for extra slides that appear in the query run,
 * but are not questions/QueryControls.
 * Can be used to provide context to symptom check.
 */
export interface InfoSlideConfig {
	title?	: { translations: TranslationList }
	message	: { translations: TranslationList }
}

/**
 * Config for a custom slides order.
 * slides is an array of question IDs (strings) or {@link InfoSlideConfig}s.
 */
export interface SymptomCheckPresentationConfig {
	// string => questionId
	slides?	: (string | InfoSlideConfig)[]
}

export interface SymptomCheckConfig {
	id?				: string
	meta			: SymptomCheckMetaConfig
	questions		: (string | QuestionRelationDto)[]
	presentation?	: SymptomCheckPresentationConfig
}


// TypeGuards:

export function assertSymptomCheckMetaConfig(x: unknown): asserts x is SymptomCheckMetaConfig {
	assert(typeof x === 'object', 				'assertSymptomCheckMetaConfig() x must be an object.', x)

	if(has(x,'reminder') && x.reminder !== undefined && x.reminder !== null){
		assert(typeof x.reminder === 'string',	'assertSymptomCheckMetaConfig() .reminder must be a string if set.', x.reminder)
		assert(x.reminder.match(/\d\d:\d\d/),	'assertSymptomCheckMetaConfig() .reminder format not matching HH:MM.', x.reminder)
	}

	// TODO other properties
}

export function assertQuestionRelationDto(x: unknown) : asserts x is QuestionRelationDto {
	assert( has(x, 'id'), 								'isQuestionRelationDto(): missing .id on x', x)
	assert( has(x, 'schedule') || has(x, 'category'),	'isQuestionRelationDto(): missing .schedule or .category on x.', x)
	assert( Object.keys(x).length <= 3, 				'isQuestionRelationDto(): x as extra properties.', x)
	assert( typeof x.id === 'string',					'isQuestionRelationDto(): x.id must be a string.', x)
	if (has(x, 'schedule'))								assertScheduleConfig(x.schedule)
	assert( (has(x, 'schedule') && isScheduleConfig(x.schedule)) || !has(x, 'schedule'),
														'isQuestionRelationDto(): x.schedule must be a ScheduleConfig.', x)
	assert( (has(x, 'category') && typeof x.category === 'string') || !has(x, 'category'),
														'isQuestionRelationDto(): x.category must be a string.', x)
}

export function isQuestionRelationDto(x: unknown): x is QuestionRelationDto {
	return isErrorFree( () => assertQuestionRelationDto(x) )
}


export function assertSymptomCheckConfig(x: unknown): asserts x is SymptomCheckConfig {

	assertProperty(x, 'meta')
	assertProperty(x, 'questions')


	// TODO: that doesn't make much sense, property check meta:
	assertSymptomCheckMetaConfig( x.meta )

	assert( (x.questions instanceof Array),	'assertSymptomCheckConfig(): x.questions must be an Array.', x)

	assert( x.questions.every( (q:unknown) => typeof q === 'string' || isQuestionRelationDto(q)),
											'assertSymptomCheckConfig(): every member of x.questions must be a string or a QuestionRelationDto.', x)

	if(has(x, 'id') && x.id !== undefined && x.id !== null) assert(typeof x.id === 'string', 'assertSymptomCheckConfig(): x.id must be a string.', x)

	if(has(x, 'presentation') && x.presentation !== undefined) assertSymptomCheckPresentationConfig(x.presentation)

}


export function isSymptomCheckConfig(x: unknown) : x is SymptomCheckConfig {
	return isErrorFree( () => assertSymptomCheckConfig(x) )
}


export const defaultSymptomCheckConfig : SymptomCheckConfig = {
	meta:		{},
	questions:	new Array<string>()
}

export function assertSlideConfig(x: unknown): asserts x is InfoSlideConfig {
	assertProperty(x, 'message')
	assert(Object.keys(x).length <= 2)
}

export function isInfoSlideConfig(x: unknown) : x is InfoSlideConfig {
	return isErrorFree( () => assertSlideConfig(x) )
}

export function assertSymptomCheckPresentationConfig(x: unknown): asserts x is SymptomCheckPresentationConfig {
	assert(x instanceof Object, `assertSymptomCheckPresentationConfig() x must be an instance of Object, got '${String(x)}' instead.`)
	assert(Object.keys(x).length <= 1)
	if (has(x, 'slides')) {
		assert(Array.isArray(x.slides))
		x.slides.forEach(slide => typeof slide === 'string' || assertSlideConfig(slide))
	}
}

// TODO:  is SymptomCheckMetaConfig
