import	{
			SymptomCheckConfig,
			assertSymptomCheckConfig,
			defaultSymptomCheckConfig
		}								from '../symptom-checks'
import	{
			ReportConfig,
			assertReportConfig
		}								from '../reports'

import	{
			QuestionConfig,
			assertQuestionConfig
		}								from '../questions'

import	{
			isErrorFree,
			assert,
			assertProperty
		}								from '../../utils'


/**
 * Data used in a single session at a doctor's appointment.
 */
export type SessionConfig = {
	symptomCheck:	SymptomCheckConfig | null,
	report:			ReportConfig | null,
	questions:		QuestionConfig[]
}


/**
 * Checks if a value conforms to the SessionConfig interface. If not, throw an AssertionError.
 */
export function assertSessionConfig( x : unknown) : asserts x is SessionConfig {

	assert( x !== null, 				'assertSessionConfig() must not be null', x)
	assert( typeof x === 'object', 		'assertSessionConfig() not an object', x)

	assertProperty(x,'symptomCheck')
	assertProperty(x,'report')
	assertProperty(x,'questions')

	assert(Array.isArray(x.questions), 	'assertSessionConfig() property \'questions\' must be an array.', x)

	x.symptomCheck 	=== null || assertSymptomCheckConfig(x.symptomCheck),

	x.report 		=== null || assertReportConfig(x.report)

	x.questions.every(assertQuestionConfig)
}

/**
 * Checks if a value conforms to the SessionConfig interface.
 */
export function isSessionConfig(x :unknown): boolean {
	return isErrorFree( () => assertSessionConfig(x) )
}


export const defaultSessionConfig: SessionConfig  = {
	symptomCheck: 	defaultSymptomCheckConfig,
	report:			null,
	questions:		[]
}

