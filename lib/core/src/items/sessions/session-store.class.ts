import	{
			adHocId,
		}							from '../../utils'

import	{
			ItemStore,
			ItemStorage
		}							from '../item-store.class'

import	{
			Session,
		}							from './session.class'


function identifyItemBy(item: Session): string {

	if(item.id) return item.id

	return item.id = adHocId()

}

export class SessionStore extends ItemStore<Session> {


	public constructor(
		storage?: ItemStorage<Session>,
	){

		super({
			itemClass: 		Session,
			identifyItemBy,
			storage,
		})

	}


}
