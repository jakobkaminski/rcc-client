import	{	BehaviorSubject				}	from 'rxjs'
import	{	distinctUntilChanged		}	from 'rxjs/operators'


export interface DayConfig {
	[index:string]: number
}



export interface TimeSlotConfig {
	key:		string,
	index:		number,
	previous:	number,
	next:		number,
	start:		{day:number, hour:number},
	end:		{day:number, hour:number},
	duration:	number,
	spansDays:	boolean,
	spansWeeks:	boolean
}






export class TimeOfDay{

	protected configSubject		= 	new BehaviorSubject<DayConfig[]>(null)
	protected timeSlotSubject	= 	new BehaviorSubject<string>(null)

	protected timeout			: 	any

	public 	timeSlotConfigs		: 	TimeSlotConfig[] = []
	public	configChange$		= 	this.configSubject.asObservable()
	public 	changes$			= 	this.timeSlotSubject.asObservable()
									.pipe( distinctUntilChanged() )



	public constructor(
		private config: DayConfig[]
	){
		if(config.length !== 7) throw new Error('TimeOfDay.constructor(): config requires exactly 7 items.')

		this.updateTimeSlots()

		// TODO: maybe separate:
		try{
			document && document.addEventListener('visibilitychange', () => document.visibilityState === 'visible' && this.trackChanges() )
		} catch(e) {
			console.warn('TimeOfDay.constructor: '+e)
		}

	}


	protected trackChanges(): void {

		this.timeSlotSubject.next(this.current.key)

		clearTimeout(this.timeout)

		this.timeout = 	setTimeout(
							() => this.trackChanges(),
							this.current.endDate.getTime() - new Date().getTime()
						)

	}


	protected updateTimeSlots(): void {

		this.timeSlotConfigs.length = 0


		const week	= 	this.config.map( (day_config: any, index:number) => Object.keys(day_config)
									.map( key => ({
										key,
										start: {
											day:	index,
											hour:	day_config[key]
										}
									}) ))
						.flat()
						.sort( (a, b) => {

							if(a.start.day  > b.start.day)		return  1
							if(a.start.day  < b.start.day)		return -1
							if(a.start.hour > b.start.hour)		return  1
							if(a.start.hour < b.start.hour)		return -1

							return 0
						})
						.map( ({ key, start }, index, array) => {

							const previous		=	( array.length + index -1 ) % array.length
							const next			= 	( array.length + index +1 ) % array.length

							const end 			= 	array[ next ].start
							const spansDays		= 	start.hour 	> end.hour
							const spansWeeks	= 	start.day	> end.day

							const duration		=		( end.day	- start.day		) * 24
													+ 	( end.hour	- start.hour	)
													+	( spansWeeks ? 24 * 7 : 0	)

							return { key, index, previous, next, start, end, duration, spansDays, spansWeeks }
						})



		this.timeSlotConfigs.push(...week)

		this.configSubject.next(this.config)

		this.trackChanges()
	}

	public addTime(day:number, key: string, start: number): void {
		try{
			this.config[day][key] = null
			this.setTime(day, key, start)
		}catch(e){
			delete this.config[day][key]
			throw e
		}
	}

	public removeTime(day: number, key: string): void{
		if(!this.config[day].key) return
		delete this.config[day][key]
		this.updateTimeSlots()
	}

	public setTime(day: number, key: string, start: number): void {

		if(!this.config[day][key])		throw new Error('TimeOfDay.setTime() unknown time of day: '+start+' please use .addTime instead')
		if(start < 0 || start >= 24)	throw new Error('TimeOfDay.setTime() invalid starting time: '+start)

		this.config[day][key] = start

		this.updateTimeSlots()
	}

	public getTimeSlotConfig(date: Date = new Date()): TimeSlotConfig {

		const day	= date.getDay()
		const hour 	= this.getHours(date)


		return 	this.timeSlotConfigs
				.find( config =>	{

					const h			= 	day 				* 24 + hour
					const h_start	= 	config.start.day 	* 24 + config.start.hour
					const h_end		= 	config.end.day 		* 24 + config.end.hour

					return 	config.spansWeeks
							?	h_start <= 	h	||	h	< h_end
							:	h_start <=	h	&&	h 	< h_end


				})
	}

	public getBoundingDate(bound: 'start'|'end', date: Date = new Date()): Date {

		const timeSlotConfig	= 	this.getTimeSlotConfig(date)
		const hours				= 	this.getHours(date)
		const day				=	date.getDay()

		const boundingDay		=	timeSlotConfig[bound].day

		const offsetDay			=	{
										'start':	boundingDay - day - (boundingDay > day ? 7 : 0),
										'end':		boundingDay - day + (boundingDay < day ? 7 : 0)
									}[bound]


		return 	new Date(
					date.getFullYear(),
					date.getMonth(),
					date.getDate() + offsetDay,
					...this.splitHours(timeSlotConfig[bound].hour)
				)
	}


	public getTimes(): string[]{
		return Object.keys(this.config)
	}

	public get current(): TimeSlot {
		return this.at()
	}

	public get upcoming(): TimeSlot {
		return this.at().next()
	}

	public get previous(): TimeSlot {
		return this.at().previous()
	}

	public at(date: Date = new Date() ): TimeSlot {
		return new TimeSlot(this, date)
	}

	public getHours(date: Date = new Date() ): number{
		return date.getHours()+date.getMinutes()/60 + date.getSeconds() / (60*60) + date.getMilliseconds() / (60*60*1000)
	}

	public splitHours(x:number): [number, number, number, number] {

		const sign = Math.sign(x)

		x = Math.abs(x)

		const fullHours 		= Math.floor(x)

		x -= fullHours
		x *= 60

		const fullMinutes		= Math.floor(x)

		x -= fullMinutes
		x *= 60

		const fullSeconds		= Math.floor(x)

		x -= fullSeconds
		x *= 1000

		const fullMilliseconds	= Math.floor(x)

		return [sign*fullHours, sign*fullMinutes, sign*fullSeconds, sign*fullMilliseconds]
	}

}


export class TimeSlot{

	// This ist basically just a wrapper for TimeSlots and Date related methods of TimeOfDay

	public constructor(
		protected timeOfDay: TimeOfDay,
		protected date: Date
	){}

	// TimeSlotConfigs have to be retrieved dynamically, because the timeOfDay config may have changed in between!
	// Don't want theses Objects to subscribe to an Observable



	public get key(): string {
		return this.timeOfDay.getTimeSlotConfig(this.date).key
	}

	public get duration(): number {
		return this.timeOfDay.getTimeSlotConfig(this.date).duration
	}

	public get startDate(): Date {
		return this.timeOfDay.getBoundingDate('start', this.date)
	}

	public get endDate(): Date {
		return this.timeOfDay.getBoundingDate('end', this.date)
	}

	public next(): TimeSlot {
		const currentConfig	=	this.timeOfDay.getTimeSlotConfig(this.date)
		const next			= 	this.timeOfDay.timeSlotConfigs[ currentConfig.next ]

		return new TimeSlot(this.timeOfDay, new Date( this.endDate.getTime() 	+ next.duration * 60 * 60 * 1000 / 2))
	}

	public previous() : TimeSlot {
		const currentConfig	=	this.timeOfDay.getTimeSlotConfig(this.date)
		const prev			= 	this.timeOfDay.timeSlotConfigs[ currentConfig.previous ]


		return new TimeSlot(this.timeOfDay,  new Date( this.startDate.getTime()	- prev.duration * 60 * 60 * 1000 / 2))
	}
}
