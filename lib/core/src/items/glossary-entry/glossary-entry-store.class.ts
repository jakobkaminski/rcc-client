
import	{	adHocId					}	from '../../utils'
import	{
			ItemStore,
			ItemStorage
		}								from '../item-store.class'

import	{
			GlossaryEntry
		}								from './glossary-entry.class'

export function identifyItemBy(item: GlossaryEntry): string {

		if(item.id) return item.id

		return item.id = adHocId()

}

export abstract class GlossaryEntryStore extends ItemStore<GlossaryEntry> {

	public constructor(
		storage? : ItemStorage<GlossaryEntry>
	){
		super({
			itemClass: 	GlossaryEntry,
			storage,
			identifyItemBy
		})
	}

}
