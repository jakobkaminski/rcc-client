import	{	Item				}		from '../item.class'
import	{
			GlossaryEntryConfig,
			assertGlossaryEntryConfig,
			isGlossaryEntryConfig,
		}								from './glossary-entry.commons'

export class GlossaryEntry extends Item<GlossaryEntryConfig> {

	public static acceptsAsConfig(config: unknown): config is GlossaryEntryConfig {
		return isGlossaryEntryConfig(config)
	}

	public set config(config: GlossaryEntryConfig) {

		assertGlossaryEntryConfig(config)

		this.language 	=	config.language
		this.title 		=	config.title
		this.lastUpdate =	config.lastUpdate
		this.content 	=	config.content
		this.authors 	=	config.authors
		this.tags 		=	config.tags

	}

	public get config(): GlossaryEntryConfig {
		return {
			language	: this.language,
			title		: this.title,
			lastUpdate	: this.lastUpdate,
			content		: this.content,
			authors		: this.authors,
			tags		: this.tags
		}
	}

	// INSTANCE
	public language		: string
	public title		: string
	public lastUpdate	: string
	public content		: string[]
	public authors		: string[]
	public tags			: string[]

}
