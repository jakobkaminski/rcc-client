import	{
			Item,
			ConfigOf
		}							from './item.class'

import	{
			Subject,
			Subscription,
			merge,
			Observable
		}							from 'rxjs'

import	{
			share
		}							from 'rxjs/operators'


import	{
			assert,
			assertProperty,
			throwError,
		}							from '../utils'

/**
 * Classes conforming to this interface represent item source, that most of the
 * time can also be used to persist the data.
 *
 * An item storage can be a static storage that just returns a hard coded
 * list of items. It could also be an interface to a remote server or something
 * local storage. That is why .store is optional and all calls have to be async.
 */
export interface ItemStorage<I extends Item = Item, C = ConfigOf<I> > {

	/**
	 * Retrieves all items from the storage.
	 */
	getAll			: () 				=> Promise<C[]>

	/**
	 * Overwrites(!) all items in the storage with the provided items:
	 * If you store only one item, the storage will be left with only
	 * that one item.
	 */
	store?			: (items: (I|C)[])	=> Promise<void>
}

/**
 * Checks if an input conforms to {@link ItemStorage} interface. Throws an error it doesn't.
 */

export function assertItemStorage(x:unknown ) : asserts x is ItemStorage {

	assertProperty(x, 'getAll')
	assert(typeof x.getAll === 'function', 'assertItemStorage: property .getAll is not a function')

	if ('store' in x) assert(typeof x.store === 'function', 'assertItemStorage: property .store is not a function')
}

/**
 * Configuration data for {@link ItemStore}.
 */

export interface ItemStoreConfig<I extends Item = Item > {
	/**
	 * The class of the items meant to be managed by the store.
	 */
	itemClass		: new (...args: unknown[]) => I,

	/**
	 * This is where the store can retrieve items from or store changes to.
	 * If omitted the store will be empty on startup and nothing will be stored.
	 * This is still useful for temporary item management and items that are
	 * supposed to be flushed, when the app reloads.
	 */
	storage?		: ItemStorage<I>,

	/**
	 * This function is used in order to map items to a (temporary) id. It will
	 * default to the '.id' property if the items have one. If no
	 * .identifyItemBy callback is provided and items do not have an .id
	 * property, you will not be able to reference items by a string –
	 * sometimes you just do not need to.
	 *
	 * How is this different to an id? If the item does not have an .id property
	 * the value returned by this function will not be stored alongside the item.
	 * So when reloading the app, the temporary id will change; e.g. when this
	 * callback returns a random uuid. It will stay the same if this function
	 * is used to return e.g. a hash value or maybe some other existing property
	 * to be used as id replacement, maybe a .name property.
	 *
	 * How is this useful? Some item types just do not benefit from ids,
	 * others run into problems like two items having the same content but
	 * different ids, and we do not recognize them as the same. Sometimes we do
	 * not want the ids to be transmitted. Regardless of what kind of item we
	 * have at hand we want the items store to be able to deal with them; and
	 * often times we would still like to reference items by a string; e.g. when
	 * we want to use a reference to an item in a path:
	 * /item/edit/my-random-uuid These kinds of reference do not have to persist
	 * after a page reload, so a temporary id will do.
	 *
	 * Providing a string will use the property of that name as id.
	 *
	 * Make sure to use a value that will give a unique id!
	 *
	 */
	identifyItemBy?	: string | ( (item: I) => string),
}

/**
 * Checks if an input is an {@link ItemStoreConfig}. Throws an error if it isn't
 */
export function assertItemStoreConfig( x: unknown ): asserts x is ItemStoreConfig {

	assertProperty(x, 			'itemClass')
	assertProperty(x.itemClass, 'prototype')
	assert(x.itemClass.prototype instanceof Item, 	'assertItemStoreConfig: property .itemClass does not extend Item')

	if ('storage' in x 			&& x.storage) 			assertItemStorage(x.storage)
	if ('identifyItemBy' in x 	&& x.identifyItemBy)	assert(
																typeof x.identifyItemBy === 'string'
															||	typeof x.identifyItemBy === 'function',
															'assertItemStoreConfig: property .identifyItemBy must be string or function'
														)
}


/**
 * This class represents the default storage.
 * Everything is thrown into the void; aka not stored at all.
 */
export class VoidStorage<I extends Item = Item> implements ItemStorage<I> {
	public async getAll(): Promise<ConfigOf<I>[]> { return await Promise.resolve([] as ConfigOf<I>[]) }
}


/**
 * ItemStores represent collections of items from a specific source.
 * The source is represented by ItemStoreConfig.storage. Items in the store
 * can be added removed or changed. These mutations will be tracked by the
 * associated Observables and can be handed back to the source for storage.
 *
 * For each item type there can be multiple stores with various sources and
 * purposes; e.g. one store that loads items from a remote source, another one
 * that stores item in the local storage and yet another one that just add a
 * predefined set of hard coded items.
 */

export abstract class ItemStore<I extends Item = Item>{

	protected	_map				: Map<string,I>
									= new Map<string,I>()

	// This getter/setter pair is here, so that sub classes, can
	// better manipulate the list of items.
	protected	get map() : Map<string,I> 	{ return this._map 	}
	protected	set map(m : Map<string,I>) 	{ this._map = m		}

	protected	updateSubscriptions	: Map<string, Subscription>
									= new Map<string, Subscription>()

	/**
	 * The name of the store used in the UI, if needed; translation
	 * string recommended.
	 */
	public		name				:	string
	public 		itemClass!			: 	new (...args: unknown[]) => I

	/**
	 * The item source and also place where to store changes. See
	 * {@link ItemStoreConfig} or {@link ItemStorage} for details.
	 */
	protected 	storage!			: 	ItemStorage<I>

	/**
	 * See {@link ItemStoreConfig}
	 */
	protected 	identifyItem!		:	(item: I) => string


	protected	additionSubject		:	Subject<I>
									= 	new Subject<I>()
	protected	removalSubject		:	Subject<I>
									= 	new Subject<I>()
	protected	updateSubject		:	Subject<I>
									= 	new Subject<I>()

	/**
	 * When this promise resolves, initialization is done and all items have
	 * been retrieved from the source/storage.
	 */
	public		ready				:	Promise<void>

	public		addition$			:	Observable<I>
									= 	this.additionSubject.asObservable()
	public		removal$			:	Observable<I>
									= 	this.removalSubject.asObservable()
	public		update$				:	Observable<I>
									= 	this.updateSubject.asObservable()


	public		change$				:	Observable<I>
									= 	merge(this.addition$, this.removal$, this.update$)
										.pipe( share() )




	public constructor( config: ItemStoreConfig<I> ) {

		assertItemStoreConfig(config)

		this.ready = this.init(config)

	}

	protected async init(config: ItemStoreConfig<I>): Promise<void> {

		this.itemClass 			= config.itemClass
		this.storage			= config.storage || new VoidStorage<I>()

		this.identifyItem		= this.getIdentifyItemFn(config.identifyItemBy)


		// TODO: No unsubscribe needed, will run forever anyway, right?
		this.addition$.subscribe(	(item: I) => this.addUpdateSubscription(item) )
		this.removal$.subscribe(	(item: I) => this.removeUpdateSubscription(item) )

		await this.restoreFromStorage()

	}

	protected addUpdateSubscription(item: I) : void  {

		if(!item.update$) return

		const subscription 	: Subscription
							= item.update$.subscribe( () => this.updateSubject.next(item) )

		this.updateSubscriptions.set(item.id,  subscription)
	}

	protected removeUpdateSubscription(item : I) : void {

		const subscription 	: Subscription
							= this.updateSubscriptions.get(item.id)

		if(!subscription) return

		subscription.unsubscribe()

		this.updateSubscriptions.delete(item.id)
	}

	/**
	 * Determines what string should be used in order to reference an item.
	 * Normally that would be the .id property, but if no such property exists,
	 * we can specify an alternative; see {@link ItemStoreConfig}.
	 */
	protected getIdentifyItemFn(identifyItemBy: undefined | string | ((item:I) => string) ): (item:I) => string {

		// default:
		if(!identifyItemBy) 						return 	(item:I) : string  => item.id

		// use some property as identifier
		if(typeof identifyItemBy === 'string')		return 	(item:I) : string => {

																assertProperty(item, identifyItemBy, 'ItemStore.getIdentifyItemFn() property does not exist on I: '+identifyItemBy)

																const key	: string
																			= identifyItemBy
																const value	: unknown
																			= item[key]

																if(typeof value  === 'string') return value

																throwError(`ItemStore.getIdentifyItemFn() property ist not a string: ${key}`, item)

															}
		// use a custom function, e.g. random uuid or hash
		if(typeof identifyItemBy === 'function') 	return 	(item:I) => identifyItemBy(item)

		throw new Error('ItemSource.getIdentifyItemFn: invalid identifyItemConfig')
	}



	protected async restoreFromStorage(): Promise<void>{

		const configs	: ConfigOf<I>[]
						= await this.storage.getAll()

		configs.forEach( (config:ConfigOf<I>) => this.addConfig(config) )
	}



	protected addConfig( config: ConfigOf<I>): I {

		const item  	: I
						= new this.itemClass(config)

		this.addItem(item)

		return item
	}


	/**
	 * This method should be used in every subclass when items are to be added;
	 * it will trigger the associated observables to track the
	 * change, see above.
	 *
	 * If this method is supposed to be publically available, you should
	 * overwrite it in your child class and probably also add a call to
	 * .storeAll() in your overwriting method to ensure that changes are stored.
	 */
	protected addItem(item: I): I {

		const id 			: string
							= this.identifyItem(item)

		const existing_item : I
							= this.map.get(id)

		this.map.set(id, item)

		if (existing_item) 	this.updateSubject.next(item)
		else 				this.additionSubject.next(item)

		return item
	}


	/**
	 * This method should be used in every subclass when items are to be removed;
	 * it will trigger the associated observables to track the
	 * change, see above.
	 *
	 * If this method is supposed to be publically available, you should
	 * overwrite it in your child class and probably also add a call to
	 * .storeAll() in your overwriting method to ensure that changes are stored.
	 */
	protected removeItem(item: I): I {

		const id 		: string
						= this.identifyItem(item)

		const storeType : string
						= this.constructor.name

		assert(this.map.has(id), `${storeType}.removeItem: item not found, id: ${id}`, { item, store:this })

		this.map.delete(id)

		this.removalSubject.next(item)

		return item
	}


	/**
	 * Stores all items to the source. There is no way to only stores changes
	 * for a particular item, you always store all of them at once. Some sources
	 * just do not allows to add elementary changes; e.g. when the source is
	 * encrypted or a zip file.
	 */
	protected async storeAll(): Promise<void> {
		if(!this.storage.store) throw new Error('ItemStore.storage.store() not implemented: '+ this.constructor.name)

		return this.storage.store(Array.from(this.map.values()))
	}


	/**
	 * Gets an item by its id. This also works for items that do not have an .id
	 * property. Those are treated according to .indentifyItemBy property, see
	 * {@link ItemStoreConfig}
	 *
	 * Can be used to get multiple items at once by providing an array of ids.
	 * Ids that do not resolve to an item will add null to the result.
	 */
	public async get(id: 		string)				: Promise<I | null>
	public async get(ids: 		string[])			: Promise< (I | null)[]>
	public async get(id_or_ids: string | string[])	: Promise<I|(I | null)[]> {

		await this.ready

		if(typeof id_or_ids === 'string') return await this.get([id_or_ids]).then( items => items[0] || null)

		const unique_ids	: string[]
							= Array.from(new Set(id_or_ids) )

		const ready_items 	: (I|null)[]
							= unique_ids.map(id => this.map.get(id) || null)

		return ready_items
	}

	public get items(): I[]{
		return Array.from(this.map.values())
	}

}
