// TODO Plugin for Question Types!

import	{	Item				}		from '../item.class'

import	{
	AnswerType,
			QuestionConfig,
			QuestionOptionConfig,
			TranslationList,
			isQuestionConfig
		}								from './questions.commons'

import	{
			QuestionValidator
		}								from './question-validator'

import	{
			assert
		}								from '../../utils'


export function unknownConfig(id: string): QuestionConfig {
	return	{
				id: 			id,
				translations: 	{ en: 'Unknown question: #'+id }, // todo use translationService
				type:			'unknown',
				meaning:		'Missing config for question: #'+id
			}
}


export class Question extends Item<QuestionConfig> {



	// INSTANCE



	declare public id			: string

	public type					: AnswerType
	public meaning				: string
	public translations			: TranslationList
	private _options?				: QuestionOptionConfig[]
	public set options(value: QuestionOptionConfig[]) {
		this._options = value
	}
	public get options(): QuestionOptionConfig[] {
		if(this._options)
			return this._options
		if(!this._options && this.type === 'boolean')
			return [
				{	value: true,	translations: { de: 'ja',	en: 'yes'	} },
				{	value: false,	translations: { de: 'nein',	en: 'no'	} }
			]
	}

	public min					: number
	public max					: number
	public tags					: string[]
	public unit					: string

	public static override acceptsAsConfig(x:unknown): x is QuestionConfig {
		return isQuestionConfig(x)
	}


	public constructor(id:string)
	public constructor(config:QuestionConfig)
	public constructor(idOrConfig: string | QuestionConfig)

	public constructor(idOrConfig: string | QuestionConfig){

		const config: QuestionConfig
			=	typeof idOrConfig === 'string'
					?	unknownConfig(idOrConfig)
					:	idOrConfig

		super(config)

	}

	public override set config(config: QuestionConfig){
		assert(Question.acceptsAsConfig(config), 'Invalid Question config: '+JSON.stringify(config))

		this.id				= config.id
		this.type			= config.type
		this.meaning		= config.meaning
		this.translations	= config.translations
		this.options		= config.options
		this.min			= config.min
		this.max			= config.max
		this.tags			= config.tags
		this.unit			= config.unit
	}

	public override get config(): QuestionConfig {
		return  {
			id			: this.id,
			type		: this.type,
			meaning		: this.meaning,
			translations: this.translations,
			options		: this.options,
			min			: this.min,
			max			: this.max,
			tags		: this.tags,
			unit		: this.unit
		}
	}

	public async validateAnswer(value:unknown):Promise<void>{
		return QuestionValidator.validateAnswer(value, this.config)
	}




}
