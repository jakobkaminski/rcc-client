
import	{
			ItemStore,
			ItemStorage
		}								from '../item-store.class'

import	{
			Question
		}								from './question.class'




export abstract class QuestionStore extends ItemStore<Question> {


	public constructor(
		storage? : ItemStorage<Question>
	){
		super({
			itemClass: 	Question,
			storage
		})
	}

}
