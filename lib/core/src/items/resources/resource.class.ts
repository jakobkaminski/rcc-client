import { assert } from '../../utils'
import { Item } from '../item.class'
import { ResourceConfig } from './resource.commons'

export class Resource extends Item<ResourceConfig> {
	public constructor(config: ResourceConfig) {
		super(config)
		this.id = config.id
	}

	public get text(): string {
		return this.config.text
	}
	public set text(value: string) {
		this.config.text = value
	}

	public static acceptsAsConfig(potentialResourceConfig: unknown): potentialResourceConfig is ResourceConfig {
		assert(typeof potentialResourceConfig === 'object', 'config must be an object')

		const config: ResourceConfig = potentialResourceConfig as ResourceConfig

		assert(typeof config.id === 'string', 'config must have an id')
		assert(typeof config.text === 'string', 'config must have text')

		return true
	}
}
