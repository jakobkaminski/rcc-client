import { ItemStorage, ItemStore } from '../item-store.class'
import { Resource } from './resource.class'

export class ResourceStore extends ItemStore<Resource> {
	public constructor(
		storage?: ItemStorage<Resource>,
	) {
		super({
			itemClass: Resource,
			storage
		})
	}
}
