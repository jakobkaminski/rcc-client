import { Question, QuestionOptionConfig, Report } from '../../items'

// Columns: Pseudonym, answer date, question ID, question wording, raw answer, answer wording
export type CSVRow = [string, string, string, string, string|number|boolean, string] | []

/**
 * Converts a report + corresponding questions to a CSV string
 * @param report Input report
 * @param questions Array of all questions
 * @param lang Language of question and answer wording
 * @param pseudonym User pseudonym/patient ID, if applicable. Defaults to '0000'
 * @returns All answers + questions in a comma- and newline-separated string
 * with column headings on the first line
 */
export function report2csv(report: Report, questions: Question[], lang: string, pseudonym: string = '0000') : string {
	// Get 2-d array holding all rows
	const rows : CSVRow[] = createContentMatrix(report, questions, lang, pseudonym)

	// Convert non-numerical cells to strings and escape double quotes
	const escapedRows : (string|number)[][]	= rows.map(row => row.map(
														col => {
															if (typeof col === 'number') return col

															const escapedCol : string = String(col).replace('"', '""')
															return `"${escapedCol}"`
														}))

	// Convert 2-dimensional array to CSV string
	const csvContent : string = escapedRows.map( row => row.join(',') ).join('\n')

	// Column headings
	const csvMetadata : string = '"PSEUDONYM","ANSWER DATE","QUESTION ID","QUESTION WORDING","ANSWER RAW","ANSWER WORDING"'

	return csvMetadata + '\n' + csvContent
}

/**
 * Helper function that takes all the user data
 * and creates an array that holds all rows as arrays.
 */
function createContentMatrix(report: Report, questions: Question[], lang: string, pseudonym: string = '0000') : CSVRow[] {
	// 2-dimensional matrix that holds all rows
	const rows : CSVRow[] = []

	// 1 row = 1 entry, in chronological
	report.entries.forEach(entry => {
		const question			: Question	= questions.find(q => q.id === entry.questionId)
		const questionWording	: string 	= question.translations[lang] || question.meaning

		// Find selected answer wording from options object, if present
		const answerRaw			: string|number|boolean	= entry.answer
		let answerWording		: string = answerRaw.toString()
		if (question.options) {
			const option : QuestionOptionConfig = question.options.find(opt => opt.value === answerRaw)
			answerWording = option && (option.translations?.[lang] || option.meaning ) || ''
		}

		rows.push([pseudonym, entry.date, entry.questionId, questionWording, answerRaw, answerWording])
	})

	return rows
}
