import	{	OperatorFunction			}	from 'rxjs'
import	{	flatMap 					}	from 'rxjs/operators'
import	{	EncryptionHandler			}	from './encryption.commons'


export function decrypt(encryptionHandler: EncryptionHandler, pass_through = false): OperatorFunction<any, any> {

	return 	flatMap( (value:any) => encryptionHandler.decrypt(value)
						.catch( e => pass_through
									?	value
									:	null))
}


export function encrypt(encryptionHandler: EncryptionHandler): OperatorFunction<any, any> {

	return 	flatMap( (value:any) => {
				try {
					return encryptionHandler.encrypt(value)
				} catch(e) {
					throw new Error('encrypt Operator: Encryption failed.')
				}
			})
}
