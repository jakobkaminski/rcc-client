import { ApplicationRef } from '@angular/core'
import { Subject, lastValueFrom, takeUntil } from 'rxjs'

/**
 * Checks if callback throws any errors; returns true if so.
 */
export function hasErrors(callback: () => unknown, log_errors : boolean = false): boolean {

	try{
		callback()
		return false
	} catch(e){
		// We explicitly want the log here:
		// eslint-disable-next-line no-console
		if(log_errors) console.log(e)

		return true
	}

}

/**
 * Checks if callback throws any errors; returns true if not.
 */
export function isErrorFree(callback: () => unknown, log_errors : boolean = false): boolean {
	return !hasErrors(callback, log_errors)
}



/**
 * Use isErrorFree() instead
 * @deprecated
 */
export function flattenErrors(callback: () => unknown): boolean {
	console.warn('flattenErrors() is deprecated, use isErrorFree() instead.')
	try{
		callback()
		return true
	} catch(e){
		return false
	}
}



/**
 * Throws an error and logs extra information.
 * Also unlike normal throw it can be used after a return statement:
 * ```return throwError(...)```
 */
export function throwError(e: Error|string, offender?: unknown, context?: unknown): never {


	const error : Error	= 	(e instanceof Error)
							?	e
							:	new Error(e)

	// Do not log extra info during tests.
	if( (typeof process !== 'undefined') && process.env?.NODE_ENV === 'test') throw error

	// We explicitly want the logs here:
	/* eslint-disable no-console*/

	console.groupCollapsed('Error (Details) '+ error.message)
	console.info('Offender', offender)
	console.info('Context', context)
	console.error(error)
	console.groupEnd()

	/* eslint-enable no-console*/

	throw error
}


export class AssertionError extends Error {

	public name			: string 	= 'AssertionError'

	public context		: unknown
	public assertion	: unknown

	public constructor(message: string, { cause, assertion, context }:{ cause?: string | Error, assertion?: unknown, context?: unknown}){

		super(message) // super(message, { cause }) does not work =/

		this.cause		=	cause instanceof Error ? cause : new Error(cause)
		this.context	= 	context
		this.assertion	= 	assertion

	}
}



export function assert(x: unknown, e: string | Error = 'unknown assertion error', context?: unknown, cause?: string|Error): asserts x {

	if(x) return undefined

	const error : Error	= 	e instanceof Error
							?	e
							:	new AssertionError(e, { assertion: x, context, cause })
	throw error
}


/**
 * Checks if all keys are property names of given object x. Works as a type guard
 * asserting that properties to all the keys exist.
 * Will throw an error if a property is missing -- unlike {@link has}.
 */
export 	function assertProperty

		// type variables:
		<PropertyNames extends string[] | string>

		// function parameters:
		(x:unknown, keys: PropertyNames, message?: string)

		// type assertion:
		: asserts x is PropertyNames extends string ? {[key in PropertyNames]: unknown }: {[key in PropertyNames[number]]: unknown }

		// function body:
		{
			if(typeof keys === 'string') return assertProperty(x, [keys], message)

			keys.forEach(
				key => 	assert(
							x && (typeof x === 'object' || typeof x === 'function') && (key in x ),
							message || `assertProperty(): missing property: ${key}`,
							{ x, key },
							message ? `assertProperty(): missing property: ${key}` : undefined
						)
			)
		}


export function assertMap<KEY, VALUE>(x: unknown) : asserts x is Map<KEY,VALUE> {
	assert(x instanceof Map, 'assertMap() x is not an instance of Map.')
}



/**
 * Checks if all keys are property names of given object x. Works as a type guard asserting that properties to all the keys exist.
 * Will return false if a property is missing-- unlike {@link assertProperties}.
 */
export function has
		<PropertyNames extends string[]>
		(x:unknown, ...keys: PropertyNames)
		: x is PropertyNames extends string ? {[key in PropertyNames]: unknown }: {[key in PropertyNames[number]]: unknown }
		{
			return isErrorFree( () => assertProperty(x, keys) )
		}



export function log(...args: unknown[]): unknown {
	// We explicitly want the console.log here:
	// eslint-disable-next-line
	console.log(...args)
	return args[0]
}

log.info = function(...args: unknown[]){
	// We explicitly want the console.info here:
	// eslint-disable-next-line
	console.info(...args)
}



export function cycleString(str: string): string{
	return str.substr(1)+str[0]
}




export function camel2Underscore(str: string) : string {
	return str.replace(/([a-z])([A-Z])/g, '$1_$2')
}


/**
 * Typescript is not properly inferring the type from Number.isFinite()
 * That is what this method is for.
 */
export function isFinite(value: unknown) : value is number {
	return Number.isFinite(value)
}



export interface NestedRecord<T> {
	[index:string]:	NestedRecord<T> | T
}


export function isNestedRecord<T>(x: unknown, type: string | (new (...args:unknown[]) => T) ): x is NestedRecord<T> {
	if(typeof x !== 'object') 	return false
	if(x === null)				return false

	return Object.keys(x).every( key => {
		if(!has(x,key)) 												return false
		if(typeof type === 'function'		&& x[key] instanceof type)	return true
		if(typeof type === 'string' 		&& typeof x[key] === type)	return true
		if(isNestedRecord<T>(x[key], type))								return true

		return false
	})
}

/**
 * Needs documentation :D
 *
 *  obj, 'PROP1.PROP2' -> obj.PROP1.PROP2
 */
export function deflateObject<T>(obj: NestedRecord<T> | T | undefined, path : string = '', recursionCount : number = 0): NestedRecord<T> | T | undefined {

	if(recursionCount > 20)	throwError('deflateObject() recursion count greater than 20.', obj, path)

	if(path === '') 		return obj
	if(obj === undefined)	return undefined

	const parts 		: string[] 	= path.split('.')
	const first_part 	: string	= parts.shift()
	const rest			: string	= parts.join('.')

	const def_obj 		: NestedRecord<T> | T	= has(obj, first_part) ? obj[first_part] : undefined
	const def_path		: string				= rest

	return deflateObject( def_obj, def_path, recursionCount++)

}

/**
 * Needs documentation :D
 *
 * obj, 'PROP1.PROP2' -> { PROP1: { PROP2: obj }}
 */

export function inflateObject<T>(obj: NestedRecord<T> | T, path: string, recursionCount : number = 0): NestedRecord<T> {

	if(recursionCount > 20) throwError('inflateObject() recursion count greater than 20.', obj, path)

	assert(typeof path === 'string' && path !== '', 'inflateObject path argument must be a non-empty string.')

	const parts 	: string[]	= path.split('.')
	const last_part : string	= parts.pop()
	const rest		: string	= parts.join('.')

	const inflated	: NestedRecord<T> 	= { [last_part]: obj }

	return	rest === ''
			?	inflated
			:	inflateObject(inflated , rest, recursionCount++)

}


/**
 * Needs documentation -.-
 *
 */
export function mergeObjects<T> (obj1 : NestedRecord<T> | T, obj2 : NestedRecord<T> | T, recursionCount? : undefined| number)	: NestedRecord<T>
export function mergeObjects<T> (obj1 : NestedRecord<T> | T, obj2 : NestedRecord<T> | T, recursionCount? : number)				: NestedRecord<T> | T {

	recursionCount = recursionCount || 0

	assert(recursionCount <= 20, 'mergeObjects() recursion count greater than 20.', [obj1, obj2])

	const keys1 : string[] = obj1 && typeof obj1 === 'object' && Object.keys(obj1)
	const keys2 : string[] = obj2 && typeof obj2 === 'object' && Object.keys(obj2)

	if(!keys1) return obj2
	if(!keys2) return obj2

	const keys 		: Set<string>		= new Set([...keys1, ...keys2])
	const merged 	: NestedRecord<T> 	= {}

	keys.forEach( key => {

		const o1 : NestedRecord<T> | T = has(obj1,key) ? obj1[key] : undefined
		const o2 : NestedRecord<T> | T = has(obj2,key) ? obj2[key] : undefined

		if(o1 === undefined)	return merged[key] = o2
		if(o2 === undefined)	return merged[key] = o1

		merged[key] = mergeObjects( o1, o2, recursionCount++)


	})

	return merged

}

/**
 * Creates a random string of 0s and 1s using crypto.getRandomValues()
 */
export function randomBitString(length: number): string {

	assert(length <= 8192, 				new RangeError('randomBitString: length must not be larger than 8192') )
	assert(length >= 1, 			 	new RangeError('randomBitString: length must be at least 1') )
	assert(Number.isInteger(length),	new TypeError('randomBitString: length must be an integer') )

	const minNumberOfBytes	: 	number
							=	Math.ceil(length / 8)

	const arr 				: 	Uint8Array
							= 	new Uint8Array(minNumberOfBytes)

	window.crypto.getRandomValues(arr)


	// Convert into binary string, with leading zeros if need be.
	const bitString			: 	string
							= 	Array.from(arr).map( x => x.toString(2).padStart(8,'0') )
								.join('')

	// Maybe bitString is a too long (because the number of bits requested
	// does not evenly add up to one byte); removing the excess.
	const clippedBitString	:	string
							=	bitString.substring(0,length)

	return clippedBitString
}

/**
 * Creates a random string of digits (0-9) and letters (a-v). Base on
 * crypto.getRandomValues()
 */
export function randomBase32String(length: number): string {

	assert(length <= 1024, 				new RangeError('randomBase32String: length must not be larger than 1024') )
	assert(length >= 1, 			 	new RangeError('randomBase32String: length must be at least 1') )
	assert(Number.isInteger(length),	new TypeError('randomBase32String: length must be an integer') )

	// A Base 32 character stores 5 bits
	const numberOfBits 		: 	number
							= 	length * 5

	const bitString			: 	string
							= 	randomBitString(numberOfBits)

	// Split the string into chunks of 5 characters.
	const chunksOf5Bits		: 	string[]
							= 	bitString.match(/.{5}/g)

	const base32String		: 	string
							= 	chunksOf5Bits
								.map( chunk => parseInt(chunk,2).toString(32) )
								.join('')


	return base32String

}


/**
 * Creates a random string consisting of numbers and uppercase
 * letters, omitting 0,1,'I' and 'O'. Because these characters
 * are easy to confuse and it leaves us with 32 characters, which are
 * why better to handle than 36.
 */
export function randomReadableBase32String(length: number) : string {
	return 	randomBase32String(length)
			.toUpperCase()
			.replace(/O/g,'W')
			.replace(/0/g,'X')
			.replace(/1/g,'Y')
			.replace(/I/g,'Z')
}

/**
 * Creates a random string consisting of digits 0-9. 2-7 have a slightly
 * higher chance to turn up.
 */
export function randomBiasedBase10String(length: number) : string {

	const shift	: number
				= 2

	const arr 	: Uint8Array
				= new Uint8Array(length)

	window.crypto.getRandomValues(arr)

	return 	Array.from(arr)
			.map( x => (x+shift) % 10)
			.join('')

}


// Thanks to https://stackoverflow.com/questions/105034/how-to-create-guid-uuid
export function uuidv4(): string {

	if(crypto.randomUUID) return crypto.randomUUID()

	// crypto.randomUUID is present in modern browsers (Chrome > 92), so the
	// following is meant as a fallback for older browsers.

	// Disabling eslint, because it complains about ts being ignored.
	/* eslint-disable */
	// @ts-ignore
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16) ) as string
	/* eslint-enable  */
}

export function adHocId(): string {
	return uuidv4()
}


/**
 * Encode a utf-8 string as base64
 */
export function b64EncodeUnicode(str: string): string {
    return 	btoa(
				encodeURIComponent(str)
				.replace(/%([0-9A-F]{2})/g, (match:string, p1:string) => String.fromCharCode(parseInt(p1, 16)) )
			)
}

/**
 * Decode base64 string into utf-8
 */

export function b64DecodeUnicode(str: string): string {
    return decodeURIComponent( (Array.prototype.map.call(atob(str), (c : string) => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)) as string[]) .join(''))
}



/**
 * Sorting function meant to be used in Array.sort().
 * Interprets zero and positive values as counting from the start and negative values as counting from the end.
 * Non-number values are treated as in between the positive and negative values (their order is considered to be irrelevant).
 *
 */
export function biEndSortFn(pos1: unknown, pos2:unknown) : -1 | 0 | 1 {

	if(pos1 === pos2) return null

	const nonum1 : boolean = typeof pos1 !== 'number'
	const nonum2 : boolean = typeof pos2 !== 'number'

	// non-number values are treated as being equally big
	if(nonum1 && nonum2) return 0

	// non-numbers are treated as being bigger than any zero or positive value
	if(nonum1 && (pos2 as number >= 0) ) return +1
	if(nonum2 && (pos1 as number >= 0) ) return -1

	// non-numbers are treated as being smaller than any negative value
	if(nonum1 && (pos2 as number < 0) ) return -1
	if(nonum2 && (pos1 as number < 0) ) return +1


	// negative values are treated as position counted from the end

	// < 0 means bigger than any zero or positive value
	if( (pos1 as number < 0) && pos2 as number >= 0)	return +1
	if( (pos2 as number < 0) && pos1 as number >= 0)	return -1


	return 	pos1 > pos2
			?	+1
			:	-1

	// 3, -1, -3, undefined, 'blub', 0, 0, 5, -10
	// becomes:
	// 0, 0, 3, 5 undefined, 'blub' , -10, -3, -1
}

export function mapSort( map: (u: unknown) => unknown, sort : (x: unknown, y: unknown) => number ) : (a: unknown, b: unknown) => number {

	return (a: unknown, b: unknown) => sort( map(a), map(b) )

}


/**
 * Returns a sorting function to be used in Array.sort(). Objects will be sorted by their property matching the first parameter.
 * If a second parameter is given, the property matching it will be used as secondary sort criterium. Values will be sorted by {@link biEndSortFn}.
 * If a property turns out to be a function, the function will be called and its return value used for sorting.
 */

export function sortByKeyFn(key: string, secondary_key : string | null = null) : (x:unknown, y:unknown) =>  -1 | 0 | 1 {

	return 	(item1: unknown, item2: unknown) => {

				if(typeof key !== 'string') return 0

				let pos1 : unknown	= (item1 as Record<string, unknown>)[key]
				let pos2 : unknown	= (item2 as Record<string, unknown>)[key]

				if(typeof pos1 === 'function') pos1 = pos1()
				if(typeof pos2 === 'function') pos2 = pos2()

				const s : -1 | 0 | 1	= 	(typeof pos1 === 'number') && (typeof pos2 === 'number')
											?	biEndSortFn(pos1, pos2)
											:	pos1 > pos2
											?	1
											:	pos1 < pos2
											?	-1
											:	0

				return 	s === 0
						?	sortByKeyFn(secondary_key)(item1, item2)
						:	s
			}

}

/**
 * Returns a promise that will never resolve, but reject after the given
 * number of milliseconds with an optional message. Meant to be used in
 * Promise.race in conjunction with other promises, that are prone to never resolve.
 * @deprecated
 */
export async function timeoutPromise(ms: number, message? : string) : Promise<void>{

	console.warn('timeoutPromise is deprecated, use getTimeoutPromise instead.')

	return 	getTimeoutPromise(ms,message)
}

/**
 * Returns a promise, that will resolve after a given number of milliseconds
 * with optionally provided data. Meant to be used in
 * Promise.race in conjunction with other promises, that are prone to never resolve.
 * @deprecated
 */
export async function fallbackPromise<T>(ms: number, data? :T) : Promise<T>{

	console.warn('fallbackPromise is deprecated, use getFallbackPromise instead.')

	return 	getFallbackPromise<T>(ms,data)
}



/**
 * Returns a promise that will never resolve, but reject after the given
 * number of milliseconds with an optional message. Meant to be used in
 * Promise.race in conjunction with other promises, that are prone to never resolve.
 */
export async function getTimeoutPromise(ms: number, message? : string) : Promise<void>{

	return 	new Promise( (resolve, reject) => {
				setTimeout(() => reject(new Error(message || 'timeout')), ms)
			})
}

/**
 * Returns a promise, that will resolve after a given number of milliseconds
 * with optionally provided data. Meant to be used in
 * Promise.race in conjunction with other promises, that are prone to never resolve.
 */
export async function getFallbackPromise<T>(ms: number, data? :T) : Promise<T>{

	return 	getDelayedPromise(ms).then(() => data)
}

/**
 * Returns a promise that will resolve after a given number of milliseconds.
 */
export async function getDelayedPromise(ms: number) : Promise<void> {
	return 	new Promise( (resolve) => setTimeout(() => resolve(undefined), ms))
}



/**
 * Counts the number of decimal places
 * @returns Number of decimal places if a single number is provides.
 * If an array of number is provided, returns the the maximum of the individual results.
 */
export function getDecimalPlaces( num : number[] | number ): number {

	if(Array.isArray(num)) return Math.max(0, ...num.map( n => getDecimalPlaces(n) ) )

	return (num.toString().split('.')[1] || '').length

}

/**
 * Round a number to a given number of decimal places.
 */
 export function round(num : number, decimal_places: number): number {

	const scale : number = Math.pow(10,decimal_places)

	return Math.round(num*scale)/scale
 }

/**
 * Find the greatest common divisor. *
 */
 export function gcd(...numbers: number[]): number {

	if(numbers.length === 1) return numbers[0]
	if(numbers.length >  1)	return gcd( gcd(numbers[0], numbers[1]), ...numbers.slice(2) )

	const a :number = numbers[0]
	const b :number = numbers[1]

	return	b === 0
			?	a
			:	gcd(b, a % b)

 }


/**
 * Find the class name of a Class or an instance.
 */
export function getClassName(x: unknown): string {

	const instance 		: unknown 	= 	typeof x === 'function'
									?	x.prototype
									:	x

	return instance.constructor.name

}

const prefixCount: Map<string, number> = new Map()

/** Provides a unique ID with a prefix. ID is kept unique by an incrementing global integer */
export function uniqueId(prefix: string): string {
	if (!prefixCount.has(prefix))
		prefixCount.set(prefix, 0)


	prefixCount.set(prefix, prefixCount.get(prefix) + 1)

	return `${prefix}_${prefixCount.get(prefix)}`
}

/** Transform an array of of items containing arrays into a flat array */
export function flattenArray<T, R>(nestedArray: T[], callback: (item: T) => R[]): R[] {
	return nestedArray.reduce<R[]>((result, item) => [...result, ...callback(item)], [])
}

export async function isApplicationStable(applicationRef: ApplicationRef): Promise<void> {
	const isStable$: Subject<void> = new Subject()

	applicationRef.isStable
		.pipe(
			takeUntil(isStable$)
		)
		.subscribe((isStable: boolean) => {
			if (isStable) {
				isStable$.next()
				isStable$.complete()
			}
		})

	return lastValueFrom(isStable$)
}

/**
 * Index an array by a given function, transforming it into a map. This allows to look up in O(n) complexity
 *
 * @param array
 * @param indexBy The function determining what value should be the index. The return should be unique, otherwise previous values will be overwritten
 * @returns A map, indexed by the value given by indexBy
 *
 * @example
 * const array = [{ id: 'one', value: 3 }, { id: 'two', value: 7 }]
 * const index = indexArray(array, item => item.id)
 *
 * console.log(index.get('two')) // Logs "{ id: 'two', value: 7 }"
 */
export function indexArray<T, Key>(array: T[], indexBy: (item: T) => Key): Map<Key, T> {
	return array.reduce((map, item) => {
		map.set(indexBy(item), item)
		return map
	}, new Map<Key, T>())
}
