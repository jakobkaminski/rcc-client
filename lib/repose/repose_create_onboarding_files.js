import QRCode from 'qrcode'
import yargs from 'yargs'
import { hideBin } from 'yargs/helpers'
import { assert } from 'console'
import { readFile, writeFile } from 'fs/promises'
import { PDFDocument, StandardFonts } from 'pdf-lib'

// CLI args
const argv	=	yargs(hideBin(process.argv))
				.options({
					input: {
						describe:		'Path to csv with Repose participant pseudonyms and passphrases.',
						demandOption:	true,
					},
					output: {
						describe:		'Target path that PDFs with onboarding information should be saved to.',
						default:		'./'
					},
				})
				.argv

/**
 * Reads Repose data from a csv file.
 * The file should have two columns.
 * The first one with the pseudonym, the second one with the corresponding passphrase.
 * @param {string} path Path to the csv file
 * @param {string} colDelim Column delimiter, default ';' (German Excel default)
 * @param {string} rowDelim Row delimiter, default '\r\n' (Windows default)
 * @returns [array of pseudonyms, array of passphrases]
 */
async function readDataFromCsv(path, colDelim=';', rowDelim='\r\n') {
	const reposeCSV = await readFile(path, 'utf8')
	const participants = reposeCSV.trim().split(rowDelim)

	const pseudonyms = []
	const passphrases = []
	participants.forEach(pat => {
		const data = pat.split(colDelim)
		pseudonyms.push(data[0])
		passphrases.push(data[1])
	})
	return [pseudonyms, passphrases]
}

/**
 * Creates a PDF Uint8Array from a given pseudonym and passphrase.
 * The PDF contains pseudonym, passphrase, QR code and link to app for one participant.
 */
async function createPDF(pseudonym, passphrase) {
	// Create new PDF document with one page
	const pdfDoc = await PDFDocument.create()
	const page = pdfDoc.addPage()
	const { _, height } = page.getSize()

	// Text options
	const font = await pdfDoc.embedFont(StandardFonts.Courier)
	const fontBold = await pdfDoc.embedFont(StandardFonts.CourierBold)
	const fontSize = 12
	let lineCount = 2
	const y = () => height - lineCount * 3 * fontSize

	const pageDrawOptions = {
		x: 50,
		size: fontSize,
		font: font
	}

	// Heading in a bigger font + bold
	page.drawText('Ihre Zugangsdaten für die REPOSE-Studie', {
		x: 50,
		size: 16,
		font: fontBold,
		y: y()
	})
	lineCount += 6

	// QR code
	const content = `{ \"repose\": { \"pseudonym\": \"${pseudonym}\", \"passphrase\": \"${passphrase}\" } }`
	const code = await QRCode.toBuffer(content, { type:'png' })
	const pngImage = await pdfDoc.embedPng(code.toString('base64'))
	page.drawImage(pngImage, { ...pageDrawOptions, ...{ y: y() } })
	lineCount += 1

	// Normal text
	const textLines =	[
							`Pseudonym: ${pseudonym}`,
							`Passwort: ${passphrase}`,
							'Link zur App: https://repose.recoverycat.de',
							'Technischer Support: support@recovery.cat oder +49 (0) 30 209 654 055',
							'Bei Supportanfragen bitte keine Gesundheitsdaten übermitteln.'
						]

	textLines.forEach(text => {
		page.drawText(text, { ...pageDrawOptions, ...{ y: y() } })
		lineCount += 1
	})

	return await pdfDoc.save()
}

/**
 * Creates PDF files with onboarding info for all Repose study participants.
 * @param {string[]} pseudonyms Array of all pseudonyms
 * @param {string[]} passphrases Array of all corresponding passphrases
 * @param {string} path Path the PDF files should be saved to
 */
async function createOnboardingFiles(pseudonyms, passphrases, path) {
	assert(pseudonyms.length === passphrases.length, 'pseudonyms and passphrases must be of same length')

	for (let i=0; i < passphrases.length; i++) {
		const pseudonym = pseudonyms[i]
		const passphrase = passphrases[i]

		const pdfBytes = await createPDF(pseudonym, passphrase)
		
		const filename = path + `onboarding-${pseudonym}.pdf`
		await writeFile(filename, pdfBytes, 'utf8')
	}

	console.info(`Created onboarding files at ${path}`)
}

const [pseudonyms, passphrases] = await readDataFromCsv(argv.input)
await createOnboardingFiles(pseudonyms, passphrases, argv.output)
