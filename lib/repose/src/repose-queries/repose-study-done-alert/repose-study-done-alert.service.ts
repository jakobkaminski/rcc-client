import	{
			Injectable
		}									from '@angular/core'
import	{
			RccAlertController
		}									from '@rcc/common'
import	{
			SymptomCheck,
			SymptomCheckConfig
		}									from '@rcc/core'
import	{
			SymptomCheckMetaStoreService
		}									from '@rcc/features'
import	{
			ReposePeriodicSymptomCheckStore
		}									from '../../repose-symptom-check-store'

/**
 * Presents an alert on the landing page when the study is done.
 * = When the ePROMs symptom check ends.
 */
@Injectable()
export class ReposeStudyDoneAlertService {
	public constructor(
		private rccAlertController : RccAlertController,
		private symptomCheckMetaStoreService : SymptomCheckMetaStoreService,
		private reposePeriodicSymptomCheckStore : ReposePeriodicSymptomCheckStore,
	) {
		void this.alertStudyEnd()
	}

	/**
	 * Presents an alert when all periodic symptom checks with an end date have ended.
	 */
	public async alertStudyEnd(): Promise<void> {
		await this.symptomCheckMetaStoreService.ready

		const periodicConfigs : SymptomCheckConfig[] = this.reposePeriodicSymptomCheckStore.assessmentConfigs
		const periodicIds : string[] = periodicConfigs.map(config => config.id)

		const finiteSymptomChecks : SymptomCheck[] =	this.symptomCheckMetaStoreService.items
														.filter(sc =>
															'endDate' in sc.meta.defaultSchedule
															&& periodicIds.includes(sc.id)
														)

		const allSchedulesEnded : boolean = finiteSymptomChecks.length
											&& finiteSymptomChecks.every(sc => sc.meta.defaultSchedule.hasEnded())

		if (allSchedulesEnded) await this.rccAlertController.present({
			message: 'SYMPTOM_CHECK_QUERIES.STUDY_DONE'
		})
	}
}
