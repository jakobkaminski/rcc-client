import 	{
			Component,
			OnInit,
			ViewChild,
		}     								from '@angular/core'
import	{	Router						}	from '@angular/router'
import	{
			Action,
			ButtonConfig,
			ItemSelectService,
			KeyValuePair,
			RccAlertController,
			RccKeyValueStorageService
		}									from '@rcc/common'
import	{
			CalendarDateString,
			SymptomCheck,
			UserCanceledError
		}									from '@rcc/core'
import	{
			SymptomCheckMetaStoreService,
			SymptomCheckQueryRunComponent
		}									from '@rcc/features'
import	{
			ReposeExportService,
			ReposeHttpService,
			SymptomCheckTransmissions
		}									from '../..'

@Component({
	templateUrl:   './repose-query-run-page.component.html',
	styleUrls:     ['./repose-query-run-page.component.css']
})
export class ReposeQueryRunPageComponent implements OnInit {

	public	symptomCheck		: SymptomCheck
	/**
	 * Dictionary that tracks when answers for each symptom check was last sent to the server.
	 */
	private lastTransmissions	: SymptomCheckTransmissions

	@ViewChild(SymptomCheckQueryRunComponent)
	public 	symptomCheckQueryRunComponent	: SymptomCheckQueryRunComponent

	public constructor(
		public symptomCheckMetaStoreService: SymptomCheckMetaStoreService,
		public itemSelectService: ItemSelectService,
		private	router: Router,
		private rccAlertController: RccAlertController,
		private reposeExportService: ReposeExportService,
		private reposeHttpService: ReposeHttpService,
		private rccKeyValueStorageService: RccKeyValueStorageService,
	) {}

	/**
	 * Gets the first available due symptom checks from the meta store
	 */
	public async setup(): Promise<void> {
		const transmissionKvPair : KeyValuePair<SymptomCheckTransmissions> = await this.rccKeyValueStorageService.get('repose-last-transmission-dates')
		this.lastTransmissions = transmissionKvPair.value

		await this.symptomCheckMetaStoreService.ready
		const symptomChecks : SymptomCheck[] = this.symptomCheckMetaStoreService.items.filter(sc => sc.isDue() && !this.postedToday(sc))

		// Don't start query run if no symptom checks are due
		// Still show empty selection modal below to let user know that nothing is due today
		if (!symptomChecks.length)
			void this.router.navigate(['/'])

		if (symptomChecks.length === 1)
			this.symptomCheck = symptomChecks[0]
		else {
			const selectedSymptomChecks :	SymptomCheck[]
										=	await this.itemSelectService.select({
												singleSelect: true,
												items: symptomChecks
											})
											.catch(e => {
												if (e instanceof UserCanceledError) void this.router.navigate(['/'])
												throw e
											})
			this.symptomCheck			=	selectedSymptomChecks[0]
		}
	}

	public ngOnInit(): void {
		void this.setup()
	}

	/**
	 * Called when query run is done.
	 */
	public doneAction: Action = {
		label: 			'SYMPTOM_CHECK_QUERIES.QUERY_RUN.DONE',
		category:		'analyze',
		icon: 			'analyze',
		handler: 		() => this.onDone()
	}
	public async onDone(): Promise<void> {
		await this.symptomCheckMetaStoreService.ready

		const alertButtons : ButtonConfig[] = [
			{
				label: 		'SYMPTOM_CHECK_QUERIES.QUERY_RUN.SEND_DATA_BUTTON',
				resolveAs:	'send'
			},
			{
				label: 		'CANCEL',
				rejectAs:	'cancel'
			}
		]

		// If there are more unanswered SCs, offer to continue (re-open the item select modal)
		const openSymptomChecks : boolean = this.symptomCheckMetaStoreService.items.some(sc => sc.isDue() && !this.postedToday(sc))
		
		if (openSymptomChecks)	alertButtons.push({
									label: 		'SYMPTOM_CHECK_QUERIES.QUERY_RUN.CONTINUE_BUTTON',
									resolveAs:	'continue'
								})

		await this.rccAlertController.present({
			message: 'SYMPTOM_CHECK_QUERIES.QUERY_RUN.SEND_DATA',
			buttons: alertButtons
		})
		.then(
			(resolvedAs) => {
				if (resolvedAs === 'send')
					void this.postData()
				if (resolvedAs === 'continue') {
					this.symptomCheckQueryRunComponent.gotoStart()
					void this.setup()
				}
			}
		)
	}

	private async postData(): Promise<void> {
		const exportedData : [string, string] = await this.reposeExportService.export()
		await this.reposeHttpService.sendReposeExportToServer(exportedData)

		if (typeof this.symptomCheck.id !== 'string')
			throw new Error('ReposeQueryRunPageComponent: Symptom check ID is not a string: ', this.symptomCheck.id)

		this.lastTransmissions[this.symptomCheck.id] = CalendarDateString.today()
		await this.rccKeyValueStorageService.set('repose-last-transmission-dates', this.lastTransmissions)

		void this.router.navigate(['/'])
	}

	private postedToday(sc: SymptomCheck): boolean {
		if (!sc.id)
			throw new Error('ReposeQueryRunPageComponent: Symptom check has no ID, can\'t retrieve last transmission date.')
		return this.lastTransmissions[sc.id] === CalendarDateString.today()
	}

}
