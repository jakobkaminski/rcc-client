import	{
			NgModule,
		}									from '@angular/core'
import	{
			ReposeNewQueryAlertService
		}									from './repose-new-query-alert.service'

@NgModule({
	providers: [
		ReposeNewQueryAlertService
	]
})
export class ReposeNewQueryAlertModule {
	public constructor(
		private reposeNewQueryAlertService	: ReposeNewQueryAlertService,
	) {}
}
