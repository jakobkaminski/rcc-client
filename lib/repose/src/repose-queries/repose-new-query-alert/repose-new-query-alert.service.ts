import	{
			Injectable
		}									from '@angular/core'
import	{
			RccAlertController
		}									from '@rcc/common'
import	{
			SymptomCheck
		}									from '@rcc/core'
import	{
			SymptomCheckMetaStoreService
		}									from '@rcc/features'

/**
 * Presents an alert on the landing page when sporadic symptom checks are active.
 */
@Injectable()
export class ReposeNewQueryAlertService {
	public constructor(
		private rccAlertController : RccAlertController,
		private symptomCheckMetaStoreService : SymptomCheckMetaStoreService
	) {
		void this.alertSporadicSymptomChecks()
	}

	private async alertSporadicSymptomChecks(): Promise<void> {
		await this.symptomCheckMetaStoreService.ready
		const sporadicSymptomChecks : SymptomCheck[] = this.symptomCheckMetaStoreService.items.filter(sc => sc.isDue() && 'sporadicConfig' in sc.meta.defaultSchedule)
		
		if (sporadicSymptomChecks.length) await this.rccAlertController.present({
			message: 'SYMPTOM_CHECK_QUERIES.NEW_QUERY'
		})
	}
}
