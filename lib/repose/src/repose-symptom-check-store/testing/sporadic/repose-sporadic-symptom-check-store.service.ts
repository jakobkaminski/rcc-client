import 	{
			Injectable
		}										from '@angular/core'
import	{
			RccStorage
		}										from '@rcc/common'
import	{
			CalendarDateString,
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
			QuestionConfig,
			assertProperty,
			assertSporadicScheduleConfig,
		}										from '@rcc/core'
import	{
			ReposeAssessmentQuestionConfigs,
		}										from '../../sporadic/repose-sporadic-question-store.service'


/**
 * This {@link SymptomCheckStore} registers a sporadic test symptom check for the REPOSE study
 */
@Injectable()
export class ReposeTestSporadicSymptomCheckStore extends SymptomCheckStore {
	public ready : Promise<void>
	/**
	 * Hard-coded configs of sporadic symptom checks for REPOSE
	 */
	public assessmentConfigs : SymptomCheckConfig[] = [sporadicTestAssessmentConfig]
	
	public constructor(
		private rccStorage : RccStorage,
	) {
		super(rccStorage.createItemStorage('rcc-repose-test-sporadic-sc-import-store'))
		this.ready = this.setup()
	}

	public async setup(): Promise<void> {
		await this.ready
	}

	/**
	 * Populates the item storage with `this.assessmentConfigs`.
	 */
	public async populateItemStore() : Promise<void> {
		// Await ItemStore.ready
		// super.ready doesn't work since SymptomCheckStore doesn't have .ready
		await this.ready

		this.assessmentConfigs.forEach(config => this.addFromConfig(config))
		await this.storeAll()
	}

	/**
	 * Checks if `config.id` exists in {@link SymptomCheckStore}.
	 * If not, adds `config` and sets its `startDate` to today.
	 */
	private addFromConfig(config: SymptomCheckConfig) : void {
		assertProperty(config, 'id', 'Could not add SymptomCheckConfig since it doesn\'t have an id')

		const symptomCheck : SymptomCheck = this.items.find(sc => sc.id === config.id)
		if (!symptomCheck) {
			const configCopy : SymptomCheckConfig = Object.assign({}, config)
			assertSporadicScheduleConfig(configCopy.meta.defaultSchedule)

			// startDate must be today so that the scheduling is calculated correctly
			configCopy.meta.defaultSchedule.startDate = CalendarDateString.today()
			this.addConfig(configCopy)
		}
	}

	/**
	 * Checks if the REPOSE symptom checks have already been created.
	 * Can be used as a proxy to check whether the study has started.
	 */
	public isRunning() : boolean {
		const ids		: string[] = this.assessmentConfigs.map(config => config.id)
		const storedIds : string[] = this.items.map(sc => sc.id)
		return ids.every(id => storedIds.includes(id))
	}
}


/**
 * Returns the IDs of all questions whose config tags include `tag`.
 */
function filterByTag(tag: string, arrayToFilter: QuestionConfig[] = undefined) : string[] {
	const configs : QuestionConfig[] = arrayToFilter ?? ReposeAssessmentQuestionConfigs

	return	configs
			.filter(config => config.tags.includes(tag))
			.map(config => config.id)
}

const sporadicTestAssessmentConfig : SymptomCheckConfig = {
	id:				'rcc-repose-test-sporadic',
	meta:			{
						label: 				'REPOSE Sporadic Test Assessment',
						defaultSchedule:	{
												startDate			: '2000-01-01',
												interval			: [0, 2],
												matchesDaysAfter	: 2
											}
					},
	questions:		filterByTag('RMEQ'),
	presentation:	{
						slides: [
							{ message : { translations: { en: '', de: 'Fragen zum Chronotyp (D-rMEQ): Bitte lesen Sie jede Frage sorgfältig durch, bevor Sie antworten. Beantworten Sie bitte alle Fragen, auch dann, wenn Sie sich bei einer Frage unsicher sind. Beantworten Sie die Fragen in der vorgegebenen Reihenfolge. Beantworten Sie die Fragen so schnell wie möglich. Es sind die ersten Reaktionen auf die Fragen, die uns mehr interessieren als eine lange überlegte Antwort. Beantworten Sie jede Frage ehrlich. Es gibt keine richtige oder falsche Antwort.' } } },
							...filterByTag('RMEQ')
						]
					}
}
