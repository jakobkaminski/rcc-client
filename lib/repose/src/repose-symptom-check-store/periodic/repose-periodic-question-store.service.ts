import	{
			Injectable
		}								from '@angular/core'
import	{
			ItemStorage,
			Question,
			QuestionConfig,
			QuestionStore
		}								from '@rcc/core'


/**
 * This {@link QuestionStore} contains the ePROMS questions for the REPOSE study.
 */
@Injectable()
export class ReposePeriodicQuestionStore extends QuestionStore {

	public name : string= 'rcc-repose-periodic-question-store'

	public constructor(){
		super(ReposePeriodicQuestionStorage)
	}
}
		
const ReposePeriodicQuestionStorage	: ItemStorage<Question, QuestionConfig>
									= { getAll: () => Promise.resolve(ReposeEpromQuestionConfigs) }


export const ReposeEpromQuestionConfigs : QuestionConfig[] = [
	{
		id:       'EPROM_1',
		type:     'integer',
		meaning:  'Konnten Sie Ihren Alltag heute zufriedenstellend bewältigen?',
		translations: {
			en: '',
			de: 'Konnten Sie Ihren Alltag heute zufriedenstellend bewältigen?'
		},
		options: [
			{ value: 1, translations: { en: '', de: 'ja, vollkommen' } },
			{ value: 2, translations: { en: '', de: 'ja, mit leichten Einschränkungen' } },
			{ value: 3, translations: { en: '', de: 'mit deutlichen Einschränkungen' } },
			{ value: 4, translations: { en: '', de: 'kaum' } },
		],
		tags: ['EPROM', 'EPROM_1']
	},
	{
		id:       'EPROM_2',
		type:     'integer',
		meaning:  'Haben Sie das Gefühl, dass etwas Seltsames vor sich geht?',
		translations: {
			en: '',
			de: 'Haben Sie das Gefühl, dass etwas Seltsames vor sich geht?'
		},
		options: [
			{ value: 1, translations: { en: '', de: 'nein' } },
			{ value: 2, translations: { en: '', de: 'manchmal' } },
			{ value: 3, translations: { en: '', de: 'häufig' } },
			{ value: 4, translations: { en: '', de: 'stark' } },
		],
		tags: ['EPROM', 'EPROM_2']
	},
	{
		id:       'EPROM_3',
		type:     'integer',
		meaning:  'Sind Sie misstrauisch?',
		translations: {
			en: '',
			de: 'Sind Sie misstrauisch?'
		},
		options: [
			{ value: 1, translations: { en: '', de: 'nein' } },
			{ value: 2, translations: { en: '', de: 'gering' } },
			{ value: 3, translations: { en: '', de: 'mäßig' } },
			{ value: 4, translations: { en: '', de: 'stark' } },
		],
		tags: ['EPROM', 'EPROM_3']
	},
	{
		id:       'EPROM_4',
		type:     'integer',
		meaning:  'Fühlen Sie sich niedergeschlagen?',
		translations: {
			en: '',
			de: 'Fühlen Sie sich niedergeschlagen?'
		},
		options: [
			{ value: 1, translations: { en: '', de: 'nein' } },
			{ value: 2, translations: { en: '', de: 'etwas' } },
			{ value: 3, translations: { en: '', de: 'mäßig' } },
			{ value: 4, translations: { en: '', de: 'stark' } },
		],
		tags: ['EPROM', 'EPROM_4']
	},
	{
		id:       'EPROM_5',
		type:     'integer',
		meaning:  'Sind Sie antriebslos?',
		translations: {
			en: '',
			de: 'Sind Sie antriebslos?'
		},
		options: [
			{ value: 1, translations: { en: '', de: 'nein' } },
			{ value: 2, translations: { en: '', de: 'etwas' } },
			{ value: 3, translations: { en: '', de: 'deutlich' } },
			{ value: 4, translations: { en: '', de: 'sehr stark' } },
		],
		tags: ['EPROM', 'EPROM_5']
	},
	{
		id:       'EPROM_6',
		type:     'integer',
		meaning:  'Sind Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?',
		translations: {
			en: '',
			de: 'Sind Sie voller Energie und motiviert, viele verschiedene Dinge zu tun?'
		},
		options: [
			{ value: 1, translations: { en: '', de: 'nein' } },
			{ value: 2, translations: { en: '', de: 'gering' } },
			{ value: 3, translations: { en: '', de: 'mäßig' } },
			{ value: 4, translations: { en: '', de: 'stark' } },
		],
		tags: ['EPROM', 'EPROM_6']
	},
	{
		id:       'EPROM_7',
		type:     'integer',
		meaning:  'Haben Sie den Eindruck, die Gedanken rasen Ihnen durch den Kopf?',
		translations: {
			en: '',
			de: 'Haben Sie den Eindruck, die Gedanken rasen Ihnen durch den Kopf?'
		},
		options: [
			{ value: 1, translations: { en: '', de: 'nein' } },
			{ value: 2, translations: { en: '', de: 'manchmal' } },
			{ value: 3, translations: { en: '', de: 'häufig' } },
			{ value: 4, translations: { en: '', de: 'stark' } },
		],
		tags: ['EPROM', 'EPROM_7']
	}
]
