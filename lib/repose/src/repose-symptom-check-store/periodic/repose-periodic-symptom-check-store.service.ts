import 	{
			Injectable
		}										from '@angular/core'
import	{
			RccStorage
		}										from '@rcc/common'
import	{
			assertPeriodicScheduleConfig,
			assertProperty,
			CalendarDateString,
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
		}										from '@rcc/core'
import	{
			ReposeEpromQuestionConfigs,
		}										from './repose-periodic-question-store.service'


/**
 * This {@link SymptomCheckStore} registers the periodic symptom check for the REPOSE study,
 * which contains the ePROMS questionnaire.
 */
@Injectable()
export class ReposePeriodicSymptomCheckStore extends SymptomCheckStore {
	public ready : Promise<void>
	/**
	 * Hard-coded configs of periodic symptom checks for REPOSE
	 */
	public assessmentConfigs : SymptomCheckConfig[] = [ePromsAssessmentConfig]

	public constructor(
		private rccStorage : RccStorage,
	) {
		super(rccStorage.createItemStorage('rcc-repose-periodic-sc-import-store'))
		this.ready = this.setup()
	}

	public async setup(): Promise<void> {
		await this.ready
	}

	/**
	 * Populates the item storage with `this.assessmentConfigs`.
	 */
	public async populateItemStore() : Promise<void> {
		// Await ItemStore.ready
		// super.ready doesn't work since SymptomCheckStore doesn't have .ready
		await this.ready

		this.assessmentConfigs.forEach(config => this.addFromConfig(config))
		await this.storeAll()
	}

	/**
	 * Checks if `config.id` exists in {@link SymptomCheckStore}.
	 * If not, adds `config` and sets its `startDate` to today.
	 */
	private addFromConfig(config: SymptomCheckConfig) : void {
		assertPeriodicScheduleConfig(config.meta.defaultSchedule)
		assertProperty(config, 'id', 'Could not add SymptomCheckConfig since it doesn\'t have an id')

		const symptomCheck : SymptomCheck = this.items.find(sc => sc.id === config.id)
		if (!symptomCheck) {
			const configCopy : SymptomCheckConfig = Object.assign({}, config)
			assertPeriodicScheduleConfig(configCopy.meta.defaultSchedule)

			// Add config with end date 193 days from today
			configCopy.meta.defaultSchedule[2] = CalendarDateString.daysAfter(CalendarDateString.today(), 193)
			this.addConfig(configCopy)
		}
	}

	/**
	 * Checks if the REPOSE symptom checks have already been created.
	 * Can be used as a proxy to check whether the study has started.
	 */
	public isRunning() : boolean {
		const ids		: string[] = this.assessmentConfigs.map(config => config.id)
		const storedIds : string[] = this.items.map(sc => sc.id)
		return ids.every(id => storedIds.includes(id))
	}
}

export const ePromsAssessmentConfig : SymptomCheckConfig = {
		id:				'rcc-repose-sc-eproms',
		meta:			{
							label: 				'REPOSE ePROMS Assessment',
							defaultSchedule:	[
													[3, 6],	// Active on Wednesdays and Saturdays (0=Sunday)
													[]
												],
						},
		questions:		ReposeEpromQuestionConfigs.map(config => config.id)
}
