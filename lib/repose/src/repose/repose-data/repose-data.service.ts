import { Injectable } from '@angular/core'
import { IncomingDataService, RccAlertController, RccSettingsService, RccToastController, RccTranslationService } from '@rcc/common'
import { assert, assertProperty, isErrorFree } from '@rcc/core'
import { filter } from 'rxjs'
import { ReposeHttpService } from '../repose-http.service'

/**
 * Interface for REPOSE data, currently pseudonym and passphrase.
 */
export type ReposeData = {
	'repose' : {
		pseudonym	: string
		passphrase		: string
	}
}

export function assertReposeData(data: unknown): asserts data is ReposeData {
	assertProperty(data, 'repose', 'assertReposeData: missing \'repose\' key')
	assertProperty(data.repose, 'pseudonym', 'assertReposeData: missing .pseudonym')
	assertProperty(data.repose, 'passphrase', 'assertReposeData: missing .passphrase')
	assert(typeof data.repose.pseudonym === 'string', 'assertReposeData: .pseudonym must be a string')
	assert(typeof data.repose.passphrase === 'string', 'assertReposeData: .passphrase must be a string')
}

export function isReposeData(data: unknown): data is ReposeData {
	return isErrorFree(() => assertReposeData(data))
}

@Injectable()
export class ReposeDataService {
	public constructor (
		private incomingDataService: IncomingDataService,
		private rccSettingsService: RccSettingsService,
		private rccAlertController: RccAlertController,
		private rccToastController: RccToastController,
		private rccTranslationService: RccTranslationService,
		private reposeHttpService: ReposeHttpService
	) {
		// Listen for ReposeData from IncomingDataService and put it in storage
		this.incomingDataService
			.pipe(filter(isReposeData))
			.subscribe(data => void this.handleIncomingReposeData(data))
	}

	/**
	 * Presents a toast to the user asking whether incoming ReposeData should be saved.
	 * If yes, save it (overwriting existing data), else discard it.
	 * @param data A ReposeData object
	 */
	public async handleIncomingReposeData(data: ReposeData): Promise<void> {
		const currentData : ReposeData	= await this.getReposeData()
		const newData : Record<string,string> = { newId: data.repose.pseudonym, newPw: data.repose.passphrase }

		// If no data exists yet: Ask to save
		if (!currentData.repose.pseudonym || !currentData.repose.passphrase) {
			const saveIdMessage : string = this.rccTranslationService.translate(
				'REPOSE.DATA_SERVICE.NEW_ID.SAVE',
				newData
			)
			await this.rccAlertController.confirm(saveIdMessage, undefined, undefined, 'REPOSE-Onboarding')
		}

		// If data already exists: Ask to overwrite
		else {
			const overwriteIdMessage : string = this.rccTranslationService.translate(
				'REPOSE.DATA_SERVICE.NEW_ID.OVERWRITE',
				{ ...newData, currentId: currentData.repose.pseudonym, currentPw: currentData.repose.passphrase }
			)
			await this.rccAlertController.confirm(overwriteIdMessage, undefined, undefined, 'REPOSE-Onboarding')
		}

		await this.setReposeData(data)
	}

	/**
	 * Gets Repose respondent information from the SettingsService.
	 * Gets 'repose-pseudonym' and 'repose-passphrase' individually,
	 * @returns A ReposeData object constructed from the settings entries
	 */
	public async getReposeData(): Promise<ReposeData> {
		const pseudonym		: string = (await this.rccSettingsService.get<string>('repose-pseudonym')).value
		const passphrase	: string = (await this.rccSettingsService.get<string>('repose-passphrase')).value

		const reposeData	: ReposeData = { 'repose' : { 'pseudonym': pseudonym, 'passphrase': passphrase } }
		return reposeData
	}

	/**
	 * Saves ReposeData using SettingsService.
	 * Deconstructs the ReposeData object into two individual settings entries
	 * 'repose-pseudonym' and 'repose-passphrase'
	 * @param data A ReposeData object
	 */
	public async setReposeData(data: ReposeData): Promise<void> {
		void this.rccToastController.info('REPOSE.DATA_SERVICE.SAVE.INFO')
		try {
			assertReposeData(data)
			await this.rccSettingsService.set('repose-pseudonym', data.repose.pseudonym)
			await this.rccSettingsService.set('repose-passphrase', data.repose.passphrase)
			void this.rccToastController.success('REPOSE.DATA_SERVICE.SAVE.SUCCESS')
		} catch (error) {
			void this.rccToastController.failure('REPOSE.DATA_SERVICE.SAVE.FAILURE')
			if (error instanceof Error) throw error
			throw new Error(String(error))
		}
	}
}
