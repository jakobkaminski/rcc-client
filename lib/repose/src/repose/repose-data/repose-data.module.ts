import { NgModule } from '@angular/core'
import { IncomingDataService, IncomingDataServiceModule, provideSettingsEntry, provideTranslationMap, SettingsEntry, SettingsModule } from '@rcc/common'
import { uuidv4 } from '@rcc/core'
import { ReposeDataService } from './repose-data.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

const reposePseudonymSettingsEntry: SettingsEntry<null> = {
	id:				'repose-pseudonym',
	label: 			'REPOSE.SETTINGS.PSEUDONYM.LABEL',
	description:	'REPOSE.SETTINGS.PSEUDONYM.DESCRIPTION',
	icon:			'settings',
	type:			'string' as const,
	defaultValue:	null,
	restriction:	'read-only'
}

const reposePassphraseSettingsEntry : SettingsEntry<null> = {
	id:				'repose-passphrase',
	label: 			'REPOSE.SETTINGS.PASSPHRASE.LABEL',
	description:	'REPOSE.SETTINGS.PASSPHRASE.DESCRIPTION',
	icon:			'settings',
	type:			'string' as const,
	defaultValue:	null,
	restriction:	'hidden'
}

const reposeDataSettingsGroup: SettingsEntry = {
	id:				'repose',
	type:			'group',
	label:			'REPOSE.SETTINGS.LABEL',
	description:	'REPOSE.SETTINGS.DESCRIPTION',
	position:		-1,
	icon:			'settings',
	subSettingIds:	[
		'repose-pseudonym',
		'repose-passphrase'
	]
}


/**
 * Example data module, used by ReposeDataService.
 * Sends a ReposeData object with mock data to IncomingDataService
 */
@NgModule({
	imports: [IncomingDataServiceModule]
})
export class ExampleReposeData {
	public constructor(
		private incomingDataService: IncomingDataService,
		private reposeDataService: ReposeDataService
	) {
		this.incomingDataService.next( { 'repose': { pseudonym: '0123', passphrase: uuidv4() } } )
	}
}

@NgModule({
	imports:	[
					// ExampleReposeData,
					SettingsModule
				],
	providers:	[
					ReposeDataService,
					provideTranslationMap('REPOSE', { en, de }),
					provideSettingsEntry(reposePseudonymSettingsEntry),
					provideSettingsEntry(reposePassphraseSettingsEntry),
					provideSettingsEntry(reposeDataSettingsGroup),
				]
})
export class ReposeDataModule {
	public constructor(private reposeDataService: ReposeDataService) {}
}
