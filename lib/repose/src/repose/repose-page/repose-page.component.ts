// REPOSE study landing page
import { Component } from '@angular/core'
import { ReposeExportService } from '../repose-export.service'
import { ReposeHttpService } from '../repose-http.service'

@Component({
	templateUrl:	'./repose-page.component.html'
})
export class ReposePageComponent {
	public ready 		: Promise<void>
	public exportResult	: [string, string]
	public json_string	: string
	public csv_string	: string

	public constructor(
		private reposeExportService: ReposeExportService,
		private reposeHttpService: ReposeHttpService,
	) {
		this.ready = this.setup()
	}

	public async setup(): Promise<void> {
		this.exportResult = await this.reposeExportService.export()
		this.csv_string = this.exportResult[0]
		this.json_string = this.exportResult[1]
	}

	public async onSendData(): Promise<void> {
		await this.reposeHttpService.sendReposeExportToServer(this.exportResult)
	}
}
