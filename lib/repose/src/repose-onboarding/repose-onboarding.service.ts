import 	{
            Injectable
        }										from '@angular/core'
import	{
            QrCodeService,
            RccAlertController,
        }										from '@rcc/common'
import  {
            assertPeriodicScheduleConfig,
            CalendarDateString,
            Schedule,
            ScheduleConfig,
            UserCanceledError
        }                                       from '@rcc/core'
import  {
            IcsReminderConfig,
            IcsReminderService
        }                                       from '@rcc/features'
import  {
            ReposePeriodicSymptomCheckStore,
            ReposeSporadicSymptomCheckStore,
        }                                       from '../repose-symptom-check-store'
import  {
            isReposeData,
            ReposeDataService
        }                                       from '../repose/repose-data'
import  {
            ReposeHttpService
        }                                       from '../repose/repose-http.service'

/**
 * Represents the REPOSE study onboarding process as a series of alerts.
 */
@Injectable()
export class ReposeOnboardingService {
	public constructor(
        private rccAlertController: RccAlertController,
        private qrCodeService: QrCodeService,
        private reposeDataService: ReposeDataService,
        private reposeHttpService: ReposeHttpService,
        private reposeSporadicSymptomCheckStore: ReposeSporadicSymptomCheckStore,
        private reposePeriodicSymptomCheckStore: ReposePeriodicSymptomCheckStore,
        private icsReminderService: IcsReminderService,
	) {}

    public async setup() : Promise<void> {
        // If the symptom checks are already there, assume the study has started
        if (await this.isRunning()) return

        // Confirm study start
        await this.confirmAlert('REPOSE_ONBOARDING.ALERT.CONFIRM_START')

        // 1. Scan QR code to receive pseudonym and passphrase
        await this.presentAlert('REPOSE_ONBOARDING.ALERT.SCAN_QR')
        await this.scanReposeData()

        // 1.1 Offer to test the Repose data and server connection by sending mock data
		// This is optional
        await this.confirmAlert('REPOSE_ONBOARDING.ALERT.TEST_SETUP')
        .then(
            () => this.reposeHttpService.testReposeConnection(),
            (e) => {
                if (!(e instanceof UserCanceledError)) throw e
			}
        )

        // 2. Create symptom checks
		await this.createSymptomChecks()

        if (!this.reposeSporadicSymptomCheckStore.isRunning() ||
            !this.reposePeriodicSymptomCheckStore.isRunning()) {
            await this.presentAlert('REPOSE_ONBOARDING.ALERT.ERROR')
            return
        }

        // 3. Offer calendar file download
        await this.calendarFileDownload()

        // Done alert
        await this.presentAlert('REPOSE_ONBOARDING.ALERT.DONE')
    }

    private confirmAlert(message: string): Promise<unknown> {
        return  this.rccAlertController.confirm(
                    message,
                    undefined,
                    undefined,
                    'REPOSE_ONBOARDING.ALERT.HEADER'
                )
    }

    private presentAlert(message: string): Promise<unknown> {
        return  this.rccAlertController.present({
                    header  : 'REPOSE_ONBOARDING.ALERT.HEADER',
                    message : message
                })
    }

    private async isRunning(): Promise<boolean> {
        await this.reposeSporadicSymptomCheckStore.ready
        await this.reposePeriodicSymptomCheckStore.ready

        if (this.reposeSporadicSymptomCheckStore.isRunning() &&
            this.reposePeriodicSymptomCheckStore.isRunning()) {
            await this.presentAlert('REPOSE_ONBOARDING.ALERT.ALREADY_RUNNING')
            return true
        }

        return false
    }

    private async scanReposeData(): Promise<void> {
        const scanResult : unknown = await this.qrCodeService.scan()
        if (isReposeData(scanResult))
            await this.reposeDataService.handleIncomingReposeData(scanResult)
        else {
            await this.presentAlert('REPOSE_ONBOARDING.ALERT.WRONG_QR')
            throw new Error('Scanned non-REPOSE QR code')
        }
    }

    private async createSymptomChecks(): Promise<void> {
        try {
			await this.reposePeriodicSymptomCheckStore.populateItemStore()
			await this.reposeSporadicSymptomCheckStore.populateItemStore()
		} catch(e) {
            const error : string = (e instanceof Error) ? e.message : String(e)
            const alertMsg : string = '{{REPOSE_ONBOARDING.ALERT.ERROR_MSG}}: ' + error
			void this.presentAlert(alertMsg)
			throw e
		}
    }

    private async calendarFileDownload(): Promise<void> {
        const ePromsScheduleConfig : ScheduleConfig = this.reposePeriodicSymptomCheckStore.items[0].config.meta.defaultSchedule
        assertPeriodicScheduleConfig(ePromsScheduleConfig)
        
        const icsConfig : IcsReminderConfig = {
            timeString      : '12:00',
            frequency       : new Schedule(ePromsScheduleConfig),
            startDateString : CalendarDateString.today()
        }

        await this.confirmAlert('REPOSE_ONBOARDING.ALERT.ICS_DOWNLOAD')
        .then(
            () =>   this.icsReminderService.promptDownload(icsConfig, 'repose-reminder.ics', 'REPOSE_ONBOARDING.ICS_REMINDER_EVENT_NAME')
                    .then(() => this.presentAlert('REPOSE_ONBOARDING.ALERT.ICS_EXPLAINER')),
            (e) =>  {
                        if (!(e instanceof UserCanceledError)) throw e
                    }
        )
    }
}
