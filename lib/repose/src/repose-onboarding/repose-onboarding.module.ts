import { NgModule } from '@angular/core'
import { provideTranslationMap } from '@rcc/common'
import { ReposeOnboardingService } from './repose-onboarding.service'

import en from './i18n/en.json'
import de from './i18n/de.json'


/**
 * Module containing the REPOSE onboarding user flow
 */
@NgModule({
	providers: [
		provideTranslationMap('REPOSE_ONBOARDING', { en,de }),
		ReposeOnboardingService,
	]
})
export class ReposeOnboardingModule {}
