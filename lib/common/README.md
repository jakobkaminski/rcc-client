This folder includes everything for the app foundation.
The app foundation is meant to provide all the interfaces a feature module might need to interact with the user and other parts of the app.

Modules in this folder will make heavy use of angular.

For every feature: The module will be imported in app.module.ts; 
it must declare all dependencies itself. If it needs a main menu entry, 
it has to register that by itself and must not rely on any other module for that.

Removing a feature module from app.module.ts must not cause any errors.
