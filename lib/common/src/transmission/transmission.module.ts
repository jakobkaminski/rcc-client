import 	{
			NgModule,
			Type,
			ModuleWithProviders

		}										from '@angular/core'

import	{	provideTranslationMap			}	from '../translations'


import	{
			TRANSMISSION_SERVICE,
			AbstractTransmissionService,
		}										from './transmission.commons'
import	{
			RccTransmissionService
		}										from './transmission.service'


import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	providers: [
		RccTransmissionService,
		provideTranslationMap('TRANSMISSION', { en,de }),
	]
})
export class TransmissionModule {


	public static forChild( transmissionServiceClass: Type<AbstractTransmissionService> ): ModuleWithProviders<TransmissionModule>{
		return 	{
					ngModule: TransmissionModule,
					providers: 	[
									{ provide: TRANSMISSION_SERVICE, useExisting: transmissionServiceClass, multi: true }
								]
				}
	}

}
