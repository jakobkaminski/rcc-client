import	{	Component							}	from '@angular/core'

import	{	RccTransmissionService				}	from '../../transmission.service'

@Component({
	templateUrl	: './transmission-settings-information.component.html',
	styleUrls	: ['./transmission-settings-information.component.scss']
})
export class TransmissionSettingsInformationComponent {

	public constructor(
		public rccTransmissionService: RccTransmissionService
	){}
}

