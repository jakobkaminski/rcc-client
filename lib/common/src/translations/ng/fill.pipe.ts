import	{
			Pipe,
			PipeTransform,
		}							from '@angular/core'


import	{
			extendTranslationKey,
			TranslationKey,
		}							from '../nong'


/**
 * This pipe is meant for basic replacements in translation strings.
 * Occurrences of %_n_ , with _n_ being some integer, will be replaced by the _n_-th parameter, starting at 1.
 * Occurrences of %_s_ with _s_ being a single lowercase letter, will be replaced by the parameters in the order of their appearance.
 * The replacement text will only include capital letters, digits and underscores, everything else will be stripped off:
 * ```
 * 	{{ 'SOME_SCOPE.%s' | fill: paused ? 'player_running':'player_paused' }} // yields 'SOME_SCOPE.PLAYER_PAUSED' if pause is truthy.
 *
 * 	{{ 'SOME_SCOPE.GROUP_%2.LABEL_%1' | fill :  'long', 1}} // yields 'SOME_SCOPE.GROUP_1.LABEL_LONG'.
 *
 * 	{{ 'SOME_SCOPE.GROUP_%s.LABEL_%t' | fill :  'long', 1}} // yields 'SOME_SCOPE.GROUP_LONG.LABEL_1'.
 *
 * ```
 **/
@Pipe({ name: 'fill' })
export class RccFillPipe implements PipeTransform {

	public transform(translationKey : TranslationKey, ...content: string[]) : TranslationKey {
		return extendTranslationKey(translationKey, ...content)
	}
}

