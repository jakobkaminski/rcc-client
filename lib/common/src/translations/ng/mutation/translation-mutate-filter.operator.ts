import	{
			AbstractControl
		}								from '@angular/forms'

import	{
			combineLatestWith,
			debounceTime,
			distinctUntilChanged,
			map,
			mergeMap,
			Observable,
			of,
			OperatorFunction,
			pipe,
			startWith,
		}								from 'rxjs'


import	{
			TranslationComplement
		}								from './translation-mutate.commons'


export type TranslationFilterFunction			= 	(translation: string, filterBy: string) => boolean
export type TranslationFilterObject				= 	Observable<string> | AbstractControl<string> | string

export const defaultTranslationFilterFunction 	: 	TranslationFilterFunction
												= 	(value:string, filterStr: string) => {

														const haystack	: string
																		= value?.toLocaleLowerCase() 	|| ''

														const needle	: string
																		= filterStr?.toLocaleLowerCase()	||''

														return haystack.includes(needle)
													}


export interface FilterParams {

	filterFn?	: TranslationFilterFunction,
	filterObj?	: TranslationFilterObject

}
 /**
  * Operator, meant to be used in rxjs .pipe().
  *
  * Filters an Array of {@link TranslationComplement}s based
  * on a provided filtering string and filtering method. Method
  * and string can be provided in various forms
  * (see {@link FilterParams}).
  *
  * Built to be used in {@link RccMutateByTranslationPipe}
  */
export function translationMutateFilter<T>(

	params? : FilterParams | Observable<FilterParams>

): OperatorFunction<TranslationComplement<T>[], TranslationComplement<T>[] > {

	const param$				: 	Observable<FilterParams>
								=	(
										params instanceof Observable
										?	params
										:	of(params)
									)
									.pipe( distinctUntilChanged() )

	const filterStr$			:	Observable<string>
								=	param$
									.pipe(
										mergeMap( parameters => normalizeFilterObject(parameters?.filterObj) ),
										distinctUntilChanged()
									)

	const filterFn$ 			:	Observable<TranslationFilterFunction>
								=	param$
									.pipe(
										map( parameters => parameters?.filterFn ),
										distinctUntilChanged(),
									)

	return 	pipe(
				combineLatestWith(filterStr$, filterFn$),
				map( ([complements, filterStr, filterFn]) => filter<T>(complements, filterStr, filterFn) )
			)

}


/**
 * A {@link TranslationFilterObject} can be various things.
 * This method turns all of them into `Observable<string>`
 * for further processing.
 */
function normalizeFilterObject(this:void, filterObject: TranslationFilterObject): Observable<string>{

	if(!filterObject) 							return 	of('')

	if(typeof filterObject === 'string') 		return 	of(filterObject)

	if(filterObject instanceof Observable)		return 	filterObject

	if(filterObject instanceof AbstractControl)	return 	filterObject.valueChanges
														.pipe(
															startWith(filterObject.value),
															debounceTime(150)
														)

	throw new Error('getFilterObservable: invalid filterObject')
}


/**
 * Creates a copy of the provided Array, containing only
 * those values, where the translation matches the provided
 * filtering string and method.
 */
function filter<T>(

	translationComplements	: TranslationComplement<T>[],
	filterStr				: string | undefined,
	filterFn				: TranslationFilterFunction | null = defaultTranslationFilterFunction

) : TranslationComplement<T>[] {

	if(!translationComplements) 				return undefined
	if(filterFn === null)						return translationComplements

	// filterStr can sensibly be undefined,
	// e.g. when filterFn does not need external input

	return 	translationComplements
			.filter(	({ translation }) 	=> filterFn(translation, filterStr) )
}
