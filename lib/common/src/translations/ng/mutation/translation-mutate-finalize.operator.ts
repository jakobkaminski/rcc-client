
import	{
			map,
			pipe,
			OperatorFunction
		}								from 'rxjs'


import	{
			TranslationComplement
		}								from './translation-mutate.commons'

 /**
  * Operator, meant to be used in rxjs .pipe().
  *
  * Projects each element of an Array of {@link TranslationComplement}s
  * onto its original value.
  * This is the complement to {@link translationMutateComplements}
  *
  * Built to be used in {@link RccMutateByTranslationPipe}
  *
  */
export function translationMutateFinalize<T>(): OperatorFunction<TranslationComplement<T>[], T[] > {

	return 	pipe(
				map( complements  => complements?.map( complement => complement.source))
			)

}
