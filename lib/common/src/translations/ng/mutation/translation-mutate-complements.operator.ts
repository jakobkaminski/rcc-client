
import	{
			map,
			pipe,
			combineLatestWith,
			OperatorFunction,
			distinctUntilChanged
		}								from 'rxjs'

import	{
			RccTranslationService
}										from '../translations.service'

import	{
			TranslationComplement
		}								from './translation-mutate.commons'

 /**
 * Operator, meant to be used in rxjs .pipe().
 *
 * Turns an array of translatable values into {@link TranslationComplement}s
 * containing the translation and the original value.
 *
 * This is the complement to {@link translationMutateFinalize}
 *
 * Built to be used in {@link RccMutateByTranslationPipe}
 */
export function translationMutateComplements<T = unknown>(

	rccTranslationService: RccTranslationService

): OperatorFunction<T[], TranslationComplement<T>[] > {

	const cache	: Map<T,  Record<string, string> >
				= new Map<T,  Record<string, string> >()

	return 	pipe(
				map( translatables => Array.isArray(translatables) ? translatables : undefined ),
				distinctUntilChanged(),
				combineLatestWith(rccTranslationService.activeLanguageChange$),
				map( ([translatables]) => {

					if(!Array.isArray(translatables)) return undefined

					return translatables.map( (translatable:T) => {

						const activeLanguage 			:	string
														= 	rccTranslationService.activeLanguage

						const cachedTranslationTable	:	Record<string, string>
														= 	cache.get(translatable) || {}

						const hasCachedTranslation		:	boolean
														= 	activeLanguage in cachedTranslationTable

						const translation				:	string
														= 	hasCachedTranslation
															?	cachedTranslationTable[activeLanguage]
															:	rccTranslationService.translate(translatable, null, activeLanguage)

						const translationComplement		:	TranslationComplement<T>
														=	{
																source:		translatable,
																translation
															}


						// When the translation was already cashed
						// before the following changes nothing;
						// but that is okay, because it also helps to avoid an
						// if-clause, checking if we had a cached translation:
						cachedTranslationTable[activeLanguage] = translation
						cache.set(translatable, cachedTranslationTable)

						return translationComplement

					})
				})
			)

}
