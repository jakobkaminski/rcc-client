
import	{
			Observer,
			of,
		}										from 'rxjs'
import	{
			TranslationComplement
		}										from './translation-mutate.commons'
import	{
			TranslationSortFunction,
			translationMutateSort
		}										from './translation-mutate-sort.operator'

const translationComplementA	: 	TranslationComplement
								= 	{
										source:			'TEST.A',
										translation:	'xxA'
									}

const translationComplementB	: 	TranslationComplement
								= 	{
										source:			'TEST.B',
										translation:	'xxB'
									}

const translationComplementC	: 	TranslationComplement
								= 	{
										source:			'TEST.C',
										translation:	'xxC'
									}




describe('translationMutateFilter operator', () => {

	let observer					: Observer<unknown>
									= undefined

	let nextSpy						: jasmine.Spy
									= undefined


	beforeEach(() => {

		observer					= 	{
											next: 		() => {/**/},
											error:		() => {/**/},
											complete:	() => {/**/}
										}


		nextSpy						= 	spyOn(observer, 'next')
	})

	it('does not emit on subscribe if source does not', () => {

		of()
		.pipe( translationMutateSort() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(0)


	})

	
	it('emits undefined if source emits undefined', () => {

		of(undefined)
		.pipe( translationMutateSort() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)
		expect(nextSpy).toHaveBeenCalledWith(undefined)

	})

	it('emits [] if source emits []', () => {

		of([])
		.pipe( translationMutateSort() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)
		expect(nextSpy).toHaveBeenCalledWith([])

	})

	it('sorts alphabetically if no parameters are provided', done => {

		of([translationComplementA, translationComplementC, translationComplementA, translationComplementB])
		.pipe( translationMutateSort() )
		.subscribe( complements => {

			expect(complements).toEqual([
				translationComplementA,
				translationComplementA,
				translationComplementB,
				translationComplementC
			])

			done()

		})

	})

	it('passes through source values if .sortFn is set to null and .reverse is not set', done => {

		of([translationComplementA, translationComplementC, translationComplementA, translationComplementB])
		.pipe( translationMutateSort({ sortFn: null }) )
		.subscribe( complements => {

			expect(complements).toEqual([
				translationComplementA,
				translationComplementC,
				translationComplementA,
				translationComplementB,
			])

			done()

		})

	})

	it('sorts source values alphabetically if .sortFn is set to undefined and .reverse is not set', done => {

		of([translationComplementA, translationComplementC, translationComplementA, translationComplementB])
		.pipe( translationMutateSort({ sortFn: undefined }) )
		.subscribe( complements => {

			expect(complements).toEqual([
				translationComplementA,
				translationComplementA,
				translationComplementB,
				translationComplementC,
			])

			done()

		})

	})

	it('sorts source values alphabetically in reverse if .reverse is set to true and .sortFn is not set', done => {

		of([translationComplementA, translationComplementC, translationComplementA, translationComplementB])
		.pipe( translationMutateSort({ reverse: true }) )
		.subscribe( complements => {

			expect(complements).toEqual([
				translationComplementC,
				translationComplementB,
				translationComplementA,
				translationComplementA
			])

			done()

		})

	})

	it('reverses source values if .reverse is set to true and .sortFn is set to null', done => {

		of([translationComplementA, translationComplementC, translationComplementA, translationComplementB])
		.pipe( translationMutateSort({ sortFn: null, reverse: true }) )
		.subscribe( complements => {

			expect(complements).toEqual([
				translationComplementB,
				translationComplementA,
				translationComplementC,
				translationComplementA,
			])

			done()

		})

	})

	it('sorts source by custom function if .sortFn is set and reverse is not set', done => {

		const targetOrder	: string[]
							= ['xxA', 'xxC', 'xxB']

		const customSort	: TranslationSortFunction
							= (tc1, tc2) => Math.sign(targetOrder.indexOf(tc1)-targetOrder.indexOf(tc2)) as -1|0|1


		of([translationComplementA, translationComplementC, translationComplementA, translationComplementB])
		.pipe( translationMutateSort({ sortFn: customSort }) )
		.subscribe( complements => {

			expect(complements).toEqual([
				translationComplementA,
				translationComplementA,
				translationComplementC,
				translationComplementB,
			])

			done()

		})

	})

	it('sorts source by custom function in reverse if .sortFn is set and reverse is true', done => {

		const targetOrder	: string[]
							= ['xxA', 'xxC', 'xxB']

		const customSort	: TranslationSortFunction
							= (tc1, tc2) => Math.sign(targetOrder.indexOf(tc1)-targetOrder.indexOf(tc2)) as -1|0|1

		of([translationComplementA, translationComplementC, translationComplementA, translationComplementB])
		.pipe( translationMutateSort({ sortFn: customSort, reverse: true }) )
		.subscribe( complements => {

			expect(complements).toEqual([
				translationComplementB,
				translationComplementC,
				translationComplementA,
				translationComplementA,
			])

			done()

		})

	})



})
