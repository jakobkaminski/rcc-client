import	{
			combineLatestWith,
			map,
			Observable,
			of,
			pipe,
			distinctUntilChanged,
			OperatorFunction
		}								from 'rxjs'


import	{
			TranslationComplement
		}								from './translation-mutate.commons'



export type TranslationSortFunction				= 	(value_1: string, value_2: string) => -1 | 0 | 1

export const defaultTranslationSortFunction 	: 	TranslationSortFunction
												= 	(value_1:string, value_2: string) => {

														if(!value_1 && !value_2) 	return  0
														if(!value_1) 				return -1
														if(!value_2) 				return  1

														if(value_1 === value_2)		return  0
														if(value_1  >  value_2)		return  1
														if(value_1  <  value_2)		return -1
													}

export interface SortParams {
	/**
	 * A value of null means: 'keep original order';
	 * a value of undefined will fallback to
	 * {@link defaultTranslationSortFunction}
	 */
	sortFn?			: TranslationSortFunction | null,
	reverse?		: boolean
}



 /**
  * Operator, meant to be used in rxjs .pipe().
  *
  * Sorts an Array of {@link TranslationComplement}s based
  * on a provided sorting method.
  *
  * Built to be used in {@link RccMutateByTranslationPipe}
  */
export function translationMutateSort<T>(

	params? : SortParams | Observable<SortParams>

) : OperatorFunction<TranslationComplement<T>[], TranslationComplement<T>[] > {


	const param$			: 	Observable<SortParams>
							= 	(
									params instanceof Observable
									?	params
									:	of(params)
								)
								.pipe( distinctUntilChanged() )

	const sortFunction$		: 	Observable<TranslationSortFunction>
							=	param$
								.pipe(
									map( p => p?.sortFn),
									distinctUntilChanged()
								)

	const reverse$			: 	Observable<boolean>
							=	param$
								.pipe(
									map( p => p?.reverse),
									distinctUntilChanged()
								)


	return 	pipe(
				combineLatestWith(sortFunction$,reverse$),
				map( ([complements, sortFn, reverse]) => sort(complements, sortFn, reverse) )
			)

}




/**
 * Sorts {@link TranslationComplement}s based on
 * a provided sorting function.
 */
function sort<T>(

	translationComplements	: TranslationComplement<T>[],
	/**
	 * If null, keeps original order; if undefined falls back to
	 * {@link defaultTranslationSortFunction}.
	 */
	sortFn	: TranslationSortFunction | null = defaultTranslationSortFunction,
	reverse	: boolean = false

) : TranslationComplement<T>[] {


	const properComplements		: boolean
								= Array.isArray(translationComplements)

	if(!properComplements)			return translationComplements
	if(sortFn === null && !reverse)	return translationComplements

	// Ensure, not to change the original Array!
	const copy					:	TranslationComplement<T>[]
								= 	[...translationComplements]

	if(sortFn === null &&  reverse)	return copy.reverse()

	const sorted				:	TranslationComplement<T>[]
								=	copy
									.sort( ( trComplement_1, trComplement_2 ) => {

										const translation_1 : string
															= trComplement_1.translation

										const translation_2 : string
															= trComplement_2.translation

										return sortFn(translation_1, translation_2)
									})
	if(reverse) sorted.reverse()

	return 	sorted

}
