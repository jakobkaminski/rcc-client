import	{	FormControl						}	from '@angular/forms'
import	{
			Observer,
			Subject,
			of,
		}										from 'rxjs'
import	{	translationMutateFilter			}	from './translation-mutate-filter.operator'
import	{	TranslationComplement			}	from './translation-mutate.commons'

const translationComplementA	: 	TranslationComplement
								= 	{
										source:			'TEST.A',
										translation:	'xxA'
									}

const translationComplementB	: 	TranslationComplement
								= 	{
										source:			'TEST.B',
										translation:	'xxB'
									}

const translationComplementC	: 	TranslationComplement
								= 	{
										source:			'TEST.C',
										translation:	'xxC'
									}


describe('translationMutateFilter operator', () => {

	let observer					: Observer<unknown>
									= undefined

	let nextSpy						: jasmine.Spy
									= undefined


	beforeEach(() => {

		observer					= 	{
											next: 		() => {/**/},
											error:		() => {/**/},
											complete:	() => {/**/}
										}


		nextSpy						= 	spyOn(observer, 'next')

	})


	it('does not emit on subscribe if source does not', () => {

		of()
		.pipe( translationMutateFilter() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(0)


	})

	it('emits on subscribe, if source does', () => {

		of([])
		.pipe( translationMutateFilter() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)

	})

	it('emits undefined if source emits undefined', () => {

		of(undefined)
		.pipe( translationMutateFilter() )
		.subscribe(observer)

		expect(nextSpy).toHaveBeenCalledTimes(1)
		expect(nextSpy).toHaveBeenCalledWith(undefined)

	})

	it('filters by lowercase sub string match if only a filter string observable was provided', done => {

		let count : number = 0

		const filterString$	: Subject<string>
							= new Subject<string>()

		of([translationComplementA, translationComplementB])
		.pipe( translationMutateFilter( { filterObj: filterString$.asObservable() } ) )
		.subscribe( complements => {
			count++

			if(count === 1) expect(complements).toEqual([{ source: 'TEST.A', translation: 'xxA' }])

			if(count === 2)	expect(complements).toEqual([{ source: 'TEST.B', translation: 'xxB' }])

			if(count >= 2)	done()
		})


		filterString$.next('xa')
		filterString$.next('xb')

	})

	it('filters by lowercase sub string match if only a FormControl was provided; value changes are debounced', done => {

		let count : number = 0

		const formControl	: 	FormControl<string>
							= 	new FormControl<string>(null)

		of([translationComplementA, translationComplementB])
		.pipe( translationMutateFilter( { filterObj: formControl } ) )
		.subscribe( complements => {

			count++

			if(count === 1) expect(complements).toEqual([{ source: 'TEST.A', translation: 'xxA' }])

			if(count === 2)	expect(complements).toEqual([{ source: 'TEST.B', translation: 'xxB' }])

			if(count >= 2)	done()
		})

		// Changes are too quick, since the formControl
		// gets debounced inside the operator,
		// only the second one will be recognized:
		formControl.setValue('xc')
		formControl.setValue('xa')

		setTimeout( () => formControl.setValue('xb'), 500)

	})

	it('filters by lowercase sub string match, if only a string was provided', done => {

		of([translationComplementA, translationComplementB, translationComplementC])
		.pipe( translationMutateFilter( { filterObj: 'xc' }) )
		.subscribe( complements => {

			expect(complements).toEqual([{ source: 'TEST.C', translation: 'xxC' }])

			done()
		})

	})

	it('emits [], if nothing matches', done => {

		of([translationComplementA, translationComplementB, translationComplementC])
		.pipe( translationMutateFilter( { filterObj: '-.O.o.-' }) )
		.subscribe( complements => {

			expect(complements).toEqual([])

			done()
		})

	})

	it('filters by custom filter function if provided - without additional filter string', done => {

		function customFilter( value : string) : boolean { return value === 'xxB' }

		of([translationComplementA, translationComplementB, translationComplementC])
		.pipe( translationMutateFilter( { filterObj: undefined, filterFn: customFilter }) )
		.subscribe( complements => {

			expect(complements).toEqual([{ source: 'TEST.B', translation: 'xxB' }])

			done()
		})

	})

	it('filters by custom filter function if provided - with additional filter string', done => {
		
		function exclusionFilter( value : string, filterString: string ) : boolean {
			return !value?.includes(filterString)
		}

		of([translationComplementA, translationComplementB, translationComplementC])
		.pipe( translationMutateFilter( { filterObj: 'xC', filterFn:exclusionFilter }) )
		.subscribe( complements => {

			expect(complements)
			.toEqual([
				{ source: 'TEST.A', translation: 'xxA' },
				{ source: 'TEST.B', translation: 'xxB' }
			])

			done()
		})

	})

})
