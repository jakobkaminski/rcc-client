import { TranslationService, Translator, TranslationResult } from './'

describe('TranslationService', () => {

	it('TranslationService.constructor() should throw an error if translators are missing or empty.', () => {
		expect(() => new TranslationService(undefined)  ).toThrowError()
		expect(() => new TranslationService(null)       ).toThrowError()
		expect(() => new TranslationService([])         ).toThrowError()
	})

	describe('constructor', () => {

		it('should throw an error if translators don\'t enable any language.', () => {

			class TranslatorWithOnlyOneLanguageX extends Translator {

				public compatibleLanguages	: string[]
											= ['xx']

				public match(): number {
					return 1
				}

				public translate(): TranslationResult {
					return { final: '' }
				}
			}

			class TranslatorWithOnlyOneLanguageY extends Translator {
				public compatibleLanguages 	: string[]
											= ['yy']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			expect(
				() =>
					new TranslationService([
						new TranslatorWithOnlyOneLanguageX(),
						new TranslatorWithOnlyOneLanguageY(),
					])
			).toThrowError()
		})

	} )

	describe('.availableLanguages', () => {


		it('should be null if translators have no language restrictions, unless manual restrictions are in place.', () => {

			class TranslatorWithoutLanguageRestriction extends Translator {
				public compatibleLanguages : null = null

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			const translationService	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithoutLanguageRestriction(),
											])

			expect(translationService.availableLanguages).toBeNull()

			translationService.restrictLanguagePool(['de', 'fr'])

			expect(translationService.availableLanguages).toEqual(['de', 'fr'])
		})

		it('should match the intersection of all translator\'s language restrictions and manual language restrictions.', () => {

			class TranslatorWithTwoLanguagesXZ extends Translator {
				public compatibleLanguages 	: string[]
											= ['xx', 'zz', 'aa']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult{
					return { final: '' }
				}
			}

			class TranslatorWithTwoLanguagesYZ extends Translator {

				public compatibleLanguages 	: string[]
											= ['yy', 'zz', 'aa']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			const translationService 	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithTwoLanguagesXZ(),
												new TranslatorWithTwoLanguagesYZ(),
											])

			expect(translationService.availableLanguages).toEqual(['zz', 'aa'])

			translationService.restrictLanguagePool(['zz'])

			expect(translationService.availableLanguages).toEqual(['zz'])

			expect( () => translationService.restrictLanguagePool(['de', 'jp']) ).toThrowError()

		})

	})

	describe('setter .activeLanguage', () => {

		it(' should trigger next on TranslationsService.activeLanguageChange$.', () => {

			class TranslatorWithTwoLanguagesXY extends Translator {
				public compatibleLanguages 	: string[]
											= ['xx', 'yy']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			const translationService 	: 	TranslationService
										= 	new TranslationService(
												[new TranslatorWithTwoLanguagesXY()],
												'aa'
											)

			const languages: string[] = []

			translationService.activeLanguageChange$.subscribe((lang) =>
				languages.push(lang)
			)

			translationService.activeLanguage = 'abc'

			expect(languages).toEqual(['aa', 'abc'])
		})

	})

	describe('getter .activeLanguage', () => {

		it('should return the current value of TranslationsService.activeLanguageChange$.', () => {

			class TranslatorWithOnlyLanguagesXY extends Translator {
				public compatibleLanguages : string[] = ['xx', 'yy']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			const translationService 	: 	TranslationService
										= 	new TranslationService(
												[new TranslatorWithOnlyLanguagesXY()],
												'aa'
											)

			expect(translationService.activeLanguage).toBe('aa')

			translationService.activeLanguageChange$.next('bb')

			expect(translationService.activeLanguage).toBe('bb')
		})

	})

	describe('.addTranslatator()', () => {

		it('should throw an error if translator has no proper compatibleLanguages property', () => {

			class TranslatorWithOnlyOneLanguageX extends Translator {

				public compatibleLanguages : string[] = ['xx']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			class BadTranslator extends Translator {

				public compatibleLanguages: string[] = []

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			const translationService 	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithOnlyOneLanguageX(),
											])

			expect(() => translationService.addTranslator(new BadTranslator())).toThrowError()
		})

		it('should throw an error if after adding a translator no language is available.', () => {

			class TranslatorWithOnlyOneLanguageX extends Translator {
				public compatibleLanguages : string[] = ['xx']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			class TranslatorWithOnlyOneLanguageY extends Translator {
				public compatibleLanguages : string[] = ['yy']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			const translationService 	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithOnlyOneLanguageX(),
											])

			expect(() =>
				translationService.addTranslator(
					new TranslatorWithOnlyOneLanguageY()
				)
			).toThrowError()

		})
	})

	describe('.restrictLanguagePool', () => {

		it('should restrict available languages', () => {

			class TranslatorWithLanguageXandY extends Translator {
				public compatibleLanguages : string[] = ['xx', 'yy']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}


			const translationService 	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithLanguageXandY(),
											])

			expect(translationService.availableLanguages).toEqual(['xx', 'yy'])

			translationService.restrictLanguagePool(['xx'])

			expect(translationService.availableLanguages).toEqual(['xx'])

		})

		it('should throw an error if it results in no language being available.', () => {

			class TranslatorWithOnlyOneLanguageY extends Translator {
				public compatibleLanguages : string[] = ['yy']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}


			const translationService 	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithOnlyOneLanguageY(),
											])

			expect(translationService.availableLanguages).toEqual(['yy'])

			expect( () => translationService.restrictLanguagePool(['xx'])).toThrowError()
		})

	})

	describe('.translate()', () => {


		it('should use translator with best match for translation', () => {

			class TranslatorWithTwoLanguagesXZ extends Translator {
				public compatibleLanguages : string[] = ['xx', 'zz']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			class TranslatorWithTwoLanguagesYZ extends Translator {
				public compatibleLanguages : string[] = ['yy', 'zz']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			class TranslatorWithHighPriority extends Translator {
				public compatibleLanguages : string[] = ['xx', 'yy', 'zz']

				public match() : number {
					return 10
				}

				public translate() : TranslationResult {
					return { final: 'high priority' }
				}
			}

			const translationService 	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithTwoLanguagesXZ(),
												new TranslatorWithHighPriority(),
												new TranslatorWithTwoLanguagesYZ(),
											])

			expect(translationService.translate('anything')).toBe('high priority')
		})

		it('should try the next best translator when best matching translator return null', () => {

			class TranslatorWithTwoLanguagesXZ extends Translator {
				public compatibleLanguages : string[] = ['xx', 'zz']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			class TranslatorWithTwoLanguagesYZ extends Translator {
				public compatibleLanguages : string[] = ['yy', 'zz']

				public match() : number {
					return 1
				}

				public translate() : TranslationResult {
					return { final: '' }
				}
			}

			class TranslatorWithHighPriority extends Translator {
				public compatibleLanguages : string[] = ['xx', 'yy', 'zz']

				public match() : number {
					return 10
				}

				public translate() : TranslationResult {
					return { final: 'high priority after higher priority failed' }
				}
			}

			class TranslatorWithHigherPriority extends Translator {
				public compatibleLanguages : string[] = ['xx', 'yy', 'zz']

				public match() : number {
					return 20
				}

				public translate() : TranslationResult | null {
					return null
				}
			}

			const translationService 	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithTwoLanguagesXZ(),
												new TranslatorWithHighPriority(),
												new TranslatorWithTwoLanguagesYZ(),
												new TranslatorWithHigherPriority(),
											])

			expect(translationService.translate('anything'))
			.toBe('high priority after higher priority failed')
		})

		it('should hand over intermediate values to the next translator', () => {

			let intermediate_called : number = 0

			class TranslatorWithFinalValue extends Translator {
				public compatibleLanguages : string[] = ['xx', 'yy', 'zz']

				public match(x: unknown) : number {
					return x === 'intermediate value' ? 20 : 0
				}

				public translate() : TranslationResult {
					return { final: 'final value' }
				}
			}

			class TranslatorWithIntermediateValue extends Translator {
				public compatibleLanguages : string[] = ['xx', 'yy', 'zz']

				public match(x: unknown) : number {
					return x === 'intermediate value' ? 0 : 20
				}

				public translate() : TranslationResult {
					intermediate_called++
					return { intermediate: 'intermediate value' }
				}
			}

			const translationService 	: 	TranslationService
										= 	new TranslationService([
												new TranslatorWithFinalValue(),
												new TranslatorWithIntermediateValue(),
											])

			expect(translationService.translate('anything')).toBe('final value')
			expect(intermediate_called).toBe(1)
		})

	})
})
