import	{
			BehaviorSubject,
		}							from 'rxjs'

import	{
			assert,
			TranslationList
		}							from '@rcc/core'

import	{
			Translator,
		}							from './translator.class'


import	{
			isFinal,
			isIntermediate,
			TranslationResult
		}							from './interfaces'

// TODO settings

/**
 * This service manages all translation related tasks. It is rather basic, but extendable.
 * Heavy lifting is done in {@link StringTranslator}.
 *
 * In order to extend its capabilities add custom extensions of {@link Translator} to the constructor call.
 *
 * TODO:  default Language should not be hard-coded
 *
 */

export class TranslationService {


	/**
	 * The currently set language; all translations will default to this language,
	 * when no other language is specified.
	 * Languages are denoted by language code (i.e. 'en', 'de', 'fr', ...)
	 *
	 * Emits on subscribe and when the active language is switched to a new one.
	 */
	public activeLanguageChange$ 	: BehaviorSubject<string>


	/**
	 * Service instances of all registered Translators.
	 */
	protected translators			: Translator[]	= []

	/**
	 * This language will be used as initial active language.
	 */
	public defaultLanguage			: string		= 'en'

	/**
	 * Restricts available languages to a limited set,
	 * even if translators could deal with languages
	 * beyond this set.
	 *
	 * @example ['en', 'fr']
	 */
	protected maxLanguagePool		: string[]		= null

	public constructor(
		translators			: Translator[],
		defaultLanguage?	: string

	){

		this.defaultLanguage 		= defaultLanguage || this.defaultLanguage
		this.activeLanguageChange$ 	= new BehaviorSubject<string>(this.defaultLanguage)


		// Without any translators, this service can't do anything:
		assert(translators, 'RccTranslationService.constructor() missing translators.')
		assert(translators.length, 'RccTranslationService.constructor() translators empty or not an array.')

		translators.forEach( translator => this.addTranslator(translator) )

	}


	public set activeLanguage( lang: string ) {
		this.activeLanguageChange$.next(lang)
	}


	public get activeLanguage(): string  {
		return this.activeLanguageChange$.getValue()
	}

	/**
	 * Manually restricts available languages.
	 */
	public restrictLanguagePool(languages: string[]): void {

		this.maxLanguagePool = languages

		assert(this.availableLanguages.length > 0, 'TranslationService.restrictLanguagePool() no languages available after restriction.')
	}

	/**
	 * Adds a translator to the pool of translators. Checks if the translator is compatible with at least one language.
	 */
	public addTranslator( translator: Translator): void {

		const className : string = translator.constructor.name

		// Translators with no language restrictions will be added without further checks:
		if(translator.compatibleLanguages === null) { this.translators.push(translator); return }


		// Translators with bad compatibleLanguages property:
		assert(Array.isArray(translator.compatibleLanguages), 	`RccTranslationService.addTranslator(): ${className}.compatibleLanguages must either be null or an array.`, { translator })
		assert(translator.compatibleLanguages.length > 0,		`RccTranslationService.addTranslator(): ${className}.compatibleLanguages must not be empty.`, { translator })


		// For translators restricted to some languages:
		this.translators.push(translator)


		// Everything still works for *all* languages:
		if(this.availableLanguages === null) return

		// Make sure than after adding the Translator, at least 1 language is still available available:
		assert(this.availableLanguages.length > 0, `RccTranslationService.addTranslator(): no available languages after adding ${className}.`, this.translators)

		// At this point the TranslationService is restricted to some languages,
		// but can handle at least one. This should be the most common case.
	}


	/**
	 * Checks all translators for compatible languages. Only those languages
	 * that are compatible with all translators will be recognized as overall available.
	 *
	 * @returns An Array of available languages or null if all languages are available (e.g. the app can rely on the browser to do all the translations).
	 */
	public get availableLanguages(): null | string[] {


		// All translators, that only work for some languages:
		let restrictedTranslators	: Translator[]	=	undefined

		// Languages some translators are restricted to:
		let conspicuousLanguages	: string[]		= 	undefined


		restrictedTranslators	=	this.translators.filter( translator => Array.isArray(translator.compatibleLanguages) )

		conspicuousLanguages	= 	restrictedTranslators
									.map( translator => translator.compatibleLanguages || [] )
									.flat()
									.filter( (x,i,a) => a.indexOf(x) === i) // return only unique values



		// This is a bit silly, it only happens when all translators are working with all languages
		// That's usually not the case; StringTranslator will always be restricted to some languages
		// and that's the most prominent translator.
		if(conspicuousLanguages.length === 0) return this.maxLanguagePool

		// Languages supported by all translators:
		const supportedLanguages	: 	string[]
									=	conspicuousLanguages
										.filter( language 	=>
											restrictedTranslators
											.every( translator =>
												translator.compatibleLanguages?.includes(language)
											)
										)

		// Apply manual restrictions to available languages:
		const availableLanguages	: 	string[]
									= 	this.maxLanguagePool === null
										?	supportedLanguages
										:	supportedLanguages
											.filter( language => this.maxLanguagePool.includes(language) )

		return 	availableLanguages


	}

	/**
	 * Find all matching translators for the given input and returns them in order
	 * of their matching score (best first).
	 */
	public getMatchingTranslator(input: unknown): Translator[] {

		let translators_with_priority 	: 	{

												translator	: Translator
												priority	: number

											}[] 			= undefined

		let ranked_matching_translators	: 	Translator[] 	= undefined



		translators_with_priority 	= 	this.translators
										.map( translator => ({
											translator,
											priority: translator.match(input)
										}))


		ranked_matching_translators	= 	translators_with_priority
										.filter( ({ priority }) => priority >= 0)
										.sort( ( { priority : p1 }, { priority : p2 } ) => {

											if(p1 <  p2) return  1
											if(p1 >  p2) return -1

											return  0
										})
										.map( ({ translator }) => translator )

		return ranked_matching_translators
	}

	/**
	 * Tries to translate any given input, by finding a matching translator and
	 * returning that translator's translation.
	 *
	 * @returns the translation result and the associated {@link Translator}.
	 **/
	public getTranslationData(

		input			: unknown,
		param?			: Record<string,unknown>,
		language		: string  = this.activeLanguage,
		recursionCount  : number = 0

	): [string, Translator] {

		if(recursionCount > 20) {
			console.warn('TranslationService.translate() recursionCount > 20.')
			return null
		}

		const translators 	: Translator[]		=	this.getMatchingTranslator(input)

		let	translation 	: TranslationResult =	null


		// Try translators one by one starting with the best match:
		for(const translator of translators){

			translation = translator.translate(input, language, param)

			if( isFinal(translation) ) 			return [ translation.final,	translator]

			if( isIntermediate(translation) )	return [
													this.getTranslationData(translation.intermediate, param, language, recursionCount++)[0],
													translator
												]

		}

		return [String(input), null]
	}

	/**
	 * Tries to translate any given input, by finding a matching translator and
	 * returning that translator's translation.
	 *
	 * Returns null, if the input cannot be handled by any translator.
	 **/
	public translate(input: unknown, param?: Record<string,unknown>, language: string  = this.activeLanguage): string | null {

		const [text] = this.getTranslationData(input, param, language)

		return text

	}

	/**
	 * Gets translations for all available languages and the provided
	 * translation key.
	 */
	public translationList(key: string): TranslationList {

		const translationList: TranslationList = {}

		this.availableLanguages.forEach((language: string) => {
			translationList[language] = this.translate(key, null, language)
		})

		return translationList
	}

}
