import { TranslationResult } from '../interfaces'
import { Translator } from '../translator.class'

export class WithLabelTranslator extends Translator {
	public compatibleLanguages: string[] = null

	public match(x?: unknown): number {
		if (typeof x !== 'object' || !x)
			return -1
		if (!('label' in x))
			return -1

		if ('label' in x && typeof x?.label === 'string' && x.label !== '')
			return 1

		if (x?.label === '')
			return 0

		return -1
	}

	public translate(input: { label: string }): TranslationResult {
		return { intermediate: input.label }
	}
}
