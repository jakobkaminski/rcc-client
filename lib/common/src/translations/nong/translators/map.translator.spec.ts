import { MapTranslator } from './map.translator'
import { genericTranslatorClassTests } from '../translator.class.spec'


describe('MapTranslator', () => {

    let mapTranslator = new MapTranslator

    beforeEach(() => {
        mapTranslator = new MapTranslator()
    })

    genericTranslatorClassTests(MapTranslator)

	it('MapTranslator.match() should return -1 if value has no translation map attached.', () => {
		console.log('Running assertions for MapTranslator.match()')


		expect(mapTranslator.match({ translations: {} })                                ).toBe(2)

		expect(mapTranslator.match(new Date())                                          ).toBe(-1)
		expect(mapTranslator.match(true)                                                ).toBe(-1)
		expect(mapTranslator.match(false)                                               ).toBe(-1)
		expect(mapTranslator.match(5)                                                   ).toBe(-1)
		expect(mapTranslator.match('abc')                                               ).toBe(-1)
		expect(mapTranslator.match({})                                                  ).toBe(-1)
		expect(mapTranslator.match([])                                                  ).toBe(-1)
	})

	it('MapTranslator.translate() should return null if tried on a value without translation map attached.', () => {
		console.log('Running 1st assertion for MapTranslator.translate()')


		expect(mapTranslator.translate(true, 'xx')                                      ).toBeNull()
		expect(mapTranslator.translate(5, 'xx')                                         ).toBeNull()
		expect(mapTranslator.translate('abc', 'xx')                                     ).toBeNull()
		expect(mapTranslator.translate({}, 'xx')                                        ).toBeNull()
		expect(mapTranslator.translate([], 'xx')                                        ).toBeNull()
		expect(mapTranslator.translate(new Date(), 'xx')                                ).toBeNull()
	})

	it('MapTranslator.translate() should return final string value matching the entry of the translation map for the provided language .', () => {
		console.log('Running 2nd assertion for MapTranslator.translate()')

		expect(mapTranslator.translate({ translations: { xx: 'final-xx' } }, 'xx').final).toBe('final-xx')
	})

	it('MapTranslator.translate() should return null if the entry of the translation map for the provided language has no string value.', () => {
		console.log('Running 3nd assertion for MapTranslator.translate()')


		expect(mapTranslator.translate({ translations: {} }, 'xx')                      ).toBeNull()
	})
})
