import { UndefinedTranslator } from './undefined.translator'
import { genericTranslatorClassTests } from '../translator.class.spec'


describe('UndefinedTranslator', () => {

	let undefinedTranslator = new UndefinedTranslator
	let numberTranslator = new UndefinedTranslator


	beforeEach(() => {
		undefinedTranslator = new UndefinedTranslator()
		numberTranslator = new UndefinedTranslator()
	})

	genericTranslatorClassTests(UndefinedTranslator)

	it('UndefinedTranslator.match() should return -1, if called for anything other than undefined.', () => {

		expect(undefinedTranslator.match(undefined)           ).toBe(0)

		expect(undefinedTranslator.match(5)                   ).toBe(-1)
		expect(undefinedTranslator.match(9999)                ).toBe(-1)
		expect(undefinedTranslator.match(0)                   ).toBe(-1)
		expect(undefinedTranslator.match(-10)                 ).toBe(-1)
		expect(undefinedTranslator.match(NaN)                 ).toBe(-1)
		expect(undefinedTranslator.match(null)                ).toBe(-1)
		expect(undefinedTranslator.match({ translations: {} })).toBe(-1)
		expect(undefinedTranslator.match(new Date())          ).toBe(-1)
		expect(undefinedTranslator.match(true)                ).toBe(-1)
		expect(undefinedTranslator.match(false)               ).toBe(-1)
		expect(undefinedTranslator.match('abc')               ).toBe(-1)
		expect(undefinedTranslator.match({})                  ).toBe(-1)
		expect(undefinedTranslator.match([])                  ).toBe(-1)
	})

	it('UndefinedTranslator.translate() should return null, if tried on anything other than undefined.', () => {

		expect(numberTranslator.translate(true, 'en')      ).toBeNull()
		expect(numberTranslator.translate('5', 'en')       ).toBeNull()
		expect(numberTranslator.translate('abc', 'en')     ).toBeNull()
		expect(numberTranslator.translate({}, 'en')        ).toBeNull()
		expect(numberTranslator.translate([], 'en')        ).toBeNull()
		expect(numberTranslator.translate(new Date(), 'en')).toBeNull()
		expect(numberTranslator.translate(null, 'en')      ).toBeNull()
		expect(numberTranslator.translate(NaN, 'en')       ).toBeNull()
	})

	it('UndefinedTranslator.translate() should return a language independent intermediate string value for undefined.', () => {

		expect(
			numberTranslator.translate(undefined, 'en').intermediate).toBe(
			'UNDEFINED'
		)

  expect(
			numberTranslator.translate(undefined, 'de').intermediate).toBe(
			'UNDEFINED'
		)

  expect(
			numberTranslator.translate(undefined, 'xx').intermediate).toBe(
			'UNDEFINED'
		)
	})
})
