import	{ 	Translator 			} from '../translator.class'
import	{	TranslationResult	} from '../interfaces'

/**
 *	Any object with a value property `{value: 'something'}`, will be translated to that value.
 */
export class WithValueTranslator extends Translator {

	compatibleLanguages : null 	= null  // works with all languages


	match(x: any): number{
		if(!x) 						return -1
		if(x.value !== undefined)	return  1

		return -1
	}

	translate(input:any, language: string, param?: any): TranslationResult {

		if(this.match(input) === -1) return null

		return { final : String(input.value) }

	}
}
