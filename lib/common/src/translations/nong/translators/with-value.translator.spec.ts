import { WithValueTranslator } from './with-value.translator'
import { genericTranslatorClassTests } from '../translator.class.spec'


describe('WithValueTranslator', () => {

    let withValueTranslator = new WithValueTranslator

    beforeEach(() => {
        withValueTranslator = new WithValueTranslator()
    })

    genericTranslatorClassTests(WithValueTranslator)

    it('WithValueTranslator.match() should return -1, if called for anything lacking the meaning property.', () => {



        expect(withValueTranslator.match({ value: 'blub' })               ).toBe(1)
        expect(withValueTranslator.match({ value: 'abcdefghijkl mnop' })  ).toBe(1)
        expect(withValueTranslator.match({ value: 3 })                    ).toBe(1)
        expect(withValueTranslator.match({ value: null })                 ).toBe(1)
        expect(withValueTranslator.match({ value: {} })                   ).toBe(1)

        expect(withValueTranslator.match(undefined)                       ).toBe(-1)
        expect(withValueTranslator.match(5)                               ).toBe(-1)
        expect(withValueTranslator.match(9999)                            ).toBe(-1)
        expect(withValueTranslator.match(0)                               ).toBe(-1)
        expect(withValueTranslator.match(-10)                             ).toBe(-1)
        expect(withValueTranslator.match(NaN)                             ).toBe(-1)
        expect(withValueTranslator.match(null)                            ).toBe(-1)
        expect(withValueTranslator.match({ translations: {} })            ).toBe(-1)
        expect(withValueTranslator.match(new Date())                      ).toBe(-1)
        expect(withValueTranslator.match(true)                            ).toBe(-1)
        expect(withValueTranslator.match(false)                           ).toBe(-1)
        expect(withValueTranslator.match('abc')                           ).toBe(-1)
        expect(withValueTranslator.match({})                              ).toBe(-1)
        expect(withValueTranslator.match([])                              ).toBe(-1)


    })


    it('WithValueTranslator.translate() should return null, if tried on anything lacking the meaning property.', () => {

        expect(withValueTranslator.translate(true, 'xx')                  ).toBeNull()
        expect(withValueTranslator.translate('5', 'xx')                   ).toBeNull()
        expect(withValueTranslator.translate('abc', 'xx')                 ).toBeNull()
        expect(withValueTranslator.translate({}, 'xx')                    ).toBeNull()
        expect(withValueTranslator.translate([], 'xx')                    ).toBeNull()
        expect(withValueTranslator.translate(new Date, 'xx')              ).toBeNull()
        expect(withValueTranslator.translate(null, 'xx')                  ).toBeNull()
        expect(withValueTranslator.translate(NaN, 'xx')                   ).toBeNull()

    })

    it('WithValueTranslator.translate() should return a final string value that equals the string representation of the present meaning property.', () => {

        expect(withValueTranslator.translate({ value: 'abc' }, 'en').final).toBe('abc')
        expect(withValueTranslator.translate({ value: 'abc' }, 'de').final).toBe('abc')
        expect(withValueTranslator.translate({ value: 'abc' }, 'xx').final).toBe('abc')

        expect(withValueTranslator.translate({ value: 7 }, 'en').final    ).toBe('7')
        expect(withValueTranslator.translate({ value: 7 }, 'de').final    ).toBe('7')
        expect(withValueTranslator.translate({ value: 7 }, 'xx').final    ).toBe('7')

        expect(withValueTranslator.translate({ value: {} }, 'en').final   ).toBe(String({}))
        expect(withValueTranslator.translate({ value: {} }, 'de').final   ).toBe(String({}))
        expect(withValueTranslator.translate({ value: {} }, 'xx').final   ).toBe(String({}))

    })
})
