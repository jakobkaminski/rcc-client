import	{ 	Translator 			} from '../translator.class'
import	{	TranslationResult	} from '../interfaces'

/**
 *	Any object with a meaning property `{meaning: 'something'}`, will be translated to that value.
 */
export class WithMeaningTranslator extends Translator {

	compatibleLanguages : null 	= null  // works with all languages


	match(x: any): number{
		if(!x) 								return -1
		if(typeof x.meaning !== 'string')	return -1

		if(x.meaning)						return  1
		if(x.meaning === '')					return  0

		return -1
	}

	translate(input:any, language: string, param?: any): TranslationResult {

		if(this.match(input) === -1) return null

		return { final : `[${input.meaning}]` }

	}
}
