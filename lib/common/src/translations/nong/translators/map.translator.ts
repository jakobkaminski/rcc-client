import	{ 	Translator 			} from '../translator.class'
import	{ 	TranslationResult	} from '../interfaces'



/**
 *	This interface describes what kind of input MapTranslator can translate.
 */
export interface ObjectWithTranslations {
	translations:	{ [index: string]: string }
}

/**
 * Type guard for ObjectWithTranslations
 */
export function hasTranslations(x: unknown): x is ObjectWithTranslations{

	if(!x) return false

	const translations = (x as ObjectWithTranslations).translations

	if(!translations) return false

	return Object.keys(translations).every( key => typeof key === 'string')
}


/**
 * If an input comes with it's own translations attatched to it, this translator will pick them up:
 * ```
 * 	<span>{{ {something:something, translations: {'en': 'some text', 'de': 'irgendein Text'}} | translate}}</span>
 * 	//will give for german('de'):
 * 	<span>irgendein Text</span>
 *
 * ```
 **/
export class MapTranslator extends Translator {

	compatibleLanguages : null 	= null  // works with all languages

	match(x: unknown): number {

		return	hasTranslations(x)
				?	 2
				:	-1

	}

	translate(input: unknown, language: string, param?: any): TranslationResult {

		if(!hasTranslations(input)) return null

		const translation = input.translations[language]

		return 	typeof translation === 'string'
				?	{ final:  translation }
				:	null
	}
}
