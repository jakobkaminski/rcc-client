
import	{ 	DateStringTranslator 			} from './date-string.translator'
import	{ 	genericTranslatorClassTests 	} from '../translator.class.spec'
import	{	TranslationResult				} from '../interfaces'

describe('DateStringTranslator', () => {

	let dateStringTranslator 	: DateStringTranslator
								= new DateStringTranslator

	beforeEach(() => {
		dateStringTranslator 	= new DateStringTranslator()
	})
	genericTranslatorClassTests(DateStringTranslator)

	it('DateStringTranslator.match() should return -1 if argument is not a valid date string of format YYYY-MM-DD, else return 2.', () => {

		expect(dateStringTranslator.match('2022-01-30')                       ).toBe(2)

		expect(dateStringTranslator.match('2022-01-32')                       ).toBe(-1) // invalid date
		expect(dateStringTranslator.match('2022-13-01')                       ).toBe(-1) // invalid date
		expect(dateStringTranslator.match(new Date())                         ).toBe(-1)
		expect(dateStringTranslator.match(true)                               ).toBe(-1)
		expect(dateStringTranslator.match(false)                              ).toBe(-1)
		expect(dateStringTranslator.match(5)                                  ).toBe(-1)
		expect(dateStringTranslator.match('abc')                              ).toBe(-1)
		expect(dateStringTranslator.match({})                                 ).toBe(-1)
		expect(dateStringTranslator.match([])                                 ).toBe(-1)

	})

	it('dateStringTranslator.translate() should return null if tried on an invalid date string.', () => {

		expect(dateStringTranslator.translate('5', 'xx')                      ).toBeNull()
		expect(dateStringTranslator.translate('abc', 'xx')                    ).toBeNull()
		expect(dateStringTranslator.translate('2019-05-33', 'xx')             ).toBeNull()
		expect(dateStringTranslator.translate('2019-99-01', 'xx')             ).toBeNull()

		expect(dateStringTranslator.translate(true as unknown as string, 'xx')).toBeNull()
		expect(dateStringTranslator.translate({} as unknown as string, 'xx')  ).toBeNull()
		expect(dateStringTranslator.translate([] as unknown as string, 'xx')  ).toBeNull()

	})

	it('DateTranslator.translate() should return final string value for Date objects.', () => {

		const result 	: TranslationResult
						= dateStringTranslator.translate('2022-01-01', 'xx')


		expect('final' in result).toBeTrue()
		expect((typeof (result.final))).toEqual('string')

	})
})
