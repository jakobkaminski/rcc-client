import	{ TimeStringTranslator	}	from './time-string.translator'

describe('TimeStringTranslator,', () => {
	it('TimeStringTranslator.match() should return -1 if called for something that is not an iso date/time string.', () => {

		const timeStringTranslator = new TimeStringTranslator()

		expect(timeStringTranslator.match('2::00.adasd229Z')          ).toBe(-1)
		expect(timeStringTranslator.match('2021-10-01T16:41:00.229')  ).toBe(-1)		//missing timezone
		expect(timeStringTranslator.match('7AM')                      ).toBe(-1)
		expect(timeStringTranslator.match('15.08.')                   ).toBe(-1)
		expect(timeStringTranslator.match(5) 						  ).toBe(-1)
		expect(timeStringTranslator.match(9999) 					  ).toBe(-1)
		expect(timeStringTranslator.match(0) 						  ).toBe(-1)
		expect(timeStringTranslator.match(-10) 					      ).toBe(-1)
		expect(timeStringTranslator.match(NaN) 					      ).toBe(-1)
		expect(timeStringTranslator.match({translations:{}})		  ).toBe(-1)
		expect(timeStringTranslator.match(new Date())				  ).toBe(-1)
		expect(timeStringTranslator.match(true) 					  ).toBe(-1)
		expect(timeStringTranslator.match(false) 					  ).toBe(-1)
		expect(timeStringTranslator.match('abc') 					  ).toBe(-1)
		expect(timeStringTranslator.match({}) 					      ).toBe(-1)
		expect(timeStringTranslator.match([]) 					      ).toBe(-1)

	})

	it('TimeStringTranslator.match() should return a value greater than 1, when encountering an iso date/time string.', () => {

		const timeStringTranslator = new TimeStringTranslator()

	})

	it('TimeStringTranslator.translate() should return null if called for something that is not an iso date/time string.', () => {

		const timeStringTranslator = new TimeStringTranslator()

		expect(timeStringTranslator.translate('2021-10-01T16:41:00.229')                ).toBe(null) 	//missing timezone
		expect(timeStringTranslator.translate('7AM')                                    ).toBe(null)
		expect(timeStringTranslator.translate('15.08.')                                 ).toBe(null)
		expect(timeStringTranslator.translate('abc')                                    ).toBe(null)
		expect(timeStringTranslator.translate(5 				as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate(9999  			as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate(0 				as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate(-10 				as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate(NaN  				as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate({translations:{}}	as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate(new Date()  		as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate(true  			as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate(false 			as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate({}  				as unknown as string)   ).toBe(null)
		expect(timeStringTranslator.translate([] 				as unknown as string)   ).toBe(null)

	})

})
