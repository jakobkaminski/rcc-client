import {
	HierarchicalColor,
	CategoricalColor
}							from '@rcc/themes'

import	{
			assert,
			assertProperty,
		}					from '@rcc/core'

/**
 * A banner is a small colored strip of text
 * that can contain formatting. It is meant to
 * be shown on a prominent place and displayed
 * in a way to draw attention.
 *
 * @example
 * ```
 * {
 *   color: 'secondary',
 *   markdown: {
 *     translations : {
 *       "de" : "Das ist __fett__",
 *       "en" : "This is __bold__",
 *     }
 *   }
 * }
 * ```
 */
export interface BannerConfig {
	color?: HierarchicalColor | CategoricalColor,
	markdown: {
		translations: Record<string,string>
	}
}

export function assertBannerConfig(x:unknown) : asserts x is BannerConfig {

	assertProperty(x, 'markdown', 						'assertBannerconfig: x is missing .markdown property.')
	assertProperty(x.markdown, 'translations', 			'assertBannerconfig: x.markdown is missing .translations property.')

	const properTranslationObject	: 	boolean
									= 	typeof x.markdown.translations === 'object'

	assert(properTranslationObject,						'assertBannerconfig: x.markdown.translations must be an object.')

	const translations				:	Record<string, unknown>
									= 	x.markdown.translations as Record<string, unknown>

	const badTranslationValue		: 	unknown
									= 	Object.values(translations)
										.find( 	translation => typeof translation !== 'string')

	const badTranslationKey			: 	string
									=	Object.keys(translations)
										.find( key => translations[key] === badTranslationValue)


	assert(!badTranslationKey, `assertBannerconfig: x.markdown.translations.${badTranslationKey} is present, but not a string, got ${typeof badTranslationValue} instead.`)
}
