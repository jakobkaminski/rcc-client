import 	{
			Component,
			Input,
			OnDestroy,
		}										from '@angular/core'

import	{
			Observable,
			SubscriptionLike,
			take
		}										from 'rxjs'

import	{
			UserCanceledError
		}										from '@rcc/core'

import	{
			RccTransmission,
			RccTransmissionService
		}										from '../../../transmission'
import	{	RccToastController,
			RccModalController
		}										from '../../../modals-provider'
import	{	ShareMode						}	from '../../share-data.common'



@Component({
	selector:		'rcc-share-transmission-data-modal',
	templateUrl: 	'./share-transmission-data-modal.component.html',
	styleUrls: 		['./share-transmission-data-modal.component.css'],
})
export class ShareTransmissionDataModalComponent  implements OnDestroy {

	@Input()
	public mode: ShareMode

	@Input()
	public set data(value:unknown){
		this._data = value
		void this.setup()
	}
	public get data() : unknown {
		return this._data
	}

	@Input()
	public additionalInfoText		:	string

	/**
	 * Remotely close this modal by emitting something on this observable:
	 */
	public  set closeOn(stop$ : Observable<unknown> | undefined) {

		if(this.stopSubscription) this.stopSubscription.unsubscribe()

		if(!stop$){
			delete this.stopSubscription
			return
		}

		this.stopSubscription 	= 	stop$
									.pipe(take(1))
									.subscribe( () => this.rccModalController.dismiss() )
	}

	public stopSubscription : 	SubscriptionLike

	public 		_data?			:	unknown
	public 		transmission?	:	RccTransmission
	public 		qrData?			:	unknown
	public 		invalid			:	boolean				= false
	public 		failed			:	boolean				= false
	public		complete		:	boolean				= false

	public constructor(
		private rccTransmissionService			: RccTransmissionService,
		private rccToastController				: RccToastController,
		private rccModalController				: RccModalController
	){}





	public async setup() : Promise<void> {


		this.failed = false

		if(this.transmission) void this.transmission.cancel()

		this.qrData = null

		this.transmission 	= await this.rccTransmissionService.setup(this.data)

		this.qrData 		= this.transmission.meta

		try {
			await this.transmission.start()
			this.complete 		=	true

		} catch(e) {
			this.failed 		= true
			throw e
		}

	}


	public retry() : void {
		void this.setup()
	}

	public done() : void  {
		this.cancel()
	}

	public cancel() : void {
		if(this.transmission) void this.transmission.cancel()
		delete this.transmission
		this.rccModalController.dismiss(new UserCanceledError('ShareTransmissionDataModalComponent'))
	}

	public ngOnDestroy() : void  {
		if(!this.transmission) return undefined
		void this.transmission.cancel()
		void this.rccToastController.info('SHARE_DATA.TRANSMISSION.CANCELED')
	}



}
