import	{	NgModule							}	from '@angular/core'
import	{	SharedModule						}	from '../shared-module'
import	{	provideTranslationMap				}	from '../translations'
import	{	QrCodeModule						}	from '../qr-codes'
import	{	TransmissionModule					}	from '../transmission'
import	{	ShareDataService					}	from './share-data.service'
import	{	ShareTransmissionDataModalComponent	}	from './modal/transmission/share-transmission-data-modal.component'
import	{	SharePlainDataModalComponent		}	from './modal/plain/share-plain-data-modal.component'
import	{	CurrentDateMessageComponent			}	from '../current-date-message'


import en from './i18n/en.json'
import de from './i18n/de.json'



@NgModule({
	imports:[
		SharedModule,
		QrCodeModule,
		TransmissionModule,
		CurrentDateMessageComponent,
	],
	providers:[
		ShareDataService,
		provideTranslationMap('SHARE_DATA', { en, de })
	],
	declarations:[
		ShareTransmissionDataModalComponent,
		SharePlainDataModalComponent
	],
	exports: [
		ShareTransmissionDataModalComponent,
		SharePlainDataModalComponent,
	]
})
export class ShareDataModule {}
