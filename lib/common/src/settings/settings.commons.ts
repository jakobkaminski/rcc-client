import	{
			InjectionToken,
			Provider,
			Type
		}								from '@angular/core'


import	{
			ProductOrFactory,
			getProvider,
		}								from '../interfaces'

import	{
			Representation,
			WithDescription,
			WithPosition
		}								from '../actions'

import	{
			DEFAULT_SETTING_GROUP_ID
		}								from './settings.config'


export interface SettingsEntryBase {
	id				: string
	/**
	 * read-only	: Users cannot edit the settings entry through the UI. It is still possible programmatically
	 *
	 * hidden		: The settings entry is not displayed in the UI
	 */
	restriction?	: 'read-only' | 'hidden'
	information?	: Type<unknown>
}

export interface SettingsValueBase<T=unknown>{
	defaultValue	: T | null
	type			: 'time' | 'string' | 'boolean'
	options?		: never
	handler?		: never
	subSettingIds? 	: never
}

export interface SettingsSingleSelectBase<T=unknown>{
	defaultValue	: T | null
	type			: 'select',
	options			: {value: T, label:string}[]
	handler?		: never
	subSettingIds? 	: never
}

export interface SettingsMultiSelectBase<T=unknown> {
	defaultValue	: T[] | null
	type			: 'multiple'
	options			: {value: T, label:string}[]
	handler?		: never
	subSettingIds? 	: never
}

export interface SettingsHandlerBase {
	defaultValue?	: never
	type			: 'handler'
	handler			: () => void
	options?		: never
	subSettingIds? 	: never
}

export interface SettingsGroupBase {
	defaultValue?	: never
	type			: 'group'
	subSettingIds 	: string[]
	handler?		: never
	options?		: never

}


export type SettingsEntry<T = unknown>	= 	SettingsEntryBase
											&
											(
													SettingsValueBase<T>
												|	SettingsSingleSelectBase<T>
												|	SettingsMultiSelectBase<T>
												|	SettingsHandlerBase
												|	SettingsGroupBase
											)
											& 	Representation
											& 	WithDescription
											&	WithPosition

export type SettingsGroupEntry			=	SettingsEntry & SettingsGroupBase

export const SETTING_CONFIGS			: InjectionToken<SettingsEntry<unknown>[]>
										= new InjectionToken('Settings')


export function provideSettingsEntry<T>(config: ProductOrFactory<SettingsEntry<T>>) : Provider {
	return getProvider(SETTING_CONFIGS, config, true)
}



/**
 * This group will catch all the entries that do not belong to any other group.
 */
export const defaultSettingsEntryGroup : SettingsEntry = {
	id: 			DEFAULT_SETTING_GROUP_ID,
	type:			'group',
	label:			'SETTINGS.DEFAULT_GROUP.LABEL',
	description:	'SETTINGS.DEFAULT_GROUP.DESCRIPTION',
	icon:			'settings',
	position:		-0.5, 	// at the end of all entries
	subSettingIds:	[] 		// will be filled automatically
}


