import	{	NgModule					}	from '@angular/core'
import	{	CommonModule				}	from '@angular/common'
import	{	ReactiveFormsModule			}	from '@angular/forms'

import	{	MainHeaderModule			}	from '../main-header'
import	{	TranslationsModule			}	from '../translations/ng/translations.module'
import	{	provideTranslationMap		}	from '../translations/ng/translations.commons'
import	{	IconsModule					}	from '../icons'
import	{	SortByPositionPipeModule	}	from '../ui-components/sort-by-position'
import 	{ 	IonicModule 				}	from '@ionic/angular'

import	{	RccToIDPipeModule			}	from '../ui-components/toID/toID.module'

import	{
			provideSettingsEntry,
			defaultSettingsEntryGroup
		}									from './settings.commons'

import	{	RccSettingsService			}	from './settings.service'

import	{	RccColorCategoryDirective	}	from '../ui-components/color-category/color-category.directive'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	providers: [
		RccSettingsService,
		provideTranslationMap('SETTINGS', { en, de }),
		provideSettingsEntry(defaultSettingsEntryGroup),
	],

	imports: [
		ReactiveFormsModule,
		CommonModule,
		IonicModule,
		TranslationsModule,
		IconsModule,
		MainHeaderModule,
        RccToIDPipeModule,
        SortByPositionPipeModule,
		RccColorCategoryDirective,
	]
})
export class SettingsModule {}
