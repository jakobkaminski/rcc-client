import	{	NgModule					}	from '@angular/core'
import	{	CommonModule				}	from '@angular/common'
import	{	ReactiveFormsModule 		}	from '@angular/forms'
import	{	IonicModule					}	from '@ionic/angular'
import	{
			TranslationsModule,
			provideTranslationMap
		}									from '../translations'
import	{	IconsModule					}	from '../icons'
import	{
			UiComponentsModule
		}									from '../ui-components'
import	{
			ItemService,
			DefaultLabelComponent
		}									from './item.service'
import	{	ExposeTemplateComponent		}	from './expose-template.component'
import	{
			ItemSelectModalComponent,
			ItemSelectService
		}									from './select'
import	{	RccThemeModule				}	from '@rcc/themes/active'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports:[
		IonicModule,
		CommonModule,
		TranslationsModule,
		IconsModule,
		ReactiveFormsModule,
		UiComponentsModule,
		RccThemeModule,
	],
	providers:[
		ItemService,
		ItemSelectService,
		provideTranslationMap('ITEMS', { de, en }),

	],
	declarations:[
		DefaultLabelComponent,
		ExposeTemplateComponent,
		ItemSelectModalComponent
	],
	exports:[
		ExposeTemplateComponent,
		ItemSelectModalComponent
	]
})
export class ItemModule {}
