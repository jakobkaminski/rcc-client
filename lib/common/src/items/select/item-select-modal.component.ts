import  {
			Component,
			Input,
			OnInit,
			OnDestroy,
			ViewChild
		}									from '@angular/core'

import	{
			FormControl
		}									from '@angular/forms'

import	{
			IonSearchbar,
			InfiniteScrollCustomEvent
		}									from '@ionic/angular'

import	{
			Subject,
			takeUntil,
			merge,
			map,
			filter
		}									from 'rxjs'

import	{
			Item,
			ItemStore,
			assert,
			assertProperty,
			isErrorFree
		}									from '@rcc/core'

import	{
			ModalWithResultComponent,
		}									from '../../overlays/modal-with-result.component'

import	{
			Action,
			BaseRepresentation,
		}									from '../../actions'

import	{
			ItemAction,
			ItemSelectionFilter
		}									from '../item.commons'

export interface ItemGroup {
	items			: Item[],
	representation	: BaseRepresentation
}

export function assertItemGroup(x: unknown): asserts x is ItemGroup {
	assertProperty(x, 'items', 'assertItemGroup: Missing .items property')
	assert(Array.isArray(x.items) && x.items.every(i => i instanceof Item))

    assertProperty(x, 'representation', 'assertItemGroup: Missing .representation property')
	const rep : unknown = x.representation
	assertProperty(rep, 'icon', 'assertItemGroup: Missing .representation.icon property')
	assertProperty(rep, 'label', 'assertItemGroup: Missing .representation.label property')
	assert(typeof rep.icon === 'string' && typeof rep.label === 'string')
}

export function isItemGroup(x: unknown): x is ItemGroup {
    return isErrorFree(() => assertItemGroup(x))
}


@Component({
	selector: 'rcc-item-select',
	templateUrl: './item-select-modal.component.html',
	styleUrls:	['./item-select-modal.component.css'],
})
export class ItemSelectModalComponent extends ModalWithResultComponent implements OnInit, OnDestroy {



	@Input()
	public stores				: ItemStore[] = []

	@Input()
	public preselect			: Item[] = []

	@Input()
	public items				: Item[] = []

	@Input()
	public filters 				: ItemSelectionFilter[] = []

	@Input()
	public message				: string = null

	@Input()
	public heading				: string = 'ITEMS.SELECT.TITLE'

	@Input()
	public subHeading			: string = ''

	@Input()
	public singleSelect			: boolean = false

	@ViewChild(IonSearchbar)
	private set searchbar(searchbar: IonSearchbar) {
		if(searchbar)
			setTimeout( () => { void searchbar?.setFocus() } , 200)
	}

	public selected				: Set<Item>  = new Set()

	public itemGroups			: ItemGroup[] = []

	public allItems				: Item[] = []

	public showSearch			: boolean
								= false

	public groupControl			: FormControl<ItemGroup>
								= new FormControl<ItemGroup>(null)

	public searchControl		: FormControl<string>
								= new FormControl<string>('')

	public filteredItems		: Item[] = []

	public numberOfItems		: number

	public exitAction			: Action
								= 	{
										label: 'CLOSE',
										icon: 'close',
										handler: ()	=> this.rccModalController.dismiss(),
									}

	public selectAction			: ItemAction
								= 	{
										role:			'details',
										itemClass:		null,
										getAction:		(item: Item) => ({
											label: 		'ITEMS.ACTIONS.SELECT_ACTION',
											icon:		'',
											handler:	() => this.selectAndApply(item),
										})
									}

	public toggleOnAction		: ItemAction
								= 	{
										role:			'details',
										itemClass:		null,
										getAction:		(item: Item) => ({
											label: 		'ITEMS.ACTIONS.TOGGLE_ON_ACTION',
											icon:		'checkbox',
											handler:	() => this.toggleSelect(item, true),
										})
									}

	public toggleOffAction		: ItemAction
								= 	{
										role:			'details',
										itemClass:		null,
										getAction:		(item: Item) => ({
											label: 		'ITEMS.ACTIONS.TOGGLE_OFF_ACTION',
											icon:		'checkbox_checked',
											handler:	() => this.toggleSelect(item, false),
										})
									}

	protected destroy$			: Subject<void>
								= new Subject<void>()



	public ngOnInit(): void {

		this.setupItems()
		this.setupFilterGroups()
		this.updateFilteredItems()

		// Update item list, whenever search or filter changes:
		merge(
			this.groupControl.valueChanges,
			this.searchControl.valueChanges
			.pipe(
					filter(	(x	: unknown) 	=> typeof x === 'string'),
					map(	(str: string)	=> str.trim() ),
			)
		)
		.pipe( takeUntil(this.destroy$) )
		.subscribe( ()=> this.updateFilteredItems() )

	}

	public getItemAction(item: Item): ItemAction {
		if (this.singleSelect)			return this.selectAction
		if (this.selected.has(item))	return this.toggleOffAction
		return this.toggleOnAction
	}

	/**
	 * Updates the displayed item list. This method is called
	 * whenever the search/filter term changes or a filter is selected from the
	 * pulldown.
	 *
	 * Note: We could have used a getter here instead, or a method that returns
	 * the current array of filtered items at each time. However, such a method
	 * will be called dozens of times and may degrade performance. Therefore, we
	 * opt for an update function that is called only when we already know about
	 * a change.
	 */
	public updateFilteredItems() : void {

		this.resetNumberOfItems()

		const activeGroup 	: 	ItemGroup
							= 	this.groupControl.value

		const items			:	Item[]
							=	activeGroup
								?	activeGroup.items
								:	this.allItems

		const search_term 	:	string
							= 	this.searchControl.value || ''

		const needle		:	string
							= 	search_term.trim().toUpperCase()

		if(needle === '') return void (this.filteredItems = items)

		const filtered		:	Item[]
							=	items.filter( item => {

									const haystack : string	= item.toJSON().toUpperCase()

									return haystack.includes(needle)
								})

		this.filteredItems = filtered

	}



	/**
	 * Toggles the search toolbar; also clears the search, when the toolbar is
	 * hidden.
	 */
	public toggleSearch(force? : boolean) : void {

		this.showSearch = 	force === undefined
							?	!this.showSearch
							:	!!force

		if(!this.showSearch) this.searchControl.setValue('')
	}


	/**
	 * Collects all relevant items from various sources, and marks preselected
	 * items as selected.
	 */
	public setupItems() : void {

		this.resetNumberOfItems()

		const preselectedItems 	:	Item[]
								= 	this.preselect || []

		// Collect all relevant items from various sources:
		this.allItems 			= 	Array.from(new Set([
										...this.stores.map( (store: ItemStore) => store.items).flat(),
										...this.items,
										...preselectedItems,
									]))

		// Make sure, that preselected items actually get selected:
		preselectedItems.forEach( item => this.selected.add(item) )

	}

	/**
	 * Configures filter groups. Each registered itemFilter {@link provideItemFilter}
	 * will have is own item Group (if applicable for the current item type).
	 * Each group consists of a {@link Representation} and an array of items that
	 * match the filter. If no filters were registered, groups are formed for each
	 * registered {@link ItemStore}, that holds items of the current type.
	 *
	 * Note: Since neither items nor registered filters will change while this
	 * modal is displayed, we can prearrange these groups, when the component is
	 * initialized; this also gives us the number of items per group to display
	 * in the filter menu.
	 */
	public setupFilterGroups(): void {

		// grouping
		this.itemGroups.length 	= 0

		// default Grouping if stores are present:
		const storeGroups 	:	ItemGroup[]
							=	this.stores.map( store => ({
									items: store.items,
									representation:{
										label:	store.name,
										icon:	store.itemClass.constructor.name
									}
								}))

		// grouping according to provided filters:
		const filterGroups	:	ItemGroup[]
							=	this.filters.map( itemSelectionFilter => ({
									items: 			this.allItems.filter(itemSelectionFilter.filter),
									representation: itemSelectionFilter.representation
								}))

		const groups 		:	ItemGroup[]
							= 	filterGroups.length > 0
								?	filterGroups
								:	storeGroups


		// items without a group:
		const grouped_items		: Item[]
								= groups.map( group => group.items ).flat()

		const remaining_items 	: Item[]
								= this.allItems.filter( item => ![...grouped_items, ...this.preselect].includes(item))

		groups.push({
			items:	remaining_items,
			representation: {
				label:	'ITEMS.SELECT.OTHER_ITEMS',
				icon:	'unknown'
			}
		})

		const non_empty_groups	: ItemGroup[]
								= groups.filter( group => group.items.length > 0)

		// add all groups with at least one item:
		this.itemGroups.push(...non_empty_groups)

	}

	public selectAndApply(item: Item) : void {
		this.selected = new Set([item])
		this.apply()

	}

	public toggleSelect(item: Item, force? : boolean): void {

		if(force === true)	return void this.selected.add(item)
		if(force === false)	return void this.selected.delete(item)

		if (this.selected.has(item))
			this.selected.delete(item)
		else
			this.selected.add(item)

	}

	public resetNumberOfItems(): void {
		this.numberOfItems = 15
	}

	public async increaseNumberOfItems(ev: Event) : Promise<void> {

		const event : InfiniteScrollCustomEvent = (ev as InfiniteScrollCustomEvent) // workaround for https://github.com/ionic-team/ionic-framework/issues/24952

		this.numberOfItems +=10

		await event.target.complete()
	}


	public assertResult(x: unknown) : asserts x is Item[] {
		assert(Array.isArray(x), 						'ItemSelectModalComponent.assertResult() x is not an array.', x)
		assert(x.every( item => item instanceof Item), 	'ItemSelectModalComponent.assertResult() some array member is not and Item.', x)
	}

	public getResult() : Item[] {
		return Array.from(this.selected)
	}


	public ngOnDestroy(): void {

		this.destroy$.next()
		this.destroy$.complete()

	}
}
