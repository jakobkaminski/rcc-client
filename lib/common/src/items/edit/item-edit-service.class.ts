import	{
			Item,
		}									from '@rcc/core'

import	{	Type	}						from '@angular/core'

import	{
			RccModalController,
		}									from '../../modals-provider'

import	{	ItemEditResult				}	from './item-edit.commons'
import	{	ItemEditModalComponent		}	from './item-edit-modal.component'

export class ItemEditService<I extends Item = Item, Artifacts = never> {

	public constructor(
		protected itemClass				: Type<I>,
		protected itemEditModal			: Type<ItemEditModalComponent<I,Artifacts>>,
		protected rccModalController	: RccModalController,

	){}

	/**
	 * Opens modal to edit item instance (of type I).
	 *
	 * Will Resolve with an
	 * (edited) copy of the original item, when the user saved or applied
	 * changes, even if the data/config is the same as the original.
	 *
	 * Will reject, if editing is canceled in some way.
	 *
	 */
	public async editCopy(item: I, heading?: string) : Promise<ItemEditResult<I,Artifacts>> {

		const result	: 	unknown
						=	await this.rccModalController.present(
								this.itemEditModal,
								{ item, heading }
							)

		// `this.rccModalController.present` breaks the type inference,
		// but since we use a ModalWithResultComponent<ItemEditResult<I,Artifacts>>
		// the resulting type has to match ItemEditResult<I,Artifacts>.
		// `this.rccModalController.present` though just forwards whatever the
		// modal gets dismissed with, without checking the result.
		// That's why we have to verify the result ourselves so type inference
		// keeps working nicely :)


		let assertResult 	: (x:unknown) => asserts x is ItemEditResult<I,Artifacts> = undefined

		// For some reason  – even when adding `& {prototype: ItemEditModalComponent<I,Artifacts>} –
		// ts and eslint infer any type for this.itemEditModal.prototype
		assertResult   		= (this.itemEditModal.prototype as ItemEditModalComponent<I,Artifacts>).assertResult

		assertResult(result)

		return result
	}

	public async edit(item: I, heading?: string): Promise<ItemEditResult<I,Artifacts>> {

		const result	: 	ItemEditResult<I, Artifacts>
						= 	await this.editCopy(item, heading)


		if ( item ) item.config = result.item.config
		if (!item ) item 		= new this.itemClass(result.item.config)

		return { item, artifacts: result.artifacts }
	}

	public async create(heading? : string): Promise<ItemEditResult<I,Artifacts>> {

		return await this.edit(new this.itemClass(), heading)
	}

}
