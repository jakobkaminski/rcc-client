import	{
			Component,
		}									from '@angular/core'
import	{	Item						}	from '@rcc/core'
import	{	ModalWithResultComponent	}	from '../../overlays'
import	{	ItemEditResult				}	from './item-edit.commons'

@Component({
	template: ''
})
export abstract class ItemEditModalComponent<I extends Item, Artifacts = never> extends ModalWithResultComponent<ItemEditResult<I,Artifacts>>{

	public abstract set item(i : I)

}
