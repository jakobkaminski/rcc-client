import	{
			Injectable,
			OnDestroy
		}									from '@angular/core'
import	{	SubscriptionLike			}	from 'rxjs'
import	{	IncomingDataService			}	from '../incoming-data'


@Injectable()
export class DebugService implements OnDestroy{

	public	subscriptions	: SubscriptionLike[] = []

	public constructor(
		public incomingDataService			: IncomingDataService
	) {

		console.info('DebugModule running.')

		this.subscriptions.push(
			this.incomingDataService.subscribe( data => {
				console.group('IncomingDataService:')
				console.log(data)
				console.groupEnd()
			})
		)

	}

	ngOnDestroy(){
		this.subscriptions.forEach( sub => sub.unsubscribe() )
	}
}
