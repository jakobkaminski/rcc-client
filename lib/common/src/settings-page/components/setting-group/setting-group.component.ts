import { Component, Input } from '@angular/core'
import { Setting } from '@rcc/common/settings/settings.service'

@Component({
	selector	: 'rcc-setting-group',
	templateUrl	: './setting-group.component.html',
	styleUrls	: ['./setting-group.component.scss'],
})
export class SettingGroupComponent {
	@Input()
	public setting: Setting<unknown>
}
