import { Component, Input } from '@angular/core'
import { Setting } from '@rcc/common/settings/settings.service'

@Component({
	selector: 'rcc-time-setting',
	templateUrl: './time-setting.component.html',
})
export class TimeSettingComponent {
	@Input()
	public setting: Setting<string>
}
