import { Component, Input } from '@angular/core'
import { Setting } from '@rcc/common/settings/settings.service'

@Component({
	selector: 'rcc-handler-setting',
	templateUrl: './handler-setting.component.html',
	styleUrls: ['./handler-setting.component.scss']
})
export class HandlerSettingComponent {

	@Input()
	public setting: Setting<unknown>
}
