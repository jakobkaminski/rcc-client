import	{
			Component,
			OnInit,
		}								from '@angular/core'

import	{	ActivatedRoute			}	from '@angular/router'
import	{	Location				}	from '@angular/common'
import	{
			map
		}								from 'rxjs'

import	{
			Group,
			RccSettingsService,
			Setting,
		}								from '@rcc/common/settings/settings.service'

@Component({
	selector: 		'rcc-overview-page',
	templateUrl: 	'./overview-page.component.html',
	styleUrls:		['./overview-page.component.scss']
})
export class OverviewPageComponent implements OnInit {

	public settings			: Setting[]
	public label			: string
	public description		: string


	public constructor(
		public activatedRoute		: ActivatedRoute,
		private rccSettingsService	: RccSettingsService,
		public location				: Location
	){
		void this.setup()
	}

	public ngOnInit(): void {

		this.activatedRoute.paramMap
		.pipe(map(	params 	=> params.get('id')) )
		.subscribe( id 		=> void this.setup(id) )

	}

	public goBack(): void {
		this.location.back()
	}

	private async setup(groupId?: string): Promise<void>{

		await this.rccSettingsService.ready

		if(!groupId) {
			this.settings = this.rccSettingsService.rootSettings
			return
		}


		const group	:	Group
					= 	this.rccSettingsService.activeGroupsMap.get(groupId)

		this.settings 		= group.subSettings.filter(setting => setting.entry.restriction !== 'hidden')
		this.label			= group.groupSetting.entry.label
		this.description	= group.groupSetting.entry.description
	}

	protected isGroup(setting: Setting<unknown>): boolean {
		return setting.entry.type === 'group'
	}

	protected isSelect(setting: Setting<unknown>): boolean {
		return setting.entry.type === 'select' || setting.entry.type === 'multiple'
	}

	protected isBoolean(setting: Setting<unknown>): setting is Setting<boolean> {
		return setting.entry.type === 'boolean'
	}

	protected isTime(setting: Setting<unknown>): setting is Setting<string> {
		return setting.entry.type === 'time'
	}

	protected isString(setting: Setting<unknown>): setting is Setting<string> {
		return setting.entry.type === 'string'
	}

	protected hasHandler(setting: Setting<unknown>): boolean {
		return setting.entry.type === 'handler'
	}
}
