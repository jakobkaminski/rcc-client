import	{
			NgModule,
			ModuleWithProviders
		} 						from '@angular/core'

import	{
			CommonModule
		} 						from '@angular/common'


import	{
			ICON_MAP
		}						from './icons.commons'

import	{
			RccIconPipe
		}						from './icons.pipe'

import	{
			IconsService
		}						from './icons.service'

@NgModule({
	declarations: [
		RccIconPipe
	],
	imports: [
		CommonModule
	],
	providers: [
		IconsService,
		{ provide: ICON_MAP, useValue: new Map() }
	],
	exports: [
		RccIconPipe
	]
})
export class IconsModule {

	public static forChild(iconMap: Map<string, string>): ModuleWithProviders<IconsModule>{
		return	{
					ngModule: 	IconsModule,
					providers:	[{ provide: ICON_MAP, useValue: iconMap }]
				}
	}
}
