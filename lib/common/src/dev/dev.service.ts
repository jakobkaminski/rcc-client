import 	{
			Injectable,
		} 							from '@angular/core'

import	{	DevWarnings			}	from './dev.commons'


@Injectable()
export class DevService {

	public constructor(
		public warnings: DevWarnings
	){}

	public addWarning(name: string, note: string) : void {
		this.warnings.push({ name,note })
	}

}
