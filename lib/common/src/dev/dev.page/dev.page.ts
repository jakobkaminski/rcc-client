import	{	Component, OnInit		}	from '@angular/core'
import	{	DevWarnings				}	from '../dev.commons'

@Component({
	selector: 		'rcc-dev.page',
	templateUrl: 	'./dev.page.html',
	styleUrls: 		['./dev.page.scss'],
})
export class DevPageComponent implements OnInit {

	public constructor(
		public warnings : DevWarnings
	) { }

	ngOnInit() {}

}
