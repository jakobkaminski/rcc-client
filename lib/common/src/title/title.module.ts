
import	{	NgModule			}	from '@angular/core'
import	{
			Router,
			TitleStrategy
		}							from '@angular/router'
import	{	RccTitleService		}	from './title.service'
import	{	RccTitleStrategy	}	from './title-strategy.class'


@NgModule({
	providers:		[
		RccTitleService,
		{ provide: TitleStrategy, useClass: RccTitleStrategy }
	]

})
export class RccTitleModule {

	public constructor(
		private rccTitleService: RccTitleService,
		private router: Router
		) {
	}
}
