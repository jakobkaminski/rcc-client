import	{
			ApplicationInitStatus
		}										from '@angular/core'

import	{
			HttpClient,
		}										from '@angular/common/http'

import	{
			TestBed,
		}										from '@angular/core/testing'

import	{
			of,
			throwError
		}										from 'rxjs'

import	{
			RUNTIME_CONFIG_REQUESTED_VALUES,
			RequestedConfigValue
		}										from './public-runtime-config.commons'

import	{
			RccPublicRuntimeConfigService,
		}										from './public-runtime-config.service'

import	{
			RccPublicRuntimeConfigModule
		}										from './public-runtime-config.module'


async function getConfiguredService(

	requestedValues	: RequestedConfigValue[],
	config			: Record<string, unknown> | Error

) : Promise<RccPublicRuntimeConfigService> {

	const mockHttpClient 	: Record<string, jasmine.Spy>
							= jasmine.createSpyObj(['get']) as Record<string, jasmine.Spy>

	mockHttpClient.get.and.callFake( () => config instanceof Error ? throwError( () => config) : of(config) )

	TestBed
	.configureTestingModule({
		imports:[
			RccPublicRuntimeConfigModule
		],
		providers : [
			{
				provide: 	HttpClient,
				useValue:	mockHttpClient
			},
			{
				provide:	RUNTIME_CONFIG_REQUESTED_VALUES,
				useValue:	requestedValues
			},
			RccPublicRuntimeConfigService,
		]

	})

	await TestBed.inject(ApplicationInitStatus).donePromise

	return TestBed.inject(RccPublicRuntimeConfigService)
}








let configService	:	RccPublicRuntimeConfigService
					=	undefined

let config			: 	Record<string, Record<string, unknown>>
					=	undefined

let requestedValues	:	RequestedConfigValue[]
					=	undefined

describe('RccPublicRuntimeConfigService', () => {


	it('throws an error when loaded for invalid request values', async () => {

		requestedValues	=	[
								{
									path: 			'test.my_value',
									type: 			'number',
									description:	'my test value description'
								},
								{
									path: 			'test.my_value.sub_value',
									type: 			'string',
									description:	'my sub value description',
								},
							]

		config			=	{
								test: {
									my_value: 14,
									my_other_value: 'something different'
								}
							}


		await expectAsync(getConfiguredService(requestedValues, config)).toBeRejected()

	})

	describe('.ready', () => {

		it('rejects, if config is missing a required value', async () => {

			requestedValues	=	[
									{
										path: 			'test.my_value',
										type: 			'number',
										description:	'my test value description'
									},
									{
										path: 			'test.other_value',
										type: 			'string',
										description:	'my sub value description',
										required:		true
									},
								]

			config			=	{
									test: {
										my_value: 14,
									}
								}


			configService	=	await getConfiguredService(requestedValues, config)

			await expectAsync(configService.ready).toBeRejected()

		})

		it('rejects, if loading the config fails, but some values are required', async () => {

			requestedValues	=	[
									{
										path: 			'test.my_value',
										type: 			'number',
										description:	'my test value description',
										required:		true
									}
								]


			configService	=	await getConfiguredService(requestedValues, new Error() )

			await expectAsync(configService.ready).toBeRejected()
		})

		it('resolves, if loading the config fails and no values are required', async () => {

			requestedValues	=	[
									{
										path: 			'test.my_value',
										type: 			'number',
										description:	'my test value description',
									}
								]


			configService	=	await getConfiguredService(requestedValues, config)

			await expectAsync(configService.ready).toBeResolved()

		})

	})



	describe('with valid config and valid requested values', () => {


		beforeEach( async () : Promise<void> => {
			requestedValues	=	[
									{
										path: 			'test.my_value',
										type: 			'number',
										description:	'my test value description'
									},
									{
										path: 			'test.my_other_value',
										type: 			'string',
										description:	'my test value description',
										required:		true
									},
								]

			config			=	{
									test: {
										my_value: 14,
										my_other_value: 'something different'
									},

									value: {
										without:{
											request: '17'
										}
									},

									'path with spaces':{
										value: 'something something'
									},

									'path:WithSpecialCharaters*' : {
										something: 'something'
									}
								}

			configService =	await getConfiguredService(requestedValues, config)


		})

		describe('.get()', () => {

			it('returns the config value of the respective path', async () => {
				expect(await configService.get('test.my_value')).toBe(config.test.my_value)
				expect(await configService.get('test.my_other_value')).toBe(config.test.my_other_value)
			})

			it('rejects, if no value exists for a given path', async () => {
				await expectAsync(configService.get('test.does_not_exist')).toBeRejected()
			})

			it('rejects, if a value was not properly requested', async () => {
				await expectAsync(configService.get('value.without.request')).toBeRejected()
			})

		})

		describe('.configSchema', () => {

			it('reflects the requested values', () =>{

				const schema 	: Record<string, Record<string, unknown>>
								= configService.configSchema as Record<string, Record<string,unknown>>

				expect(schema.test.my_value).toContain('[optional]')
				expect(schema.test.my_value).toContain(requestedValues[0].type)
				expect(schema.test.my_value).toContain(requestedValues[0].description)

				expect(schema.test.my_other_value).not.toContain('[optional]')
				expect(schema.test.my_other_value).toContain(requestedValues[1].type)
				expect(schema.test.my_other_value).toContain(requestedValues[1].description)
			})


		})

	})

})
