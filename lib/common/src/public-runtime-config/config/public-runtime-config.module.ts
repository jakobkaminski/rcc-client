import	{
			NgModule,
		}										from '@angular/core'

import	{
			HttpClientModule
		}										from '@angular/common/http'

import	{
			RccPublicRuntimeConfigService
		}										from './public-runtime-config.service'


@NgModule({
	imports:[
		HttpClientModule
	],
	providers: [
		RccPublicRuntimeConfigService,
	]
})
export class RccPublicRuntimeConfigModule {}
