import	{
			NgModule,
		}											from '@angular/core'

import	{
			HttpClientModule
		}											from '@angular/common/http'

import	{
			RccPublicRuntimeTextResourceService
		}											from './public-runtime-text-resources.service'


@NgModule({
	imports:[
		HttpClientModule
	],
	providers: [
		RccPublicRuntimeTextResourceService,
	]
})
export class RccPublicRuntimeTextResourceModule {}
