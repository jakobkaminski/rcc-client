import	{
			InjectionToken,
			Provider
		}									from '@angular/core'

import	{
			assertProperty,
			assert
		}									from '@rcc/core'

/**
 * Whenever a module requests a runtime resource,
 * it should describe what the resource is needed for.
 *
 * Collecting requested resources at build time is important,
 * so that the app can prefetch them for caching in order to
 * enable offline usage.
 */
export interface RequestedTextResource {
	/**
	 * If language dependent, the name should include "%lang"
	 * to be replaced by the language code.
	 */
	name: 					string,
	description:			string,
	/**
	 * A resource us regraded language independent, if it doe not need translation.
	 *
	 * A language dependent resource on the other hand is a resources that comes with
	 * a separate version for each language; e.g. 'manual_%lang.md' refers to a set of
	 * markdown files, one markdown file for each available language: 'manual_de.md',
	 * 'manual_en.md'...
	 */
	languageIndependent?: 	boolean,
	required?:				boolean
}


export const REQUESTED_RUNTIME_TEXT_RESOURCES	: InjectionToken<string[]>
												= new InjectionToken<string[]>('List of runtime resources requested by various modules.')


export function assertRequestedTextResource(x:unknown): asserts x is RequestedTextResource {

	assertProperty(x, ['name', 'description'], 	'assertRequestedTextResource')
	assert(typeof x.name === 'string', 			'assertRequestedTextResource() .name must be a string')

	if( x.languageIndependent) assert(!x.name.includes('%lang'), 'assertRequestedTextResource() language independent resources must not include "%lang" in their names.')
	if(!x.languageIndependent) assert( x.name.includes('%lang'), 'assertRequestedTextResource() language dependent resources must include "%lang" in their names.')


}


/**
 * Announce to the rest of the app, that
 * a module will need this text resource during runtime.
 */
export function requestPublicRuntimeTextResource(requestedTextResource: RequestedTextResource) : Provider {

	assertRequestedTextResource(requestedTextResource)

	return {
		provide:	REQUESTED_RUNTIME_TEXT_RESOURCES,
					multi:		true,
					useValue:	requestedTextResource
	}
}

