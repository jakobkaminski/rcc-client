import	{
			HttpClient,
		}											from '@angular/common/http'



import	{
			WithValueTranslator,
			RccTranslationService
		}											from '@rcc/common'

import	{
			RequestedTextResource
		}											from './public-runtime-text-resources.commons'

import	{
			RccPublicRuntimeTextResourceService,
		}											from './public-runtime-text-resources.service'




function getConfiguredService(

	requestedTextResources	: RequestedTextResource[],
	data					: Record<string, string>

) : RccPublicRuntimeTextResourceService {


	const mockHttpClient 						: 	Record<string, jasmine.Spy>
												= 	jasmine.createSpyObj(['get']) as Record<string, jasmine.Spy>

	spyOn(RccPublicRuntimeTextResourceService.prototype,'load')
	.and.callFake(
		(name: string) => 	typeof data[name] === 'string'
							?	Promise.resolve(data[name])
							:	Promise.reject(new Error(String(data[name])))
	)

	const rccPublicRuntimeTextResourceService 	: 	RccPublicRuntimeTextResourceService
												= 	new RccPublicRuntimeTextResourceService(
														requestedTextResources,
														// will not be used in service, since .load will be mocked below:
														mockHttpClient as unknown as HttpClient,
														rccTranslationService
													)


	return rccPublicRuntimeTextResourceService
}

let rccTranslationService				:	RccTranslationService
										=	undefined

let rccPublicRuntimeTextResourceService	:	RccPublicRuntimeTextResourceService
										=	undefined

let data								: 	Record<string, string>
										=	undefined

let requestedTextResources				:	RequestedTextResource[]
										=	undefined


describe('RccPublicRuntimeTextResourceService', () => {

	beforeEach( () => {
		rccTranslationService	=	new RccTranslationService(
										[new WithValueTranslator()],
										document
									)

		data					=	undefined
		requestedTextResources	=	undefined

	})

	describe('constructor / .ready', () => {

		it('should run without errors on proper setup', async () => {

			requestedTextResources	= 	[
											{
												name: 					'my_text.txt',
												description: 			'example text',
												languageIndependent:	true,
												required:				false
											},
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data	=					{
											'my_text.txt': 		'My example text.',
											'lang_text_xx.md':	'Greeting: XX',
											'lang_text_yy.md':	'Greeting: YY',
										}

			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeResolved()

		})

		it('should run without errors, even when an optional language independent resource is missing', async () => {

			requestedTextResources	= 	[
											{
												name: 					'my_text.txt',
												description: 			'example text',
												languageIndependent:	true,
												required:				false
											},
										]

			data	=					{}

			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeResolved()

		})

		it('should run without errors, even when an optional language dependent resource is completely missing', async () => {

			requestedTextResources	= 	[
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data	=					{}

			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeResolved()

		})

		it('should trigger an unhandled error when multiple language independent resources use the same name, before loading resources', async () => {

			requestedTextResources	= 	[
											{
												name:					'popup_text.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											},
											{
												name:					'popup_text.md',
												description:			'another popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data	=					{
											'popup_text.md' : 'something'
										}

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeRejected()

			const loadSpy						:jasmine.Spy
												= rccPublicRuntimeTextResourceService['load'] as jasmine.Spy

			expect(loadSpy).not.toHaveBeenCalled()

		})

		it('should trigger an unhandled error when multiple language dependent resources use the same name, before loading resources', async () => {

			requestedTextResources	= 	[
											{
												name:					'xx_lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											},

											{
												name:					'%lang_lang_text_xx.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data	=					{
											'xx_lang_text_xx.md':	'Greeting: XX',
											'xx_lang_text_yy.md':	'Greeting: XY',
											'yy_lang_text_xx.md':	'Greeting: YX',
										}

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeRejected()

			const loadSpy						:jasmine.Spy
												= rccPublicRuntimeTextResourceService['load'] as jasmine.Spy

			expect(loadSpy).not.toHaveBeenCalled()

		})

		it('should trigger an unhandled error when multiple language in/dependent resources use the same name, before loading resources', async () => {

			requestedTextResources	= 	[
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											},

											{
												name:					'lang_text_xx.md',
												description:			'greetings popup',
												languageIndependent:	true,
												required:				false
											}
										]

			data	=					{
											'xx_lang_text_xx.md':	'Greeting: XX',
											'xx_lang_text_yy.md':	'Greeting: XY',
										}

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeRejected()

			const loadSpy						:jasmine.Spy
												= rccPublicRuntimeTextResourceService['load'] as jasmine.Spy

			expect(loadSpy).not.toHaveBeenCalled()

		})


		it('should trigger an unhandled error when a language independent resource is not available, but required', async () => {

			requestedTextResources	= 	[
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				true
											}
										]

			data	=					{}

			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeRejected()

		})

		it('should trigger an unhandled error when a language dependent resource is partially available, even if not required', async () => {

			requestedTextResources	= 	[
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data	=					{
											'lang_text_xx.md':	'Greeting: XX',
										}

			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeRejected()

		})

		it('should trigger an unhandled error when a language dependent resource is requested, even though the app has no language restrictions', async () => {

			requestedTextResources	= 	[
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data	=					{
											'lang_text_xx.md':	'Greeting: XX',
											'lang_text_yy.md':	'Greeting: YY',
										}

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeRejected()

		})

		it('should trigger an unhandled error when a language dependent resource is not available at all, but required', async () => {

			requestedTextResources	= 	[
											{
												name: 					'my_text.txt',
												description: 			'example text',
												languageIndependent:	true,
												required:				true
											},
										]

			data	=					{}

			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeRejected()

		})



		it('should prefetch all resources for all available languages', async () => {

			requestedTextResources	= 	[
											{
												name: 					'my_text.txt',
												description: 			'example text',
												languageIndependent:	true,
												required:				false
											},
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data	=					{
											'my_text.txt': 		'My example text.',
											'lang_text_xx.md':	'Greeting: XX',
											'lang_text_yy.md':	'Greeting: YY',
										}

			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService	= getConfiguredService(requestedTextResources, data)

			const loadSpy						:jasmine.Spy
												= rccPublicRuntimeTextResourceService['load'] as jasmine.Spy

			await expectAsync(rccPublicRuntimeTextResourceService.ready).toBeResolved()

			expect(loadSpy).toHaveBeenCalledTimes(3)

			await expectAsync(rccPublicRuntimeTextResourceService.get('my_text.txt')).toBeResolvedTo(data['my_text.txt'])

		})

	})

	describe('.get()', () => {

		it('should load language independent resource with a given name, if everything was setup properly', async () => {

			requestedTextResources	= 	[
											{
												name: 					'my_text.txt',
												description: 			'example text',
												languageIndependent:	true,
												required:				false
											},
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data					=	{
											'my_text.txt': 		'My example text.',
											'lang_text_xx.md':	'Greeting: XX',
											'lang_text_yy.md':	'Greeting: YY',
										}


			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService = getConfiguredService(requestedTextResources, data)

			await expectAsync(rccPublicRuntimeTextResourceService.get('my_text.txt')).toBeResolvedTo(data['my_text.txt'])

		})


	})

	describe('.getTranslations', () => {

		it('should load all translations of a language dependent resource with a given name, if everything was setup properly', async () => {

			requestedTextResources	= 	[
											{
												name:					'lang_text_%lang.md',
												description:			'greetings popup',
												languageIndependent:	false,
												required:				false
											}
										]

			data					=	{
											'lang_text_xx.md':	'Greeting: XX',
											'lang_text_yy.md':	'Greeting: YY',
										}

			rccTranslationService.restrictLanguagePool(['xx', 'yy'])

			rccPublicRuntimeTextResourceService = getConfiguredService(requestedTextResources, data)

			const translations		: Record<string, string>
									= await rccPublicRuntimeTextResourceService.getTranslations('lang_text_%lang.md')

			expect(translations).toEqual({ xx:'Greeting: XX', yy:'Greeting: YY' })

		})

	})

})
