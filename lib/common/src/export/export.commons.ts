import	{
			InjectionToken,
			Provider,
			Type
		}							from '@angular/core'
import	{	assert			}		from '@rcc/core'

export interface ExportWithString<T=unknown> {
	raw			: () => T
	toString	: () => string
	toBlob?		: () => Blob
}

export interface ExportWithBlob<T=unknown> {
	raw			: () => T
	toString?	: () => string
	toBlob		: () => Blob
}

/**
 * ExportResult contains the raw result of ExportService.export(),
 * as well as a string and/or a blob representation of the result.
 * E.g. export services that trigger downloads will return the file contents via toBlob()
 */
export type ExportResult<T=unknown> = ExportWithString<T> | ExportWithBlob<T>

/**
 * Export input
 */
export interface ExportConfig<F=unknown> {
	data			: F,
	context?		: unknown[],	// Whatever is needed for the export apart from data
	label?			: string,
	description?	: string
}

/**
 * Metadata for the export output, will be provided by an export service
 */
export interface ExportMetadata {
	suggestedFilename	: string,
	mimeType			: string
}

/**
 * Converts an ExportResult to a BlobPart using either `toString()` or `toBlob()`
 */
export function toBlobPart<T=unknown>(x: ExportResult<T>): BlobPart {
	let data : BlobPart = undefined
	if (x.toString)	data = x.toString()
	if (x.toBlob)	data = x.toBlob()
	assert(data, 'toBlobPart: ExportResult.toString() or .toBlob() must return a valid result')
	return data
}

/**
 * Exports from type F to type T
 */
export abstract class ExportService<F=unknown,T=unknown> {
	public abstract label			: string
	public abstract description		: string

	/**
	 * Checks if x is of the correct type.
	 * @param x `ExportConfig<F>.data`
	 * @param context `ExportConfig<F>.context` (optional)
	 */
	public abstract canExport(x: unknown, ...context: unknown[]): x is F

	/**
	 * Export from type F to type T
	 */
	public abstract export(x: ExportConfig<F>): Promise<ExportResult<T>> | ExportResult<T>

	/**
	 * Return {@link ExportMetadata} based on the export input
	 */
	public abstract getMetadata?(x: ExportConfig<F>): Promise<ExportMetadata> | ExportMetadata
}

export const EXPORT_SERVICES : InjectionToken<ExportService[]> = new InjectionToken<ExportService>('ExportServices')

/**
 * Adds an {@link ExportService} class to the {@link EXPORT_SERVICES} InjectionToken
 * The Provider[] also includes exportServiceClass itself,
 * so that we can easily provide the class to a module in a single line
 * with `@NgModule({ providers: [...provideExportService(MyExportService)] })`
 */
export function provideExportService<F,T>(exportServiceClass: Type<ExportService<F,T>>): Provider[] {
	return [
		exportServiceClass,
		{ provide: EXPORT_SERVICES, useExisting: exportServiceClass, multi: true }
	]
}
