import	{
			Type,
			Provider
		}								from '@angular/core'
export class RccQrCodeScanner {

	public async scan(): Promise<unknown> {
		return Promise.reject('RccQrCodeScanner: missing QR Code scanning service, please provide a service at QrCodeModule.')
	}
}

export function provideScanService(scanService: Type<RccQrCodeScanner>): Provider {
		return { provide: RccQrCodeScanner, useExisting: scanService }
}
