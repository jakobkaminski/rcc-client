import	{
			Optional,
			Inject,
			Injector,
			Type,
			Injectable,
		}								from '@angular/core'


import	{
			Item,
			ItemStore,
		}								from '@rcc/core'

import	{	RccModalController		}	from '../modals-provider'

import	{
			MetaStoreConfig,
			META_STORE_CONFIGS,
			MetaStore
		}								from './meta-store.class'


/**
 * This Service registers all MetaStores for easier access by item
 * or item type.
 */
@Injectable()
export class MetaStoreService {

	public 	metaStoreConfigs	: MetaStoreConfig<unknown,Item<unknown>,ItemStore<Item<unknown>>> []


	public constructor(
		@Optional() @Inject(META_STORE_CONFIGS)
		metaStoreConfigs				: MetaStoreConfig<unknown,Item<unknown>,ItemStore<Item<unknown>>> [],
		public injector					: Injector,
		public rccModalController		: RccModalController,
	){
		this.metaStoreConfigs 			= metaStoreConfigs || []
	}

	public getMetaStoreConfig<I extends Item>(item: 		I)			: MetaStoreConfig<unknown,I,ItemStore<I>>
	public getMetaStoreConfig<I extends Item>(itemClass:	Type<I>)	: MetaStoreConfig<unknown,I,ItemStore<I>>
	public getMetaStoreConfig<I extends Item>(value:		I|Type<I>)	: MetaStoreConfig<unknown,I,ItemStore<I>>
	public getMetaStoreConfig<I extends Item>(value:		I|Type<I>)	: MetaStoreConfig<unknown,I,ItemStore<I>> {

		if(value instanceof Item) return this.getMetaStoreConfig(value.constructor as Type<I>)

		const config 	: 	MetaStoreConfig<unknown,I,ItemStore<I>>
						= 	this.metaStoreConfigs.find(
								(item: MetaStoreConfig<unknown,I,ItemStore<I>>) =>
								item.itemClass === value
							) as MetaStoreConfig<unknown,I,ItemStore<I>>

		if(!config) throw new Error('MetaStoreService.getMetaStoreConfig(): MetaStoreConfig missing for '+value.name)
		return config
	}

	public getMetaStore<I extends Item>(item: 		I)				: MetaStore<unknown,I,ItemStore<I>>
	public getMetaStore<I extends Item>(itemClass:	Type<I>)		: MetaStore<unknown,I,ItemStore<I>>
	public getMetaStore<I extends Item>(value: 		I | Type<I>)	: MetaStore<unknown,I,ItemStore<I>>
	public getMetaStore<I extends Item>(value: 		I | Type<I>)	: MetaStore<unknown,I,ItemStore<I>> {

		const config : MetaStoreConfig<unknown,I,ItemStore<I>> = this.getMetaStoreConfig(value)
		return this.injector.get(config.serviceClass)

	}

	public getStore<I extends Item>(item: I): ItemStore<I> {
		return this.getMetaStore(item).getStore(item)
	}



	public async get<I extends Item>(itemClass: Type<I>, id			: string			): Promise<I | null>
	public async get<I extends Item>(itemClass: Type<I>, ids		: string[]			): Promise<(I | null)[]>
	public async get<I extends Item>(itemClass: Type<I>, id_or_ids 	: string | string[]	): Promise< I | null | (I|null)[] > {

		return await this.getMetaStore(itemClass).get(id_or_ids)

	}

}
