import	{
			Type,
			ModuleWithProviders,
			InjectionToken
		}							from '@angular/core'
import	{
			Item,
			ItemStore
		}							from '@rcc/core'
import	{
			MetaAction,
		}							from './meta-store.class'
import	{
			getProvider
		}							from '../interfaces'



/**
 * {@link ItemStore}s and {@link MetaAction}s can be registered directly at the
 * respective {@link MetaStore} by providing the right {@link InjectionToken}.
 * The base module class below abstracts the process in the form of static
 * method. So whenever you create a new MetaStore for one type of item and
 * provide it in a module, you should use the below base class for that module,
 * to have a shortcut for providing the above mentioned values.
 *
 * TODO: Replace this with provideMetaAction() and provideStore() function as in
 * other parts already done for various other {@link InjectionToken}s.
 */
export class BaseMetaStoreModule {

	public static getModuleWithProviders<T extends BaseMetaStoreModule>(
		module		: Type<T>,
		stores		: Type<ItemStore>[],
		metaActions	: MetaAction<unknown,Item<unknown>,ItemStore<Item<unknown>>>[],
		storeToken	: InjectionToken<unknown>,
		metaToken	: unknown
	) : ModuleWithProviders<T> {

		return 	{
					ngModule:	module,
					providers:	[
									...(stores||[]).map( storeClass	=> ({ provide: storeToken,	useExisting: 	storeClass, 	multi:true })),
									...(metaActions||[]).map( x => getProvider(metaToken, 	x, true ) )
								]
				}
	}

}
