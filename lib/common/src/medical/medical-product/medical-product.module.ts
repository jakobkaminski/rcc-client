import	{	NgModule					} 	from '@angular/core'
import	{	IS_MEDICAL_PRODUCT			}	from '../medical.commons'

@NgModule({
	providers: [
		{ provide: IS_MEDICAL_PRODUCT, multi: false, useValue: true }
	]
})
export class MedicalProductModule {}
