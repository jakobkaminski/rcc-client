import 	{
			Inject,
			Injector,
			NgModule,
			Optional,
			Type
		} 									from '@angular/core'
import 	{
			IS_MEDICAL_PRODUCT
		} 									from '../medical.commons'

@NgModule({})
export class MedicalFeatureModule {
	public constructor(
		@Inject(IS_MEDICAL_PRODUCT) @Optional()
		private isMedicalProduct	: boolean,
		private injector			: Injector
	) {


		if (isMedicalProduct !== true){
			console.group('Modules importing MedicalFeatureModule:')
			this.guessParentModules().forEach( module => console.info( module ))
			console.groupEnd()

			throw new Error('Medical features must only be included in a product marked for medical use. Import the MedicalProductModule to flag a product for medical use.')
		}
	}

	public guessParentModules() : Array<Type<unknown>>{
		// Disabling eslint, because it complains about ts being ignored.
		/* eslint-disable */
		// @ts-ignore
		const defTypes 		: Type<unknown>[] = Array.from(this.injector.injectorDefTypes)

		// @ts-ignore
		return defTypes.filter( defType => defType.ɵmod?.imports?.includes(MedicalFeatureModule))
		/* eslint-enable */
	}
}
