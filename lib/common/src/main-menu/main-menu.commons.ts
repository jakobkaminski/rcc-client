import	{
			InjectionToken,
			Type,
			Provider
		} 								from '@angular/core'

import	{
			Action,
			CustomAction
		}								from '../actions'

import	{
			ProductOrFactory,
			getProvider
		}								from '../interfaces'

export type MainMenuEntryComponent = Type<unknown>

export type MainMenuEntry = MainMenuEntryComponent | Action | CustomAction

export const MAIN_MENU_COMPONENT		: InjectionToken<Type<unknown>> 			= new InjectionToken<Type<unknown>>('Component used as main menu')
export const MAIN_MENU_CONFIG			: InjectionToken<Record<string, unknown>>	= new InjectionToken<Record<string, unknown>>('Configuration object for main menu component')
export const MAIN_MENU_ENTRIES			: InjectionToken<MainMenuEntry[]> 			= new InjectionToken<MainMenuEntry[]>('List of regular main menu entries')
export const MAIN_MENU_ENTRIES_META		: InjectionToken<MainMenuEntry[]> 			= new InjectionToken<MainMenuEntry[]>('List of main menu entries, that do not add to the functionality of the app, such as legal pages.')

/**
* Add a regular entry to the main menu.
*/
export function provideMainMenuEntry(entry: ProductOrFactory<MainMenuEntry>) : Provider{
	return 	getProvider(MAIN_MENU_ENTRIES, entry, true)
}

/**
* Add a meta menu entry to the main menu. These entries are reserved for menu
* entries, that do not add functionality to the app, such as legal pages.
*/
export function provideMainMenuMetaEntry(entry: ProductOrFactory<MainMenuEntry>) : Provider{
	return 	getProvider(MAIN_MENU_ENTRIES_META, entry, true)
}
