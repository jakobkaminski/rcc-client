import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'

@Injectable({
	providedIn: 'root'
})
export class MainMenuService {
	private _isMenuOpen$: BehaviorSubject<boolean> = new BehaviorSubject(false)
	public get isMenuOpen$(): Observable<boolean> {
		return this._isMenuOpen$.asObservable()
	}

	public setIsMenuOpen(isOpen: boolean): void {
		this._isMenuOpen$.next(isOpen)
	}
}
