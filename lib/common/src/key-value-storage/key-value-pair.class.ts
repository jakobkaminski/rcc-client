import { assert, has, isErrorFree, Item } from '@rcc/core'

export interface KeyValuePairConfig<T=unknown> {
    key     : string
    value   : T
}

export function assertKeyValuePairConfig(x: unknown): asserts x is KeyValuePairConfig {
    assert(has(x, 'key'), 'assertKeyValuePairConfig: Missing .key property')
    assert(has(x, 'value'), 'assertKeyValuePairConfig: Missing .value property')
    assert(Object.keys(x).length === 2, 'assertKeyValuePairConfig: Has more properties than key and value')
}

export function isKeyValuePairConfig(x: unknown): x is KeyValuePairConfig {
    return isErrorFree(() => assertKeyValuePairConfig(x))
}

export class KeyValuePair<T=unknown> extends Item<KeyValuePairConfig> {

    public key     : string
    public value   : T

    public acceptsAsConfig(config: unknown): boolean {
        return isKeyValuePairConfig(config)
    }

    public set config(config: KeyValuePairConfig<T>) {
        assertKeyValuePairConfig(config)
        this.key    = config.key
        this.value  = config.value
    }

    public get config(): KeyValuePairConfig<T> {
        return {
            key     : this.key,
            value   : this.value
        }
    }
}
