import  {
			Injectable,
			InjectionToken,
			Injector,
			Provider
		}                           from '@angular/core'
import  {
			ItemStorage
		}                           from '@rcc/core'
import  {
			getProvider,
			ProductOrFactory
		}                           from '../interfaces'
import  {
			RccStorage
		}                           from '../storage-provider'
import  {
			KeyValuePair,
			KeyValuePairConfig
		}                           from './key-value-pair.class'


export const KEYVALUESTORAGE_CONFIGS	: InjectionToken<KeyValuePairConfig<unknown>[]>
										= new InjectionToken('KeyValueStorage')

export function provideKeyValuePair<T>(config: ProductOrFactory<KeyValuePairConfig<T>>) : Provider {
	return getProvider(KEYVALUESTORAGE_CONFIGS, config, true)
}

/**
 * Service for a simple key-value ({@link KeyValuePair}) storage.
 * Can be used to store miscellaneous data that is not complicated enough to warrant its own storage.
 */
@Injectable({
	providedIn: 'root'
})
export class 	RccKeyValueStorageService {

	public		ready				: Promise<void>
	protected	storage				: ItemStorage<KeyValuePair>
	private		allKeyValuePairs	: Record<string, KeyValuePair>	= {}

	public constructor(
		protected rccStorage : RccStorage,
		private injector : Injector,
	) {
		this.ready = this.setup()
	}

	private async setup(): Promise<void> {
		this.storage = this.rccStorage.createItemStorage<KeyValuePair>('rcc-key-value')
		const storedConfigs : KeyValuePairConfig<unknown>[] = await this.storage.getAll()

		const configs : KeyValuePairConfig<unknown>[] = this.injector.get(KEYVALUESTORAGE_CONFIGS, [])

		// Create new KeyValuePair instances from configs
		configs.forEach(config => this.allKeyValuePairs[config.key] = new KeyValuePair(config))

		// Overwrite values from config with values from storage if available
		storedConfigs.forEach(kvpairConfig => this.allKeyValuePairs[kvpairConfig.key] = new KeyValuePair(kvpairConfig))

		await this.storage.store(Object.values(this.allKeyValuePairs))

	}

	public async get<T=unknown>(key: string): Promise<KeyValuePair<T>|undefined> {
		await this.ready
		return this.allKeyValuePairs[key] as KeyValuePair<T>
	}

	public async set<T>(key: string, value: T): Promise<void> {
		await this.ready

		this.allKeyValuePairs[key] = new KeyValuePair({ key, value })
		await this.storage.store(Object.values(this.allKeyValuePairs))
	}
}
