import	{	Injectable					}	from '@angular/core'
import	{	Subject						}	from 'rxjs'

export type IncomingDataType = 'questions' | 'answers'

@Injectable()
export class IncomingDataService extends Subject<unknown> {}
