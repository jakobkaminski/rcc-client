import	{	NgModule					}	from '@angular/core'
import	{	IncomingDataService				}	from './incoming-data.service'


@NgModule({
	providers: [
		IncomingDataService
	]
})
export class IncomingDataServiceModule {}
