import	{
			assert,
			assertProperty,
			isErrorFree
		}										from '@rcc/core'

export interface ScheduledNotification {
	[index:string]:		unknown
	timestamp:			number
	data?:				Record<string, string|number|boolean>
}


export function assertScheduledNotification(x:unknown): asserts x is ScheduledNotification {
	assertProperty(x, 'timestamp')
	assert(typeof x.timestamp === 'number', '.timestamp is not a number')
}

export function isScheduledNotification(x:unknown): x is ScheduledNotification {
	return isErrorFree( () => assertScheduledNotification(x) )
}


