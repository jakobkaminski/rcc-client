import	{
			NgModule,
		}											from '@angular/core'

import	{	RccServiceWorkerModule				}	from '../service-worker'
import	{	SettingsEntry, provideSettingsEntry				}	from '../settings'
import	{	provideTranslationMap				}	from '../translations'

import	{	ScheduledNotificationService		}	from './scheduled-notification.service'


import en from './i18n/en.json'
import de from './i18n/de.json'


const settingsEntryGroup : SettingsEntry = {
	id: 			'noficiation_group',
	type:			'group',
	label:			'SCHEDULED_NOTIFICATIONS.SETTINGS.NOTIFICATIONS_GROUP.LABEL',
	description:	'SCHEDULED_NOTIFICATIONS.SETTINGS.NOTIFICATIONS_GROUP.DESCRIPTION',
	icon:			'notfification',
	subSettingIds:	['notifications-enabled'] 		// will be filled automatically
}

const settingsEntry				: SettingsEntry	=	{
	id:				'notifications-enabled',
	label:			'SCHEDULED_NOTIFICATIONS.SETTINGS.NOTIFICATIONS_ENABLED.LABEL',
	description:	'SCHEDULED_NOTIFICATIONS.SETTINGS.NOTIFICATIONS_ENABLED.DESCRIPTION',
	defaultValue:	true,
	type:			'boolean' as const,
	icon:			'notfification'
}

@NgModule({
	imports:[
		RccServiceWorkerModule
	],
	providers:[
		ScheduledNotificationService,
		provideSettingsEntry(settingsEntryGroup),
		provideSettingsEntry(settingsEntry),
		provideTranslationMap('SCHEDULED_NOTIFICATIONS', { en,de })
	]
})
export class ScheduledNotificationModule {}
