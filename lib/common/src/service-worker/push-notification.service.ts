import	{	Injectable					}	from '@angular/core'

import	{	RccAlertController			}	from '../modals-provider'
import	{	RccSwService				}	from './service-worker.service'



@Injectable()
export class RccPushNotificationService {


	public ready 				: Promise<void>

	public constructor(
		private rccSwService		: 	RccSwService,
		private rccAlertController	: 	RccAlertController,
	){
		this.ready = this.setup()
	}


	protected async setup() : Promise<void> {

		await this.rccSwService.ready

	}

	private requestInProgress: boolean = false
	private requestFinished: Promise<void>

	public async askPermission(): Promise<void> {
		if (this.requestInProgress) return await this.requestFinished

		this.requestInProgress = true

		this.requestFinished = this.showAlertModal()
		await this.requestFinished

		this.requestInProgress = false
	}

	private async showAlertModal(): Promise<void> {
		await 	this.ready

		await	this.rccAlertController.present({
					header:		'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.HEADER',
					message: 	'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.MESSAGE',
					buttons:	[
									{ label: 'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.NO', 	rejectAs: 	'user_no'	},
									{ label: 'SERVICE_WORKER.NOTIFICATIONS.PERMISSION.YES', resolveAs: 	'user_yes'	},
								]
				})
	}

	public async getSubscription(serverPublicKey: string): Promise<PushSubscription>{

		await this.ready

		const pushManager	: PushManager		= this.rccSwService.serviceWorkerRegistration.pushManager

		let subscription	: PushSubscription	= await pushManager.getSubscription()

		if(subscription) return subscription

		const permission	: PermissionState	= await pushManager.permissionState()

		if(permission === 'prompt')	await this.askPermission()
		if(permission === 'denied')	throw new Error('permission denied')

		const userVisibleOnly		: boolean	= false
		const applicationServerKey	: string	= serverPublicKey

		subscription 		= await pushManager.subscribe({ userVisibleOnly, applicationServerKey })

		return subscription
	}
}
