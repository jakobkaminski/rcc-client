import { ModuleWithProviders, NgModule } from '@angular/core'
import { provideTranslationMap } from '../translations'
import { PathAction, WithPosition, getEffectivePosition } from '../actions'
import { provideMainMenuEntry } from '../main-menu'

const de: Record<string, string> = {
	LABEL: 'Hilfe & Benutzerhandbuch'
}

const en: Record<string, string> = {
	LABEL: 'Help & User manual'
}

@NgModule({
	providers: [
		provideTranslationMap('SUPPORT_MENU_ENTRY', { de, en }),
	],
})
export class SupportMainMenuEntryModule {
	public static addEntry(config?: WithPosition): ModuleWithProviders<SupportMainMenuEntryModule> {
		const mainMenuEntry: PathAction = {
			position: getEffectivePosition(config, 1),
			icon: 'help',
			path: 'support',
			label: 'SUPPORT_MENU_ENTRY.LABEL'
		}

		return {
			ngModule:	SupportMainMenuEntryModule,
			providers:	[
				provideMainMenuEntry(mainMenuEntry)
			]
		}
	}
}
