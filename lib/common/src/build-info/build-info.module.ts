import  {   NgModule            }   from '@angular/core'
import  {   RccBuildInfoService }   from './build-info.service'

@NgModule({
	providers: [
		RccBuildInfoService,
	]
})
export class BuildInfoModule {}
