import	{	Injectable				}	from '@angular/core'
import	{	RccAlertController		}	from '../modals-provider'
import	{	getDelayedPromise		}	from '@rcc/core'

export interface RetryAlertConfig {
	successMsg		: string	// When callback is successful
	retryMsg		: string	// When callback fails (offer to retry)
	confirmLabel	: string	// Confirm retry
	cancelLabel		: string	// Cancel retry
}

@Injectable()
export class RccRetryService {

	public constructor(
		private rccAlertController : RccAlertController
	) {}

	/**
	 * Calls an async function and display success/failure alerts.
	 * If the call failed, offers to try the call again.
	 * @param callback Async function to try
	 * @param config {@link RetryAlertConfig} object containing all alert messages
	 * @param delay How many ms to wait before retrying. Defaults to 0
	 */
	public async retry(callback: () => Promise<unknown>, config: RetryAlertConfig, delay: number = 0): Promise<void> {
		await callback()
		.then(
			() 	=>	this.rccAlertController.present({ message: config.successMsg }),
			() 	=>	this.rccAlertController.confirm(config.retryMsg, config.confirmLabel, config.cancelLabel)
					// Delay retry to give the user the impression that something is "happening"
					.then(() => getDelayedPromise(delay))
					.then(() => this.retry(callback, config))
		)
	}

}
