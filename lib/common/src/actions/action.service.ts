import	{
			Injectable,
			Inject
		}							from '@angular/core'
import	{	DOCUMENT			}	from '@angular/common'
import	{	Router				}	from '@angular/router'
import	{	assert 				}	from '@rcc/core'
import	{
			RccAlertController,
			RccToastController
		}							from '../modals-provider'

import	{
			Action,
			PathAction,
			HandlerAction,
			DownloadAction,
			isPathAction,
			isHandlerAction,
			isDownloadAction
		}							from './actions.commons'


@Injectable({
	providedIn: 'root'
})
export class ActionService {

	public constructor(
		private rccAlertController	: RccAlertController,
		private rccToastController	: RccToastController,
		private router				: Router,
		@Inject(DOCUMENT)
		private document			: Document
	){}


	public executePathAction(action: PathAction) : void {
		void this.router.navigateByUrl(action.path)
	}

	/**
	 * Executes the given {@link HandlerAction}. If `confirmMessage` property is present,
	 * ask for confirmation beforehand. Triggers toasts with `successMessage`
	 * resp. `failureMessage`.
	 */
	public async executeHandlerAction(action: HandlerAction) : Promise<void> {

		if(action.confirmMessage) 		await this.rccAlertController.confirm(action.confirmMessage)

		try {
			await action.handler()
			if (action.successMessage)	void this.rccToastController.success(action.successMessage)
		}catch(e){
			if (action.failureMessage){
				void this.rccToastController.failure(action.failureMessage)
				console.error(e)
			}
			else throw e

		}

	}

	private async getBlobFromDownloadAction(action: DownloadAction) : Promise<Blob> {
		function isPromise(x: unknown): x is Promise<unknown> {
			return x instanceof Promise
		}

		function isCallback(x: unknown): x is (() => Promise<unknown>) {
			return typeof x === 'function'
		}

		let data : BlobPart = undefined
		if (isPromise(action.data))			data = await action.data
		if (isCallback(action.data))		data = await action.data()
		if (!isPromise(action.data)
			&& !isCallback(action.data))	data = action.data
		if (data instanceof Blob) return data

		assert(data, 'RccActionService.getBlobFromDownloadAction(): DownloadAction missing .data')

		const mimeType : string = action.mimeType || 'text/plain'

		return new Blob([data], { type: mimeType })
	}

	public async executeDownloadAction(action: DownloadAction) : Promise<void> {

		const element	: HTMLAnchorElement	= this.document.createElement('a')

		const blob 		: Blob				= await this.getBlobFromDownloadAction(action)

		element.href 		= URL.createObjectURL(blob)
		element.download 	= this.getFileName(action)

		element.click()

	}

	private getFileName(action: DownloadAction): string {
		return typeof action.filename === 'string' ? action.filename : action.filename()
	}

	public async execute(action: Action): Promise<void> {
		if(isPathAction(action))		void this.executePathAction(action)
		if(isHandlerAction(action))		void await this.executeHandlerAction(action)
		if(isDownloadAction(action))	void await this.executeDownloadAction(action)
	}



}
