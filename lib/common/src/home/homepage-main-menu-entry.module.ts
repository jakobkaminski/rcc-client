import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'
import	{
			Action,
			getEffectivePosition,
			WithPosition
		}											from '../actions'

import	{
			provideMainMenuEntry,
		}											from '../main-menu'

import	{
			HomePageModule,
			HomePageHomePath
		}											from './homepage.module'


@NgModule({
	imports: [
		HomePageModule,
	],

})

export class HomePageMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `HomePageMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<HomePageMainMenuEntryModule> {

		const mainMenuEntry	: Action =	{
				position:		getEffectivePosition(config, .5),
				icon: 			'home',
				label: 			'HOMEPAGE.MENU_ENTRY',
				path:			HomePageHomePath,
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	HomePageMainMenuEntryModule,
			providers
		}
	}

}
