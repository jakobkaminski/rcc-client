import 	{
			Component,

		}										from '@angular/core'
import	{	Observable						}	from 'rxjs'
import	{
			BackupRestoreHelper,
		}										from '@rcc/features/backup/backup-restore.helper'
import	{	NotificationService 			}	from '../notifications'
import	{	Action							}	from '../actions'
import	{
			HomePageEntry,
			ItemEntry,
		}										from './homepage.commons'
import	{	RccHomePageEntryService			}	from './homepage-entry.service'
import	{	RccBannerService				}	from '../banners/banner-service/banner.service'

@Component({
				selector: 		'rcc-home',
				templateUrl: 	'homepage.component.html',
				styleUrls:		['homepage.component.scss'],
			})
export class HomePageComponent {

	public constructor(
		public notificationService		: NotificationService,
		backupRestoreHelper				: BackupRestoreHelper,
		public rccHomePageEntryService	: RccHomePageEntryService,
		protected rccBannerService		: RccBannerService,
	) {
		backupRestoreHelper.showToastAfterReload()
	}

	public isItemEntry(entry: HomePageEntry) : entry is ItemEntry {
		return 'item' in entry
	}
	public isAction(entry: HomePageEntry) : entry is Action {
		return !('item' in entry)
	}

	public get activeEntries$(): Observable<HomePageEntry[]> {
		return 	this.rccHomePageEntryService.activeEntries$
	}

}
