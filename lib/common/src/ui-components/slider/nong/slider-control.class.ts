import	{
			BehaviorSubject,
			Subject
		}								from 'rxjs'


export interface SliderConfig {
	/**
	 * Sets whether or not you can keep sliding into one direction. If true when sliding to the next slide at the end
	 * (resp. the previous slide at the start), the slider will slide to the first (resp. last element). Default is false.
	 */
	loopAround?		: boolean,

	/**
	 * Selector string as used in [.querySelectorAll](https://developer.mozilla.org/en-US/docs/Web/API/Document/querySelectorAll).
	 * Determines which child elements of the container will be treated as slides. Default is all child elements.
	 */
	slidesSelector?	: string,

	snapStop?		: boolean
}


/**
 * This class is going to be the base class for {@link RccSliderComponent}.
 * It wraps a html element and turns it into a horizontal slider.
 * The element itself is considered the container of the slides,
 * and by default any direct child element is considered to be a slide.
 *
 * This class works without any reference to angular.
 *
 * _Important_: In order for the slider to actually work, you have to set some CSS rules yourself.
 * The rules may vary, depending on your use case; here's a working example for full width horizontal slides:
 *
 * Say #my-slider is the id of your HTML-element you want to use SliderControl on; and .slide is a class of your slide elements.
 *
 * ```css
 * 	#my-slider {
 * 		display:					flex;
 * 		flex-direction:				row;
 * 		overflow-x:					scroll;
 * 		scroll-behavior:			smooth;
 * 		scroll-snap-type:			x mandatory;
 * 	}
 *
 * 	.slide {
 * 		scroll-snap-stop:			always;
 * 		scroll-snap-align:			center;
 * 		flex:						0 0 100%;
 * 		height:						100%;
 * 	}
 *
 * ```
 *
 * Note: If your slides are smaller then their container and scroll-snap to one side,
 * it is possible that you won't be able to scroll all the way to the other side,
 * because the last/first slide cannot snap properly. To mitigate you can add
 * a pseudo-element with width 0, add padding or scroll-padding to the container.
 *
 */
export class SliderControl {

	/**
	 * Observable emitting the current slide, whenever it changes
	 * (due to e.g. scrolling).
	 */
	public slideChange$ 	: BehaviorSubject<HTMLElement|null>		=	new BehaviorSubject<HTMLElement|null>(null)


	/**
	 * Observable emitting after slide elements have changes (added, removed or moved).
	 */
	public setupChange$ 	: Subject<HTMLElement[]>				=	new Subject<HTMLElement[]>()

	/**
	 * States whether or not the slider is at its start. The slider is considered to be at its start,
	 * if the first slide is fully visible.
	 */
	public atStart 			: boolean								=	true

	/**
	 * States whether currently the first slide is focused
	 */
	public atFirstPage		: boolean								=	true

	/**
	 * States whether or not the slider is at its end. The slider is considered to be at its end,
	 * if the last slide is fully visible.
	 */
	public atEnd 			: boolean								=	true

	/**
	 * States whether currently the last slide is focused
	 */
	public atLastPage		: boolean								=	true


	/**
	 * Callback called when changes to the slides have been detected.
	 * Called with latest slide / position before the changes took effect.
	 * Expected to return some value, the current slide can be restored from (see. {@link #restoreState}, below)
	 */
	public storeState		: (slide?:HTMLElement, position?: number) => unknown

	/**
	 * Callback called after changes took effect. Called wit the previously stored value. (see, {@link #storeState}, above)
	 */
	public restoreState		: (x:unknown) => void

	public constructor(
		/**
		 * The element to be wrapped and turned into a slider
		 */
		public element	: HTMLElement,
		public config	: SliderConfig = {}
	){

		if(!window) console.error('SliderControl: missing global window; maybe needs different implementation for non-browser contexts.')


		const defaultConfig : SliderConfig = {

			loopAround: 	false,
			slidesSelector:	':scope > *',
			snapStop:		this.element.hasAttribute('snap-stop')

		}

		this.config = Object.assign(config || {}, defaultConfig)

		// Check if elements are added or removed:
		const mutation_observer : MutationObserver 	= new MutationObserver(	mutationRecords => this.handleMutations(mutationRecords) )
		// Check if elements are resized (e.g. at orientation change)
		const resize_observer 	: ResizeObserver	= new ResizeObserver( () => this.handleResize() )

		mutation_observer.observe(this.element, { childList:true })

		/**
		 * At the moment only observes resizing of the container element.
		 * It might be useful to also check on slides, but that seems to happen rarely
		 * without the container resizing at the same time. Providing for such a case
		 * seems too be overly complex and not yet worth it.
		 */
		resize_observer.observe(this.element)

		// When scrolling occurs check if current slide changed:
		this.element.addEventListener('scroll', () => this.updateStatus(), { passive:true })

		// Initial Check:
		this.updateStatus()
	}

	protected handleMutations(mutationRecords: MutationRecord[]) : void {

		const addedNodes 	: Node[]
							= mutationRecords
								.map( record => Array.from(record.addedNodes))
								.flat()

		const removedNodes 	: Node[]
							= mutationRecords
								.map( record => Array.from(record.removedNodes))
								.flat()

		const changedNodes 	: Node[]
							= [...addedNodes, ...removedNodes]

		const needsHandling	: boolean
							= changedNodes.some( node => node.nodeType === 1) // Element

		if(needsHandling) this.handleChanges()
	}

	protected handleResize() : void{
		this.handleChanges()
	}



	protected getScrollSnapAlign(slide: HTMLElement): string {
		return window.getComputedStyle(slide).getPropertyValue('scroll-snap-align')
	}


	protected scrollSnapCheck : boolean = false

	/**
	 * Gets all child elements that are considered to be slides.
	 */
	public get slides(): HTMLElement[] {
		return Array.from(this.element.querySelectorAll(this.config.slidesSelector))
	}

	/**
	 * The currently focused slide. Using this getter will not reevaluate the actual
	 * slide positions, but return the last known focused slide since the last change.
	 */
	public get currentSlide(): HTMLElement {
		return this.slideChange$.getValue()
	}

	/**
	 * The currently focused slide's position. Position _n_ means n/th/ place amoung
	 * the elements considered to be slides as displayed; starting at 0.
	 * (The display order of slides may be different to the order in the DOM tree, due to e.g. flex order)
	 * Using this getter will not reevaluate the actual slide positions,
	 * but return the position of the last known focused slide since the last change.
	 */
	public get currentSlidePosition(): number {

		const ordered_slides : HTMLElement[]	=  this.slides.sort(this.visualOrderSortFn)
		const pos			 : number			=  ordered_slides.indexOf(this.currentSlide)

		return pos

	}

	/**
	 * This method is meant to be used by Array.sort() in order to sort slides by visual order,
	 * DOM order might be different (e.g. due to flex order).
	 * Uses .offsetLeft to determine the visual order.
	 */
	protected visualOrderSortFn(this: void, slide1 : HTMLElement, slide2 : HTMLElement): number {

		if(slide1.offsetLeft  > slide2.offsetLeft) return  1
		if(slide1.offsetLeft  < slide2.offsetLeft) return -1

		return  0

	}

	/**
	 * Gets the width of the visible horizontal portion of the slide.
	 * This method is used internally.
	 */
	protected getVisiblePortion(slide : HTMLElement): number {
		const scrollStart 	: number	= this.element.scrollLeft
		const clientWidth 	: number	= this.element.clientWidth
		const scrollEnd 	: number	= scrollStart + clientWidth

		const visible_left 	: number	= Math.max(slide.offsetLeft, scrollStart)
		const visible_right	: number	= Math.min(slide.offsetLeft + slide.offsetWidth, scrollEnd)

		return Math.max(0, visible_right - visible_left)
	}

	/**
	 * Unlike the getter .currentSlide above, this method will actually evaluate the positions
	 * of all the slides and return the current slide.
	 */
	public getCurrentSlide(): HTMLElement {

		const scrollStart 		: number		= this.element.scrollLeft
		const clientWidth		: number		= this.element.clientWidth
		const scrollEnd			: number		= scrollStart + clientWidth

		const visible_slides 	: HTMLElement[]  = this.slides
									// remove slides that are out of view to left
									.filter( slide => slide.offsetLeft + slide.offsetWidth	>= scrollStart)
									// remove slides that are out of view to right
									.filter( slide => slide.offsetLeft 						<= scrollEnd)
									// sort by size of visible horizontal portion
									.sort( (s1, s2) => {

										const visibleH1 : number = this.getVisiblePortion(s1)
										const visibleH2 : number = this.getVisiblePortion(s2)

										if( visibleH1  < visibleH2) return  1
										if( visibleH1  > visibleH2) return -1

										return  0
									})

		return visible_slides[0] || null
	}

	/**
	 * Inverse of {@link getCurrentSlide}. Slides the provided slide into view, such that it will be the next current slide.
	 * This method is used internally, for all the sliding that is done programmatically.
	 */
	protected focus(slide: HTMLElement, behavior : string = 'smooth') : void {

		const slide_start 		: number 	= slide.offsetLeft
		const slide_width 		: number 	= slide.offsetWidth
		const slide_end 		: number 	= slide_start + slide_width
		const slide_center 		: number 	= slide_start + slide_width / 2

		const container_width 	: number	= this.element.clientWidth

		const align				: string	= this.getScrollSnapAlign(slide)

		let destination			: number	= slide_center - container_width / 2 // default case treated as if align was 'center'

		if(align === 'start')	destination = slide_start
		if(align === 'end')		destination = slide_end - container_width

		this.element.scroll({ left:destination, behavior: behavior as ScrollBehavior })

		// Firefox will not trigger scroll snap after non-smooth scroll
		// This fixes it:
		if(behavior === 'instant') this.element.scroll({ left:destination, behavior: 'smooth' })

	}

	/**
	 * Method to advance slider by _amount_ slides (use a negative number to slide backwards)
	 * starting from either the current slide, a provided slide or slide at given position.
	 * This method is used internally.
	 */
	protected relativeSlide(amount: number, from? : HTMLElement | number, behavior : string = 'smooth'): void {

		const 	slides: HTMLElement[]			=	this.slides

		if(slides.length === 0)	return

		const 	ordered_slides 	: HTMLElement[]	= 	slides.sort(this.visualOrderSortFn) // this sorts the slides in place - is that intended?
		const 	count 			: number		=	ordered_slides.length

		let 	from_position 	: number		= 	ordered_slides.indexOf(this.currentSlide) || 0

		if(typeof from === 'number')		from_position = (count + from) % count
		if(from instanceof HTMLElement) 	from_position = ordered_slides.indexOf(from)

		let 	new_position	: number		=	from_position + amount


		new_position 			=	this.config.loopAround
									?	(count + new_position) % count
									:	Math.min( Math.max( 0, new_position), count -1)

		this.focus(ordered_slides[new_position], behavior)

	}

	/**
	 * Advance the slider for _amount_ slides (use negative number to slide backwards).
	 */
	public slideBy(amount: number, behavior : string = 'smooth'): void {

		this.relativeSlide(amount, null, behavior)
	}

	/**
	 * Slide provided slide or slide at given position into view.
	 */
	public slideTo(target: number | HTMLElement, behavior : string = 'smooth') : void {

		if(target instanceof HTMLElement)	this.focus(target)
		if(typeof target === 'number')		this.relativeSlide(0, target, behavior)

	}

	/**
	 * Slide next slide into view.
	 */
	public next()		:void { this.slideBy( 1) }

	/**
	 * Slide previous slide into view.
	 */
	public previous()	:void { this.slideBy(-1) }


	/**
	 * This property is used to throttle reactions to changes in DOM structure or resizing, when they happen frequently.
	 */
	protected changing	: {requestId?: number, slideElement?: HTMLElement, slidePosition? :number, state?: unknown } = {}

	/**
	 * This method is called whenever changes to the setup of elements happen,
	 * that might change the current slide unintentionally or distort the current view.
	 * Ensures that after the change the current slide is still the same and that scroll snap
	 * will be triggered, so that all slides snap properly
	 * (e.g. some browsers will not re-snap on resize).
	 *
	 * This method is throttled, because it might be called very frequently
	 * (e.g. manual resize of the window by dragging the mouse).
	 */
	protected handleChanges(): void {

		if(!this.changing.requestId){

			// Record state when the changes happen
			this.changing.slideElement	= 	this.currentSlide
			this.changing.slidePosition	= 	this.currentSlidePosition
			this.changing.state			= 	typeof this.storeState === 'function'
											?	this.storeState(this.currentSlide,this.currentSlidePosition)
											:	null
		} else

			window.clearTimeout(this.changing.requestId)




		// Ensures that DOM changes (and everything it might trigger) happens at most once per animation frame:
		this.changing.requestId = window.setTimeout( () => {

			const hasCustomRestore : boolean = typeof this.restoreState === 'function'

			if( hasCustomRestore)	this.restoreState(this.changing.state)

			if(!hasCustomRestore)
									if		(this.slides.includes(this.changing.slideElement))
											this.slideTo(this.changing.slideElement, 'instant')
									else	this.slideTo(this.changing.slidePosition, 'instant')

			this.changing = {}

			if(this.config.snapStop) this.slides.forEach( slide => slide.style.setProperty('scroll-snap-stop', 'always'))

			this.setupChange$.next(this.slides)

			this.updateStatus()

		}, 64)
	}

	/**
	 * This property is used to throttle {@link updateStatus}, which will possibly be called frequently.
	 */
	protected updatingStatus : boolean	=	false


	/**
	 * Checks various states, such as currentSlide, atStart and atEnd.
	 *
	 * Checks if a new slide should be considered the current slide,
	 * if so emit that slide with .slideChange$.
	 *
	 * This method is throttled, because it might be called very frequently
	 * (most common case: scrolling)
	 */
	public updateStatus(): void {


		// Check if the component is currently handling DOM changes:
		if(this.changing.requestId) return

		if(this.updatingStatus) 	return

		this.updatingStatus = true

		// Ensures that sliding (and everything it might trigger) happens at most once per animation frame:
		window.requestAnimationFrame( () => {

			const ordered_slides 	: HTMLElement[]		= this.slides.sort(this.visualOrderSortFn)
			const first_slide 		: HTMLElement		= ordered_slides[0]
			const last_slide		: HTMLElement		= ordered_slides[ordered_slides.length-1]

			// Accounting for rounding deviations
			this.atStart 	= !first_slide	|| Math.abs(this.getVisiblePortion(first_slide)	- first_slide.offsetWidth)	< 2
			this.atEnd		= !last_slide	|| Math.abs(this.getVisiblePortion(last_slide)	- last_slide.offsetWidth)	< 2


			const past_slide 	: HTMLElement 	= this.slideChange$.getValue()
			const current_slide : HTMLElement	= this.getCurrentSlide()

			this.atFirstPage = current_slide === first_slide
			this.atLastPage = current_slide === last_slide

			if(past_slide !== current_slide) this.slideChange$.next(current_slide)

			this.updatingStatus = false

		})


	}

	/**
	 * Completes all associated Observables.
	 */
	public complete(): void {
		this.slideChange$.complete()
		this.setupChange$.complete()
	}
}
