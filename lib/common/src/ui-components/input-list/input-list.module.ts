import	{	NgModule				} from '@angular/core'
import	{	RccInputListDirective			} from './input-list.directive'

@NgModule({
	declarations:[
		RccInputListDirective,
	],
	exports: [
		RccInputListDirective,
	]
})
export class RccInputListModule {}
