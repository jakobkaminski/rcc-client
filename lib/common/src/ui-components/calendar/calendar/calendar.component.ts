import	{
			Component,
			Input,
			Output,
			OnChanges,
			AfterViewInit,
			ViewChild,
			EventEmitter,
			HostBinding
		}							from '@angular/core'

import	{
			CalendarDateString
		}							from '@rcc/core'

import	{	RccSliderComponent 	}	from '../../slider'

import	{
			FormControl
		}							from '@angular/forms'


export interface CalendarTickConfig {
	[index: string]:	[number,number] 	// index should be a date string; e.g. '2022-01-01'
}


@Component({
	selector:		'rcc-calendar',
	templateUrl:	'./calendar.component.html',
	styleUrls:		['./calendar.component.css']
})
export class RccCalendarComponent implements OnChanges, AfterViewInit {

	@Input()
	public startDate		: string 		// '2022-01-01'

	@Input()
	public endDate			: string		// '2022-01-03'

	@Input()
	public tickConfig		: CalendarTickConfig = {}

	@Input()
	public dateControl		: FormControl

	@Input()
	public color			: string	= 'medium'

	@Input()
	public highlight		: string	= 'primary'

	@Input()
	public snap				: string	= 'right'

	@Output()
	public dateChange		= new EventEmitter<string>()

	@HostBinding('class')
	public get snapClass(): Record<string, boolean> { return { [`snap-${this.snap}`]: true } }

	@ViewChild(RccSliderComponent)
	public slider: RccSliderComponent

	public dateStrings		: string[] 			= []


	public update(): void {

		if(typeof this.startDate 	!== 'string') 	return null
		if(typeof this.endDate 		!== 'string') 	return null
		if(!this.slider)							return null

		this.dateStrings 	= CalendarDateString.range(this.startDate, this.endDate)
	}

	public selectDate(dateString: string) : Promise<void> {

		if(!this.dateControl) 	return

		this.dateControl.setValue(dateString)

		this.dateChange.emit(dateString)

		this.focus(dateString)

	}

	public focus(dateString : string = this.dateControl?.value as string) : void {

		if(typeof dateString !== 'string') 	return
		if(!this.slider)					return


		const targetSlideIndex 	= this.dateStrings.indexOf(dateString)
		const currentSlideIndex	= this.slider.currentSlidePosition

		// If everything is in place already:
		if(currentSlideIndex === targetSlideIndex) return

		this.slider.slideTo( targetSlideIndex )

	}

	public ngAfterViewInit() : void {
		this.slider.restoreState = () => this.focus()
	}

	public ngOnChanges()		: void { void this.update()	}

}
