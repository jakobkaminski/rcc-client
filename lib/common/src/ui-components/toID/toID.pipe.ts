import	{
			Pipe, PipeTransform
		}								from '@angular/core'

@Pipe({
	name : 'toID'
})
export class RccToIDPipe implements PipeTransform {

	public transform(x: string, prefix: string = ''): string {

		if(typeof x !== 'string') return x

		const noCamelCaseX      = x.replace(/([a-z])([A-Z]+)/g, this.resolveCamelCase )
		const lowerCaseX        = noCamelCaseX.toLowerCase()
		const ID                = lowerCaseX.replace(/[^a-z0-9]/g,'-')

		return	prefix
				? prefix + '-' + ID
				: ID
	}

	public resolveCamelCase(match: string, s1: string, s2: string) : string {
		return s1+'-'+s2.toLowerCase()
	}
}
