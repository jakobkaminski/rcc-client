import	{	NgModule			} from '@angular/core'
import	{	RccToIDPipe			} from './toID.pipe'

@NgModule({
	declarations:[
        RccToIDPipe
	],
	exports: [
        RccToIDPipe
	]
})
export class RccToIDPipeModule {

}
