import	{
			InjectionToken,
			Provider,
		}							from '@angular/core'

import	{
			getProvider
		}							from '../../interfaces'

export const LOGO_VARIANTS: InjectionToken<LogoVariants> = new InjectionToken<LogoVariants>('Logo')

export interface LogoVariants {

	full		:	string // name of full logo image file in assets folder
	minimal		:	string // name of minimal logo image file in assets folder

}

const missingSVG : string = 'data:image/svg+xml,%3Csvg width="$WIDTH" height="$HEIGHT" viewBox="0 0 $WIDTH $HEIGHT" xmlns="http://www.w3.org/2000/svg"%3E%3Cg stroke-width="2" stroke="%23000000"%3E%3Cpath d="M 0,0 L $WIDTH,$HEIGHT z" /%3E%3Cpath d="M 0,$HEIGHT L $WIDTH,0 z" /%3E%3Cpath fill = "none" d="M 0,0 L $WIDTH,0 L $WIDTH,$HEIGHT L 0,$HEIGHT L 0,0 z" /%3E%3C/g%3E%3C/svg%3E'

export const missingLogoVariants: LogoVariants = {

	full: 		missingSVG
				.replace(/\$WIDTH/g,		'200')
				.replace(/\$HEIGHT/g, 	'32'),

	minimal: 	missingSVG
				.replace(/\$WIDTH/g,		'32')
				.replace(/\$HEIGHT/g, 	'32'),

}

export function provideLogoVariants(logoVariants: LogoVariants): Provider {

	return 	getProvider(LOGO_VARIANTS, logoVariants)

}
