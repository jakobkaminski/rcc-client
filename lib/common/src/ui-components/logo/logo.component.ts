import	{
			Component,
			Inject,
			Input,
			Optional
		}										from '@angular/core'

import	{	LOGO_VARIANTS,
			LogoVariants,
			missingLogoVariants
		}										from './logo.commons'
import	{
			TranslationsModule,
		}										from '../../translations'
@Component({
		selector:		'rcc-logo',
		templateUrl:	'./logo.component.html',
		styleUrls:		['./logo.component.css'],
		standalone:		true,
		imports:[		TranslationsModule
		]
	})

export class RccLogoComponent {

	@Input()
	public set size(s: keyof LogoVariants) {
		this.logoSource  = this.logoVariants && this.logoVariants[s] || missingLogoVariants[s]
	}

	protected fallbackLogoSource: string	= undefined

	public logoSource: string 				= undefined

	public constructor(

		@Optional() @Inject(LOGO_VARIANTS)
		public logoVariants: LogoVariants

	){
		if (!logoVariants) console.warn('RccLogoComponent: Missing logoVariants: please use provideLogoVariants().')

		// default size:
		this.size = 'minimal'
	}


}
