import	{
			Provider,
		}										from '@angular/core'

import	{	ComponentFixture,
			TestBed
		}										from '@angular/core/testing'

import	{	RccLogoComponent				}	from './logo.component'

import	{	LOGO_VARIANTS,
			LogoVariants,
			missingLogoVariants,
			provideLogoVariants
		}										from './logo.commons'

import	{
			getProvider
		}										from '../../interfaces'




describe('provideLogoVariants', () => {

	const logoVariants 	: LogoVariants	= 	{ full: 'svg/rcc-logo-full.svg', minimal: 'svg/rcc-logo-minimal.svg' }

	it('should create a provider for each logo variants', () => {

		const provider 	: Provider		= 	provideLogoVariants(logoVariants)

		expect(provider).toEqual(getProvider(LOGO_VARIANTS, logoVariants))

	})

	it('should provide LogoVariants for the proper injectionToken', () => {

		const variants 	: LogoVariants	= 	TestBed.configureTestingModule({
												providers: [
													provideLogoVariants(logoVariants)
												]
											})
											.inject(LOGO_VARIANTS)

		expect(variants).toEqual(logoVariants)
	})


})


describe('RccLogoComponent with provided logo variants', () => {

	const fullLogoVariantSVG	: string = 'data:image/svg+xml,%3Csvg width="200" height="32" viewBox="0 0 200 32" fill="none" xmlns="http://www.w3.org/2000/svg"%3E%3C/svg%3E'
	const minimalLogoVariantSVG	: string = 'data:image/svg+xml,%3Csvg width="50"  height="50" viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg"%3E%3C/svg%3E'
	const logoVariants 			: LogoVariants = { full: fullLogoVariantSVG, minimal: minimalLogoVariantSVG }


	let component		: RccLogoComponent						= undefined
	let fixture			: ComponentFixture<RccLogoComponent>	= undefined
	let element			: HTMLElement							= undefined

	beforeEach(() =>  {

		TestBed.configureTestingModule({
			providers: [
				provideLogoVariants(logoVariants),
				RccLogoComponent
			]
		})

		fixture 	= TestBed.createComponent(RccLogoComponent)
		component 	= fixture.componentInstance
		element		= fixture.nativeElement as HTMLElement
		fixture.detectChanges()
	})

	it('should compile into a component', () => {
		expect(component).toBeDefined()
	})

	it('should use minimal variant if no size is given', () => {
		expect(component.logoSource).toEqual(logoVariants.minimal)
	})

	it('should set logoSource depending on size property', () => {

		component.size = 'minimal'

		expect(component.logoSource).toEqual(logoVariants.minimal)

		component.size = 'full'

		expect(component.logoSource).toEqual(logoVariants.full)

	})

	it('should have the same dimensions as provided variants', async () => {

		component.size = 'minimal'
		fixture.detectChanges()

		await new Promise(r => setTimeout(r, 200) )

		expect(element.offsetHeight).toEqual(50)
		expect(element.offsetWidth).toEqual(50)

		component.size = 'full'
		fixture.detectChanges()

		await new Promise(r => setTimeout(r, 200) )

		expect(element.offsetHeight).toEqual(32)
		expect(element.offsetWidth).toEqual(200)
	})

	it('should keep aspect ratio when height changes', async () => {

		element.style.height = '64px'

		component.size = 'minimal'
		fixture.detectChanges()

		await new Promise(r => setTimeout(r, 200) )

		expect(element.offsetHeight).toEqual(64)
		expect(element.offsetWidth).toEqual(64)

		component.size = 'full'
		fixture.detectChanges()

		await new Promise(r => setTimeout(r, 200) )

		expect(element.offsetHeight).toEqual(64)
		expect(element.offsetWidth).toEqual(400)

		element.style.height = 'auto'
	})


})

describe('RccLogoComponent without provided logo variants', () => {

	it('should have fallback values', () => {
		expect(missingLogoVariants).toBeDefined()
	})

	it('should use missingLogoVariants as fallback', () => {

		TestBed.configureTestingModule({
			providers: [
				RccLogoComponent
			]
		})

		const fixture 	: ComponentFixture<RccLogoComponent> 	= TestBed.createComponent(RccLogoComponent)
		const component : RccLogoComponent						= fixture.componentInstance

		expect(component).toBeDefined()

		component.size	= 'full'
		fixture.detectChanges()

		expect(component.logoSource).toEqual(missingLogoVariants.full)

		component.size	= 'minimal'
		fixture.detectChanges()

		expect(component.logoSource).toEqual(missingLogoVariants.minimal)


	})
})
