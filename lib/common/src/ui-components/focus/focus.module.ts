import 	{	NgModule				} from '@angular/core'
import	{	RccFocusDirective		} from './focus.directive'

@NgModule({
	declarations:[
		RccFocusDirective
	],
	exports:[
		RccFocusDirective
	]
})
export class RccFocusModule {}
