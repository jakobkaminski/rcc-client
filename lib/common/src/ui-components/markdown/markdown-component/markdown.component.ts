import 	{
			Component,
			Input
		} 									from '@angular/core'
import	{
			NgIf
		}									from '@angular/common'

import 	{
			MarkdownPipe
		} 									from '../markdown-pipe'


@Component({
	selector	: 'rcc-markdown',
	templateUrl	: './markdown.component.html',
	standalone	: true,
	imports		: [
		NgIf,
		MarkdownPipe,
	]
})
export class RccMarkdownComponent{

	@Input()
	public content		: string

	@Input()
	public inline		: boolean
}
