import	{	NgModule				} from '@angular/core'
import	{	SortByPositionPipe		} from './sort-by-position.pipe'

@NgModule({
	declarations:[
        SortByPositionPipe
	],
	exports: [
        SortByPositionPipe
	]
})
export class SortByPositionPipeModule {

}
