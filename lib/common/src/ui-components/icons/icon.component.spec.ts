import	{
			ComponentFixture,
			TestBed
		}							from '@angular/core/testing'
import	{
			RccIconComponent
		}							from	'./icon.component'
import	{	RccIconService		}	from	'./icon.service'


describe('RccIconComponent', () => {

	let component		: RccIconComponent						= undefined
	let nativeElement	: HTMLElement							= undefined
	let fixture			: ComponentFixture<RccIconComponent>	= undefined
	let rccIconService	: RccIconService						= undefined
	
	const testSVG: string = `
								<svg width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg">
									<g id="$NAME"></g>
									<g id="$SIZE"></g>
								</svg>
							`
	beforeEach( async () => {

		await 	TestBed
				.configureTestingModule({
					providers: [
						RccIconService
					]
				})
				.compileComponents()

		rccIconService	= TestBed.inject(RccIconService)

		;['test1', 'test2', 'test3']
		.map((name: string) => ({
			name,
			iconVariants: {
				small: testSVG.replace('$NAME', name).replace('$SIZE', 'small'),
				medium: testSVG.replace('$NAME', name).replace('$SIZE', 'medium'),
				large: testSVG.replace('$NAME', name).replace('$SIZE', 'large'),
			}
		}))
		.forEach( iconConfig => rccIconService.addIcon(iconConfig) )

		fixture 		= TestBed.createComponent(RccIconComponent)
		component		= fixture.componentInstance
		nativeElement	= fixture.nativeElement as HTMLElement

		fixture.detectChanges()

    
	})

	it('should display a medium size icon by default', () => {

		component.name = 'test1'

		expect(nativeElement.querySelector('#medium')).toBeTruthy()

	})

	it('should change the class when the size attribute changes', () => {

		component.size = 'large'

		component.name = 'test1'

		// Because of HostBinding() we need to wait for the change to propagate.
		fixture.detectChanges()

		expect(nativeElement.classList.contains('icon-large')).toBeTruthy()

		component.size = 'medium'

		fixture.detectChanges()

		expect(nativeElement.classList.contains('icon-medium')).toBeTruthy()

		component.size = 'small'

		fixture.detectChanges()

		expect(nativeElement.classList.contains('icon-small')).toBeTruthy()
	})



	it('should change the icon when the size attribute changes', () => {

		component.size = 'large'

		component.name = 'test1'

		expect(nativeElement.querySelector('#large')).toBeTruthy()

		component.size = 'medium'

		expect(nativeElement.querySelector('#medium')).toBeTruthy()

		component.size = 'small'

		expect(nativeElement.querySelector('#small')).toBeTruthy()
	})


	it('should change the icon when the name attribute changes', () => {

		component.size = 'large'

		component.name = 'test1'

		expect(nativeElement.querySelector('#test1')).toBeTruthy()

		component.name = 'test2'

		expect(nativeElement.querySelector('#test2')).toBeTruthy()

		component.name = 'test3'

		expect(nativeElement.querySelector('#test3')).toBeTruthy()
	})

	it('should change to missing-icon when the value of the name attribute is not recognized', () => {

		component.name = 'test4'

		expect(nativeElement.querySelector('#missing-icon')).toBeTruthy()

	})

	it('should not show any icon if .name is falsy', () => {

		component.size = 'large'

		expect(nativeElement.querySelector('svg')).toBe(null)

	})


	it('should not show any icon if .name is falsy, even when it had a proper value before', () => {

		component.name = 'missing'
		component.size = 'large'

		expect(nativeElement.querySelector('svg')).toBeTruthy()

		component.name = undefined

		expect(nativeElement.querySelector('svg')).toBe(null)

		component.name = null

		expect(nativeElement.querySelector('svg')).toBe(null)

		component.name = ''

		expect(nativeElement.querySelector('svg')).toBe(null)


	})

})
