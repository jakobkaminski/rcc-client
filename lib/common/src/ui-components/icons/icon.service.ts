import	{
			Injectable,
			Inject,
			Optional
		}									from '@angular/core'

import	{
			assert
		}									from '@rcc/core'

import	{
			IconVariants,
			IconConfig,
			IconAlias,
			ICONS
		}									from './icon.commons'

import	{
			IconName
		}									from './icon-names'

export interface ResolvedIconName {
	icon		:	IconName,
	aliases		:	IconName[]
}


@Injectable({
	providedIn: 'root'
})
export class RccIconService{

	protected iconRegistry		: 	Map<string,IconVariants>
								= 	new Map<string,IconVariants>()

	protected aliasRegistry		:	Map<string,IconAlias>
								=	new Map<string,IconAlias>()

	public constructor(
		@Optional() @Inject(ICONS)
		private icons	: (IconConfig | IconAlias)[]
	){

		this.addMissingIcon()
		this.setupInitialIcons(icons)
	}

	/**
	 * Ensures that at instantiation provided icons are added in a
	 * favorable order, such that aliases come after regular icons.
	 *
	 * Without this an alias might be tried to be added before the regular
	 * icon it refers to, thus causing an error.
	 */
	protected setupInitialIcons(icons: (IconConfig | IconAlias)[]) : void {

		const normalIcons 			: 	IconConfig[]
									= 	(icons||[]).filter( (icon): icon is IconConfig	=> this.isIconConfig(icon) )

		const iconAliases			: 	IconAlias[]
									= 	(icons||[]).filter( (icon): icon is IconAlias 	=> this.isIconAlias(icon) )

		const sortedIconAliases: IconAlias [] = []
		iconAliases.forEach((iconAlias: IconAlias) => {
			const indexOfSuccessor: number = sortedIconAliases.findIndex((sortedIconAlias: IconAlias) =>
				this.isPredecessor(sortedIconAlias, iconAlias, iconAliases)
			)
			if(indexOfSuccessor >= 0) {
				sortedIconAliases.splice(indexOfSuccessor, 0, iconAlias)
				return
			}
			
			const indexOfPredecessor: number = sortedIconAliases.findIndex((sortedIconAlias: IconAlias) =>
					this.isPredecessor(iconAlias, sortedIconAlias, iconAliases)
			)
			if(indexOfPredecessor >= 0) {
				sortedIconAliases.splice(indexOfPredecessor + 1, 0, iconAlias)
				return
			}

			sortedIconAliases.push(iconAlias)
		})

		;[...normalIcons, ...sortedIconAliases].forEach( (icon : IconConfig | IconAlias) => this.addIcon(icon) )

	}

	/**
	 * The first predecessor of an IconAlias in a haystack is
	 * another IconAlias in the same haystack, such that the
	 * original IconAlias's .from property equals the name
	 * of that predecessors.
	 * Predecessors of predecessors are also predecessors.
	 *
	 * This method returns a sorted Array of all predecessors of a
	 * given IconAlias, starting wit hthe closest predecessor.
	 */
	protected getPredecessors(iconAlias: IconAlias, haystack: IconAlias[], successors: IconAlias[] = []) : IconAlias[]  {
		const predecessor		:	IconAlias
								= 	haystack.find( ia => ia.name === iconAlias.from)

		assert(!successors.includes(predecessor), `RccIconService.setupInitialIcons() circular aliases -> '${iconAlias.name}'`)

		if(!predecessor) return successors

		return this.getPredecessors(predecessor, haystack, [...successors, predecessor])
	}

	/**
	 * Checks if one IconAlias is a predecessor of another; see {@link getPredecessors}
	 */
	protected isPredecessor(iconAlias: IconAlias, possiblePredecessor: IconAlias, haystack : IconAlias[]) : boolean {

		const predecessors		:	IconAlias[]
								=	this.getPredecessors(iconAlias, haystack)

		return predecessors.includes(possiblePredecessor)
	}

	/**
	 * The missing-icon is used, when another icon cannot be displayed;
	 * e.g. no icon is associated with a given icon name.
	 * This helps with prototyping and spotting broken or missing icons.
	 */
	protected addMissingIcon() : void {

		const missingSVG 		: 	string
								= 	`
										<svg id="missing-icon" width="$WIDTH" height="$HEIGHT" viewBox="0 0 $WIDTH $HEIGHT" xmlns="http://www.w3.org/2000/svg">
											<g stroke-width="2" stroke="#000000">
												<path d="M 0,0 L $WIDTH,$HEIGHT z" />
												<path d="M 0,$HEIGHT L $WIDTH,0 z" />
												<path stroke-width = "4" d="M 0,0 L $WIDTH,0 L $WIDTH,$HEIGHT L 0,$HEIGHT L 0,0 z" />
											</g>
										</svg>
									`

		const large				: 	string
								= 	missingSVG
									.replace(/\$WIDTH/g,		'128')
									.replace(/\$HEIGHT/g, 		'128')

		const medium			: 	string
								=	missingSVG
									.replace(/\$WIDTH/g,		'64')
									.replace(/\$HEIGHT/g, 		'64')

		const small				:	string
								=	missingSVG
									.replace(/\$WIDTH/g,		'32')
									.replace(/\$HEIGHT/g, 		'32')

		const missingVariants	: 	IconVariants
								=	{ small, medium, large }

		const missingConfig		:	IconConfig
								=	{ name: 'missing', iconVariants: missingVariants }

		this.addIcon(missingConfig)
										
	}


	public isIconConfig(x: (IconConfig | IconAlias)) : x is IconConfig {
		return 'iconVariants' in x
	}

	public isIconAlias(x: (IconConfig | IconAlias)) : x is IconAlias {
		return 'from' in x
	}

	/**
	 * Add icons or aliases.
	 *
	 * Duplicate names will throw an error, unless `forceReplace == true`,
	 * then the following rules apply:
	 *
	 * * An icon with the same name as an existing icon,
	 *   will replace the existing icon.
	 *
	 * * An alias with the same name as an existing alias,
	 *   will replace the existing alias
	 *
	 * * An icon and an alias are allowed to have the same name,
	 *   but when requested (see {@link getIconVariants}) the icon takes
	 *   precedence.
	 *
	 * @param forceReplace boolean 	If true suppresses duplicate name errors.
	 */
	public addIcon(icon: (IconConfig | IconAlias), forceReplace : boolean = false): void {

		const nameExistsForIcon	: boolean
								= this.iconRegistry.has(icon.name)

		const nameExistsForAlias: boolean
								= this.aliasRegistry.has(icon.name)

		assert(!nameExistsForIcon	|| forceReplace, `RccIconService.addIcon(): name already registered for an icon: ${icon.name}; use forceReplace to suppress this error.`)
		assert(!nameExistsForAlias	|| forceReplace, `RccIconService.addIcon(): name already registered for an alias: ${icon.name}; use forceReplace to suppress this error.`)

		if( this.isIconConfig(icon) )
			this.iconRegistry.set(icon.name, icon.iconVariants)

		if( this.isIconAlias(icon) ){
			assert( icon.name !== icon.from, 		`RccIconService.addIcon(): alias .name and .from must be different: ${icon.name}`)
			assert( this.resolveName(icon.from),	`RccIconService.addIcon(): alias .from of '${icon.name}' does not resolve to anything: ${icon.from}`)
			this.aliasRegistry.set(icon.name, icon)
		}

	}

	/**
	 * Recursively resolves a provided icon name or alias.
	 *
	 * Throws an error when the process encounters a loop.
	 *
	 * @param previousNames string[] Used to keep track of previous steps and
	 * prevent infinite recursion loops.
	 */

	public resolveName(iconName : IconName, previousNames : string[] = []) :  ResolvedIconName | undefined {

		// recursive!

		if(previousNames.includes(iconName))
			throw new Error(`RccIconService.resolveName() reference loop for aliases: ${JSON.stringify(previousNames)}`)

		const nameExistsForIcon	: boolean
								= this.iconRegistry.has(iconName)

		const nameExistsForAlias: boolean
								= this.aliasRegistry.has(iconName)


		if(nameExistsForIcon) 	return { icon: iconName, aliases : previousNames }

		if(nameExistsForAlias){

			const iconAlias	:	IconAlias
							=	this.aliasRegistry.get(iconName)

			const fromName	:	string
							=	iconAlias.from

			return this.resolveName(fromName, [...previousNames, iconName])
		}

		console.warn(`RccIconService.resolveName() Unable to find icon/alias with the name of ${iconName}, did you add it to the iconRegistry?`)

		return undefined
	}

	/**
	 * Returns `IconVariants` for the provided icon name.Throws an error, when
	 * an alias loop is encountered.
	 *
	 * If an alias has the same name as an icon, the icon takes precedence.
	 */
	public getIconVariants(iconName: IconName): IconVariants | undefined {

		const resolvedIconName 	: ResolvedIconName
								= this.resolveName(iconName)

		const effectiveIconName	: IconName
								= resolvedIconName?.icon

		return this.iconRegistry.get(effectiveIconName)
	
	}

	/**
	 * Returns the effective transformation for an alias, ready to be used in
	 * the CSS property 'transform'. If an alias points to another alias the
	 * results are merged, such that transformations of every step
	 * stack (multiple rotations will add up).
	 *
	 * Throws an error, when an alias loop is encountered.
	 *
	 * Returns the empty string, if the provided iconName does not refer to an
	 * `IconAlias` (e.g. just does not exists, or points to a regular icon).
	 */
	public getTransformation(iconName: IconName): string {

		const resolvedIconName 	: 	ResolvedIconName
								= 	this.resolveName(iconName)

		const passedAliases		: 	IconName[]
								= 	resolvedIconName?.aliases || []

		const mergedTransform	: 	string
								= 	passedAliases
									.map( name => this.aliasRegistry.get(name)?.transform || '' )
									.join('')

		return 	mergedTransform
	}


}
