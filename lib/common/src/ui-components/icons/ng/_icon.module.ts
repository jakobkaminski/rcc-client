import	{	NgModule			}	from '@angular/core'
import	{	IonicModule			}	from '@ionic/angular'
import	{	RccIconComponent	}	from './_icon.component'
import	{	IconsModule			}	from '../../../icons'


@NgModule({
	imports: [
		IonicModule,
		IconsModule	// legacy
	],
	declarations: [
		RccIconComponent
	],
	exports: [
		RccIconComponent
	]
})
export class RccIconModule{}
