import	{
			Component,
			Input
		}								from '@angular/core'


@Component({
	selector:	'rcc-icon',
	template:	'<ion-icon [name] ="name | rccIcon" [size] = "size"></ion-icon>'
})
export class RccIconComponent {

	@Input()
	public name: string

	@Input()
	public size: string

}
