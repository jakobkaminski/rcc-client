import	{
			Component,
			Input,
			ElementRef,
			HostBinding,
			ChangeDetectionStrategy,
			ViewEncapsulation,
		}									from '@angular/core'

import	{	IconName					 }	from './icon-names'

import	{
			RccIconService,
		}									from './icon.service'
import	{
			IconVariants
		}									from './icon.commons'


@Component({
	selector:			'rcc-icon',
	template:			'',
	styleUrls:			['icon.component.css'],
	changeDetection:	ChangeDetectionStrategy.OnPush,
	standalone:			true,
	encapsulation:		ViewEncapsulation.None

})

/**
 * Shows an icon, picked by its name. Icons have to be registered
 * with {@link RccIconService} beforehand in order for their names
 * be usable with this component.
 */
export class RccIconComponent {

	@HostBinding('class')
	protected get className() : string {
		if (this.size === 'tiny')	return 'icon-tiny'
		if (this.size === 'small')	return 'icon-small'
		if (this.size === 'medium')	return 'icon-medium'
		if (this.size === 'large')	return 'icon-large'
	}

	@Input()
	public set name(iconName : IconName) { this._name = iconName; this.updateIcon() }
	public get name() : IconName  { return this._name }
	private _name : IconName

	/**
	 * Determines two things:
	 * * size of the component/icon, this however can also be adjusted with css
	 * later on
	 * * the quality of the icon. Icons come in variants for each size; e.g.
	 * small icons will have less details.
	 */
	@Input()
	public set size(size : keyof IconVariants){ this._size = size; this.updateIcon() }
	public get size() : keyof IconVariants { return this._size || 'medium' }
	private _size : keyof IconVariants

	private _alt: string
	public get alt(): string {
		return this._alt
	}

	@HostBinding('attr.alt')
	@Input()
	public set alt(value: string) {
		this._alt = value
		if (this.svgIcon != null)
			this.svgIcon.setAttribute('alt', value)
	}

	protected svgIcon			: SVGElement
								= undefined

	protected aliasTransform	: string
								= undefined

	protected element			: HTMLElement
								= undefined

	protected iconVariants		: IconVariants

	public  constructor(
		protected elementRef	: ElementRef<HTMLElement>,
		protected iconService	: RccIconService,
	){
		this.element 	= this.elementRef.nativeElement
	}

	protected getSVGFallback(): string {

		return 		this.iconVariants?.medium
				||	this.iconVariants?.large
				||	this.iconVariants?.small
				||	this.iconService.getIconVariants('missing').medium



	}

	protected updateIcon() : void {

		this.clearSVG()

		// Prevent rendering the missing icon, when the component was just yet
		// created and has no set name yet.
		if (!this.name) return undefined

		this.iconVariants	= this.iconService.getIconVariants(this.name)
		this.aliasTransform	= this.iconService.getTransformation(this.name)

		const svgData	: string
						= this.iconVariants && this.iconVariants[this.size] || this.getSVGFallback()

		this.svgIcon	= this.svgElementFromString(svgData)

		if(this.aliasTransform) this.svgIcon.style.setProperty('transform', this.aliasTransform)

		if (this.alt)
			this.svgIcon.setAttribute('alt', this.alt)
			
		this.element.appendChild(this.svgIcon)
	}

	private svgElementFromString(svgContent: string): SVGElement {
		// This might be a security issue, maybe we should add a sanitation step here:
		// (SVG content though does not come from user input or third parties)
		this.element.innerHTML = svgContent

		const svgElement 	: SVGElement
							= this.element.querySelector('svg')

		return svgElement
	}

	protected clearSVG() : void {
		if (this.svgIcon && this.svgIcon.parentNode === this.element) this.element.removeChild(this.svgIcon)
		this.svgIcon = undefined
	}

}
