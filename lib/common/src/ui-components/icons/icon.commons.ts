import	{
			InjectionToken,
			Provider
		}								from '@angular/core'

import	{
			getProvider
		}								from '../../interfaces'
import	{	IconName 				}	from './icon-names'

export interface IconVariantsOptional {
	large?			: string
	medium?			: string
	small?			: string
	tiny?			: string
}

export interface IconVariantLarge {
	large			: string
}

export interface IconVariantMedium {
	medium			: string
}

export interface IconVariantSmall {
	small			: string
}

export interface IconVariantTiny {
	tiny			: string
}

export type IconVariants = IconVariantsOptional & (IconVariantLarge | IconVariantMedium |  IconVariantSmall | IconVariantTiny)

export interface IconConfig {
	name			: IconName,
	iconVariants	: IconVariants
}

export interface IconAlias {
	name			: IconName,
	from			: IconName,
	transform?		: string // value as in CSS property transform.
}


export const ICONS 	: InjectionToken<(IconConfig | IconAlias)[]>
					= new InjectionToken<(IconConfig | IconAlias)[]>('Icons')

export function provideIcon(icon: IconConfig | IconAlias ) : Provider {
	return getProvider(ICONS, icon, true)
}
