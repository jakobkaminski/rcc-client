import {	ComponentFixture, TestBed	}	from '@angular/core/testing'
import {	RccCardComponent 			}	from './card.component'
import {	RccColorService				}	from '@rcc/themes'
import {	ActionService				}	from '@rcc/common/src'

describe('RccCardComponent', () => {
	let component				: RccCardComponent						= undefined
	let fixture					: ComponentFixture<RccCardComponent>	= undefined


	beforeEach(() => {
		TestBed.configureTestingModule({
			providers: [
				RccColorService,
				{
					provide: ActionService,
					useValue: jasmine.createSpyObj('ActionService', ['execute']) as ActionService
				}
			]
		})

		fixture = TestBed.createComponent(RccCardComponent)

		component = fixture.componentInstance
	})

	it('should set the heading input correctly', () => {
		component.heading = 'Test Title'

		fixture.detectChanges()

		const element: HTMLElement = fixture.nativeElement as HTMLElement

		expect(element.textContent.trim()).toBe('Test Title')
	})

	it('should set the note input correctly', () => {
		component.note = 'Test Note'

		fixture.detectChanges()

		const element: HTMLElement = fixture.nativeElement as HTMLElement

		expect(element.textContent.trim()).toBe('Test Note')			// https://github.com/angular/angular/issues/37635 There seems to be a whitespace at the beginning of the string, not sure why, thus used #toContain(). Check link for more information about this issue. Solution is done with #trim().
	})

	it('should set the withPrimaryAction property correctly', () => {
		expect(component.withPrimaryAction).toBe(false)

		component.primaryAction = { label: 'Test', icon: 'edit', handler: () => true }

		expect(component.withPrimaryAction).toBe(true)
	})

	it('should set the withSecondaryAction property correctly', () => {
		expect(component.withSecondaryAction).toBe(false)

		component.secondaryAction = { label: 'Test', icon: 'edit', handler: () => true }

		expect(component.withSecondaryAction).toBe(true)
	})

})
