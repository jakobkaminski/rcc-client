import	{
			Component,
			Input,
			HostBinding,
			Output,
			EventEmitter,
		}									from '@angular/core'
import	{	CommonModule		}			from '@angular/common'
import	{	RccIconComponent	}			from '../icons'
import	{	provideTranslationMap,
			TranslationsModule
		}									from '../../translations'
import	{	RccColorCategoryDirective }		from '@rcc/common/ui-components/color-category'
import	{	Action,
			ActionService
		}									from '@rcc/common/actions'

import en from './i18n/en.json'
import de from './i18n/de.json'
/**
 * This RccCardComponent wraps arbitrary content, adds a title and an optional
 * note together with two icons.
 * If the editClick output is set, the content is regarded as editable and an
 * edit button is displayed. Clicking it
 * will cause the editClick output to emit.
 *
 * If the removeClick output is set, the content is regarded as removable from
 * something and the remove button is displayed. Clicking it will cause the
 * removeClick output to emit.
 *
 * The color of the note is determined by the `color` input which relies on
 * RccColorService and the theme that is used.
 * All theme colors are valid. For more information, see themes section of
 * the docs.
 *
 * Refactor some time in the future: PrimaryIcon was introduced to solve a
 * special case:
 * Namely rcc-list-item-tags with just one action. Those were supposed
 * to trigger that single action when clicked anywhere, not just on the card's
 * icons. But then the card would also trigger an action when clicked directly
 * on the icon, thus the action is triggered twice -.-. So at some point we have
 * to disable that, and that is what primaryIcon is for. It replaces the button
 * the primary action with just an icon.
 *
 */

@Component({
	selector:			'rcc-card',
	templateUrl:		'./card.component.html',
	styleUrls:			['card.component.scss'],
	standalone:			true,
	imports:			[
		RccIconComponent,
		CommonModule,
		TranslationsModule,
		RccColorCategoryDirective,
	],
	providers:			[
		provideTranslationMap('CARD', { en, de }),
	],
})

export class RccCardComponent {

	@Input()
	public showActionIcons		: boolean = true

	@Input()
	public heading?				: string

	@Output()
	public headingClick			: EventEmitter<void> = new EventEmitter<void>()

	@Input()
	public note?				: string

	@Input()
	public primaryAction		: Action

	@Input()
	public primaryIcon			: string

	@Input()
	public primaryActionInert	: boolean = false

	@Input()
	public secondaryAction		: Action

	@HostBinding('class.with-primary-action')
	public get withPrimaryAction()		: boolean { return !!this.primaryAction }

	@HostBinding('class.with-secondary-action')
	public get withSecondaryAction()	: boolean { return !!this.secondaryAction }

	public get showPrimaryAction()		: boolean { return !!this.primaryAction && this.showActionIcons }
	public get showSecondaryAction() 	: boolean {  return !!this.secondaryAction && this.showActionIcons }


	public constructor(
		protected readonly actionService: ActionService,
	){}

}
