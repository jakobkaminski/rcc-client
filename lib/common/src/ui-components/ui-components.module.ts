import	{
			NgModule,
			Type
		}											from '@angular/core'

import	{	HttpClientModule					}	from '@angular/common/http'

import	{	RccSliderModule						}	from './slider'
import	{	RccPaginationModule					}	from './pagination'
import	{	RccCalendarModule					}	from './calendar'
import	{	RccClickOutsideModule				}	from './click-outside'
import	{	RccToIDPipeModule					}	from './toID'
import	{	RccLogoComponent					}	from './logo'
import	{	IconsModule							}	from '../icons'
import	{	RccIconComponent					}	from './icons'
import	{	RccAutofocusModule					}	from './autofocus'
import	{	RccFocusModule						}	from './focus'
import	{	RccInputListModule					}	from './input-list'
import	{	SortByPositionPipeModule			}	from './sort-by-position'
import	{	AllIconsModule						}	from './icons'
import	{	RccCardComponent					}	from './card'
import	{	RccYyyyMmDdPipe						}	from './yyyy-mm-dd'
import	{	RccTabTrapDirective					}	from './tab-trap'
import	{ 	RccColorCategoryDirective 			}	from './color-category'
import	{	RccFabComponent						}	from './fab'

const modules : Type<unknown> [] = [
	RccAutofocusModule,
	RccFocusModule,
	RccCalendarModule,
	RccClickOutsideModule,
	RccPaginationModule,
	RccSliderModule,
	RccToIDPipeModule,
	SortByPositionPipeModule,
	RccInputListModule,
	HttpClientModule,
	IconsModule,
	RccColorCategoryDirective,
	AllIconsModule,
]

const standaloneComponents : Type<unknown>[] = [
	RccCardComponent,
	RccFabComponent,
	RccIconComponent,
	RccLogoComponent,
	RccTabTrapDirective,
	RccYyyyMmDdPipe,
]
@NgModule({
	imports: [...modules, ...standaloneComponents],
	exports: [...modules, ...standaloneComponents],
})

export class UiComponentsModule{}
