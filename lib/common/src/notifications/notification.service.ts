import	{
			Injectable,
			InjectionToken,
			Inject,
			Optional,
			Type
		}								from '@angular/core'

import	{
			Subject,
			Observable,
			combineLatest
		}								from 'rxjs'

import	{
			throwError
		}								from '@rcc/core'

import	{	RccTranslationService	}	from '../translations'
import	{	RccAlertController		}	from '../modals-provider'
import	{	map, shareReplay		}	from 'rxjs/operators'


export interface NotificationConfig {
	deps:		Type<any>[],
	factory: 	(...args:any[]) =>  Observable<number>
}


// This service only counts notifications, in order to display the number in a bubble above icons at certain places.
// This should probably be replaced at some point by some other logic

export const NOTIFICATION_COUNTERS = new InjectionToken<NotificationConfig>('Function to return a number of notications, 0 for none')

@Injectable()
export class NotificationService {

	public	notification$	: Observable<number>


	public constructor(
		@Optional() @Inject(NOTIFICATION_COUNTERS)
		protected notificationObs		: Observable<number>[],
		// protected rccTranslationService	: RccTranslationService,
		// protected rccAlertController	: RccAlertController
	) {

		this.notification$ 	= 	combineLatest(notificationObs||[])
								.pipe(
									map( results => results.reduce( (sum, value) => sum + value ,0) ),
									shareReplay(1)
								)

		// window.onclick = () => this.notify('', 'Test me', '').catch(console.log)

		// window.navigator.vibrate([200, 100, 200])
	}


	// async notify(title: string, message: string, tag: string): Promise<any> {


	// 	const translated_title		= this.rccTranslationService.translate(title)
	// 	const translated_message	= this.rccTranslationService.translate(message)



	// 	Notification 							|| throwError('NotificationService.notify: Missing Notification API.')
	// 	Notification.permission === 'granted'	|| await  Notification.requestPermission()


	// 	//TODO: translation

	// 	const notification	=	new Notification(translated_title, {
	// 								lang: 		this.rccTranslationService.activeLanguageChange$.getValue(),
	// 								body:		translated_message,
	// 								tag:		tag,
	// 								vibrate:	[200, 100, 200]
	// 							})


	// 	if(document.visibilityState !== 'visible') return null

	// 	this.rccAlertController.present({message:translated_message})
	// 	.then(
	// 		() => {}, //TODO
	// 		() => {}
	// 	)

	// }

}
