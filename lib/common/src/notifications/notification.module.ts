import	{
			NgModule,
			ModuleWithProviders,
		}								from '@angular/core'

import	{
			NOTIFICATION_COUNTERS,
			NotificationConfig,
			NotificationService
		}								from './notification.service'

import	{
			provideTranslationMap,
		}								from '../translations'

import en from './i18n/en.json'
import de from './i18n/de.json'




@NgModule({
	providers: [
		NotificationService,
		provideTranslationMap('NOTIFICATIONS', { en, de }),
	]
})
export class NotificationModule {

	public static forChild(notificationConfigs: NotificationConfig[] = []): ModuleWithProviders<NotificationModule> {

		return 	{
					ngModule:	NotificationModule,
					providers:	[
									...notificationConfigs.map( notificationConfig => ({
										provide: 	NOTIFICATION_COUNTERS,
										multi:		true,
										deps:		notificationConfig.deps,
										useFactory:	notificationConfig.factory
									}))
								]
				}
	}
}
