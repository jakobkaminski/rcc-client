import 	{
			Component,
			Inject,
			Injectable,
			Type
		}										from '@angular/core'

import	{
			RccTranslationService
		}										from '../translations'
import	{
			RccModalController,
			RccLoadingController,
			RccAlertController,
			ModalPresentationOptions,
		}										from '../modals-provider'

import	{
			RELAY_PROPERTIES,
			RccRelayModalService
		}										from './relay'

import	{
			RccWaitOrCancelModalComponent
		}										from '@rcc/themes/active'
import	{	IconName						 }	from '../ui-components/icons/icon-names'


@Component({
	template	: `<rcc-wait-or-cancel-modal
						[contextText]	= "properties.contextText"
						[message]		= "properties.message"
						[promise]		= "properties.promise"
						[icon]			= "properties.icon"
						[direction]		= "properties.direction"
					></rcc-wait-or-cancel-modal>`,
	standalone	: true,
	imports		: [RccWaitOrCancelModalComponent],
	styles		: [':host { height: 100% }']
})
class WaitOrCancelWrapperComponent {
	public constructor(
		@Inject(RELAY_PROPERTIES)
		protected properties			: WaitOrCancelOptions & { promise: Promise<unknown> },
	) {}
}

interface WaitOrCancelOptions {
	direction	: 'share' | 'receive'
	message		: string
	contextText	: string
	icon		: IconName
}

/**
 * This service is meant to manage all overlays, that are ready to use.
 * For custom Modals use the respective Controller
 * ( {@link RccModalController}, {@link RccAlertController}, {@link RccToastControll} )
 *
 * TODO: This Service needs to be expanded so that features modules can reliably
 * use it without falling back to  {@link RccModalController} and similar.
  *
 *
 */
@Injectable()
export class RccOverlayService {


	public constructor(
		protected rccModalController	: RccModalController,
		protected rccRelayModalService	: RccRelayModalService,
		protected rccAlertController	: RccAlertController,
		protected rccLoadingController	: RccLoadingController,
		protected rccTranslationService	: RccTranslationService
	){}

	public async alert(message: string) : Promise<void> {
		return void await this.rccAlertController.present({ message })
	}

	/**
	 * Open a modal with a text and a cancel button and some waiting indicator.
	 * The Modal will stay open until,the provided promises settles or the user
	 * clicks the cancel button.
	 *
	 * @param options 			Texts to be displayed
	 * @param awaitedPromise 	Promise to be awaited
	 *
	 * @returns 	A Promise that resolves when the provided promise resolves
	 * 				and rejects when the provided promise rejects. In both cases
	 * 				using the values from the original promise.
	 * 				When the user presses the cancel button or closes the modal
	 * 				some other way, rejects with a {@link UserCanceledError}
	 */
	public async waitOrCancel<T>(awaitedPromise: Promise<T>, options: WaitOrCancelOptions) : Promise<unknown> {
		return await this.relay(WaitOrCancelWrapperComponent, { ...options, promise:awaitedPromise })
	}

	/**
	 * Opens a component in a wrapping modal. The component must be
	 * self-contained: inputs and outputs cannot be used. This method can be
	 * called repeatedly without closing the wrapping modal (or reopening it).
	 * If the modal is already open only the component will be swapped and the
	 * the promise returned by the last call will be rejected.
	 *
	 * This is useful if multiple modals/overlays are opened in succession with
	 * the intent to transition from on to the next without being disrupted by
	 * animation or flickering, that can happen when closing and reopening a
	 * normal modal.
	 *
	 * The component used in conjunction wit the relay modal, have to inject the
	 * properties with the RELAY_PROPERTIES injection token.
	 *
	 * Examples are steps in a setup wizard.
	 *
	 * @param component 	The Component displayed in the ReplayModalComponent.
	 * 						You can treat that component just like any other
	 * 						modal component. Call `.dismiss` on RccModalController
	 * 						to return a result.
	 */
	public async relay<R = unknown>(component : Type<unknown>, properties?: unknown, options?: Partial<ModalPresentationOptions>): Promise<R> {
		return await this.rccRelayModalService.handOver(component, properties, options) as R
	}


}
