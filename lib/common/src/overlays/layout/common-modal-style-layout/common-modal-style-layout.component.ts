import { Component, Input } from '@angular/core'
import { RccModalController } from '@rcc/common/modals-provider'

@Component({
	templateUrl		: './common-modal-style-layout.component.html',
	styleUrls		: ['./common-modal-style-layout.component.scss'],
	selector		: 'rcc-full-page-modal-style-layout'
})
export class CommonModalStyleLayoutComponent {
	public constructor(private readonly modalController: RccModalController) {}

	@Input()
	public headingMessage: string

	protected close(): void {
		this.modalController.dismiss()
	}
}
