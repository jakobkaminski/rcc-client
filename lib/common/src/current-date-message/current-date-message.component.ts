import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { TranslationsModule } from '../translations'

@Component({
	templateUrl:	'./current-date-message.component.html',
	selector:		'rcc-current-date-message',
	styleUrls:		['./current-date-message.component.scss'],
	standalone:		true,
	imports:		[CommonModule, TranslationsModule]
})
export class CurrentDateMessageComponent {
	@Input()
	public bold		: boolean = true

	protected today	: Date = new Date()

	private _dateFormatOptions: Intl.DateTimeFormatOptions = {
		month: '2-digit',
		year: 'numeric',
		day: '2-digit',
		weekday: 'long',
	}
	protected dateFormatOptions: Record<string, unknown> = this._dateFormatOptions as Record<string, unknown>
}
