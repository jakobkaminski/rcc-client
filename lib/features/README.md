This folder includes all feature modules.

Try to separate non-angular code into /core and /tools. At the same time try to separate angular code into /common. 
Everything else belongs here. 

The purpose of each of the feature modules is to take the capabilites of recoverycat related non-angular code from /core and plug it into angular app shell from /common.

