import	{
			Injectable,
			NgModule
		}								from '@angular/core'
import	{
			ExportConfig,
			ExportService,
			ExportMetadata,
			ExportResult,
			provideExportService,
			provideTranslationMap
		}								from '@rcc/common'
import	{
			assert,
			QuestionConfig,
			Report
		}								from '@rcc/core'
import	{	OpenSessionStoreService	}	from '../sessions'

import en from './i18n/en.json'
import de from './i18n/de.json'

/**
 * Simple JSON export for report and questions of the active session
 */
@Injectable()
export class ReportJsonExportService extends ExportService<Report,string> {
	public label	 	: string	= 'JSON_MODULE.EXPORT.LABEL'
	public description	: string	= 'JSON_MODULE.EXPORT.DESCRIPTION'

	public constructor(
		public openSessionStoreService: OpenSessionStoreService
	) {
		super()
	}

	public canExport(x: unknown): x is Report {
		return x instanceof Report
	}

	public export(x: ExportConfig<Report>): ExportResult<string> {
		assert(this.canExport(x.data), 'ReportJsonExportService.export: x.data must be of type Report')
		// Get question configs from active session = questions answered in report
		const questions : QuestionConfig[] = this.openSessionStoreService.activeSession.questions.map(q => q.config)

		const json : string = JSON.stringify(
			{
				report		: x.data.config,
				questions	: questions
			},
			null,
			2		// Prettify output
		)

		return {
			toString	: () => json,
			raw			: () => json
		}
	}

	public getMetadata(x: ExportConfig<Report>): ExportMetadata {
		assert(this.canExport(x.data), 'ReportZipExportService.export: x.data must be of type Report')
		return {
			suggestedFilename	: `RCC-Report-${x.data.date.toISOString().split('T')[0]}__ID.json`,
			mimeType			: 'application/json'
		}
	}
}

@NgModule({
	providers: [
		provideTranslationMap('JSON_MODULE', { en,de }),
		...provideExportService(ReportJsonExportService)
	]
})
export class JsonModule {}
