import	{
			NgModule
		}										from '@angular/core'

import	{
			requestPublicRuntimeConfigValue,
			RccPublicRuntimeConfigModule,
			TranslationsModule
		}										from '@rcc/common'

import	{
			RccRuntimeStartupBannerService
		}										from './runtime-startup-banner.service'

import	{
			runtimeConfigPath
		}										from './runtime-startup-banner.commons'

/**
 * Adds a banner based on runtime configuration
 * as soon as the app is loaded.
 * (see {@link  RccPublicRuntimeConfigService})
 */
@NgModule({
	imports:[
		TranslationsModule,
		RccPublicRuntimeConfigModule
	],
	providers:[
		RccRuntimeStartupBannerService,
		requestPublicRuntimeConfigValue({
			path:			runtimeConfigPath,
			description: 	'Home page banner config.',
			type:			'BannerConfig',
		})
	]
})
export class RccRuntimeStartupBannerModule {
	public constructor(
		private rccRuntimeStartupBannerService: RccRuntimeStartupBannerService
	){}
}
