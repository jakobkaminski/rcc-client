import 	{
			NgModule,
		} 									from '@angular/core'
import	{
			Routes,
			provideRoutes
		}									from '@angular/router'
import	{
			TranslationsModule,
			SharedModule,
			WidgetsModule,
			provideTranslationMap,
			provideItemAction,
			ItemAction,
		}									from '@rcc/common'
import	{	Question					}	from '@rcc/core'
import	{
			QuestionnaireServiceModule
		}									from '../questions/questionnaire'
import	{	JournalServiceModule		}	from '../entries'

import	{	EmptyLineAfterSentencePipe	}	from './query-run'
import	{	QueryViewComponent			}	from './query-view'
import	{	QueryPageComponent			}	from './query-page/query-page.component'
import	{	QueryPanelComponent			}	from './query-panel/query-panel.component'
import	{	QueryRunComponent			}	from './query-run/query-run.component'
import	{	FallbackQueryWidgetsModule	}	from './fallback-query-widgets'

import en from './i18n/en.json'
import de from './i18n/de.json'



const routes	: Routes =	[
								{
									path: 		'query/:id',
									component: 	QueryPageComponent
								},
							]

const itemAction: ItemAction<Question> =	{
							itemClass:	Question,
							role:		'productive' as const,
							getAction:	(question: Question) => 	({
												label: 		'QUERIES.ACTIONS.ANSWER.LABEL',
												icon: 		'query',
												path:		'query/'+question.id
										})
						}




@NgModule({

	imports: [
		JournalServiceModule,
		QuestionnaireServiceModule,
		SharedModule,
		WidgetsModule,
		TranslationsModule,
		FallbackQueryWidgetsModule,
		EmptyLineAfterSentencePipe,
	],

	declarations: [
		QueryViewComponent,
		QueryRunComponent,
		QueryPageComponent,
		QueryPanelComponent
	],

	providers: [
		provideItemAction(itemAction),
		provideTranslationMap('QUERIES', { en, de }),
		provideRoutes(routes),
	],

	exports:[
		QueryViewComponent,
		QueryRunComponent,
		QueryPageComponent,
		QueryPanelComponent
	]

})
export class QueriesModule{}
