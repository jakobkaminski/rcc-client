import 	{
			OnDestroy,
			Component,
		}									from '@angular/core'

import	{
			Subject,
			takeUntil,
		}									from 'rxjs'

import	{
			QueryWidgetComponent
		}									from '../query-widget-component.class'

import	{
			QueryControl,
		}									from '../query-control.class'
import	{	ValidationErrors			}	from '@angular/forms'
import	{	FallbackValueType			}	from './fallback-query-value-control-accessor/fallback-query-value-control-accessor'

@Component({
	selector:    	'fallback-query-widget',
	templateUrl:  	'./fallback-query-widget.component.html',
})
export class FallbackQueryWidgetComponent extends QueryWidgetComponent implements OnDestroy {

	public static widgetMatch() : number { return 0 }

	public		error		: string

	protected	destroy$	: Subject<void>	= new Subject()

	protected get parseValue(): (value: string) => FallbackValueType {
		const type: string = this.queryControl.question.type

		if (type === 'string') return value => String(value)
		if (type === 'integer') return value => this.toInteger(value)
		if (type === 'decimal') return value => this.toDecimal(value)
	}

	public constructor(
		public 		queryControl	: QueryControl,
	){
		super(queryControl)

		queryControl.answerControl.statusChanges
			.pipe( takeUntil(this.destroy$) )
			.subscribe(() => {
				const errors			: ValidationErrors	=  	this.queryControl.answerControl.errors
				const constraintError	: string			=	errors?.questionTypeConstraints as string

				this.error = 	constraintError || null
			})
	}

	private toInteger(x: unknown): number {

		const stringValue	: string 	= 	String(x)
		const parsed		: number	=	parseInt(stringValue, 10)

		return	isNaN(parsed)
				?	0
				:	parsed

	}

	private toDecimal(x: unknown): number {

		const stringValue	: string 	= 	String(x).replace(/,/g,'.')
		const parsed		: number	=	parseFloat(stringValue)

		return	isNaN(parsed)
				?	0
				:	parsed

	}

	protected get inputType(): string {
		return this.queryControl.question.type === 'string' ? 'text' : 'number'
	}

	protected get placeholder(): string {
		const translation: string = ['number', 'decimal'].includes(this.queryControl.question.type) ? 'NUMBER' : 'GENERIC'
		return `QUERIES.FALLBACK.PLACEHOLDER.${translation}`
	}

	public ngOnDestroy() : void{
		this.destroy$.next()
		this.destroy$.complete()
	}

	protected get step(): string {
		return this.queryControl.question.type === 'integer' ? '1' : 'any'
	}
}

