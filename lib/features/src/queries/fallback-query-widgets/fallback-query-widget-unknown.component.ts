import 	{
			Component,
		}									from '@angular/core'

import	{	WidgetComponent		}			from '@rcc/common'
import	{	QueryControl		}			from '../query-control.class'


@Component({
	selector:     'fallback-query-widget-unknown',
	templateUrl:   './fallback-query-widget-unknown.component.html',
})
export class FallbackQueryWidgetUnknownComponent extends WidgetComponent<QueryControl> {

	static controlType = QueryControl

	static widgetMatch( queryControl: QueryControl ){ return queryControl.question.type === 'unknown' ? 2 : -1 }

	public constructor(
		public queryControl: QueryControl
	){
		super(queryControl)
	}

}

