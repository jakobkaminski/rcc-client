import { Component, ElementRef, Input, ViewChild, forwardRef } from '@angular/core'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms'

export type FallbackValueType = string | number

@Component({
	selector	: 'fallback-query-value-control-accessor',
	templateUrl	: './fallback-query-value-control-accessor.html',
	styleUrls	: ['./fallback-query-value-control-accessor.scss'],
	providers	: [
		{
			provide		: NG_VALUE_ACCESSOR,
			useExisting	: forwardRef(() => FallbackQueryValueControlAccessorComponent),
			multi		: true,
		}
	]
})
export class FallbackQueryValueControlAccessorComponent implements ControlValueAccessor {
	private _input: ElementRef<HTMLInputElement>
	@ViewChild('input')
	protected set input(value: ElementRef<HTMLInputElement>) {
		this._input = value
		this.updateDisplayValue()
	}

	@Input()
	public type: string

	@Input()
	public parseValue: (inputValue: string) => FallbackValueType = (value) => value

	@Input()
	public placeholder: string

	@Input()
	public min: number

	@Input()
	public max: number

	@Input()
	public step: string = 'any'

	@Input()
	public ariaLabel: string

	protected value: FallbackValueType
	public writeValue(value: FallbackValueType): void {
		this.value = value
		this.updateDisplayValue()
	}

	private onChange: (value: FallbackValueType) => void = () => undefined
	public registerOnChange(fn: (value: FallbackValueType) => void): void {
		this.onChange = fn
	}

	private updateDisplayValue(): void {
		if (this._input == null || this.value == null)
			return

		this._input.nativeElement.value = this.value.toString()
	}

	protected handleInput(event: Event): void {
		const target: HTMLInputElement = event.target as HTMLInputElement
		const value: string = target.value
		const parsedValue: FallbackValueType = this.parseValue(value)

		this.onChange(parsedValue)
		this.value = parsedValue
	}

	protected handleChange(event: Event): void {
		this.handleInput(event)
		this.updateDisplayValue()
	}

	protected onTouch: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouch = fn
	}

	protected disabled: boolean = false
	public setDisabledState?(isDisabled: boolean): void {
		this.disabled = isDisabled
	}
}
