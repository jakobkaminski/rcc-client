import	{
			Component,
			Input,
		}							from '@angular/core'

import	{
			RccSliderComponent,
			RccToastController,
			RccAlertController,
			HandlerAction
		}							from '@rcc/common'

import	{
			QueryControl
		}							from '../query-control.class'


@Component({
	selector: 'rcc-query-panel',
	templateUrl: 'query-panel.component.html',
	styleUrls:	['./query-panel.component.scss']
})
export class QueryPanelComponent {

	@Input()
	public queryControl		: QueryControl

	@Input()
	public slider			: RccSliderComponent

	@Input()
	public autosave			: boolean = false

	public constructor(
		protected rccToastController	: RccToastController,
		protected rccAlertController	: RccAlertController
	){}
	public get entryExists(): boolean {
		return !!this.queryControl?.entry
	}

	public get validUserInputExists(): boolean {
		return this.queryControl?.complete
	}

	public get changesExist(): boolean {

		if(!this.validUserInputExists) 	return false
		if(!this.entryExists) 			return true

		return this.queryControl.answer !== this.queryControl.entry.answer
	}

	private async storeEntry(): Promise<void> {

		await this.queryControl.submit()

		if(!this.autosave)
			this.slider.next()
	}

	public async removeEntry(): Promise<void> {

		await	this.rccAlertController.confirm('QUERIES.DELETE.CONFIRM', 'DELETE')

		await	this.queryControl.reset()

	}

	public revertEntry(): void {
		this.queryControl.revert()
	}

	protected get showDeleteButton(): boolean {
		if (this.autosave)
			return this.canDelete
		else
			return true
	}

	protected get showResetButton(): boolean {
		if (this.autosave)
			return false
		return this.canSubmitOrRevert
	}

	private get canSubmitOrRevert(): boolean {
		if (this.autosave)
			return this.canDelete
		
		return !this.queryControl?.submissionsDisabled && this.changesExist
	}

	protected get canDelete(): boolean {
		return !this.queryControl?.submissionsDisabled && this.entryExists
	}

	protected reset: HandlerAction = {
		handler 	: () => {
			this.revertEntry()
		},
		icon		: 'refresh',
		label		: 'QUERIES.QUERY_RUN.RESET',
		disabled	: () => !this.canSubmitOrRevert
	}

	protected submit: HandlerAction = {
		handler		: () => this.storeEntry(),
		icon		: 'check_mark',
		label	 	: 'QUERIES.QUERY_RUN.SAVE',
		disabled	: () => !this.canSubmitOrRevert
	}

	protected get deleteAnswer(): HandlerAction { return {
		handler		: () => this.removeEntry(),
		icon		: this.autosave ? 'refresh' : 'delete',
		label		: 'QUERIES.QUERY_RUN.DELETE',
		disabled	: () => !this.canDelete
	}}

	protected next: HandlerAction = {
		handler		: () => this.slider.next(),
		icon		: 'next',
		label		: 'QUERIES.QUERY_RUN.NEXT'
	}

	protected previous: HandlerAction = {
		handler		: () => this.slider.previous(),
		icon		: 'previous',
		label		: 'QUERIES.QUERY_RUN.PREVIOUS'
	}
}
