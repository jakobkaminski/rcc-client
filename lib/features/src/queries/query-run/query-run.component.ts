import 	{
			Component,
			DoCheck,
			EventEmitter,
			Input,
			IterableChanges,
			IterableDiffer,
			IterableDiffers,
			OnDestroy,
			OnInit,
			Output,
			ViewChild,
		}     								from '@angular/core'
import	{
			BehaviorSubject,
			Observable,
			Subject,
			SubscriptionLike,
			debounceTime,
			takeUntil,
			firstValueFrom,
			throttleTime,
			filter,
			distinctUntilChanged
		}									from 'rxjs'
import	{
	RccToastController,
	RccAlertController,
	RccSliderComponent,
	HandlerAction
		}									from '@rcc/common'
import	{
			isInfoSlideConfig,
			InfoSlideConfig
		}									from '@rcc/core'
import	{	QueryControl				}	from '../query-control.class'

/**
 * This component deals with answering process of multiple questions at once.
 * The answering process for each question is represented by a {@link QueryControl}.
 *
 * You can provide extra slide (e.g. All-Done-Slide) by content projection. Make sure to use ```rcc-slide```.
 */
@Component({
	selector:		'rcc-query-run',
	templateUrl:	'./query-run.component.html',
	styleUrls:		['./query-run.component.css'],
})
export class QueryRunComponent implements OnInit, DoCheck, OnDestroy {

	/**
	 * One Query control for each question that is to be answered.
	 */
	@Input()
	public queryControls : QueryControl[]

	/**
	 * Slides may contain question IDs and {@link InfoSlideConfig}s.
	 */
	@Input()
	public slides : (string | InfoSlideConfig)[] = undefined

	public get slidesOrQueries() : (InfoSlideConfig | QueryControl)[] {
		if (!this.slides) return this.queryControls

		return this.slides.map(slide => {
			if (typeof slide === 'string') {
				const queryControl : QueryControl = this.queryControls.find(qc => qc.question.id === slide)
				return queryControl
			}
			if (isInfoSlideConfig(slide)) return slide
		})
	}

	/**
	 * Gets the position of a QueryControl in the query run,
	 * ignoring slides that are not QueryControls.
	 */
	private getCurrentPosition(qc: QueryControl) : number {
		// Filter slidesOrQueries instead of using this.queryControls
		// because the order of the query run must be kept
		const queryControls : (QueryControl | InfoSlideConfig)[] = this.slidesOrQueries.filter(item => this.isQueryControl(item))
		return queryControls.indexOf(qc) + 1
	}

	protected getHeadingText(queryControl: QueryControl): string {
		return `${this.getCurrentPosition(queryControl)}/${this.queryControls.length}`
	}

	public isInfoSlideConfig(x: unknown)	: x is InfoSlideConfig	{ return isInfoSlideConfig(x) }

	public isQueryControl(x: unknown)		: x is QueryControl	{ return x instanceof QueryControl }

	/**
	 * Whether changes on the [queryControls] should be automatically saved
	 */
	@Input()
	public autosave: boolean

	/**
	 * Emits when users clicks 'done' button on the last slide.
	 */
	@Output()
	public done					: EventEmitter<void>		 	= new EventEmitter<void>()

	/**
	 * Emits when the user clicks 'cancel' button.
	 */
	@Output()
	public cancel				: EventEmitter<void>			= new EventEmitter<void>()

	@Output()
	public queryControlChange	: EventEmitter<QueryControl>	= new EventEmitter<QueryControl>()

	private change$				: Subject<void>					= new Subject<void>()

	public ngDoCheck(): void {

		const diff: IterableChanges<QueryControl> = this.changes.diff(this.queryControls)

		if(diff) {

			this.change$.next()

			if(this.autosave) // {
				this.queryControls?.forEach((queryControl: QueryControl) => {

					// Track values changes, throttles and spreads them out
					// more evenly.
					queryControl.answerControl.valueChanges
					.pipe(
						takeUntil(this.change$),
						throttleTime(450, undefined, { leading:true, trailing: true }),
						distinctUntilChanged(),
					)
					.subscribe( () => {
						void this.storeEntry(queryControl)
					})

				})


				// queryControl.answerControl.valueChanges
				// queryControl.change$
					// .pipe(
					// 	debounceTime(200),
					// 	// throttleTime(350, undefined, { leading: true, trailing: true }),
					// 	takeUntil(this.change$),
					// )
					// .subscribe(() => {
					// 	console.log('JJ: AnswerControl valueChanges', queryControl.complete)
					// 	if(queryControl.complete)
					// 		void this.storeEntry(queryControl)

					// })

				// queryControl.change$
				// 	.pipe(
				// 		throttleTime(5000, undefined, { leading: true, trailing: true }),
				// 		// debounceTime(5000),
				// 	)
				// 	.subscribe(() => {
				// 		console.log('JJ: CHANGES')
				// 		this.showSuccessToast()
				// 	})
			// }

			// Submission emits are triggered by storing,
			// which is triggered after the throttled value changes,
			// ( in autosave mode!) see above.
			// With the following, debouncing rapid successive changes
			// will result in a toast after a break of 500 to 950 ms,
			// while single changes spawn a toast after 500 ms.

			// Continuous typing without toast interruption in autosave mode
			// feels okay, but could use some tweaking.

			this.queryControls?.forEach((queryControl: QueryControl) => {

				queryControl.submission$
				.pipe(
					takeUntil(this.change$),
					debounceTime(500),
				)
				.subscribe( (submissionPromise) => {

					void	submissionPromise
							.then(
								() => this.showSubmissionSuccessToast(),
								() => this.showSubmissionFailureToast()
							)
				})

				queryControl.reset$
				.pipe(
					takeUntil(this.change$),
				)
				.subscribe( (resetPromise) => {
					void 	resetPromise
							.then(
								() => this.showResetSuccessToast(),
								() => this.showResetFailureToast()
							)
				})
			})

		}

	}

	/**
	 * The Slider component. We needs this to programmatically trigger
	 * forward or backward sliding and to get the question, currently
	 * seen by the user.
	 */
	@ViewChild(RccSliderComponent)
	protected set sliderComponent(slider: RccSliderComponent){
		this.setupSlider(slider)
	}

	public slider						:	RccSliderComponent

	protected slideChangeSub			:	SubscriptionLike
	protected activeQueryControlSubject	:	BehaviorSubject<QueryControl>	= 	new BehaviorSubject<QueryControl>(null)

	public activeQueryControl$			:	Observable<QueryControl>		= 	this.activeQueryControlSubject.asObservable()

	private changes: IterableDiffer<QueryControl>

	public constructor(
		public rccToastController	: RccToastController,
		public rccAlertController	: RccAlertController,
		private iterableDiffers		: IterableDiffers
	){
		this.activeQueryControl$.subscribe(this.queryControlChange)
	}

	public ngOnInit(): void {
		this.changes = this.iterableDiffers.find(this.queryControls).create()
	}

	/**
	 * {@link QueryControl}, representing the answering process of
	 * the currently displayed Question.
	 */
	public get activeQueryControl(): QueryControl {
		return this.activeQueryControlSubject.value
	}


	/**
	 * Checks if all questions have been answered.
	 */
	protected get allDone() : boolean {

		if(!Array.isArray(this.queryControls)) return false

		return this.queryControls.every( queryControl => !!queryControl.entry)
	}


	/**
	 * Initiliazes the component to work with a slider component in the template, once it has been rendered.
	 */
	private setupSlider(slider: RccSliderComponent): void {

		// nothing has changed, no need for setup:
		if(slider === this.slider) return

		this.slider = slider || undefined

		if(this.slideChangeSub) this.slideChangeSub.unsubscribe()

		if(!this.slider) return

		this.slideChangeSub 		= this.slider.slideChange$.subscribe( () => this.onSlideChange() )

		this.slider.storeState 		= () => this.storeState()
		this.slider.restoreState 	= (questionId:string) => this.restoreState(questionId)

	}

	/**
	 * When the slider gets new slides, it has to figure out, which of them is to
	 * be initially displayed when the slider rerendered. This method tells the slider,
	 * to remember the id of the currently displayed question, before rerendering starts.
	 *
	 */
	public storeState(): string{
		return this.activeQueryControl?.question.id
	}

	/**
	 * When the slider gets new slides, it has to figure out, which of them is to
	 * be initially displayed after the slider will have rerendered. This method
	 * scrolls to the slide of the question that matches the id stored before the
	 * slides rerendered (see .storeState()).
	 */
	public restoreState(questionId?: string): void {
		if (questionId != null)
			this.gotoQuestion(questionId, 'auto')
		else
			this.gotoStart()
	}



	/**
	 * Tracks slide changes and controls emissions of activeQueryControlSubject.
	 * Everytime the slide changes, activeQueryControlSubject will emit a new queryControl.
	 *
	 * Note: We don't create activeQueryControlSubject from this.slider.slideChange$
	 * with pipe and operators, because .activeQueryControlSubject exists earlier then the
	 * slider component and it's observables; also the slider component may change or
	 * get destroyed.
	 */
	public onSlideChange(): void {

		if(!this.slider) 		return
		if(!this.queryControls)	return

		const queryControl : QueryControl = this.getQueryControlFromSlidePosition()

		this.activeQueryControlSubject.next(queryControl)

	}

	/**
	 * Returns the {@link QueryControl} for the current slide,
	 * taking into account info slides that don't have query controls.
	 */
	public getQueryControlFromSlidePosition(): QueryControl {
		if (!this.slides) return null
		if (!this.slider) return null

		const slidePosition	: number 				= this.slider.currentSlidePosition
		const currentSlide	: InfoSlideConfig|string	= this.slides[slidePosition]
		const isInfoSlide	: boolean				= typeof currentSlide !== 'string'
		if (isInfoSlide) return null

		return this.queryControls.find(qc => qc.question.id === currentSlide) || null
	}

	/**
	 * Scrolls to the earliest question that lacks an answer.
	 */
	public gotoFirstUnansweredQuery() : void {
		const position: number = this.queryControls.findIndex( queryControl => !queryControl.entry )

		this.slider.slideTo(position)
	}

	/**
	 * Scrolls to the very first question.
	 */
	public gotoFirstQuery() : void {
		this.slider.slideTo(0)
	}

	/**
	 * Scrolls to the question matching the given id.
	 */
	public gotoQuestion(id: string, scrollBehavior: ScrollBehavior = 'smooth') : void {
		const position: number = this.queryControls.findIndex( queryControl => queryControl.question.id === id)

		this.slider.slideTo(position, scrollBehavior) // if position === -1, will go to the last slide

	}

	/**
	 * Scroll to the wrap up page.
	 */
	public gotoWrapUp(): void {
		this.slider.slideTo(-1)
	}

	public gotoStart(): void {
		this.slider.slideTo(0)
	}

	public ngOnDestroy(): void {
		this.change$.complete()
		this.slideChangeSub.unsubscribe()
		this.activeQueryControlSubject.complete()
	}

	protected headingText(current: number, total: number): string {
		return `${current + 1}/${total}`
	}

	private async storeEntry(queryControl: QueryControl): Promise<void> {

		// The validators in queryControl.answerControl are asynchronous,
		// so we have to wait for them to be done:
		if(queryControl.answerControl.status === 'PENDING'){

			const nonPendingStatus	:	Observable<string>
									= 	queryControl.answerControl.statusChanges
										.pipe( filter( status => status !== 'PENDING') )

			await firstValueFrom(nonPendingStatus)
		}

		// .complete check the status of queryControl.answerControl,
		// that's why we have to wait.
		if(queryControl.complete){
			await queryControl.submit()

			if(!this.autosave)
				this.slider.next()
		} else if (!this.autosave) {
				this.showSubmissionFailureToast()
				throw new Error('QueryRunComponent.storeEntry: Answer is invalid')
		}

	}

	private showSubmissionSuccessToast(): void {
		void this.rccToastController.success('QUERIES.SAVE.SUCCESS')
	}

	private showSubmissionFailureToast(): void {
		void this.rccToastController.failure('QUERIES.SAVE.FAILURE')
	}

	private showResetSuccessToast(): void {
		void this.rccToastController.success('QUERIES.REVERT.SUCCESS')
	}

	private showResetFailureToast(): void {
		void this.rccToastController.failure('QUERIES.REVERT.FAILURE')
	}

	protected next: HandlerAction = {
		handler		: () => this.slider.next(),
		icon		: 'next',
		label		: 'QUERIES.QUERY_RUN.NEXT'
	}

	protected previous: HandlerAction = {
		handler		: () => this.slider.previous(),
		icon		: 'previous',
		label		: 'QUERIES.QUERY_RUN.PREVIOUS'
	}

	protected firstCardOnScreen(slideOrQuery: QueryControl): boolean {
		return this.slider?.currentSlidePosition === this.getCurrentPosition(slideOrQuery) - 1
	}
}
