import { Component } from '@angular/core'
import { QueryControl, QueryWidgetComponent } from '../../queries'
import { FormControl } from '@angular/forms'

@Component({
	templateUrl	: './large-scale.component.html',
	styleUrls	: ['./large-scale.component.scss'],
})
export class LargeScaleQueryWidgetComponent extends QueryWidgetComponent {
	public static widgetMatch(queryControl: QueryControl): number {
		const isInteger		: boolean = queryControl.question.type === 'integer'
		const isScale		: boolean = queryControl.question.tags?.indexOf('rcc-scale') !== -1
		const hasLimits		: boolean = queryControl.question.min != null && queryControl.question.max != null
		const difference	: number = queryControl.question.max - queryControl.question.min
		const optionLength	: number = queryControl.question.options?.length ?? 0

		if (isInteger && isScale && hasLimits && difference > 11 && optionLength === 0)
			return 3
		
		return 0
	}

	public constructor(public queryControl: QueryControl) {
		super(queryControl)
	}

	protected get value(): number {
		const answerControl: FormControl<number> = this.queryControl.answerControl as FormControl<number>
		return answerControl.value
	}

	protected handleClick(): void {
		if (this.value == null)
			// This comes from a problem using the default html
			// slider (input type='range') element together with
			// Angular's Forms library. When The value is null,
			// the slider thumb gets rendered in the middle
			// of the track (which is what we want), however
			// click events directly on the thumb don't trigger
			// the input or change events. To compensate for
			// this, so that we have a value to render, we're
			// manually calling the form setValue method when the
			// thumb is clicked, but there's no value yet
			this.queryControl.answerControl.setValue(50)
	}
}
