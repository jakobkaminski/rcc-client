import	{	NgModule, Type 					}	from '@angular/core'
import	{
			SharedModule,
			WidgetsModule,
			provideWidget
		}									from '@rcc/common'
import	{	QuestionnaireServiceModule	}	from '../questions'
import	{
			LargeScaleQueryWidgetComponent
		}									from './large-scale/large-scale.component'
import	{	ScaleQueryWidgetComponent	}	from './scale/scale.component'
import	{	SelectQueryWidgetComponent	}	from './select/select.component'
import	{	TextQueryWidgetComponent	}	from './text/text.component'

const widgets: Type<unknown>[]
				=	[
						ScaleQueryWidgetComponent,
						LargeScaleQueryWidgetComponent,
						SelectQueryWidgetComponent,
						TextQueryWidgetComponent,
					]

@NgModule({
	imports: [
		SharedModule,
		QuestionnaireServiceModule,
		WidgetsModule,
	],
	providers: [
		...widgets.map(provideWidget)
	],
	declarations: [
		...widgets
	],
	exports: [
		...widgets
	]
})
export class BasicQueryWidgetsModule { }
