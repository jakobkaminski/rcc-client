import 	{
			Component,
			OnInit,
			OnDestroy
		}													from '@angular/core'

import	{
			FormControl,
			ValidatorFn,
			AsyncValidatorFn
														}	from '@angular/forms'

import	{
			Subject,
			takeUntil,
			debounceTime,
			distinctUntilChanged
		}													from 'rxjs'

import 	{
			QueryControl,
			QueryWidgetComponent
		}													from '../../queries'

@Component({
	templateUrl: 	'./text.component.html',
	styleUrls:		['./text.component.scss']
})
export class TextQueryWidgetComponent extends QueryWidgetComponent implements OnInit, OnDestroy{

	// STATIC:

	public static widgetMatch(queryControl: QueryControl): number {
		if(queryControl.question.options) return -1

		if(queryControl.question.type !== 'string') return -1

		// only if the tags include 'rcc-long-text',
		// the question is regarded as expecting a long string answer.
		if(queryControl.question.tags.includes('rcc-long-text')) return 1

		return -1
	}


	// INSTANCE:


	public destroy$			: Subject<void>
							= new Subject<void>()

	public textFormControl	: FormControl<string>



	public ngOnInit() : void {

		// Synchronize queryControl.answerControl and local (debounced) FormControl

		const validators 	: 	(ValidatorFn | AsyncValidatorFn)[]
							= 	[
									this.queryControl.answerControl.validator,
									this.queryControl.answerControl.asyncValidator
								]
								.filter( v => !!v)

		this.textFormControl =	new FormControl<string>(this.queryControl.answer as string, validators)

		this.queryControl.answerControl
		.valueChanges
		.pipe(
			distinctUntilChanged(),
			takeUntil(this.destroy$)
		)
		.subscribe( (value: string) =>{
			this.textFormControl.setValue(value)
		})

		this.textFormControl
		.valueChanges
		.pipe(
			distinctUntilChanged(),
			takeUntil(this.destroy$),
			debounceTime(1500)
		)
		.subscribe( value => this.queryControl.answerControl.setValue(value) )

		this.queryControl.answerControl.statusChanges
		.pipe(
			distinctUntilChanged(),
			takeUntil(this.destroy$),
		)
		.subscribe( status => status === 'DISABLED' ? this.textFormControl.disable() : this.textFormControl.enable() )
	}

	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}

}
