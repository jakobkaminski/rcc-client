import	{
			Component,
			HostListener,
		}									from '@angular/core'
import	{
			QuestionOptionConfig,
			round,
			getDecimalPlaces
		}									from '@rcc/core'

import	{
			QueryControl,
			QueryWidgetComponent
		}									from '../../queries'


function getSteps(values : number[]): number[] {

	const decimalPlaces	: number		=	getDecimalPlaces(values)
	const sortedArray	: number[]		= 	values.sort((a, b) => Math.sign(b-a))
	const setOfSteps	: Set<number>	= 	sortedArray
								.reduce(
									(acc, value, index, arr ) => {

										if(index === 0) return acc

										const diff: number = arr[index-1] - value

										return acc.add( round(diff, decimalPlaces) )
									},
									new Set<number>()
								)
	return Array.from(setOfSteps)
}


@Component({
	templateUrl: 	'./scale.component.html',
	styleUrls: 		['./scale.component.css'],
})
export class ScaleQueryWidgetComponent extends QueryWidgetComponent {


	public static widgetMatch(queryControl: QueryControl): number{

		const tags			: string[]	= 	queryControl.question.tags
		const no_scale_tag	: boolean	=	!tags?.includes('rcc-scale')
								&&
								!tags?.includes('scale')// legacy can be removed

		// Cannot handle questions like this:
		if(no_scale_tag )						return -1

		const type			: string	= 	queryControl.question.type
		const isInteger		: boolean	=	type === 'integer'
		const isDecimal		: boolean	=	type === 'decimal'

		// Cannot handle questions like this:
		if(!isInteger && !isDecimal)			return -1

		const noOptions		: boolean	= 	!queryControl.question.options?.length
		const minMax		: boolean	=	Number.isFinite(queryControl.question.min) && Number.isFinite(queryControl.question.max)

		// Made to handle questions like this (min/max based):
		if(noOptions && minMax && isInteger)	return 2

		// Cannot handle questions like this, no options no min/max (decimal entails unknown step size):
		if(noOptions)							return -1


		const options		: QuestionOptionConfig[]		= 	queryControl.question.options
		const values		: (string | number | boolean)[] = 	options.map(option => option.value)
		const non_numbers	: boolean						=	values.some( value => typeof value !== 'number')

		// Cannot handle questions like this; this should never happen since the question config is invalid at this point:
		if(non_numbers) return -1

		const steps			: number[]	=	getSteps(values as number[])
		const uniform		: boolean	= 	steps.length === 1

		// Made to handle questions like this (option based):
		if(uniform)		return 2

		return -1
	}



	// prevent other slide effects when handling the scale
	@HostListener('mousedown', ['$event'])
	@HostListener('touchstart', ['$event'])
	@HostListener('pointerdown', ['$event'])
	public onTouch(event: Event): void {
		event.stopPropagation()
	}



	public min: 		number
	public max: 		number
	public step:		number
	public length:		number
	public label_count:	number

	public pinFormatter: (value:number) => string | number

	public options: QuestionOptionConfig[]

	public constructor(
		public queryControl	: QueryControl,
	){
		super(queryControl)

		// assuming all option have number value, since widgetMatch requires it.

		const originalOptions	: QuestionOptionConfig[] = this.queryControl.question.options || []


		this.min 			= 	typeof this.queryControl.question.min === 'number'
								?	this.queryControl.question.min
								:	Math.min(...originalOptions.map( option => option.value as number ) )

		this.max 			= 	typeof this.queryControl.question.max === 'number'
								?	this.queryControl.question.max
								:	Math.max(...originalOptions.map( option => option.value as number ) )


		const values: number[]	=	originalOptions.map( option => option.value as number)

		this.step			=	getSteps(values)[0] || 1 // assuming there is only one stepsize, see widgetMatch



		const decimalPlaces: number	=	getDecimalPlaces(values)

		const size: number			=	1 + round( this.max - this.min, decimalPlaces ) / this.step

		this.options 		= 	[...Array(size).keys()]
								.map( (index:number) => {

									const value: number = round(this.min + index*this.step, decimalPlaces)

									return 	originalOptions.find( option => option.value === value )
											||
											{ value , meaning: '' }
								})

		this.pinFormatter	=	(value:number) => round(value, decimalPlaces)

		this.length 		= 	size
		this.label_count 	=	this.options.filter( option => this.hasLabel(option) ).length


	}

	public hasLabel(option: QuestionOptionConfig) : boolean {
		if(!option) 	return false

		const hasMeaning		: boolean	= 	!!option.meaning
		const hasTranslations	: boolean	= 	Object.values(option.translations || {})
									.filter( x => !!x )
									.length > 0

		return hasMeaning || hasTranslations

	}

	public getOptionStyle(index:number, option?:QuestionOptionConfig): unknown {

		const width	: number				= 	1 / this.label_count

		if(!this.hasLabel(option)) return { display: 'none' }

		let left	: number 	= index/(this.length-1) - width/2
		let right	: number 	= index/(this.length-1) + width/2

		if(index === 0){
			left 	= 0
			right 	= width
		}

		if(index === this.length-1){
			left	= 1-width
			right	= 0
		}

		return 	{
					left:			`${left*100}%`,
					right:			`${right*100}%`,
					width:			`${width*100}%`,
				}
	}


}
