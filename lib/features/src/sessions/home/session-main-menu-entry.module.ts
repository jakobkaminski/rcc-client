import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			map
		}											from 'rxjs'

import	{
			provideMainMenuEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition,
		}											from '@rcc/common'

import	{
			OpenSessionStoreService
		}											from '../open-sessions'


import	{	SessionHomePageModule,
			openSessionStoreServiceHomePath
		}											from './session-homepage.module'

@NgModule({
	imports: [
		SessionHomePageModule,
	],

})

export class SessionMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `SessionMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<SessionMainMenuEntryModule> {

		const mainMenuEntry	: Factory<Action> =	{
			deps:		[OpenSessionStoreService],
			factory:	(openSessionStoreService: OpenSessionStoreService) => ({
				icon: 			'session',
				label: 			'SESSIONS.HOME.MAIN_MENU.LABEL',
				path:			openSessionStoreServiceHomePath,
				position:		getEffectivePosition(config, 3),
				notification:	openSessionStoreService.activeSession$.pipe(map(session => !!session))
			})
		}

		const providers : Provider[] = []

		providers.push(provideMainMenuEntry(mainMenuEntry))

		return {
			ngModule:	SessionMainMenuEntryModule,
			providers
		}
	}

}
