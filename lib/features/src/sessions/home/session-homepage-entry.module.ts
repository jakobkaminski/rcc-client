import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			map
		}											from 'rxjs'

import	{
			provideHomePageEntry,
			Factory,
			Action,
		}											from '@rcc/common'

import	{
			OpenSessionStoreService
		}											from '../open-sessions'

import	{
			SessionHomePageModule,
			openSessionStoreServiceHomePath
		}											from './session-homepage.module'

interface EntryConfig {
	position?: number | undefined
}

@NgModule({
	imports: [
		SessionHomePageModule,
	],

})

export class SessionHomePageEntryModule {

	/**
	* This method can add an entry to the home page.
	*
	* Calling it without parameter, will add an entry to home
	* page automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                   |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | undefined, false, null | adds no home page entry                                   |
	* |            | true                   | adds home page entry at a reasonably adequate position    |
	* |            | positive number        | adds home page entry at position counting from the top    |
	* |            | negative number        | adds home page entry at position counting from the bottom |
	* Example: 	`SessionHomePageEntryModule.addEntry({ position: 1 })`,

	*/

	public static addEntry(config?: EntryConfig): ModuleWithProviders<SessionHomePageModule> {

		let homePageEntryPosition		: number	= 0.5

		if (config != null && 'position' in config) homePageEntryPosition = config.position

		const homePageEntryActiveSession : Factory<Action>	= 	{

			deps:		[OpenSessionStoreService],
			factory:	(openSessionStoreService: OpenSessionStoreService) => ({
								position: 		() =>  openSessionStoreService.activeSession ? homePageEntryPosition : null,
								icon:			'answer-question',
								label:			'SESSIONS.HOME.WITH_ACTIVE_SESSION.LABEL',
								description:	'SESSIONS.HOME.WITH_ACTIVE_SESSION.DESCRIPTION',
								category:		'analyze',
								notification:	openSessionStoreService.activeSession$.pipe(map(session => !!session)),
								path:			openSessionStoreServiceHomePath,
						}),
		}

		const homePageEntryNoActiveSession : Factory<Action>	=	{

			deps:		[OpenSessionStoreService],
			factory:	(openSessionStoreService : OpenSessionStoreService ) => ({
								position: 		() =>  openSessionStoreService.activeSession ? null : homePageEntryPosition,
								icon:			'answer-question',
								label:			'SESSIONS.OPEN_SESSIONS.ACTIONS.NEW_SESSION.LABEL',
								description:	'SESSIONS.OPEN_SESSIONS.ACTIONS.NEW_SESSION.DESCRIPTION',
								category:		'create',
								handler:		() => openSessionStoreService.startNewSession(),
								notification:	openSessionStoreService.activeSession$.pipe(map(session => !!session))
					}	)
		}

		const providers : Provider[] = []

		providers.push(provideHomePageEntry(homePageEntryActiveSession))
		providers.push(provideHomePageEntry(homePageEntryNoActiveSession))

		return {
			ngModule:	SessionHomePageModule,
			providers
		}
	}
}
