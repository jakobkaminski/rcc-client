import	{
			NgModule,
			Provider
		}											from '@angular/core'
import	{
			Routes,
			Router,
			RouterModule,
			provideRoutes
		}											from '@angular/router'
import	{
			Session
		}											from '@rcc/core'
import	{
			SharedModule,
			ShareDataModule,
			provideTranslationMap,
			CurrentDateMessageComponent,
			Action,
			ItemAction,
			provideItemAction,
			
		}											from '@rcc/common'
import	{
			SymptomCheckEditModule
		}											from '../../symptom-checks'

import	{
			OpenSessionModule,
			OpenSessionStoreService
		}											from '../open-sessions'
import	{	BasicSessionRepresentationModule	}	from '../basic-session-representation'
import	{	SessionHomePageComponent			}	from './session-homepage.component'
import	{	QuestionnaireEditorModule			}	from '@rcc/features/questionnaire-editor/questionnaire-editor.module'
import	{	RccEditActiveSymptomCheckComponent	}	from './edit-active-symptom-check/edit-active-symptom-check.component'
import	{	canDeactivateMonitoringSetupFn		}	from '@rcc/features/monitoring-setup/can-deactivate-monitoring-setup-fn'

import	en	from './i18n/en.json'
import	de	from './i18n/de.json'

export interface SessionHomeModuleConfig {
	mainMenuEntry?: 		boolean | number,
	homePageEntry?: 		boolean | number
}

export const openSessionStoreServiceHomePath	: string = 'sessions/home'
const activeSymptomCheckEditPath				: string = 'edit-active-symptom-check'

const routes : Routes 	=	[
								{
									path: openSessionStoreServiceHomePath,
									component: SessionHomePageComponent,
								},
								{
									path: `${openSessionStoreServiceHomePath}/${activeSymptomCheckEditPath}`,
									component: RccEditActiveSymptomCheckComponent,
									canDeactivate: [canDeactivateMonitoringSetupFn]
								}
							]

export const openSessionAction : ItemAction<Session> = {
	role:		'edit',
	itemClass:	Session,
	storeClass:	OpenSessionStoreService,
	getAction:	(session: Session, openSessionStoreService: OpenSessionStoreService) : Action => ({
					icon: 			'open',
					label: 			'SESSIONS.HOME.ITEM_ACTIONS.OPEN_SESSION.LABEL',
					category: 		'misc',
					failureMessage: 'SESSIONS.HOME.ITEM_ACTIONS.OPEN_SESSION.FAIL',
					successMessage: 'SESSIONS.HOME.ITEM_ACTIONS.OPEN_SESSION.SUCCESS',
					handler: 		() => openSessionStoreService.activate(session)
				})
}



@NgModule({
	providers:[
		provideItemAction(openSessionAction),
		provideRoutes(routes) as Provider, // somehow eslint doesn't get this right and marks the whole array as problematic
		provideTranslationMap('SESSIONS.HOME', { en, de }),
	],
	imports: [
		RouterModule.forChild(routes),
		SharedModule,
		ShareDataModule,
		OpenSessionModule,
		SymptomCheckEditModule,
		BasicSessionRepresentationModule,
		CurrentDateMessageComponent,
		QuestionnaireEditorModule,
	],
	declarations :[
		SessionHomePageComponent,
		RccEditActiveSymptomCheckComponent,
	]
})

export class SessionHomePageModule {

	public constructor(
		router					: Router,
		openSessionStoreService	: OpenSessionStoreService,
	){

		// Whenever another session is opened, go to session home
		openSessionStoreService.activeSession$
		.subscribe( session => {
			if(session) void router.navigateByUrl(openSessionStoreServiceHomePath)
		})
	}

}
