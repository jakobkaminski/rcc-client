import	{
			Component,
			OnDestroy
		} 													from '@angular/core'
import	{	Router 										}	from '@angular/router'
import	{
			Question,
			QuestionConfig,
			Report,
			Session,
			SymptomCheck,
			SymptomCheckConfig,
			assert,
		} 													from '@rcc/core'
import	{
			map,
			merge,
			concat,
			mergeMap,
			Observable,
			Subject,
			takeUntil,
		} 													from 'rxjs'
import	{
			Action,
			ItemAction,
			RccAlertController,
			RccToastController,
			ShareDataService,
			ItemSelectService,
			HandlerAction,
			ExportConfig,
			RccExportService,
		}													from '@rcc/common'
import	{	QuestionnaireService						} 	from '../../questions'
import	{	OpenSessionStoreService 					}	from '../open-sessions'

@Component({
	templateUrl:	'./session-homepage.component.html',
	styleUrls:		['./session-homepage.component.scss']
})
export class SessionHomePageComponent implements OnDestroy {

	public sessionActions: ItemAction<Session>[] = [
		{
			itemClass: Session,
			getAction: (session: Session) => {
				const config: ExportConfig = {
					data: session.report,
				}

				const action: HandlerAction = {
					icon: 'download',
					label: 'SESSIONS.HOME.DOWNLOAD',
					handler: () => void this.rccExportService.runExport(config)
				}
				return action
			},
			role: undefined
		}
	]
	public symptomCheckActions: ItemAction<SymptomCheck>[] = [
		{
			itemClass: 			SymptomCheck,
			role: 				'edit',
			getAction: 			(symptomCheck: SymptomCheck) => ({
				label: 			'SESSIONS.HOME.EDIT_ACTIVE_SYMPTOM_CHECK.LABEL',
				description: 	'SESSIONS.HOME.EDIT_ACTIVE_SYMPTOM_CHECK.DESCRIPTION',
				icon: 			'edit',
				path:			'edit-active-symptom-check',
				position: 		() =>	this.activeSymptomCheck === symptomCheck
										?	undefined 	// no preference for position
										:	null		// do not show at all
			})
		},
	]

	public activeSymptomCheckUpdated: Observable<boolean>

	public destroy$: Subject<void> = new Subject<void>()

	public constructor(
		private itemSelectService		: ItemSelectService,
		private openSessionStoreService	: OpenSessionStoreService,
		private questionnaireService	: QuestionnaireService,
		private rccAlertController		: RccAlertController,
		private rccToastController		: RccToastController,
		private router					: Router,
		private shareDataService		: ShareDataService,
		private rccExportService		: RccExportService,
	) {
		this.setup()
	}

	protected viewDataAction: HandlerAction = {
		icon: 'view_data',
		label: 'SESSIONS.HOME.VIEW_DATA',
		handler: () => undefined,
	}

	protected get activeSession(): Session | null { return this.openSessionStoreService.activeSession }
	protected get activeSymptomCheck(): SymptomCheck { return this.openSessionStoreService.activeSession?.symptomCheck }
	protected get activeReport(): Report | null { return this.openSessionStoreService.activeSession?.report }
	protected get startDate(): Date { return this.openSessionStoreService.activeSession?.startDate }
	protected get activeSymptomCheckBackup(): SymptomCheckConfig { return this.openSessionStoreService.symptomCheckBackups.get(this.activeSymptomCheck) }

	public closeActiveSessionAction: Action = {

		label: 			'SESSIONS.HOME.CLOSE_SESSION.LABEL',
		description: 	'SESSIONS.HOME.CLOSE_SESSION.DESCRIPTION',
		icon: 			'close',
		category:		'misc',
		handler:		 () => this.closeActiveSession(),

	}

	public setup(): void {

		this.activeSymptomCheckUpdated = merge(
			this.openSessionStoreService.activeSession$,
		)
			.pipe(
				mergeMap((session) => concat([this.activeSession?.symptomCheck], session?.symptomCheck?.update$ || [])),
				map(() => this.getActiveSymptomCheckUpdate()),
			)

		this.openSessionStoreService.removal$
			.pipe(takeUntil(this.destroy$))
			.subscribe(() => (this.openSessionStoreService.items.length === 0) && this.exit())

		if (!this.activeSession) this.handleMissingSession()
	}


	public getActiveSymptomCheckUpdate(): boolean {

		if (!this.activeSymptomCheck) return false
		if (!this.activeSymptomCheckBackup) return false

		const activeConfigJson: string = JSON.stringify(this.activeSymptomCheck.config)
		const backupConfigJson: string = JSON.stringify(this.activeSymptomCheckBackup)

		if (activeConfigJson === backupConfigJson) return false

		return true
	}

	public async startNewSession(): Promise<void> {
		return await this.openSessionStoreService.startNewSession()
	}

	public async addNewSymptomCheck(): Promise<void> {

		assert(this.activeSession, 'SessionHomePageComponent.addNewSymptomCheck() no active session.')

		await this.openSessionStoreService.addNewSymptomCheck(this.activeSession)
	}

	public async closeActiveSession(): Promise<void> {

		assert(this.activeSession, 'SessionHomePageComponent.closeActiveSession() no active session.')


		await this.rccAlertController.confirm('SESSIONS.HOME.CLOSE_SESSION.CONFIRM')

		this.openSessionStoreService.close(this.activeSession)
	}

	public restoreOriginalSymptomCheck(): void {
		assert(this.activeSymptomCheck, 'editActiveSymptomCheck.closeActiveSession() no active symptom check.')

		this.openSessionStoreService.restoreOriginalSymptomCheck(this.activeSymptomCheck)
	}

	public handleMissingSession(): void {
		this.exit()
	}

	public async share(): Promise<void> {

		assert(
			this.openSessionStoreService.activeSession,
			'SessionHomePageComponent.share() no open session. Please open a session before trying to share.'
		)

		const symptomCheckConfig	: SymptomCheckConfig = this.activeSymptomCheck.config
		const questionIds			: string[] = this.activeSymptomCheck.questionIds

		const questions				: Question[] = await this.questionnaireService.get(questionIds)
		const questionConfigs		: QuestionConfig[] = questions.map(question => question.config)

		const data					: [SymptomCheckConfig, QuestionConfig[]] = [symptomCheckConfig, questionConfigs]

		return await this.shareDataService.share(data, 'send')
	}

	public async switch(): Promise<void> {

		const result: Session[] = await this.itemSelectService.select<Session>({
			stores: [this.openSessionStoreService],
			singleSelect: true
		})

		const session: Session = result[0]

		if (session) this.openSessionStoreService.activate(session)
	}

	protected closeAction: Action = {
		label: 'CLOSE',
		icon: 'close',
		handler: () => this.exit()
	}

	protected exit(): void {

		if (this.openSessionStoreService.items.length === 0) void this.rccToastController.present({ message: 'SESSIONS.HOME.ALL_SESSIONS_CLOSED' })

		void this.router.navigateByUrl('/')
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
