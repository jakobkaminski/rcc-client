import 	{
			Component,
			ViewEncapsulation
		}								from '@angular/core'

import	{	ExposeTemplateComponent		}	from '@rcc/common'

import	{	Session						}	from '@rcc/core'


@Component({
	selector: 		'rcc-session-label',
	templateUrl : 	'./basic-session-label.component.html',
	styleUrls:		['./basic-session-label.component.css'],
	encapsulation:	ViewEncapsulation.None
})
export class BasicSessionLabelComponent extends ExposeTemplateComponent {

	public constructor( public session : Session ){ super() }

	private _dateFormat: Intl.DateTimeFormatOptions = {
		weekday: 'long',
		day: '2-digit',
		month: '2-digit',
		year: 'numeric',
		hour: '2-digit',
		minute: '2-digit',
	}
	protected dateFormat: Record<string, string> = this._dateFormat as Record<string, string>

	protected get questionCount(): number {
		return this.session.symptomCheck?.questionIds?.length
	}
}
