import	{
			NgModule,
		} 										from '@angular/core'

import	{
			IncomingDataServiceModule,
			provideTranslationMap,
			Action,
			ItemAction,
			provideItemAction

		}										from '@rcc/common'

import	{
			Session
		}										from '@rcc/core'

import	{	OpenSessionStoreService			}	from './open-session-store.service'
import	{	OpenSessionQuestionStoreService	}	from './open-session-question-store.service'
import	{	OpenSessionReportStoreService			}	from './open-session-report-store.service'
import	{	OpenSessionHomePageEntryService	}	from './open-session-homepage-entry.service'

import	{	ReportMetaStoreServiceModule	}	from '../../reports'
import	{	QuestionnaireServiceModule		}	from '../../questions'

import en from './i18n/en.json'
import de from './i18n/de.json'

export const closeSessionAction : ItemAction<Session> = {
	role:		'destructive',
	itemClass:	Session,
	storeClass:	OpenSessionStoreService,
	getAction:	(session: Session, openSessionStoreService: OpenSessionStoreService) : Action => ({
					icon: 'close',
					label: 'SESSIONS.OPEN_SESSIONS.ACTIONS.CLOSE_SESSION.LABEL',
					category: 'misc',
					failureMessage: 'SESSIONS.OPEN_SESSIONS.ACTIONS.CLOSE_SESSION.FAIL',
					successMessage: 'SESSIONS.OPEN_SESSIONS.ACTIONS.CLOSE_SESSION.SUCCESS',
					confirmMessage: 'SESSIONS.OPEN_SESSIONS.ACTIONS.CLOSE_SESSION.CONFIRM',
					handler: () => {
						openSessionStoreService.close(session)
					}
				})
}

@NgModule({
	imports:[
		IncomingDataServiceModule,
		ReportMetaStoreServiceModule.forChild([OpenSessionReportStoreService]),
		QuestionnaireServiceModule.forChild([OpenSessionQuestionStoreService])
	],
	providers:[
		provideItemAction(closeSessionAction),
		provideTranslationMap('SESSIONS.OPEN_SESSIONS', { en, de } ),
		OpenSessionStoreService,
		OpenSessionReportStoreService,
		OpenSessionQuestionStoreService,
		OpenSessionHomePageEntryService
	]
})
export class OpenSessionModule {}
