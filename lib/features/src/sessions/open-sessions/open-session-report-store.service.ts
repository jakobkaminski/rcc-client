import	{
			Injectable
		}								from '@angular/core'

import	{
			Report,
			ReportStore
		}								from '@rcc/core'


import	{
			OpenSessionStoreService
		}								from './open-session-store.service'


// TODO: this is not needed

@Injectable()
export class OpenSessionReportStoreService extends ReportStore {

	public name = 'SESSIONS.OPEN_SESSIONS.REPORT_STORE.NAME'

	public constructor(
		private openSessionStoreService : OpenSessionStoreService
	){
		super()
	}

	public get items() : Report[] {

		const report = this.openSessionStoreService.activeSession?.report

		if(!report) return []

		return [report]
	}

}
