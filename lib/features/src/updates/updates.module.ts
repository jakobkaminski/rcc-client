import	{
			NgModule,
		}								from '@angular/core'

import {
			RccServiceWorkerModule,
			MainMenuModule,
			SharedModule,
			provideTranslationMap,
		}								from '@rcc/common'


import	{
			UpdateService
		}								from './updates.service'


import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	imports:[
		RccServiceWorkerModule,
		MainMenuModule,
		SharedModule
	],
	providers: [
		UpdateService,
		provideTranslationMap('UPDATES', { en, de }),
	]
})
export class UpdateModule {

	public constructor(
		private updateService	: UpdateService,
	) {}
}
