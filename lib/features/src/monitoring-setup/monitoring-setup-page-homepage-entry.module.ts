import	{
			NgModule,
			ModuleWithProviders,
			Provider,
		}									from '@angular/core'
import	{
			Action,
			getEffectivePosition,
			WithPosition,
			provideHomePageEntry
		}									from '@rcc/common'
import	{	RccMonitoringSetupModule	}	from './monitoring-setup.module'

@NgModule({
	imports:[
		RccMonitoringSetupModule,
	],
})
export class RccMonitoringSetupHomePageEntryModule {

/**
* This method can add entries to the home page.
*
* Calling it without parameter, will add entries to the home
* page automatically at reasonably adequate positions.
*
* If you want to determine the positions yourself provide a `config` parameter :
*
* | .position  | value                  | effect                                                    |
* |------------|------------------------|-----------------------------------------------------------|
* | .position  | true                   | adds home page entry at a reasonably adequate position    |
* |            | positive number        | adds home page entry at position counting from the top    |
* |            | negative number        | adds home page entry at position counting from the bottom |
* |            | undefined        		| adds home page entry at a position somewhere in between   |
* |------------|------------------------|-----------------------------------------------------------|
*
* Example: 	`RccMonitoringSetupHomePageEntryModule.addEntry({ position: 1 })`,
*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<RccMonitoringSetupHomePageEntryModule> {

		const homePageEntry: Action = {
			label:			'MONITORING_SETUP.HOMEPAGE_ENTRY.LABEL',
			description:	'MONITORING_SETUP.HOMEPAGE_ENTRY.DESCRIPTION',
			icon:			'monitoring_setup',
			position:		getEffectivePosition(config, 1),
			path:			'monitoring-setup',
			category:		'create',
		}

		const providers : Provider[] = []

		providers.push(provideHomePageEntry(homePageEntry))

		return {
			ngModule:	RccMonitoringSetupHomePageEntryModule,
			providers
		}
	}
}
