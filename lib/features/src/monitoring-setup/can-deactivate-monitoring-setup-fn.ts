import { CanDeactivateFn } from '@angular/router'
import { RccMonitoringSetupPageComponent } from './monitoring-setup-page.component'
import { inject } from '@angular/core'
import { RccAlertController } from '@rcc/common'
import { RccQuestionnaireEditorService } from '../questionnaire-editor/questionnaire-editor.service'

export const canDeactivateMonitoringSetupFn: CanDeactivateFn<RccMonitoringSetupPageComponent> = () => {
    const rccAlertController        : RccAlertController
                                    = inject(RccAlertController)
    
    const rccQuestionnaireEditorService : RccQuestionnaireEditorService
                                    = inject(RccQuestionnaireEditorService)
    
    if(rccQuestionnaireEditorService.dirty)
        return rccAlertController
            .confirm(
                'MONITORING_SETUP.CONFIRM_RESET',
                'YES',
                'NO'
            )
            .then(
                async () => {
                    await rccQuestionnaireEditorService.reset()
                    return true
                },
                () => false
            )
    
    return true
}
    
