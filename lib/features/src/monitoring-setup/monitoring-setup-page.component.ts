import	{
			Component,
			OnInit,
		}									from '@angular/core'
import	{
			RccTitleService,
			RccTranslationService,
			ShareDataService,
		}									from '@rcc/common'
import	{	ConfirmPayload				}	from '../questionnaire-editor/questionnaire-editor/questionnaire-editor.component'

@Component({
	selector	:	'rcc-monitoring-setup-page',
	templateUrl	:	'./monitoring-setup-page.component.html',
	styleUrls	:	['./monitoring-setup-page.component.scss'],
})
export class RccMonitoringSetupPageComponent implements OnInit {

	private questionnaireNamePrefix : string 									= 'MONITORING_SETUP.QUESTIONNAIRE'
	private questionnaireName	: string 										= `HCP-${(new Date()).getTime()}`
	protected editName			: boolean 										= false
	protected initialName		: string

	public constructor (
		private readonly rccTitleService			: RccTitleService,
		private readonly shareDataService			: ShareDataService,
		rccTranslationService		: RccTranslationService,
	) {
		this.initialName = `${rccTranslationService.translate(this.questionnaireNamePrefix)} ${this.questionnaireName}`
	}

	public ngOnInit(): void {

		this.rccTitleService.setTitle('MONITORING_SETUP.PAGE_TITLE')
	}


	protected async shareFollowUp(event: ConfirmPayload): Promise<void> {
		await this.shareDataService.share([event.config, event.questions], 'send')
	}
}
