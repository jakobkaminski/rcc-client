import	{
			Injectable,
			NgModule
		}								from '@angular/core'
import	{
			ExportConfig,
			ExportService,
			ExportMetadata,
			ExportResult,
			provideExportService,
			provideTranslationMap
		}								from '@rcc/common'
import	{
			assert,
			Report
		}								from '@rcc/core'
import	{
			ZipService
		}								from './zip.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

// The following ExportService will probably not have a use on it's own, but
// in conjunction with some other export capability.
// I leave it in as a comment to provide an example for more specialized export
// services that make use of zip files.
//
// @Injectable()
// export class ReportZipExportService extends ExportService<Report,Blob> {
// 	public label	 	: string	= 'ZIP_MODULE.EXPORT.LABEL'
// 	public description	: string	= 'ZIP_MODULE.EXPORT.DESCRIPTION'

// 	public constructor(
// 		public zipService: ZipService
// 	) {
// 		super()
// 	}

// 	public canExport(x: unknown): x is Report {
// 		return x instanceof Report
// 	}

// 	public async export(x: ExportConfig<Report>): Promise<ExportResult<Blob>> {
// 		assert(this.canExport(x.data), 'ReportZipExportService.export: x.data must be of type Report')
// 		const reportFilename : string = `RCC-Report-${x.data.date.toISOString().split('T')[0]}.json`
// 		const zip : Blob = await this.zipService.createZipOfReport(x.data, reportFilename)

// 		return {
// 			toBlob	: () => zip,
// 			raw		: () => zip
// 		}
// 	}

// 	public getMetadata(x: ExportConfig<Report>): ExportMetadata {
// 		assert(this.canExport(x.data), 'ReportZipExportService.export: x.data must be of type Report')
// 		return {
// 			suggestedFilename	: `RCC-Report-${x.data.date.toISOString().split('T')[0]}.zip`,
// 			mimeType			: 'application/zip'
// 		}
// 	}
// }

@NgModule({
	providers: [
		ZipService,
		provideTranslationMap('ZIP_MODULE', { en,de })
	]
})
export class ZipModule {}
