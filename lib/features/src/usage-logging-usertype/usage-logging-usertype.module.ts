import	{
			NgModule
		}								from '@angular/core'

import	{
			RccUsageLoggingJob,
			RccUsageLoggingService
		}								from '../usage-logging'

import	{
			Subject
		}								from 'rxjs'

import	{
			provideTranslationMap,
		}								from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	providers: [
		provideTranslationMap('USAGE_LOGGING_USERTYPE', { en, de })
	]

})

export class RccUsageLoggingUsertypeModule {

	public constructor(rccUsageLoggingService : RccUsageLoggingService) {

		const observable	:	Subject<string> = new Subject<string>()

		const job : RccUsageLoggingJob = {
			tick$		:		observable,
			description	:		'DESCRIPTION',
			label		:		'LABEL',
			loggingData	: 		{
				key					:	'siteLoad',
				category			:	'usageLoggingUsertype',
				includeUserId		:	true,
				requiresUserConsent	: 	false
			}
		}

		rccUsageLoggingService.registerLoggingJob(job)

		observable.next('_')

	}

}
