import  {
			AsyncPipe,
			NgForOf,
			NgIf
		}                                       from '@angular/common'
import  {
			Component,
			ElementRef,
			Input,
			OnChanges,
			OnInit,
			SimpleChanges,
			Type,
			ViewChild
		}                                       from '@angular/core'
import  {
			TranslationsModule,
			RccCardComponent,
			WidgetsModule,
			HandlerAction,
			RccModalController,
			RccTranslationService,
			RccSliderModule,
			RccSliderComponent,
			RccIconComponent,
			SharedModule,
			RccAlertController,
		}                                       from '@rcc/common'
import  {
			Question,
			QuestionConfig,
			Schedule,
			uuidv4
		}                                       from '@rcc/core'
import  {
			QueryControl
		}                                       from '@rcc/features/queries'
import  {
			RccThemeModule
		}                                       from '@rcc/themes/active'
import  {
			first,
			firstValueFrom,
			forkJoin,
			map,
			Observable,
			of,
			switchMap,
			take,
			tap
		}                                       from 'rxjs'
import  {
			AddSelectedQuestionsFormData,
			AddQuestionModalComponent,
			AddQuestionFormData
		}                                       from '../../modal/add-question/add-question-modal.component'
import  {
			FormData as EditQuestionFormData,
			EditQuestionModalComponent,
		}                                       from '../../modal/edit-question/edit-question-modal.component'
import	{
			EditMedicationModalComponent,
}									            from '../../modal/edit-medication/edit-medication-modal.component'
import  {
			CategorizedQuestionWithSchedule,
			isCategorizedQuestionWithSchedule,
			RccQuestionnaireEditorService
		}                                       from '../../questionnaire-editor.service'

interface DragAndDropPayload {
	questionId: string
	fromCategory: string
}

@Component({
	selector: 'rcc-question-lane',
	templateUrl: './question-lane.component.html',
	styleUrls: ['./question-lane.component.scss'],
	standalone: true,
	imports: [
				NgIf,
				NgForOf,
				AsyncPipe,
				TranslationsModule,
				RccCardComponent,
				WidgetsModule,
				RccSliderModule,
				RccThemeModule,
				RccIconComponent,
				SharedModule,
			]
})
export class RccQuestionLaneComponent implements OnInit, OnChanges {

	@Input() public category        : string | null                 = null
	@Input() public mode            : 'edit' | 'preview' | 'init'   = 'edit'

	@ViewChild('questionListContainer')
	private questionListContainer: ElementRef<HTMLDivElement>

	private _slider: RccSliderComponent

	@ViewChild(RccSliderComponent)
	private set slider(value: RccSliderComponent) {
		this._slider = value
		if (value != null)
			value.restoreState = () => {
				this.slider?.slideTo(0)
			}
	}
	private get slider(): RccSliderComponent {
		return this._slider
	}

	public categorizedQuestions$    : Observable<CategorizedQuestionWithSchedule[]>
									= of([] as CategorizedQuestionWithSchedule[])

	private hasQuestions            : boolean                       = false

	protected queryControls 	    : Map<Question, QueryControl>   = new Map<Question, QueryControl>()

	// #region getters

	public get addQuestion(): HandlerAction {
		return {
			label:		`QUESTIONNAIRE_EDITOR.${this.category.toUpperCase()}.BUTTON`,
			category: 	'create',
			icon:		this.category,
			handler:    async () => {
							await this.addCatalogueOrCustomQuestion()
						}
		} as HandlerAction
	}

	public get hide(): boolean {
		return this.mode === 'preview' && !this.hasQuestions
	}

	// #endregion

	public constructor(
		private readonly rccMonitoringSetupService: RccQuestionnaireEditorService,
		private readonly rccModalController: RccModalController,
		private readonly translationService: RccTranslationService,
		private readonly rccAlertController: RccAlertController
	) {
	}

	protected allowDragAndDrop: boolean

	public ngOnInit(): void {
		this.allowDragAndDrop = this.rccMonitoringSetupService.allowDragAndDrop(this.category)
	}

	public ngOnChanges(changes: SimpleChanges): void {
		const newCategory : string | null = changes['category']?.currentValue as string | null

		if(typeof newCategory !== 'undefined')
			this.categorizedQuestions$ = this.rccMonitoringSetupService
											.getQuestionsByCategory$(newCategory)
											.pipe(
												tap((questions: CategorizedQuestionWithSchedule[]) => {
												this.hasQuestions = questions.length > 0
											}))
	}

	// #region adding questions

	private async addCatalogueOrCustomQuestion() : Promise<void> {
		const result : AddSelectedQuestionsFormData = await firstValueFrom<AddSelectedQuestionsFormData>(
			this.getAddQuestionModalOptions$()
				.pipe(switchMap((options) => this.rccModalController.present<AddQuestionModalComponent, AddSelectedQuestionsFormData>(
						AddQuestionModalComponent,
						options,
					)
				))
			)

		if (result === null)
			return

		// adding a custom question is mutually exclusive
		// with adding/removing questions from the catalogue
		if(result.customQuestion) {
			this.rccMonitoringSetupService.addQuestion(
				result.customQuestion,
				this.category,
				result.schedule
			)
			return
		}

		const selectedIds		: Set<string> 	= new Set(result.selectedQuestions)
		const currentlySelected	: Set<string> 	= new Set(await firstValueFrom(this.getSelectedQuestionIds$(this.category)))

		const toRemove			: string[] 		= [...currentlySelected].filter((id) => !selectedIds.has(id))
		const toAdd				: string[] 		= [...selectedIds].filter((id) => !currentlySelected.has(id))

		this.rccMonitoringSetupService.addMultipleQuestions(toAdd, this.category)
		this.rccMonitoringSetupService.removeMultipleQuestions(toRemove, this.category)
	}

	private getAddQuestionModalOptions$(): Observable<object> {
		return forkJoin({
			disabledIds			: 	this.getDisabledQuestionIds$(this.category),
			selected			: 	this.getSelectedQuestionIds$(this.category),
			questions			: 	of(this.rccMonitoringSetupService.catalogue),
			category			: 	of(this.category),
			baseScheduleDays	: 	this.rccMonitoringSetupService.baseSchedule$.pipe(
										take(1),
										map((baseSchedule: Schedule) => baseSchedule.daysOfWeek)
									)
		})
	}

	private getDisabledQuestionIds$(category: string|null): Observable<string[]> {
		// When a user selects questions from a given category (A), then opens
		// another category (B), the questions that were previously selected
		// in A should show up in B, marked as selected, but should be disabled
		// so that the user can't reselect them, or disable them. This functionality
		// may be revised at a later date
		return this.rccMonitoringSetupService.questions$
			.pipe(
				first(),
				map(
					(questions: CategorizedQuestionWithSchedule[]) =>
					questions
						.filter((item: CategorizedQuestionWithSchedule) => item.category !== category)
						.map((item: CategorizedQuestionWithSchedule) => item.question.id)
				)
			)
	}

	private getSelectedQuestionIds$(category: string | null): Observable<string[]> {
		return this.rccMonitoringSetupService.getQuestionsByCategory$(category)
			.pipe(
				first(),
				map((questions: CategorizedQuestionWithSchedule[]) =>
					new Set(questions.map((item: CategorizedQuestionWithSchedule) => item.question.id))
				),
				map((questionIds: Set<string>) => [...questionIds])
			)
	}

	// #endregion


	// #region editing questions
	protected getEditAction(categorizedQuestion : CategorizedQuestionWithSchedule): HandlerAction {
		return {
			icon: 'edit',
			handler: async () => this.editQuestion(categorizedQuestion),
			label: this.translationService.translate('EDIT') + ': ' + categorizedQuestion.question.config.translations[this.translationService.activeLanguage]
		}
	}

	private async editQuestion(categorizedQuestion : CategorizedQuestionWithSchedule): Promise<void> {
		const question 		: 	Question 	= categorizedQuestion.question
		const previousId 	: 	string 		= question.id

		const component: Type<unknown> = this.category === 'medication' ? EditMedicationModalComponent : EditQuestionModalComponent

		const result : EditQuestionFormData | string = await this.rccModalController.present(
			component,
			{
				question,
				category			: this.category,
				schedule			: categorizedQuestion.schedule,
				baseScheduleDays	: this.rccMonitoringSetupService.baseScheduleDays
			}
		)

		// EditQuestionModalComponent has a "Question Catalogue" button that dismisses
		// the modal and returns string 'question-list' when clicked. If that is the case
		// the question catalogue modal should open instead.
		if (result === 'question-list') {
			void this.addCatalogueOrCustomQuestion()
			return
		}

		if(result === null || typeof result === 'string') return

		this.rccMonitoringSetupService.updateQuestion(
			previousId,
			{
				question	: 	this.questionFormDataToQuestionConfig(uuidv4(), result),
				schedule	: 	result.schedule,
				category	: 	this.category
			}
		)
	}

	private questionFormDataToQuestionConfig(id: string, formData: EditQuestionFormData | AddQuestionFormData): QuestionConfig {
		const language	: string = this.translationService.activeLanguage
		return {
			id,
			meaning			: 	formData.questionTitle,
			translations	: 	'translations' in formData ? formData.translations : {
									[language]: formData.questionTitle
								},
			options			: 	formData.options,
			type			: 	formData.answerType,
			tags			:	formData.tags,
			min				:	formData.min,
			max				:	formData.max,
			unit			:	'unitText' in formData ? formData.unitText : undefined
		}
	}
	// #endregion

	protected questionHeading(question: Question): string {
		const questionName: string = this.translationService.translate(question)
		const unitText: string = question.unit

		if (!unitText)
			return questionName
		
		return `${questionName} (${unitText})`
	}


	// #region removing questions
	protected getRemoveAction(question: CategorizedQuestionWithSchedule):  HandlerAction {
		return {
			label: this.translationService.translate('REMOVE') + ': ' + question.question.config.translations[this.translationService.activeLanguage],
			icon: 'delete_circle',
			handler: () => { this.removeQuestion(question).catch(err => console.error(err)) }
		}
	}
	private async removeQuestion(question: CategorizedQuestionWithSchedule | Question | string): Promise<void> {

		await this.confirmDeleteQuestion()

		if(isCategorizedQuestionWithSchedule(question))
			this.rccMonitoringSetupService.removeQuestion(question)
		else
			this.rccMonitoringSetupService.removeQuestion(question, this.category)
	}

	private async confirmDeleteQuestion(): Promise<void> {

		const header	: string
						= 'QUESTIONNAIRE_EDITOR.DELETE_QUESTION_ALERT.HEADER'

		await this.rccAlertController.present({

			header,
			buttons:	[
							{
								label:		'NO',
								rejectAs: 	'dismiss'
							},
							{
								label:		'YES',
								resolveAs: 	'confirm'
							}
						]
		})
	}

	// #endregion

	public getQueryControl(question: Question): QueryControl {
		const queryControl	: 	QueryControl
							=	this.queryControls.get(question)
								||
								new QueryControl(
									question,
									() => undefined, // Do not do anything on submission
									() => undefined // Do not do anything on clean up
								)
		this.queryControls.set(question, queryControl)
		queryControl.disableForms()
		return queryControl
	}

	// #region drag and drop

	private _startDragging: boolean = false
	protected get startDragging(): boolean {
		return this._startDragging
	}

	protected set startDragging(value: boolean) {
		this._startDragging = value

		if (value === true)
			setTimeout(() => this._startDragging = false, 1)
		
	}

	private isDragSource: boolean = false

	protected onDragStart($event: DragEvent, question: CategorizedQuestionWithSchedule): void {
		this.startDragging = true
		this.isDragSource = true

		const payload: DragAndDropPayload = {
			questionId: question.question.id,
			fromCategory: this.category
		}

		$event.dataTransfer.setData(
			'text/plain',
			JSON.stringify(payload),
		)
	}

	protected onDragEnd(): void {
		this.isDragSource = false
	}

	protected droppedId: string

	protected onDrop(event: DragEvent): void {
		const data: string = event.dataTransfer.getData('text/plain')
		if (!data) return

		const payload: DragAndDropPayload = JSON.parse(data) as DragAndDropPayload
		this.dragHover = false

		if (this.category === payload.fromCategory)
			return

		// We need to wait briefly so that the dragend event can be fired. Otherwise
		// the source element was already removed from the DOM, and the event can't
		// be triggered.
		setTimeout(() => {
			this.rccMonitoringSetupService.moveQuestion(payload.questionId, this.category)
			this.droppedId = payload.questionId
		}, 1)
	}

	protected allowDrop(event: DragEvent): void {
		event.preventDefault()
		event.dataTransfer.dropEffect = 'move'
	}

	protected dragHover: boolean = false

	protected onDragEnter(): void {
		if (this.isDragSource) return

		this.dragHover = true
	}

	protected onDragLeave(event: DragEvent): void {
		// Because the dragleave event gets fired when the user hovers over children
		// of the question lane, we need to check whether the new element that's being
		// dragged over is a child of the question lane. If it is we retain the hover
		// effect, otherwise the user has left the question lane, and we can remove
		// the hover effect
		const newElementBeingEntered: HTMLElement = event.relatedTarget as HTMLElement
		const isChildOfQuestionLane: boolean = this.questionListContainer.nativeElement.contains(newElementBeingEntered)

		if (isChildOfQuestionLane) return

		this.dragHover = false
	}

	// #endregion
}
