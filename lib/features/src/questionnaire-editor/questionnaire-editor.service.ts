import	{
			Inject,
			Injectable,
			InjectionToken,
			Optional
		}											from '@angular/core'
import	{
			SymptomCheck,
			Question,
			isErrorFree,
			has,
			assert,
			Schedule,
			QuestionConfig,
			toISO8601CalendarDate,
			SymptomCheckMetaConfig,
			ScheduleConfig,
			SymptomCheckConfig
		}											from '@rcc/core'
import 	{
			BehaviorSubject,
			concatMap,
			firstValueFrom,
			forkJoin,
			from,
			map,
			mergeMap,
			Observable,
			of,
			switchMap,
			take,
			filter,
			toArray,
			merge,
			takeUntil,
			Subject,
			skip
		} 											from 'rxjs'
import 	{
			CuratedDefaultQuestionStoreService
		} 											from '../curated/curated-questions/curated-default-questions'
import 	{
			CuratedExtendedQuestionStoreService
		} 											from '../curated/curated-questions/curated-extended-questions'
import 	{
			QuestionnaireService
		} 											from '../questions'

export const MONITORING_SETUP_CONFIG 	: InjectionToken<MonitoringSetupConfig>
										= new InjectionToken<MonitoringSetupConfig>('MonitoringSetupConfig')

export interface QuestionList {
	id			: string
	questions	: Question[]
}

export interface CategorizedQuestionWithSchedule {
	question	: Question;
	category	: string | null;
	schedule	: Schedule | null;
}

type QuestionUpdate  = Partial<QuestionConfig> & Required<Pick<QuestionConfig, 'id'>>;

interface CategorizedQuestionWithScheduleUpdate {
	question: QuestionUpdate;
	category?: string|null;
	schedule?: Schedule|null;
}

export interface MonitoringSetupConfig {
	questionCategories			: string[];
	hiddenOnEditCategories		: string[];
	hiddenInCatalogue			: string[];
	defaultQuestionsByCategory	: Map<string, string[]>;
	addTemplateQuestionsTo		: string|null;
	defaultScheduleConfig		: ScheduleConfig;
}

/*
export const MONITORING_SETUP_CONFIG 	: InjectionToken<MonitoringSetupConfig>
										= new InjectionToken<MonitoringSetupConfig>('MonitoringSetupConfig')
*/

function assertIsCategorizedQuestionWithSchedule(x: unknown) : asserts x is CategorizedQuestionWithSchedule {
	assert(has(x, 'question'), 'isCategorizedQuestionWithSchedule(): missing .question on x', x)
	assert(Object.keys(x).length <= 3, 'isCategorizedQuestionWithSchedule(): x has extra properties.', x)
	assert(has(x, 'category'), 'isCategorizedQuestionWithSchedule(): missing .category on x', x)
	assert(has(x, 'schedule'), 'isCategorizedQuestionWithSchedule(): missing .schedule on x', x)
	assert(x.question instanceof Question, 'isCategorizedQuestionWithSchedule(): .question must be instance of Question on x', x)
	assert(typeof x.category === 'string' || x.category === null, 'isCategorizedQuestionWithSchedule(): .category must be of type string or null on x', x)
	assert(x.schedule instanceof Schedule || x.schedule === null, 'isCategorizedQuestionWithSchedule(): .schedule must be instance of Schedule or null on x', x)
}

export function isCategorizedQuestionWithSchedule(x: unknown) : x is CategorizedQuestionWithSchedule {
	return isErrorFree(() => assertIsCategorizedQuestionWithSchedule(x))
}

@Injectable()
export class RccQuestionnaireEditorService {

	private questionCatalogue	: Map<string, Question>

	private _questions$			: BehaviorSubject<CategorizedQuestionWithSchedule[]>
								= new BehaviorSubject<CategorizedQuestionWithSchedule[]>([])

	public questions$			: Observable<CategorizedQuestionWithSchedule[]>
								= this._questions$.asObservable()

	private _baseSchedule$		: BehaviorSubject<Schedule>
								= new BehaviorSubject<Schedule>(new Schedule([[0,1,2,3,4,5,6], ['12:00']]))

	public baseSchedule$		: Observable<Schedule>
								= this._baseSchedule$.asObservable()

	public ready				: Promise<void>

	private _dirty$				: BehaviorSubject<boolean>
								= new BehaviorSubject<boolean>(false)

	public dirty$				: Observable<boolean>
								= this._dirty$.asObservable()

	private destroy$			: Subject<void> = new Subject()

	public constructor(
		@Inject(MONITORING_SETUP_CONFIG) @Optional()
		private readonly monitoringSetupConfig : MonitoringSetupConfig,
		private readonly questionnaireService: QuestionnaireService,
		private readonly defaultQuestionService: CuratedDefaultQuestionStoreService,
		private readonly extendedQuestionService: CuratedExtendedQuestionStoreService,
	){
		this.ready = this.reset()
	}

	public trackFormState(): void {
		this.unsubscribe()
		this.destroy$ = new Subject()

		// monitoring setup is flagged dirty whenever
		// _questions$ or _baseSchedule$ changes
		merge(
			this._questions$,
			this._baseSchedule$
		)
		.pipe(
			skip(2), // Both of the above BehaviorSubjects will emit on subscribe, we do not want to count these emission.
			takeUntil(this.destroy$),
			map(() => true),
		)
		.subscribe( dirty  => this._dirty$.next(dirty))
		// The line above used to look like this:
		// `.subscribe(this._dirty$)`
		// This is way more neat, but will trigger .complete
		// on `this._dirty$` when `this.destroy$` emits,
		// which cause `this._dirty$` to lose all observers,
		// what we do not want.
		// `this.destroy$` emits when this.unsubscribe()
		// is called, which happens right at the start of this method.

	}

	// #region getters & setters

	public get catalogue(): Question[] {
		return (this.questionCatalogue ? [...this.questionCatalogue.values()] : []) as Question[]
	}

	public get categories(): string[] {
		return this.monitoringSetupConfig?.questionCategories ?? []
	}

	public get hiddenOnEditCategories(): string[] {
		return this.monitoringSetupConfig?.hiddenOnEditCategories ?? []
	}

	public get categoryForTemplateQuestions(): string|null {
		return this.monitoringSetupConfig?.addTemplateQuestionsTo ?? null
	}

	public get baseScheduleDays(): number[] {
		return [...this._baseSchedule$.getValue().daysOfWeek]
	}

	public set baseScheduleDays(days: number[]) {
		this._baseSchedule$.next(new Schedule([[...days], []]))
	}

	public get dirty(): boolean {
		return this._dirty$.getValue()
	}

	public getQuestionsByCategory$(category: string | null = null): Observable<CategorizedQuestionWithSchedule[]> {
		return this._questions$.pipe(
			map((questions: CategorizedQuestionWithSchedule[]) => questions.filter((item: CategorizedQuestionWithSchedule) => item.category === category))
		)
	}

	// #endregion

	public allowDragAndDrop(category: string): boolean {
		if (category === 'medication') return false

		return true
	}

	// #region add/remove/update questions

	public addQuestion(question: CategorizedQuestionWithSchedule): void
	public addQuestion(question: Question | string, category?: string|null, schedule?: Schedule|null): void
	public addQuestion(question: CategorizedQuestionWithSchedule | Question | string, category: string|null = null, schedule: Schedule|null = null): void {
		this._questions$
			.pipe(
				take(1),
				mergeMap((currentQuestions: CategorizedQuestionWithSchedule[]) => {
					if(isCategorizedQuestionWithSchedule(question))
						return of(currentQuestions.concat([question]))
					else if(question instanceof Question)
						return of(currentQuestions.concat([{ question, category, schedule }]))
					else if(typeof question === 'string')
						return of(question)
							.pipe(
								switchMap((questionId: string) =>
									this.questionnaireService.get(questionId)
								),
								map((newQuestion: Question) =>
									({ question: newQuestion, category, schedule } as CategorizedQuestionWithSchedule)
								),
								map((newQuestion: CategorizedQuestionWithSchedule) =>
									currentQuestions.concat([newQuestion])
								)
							)
				}),
				// this is here to safeguard against faulty input
				filter((updatedQuestions: CategorizedQuestionWithSchedule[]) => !!updatedQuestions)
			)
			.subscribe(
				(updatedQuestions: CategorizedQuestionWithSchedule[]) =>
					this._questions$.next(updatedQuestions)
			)
	}

	public addMultipleQuestions(questions: CategorizedQuestionWithSchedule[]): void
	public addMultipleQuestions(questions: Question[], category?: string|null, schedule?: Schedule|null): void
	public addMultipleQuestions(questions: string[], category?: string|null, schedule?: Schedule|null): void
	public addMultipleQuestions(questions: CategorizedQuestionWithSchedule[] | Question[] | string[], category: string|null = null, schedule: Schedule|null = null): void {
		// there is an empty template that adds no questions when
		// selected, but should still be treated as editing the
		// monitoring, so the dirty flag must be set, even though
		// nothing actually happened
		if(!questions.length) {
			this._dirty$.next(true)
			return
		}
		this._questions$
			.pipe(
				take(1),
				switchMap((currentQuestions:CategorizedQuestionWithSchedule[]) => {
					if(isCategorizedQuestionWithSchedule(questions[0]))
						return of(currentQuestions
									.concat(questions as CategorizedQuestionWithSchedule[])
								)
					else if(questions[0] instanceof Question)
						return of(currentQuestions
									.concat(
										(questions as Question[])
											.map((question: Question) => ({ question, category, schedule }))
									)
								)
					else if(typeof questions[0] === 'string')
						return from((questions as string[]))
							.pipe(
								concatMap((question: string) =>
									this.questionnaireService.get(question)
								),
								map((question: Question) =>
									({ question, category, schedule } as CategorizedQuestionWithSchedule)
								),
								toArray(),
								map((newQuestions: CategorizedQuestionWithSchedule[]) =>
									currentQuestions.concat(newQuestions)
								)
							)
				}),
				// this is here to saveguard against faulty input
				filter((updatedQuestions: CategorizedQuestionWithSchedule[]) => !!updatedQuestions)
			)
			.subscribe(
				(updatedQuestions: CategorizedQuestionWithSchedule[]) =>
					this._questions$.next(updatedQuestions)
			)
	}

	public moveQuestion(questionId: string, toCategory: string): void {
		const questions: CategorizedQuestionWithSchedule[] = this._questions$.getValue()
		
		const questionToMove: CategorizedQuestionWithSchedule = questions.find(question => question.question.id === questionId)

		questionToMove.category = toCategory

		const newQuestions: CategorizedQuestionWithSchedule[] = [...questions.filter(question => question.question.id !== questionId), questionToMove]

		this._questions$.next(newQuestions)
	}


	public removeQuestion(question: CategorizedQuestionWithSchedule): void
	public removeQuestion(question: Question | string, category?: string|null): void
	public removeQuestion(question: CategorizedQuestionWithSchedule | Question | string, category: string|null = null): void {
		this._questions$
			.pipe(
				take(1),
				map((currentQuestions: CategorizedQuestionWithSchedule[]) => {
					if(isCategorizedQuestionWithSchedule(question))
						return currentQuestions
							.filter(
								(item: CategorizedQuestionWithSchedule) =>
									!(item.question.id === question.question.id && item.category === question.category)
							)
					else if(typeof question === 'string')
						return currentQuestions
							.filter(
								(item: CategorizedQuestionWithSchedule) =>
									!(item.question.id === question && item.category === category)
								)
					else if(question instanceof Question)
						return currentQuestions
							.filter(
								(item: CategorizedQuestionWithSchedule) =>
									!(item.question.id === question.id && item.category === category)
								)
				}),
				// this is here to saveguard against faulty input
				filter((updatedQuestions: CategorizedQuestionWithSchedule[]) => !!updatedQuestions)
			)
			.subscribe(
				(updatedQuestions: CategorizedQuestionWithSchedule[]) =>
					this._questions$.next(updatedQuestions)
			)
	}

	public removeMultipleQuestions(questions: CategorizedQuestionWithSchedule[]): void
	public removeMultipleQuestions(questions: Question[], category?: string|null): void
	public removeMultipleQuestions(questions: string[], category?: string|null): void
	public removeMultipleQuestions(questions: CategorizedQuestionWithSchedule[]| Question[] | string[], category: string|null = null): void {
		if(!questions.length) return
		this._questions$
			.pipe(
				take(1),
				map((currentQuestions: CategorizedQuestionWithSchedule[]) => {
					if(isCategorizedQuestionWithSchedule(questions[0]))
						return currentQuestions
							.filter(
								(categorizedQuestion: CategorizedQuestionWithSchedule) =>
									!(questions as CategorizedQuestionWithSchedule[]).includes(categorizedQuestion)
							)
					else if(typeof questions[0] === 'string')
						return currentQuestions
							.filter(
								(categorizedQuestion: CategorizedQuestionWithSchedule) =>
									!(questions as string[])
										.find(
											(questionId: string) =>
												questionId === categorizedQuestion.question.id && category === categorizedQuestion.category
										)
							)
					else if(questions[0] instanceof Question)
						return currentQuestions
							.filter(
								(categorizedQuestion: CategorizedQuestionWithSchedule) =>
									!(questions as Question[])
										.find(
											(question: Question) =>
												question.id === categorizedQuestion.question.id && category === categorizedQuestion.category
										)
							)
				}),
				// this is here to safeguard against faulty input
				filter((updatedQuestions: CategorizedQuestionWithSchedule[]) => !!updatedQuestions)
			)
			.subscribe(
				(updatedQuestions: CategorizedQuestionWithSchedule[]) =>
					this._questions$.next(updatedQuestions)
			)
	}

	/**
	 * Updating a question will not actually change that question but replace it with a new question
	 * with the updated config and a new id. Since we don't want the order of the questions to
	 * change we have to find the old question and replace it by the new one, instead of removing
	 * it and adding the new one to the list.
	 * @param {string} currentId The id of the question to replace
	 * @param {CategorizedQuestionWithScheduleUpdate | QuestionUpdate} question The new question to
	 * insert in place of the previous one
	 * @param {string | null} category Optional. The category the question should be linked to.
	 * Defaults to null
	 * @param {Schedule | null} schedule Optional. Custom schedule for this question. Defaults to null.
	 */
	public updateQuestion(currentId: string, question: CategorizedQuestionWithScheduleUpdate): void
	public updateQuestion(currentId: string, question: QuestionUpdate, category?: string|null, schedule?: Schedule|null): void
	public updateQuestion(currentId: string, question: CategorizedQuestionWithScheduleUpdate | QuestionUpdate, category: string|null = null, schedule: Schedule|null = null): void {
		this._questions$.pipe(
			take(1),
			map((questions: CategorizedQuestionWithSchedule[]) => {
				let questionToUpdate : null | CategorizedQuestionWithSchedule = null

				if(has(question, 'id'))
					questionToUpdate	= 	questions
												.find(
													(item: CategorizedQuestionWithSchedule) =>
														item.question.id === currentId && item.category === category
												)
				else
					questionToUpdate	= 	questions
												.find(
													(item: CategorizedQuestionWithSchedule) =>
													item.question.id === currentId  && item.category === question.category
												)
				if(questionToUpdate) {
					questionToUpdate.question = new Question({
													...questionToUpdate.question.config,
													...(has(question, 'id') ? question : question.question)
												})
					questionToUpdate.schedule = (has(question, 'id') ? schedule : question.schedule) ?? null
				}

				return questions
			})
		)
		.subscribe(
			(questions: CategorizedQuestionWithSchedule[]) =>
				this._questions$.next(questions)
		)
	}

	// #endregion


	// #region submitting

	private sortByCategory(questions : CategorizedQuestionWithSchedule[]) : CategorizedQuestionWithSchedule[] {
		const categoryQuestionMap: Map<string, CategorizedQuestionWithSchedule[]> = new Map()
		this.monitoringSetupConfig.questionCategories.forEach((category: string) => {
			categoryQuestionMap.set(category, [])
		})
		questions.forEach((question: CategorizedQuestionWithSchedule) => {
			const categoryQuestions: CategorizedQuestionWithSchedule[] = categoryQuestionMap.get(question.category)
			categoryQuestions.push(question)
		})
		return Array.from(categoryQuestionMap.values()).flat()
	}

	public getConfig(meta: SymptomCheckMetaConfig): SymptomCheckConfig {
		const questions 	: CategorizedQuestionWithSchedule[]
							= this._questions$.getValue()

		const symptomCheck 	: SymptomCheck
							= new SymptomCheck({
								meta: {
									creationDate: toISO8601CalendarDate(new Date()),
									defaultSchedule: this._baseSchedule$.getValue().config,
									...meta
								},
								questions: []
							})

		questions.forEach(
			(categorizedQuestion: CategorizedQuestionWithSchedule) =>
				symptomCheck.addQuestionId(
					categorizedQuestion.question.id,
					categorizedQuestion.schedule?.config,
					categorizedQuestion.category
				)
			)

		return symptomCheck.config
	}

	public getQuestions(): QuestionConfig[] {
		const questions 	: CategorizedQuestionWithSchedule[]
							= this._questions$.getValue()

		const sortedQuestions	: CategorizedQuestionWithSchedule[]
								= this.sortByCategory(questions)

		const questionConfigs 	: QuestionConfig[]
								= sortedQuestions.map(
									(categorizedQuestion: CategorizedQuestionWithSchedule) => categorizedQuestion.question.config
								)
		return questionConfigs
	}

	// #endregion


	// #region setup

	private unsubscribe(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}

	public async reset() : Promise<void> {
		this.unsubscribe()
		this.initQuestionCatalogue()
		this._questions$.next([])

		if(this.monitoringSetupConfig !== null) {
			// add default questions
			const defaultQuestions 	: { [category: string] : Question[] }
									= await this.getDefaultQuestions()

			for (const [category, questions] of Object.entries(defaultQuestions))
				this.addMultipleQuestions(questions, category)

			// set default base schedule
			this._baseSchedule$.next(new Schedule(this.monitoringSetupConfig.defaultScheduleConfig))
		}
		this._dirty$.next(false)
	}

	private initQuestionCatalogue(): void {
		void Promise
			.all([
				this.defaultQuestionService.ready,
				this.extendedQuestionService.ready
			]).then(() => {
				const questions : [string, Question][] = [...this.defaultQuestionService.items, ...this.extendedQuestionService.items]
						.filter(
							(question) => !this.monitoringSetupConfig?.hiddenInCatalogue?.includes(question.id)
						)
						.map(
							(question) => [question.id, question]
						)

				this.questionCatalogue = new Map(questions)
			})
	}

	private async getDefaultQuestions(): Promise<{ [category: string] : Question[] }> {
		const defaultQuestionsSetup : { [category: string]: Promise<Question[]> }
									= {}

		this.monitoringSetupConfig.defaultQuestionsByCategory.forEach(
			(questions: string[], category: string) => {
				defaultQuestionsSetup[category] = this.questionnaireService.get(questions)
			}
		)
		return firstValueFrom(forkJoin(defaultQuestionsSetup))
	}

	// #endregion

}
