import	{
			Component,
			OnInit,
			OnDestroy,
			Input,
			Output,
			EventEmitter,
			ViewChild,
			ChangeDetectorRef
		}									from '@angular/core'

import	{
			Question,
			QuestionConfig,
			QuestionRelation,
			QuestionRelationDto,
			SymptomCheck,
			SymptomCheckConfig,
			indexArray,
		}									from '@rcc/core'
import	{
			HandlerAction,
			Action,
			RccModalController,
			RccTranslationService,
		}									from '@rcc/common'
import	{
			RccQuestionnaireEditorService
		}									from '../questionnaire-editor.service'
import	{
			from,
			BehaviorSubject,
			Observable,
			Subject,
			combineLatest,
			map,
			switchMap,
			distinctUntilChanged,
			takeUntil,
			filter,
		}									from 'rxjs'
import	{
			SymptomCheckMetaStoreService
		}									from '../../symptom-checks'
import	{
			FormControl,
			FormGroup
		}									from '@angular/forms'
import	{
			EditScheduleModalComponent,
			FormData as ScheduleFormData
		}									from '../modal/edit-schedule/edit-schedule-modal.component'
import	{
			EditReminderModalComponent,
			FormData as ReminderFormData
		}									from '../modal/edit-reminder/edit-reminder-modal.component'
import	{	RccFullPageComponent		}	from '@rcc/themes/bengal'
import	{	DAILY_NOTES_ID				}	from '@rcc/features/curated/curated-questions/curated-default-questions'
import { QuestionnaireService } from '../..'

export interface ConfirmPayload {
	config: SymptomCheckConfig
	questions: Array<QuestionConfig>
}

export interface ExistingSymptomCheck {
	symptomCheck	: SymptomCheck
	questions		: Question[]
}

@Component({
	selector	:	'rcc-questionnaire-editor-page',
	templateUrl	:	'./questionnaire-editor.component.html',
	styleUrls	:	['./questionnaire-editor.component.scss'],
})
export class RccQuestionnaireEditorComponent implements OnInit, OnDestroy {

	@Input()
	public templateId: string

	@Input()
	public existingSymptomCheck: ExistingSymptomCheck

	@Output()
	public confirm: EventEmitter<ConfirmPayload> = new EventEmitter<ConfirmPayload>()

	@Input()
	public set confirmLabel(label: string){ this.primaryAction.label = label }

	@Input()
	public updateNameFromTemplate: boolean = false

	@Input()
	public backButtonAction: Action

	@Input()
	public set initialName(value: string) {
		if (value == null) return
		this.nameFormGroup.setValue({
			name: value
		})
	}
	@ViewChild(RccFullPageComponent, { static: true }) private rccFullPageComponent: RccFullPageComponent

	public get templateNameIsBold(): boolean {
		return this.initMode === false
	}

	private _mode$				: BehaviorSubject<'edit'|'preview'|'init'>		= new BehaviorSubject('init')

	private reminderTime		: BehaviorSubject<string> 						= new BehaviorSubject('12:00')
	private reminderEnabled		: BehaviorSubject<boolean> 						= new BehaviorSubject(true)
	private templateControl		: FormControl<string> 							= new FormControl<string>('')
	protected templateFormGroup	: FormGroup<{ template: FormControl<string> }>	= new FormGroup({ template: this.templateControl })
	protected nameFormGroup		: FormGroup<{ name: FormControl<string> }> 		= new FormGroup({ name: new FormControl('') })
	protected editName			: boolean 										= false
	protected mode$				: Observable<'edit'|'preview'|'init'>			= this._mode$.asObservable()
	protected templates$		: Observable<{value: string, label: string }[]> = from(
																					this.symptomCheckMetaStoreService.ready.then(() => this.symptomCheckMetaStoreService.items)
																				).pipe(
																					map((templates) => templates.map((template) => ({
																							value: template.id,
																							label: template.meta.label,
																						}))
																					),
																				)
	private unsubscribe$ 		: Subject<void>									= new Subject<void>()

	private onBeforeUnload : (e: BeforeUnloadEvent) => void = (e) =>{
		if(this.rccQuestionnaireEditorService.dirty) {
			e.preventDefault()
			e.returnValue = ''
		}
	}

	public constructor (
		private readonly rccQuestionnaireEditorService	: RccQuestionnaireEditorService,
		private readonly symptomCheckMetaStoreService	: SymptomCheckMetaStoreService,
		private readonly rccModalController				: RccModalController,
		private readonly translationService				: RccTranslationService,
		private readonly questionnaireService			: QuestionnaireService,
		private readonly changeDetectorRef				: ChangeDetectorRef
	) {}



	public async ngOnInit(): Promise<void> {
		await this.rccQuestionnaireEditorService.ready

		this.subscribeToModeChanges()
		this.subscribeForStatusChanges()
		this.subscribeToTemplateChanges()
		if (this.templateId)
			this.templateControl.setValue(this.templateId)

		if (this.existingSymptomCheck)
			await this.fillInSymptomCheck(this.existingSymptomCheck)

		// prompt user to confirm if they want to reload / leave
		// the page when service is in dirty state. The message
		// displayed depends on the browser.
		addEventListener(
			'beforeunload',
			this.onBeforeUnload
		)

		this.rccQuestionnaireEditorService.trackFormState()
	}

	public ngOnDestroy(): void {
		this.unsubscribe$.next()
		this.unsubscribe$.complete()
		void this.rccQuestionnaireEditorService.reset()

		removeEventListener(
			'beforeunload',
			this.onBeforeUnload
		)
	}

	protected visibleQuestionCategories : string[] = []

	protected updateVisbleQuestionCategories():void {

		const hiddenCategories			: 	string[]
										= 	this.rccQuestionnaireEditorService.hiddenOnEditCategories

		this.visibleQuestionCategories 	= 	this.editMode || this.initMode
											?	this.rccQuestionnaireEditorService.categories
												.filter( category => !hiddenCategories.includes(category))
											// preview mode:
											:	this.rccQuestionnaireEditorService.categories

		// The visible categories determine which questions
		// – and with it which query widget components are shown
		// those might raise a ExpressionChangedAfterItHasBeenCheckedError;
		// So we tell angular to look for changes right away:
		this.changeDetectorRef.detectChanges()
	}

	protected get initMode(): boolean {
		return this._mode$.getValue() === 'init'
	}

	protected get editMode(): boolean {
		return this._mode$.getValue() === 'edit'
	}

	// #endregion

	// #region questionnaire name handling

	protected focusTemplateSelection$: Subject<void> = new Subject()

	protected clickHeading(): void {
		this.focusTemplateSelection$.next()
	}

	protected editQuestionnaireName(): void {
		this.editName = true
	}

	protected cancelEditQuestionnaireName(): void {
		this.editName = false
	}

	// #endregion


	// #region actions

	protected get backAction(): Action {
		if (this.backButtonAction) return this.backButtonAction

		return {
			path: '/',
			label: 'QUESTIONNAIRE_EDITOR.BUTTON_BACK.LABEL',
			icon: 'back',
			category: 'create',
		}
	}

	protected editAction: HandlerAction	=	{
		handler: () => this._mode$.next('edit'),
		label: 'QUESTIONNAIRE_EDITOR.BUTTON_EDIT.LABEL',
		icon: 'edit',
		category: 'create',
	}

	protected primaryActionHandler(): void {

		const config: SymptomCheckConfig = this.rccQuestionnaireEditorService.getConfig({
			label: this.nameFormGroup.get('name').value,
			reminder: this.reminderTime.value
		})

		const questions: QuestionConfig[] = this.rccQuestionnaireEditorService.getQuestions()

		if (this.existingSymptomCheck)
			config.id = this.existingSymptomCheck.symptomCheck.id

		const dailyNotesQuestionIndex: number = config.questions.findIndex(q => {
			if (typeof q === 'string')
				return q === DAILY_NOTES_ID
			return q.id === DAILY_NOTES_ID
		})

		const questionsSorted	:	(string | QuestionRelationDto)[]
								=	[
										...config.questions.filter((_, index) => index !== dailyNotesQuestionIndex),
										config.questions[dailyNotesQuestionIndex]
									]

		config.questions = questionsSorted


		this.confirm.emit({
			config,
			questions,
		})
	}

	protected primaryAction	: 	HandlerAction
							=	{
									category: 	'share',
									handler: 	() => this.primaryActionHandler(),
									icon: 		undefined,
									label: 		'' // will be updated with confirmationLabel setter (see above)
								}


	protected previewAction	:	HandlerAction
							=	{
									handler: 	() => this._mode$.next('preview'),
									label: 		'QUESTIONNAIRE_EDITOR.BUTTON_PREVIEW.LABEL',
									icon: 		'preview',
									disabled: 	() => this.initMode,
									category: 	'share' // TODO: shouldn't be 'share', but this is according to the figma design
								}

	protected quickActions$: Observable<Action[]> = combineLatest(
		[this.translationService.activeLanguageChange$, this.rccQuestionnaireEditorService.baseSchedule$, this.reminderTime, this.reminderEnabled]
	).pipe(
		map(([, schedule, reminderTime, reminderEnabled]) => {

			const reminderTranslationKey: string = `QUESTIONNAIRE_EDITOR.REMINDER.${reminderEnabled ? 'VALUE' : 'NONE'}`

			const translation: string =	reminderTime
										?	this.translateReminderTime(reminderTime)
										:	undefined

			return [
				{
					icon: 'edit',
					label: schedule as unknown, // Will be replaced by the translation key
					handler: async () => {
						const result: ScheduleFormData = await this.rccModalController.present(
							EditScheduleModalComponent,
							{
								schedule: this.rccQuestionnaireEditorService.baseScheduleDays,
								title:	'QUESTIONNAIRE_EDITOR.SCHEDULE.LABEL.SYMPTOM_CHECK_DEFAULT'
							},
							{ mini: true },
						)

						if (result == null) return

						this.rccQuestionnaireEditorService.baseScheduleDays = result.schedule
					},
					data: schedule,
				} as HandlerAction,
				{
					icon: 'edit',
					label: `${this.translationService.translate(reminderTranslationKey, { time: translation })}`,
					handler: async () => {
						const result: ReminderFormData = await this.rccModalController.present(
							EditReminderModalComponent,
							{ defaultTime: reminderTime, enabled: reminderEnabled },
							{ mini: true },
						)

						if (result == null) return

						this.reminderTime.next(result.time)
						this.reminderEnabled.next(result.enabled)
					},
					data: `${this.translationService.translate(reminderTranslationKey, { time: translation })}`
				} as HandlerAction
			]
		})
	)

	private translateReminderTime(reminderTime: string): string {
		const [hours, minutes]: number[] = reminderTime.split(':').map((it: string) => parseInt(it, 10))
		const date: Date = new Date()
		date.setHours(hours)
		date.setMinutes(minutes)
		return this.translationService.translate(date, { hour: '2-digit', minute: '2-digit' })
	}

	// #endregion


	// #region subscriptions

	private subscribeToTemplateChanges(): void {
		this.templateControl.valueChanges.pipe(
			switchMap((id: string) => this.symptomCheckMetaStoreService.get(id)),
			map((symptomCheck: SymptomCheck) => ({
				questionIds: symptomCheck?.questionIds ?? [],
				meta: symptomCheck?.meta,
				relations: symptomCheck.questionRelations
			})),
			takeUntil(this.unsubscribe$)
		).subscribe(({ questionIds, meta, relations }) => {

			const fallbackCategory: string = this.rccQuestionnaireEditorService.categoryForTemplateQuestions
			const categoryFromConfigMap: Map<string, QuestionRelation> = indexArray(relations, item => item.questionId)

			for (const questionId of questionIds)
				void this.questionnaireService.get(questionId).then(question => {
						this.rccQuestionnaireEditorService.addQuestion(
							question,
							categoryFromConfigMap.get(questionId)?.category ?? fallbackCategory,
						)
					})

			this._mode$.next('edit')

			if (this.updateNameFromTemplate)
				this.nameFormGroup.setValue({ name: meta.label })


			if (meta.reminder != null)
				this.reminderTime.next(meta.reminder)

			if (meta.defaultSchedule.daysOfWeek.length > 0)
				this.rccQuestionnaireEditorService.baseScheduleDays = meta.defaultSchedule.daysOfWeek
		})
	}

	private async fillInSymptomCheck(existingSymptomCheck: ExistingSymptomCheck): Promise<void> {
		const questionIndex: Map<string, Question> = indexArray(existingSymptomCheck.questions, question => question.id)

		await this.rccQuestionnaireEditorService.reset()
		for (const questionRelation of existingSymptomCheck.symptomCheck.questionRelations) {
			if (questionRelation.questionId === DAILY_NOTES_ID)
				continue

			const question: Question = questionIndex.get(questionRelation.questionId)

			this.rccQuestionnaireEditorService.addQuestion({
				category: questionRelation.category ?? 'symptoms',
				question,
				schedule: questionRelation.schedule ?? null
			})
		}

		if (this.updateNameFromTemplate)
			this.nameFormGroup.setValue({ name: existingSymptomCheck.symptomCheck.config.meta.label })

		this._mode$.next('edit')

	}

	/**
	 * This toggles modes when RccMonitoringSetupService.dirty$ emits a
	 * value that differs from the one before. If it was false and becomes
	 * true that means that user added questions (either directly or by
	 * template selection) and the view should enter edit mode. If it was
	 * true and becomes false the service has been reset and the view
	 * returns to init mode again.
	 */
	private subscribeForStatusChanges(): void {
		this.rccQuestionnaireEditorService.dirty$
			.pipe(
				distinctUntilChanged(),
				map((dirty: boolean) => dirty ? 'edit' : 'init'),
				filter( mode => this._mode$.value !== mode ),
				takeUntil(this.unsubscribe$)
			)
			.subscribe(this._mode$)
	}

	private subscribeToModeChanges(): void {

		const modeChange$		:	Observable<string>
								=	this.mode$
									.pipe(
										distinctUntilChanged(),
										takeUntil(this.unsubscribe$),
									)

		const toPreviewSwitche$	:	Observable<string>
								=	this.mode$
									.pipe(
										filter((mode: string) => mode === 'preview')
									)

		toPreviewSwitche$
		.subscribe( () => this.rccFullPageComponent?.elementRef.nativeElement.scrollTo({ top: 0, behavior: 'smooth' }) )

		modeChange$
		.subscribe( () => {
			this.updateVisbleQuestionCategories()
		})


	}

	// #endregion
}
