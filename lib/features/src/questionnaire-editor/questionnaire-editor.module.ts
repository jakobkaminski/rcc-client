import { NgModule } from '@angular/core'
import { RccQuestionnaireEditorComponent } from './questionnaire-editor/questionnaire-editor.component'
import { RccThemeModule } from '@rcc/themes/active'
import { CurrentDateMessageComponent, SharedModule, provideTranslationMap } from '@rcc/common'
import { RccQuestionLaneComponent } from './components/question-lane/question-lane.component'
import { RccQuestionnaireEditorService } from './questionnaire-editor.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		RccThemeModule,
		CurrentDateMessageComponent,
		SharedModule,
		RccQuestionLaneComponent,
	],
	providers: [
		RccQuestionnaireEditorService,
		provideTranslationMap('QUESTIONNAIRE_EDITOR', { en, de })
	],
	declarations: [
		RccQuestionnaireEditorComponent,
	],
	exports: [
		RccQuestionnaireEditorComponent,
	]
})
export class QuestionnaireEditorModule {}
