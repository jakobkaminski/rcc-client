import	{
			Component,
			Input,
			OnInit
		}								from	'@angular/core'
import	{
			AbstractControl,
			FormControl,
			FormGroup,
		}								from	'@angular/forms'
import	{
			SharedModule,
		}								from	'@rcc/common'

import	{	FormGroupOf				}	from	'@rcc/core/types'

import	{	ModalLayoutSmallComponent	}	from	'../layout/modal-layout-small/modal-layout-small.component'

export interface FormData {
	enabled: boolean,
	time: string
}

type FormGroupType = FormGroupOf<FormData>

@Component({
	templateUrl	:	'edit-reminder-modal.component.html',
	styleUrls	:	['edit-reminder-modal.component.scss'],
	standalone	:	true,
	imports		:	[
						SharedModule,
						ModalLayoutSmallComponent,
					],
})

export class EditReminderModalComponent implements OnInit{

	// This used to be ngAfterViewInit, but that
	// caused an ExpressionChangedAfterItHasBeenCheckedError,
	// when this.formGroup.get('enabled') emitted false right away.
	// ngOnInit seems to be okay, since there is nothing we need
	// to wait for.
	public ngOnInit(): void {
		this.setDisabledState(!this.enabled)
		this.formGroup.get('enabled').valueChanges.subscribe((value: boolean) => {
			this.setDisabledState(!value)
		})
	}

	@Input()
	public set defaultTime(value: string) {
		if (value == null) return
		this.formGroup.get('time').setValue(value)
	}

	private _enabled: boolean
	public get enabled(): boolean {
		return this._enabled
	}
	@Input()
	public set enabled(value: boolean) {
		this.formGroup.get('enabled').setValue(value)
		this._enabled = value
	}

	protected formGroup: FormGroup<FormGroupType> = new FormGroup<FormGroupType>({
		enabled: new FormControl(true),
		time: new FormControl({ value: '12:00', disabled: false })
	})

	private setDisabledState(isDisabled: boolean): void {
		const control: AbstractControl = this.formGroup.get('time')
		if (isDisabled)
			control.disable()
		else
			control.enable()
	}
}
