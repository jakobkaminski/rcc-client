import { Component, Input } from '@angular/core'
import { AbstractControl, ControlContainer, FormGroup, FormGroupDirective } from '@angular/forms'
import { RccModalController, SharedModule } from '@rcc/common'
import { RccModalHeaderComponent } from '@rcc/common/modal-header/modal-header.component'
import { RccColorCategoryDirective } from '@rcc/common/ui-components'
import { UserCanceledError } from '@rcc/core'

@Component({
	templateUrl	: 'modal-layout-large.component.html',
	styleUrls	: ['./modal-layout-large.component.scss'],
	standalone	: true,
	imports		: [SharedModule, RccColorCategoryDirective, RccModalHeaderComponent],
	selector	: 'rcc-monitoring-setup-modal-layout-large',
	viewProviders: [
		{
			provide: ControlContainer,
			useExisting: FormGroupDirective,
		}
	]
})
export class ModalLayoutLargeComponent<T extends Record<string, AbstractControl>> {
	public constructor(private readonly rccModalController: RccModalController) {}

	@Input()
	public mainHeading: string

	@Input()
	public subHeading: string

	@Input()
	public formGroup: FormGroup<T>

	@Input()
	public transformSubmissionData: (value: FormGroup<T>) => unknown

	protected onSubmit(): void {
		const formData: unknown = this.formGroup.value
		const data: unknown = this.transformSubmissionData == null ? formData : this.transformSubmissionData(this.formGroup)
		this.rccModalController.dismiss(data)
	}

	protected closeModal(): void {
		this.rccModalController.dismiss(new UserCanceledError('modal closed by user'))
	}
}
