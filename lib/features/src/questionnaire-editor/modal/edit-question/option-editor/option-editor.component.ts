import { Component, forwardRef } from '@angular/core'
import { ControlValueAccessor, FormArray, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms'
import { RccTranslationService, SharedModule } from '@rcc/common'
import { QuestionOptionConfig } from '@rcc/core'
import { RccThemeModule } from '@rcc/themes/active'

@Component({
	templateUrl	: './option-editor.component.html',
	styleUrls	: ['./option-editor.component.scss'],
	selector	: 'rcc-monitoring-setup-option-editor',
	standalone	: true,
	providers	: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => OptionEditorComponent),
			multi: true
		}
	],
	imports: [RccThemeModule, SharedModule]
})
export class OptionEditorComponent implements ControlValueAccessor {
	public constructor(private readonly translationService: RccTranslationService) {
		this.formGroup.valueChanges.subscribe((formValue: { values: string[] }) => {
			const newValue: QuestionOptionConfig[] = this.value.map((itemValue, index) => ({
				...itemValue,
				translations: {
					[this.translationService.activeLanguage]: formValue.values[index]
				}
			}))
			this.value = newValue
			this.onChange(newValue)
		})
	}

	protected value: QuestionOptionConfig[]

	protected formGroup: FormGroup = new FormGroup({
		values: new FormArray([])
	})

	protected get valueControls(): FormControl[] {
		return (this.formGroup.get('values') as FormArray).controls as FormControl[]
	}

	public writeValue(value: QuestionOptionConfig[]): void {
		this.value = value
		this.formGroup.setControl(
			'values',
			new FormArray(value.map(v => new FormControl(this.translationService.translate(v)))),
		)
	}

	private onChange: (value: QuestionOptionConfig[]) => void = () => undefined
	public registerOnChange(fn: (value: QuestionOptionConfig[]) => void): void {
		this.onChange = fn
	}

	private onTouched: () => void = () => undefined
	public registerOnTouched(fn: () => void): void {
		this.onTouched = fn
	}

	protected onStartEdit(): void {
		this.onTouched()
	}
}
