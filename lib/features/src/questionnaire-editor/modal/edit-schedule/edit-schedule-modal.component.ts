import { Component, Input } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { SharedModule } from '@rcc/common'
import { FormGroupOf } from '@rcc/core/types'
import { ModalLayoutSmallComponent } from '../layout/modal-layout-small/modal-layout-small.component'

export interface FormData {
	schedule: number[]
}

type FormGroupType = FormGroupOf<FormData>

@Component({
	templateUrl	: './edit-schedule-modal.component.html',
	standalone	: true,
	imports		: [SharedModule, ModalLayoutSmallComponent],
})
export class EditScheduleModalComponent {

	@Input()
	public set schedule(value: number[]) {
		this.formGroup.patchValue({ schedule: value })
	}

	@Input()
	public title								: string	= 'SCHEDULE'

	protected formGroup: FormGroup<FormGroupType> = new FormGroup<FormGroupType>({
		schedule: new FormControl([], [Validators.required, Validators.minLength(1)])
	})
}
