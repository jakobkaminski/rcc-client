import	{
			Component,
			AfterViewInit,
			ElementRef,
			HostBinding,
		}											from '@angular/core'
import	{
			CategoryScale,
			Chart,
			LinearScale,
			LineController,
			BarController,
			LineElement,
			PointElement,
			BarElement,
			Filler,
			Tooltip,
		}											from 'chart.js'
import  {
			WidgetComponent, WidgetMatchFn,
		} 											from '@rcc/common'
import	{
			DataViewControl,
		}											from '../data-visualization'
import	{
			CombinedData
		}											from './basic-data-view-widgets.commons'
import	{
			BasicDataViewService
		}											from './basic-data-view.service'


// Chart.js setup
Chart.register(
	BarController,
	BarElement,
	CategoryScale,
	LinearScale,
	LineController,
	LineElement,
	PointElement,
	Filler,
	Tooltip
)



/**
 * Needs to ne cleaned up and restructured!
 * This component visualizes two numeric or boolean Datasets with two alined charts.
 */
@Component({
	template: 	`
					<div
						*ngFor 	= "let dataset of dataViewControl.datasets"
						class	= "ion-padding"
					>
						<canvas></canvas>
					</div>

				`
})
export class BaseChartWidgetComponent extends WidgetComponent<DataViewControl> implements AfterViewInit {

	// STATIC

	public static controlType = DataViewControl

	// Needs to be overwritten by an extending class:
	public static override widgetMatch: WidgetMatchFn<DataViewControl> = () => -1


	// INSTANCE:


	// The number of data points will be used to adjust styles in order to set
	// the width of the wrapper div. This way we can make the chart scroll rather
	// than squeeze all the data points onto one screen.
	@HostBinding('style.--number-of-data-points')
	public get getNumberOfDataPoints()	: number { return this.combinedData?.dateStrings.length }
	public numberOfDataPoints			: number

	public canvases 					: HTMLCanvasElement[] = []
	public combinedData					: CombinedData


	public constructor(
		private elementRef				: ElementRef<HTMLCanvasElement>,

		public dataViewControl			: DataViewControl,
		public basicDataViewService		: BasicDataViewService,
	){
		super(dataViewControl)

		this.combinedData = this.basicDataViewService.getCombinedDailyData(dataViewControl)

	}


	public ngAfterViewInit() : void {
		this.canvases	= Array.from(this.elementRef.nativeElement.querySelectorAll('canvas'))
		this.setupCharts()
	}


	public setupCharts(): void {

		// TODO: Colors should be managed by a separate central service.
		// TODO: Make chart config publicly accessible for extensions.
		// TODO: why is everything scrolling to the start, when datasets are changed?



		this.canvases.forEach( (_, index) => {
			if(!this.combinedData.data[index]) return null
		})

	}

}
