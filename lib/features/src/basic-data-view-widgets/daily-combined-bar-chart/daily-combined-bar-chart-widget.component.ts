import	{
			Component,
			AfterViewInit,
			ElementRef,
			HostBinding,
			Inject,
		}											from '@angular/core'
import	{	DOCUMENT							}	from '@angular/common'
import	{
			CategoryScale,
			Chart,
			LinearScale,
			LineController,
			BarController,
			LineElement,
			PointElement,
			BarElement,
			Filler,
			TooltipItem,
			ScaleOptionsByType,
			ChartTypeRegistry,
			ChartDataset,
		}											from 'chart.js'
import	{	_DeepPartialObject					}	from 'chart.js/types/utils'
import  {
			RccTranslationService,
			WidgetComponent,
		} 											from '@rcc/common'
import	{
			getDayOfWeek,
			Question,
			QuestionOptionConfig
		}											from '@rcc/core'
import	{	DataViewControl						}	from '../../data-visualization'
import	{	CombinedData						}	from '../basic-data-view-widgets.commons'
import	{	BasicDataViewService				}	from '../basic-data-view.service'
import	{
			ColorVariants,
			RccColorService,
		}											from '@rcc/themes/index'

// Chart.js setup
Chart.register(
	BarController,
	BarElement,
	CategoryScale,
	LinearScale,
	LineController,
	LineElement,
	PointElement,
	Filler,
)


@Component({
	templateUrl: 	'./daily-combined-bar-chart-widget.component.html',
	styleUrls:		['./daily-combined-bar-chart-widget.component.css']
})
export class DailyCombinedBarChartWidgetComponent extends WidgetComponent<DataViewControl> implements AfterViewInit {

	// STATIC

	public static controlType: typeof DataViewControl = DataViewControl


	public static widgetMatch( dataViewControl	: DataViewControl ): number {
		const answerTypes	: string[]	= dataViewControl.datasets.map( ds => ds?.question?.type )
		const numberLike	: boolean 	= answerTypes.every( answerType => ['integer', 'decimal', 'boolean'].includes(answerType))
		const someBoolean	: boolean	= answerTypes.every( answerType => answerType === 'boolean')

		if(!numberLike) 	return -1
		if(someBoolean)		return 0

		return 1
	}



	// INSTANCE:


	// The number of data points will be used to adjust styles in order to set
	// the width of the wrapper div. This way we can make the chart scroll rather
	// than squeeze all the data points onto one screen.
	@HostBinding('style.--number-of-data-points')
	public get getNumberOfDataPoints(): number { return this.combinedData?.dateStrings.length }
	public numberOfDataPoints			: number

	public canvas 						: HTMLCanvasElement
	public combinedData					: CombinedData
	public legend						: Record<string, unknown> & {primaryQuestion: Question, secondaryQuestion: Question}


	public constructor(
		@Inject(DOCUMENT)
		private document				: Document,
		private elementRef				: ElementRef<HTMLCanvasElement>,
		private rccTranslationService	: RccTranslationService,

		dataViewControl					: DataViewControl,
		private basicDataViewService	: BasicDataViewService,
		private rccColorService			: RccColorService,
	){
		super(dataViewControl)

		this.combinedData = this.basicDataViewService.getCombinedDailyData(dataViewControl)

		this.legend = {
			firstDate:			this.combinedData.dateStrings[0],
			lastDate:			this.combinedData.dateStrings[this.combinedData.dateStrings.length-1],
			primaryQuestion:	this.combinedData.questions[0],
			secondaryQuestion:	this.combinedData.questions[1],
		}

	}


	public ngAfterViewInit() : void {
		this.canvas	= this.elementRef.nativeElement.querySelector('canvas')
		this.setupCharts()
	}


	public setupCharts(): void {

		const labels			: string[][]	= this.combinedData.labels.map( (l, index) => this.combinedData.getMediumLabel(index))

		const primary: ColorVariants = this.rccColorService.getComputedColorVariants('primary', this.document.documentElement)
		const secondary: ColorVariants = this.rccColorService.getComputedColorVariants('secondary', this.document.documentElement)

		const primary_color		: string		=	`rgb(${primary.main})`
		const secondary_color	: string		=	`rgb(${secondary.main})`

		const afterChartRendered: () => void = () => {
			const legend: Element	= this.elementRef.nativeElement.querySelector('.legend')

			// effects only portrait orientation
			this.canvas.scrollIntoView({ inline: 'end' })

			// effects only landscape orientation
			legend.scrollIntoView({ inline: 'end' })
		}

		// TODO: Make chart config publicly accessible for extensions.

		// TODO: why is everything scrolling to the start, when datasets are changed?

		if(!this.combinedData.data[0]) return null


		const options0	: QuestionOptionConfig[]	= this.combinedData.questions[0]?.options ||[]
		const options1	: QuestionOptionConfig[]	= this.combinedData.questions[1]?.options ||[]


		const booleanDefaultLabels: Record<string, string> = 	{
											'1': 	this.rccTranslationService.translate('TRUE'),
											'-1': 	this.rccTranslationService.translate('FALSE')
										}


		type YAxis = _DeepPartialObject<ScaleOptionsByType<ChartTypeRegistry['bar']['scales']>>
		const yAxis: YAxis =	{
			beginAtZero:	true,
			suggestedMin: 	this.combinedData.min[0] !== null ? this.combinedData.min[0] 	: undefined,
			suggestedMax: 	this.combinedData.max[0] !== null ? this.combinedData.max[0] 	: undefined, // TODO
			ticks:{
				stepSize: 	this.combinedData.stepSize[0],
				callback:	(value:unknown) : string |  string[] => {

								const option0		: QuestionOptionConfig 	= 	options0.find( o => o.value === value)
								const option1		: QuestionOptionConfig 	= 	options1.find( o => o.value === value)

								const optionLabel0	: string				= 	option0 && this.rccTranslationService.translate(option0)
								const optionLabel1	: string				= 	option1 && this.rccTranslationService.translate(option1)

								const boolean0		: boolean				= 	this.combinedData.questions[0]?.type === 'boolean'
								const boolean1		: boolean				= 	this.combinedData.questions[1]?.type === 'boolean'

								const booleanLabel0	: string				=	optionLabel0 || boolean0 && booleanDefaultLabels[String(value)]
								const booleanLabel1	: string				=	optionLabel1 || boolean1 && booleanDefaultLabels[String(value)]

								const yLabels		: string[]				=
														[
															String(value),
															optionLabel0 || booleanLabel0,
															optionLabel1 || booleanLabel1
														]
														.filter(x => !!x)

								const uniqueLabels	: string[]				= Array.from( new Set(yLabels))

								return 	uniqueLabels.length > 0
										?	uniqueLabels
										:	'?'
							}
			},
		}

		// dataset config:
		const mainDataSets: ChartDataset<'bar'>[] = []


		if(this.combinedData.data[0])
			mainDataSets.push({
				data: 				this.combinedData.data[0] as number[],
				minBarLength:		10,
				backgroundColor: 	primary_color,
				borderColor:		primary_color,
			})


		if(this.combinedData.data[1])
			mainDataSets.push({
				data: 				this.combinedData.data[1] as number[],
				minBarLength:		10,
				backgroundColor: 	secondary_color,
				borderColor:		secondary_color,
				borderWidth:		1,
			})


		new Chart( this.canvas , {
			type:	'bar' as const,
			data:	{
						labels,
						datasets: [
							...mainDataSets,
							{
								type:				'bar',
								data:				this.combinedData.dateStrings.map(
														(dateStr) => [1,2,3,4,5].includes(getDayOfWeek(dateStr))
														?	0
														: 	1
													),
								backgroundColor:	'rgba(0,0,0,0.1)',
								categoryPercentage:	1,
								barPercentage:		1,
								xAxisID:			'xWeekend',
								yAxisID:			'yWeekend'
							},

						]
					},

			options: {
				scales: {
					yAxisLeft: 	{ ...yAxis, position:'left' },
					yAxisRight:	{ ...yAxis, position:'right' },
					yWeekend:	{ display: false },

					x:{
						ticks: {
							minRotation: 0,
							maxRotation: 0
						}
					},

					xSecondary:	{ display: false, offset: true },
					xWeekend:	{ display: false }
				},
				responsive: 			true,
				maintainAspectRatio:	false,
				animation: 				{
											duration: 0,
										},
				onResize: () => afterChartRendered(),
				plugins: {
						tooltip: {
							mode: 'index',
							filter: (item: TooltipItem<'bar'>):boolean => [0,1].includes(item.datasetIndex),

							callbacks:{

								title: (items: TooltipItem<'bar'>[]): string[] => {
									if(!items[0]) return ['error']

									const dataIndex		: number 	= items[0].dataIndex
									const title_parts	: string[] 	= this.combinedData.getComplexLabel(dataIndex)

									return title_parts
								},

								label: (item: TooltipItem<'bar'>): string => {

									const datasetIndex	: number					=	item.datasetIndex
									const question		: Question					= 	this.combinedData.questions[datasetIndex]
									const is_boolean	: boolean					=	question?.type === 'boolean'
									const options		: QuestionOptionConfig[]	=	question?.options || []

									const getFormattedValue: () => string | null = () : string | null  => {
										if(typeof item.raw !== 'number')	return '?'
										if(!is_boolean)					return `${item.formattedValue || item.raw}`

										if(item.raw ===  1) 			return this.rccTranslationService.translate('TRUE')
										if(item.raw === -1) 			return this.rccTranslationService.translate('FALSE')
									}

									const value	: string | null	=	getFormattedValue()

									if(!options) 			return value

									const option		: QuestionOptionConfig	=	options.find( op => op.value === item.raw)
									const annotation	: string				=	item.raw
															?	`(${this.rccTranslationService.translate(option)})`
															:	''

									return `${value} ${annotation}`.trim()

								}
							}
						}
				}
			}

		})

	}

}
