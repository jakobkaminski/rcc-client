import	{
			Component,
		}											from '@angular/core'


import	{
			CategoryScale,
			Chart,
			LinearScale,
			LineController,
			BarController,
			LineElement,
			PointElement,
			BarElement,
			Filler,
		}											from 'chart.js'

import	{
			DataViewControl
		}											from '../../data-visualization'

import	{
			BaseChartWidgetComponent
		}											from '../base-chart-widget.component'

// Chart.js setup
Chart.register(
	BarController,
	BarElement,
	CategoryScale,
	LinearScale,
	LineController,
	LineElement,
	PointElement,
	Filler
)


@Component({
	template: 	`
					<div
						*ngFor	= "let dataset of dataViewControl.datasets"
						class	= "ion-padding chart"
					>
						<canvas></canvas>
					</div>

				`,
	styleUrls:	['./daily-bar-chart-widget.component.css']
})
export class DailyBarWidgetComponent extends BaseChartWidgetComponent {

	public static widgetMatch(dataViewControl	: DataViewControl): number {
		const answer_types	= dataViewControl.datasets.map( ds => ds.question.type )
		const number_like 	= answer_types.every( answertype => ['integer', 'decimal', 'boolean'].includes(answertype))
		const some_boolean	= answer_types.every( answertype => answertype === 'boolean')

		if(!number_like) 	return -1
		if(some_boolean)	return 0

		return 1
	}



}
