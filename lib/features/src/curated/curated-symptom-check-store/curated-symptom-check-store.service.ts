import 	{ 	Injectable 							}	from '@angular/core'

import	{
			SymptomCheckStore,
			SymptomCheckConfig,
		}											from '@rcc/core'

import	{	RccTranslationService				}	from '@rcc/common'
import	{	CuratedDefaultQuestionStoreService	}	from '../curated-questions/curated-default-questions'
import	{
			firstValueFrom,
			skip,
		}											from 'rxjs'


// TODO: Switch Translation Keys QUESTIONS.CURATED to CURATED.QUESTIONS  + move curated questions to features/src/curated

const categories: string[] = 	[
	'rcc-category-empty',						// there is no question with this tag, thus the resulting symptom check will have no questions
	'rcc-category-depression',
	'rcc-category-bipolar-disorder',
	'rcc-category-psychosis',
	'rcc-category-schizoaffective-disorders'
]

@Injectable()
export class CuratedSymptomCheckStoreService extends SymptomCheckStore {

	public readonly name: string = 'CURATED.SYMPTOM_CHECKS.STORE_NAME'

	public constructor(
		curatedDefaultQuestionStoreService	: CuratedDefaultQuestionStoreService,
		rccTranslationService				: RccTranslationService
	){
		super({
			getAll: async () =>		{
				// needed, because async function -> if not awaited, items might not be present yet
				await curatedDefaultQuestionStoreService.ready

				// TODO: #271 This is a temporary fix to allow the translations of the template
				// names after the user's language settings has been determined. This should be
				// made reactive such that these translations update whenever the user's language
				// changes
				await firstValueFrom(rccTranslationService.activeLanguageChange$.pipe(skip(1)))

				return 	categories.map((tag):SymptomCheckConfig =>	({
							meta:	{
									label: rccTranslationService.translate('CURATED.SYMPTOM_CHECKS.TEMPLATE.LABEL', { category:rccTranslationService.translate(`CURATED.QUESTIONS.CATEGORIES.${tag.toUpperCase()}`)
								})
							},
							questions: 	curatedDefaultQuestionStoreService.items
										.filter( question => question.tags.includes(tag) )
										.map( q => q.id )
						}))
			}
		})
	}
}
