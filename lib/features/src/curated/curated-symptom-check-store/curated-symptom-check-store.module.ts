import	{	NgModule 						}		from '@angular/core'
import	{
			provideTranslationMap,
		}											from '@rcc/common'

import	{
			SymptomCheckMetaStoreServiceModule,
		}											from '../../symptom-checks/meta-store'
import	{	CuratedSymptomCheckStoreService		}	from './curated-symptom-check-store.service'


import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		SymptomCheckMetaStoreServiceModule.forChild([CuratedSymptomCheckStoreService]),
	],
	providers: [
		CuratedSymptomCheckStoreService,
		provideTranslationMap('CURATED', { en, de })
	]
})
export class CuratedSymptomCheckStoreServiceModule{}
