import 	{	NgModule									}	from '@angular/core'
import	{	provideTranslationMap,
			provideItemSelectionFilter					}	from '@rcc/common'
import	{
			Item,
			Question,
														}	from '@rcc/core'
import  {
			CuratedDefaultQuestionStoreServiceModule,
			questionCategories as defaultCategories		} 	from './curated-default-questions'
import  { 	CuratedExtendedQuestionStoreServiceModule,
			questionCategories as extendedCategories	} 	from './curated-extended-questions'

import en from './i18n/en.json'
import de from './i18n/de.json'


		const 	questionCategories 				= Array.from(new Set([...defaultCategories, ...extendedCategories]))
export	const	curatedQuestionCategoryFilters 	= questionCategories.map( tag => ({
	filter: (item:Item) => (item instanceof Question) && item.tags?.includes(tag),
	representation: {
		label: `CURATED.QUESTIONS.CATEGORIES.${tag.toUpperCase()}`,
		icon: 'question'
	}
}))
@NgModule({
	imports:[
		CuratedDefaultQuestionStoreServiceModule,
		CuratedExtendedQuestionStoreServiceModule
	],
	providers:[
		provideTranslationMap('CURATED.QUESTIONS', { en,de }),
		...curatedQuestionCategoryFilters.map(provideItemSelectionFilter)

	]
})
export class CuratedQuestionStoreServiceModule{}
