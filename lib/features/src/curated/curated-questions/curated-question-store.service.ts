import	{	Injectable				}	from '@angular/core'
import	{
			QuestionStore,
		}								from '@rcc/core'

// Question Config in: lib/core/src/items/questions/questions.commons.ts


@Injectable()
export class CuratedQuestionStoreService extends QuestionStore {
	public constructor(){
		super()
	}
}
