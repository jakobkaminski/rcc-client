import	{	Inject, Injectable		}	from '@angular/core'
import	{	randomString, has		}	from '@rcc/core'

import	{	RccUserSupportService	}	from '../user-support/user-support.service'
import	{
			SupportTicketData,
			SERVER,
			ZammadServiceConfig,
			DebugInfo,
			DEBUG_INFOS
		}								from '../user-support/user-support.commons'
import	{	RccTranslationService	}	from '@rcc/common'
@Injectable()
export class RccZammadService extends RccUserSupportService {

	public constructor(
		@Inject(SERVER)
		public serverUrl: string,
		protected rccTranslationService: RccTranslationService,
		@Inject(DEBUG_INFOS)
		public debugInfos: DebugInfo[]
	){
		super(debugInfos, rccTranslationService)
	}

	private fingerprint			: string
								= undefined
	private token				: string
								= undefined
	private formEndpoint			: string
								= undefined

	private ticketTitlePrefix	: string
								= 'RC Helpdesk Form |'

	private zammadServiceConfig	: ZammadServiceConfig
								= { url: this.serverUrl }


	/**
	 * This method negotiates a token with the Zammad backend.
	 * We need this token (and a fingerprint) to submit data,
	 * but the inner workings of this mechanism are rather unclear.
	 *
	 * After this method resolved, we have a new token and can submit
	 * to the Zammad backend.
	 */
	public async renewToken(): Promise<void> {

		this.fingerprint		=	this.getFingerprint()

		const config			:	unknown
								=	await this.getZammadBackendConfig()

		if (has(config, 'token', 'endpoint')) {
			this.token			=	config.token as string
			this.formEndpoint	=	config.endpoint as string
		}

	}

	/**
	 * This method overwrites its counterpart on {@link RccUserSupportService}.
	 * It will submit a support ticket to the Zammad backend.
	 *
	 * Use isRetryAfterFailure to indicate that this run is a retry
	 * and should not retry again.
	 */
	public async submitSupportTicket(supportTicketData: SupportTicketData, isRetryAfterFailure: boolean = false): Promise<void> {

		if (!this.token) 			await this.renewToken()
		if (!this.formEndpoint)		await this.renewToken()
		if (isRetryAfterFailure)	await this.renewToken()

		const name				:	string
								=	supportTicketData.userInput.name

		const title				:	string
								=	this.ticketTitlePrefix
									+	` Topic: ${supportTicketData.userInput.topic} |`
									+	` Subject: ${supportTicketData.userInput.subject}`
		const email				:	string
								=	supportTicketData.userInput.email
		const fingerprint		:	string
								=	this.fingerprint
		const token				:	string
								=	this.token

		const extendedBody		:	string
								=	JSON.stringify({
										name,
										title,
										email,
										body: await this.getSupportMessage(supportTicketData),
										fingerprint,
										token
									})

		try {
			console.warn('ZammadService submitSupportTicket: Manually replacing "http" with "https" in formEndpoint')

				const response		:	Response
									=	await fetch(this.formEndpoint.replace('http:', 'https:'),
										{
											method: 'POST',
											headers: {
												'Content-Type': 'application/json'
											},
											body: extendedBody
										})

				return void response.json()
		}
		catch(error){
			if (!isRetryAfterFailure) throw error
			return await this.submitSupportTicket(supportTicketData, true)
		}

	}

	private async getZammadBackendConfig(): Promise<unknown> {
		const server		: 	string
							=	this.zammadServiceConfig.url
		const configUrl		:	string
							=	server + '/api/v1/form_config'

		this.fingerprint	=	this.fingerprint || this.getFingerprint()

		const body			:	string
							=	JSON.stringify({ fingerprint: this.fingerprint })

		const response		:	Response
							=	await fetch(configUrl ,{
								method: 'POST',
								headers: {
									'Content-Type': 'application/json'
								},
								body,
							})

		return response.json()
	}

/**
 * Creates a random string, prefixed with a mimetype, that Zammad requires.
 * Sadly it is unclear what purpose this serves, but we have to supply it,
 * when we post or retrieve a token.
 */
	private getFingerprint()	: string {
		const prefix		:	string
							=	'data:image/png;base64,'
		const fingerprint	:	string
							=	prefix + randomString(64)

		return fingerprint
	}

}
