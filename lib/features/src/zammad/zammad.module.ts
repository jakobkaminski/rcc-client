import	{	ModuleWithProviders, NgModule	}	from '@angular/core'
import	{	RccZammadService				}	from './zammad.service'
import	{
			RccUserSupportModule,
			RccUserSupportService
		}										from '../user-support'
import	{	SERVER							}	from '../user-support/user-support.commons'


@NgModule({
	providers:		[
						RccZammadService,
						{
							provide:		RccUserSupportService,
							useExisting:	RccZammadService
						}
					],
	imports:		[
						RccUserSupportModule
					]


})
/* The RccZammadModule class overwrites the RccUserSupportModule and the
RccUserSupportService with the ZammadService.  */
export class RccZammadModule {
	public constructor(rccUserSupportService: RccUserSupportService) {
		console.warn(rccUserSupportService)
	}

	/**
Configures Zammad API endpoint.
	 */
	public static forRoot(config: {url:string}): ModuleWithProviders<RccZammadModule> {

		return 	{
					ngModule: 	RccZammadModule,
					providers:	[
									{
										provide: 	SERVER,
										useValue: 	config.url
									},
								]
				}
	}
}
