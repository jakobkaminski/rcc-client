import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			provideMainMenuEntry,
			Action,
			getEffectivePosition,
			WithPosition
		}											from '@rcc/common'
import	{
			BackupModule,
			BackupHomePath
		}											from './backup.module'
@NgModule({
	imports: [
		BackupModule,
	],
})

export class BackupMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `BackupMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<BackupMainMenuEntryModule> {

		const mainMenuEntry	: Action =	{
				path:			BackupHomePath,
				label: 			'BACKUP.MENU_ENTRY',
				icon: 			'backup',
				position:		getEffectivePosition(config, 5),
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	BackupMainMenuEntryModule,
			providers
		}
	}

}
