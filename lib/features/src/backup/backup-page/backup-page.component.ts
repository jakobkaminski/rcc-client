import 	{
			Component,
			ElementRef,
			ViewChild
		}								from '@angular/core'

import {
			HandlerAction,
			DownloadAction,
			RccAlertController,
			RccTranslationService,
			ShareDataService,
		}								from '@rcc/common'
import	{
			BackupRestoreHelper,
		}								from '../backup-restore.helper'
import	{	BackupService			}	from '../backup.service'
import	{	CalendarDateString		}	from '@rcc/core'

const BACKUP_FILE_PREFIX	: string = 'rcc-backup-'

@Component({
	templateUrl: 	'./backup-page.component.html',
	styleUrls:		['./backup-page.component.scss']
})

export class BackupPageComponent{
	protected filename: string = undefined

	@ViewChild('fileInput')
	public elementRef: ElementRef<HTMLInputElement>

	public downloadAction: DownloadAction = {
		label:       'BACKUP.PAGE.DOWNLOAD.LABEL',
		description: 'BACKUP.PAGE.DOWNLOAD.DESCRIPTION',
		icon:        'backup_download',
		data:		 () => this.downloadBackup(),
		filename:	 () =>  BACKUP_FILE_PREFIX + CalendarDateString.today() + '-' + String(new Date().getTime()),
	}

	public restoreAction: HandlerAction = {
		label:       'BACKUP.PAGE.RESTORE.LABEL',
		description: 'BACKUP.PAGE.RESTORE.DESCRIPTION',
		icon:		 'backup_restore',
		handler:	 () => this.restore()
	}

	public sendAction: HandlerAction = {
		label:		 'BACKUP.PAGE.SEND.LABEL',
		description: 'BACKUP.PAGE.SEND.DESCRIPTION',
		icon:        'backup_transfer',
		handler:	 () => this.sendData()
	}


	// 1. Backup erzeugen
	// 2. Download triggern (Action oben)

	public constructor(
		private backupService: BackupService,
		private rccAlertController: RccAlertController,
		private rccTranslationService: RccTranslationService,
		private shareDataService	: ShareDataService,
		private backupRestoreHelper	: BackupRestoreHelper
	){
		backupRestoreHelper.showToastAfterReload()
	}

	public async downloadBackup(): Promise<string> {
		this.filename = BACKUP_FILE_PREFIX + CalendarDateString.today() + '-' + String(new Date().getTime())
		return this.rccAlertController.confirm('BACKUP.PAGE.DOWNLOAD.CONFIRM').then(
			async () => {
				const data:  Record<string, unknown[]> = await this.backupService.exportData()
				return JSON.stringify(data)
			}
		)
	}

	public restore(): void{
		this.elementRef.nativeElement.click()
	}

	public onChange(): void {
		const file		: File		= this.elementRef.nativeElement.files[0]
		const date		: Date		= this.getDateFromFileName(file.name)

		const confirmationMessage : string = this.getConfirmationMessage(date)

		void this.rccAlertController.confirm(confirmationMessage).then(
			() => { this.restoreFromFile(file) }
		)
	}

	private getDateFromFileName(filename: string): Date {
		try {
			const dateAndTime	: string	= filename.split(BACKUP_FILE_PREFIX)[1]
			const split		: string[]	= dateAndTime.split('-')

			const year	: string = split[0]
			const month	: string = split[1]
			const day 	: string = split[2]

			return CalendarDateString.toDate(`${year}-${month}-${day}`)
		} catch (e: unknown) {
			return undefined
		}
	}

	private getConfirmationMessage(date: Date): string {
		if (date) {
			const dateString: string	= this.rccTranslationService.translate(
				date,
				{ year: 'numeric', month:'2-digit', day: '2-digit' }
			)
			return this.rccTranslationService.translate('BACKUP.PAGE.RESTORE.CONFIRM', { date: dateString })
		} else
			return this.rccTranslationService.translate('BACKUP.PAGE.RESTORE.CONFIRM_UNKNOWN_DATE')
	}

	private restoreFromFile(file: File): void {
		const reader: FileReader	= new FileReader()
		reader.onload = () => {
			const result: Record<string, unknown[]> = JSON.parse(reader.result.toString()) as  Record<string, unknown[]>
			this.backupRestoreHelper.importAndToast(result)
		}
		reader.readAsText(file)
	}

	public async sendData(): Promise<void> {
		const data: Record<string, unknown[]> = await this.backupService.exportData()
		await this.shareDataService.share(['rcc-backup', data], 'send', 'SHARE_DATA.SHOW_QR_CODE.BACKUP')
	}
}
