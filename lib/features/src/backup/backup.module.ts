import 	{
			NgModule,
		}									from '@angular/core'

import	{
			RouterModule,
			Routes
		}									from '@angular/router'

import	{
			SharedModule,
			ShareDataModule,
			TranslationsModule,
			provideTranslationMap,
		}									from '@rcc/common'

import	{	BackupService				}	from './backup.service'
import	{	BackupPageComponent			}	from './backup-page/backup-page.component'
import	{	BackupRestoreHelper			}	from './backup-restore.helper'


import de from './i18n/de.json'
import en from './i18n/en.json'

export const BackupHomePath: string = '/backup'

const routes: Routes	=	[
								{
									path: 		'backup',
									title:		'BACKUP.PAGE_TITLE',
									component: 	BackupPageComponent
								}
							]

@NgModule({
	imports:[
		SharedModule,
		RouterModule.forChild(routes),
		TranslationsModule,
		ShareDataModule,
	],
	providers:[
		BackupRestoreHelper,
		BackupService,
		provideTranslationMap('BACKUP', { en, de })
	],
	declarations:[
		BackupPageComponent
	]
})
export class BackupModule {}
