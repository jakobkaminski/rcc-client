import	{	InjectionToken		}	from '@angular/core'
import 	{
			Question,
			Report,
		}							from '@rcc/core'

/**
 *	Datapoint is used for plotting
 * 	@param 	{unknown} 	value 	The value (most probably an answer to a question).
 * 	@param 	{string}	date	A DateTimeString in ISO8601 (maybe with timezone RFC3339).
 * 	@param 	{string}	note	An optional note for additional information.
 */
export interface Datapoint{
	value:		NonNullable<unknown>,
	date:		NonNullable<string>,
	note?:		string
}

/**
 * Dataset maps a question to it's datapoints
 * @param	{Question}		question 	The question the data belongs to.
 * @param	{Datapoint[]}	datapoints 	The corresponding datapoints.
 */
export interface Dataset {
	question:		Question,
	datapoints:		Datapoint[]
}



export abstract class ReportPreparator {

	/**
	 * Compiles a {@Report} into a Dataset.
	 */
	public abstract prepare(report: Report):Promise<Dataset[]>

}

export const REPORT_PREPARATORS = new InjectionToken<ReportPreparator>('ReportPreparator')
