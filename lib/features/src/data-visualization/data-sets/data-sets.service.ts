import	{
			Inject,
			Injectable,
			Injector,
			Optional,
			Type,
		}							from '@angular/core'

import	{	Report,
			assert
		}							from '@rcc/core'

import	{
			Dataset,
			REPORT_PREPARATORS,
			ReportPreparator,
		}							from './data-sets.commons'


@Injectable()
export class DatasetsService {

	public constructor(
		@Optional() @Inject(REPORT_PREPARATORS)
		protected reportPreparatorClasses: 		Type<ReportPreparator>[],
		protected injector:						Injector
	){

		if(!reportPreparatorClasses) throw new Error('DatasetsService.constructor() Missing report preparators. Please make sure to provide at least one preparator (e.g. BasicReportPreparatorModule).')
	}

	public async getDatasets(report: Report): Promise<Dataset[]> {

		assert(report, 'DatasetsService.getDatasets(): missing report.')

		const datasetPromises 	:	Promise<Dataset[]>[]
								=	this.reportPreparatorClasses.map( async PreparatorClass => {

										let prep_instance : ReportPreparator = undefined

										try			{		prep_instance = this.injector.get(PreparatorClass) 		}
										catch(e)	{		console.error(e); prep_instance = new PreparatorClass() 	}

										const datasets	: Dataset[]
														= await prep_instance.prepare(report)

										return datasets

									})

		const datasets 			:	Dataset[]
								=	(await Promise.all(datasetPromises)).flat()

		return 	datasets

	}

}
