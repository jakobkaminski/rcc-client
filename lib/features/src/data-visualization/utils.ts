import  {
            CalendarDateString,
			isFinite
        }                               from '@rcc/core'

/**
 * Returns `'up'` for positive and `'down'` for negative values
*/
export function getIcon(value: unknown) : string {
	if(!isFinite(value))	return null

	if(value > 0)			return 'up'
	if(value < 0)			return 'down'

	return null
}

/**
 * Converts `value` to a string of its absolute value in fixed-point notation
 * with `fractionDigits` numbers after the decimal point (defaults to `1`).
 */
export function getAbsValueString(value: unknown, fractionDigits: number = 1) : string {
	if (!isFinite(value))	return

	const absValue : number = Math.abs(value)
	return absValue.toFixed(fractionDigits)
}

/**
 * Calculates the difference in days between `startDate` and `endDate`
 * and substracts it from both dates. Returns the resulting ISO8601 strings
 *
 * @example
 * startDate = 2023-08-23, endDate = 2023-08-30
 * -> difference = 7
 * -> return ['2023-08-16', '2023-08-23']
*/
export function getPreviousRange(startDate: string, endDate: string): [string, string] {
	const numberOfDays		: 	number
							= 	CalendarDateString.diffInDays(endDate, startDate)

	const previousStartDate	: 	string
							= 	CalendarDateString.daysBefore(startDate, numberOfDays)

	const previousEndDate	: 	string
							= 	CalendarDateString.daysBefore(endDate, numberOfDays)

	return [previousStartDate, previousEndDate]
}
