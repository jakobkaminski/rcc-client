import	{
			NgModule,
			ModuleWithProviders,
			Type
		}										from '@angular/core'

import	{	DatasetsService					}	from './data-sets/data-sets.service'

import	{
			ReportPreparator,
			REPORT_PREPARATORS,
		}										from './data-sets/data-sets.commons'




@NgModule({
	declarations: [
	],
	providers: [
		DatasetsService,
	]
})
export class DataVisualizationModule {

	public static forChild(reportPreparatorClasses: Type<ReportPreparator>[]): ModuleWithProviders<DataVisualizationModule> {

		console.log('DataVisualizationModule.forChild don\'t use this method, implement provideReportPreparator')

		return {
			ngModule: 	DataVisualizationModule,
			providers:	[
							...(reportPreparatorClasses||[]).map( reportPreparatorClass => ({ provide: REPORT_PREPARATORS, useValue: reportPreparatorClass, multi: true })),
						]
		}
	}


}
