import	{	InjectionToken, Provider	}	from '@angular/core'
import	{	getProvider		}	from '@rcc/common'

export interface DebugInfo {
	id					: string,
	label 				: string,
	description 		: string,
	getValue 			: () => string,
	includeByDefault?	: boolean

}

export interface SupportTicketUserInput {
	name:		string,
	email:		string,
	subject?:	string,
	topic:		string,
	area:		string,
	message:	string,
}
export interface SupportTicketDebugInfo {
	[index: string] : string
}

export interface SupportTicketData {
	userInput: SupportTicketUserInput,
	includedDebugInfo: string[]
}

export interface ZammadServiceConfig {
	url: string
}

export const DEBUG_INFOS: InjectionToken<DebugInfo[]> = new InjectionToken<DebugInfo[]>('DEBUG_INFOS')

export const SERVER 	: InjectionToken<string>
						= new InjectionToken<string>('UserSupportService: URL used to connect to the Recovery Cat support system server.')

export function provideDebugInfo(debugInfo: DebugInfo): Provider {
	return getProvider(DEBUG_INFOS, debugInfo, true)
}

export const supportPagePath : string = '/support'
