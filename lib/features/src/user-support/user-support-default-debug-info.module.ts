import	{	NgModule						}	from '@angular/core'
import	{
			ProductOrFactory,
			RccBuildInfoService,
		}										from '@rcc/common'
import	{	DebugInfo, provideDebugInfo		}	from './user-support.commons'

/* `debugInfos` is an array of objects that define the debug information to
be provided by the `RccUserSupportDefaultDebugInfoModule`. Each object in
the array represents a debug information item and contains the following
properties: */
const debugInfos : ProductOrFactory<DebugInfo>[] = [
	{
		deps: 		[ RccBuildInfoService ],
		factory: 	(rccBuildInfoService : RccBuildInfoService) => ({
						id				: 'version',
						label			: 'USER_SUPPORT.DEBUG_INFO.VERSION.LABEL',
						description		: 'USER_SUPPORT.DEBUG_INFO.VERSION.DESCRIPTION',
						includeByDefault: true,
						getValue		: () => rccBuildInfoService.buildInfos.tag + '-' + rccBuildInfoService.buildInfos.flavor
					})
	},
	{
		deps: 		[],
		factory: 	() => ({
						id				: 'user-agent',
						label			: 'USER_SUPPORT.DEBUG_INFO.USER_AGENT.LABEL',
						description		: 'USER_SUPPORT.DEBUG_INFO.USER_AGENT.DESCRIPTION',
						includeByDefault: true,
						getValue		: () => navigator.userAgent
					})
	},
]

@NgModule({
	providers:		[
						...debugInfos.map(provideDebugInfo),
					],

})
export class RccUserSupportDefaultDebugInfoModule {}
