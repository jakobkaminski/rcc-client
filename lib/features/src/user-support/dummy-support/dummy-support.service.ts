import	{	Inject, Injectable		}	from '@angular/core'
import	{	RccUserSupportService	}	from '../user-support.service'
import	{
			SupportTicketData,
			DebugInfo,
			DEBUG_INFOS
		}								from '../user-support.commons'
import	{	RccTranslationService	}	from '@rcc/common'

@Injectable()
export class RccDummySupportService extends RccUserSupportService {

	public constructor(
		protected rccTranslationService: RccTranslationService,
		@Inject(DEBUG_INFOS)
		public debugInfos: DebugInfo[]
	){
		super(debugInfos, rccTranslationService)
	}

/**
 * The function `submitSupportTicket` is an asynchronous function that takes in a
 * `supportTicketData` object and a boolean `isRetryAfterFailure` parameter, and
 * simply logs a warning message with the provided data.
 * @param {SupportTicketData} supportTicketData - The supportTicketData parameter
 * is an object that contains the data for the support ticket. It includes
 * information such as the user's name, email, subject etc.
 * @param {boolean} [isRetryAfterFailure=false] - The `isRetryAfterFailure`
 * parameter is a boolean flag that indicates whether the support ticket
 * submission was a retry after a failure. By default, it is set to `false`,
 * and it should be set to true if the method is called a second time after it
 * failed before.
 */
	public async submitSupportTicket(supportTicketData: SupportTicketData, isRetryAfterFailure: boolean = false): Promise<void> {
		const debugInfoMessage	: string
								= await this.getDebugInfoMessage(supportTicketData.includedDebugInfo)
		console.warn('DummySupportService.submitSupportTicket', { supportTicketData, isRetryAfterFailure, debugInfoMessage })
	}
}
