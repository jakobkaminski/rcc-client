import	{
			Component,
			OnDestroy
		}								from '@angular/core'
import	{
			FormControl,
			FormGroup,
			Validators,
		}								from '@angular/forms'
import	{
			DebugInfo,
			SupportTicketData,
			SupportTicketUserInput,
			supportPagePath
		}									from './user-support.commons'
import	{	RccUserSupportService		}	from './user-support.service'
import	{
			RccAlertController,
			RccToastController
		}									from '@rcc/common'
import	{	Subject, merge, takeUntil	}	from 'rxjs'
import	{	Router						}	from '@angular/router'


@Component({
	selector:		'rcc-user-support-form',
	templateUrl:	'./user-support-form.component.html',
	styleUrls:		['./user-support-form.component.scss'],

})
export class RccUserSupportFormComponent implements	OnDestroy {

	protected destroy$					: Subject<void> = new Subject<void>()
	protected supportForm				: FormGroup
	protected debugInfoFormControls		: FormGroup		= new FormGroup({})
	protected failureMessage			: string 		= 'USER_SUPPORT.INSUFFICIENT_DETAILS'
	protected successMessage			: string 		= 'USER_SUPPORT.SUCCESS'
	protected sendDebugInfoCheckbox		: boolean 		= false
	protected debugInfos				: DebugInfo[]	= []

	protected contentVisible			: boolean		= false
	protected contentHeight				: string		= '0'

	protected debugMessage				: string 		= 'USER_SUPPORT.DEFAULT_DEBUG_MESSAGE'

	protected areas						: {value: string, label: string }[]
	protected topics					: {value: string, label: string }[]

	protected latestDebugInfoValues		: Record<string, string> = {}

	protected showDebugInfoDetails		: boolean = false


	public constructor(
	protected userSupportService		: RccUserSupportService,
	protected rccAlertController		: RccAlertController,
	protected rccToastController		: RccToastController,
	protected router					: Router,

	) {
		this.areas 		= this.userSupportService.areas
		this.topics 	= this.userSupportService.topics
		this.debugInfos	= this.userSupportService.debugInfos

		this.setup()
	}

	/**
	 * The setup function initializes form controls and sets up subscriptions for
	 * value changes.
	 */
	protected setup(): void {
		this.debugInfos.forEach(debugInfo => {
			this.debugInfoFormControls.addControl(debugInfo.id, new FormControl<boolean>(!!debugInfo.includeByDefault))
		})
		this.supportForm = new FormGroup({
			name:				new FormControl<string>(''),
			email:				new FormControl<string>('', [Validators.email, Validators.required]),
			subject:			new FormControl<string>(''),
			message:			new FormControl<string>('',  Validators.required),
			topic:				new FormControl<string>('', Validators.required),
			area:				new FormControl<string>(''),
			sendDebugInfo:		new FormControl<boolean>(false),
			debugInfos:			this.debugInfoFormControls,
		})
		merge(
			this.supportForm.get('debugInfos').valueChanges,
			this.supportForm.get('sendDebugInfo').valueChanges,
			this.supportForm.get('topic').valueChanges,
		)
		.pipe(
			takeUntil(this.destroy$)
		)
		.subscribe(() => {
			this.updateIfAreaSelected()
		})
	}

	/**
	 * The function updates the validation rules for the 'area' field in a form based
	 * on the selected value of the 'topic' field.
	 */
	protected updateIfAreaSelected(): void {
		if (this.supportForm.get('topic').value === 'bug') {
			this.supportForm.get('area').setValidators(Validators.required)
			this.supportForm.controls.area.updateValueAndValidity()
		}
		else {
			this.supportForm.get('area').removeValidators(Validators.required)
			this.supportForm.controls.area.updateValueAndValidity()
		}
	}

	/**
	 * The function `getUserSelectedDebugInfoIds` returns an array of selected debug
	 * info IDs based on the user's input in a form.
	 * @returns an array of string values.
	 */
	protected getUserSelectedDebugInfoIds(): string[] {

		if (this.supportForm.get('sendDebugInfo').value !== true) return []

		const debugInfoChecked	:	Record<string, unknown>
								=	this.supportForm.get('debugInfos').value as Record<string, unknown>
		return Object.entries(debugInfoChecked)
			.filter(([, checked]) => checked)
			.map(([key,]) => key)
	}

/**
 * The function updates the latest debug info values by iterating through the
 * debugInfos array and assigning the value of each debug info to the corresponding
 * id in the latestDebugInfoValues object.
 */
	protected updateLatestDebugInfoValues(): void {
		this.latestDebugInfoValues = {}
		this.debugInfos.forEach((debugInfo) => {
			this.latestDebugInfoValues[debugInfo.id] = debugInfo.getValue()
		})
	}

/**
 * The function `toggleDebugInfoDetails` toggles the visibility of debug
 * information details and updates the latest debug information values.
 * @param {boolean} [toggle] - The toggle parameter is an optional boolean value
 * that determines whether to show or hide the debug info details. If toggle is not
 * provided, the function will toggle the value of this.showDebugInfoDetails. If
 * toggle is provided, the function will set this.showDebugInfoDetails to the value
 * of toggle.
 */
	protected toggleDebugInfoDetails(toggle?: boolean): void {
		const show : boolean	=	toggle === undefined
								?	!this.showDebugInfoDetails
								:	toggle

		this.showDebugInfoDetails = show
		this.updateLatestDebugInfoValues()
	}

	/**
	 * The function returns a FormControl object from a Map based on the provided id.
	 * @param {string} id - A string representing the ID of the debug info form
	 * control.
	 * @returns The method is returning a `FormControl<boolean>` object.
	 */
	protected getDebugInfoFormControl(id: string): FormControl<boolean> {
		return this.debugInfoFormControls.get(id) as FormControl<boolean>
	}

	/**
	 * The submitForm function is used to handle form submission, validate the form,
	 * create a support ticket and display a success message.
	 */
	protected async submitForm(): Promise<void> {
		this.supportForm.markAllAsTouched()
		if (this.supportForm.invalid) await this.rccToastController.failure(this.failureMessage)
		if (this.supportForm.valid) {
			const supportTicketUserInput	:	SupportTicketUserInput
											=	this.supportForm.value as SupportTicketUserInput
			const debugInfoIds				:	string[]
											=	this.getUserSelectedDebugInfoIds()
			const supportTicketData			: 	SupportTicketData
											= 	this.userSupportService.createSupportTicket(
													supportTicketUserInput,
													debugInfoIds
												)

			await this.userSupportService.submitSupportTicket(supportTicketData)
			await this.rccAlertController.success(this.successMessage)
			await this.rccToastController.success(this.successMessage)
			void this.router.navigate([supportPagePath])
		}
	}

	/**
	 * The function toggles the visibility and height of a content element.
	 */
	protected toggleDetails(): void {
		this.contentVisible = !this.contentVisible
		this.contentHeight = this.contentVisible ? '31,25rem' : '0'
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
