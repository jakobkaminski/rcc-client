import	{	Inject, Injectable, Optional	}	from '@angular/core'
import	{
			SupportTicketData,
			SupportTicketUserInput,
			DebugInfo,
			DEBUG_INFOS,
		}										from './user-support.commons'
import	{	RccTranslationService			}	from '@rcc/common'

@Injectable() export class RccUserSupportService {

	public constructor(
		@Optional() @Inject(DEBUG_INFOS)
		public debugInfos: DebugInfo[],
		protected rccTranslationService: RccTranslationService
	) {
		this.debugInfos = this.debugInfos || []
	}

	protected areaList : string[] =	[
		'homepage'
	,	'monitoring_setup'
	,	'therapy_plans'
	,	'answer_questions'
	,	'therapy_plan'
	,	'receive'
	,	'share'
	,	'main_menu'
	,	'settings'
	,	'backup_restore'
	,	'miscellaneous'
	,	'question_setup'
]
	protected topicList : string[] =	[
		'general'
	,	'bug'
	,	'feedback'
	,	'feature'
	,	'incident'
	,	'press'
	,	'miscellaneous'

]

	/* The code is creating two arrays, `topics` and `areas`, which are used for
	selecting topics and areas in the user support form. */

	public topics: {value: string, label: string }[] = [
		...this.topicList.map((topic: string) => (
			{ value: topic, label: `USER_SUPPORT.SELECT.TOPICS.${topic.toUpperCase()}` }
		))
	]
	public areas: {value: string, label: string }[] = [
		...this.areaList.map((area: string) => (
			{ value: area, label: `USER_SUPPORT.SELECT.AREAS.${area.toUpperCase()}` }
		))
	]

	/**
	 * The function "createSupportTicket" takes in user input and debug information
	 * IDs, and returns a support ticket data object containing the user input and
	 * included debug information.
	 * @param {SupportTicketUserInput} supportTicketUserInput - The
	 * supportTicketUserInput parameter is an object that contains the user's input
	 * for creating a support ticket.
	 * @param {string[]} debugInfoIds - The `debugInfoIds` parameter is an optional
	 * array of strings that represents the IDs of any additional debug information
	 * that should be included in the support ticket. This can be useful for providing
	 * additional context or troubleshooting information related to the issue being
	 * reported. If no debug information is provided, the array will be empty.
	 * @returns an object of type SupportTicketData.
	 */
	public createSupportTicket(supportTicketUserInput: SupportTicketUserInput, debugInfoIds: string[] = []): SupportTicketData {
		const userInput: SupportTicketUserInput = supportTicketUserInput
		const includedDebugInfo: string[] = debugInfoIds

		return {
			userInput,
			includedDebugInfo
		}
	}

	/**
	 * The function takes an array of debug info IDs, filters the
	 * relevant debug info objects, retrieves their values asynchronously, and returns
	 * a formatted message containing the labels and values of the debug info objects.
	 * @param {string[]} debugInfoIds - An array of string values representing the IDs
	 * of the debug information to retrieve.
	 * @returns a Promise that resolves to a message string that includes all debug info on individual lines that are then.
	 */

	protected async getDebugInfoMessage(debugInfoIds: string[]): Promise<string> {
		const relevantDebugInfos:	DebugInfo[]
								=	this.debugInfos.filter(
										debugInfo => debugInfoIds.includes(debugInfo.id)
									)
		const debugInfoPromises	:	string[][]
								=	relevantDebugInfos.map( debugInfo => {
										const label: string = this.rccTranslationService.translate(debugInfo.label)
										const value: unknown = debugInfo.getValue()
										return [label, JSON.stringify(value)]
									})
		const debugInfoLines	:	string[][]
								=	await Promise.all(debugInfoPromises)
		const message			: 	string
								=	debugInfoLines
									.map(line => line.join(': '))
									.join('\n')
		return message
	}

	/**
	 * The function "getSupportMessage" returns a formatted support message including
	 * user input and debug information.
	 * @param {SupportTicketData} supportTicketData - The supportTicketData parameter
	 * is an object that contains the data related to a support ticket. It has the
	 * following properties:
	 * @returns a string message.
	 */
	protected async getSupportMessage(supportTicketData: SupportTicketData): Promise<string> {
		console.warn(supportTicketData)
		let message	: string	= ''

		if(supportTicketData.includedDebugInfo.length === 0)
		message =	`
${supportTicketData.userInput.area}
Message:
${supportTicketData.userInput.message}
									`

		else message = `
${supportTicketData.userInput.area}
Message:
${supportTicketData.userInput.message}

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
_________________DebugInfo_________________ᨐฅ

${await this.getDebugInfoMessage(supportTicketData.includedDebugInfo)}

									`


		return message
	}

	/**
	 * This function is supposed to be overwritten by a child class.
	 * @param {SupportTicketData} supportTicketData - The supportTicketData parameter
	 * is an object that contains the data for the support ticket. It could include
	 * information such as the user's name, email, description of the issue, and any
	 * other relevant details.
	 * @param {boolean} [isRetryAfterFailure=false] - The `isRetryAfterFailure`
	 * parameter is a boolean flag that indicates whether the support ticket
	 * submission was a retry after a failure. By default, it is set to `false`,
	 * and it should be set to true if the method is called a second time after
	 * it failed before.
	 * @returns a Promise that resolves to void.
	 */
	public async submitSupportTicket(supportTicketData: SupportTicketData,isRetryAfterFailure: boolean = false): Promise<void> {
		console.warn(isRetryAfterFailure)

		throw new Error('RccUserSupportService.submitSupportTicket: missing implementation')
		return await Promise.resolve()
	}

}
