import 	{
			NgModule,
			ModuleWithProviders,
			Type,
		} 											from '@angular/core'
import	{
			BaseMetaStoreModule,
			MetaAction,
			MetaStoreConfig,
			MetaStoreModule,
			provideTranslationMap,
			SharedModule,
		}											from '@rcc/common'
import	{
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
		}											from '@rcc/core'
import	{	SymptomCheckMetaStoreService		}	from './symptom-check-meta-store.service'
import	{
			SYMPTOM_CHECK_STORES,
			SYMPTOM_CHECK_META_ACTIONS,
		}											from './symptom-check-meta-store.commons'
import	{	PatSymptomCheckMetaStorePageComponent 	}	from './overview-page-pat/overview-page.component'
import	{	HCPSymptomCheckMetaStorePageComponent 	}	from './overview-page-hcp/overview-page.component'


import en from './i18n/en.json'
import de from './i18n/de.json'


const metaStoreConfig: MetaStoreConfig<SymptomCheckConfig, SymptomCheck, SymptomCheckStore>
							=	{
									itemClass:			SymptomCheck,
									serviceClass:		SymptomCheckMetaStoreService
								}



export class MenuEntrySymptomCheckMetaStoreServiceComponent {}

@NgModule({
	declarations: [
		PatSymptomCheckMetaStorePageComponent,
		HCPSymptomCheckMetaStorePageComponent
	],
	imports: [
		SharedModule,
		MetaStoreModule.forChild(metaStoreConfig),
	],

	exports: [
		MetaStoreModule // So other Module need not import this specifically
	],

	providers:[
		SymptomCheckMetaStoreService,
		provideTranslationMap('SYMPTOM_CHECK_META_STORE', { en, de })
	]
})
export class SymptomCheckMetaStoreServiceModule extends BaseMetaStoreModule {

	public static 	forChild(
				stores?			: Type<SymptomCheckStore>[],
				metaActions?	: MetaAction<SymptomCheckConfig, SymptomCheck, SymptomCheckStore>[]
			): ModuleWithProviders<SymptomCheckMetaStoreServiceModule> {


		const mwp: ModuleWithProviders<SymptomCheckMetaStoreServiceModule>
			=	BaseMetaStoreModule.getModuleWithProviders<SymptomCheckMetaStoreServiceModule>(
				this,
				stores,
				metaActions,
				SYMPTOM_CHECK_STORES,
				SYMPTOM_CHECK_META_ACTIONS
			)

		return mwp

	}

}
