import 	{
			Component,
			Injectable,
			OnDestroy
		} 								from '@angular/core'

import	{	SubscriptionLike		}	from 'rxjs'
import	{
			map,
			filter,
		}								from 'rxjs/operators'

import	{
			HandlerAction,
			RccAlertController,
			RccBuildInfoService,
			RccModalController,
			RccOverlayService,
		}								from '@rcc/common'

import	{
			Schedule,
			ScheduleConfig,
			SymptomCheck,
			SymptomCheckConfig,
			SymptomCheckStore,
			UserCanceledError
		}								from '@rcc/core'


import	{
			RccStorage,
			IncomingDataService
		}								from '@rcc/common'

import	{	IcsReminderService		}	from '../../ics-reminders'
import	{
			RccDataArrivedModalComponent
		}								from '@rcc/themes/active'
import	{	IonicModalsModule		}	from '@rcc/ionic'
import	{	Router					}	from '@angular/router'

@Component({
	template	: '<rcc-data-arrived-modal [action]="action"></rcc-data-arrived-modal>',
	imports		: [RccDataArrivedModalComponent, IonicModalsModule],
	standalone	: true,
	styles		: [':host { height: 100% }'],
})
class ModalRelayComponent {
	public constructor(
		private readonly rccModalController: RccModalController,
		private readonly rccBuildInfoService: RccBuildInfoService,
	) {}

	protected action: HandlerAction = {
		label		: 'DATA_TRANSFER_BUTTON_TEXT',
		description	: 'DATA_TRANSFER_ARRIVED_TEXT',
		handler		: () => {
			this.rccModalController.dismiss('import-symptomcheck')
		},
		icon		: this.rccBuildInfoService.buildInfos.flavor === 'hcp' ? 'transmission_complete' : 'smiley',
		category	: 'create',
	}
}

@Injectable()
export class ImportSymptomCheckStoreService extends SymptomCheckStore implements OnDestroy {

	public readonly name	: string = 'IMPORT_SYMPTOM_CHECK_STORE.NAME'

	private subscriptions	: SubscriptionLike[] = []

	public constructor(
		private incomingDataService	: IncomingDataService,
		rccStorage					: RccStorage,
		private rccAlertController	: RccAlertController,
		private icsReminderService	: IcsReminderService,
		private overlayService		: RccOverlayService,
		private modalController		: RccModalController,
		private router				: Router,
	){
		super(
			rccStorage.createItemStorage('rcc-import-symptom-checks'),
		)

		this.listenToIncomingDataService()
	}


	protected listenToIncomingDataService() : void {
		this.subscriptions.push(
			this.incomingDataService
			.pipe(
				map( 	(data:unknown) 								=> SymptomCheck.findConfigs(data) ),
				filter(	(symptomCheckConfigs: SymptomCheckConfig[])	=> symptomCheckConfigs.length > 0),
			)
			.subscribe( (symptomCheckConfigs) => void this.handleIncomingSymptomCheckConfigs(symptomCheckConfigs) )
		)
	}


	public ngOnDestroy() : void {
		this.subscriptions.forEach( sub => sub.unsubscribe() )
	}

	public async handleIncomingSymptomCheckConfigs(configs: SymptomCheckConfig[]) : Promise<void> {


		const result: string = await this.overlayService.relay(ModalRelayComponent)


		if(result !== 'import-symptomcheck') throw new UserCanceledError('handleIncomingSymptomCheckConfigs')

		await this.addSymptomCheckConfig(configs)

		try {
			await this.router.navigate(['/day-query-run'])
			await this.rccAlertController.present({
						header: 	'IMPORT_SYMPTOM_CHECK_STORE.ADD_ICS_REMINDER.HEADER',
						message: 	'IMPORT_SYMPTOM_CHECK_STORE.ADD_ICS_REMINDER.DESCRIPTION',
						buttons:	[
										{
											label:		'IMPORT_SYMPTOM_CHECK_STORE.ADD_ICS_REMINDER.DISMISS',
											rejectAs: 	'dismiss'
										},
										{
											label:		'IMPORT_SYMPTOM_CHECK_STORE.ADD_ICS_REMINDER.CONFIRM',
											resolveAs: 	'ics'
										}
									]
					})

			const timeGuess: string =
							configs
								.map( config => config.meta?.reminder)
								.filter( x => !!x)[0]

			const scheduleConfig: ScheduleConfig = configs.filter(config => config?.meta?.defaultSchedule != null)[0]?.meta?.defaultSchedule

			await this.icsReminderService.promptDownload({ timeString: timeGuess, frequency: new Schedule(scheduleConfig) }, 'RecoveryCat.ics')
		} finally {
			this.modalController.dismiss()
		}
	}


	public async addSymptomCheckConfig(configs: SymptomCheckConfig[])	: Promise<SymptomCheck[]>
	public async addSymptomCheckConfig(config: 	SymptomCheckConfig)		: Promise<SymptomCheck>

	public async addSymptomCheckConfig(x: 		SymptomCheckConfig | SymptomCheckConfig[] )	: Promise<SymptomCheck|SymptomCheck[]>
	{

		if(!Array.isArray(x)) return this.addSymptomCheckConfig([x]).then( arr => arr[0] )

		const symptom_checks: SymptomCheck[] = x.map( config => this.addConfig(config) )

		await this.storeAll()

		return symptom_checks

	}


	public async deleteSymptomCheck(symptom_check: SymptomCheck): Promise<SymptomCheck> {
		if(!this.removeItem(symptom_check)) throw new Error('ImportSymptomCheckStoreService.delete: Unable to delete symptom check with id: ' + symptom_check.id)

		return 	this.storeAll()
				.then( () => symptom_check)
	}
}
