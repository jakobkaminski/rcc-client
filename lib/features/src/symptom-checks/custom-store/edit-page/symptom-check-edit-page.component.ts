import 	{
			Component,
			OnInit,
			ViewChild
		}									from '@angular/core'

import	{	Location					}	from '@angular/common'

import	{
			ActivatedRoute,
		}									from '@angular/router'

import	{
			UserCanceledError,
			SymptomCheck,
		}									from '@rcc/core'

import	{
			MetaStoreService,
			RccToastController,
			ItemEditResult
		}									from '@rcc/common'

import	{
			assert,
			Question
		}									from '@rcc/core'

import	{
			combineLatest,
			map
		}									from 'rxjs'

import	{
			CustomQuestionStoreService,
			QuestionnaireService
		}									from '../../../questions'

import	{
			CustomSymptomCheckStoreService
		}									from '../custom-symptom-check-store.service'


import	{
			SymptomCheckEditComponent
		}									from '../../edit'


@Component({
	templateUrl: 	'./symptom-check-edit-page.component.html',
})
export class SymptomCheckEditPageComponent implements OnInit{



	@ViewChild(SymptomCheckEditComponent)
	public symptomCheckEditComponent: SymptomCheckEditComponent

	public symptomCheckEdit			: SymptomCheck

	public mode						: string

	public constructor(
		private activatedRoute					: ActivatedRoute,
		private customSymptomCheckStoreService	: CustomSymptomCheckStoreService,
		private metaStoreService				: MetaStoreService,
		private rccToastController				: RccToastController,
		private location						: Location,
		private customQuestionStoreService		: CustomQuestionStoreService,
		private questionnaireService			: QuestionnaireService
	){}


	public ngOnInit(): void {

		const observables = {
								id:		this.activatedRoute.paramMap
										.pipe( map ( params		=> params.get('id') ) ),

								mode: 	this.activatedRoute.data
										.pipe( map ( ({ mode })	=> typeof mode === 'string' ? mode : undefined ) )
							}

		combineLatest(observables)
		.subscribe( ({ id, mode }) => void this.setup(id, mode) )

	}


	public async setup(id: string | undefined, mode: string): Promise<void> {

		assert(!this.symptomCheckEditComponent, 'SymptomCheckEditPageComponent.setup(): symptomCheckEditComponent was rendered before .setup(). Maybe the path has changed and the component got rerendered?')
		assert(['new', 'edit', 'from'].includes(mode), `SymptomCheckEditPageComponent.setup(): unknown mode; must be one of 'new', 'edit' or 'from', got ${mode}.`, mode)

		this.mode 				= 	mode


		if(mode === 'new')	this.symptomCheckEdit = new SymptomCheck()
		if(mode === 'from')	this.symptomCheckEdit = await this.metaStoreService.get(SymptomCheck, id)
		if(mode === 'edit')	this.symptomCheckEdit = await this.customSymptomCheckStoreService.get(id)

		// TODO: how to handle this case? :
		assert(this.symptomCheckEdit instanceof SymptomCheck, `SymptomCheckEditPageComponent.setup(): no matching symptom check found ${id}`)

	}

	/**
	 * Store {@link Question | Questions}, that might have been created on the
	 * fly while editing the {@link SymptomCheck}.
	 */
	public async storeOnTheFlyQuestions(questions: Question[]) : Promise<void> {

		await Promise.all(questions.map( async question => {

			const store = this.questionnaireService.getStore(question)

			// question already stored somewhere:
			if(store) return

			await this.customQuestionStoreService.addQuestionConfig(question.config)

		}))

	}

	/**
	 * Store the supplied {@link SymptomCheck}. If ``this.mode`` is ``'new'`` or ``'from'``, store it
	 * with {@link CustomSymptomCheckStoreService}; if ``this.mode`` is ``'edit'`` update the original symptomCheck.
	 */
	public async storeSymptomCheck(symptomCheck: SymptomCheck): Promise<SymptomCheck>{

		const config = symptomCheck.config

		if(this.mode === 'new' || this.mode === 'from'){

			if(config.id) config.id = undefined // making sure we add a copy of symptomCheck and won't overwrite the original one.

			const newSymptomCheck 	= await this.customSymptomCheckStoreService.addSymptomCheckConfig(config)

			return newSymptomCheck
		}

		if(this.mode === 'edit'){

			this.symptomCheckEdit.config = config
			this.symptomCheckEdit.update()

			return this.symptomCheckEdit
		}

		throw new Error(`SymptomCheckEditPageComponent.storeSymptomCheck() unknown mode: ${this.mode}.`)
	}

	public async onApply(result: ItemEditResult<SymptomCheck, Question[]>): Promise<void> {

		const symptomCheck 	= result.item
		const questions		= result.artifacts

		// Store all custom questions that may have been added on the fly
		try {		await	this.storeOnTheFlyQuestions(questions) }
		catch(e){	void	this.rccToastController.failure('CUSTOM_SYMPTOM_CHECK_STORE.EDIT.SAVE_QUESTIONS_FAILURE'); throw e }


		try{		await 	this.storeSymptomCheck(symptomCheck) }
		catch(e){	void	this.rccToastController.failure('CUSTOM_SYMPTOM_CHECK_STORE.EDIT.SAVE_FAILURE'); throw e }

		this.location.back()

		void this.rccToastController.success('CUSTOM_SYMPTOM_CHECK_STORE.EDIT.SAVE_SUCCESS')

	}



	public onCancel(): void {

		this.location.back()

		throw new UserCanceledError()

	}


}
