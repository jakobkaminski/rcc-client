import 	{
			NgModule
		} 											from '@angular/core'

import	{
			RouterModule,
		}											from '@angular/router'

import	{
			SymptomCheck
		}											from '@rcc/core'
import	{
			SharedModule,
			DevModule,
			provideItemAction,
			ItemAction,
			provideTranslationMap,
		}											from '@rcc/common'

import	{
			CustomQuestionStoreServiceModule
		}											from '../../questions'

import	{
			ScheduleModule
		}											from '../../schedules'

import	{
			SymptomCheckEditModule
		}											from '../edit'

import	{
			SymptomCheckMetaStoreServiceModule
		}											from '../meta-store'

import	{	SymptomCheckEditPageComponent	}		from './edit-page/symptom-check-edit-page.component'

import	{	CustomSymptomCheckStoreService			}		from './custom-symptom-check-store.service'

import	en	from './i18n/en.json'
import	de	from './i18n/de.json'


const routes 		=	[
							{
								path: 'symptom-checks/custom',
								children: [
									{
										path: 'new',
										component: SymptomCheckEditPageComponent,
										data: { mode:'new' }

									},
									{
										path: 'edit/:id',
										component: SymptomCheckEditPageComponent,
										data: { mode:'edit' }

									},
									{
										path: 'from/:id',
										component: SymptomCheckEditPageComponent,
										data: { mode:'from' }
									}
								]
							}
						]


const itemActions : ItemAction<SymptomCheck>[]	= 	[
	{
		itemClass:		SymptomCheck,
		storeClass:		CustomSymptomCheckStoreService,
		role:			'destructive' as const,
		getAction:		(symptomCheck: SymptomCheck, store: CustomSymptomCheckStoreService) => ({

			label: 			'CUSTOM_SYMPTOM_CHECK_STORE.ACTIONS.DELETE.LABEL',
			icon:			'delete',
			successMessage:	'CUSTOM_SYMPTOM_CHECK_STORE.ACTIONS.DELETE.SUCCESS',
			failureMessage:	'CUSTOM_SYMPTOM_CHECK_STORE.ACTIONS.DELETE.FAILURE',
			confirmMessage:	'CUSTOM_SYMPTOM_CHECK_STORE.ACTIONS.DELETE.CONFIRM',
			handler: 		() => store.deleteSymptomCheck(symptomCheck),

		})

	},
	{
		itemClass:		SymptomCheck,
		storeClass:		CustomSymptomCheckStoreService,
		role:			'edit' as const,
		getAction:		(symptomcheck: SymptomCheck) => ({

			label: 			'CUSTOM_SYMPTOM_CHECK_STORE.ACTIONS.EDIT',
			path:			'/symptom-checks/custom/edit/'+symptomcheck.id,
			icon:			'edit'

		})
	},
	{
		itemClass:		SymptomCheck,
		role:			'productive' as const,
		getAction:		(symptomcheck: SymptomCheck) => ({

			label: 			'CUSTOM_SYMPTOM_CHECK_STORE.ACTIONS.CREATE_NEW_FROM',
			path:			'/symptom-checks/custom/from/'+symptomcheck.id,
			icon:			'create-from'

		})
	}
]

const metaActions		=	[
								{
									label:			'CUSTOM_SYMPTOM_CHECK_STORE.ACTIONS.CREATE',
									role:			'productive' as const,
									icon:			'new',
									path:			'/symptom-checks/custom/new'
								}
							]

@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		SymptomCheckEditModule,
		SymptomCheckMetaStoreServiceModule.forChild([CustomSymptomCheckStoreService], metaActions),
		DevModule.note('CustomSymptomCheckStoreServiceModule'),
		CustomQuestionStoreServiceModule,
		ScheduleModule,
	],
	declarations: [
		SymptomCheckEditPageComponent,
	],
	exports: [
		SymptomCheckEditPageComponent,
	],
	providers:[
		...itemActions.map(provideItemAction),
		CustomSymptomCheckStoreService,
		provideTranslationMap('CUSTOM_SYMPTOM_CHECK_STORE', { en, de })
	]
})
export class CustomSymptomCheckStoreServiceModule { }
