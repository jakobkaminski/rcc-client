import { NgModule } from '@angular/core'
import { ItemRepresentation, ProductOrFactory, SharedModule, provideItemRepresentation } from '@rcc/common'
import { SymptomCheck } from '@rcc/core'
import { SymptomCheckLabelComponent } from './basic-symptom-check-label/item-label.component'

const basicSymptomCheckRepresentation: ProductOrFactory<ItemRepresentation>
							=	{
									itemClass:			SymptomCheck,
									icon:				'symptom_check',
									labelComponent:		SymptomCheckLabelComponent,
								}

@NgModule({
	imports: [
		SharedModule,
	],
	providers: [
		provideItemRepresentation(basicSymptomCheckRepresentation),
	],
	declarations: [
		SymptomCheckLabelComponent,
	]
})
export class BasicSymptomCheckRepresentationModule {}
