import 	{	ModuleWithProviders,
			NgModule,
			Type
		}											from '@angular/core'
import	{
			BaseMetaStoreModule,
			MetaAction
		}											from '@rcc/common'
import	{
			GlossaryEntry,
			GlossaryEntryConfig,
			GlossaryEntryStore
		}											from '@rcc/core/src/items/glossary-entry'
import	{
			GLOSSARY_ENTRY_META_ACTIONS,
			GLOSSARY_ENTRY_STORES
		}											from './curated-glossary-store.commons'
import	{	CuratedGlossaryEntryStoreService	}	from './curated-glossary-store.service'
@NgModule({
	providers:[
		CuratedGlossaryEntryStoreService,
	]
})
export class CuratedGlossaryEntryStoreServiceModule extends BaseMetaStoreModule {

	public static 	forChild(
		stores			: Type<GlossaryEntryStore>[],
		metaActions?	: MetaAction<GlossaryEntryConfig, GlossaryEntry, GlossaryEntryStore>[]
	): ModuleWithProviders<CuratedGlossaryEntryStoreServiceModule> {

	return BaseMetaStoreModule.getModuleWithProviders<CuratedGlossaryEntryStoreServiceModule>(this, stores, metaActions, GLOSSARY_ENTRY_STORES, GLOSSARY_ENTRY_META_ACTIONS)

	}
}
