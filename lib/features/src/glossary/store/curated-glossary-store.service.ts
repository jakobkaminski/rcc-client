import	{	Injectable				}	from '@angular/core'
import	{
			ItemStorage,
		}								from '@rcc/core'
import
		{
			GlossaryEntry,
			GlossaryEntryConfig,
			GlossaryEntryStore
		}								from '@rcc/core/src/items/glossary-entry'



@Injectable()
export class CuratedGlossaryEntryStoreService extends GlossaryEntryStore {

	public readonly name 	: string = 'GLOSSARY.XXX'

	public constructor(){
		super(staticStorage)
	}
}

export const 	glossaryCategories : string[]= [
					'rcc-glossary-category-default',
					'rcc-glossary-category-hcp',
					'rcc-glossary-category-pat',
				]


const staticStorage : ItemStorage<GlossaryEntry> = { getAll: () => Promise.resolve(configs) }

const configs:GlossaryEntryConfig[] = [			// TODO: remove

	{
		language: 'de',
		title: 'Adhärenz (Compliance)',
		lastUpdate: '2023-08-11',
		content: [
			'Was bedeuten die Begriffe Adhärenz und Compliance? Grob übersetzt könnte man bei Compliance von Behandlungstreue sprechen, also dem Befolgen einer ärztlichen Einschätzung. Die Patient:in hält sich somit an die Richtlinien, die Psychiater:innen oder Ärzt:innen vorschlagen.',

			'Das Wort Compliance ist in die Kritik geraten, weil es auch bedeuten kann, dass die Patient:in nur Dinge abnicken soll, die von den Behandelnden empfohlen werden. Gerade eine kritische Haltung, bei der man Dinge hinterfragt, die eine Fachperson äußert und somit nicht compliant ist, kann jedoch manchmal sinnvoll erscheinen.',
			'Wer sich selbst mit seiner Diagnose auseinandergesetzt und sich darüber informiert hat, ist ein Experte bzw. eine Expertin aus eigener Erfahrung auf diesem Gebiet. Mut zur Kritik an gängigen Lehrmeinungen und das Einbringen der eigenen Erfahrung und Meinung zu einem Behandlungskonzept ist in einer Akutphase besonders schwierig, denn in einer solchen Situation auch noch kritisch zu denken und Dinge zu hinterfragen, ist viel verlangt.',

			'Ziel bleibt es, einer Ärztin oder einem Arzt auf Augenhöhe zu begegnen und wechselseitig zu erörtern, welche nächsten Schritte in der Behandlung sinnvoll erscheinen. Da Compliance unter vielen krisenerfahrenen Menschen aus derartigen Gründen keinen guten Ruf genießt, ist es sinnvoll zu schauen, wie das Verhältnis zu den Behandelnden gut und ohne großes Machtgefälle gestaltet werden kann. Der neuere Begriff, der das Konzept Compliance ersetzt, lautet Adhärenz. Damit meint man, dass sich sowohl behandelte Personen als auch Behandler:innen an den vereinbarten Therapieplan und die Absprachen halten.',

			'Das sind keine unwichtigen Detailfragen, denn dieser Begriffswechsel hat das Zeug, das Verhältnis zwischen Patient:innen und Ärzt:innen entscheidend zu verbessern und einen vertrauensvollen Umgang miteinander zu ermöglichen. Ob diese beiden Fachbegriffe bei krisenerfahrenen Menschen hängen bleiben, ist nicht entscheidend. Wichtig ist vielmehr, ein Vertrauensverhältnis aufzubauen und ein Konzept zur weiteren erfolgreichen Behandlung zu entwickeln, zu Themen wie passgenauer Medikation und weiterführender Therapieangebote. Denn dieses Vertrauen trägt zu einer bestmöglichen Behandlung auf Augenhöhe bei.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Adhärenz', 'Compliance']
	},
	{
		language: 'en',
		title: 'Adherence (Compliance)',
		lastUpdate: '2023-08-11',
		content: [
			'What do the terms adherence and compliance mean? Roughly translated, compliance could mean to what degree one is following a prescribed treatment, i.e. following a doctor\'s advice. The patient thus is sticking to the guidelines suggested by psychiatrists or physicians.',
			'The word compliance has recently come under criticism because it can also imply that the patient should only nod to things recommended by those treating them. However, it is precisely a critical attitude, in which one questions things that a specialist says and communicates with them that is most sensible, and thus possibly “not compliant”.',
			'Anyone who has come to terms with his or her own diagnosis and informed himself or herself about it is an expert in this field from his or her own experience. Having the courage to criticize current doctrines and to express one\'s own experience and opinion about a treatment concept is particularly difficult in an acute phase because thinking critically and questioning things in such a situation is a lot to ask.',
			'The goal remains to meet a doctor at eye level and to discuss mutually which next steps in treatment seem sensible. Since compliance does not have a good reputation among many people in crisis for such reasons, it makes sense to look at how the relationship with those treating them can be shaped well and without power imbalance. The newer term replacing the concept of compliance is adherence. This means that both the person being treated and the person treating them adhere to the agreed treatment plan and arrangements.',
			'These are not unimportant matters of trivial detail because this change of terminology has what it takes to decisively improve the relationship between patients and physicians. This change thus enables them to communicate with each other with a spirit of trust. Whether these two technical terms stick with crisis-experienced people is not decisive. It is much more important to build up a relationship of trust and to develop a concept for further successful treatment, on topics such as appropriate medication and further therapy offers. This trust and open communication contributes to the best possible treatment at eye level.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat', 'Adherence', 'Compliance']
	},
	{
		language: 'de',
		title: 'Akzeptanz',
		lastUpdate: '2023-08-11',
		content: [
			'Ein Leben mit Einschränkungen zu akzeptieren und eine bestimmte Diagnose anzunehmen, ist ein wichtiger Schritt auf dem Weg zur Genesung. Diese Aussage lässt sich leicht in Worte fassen, denn in der Theorie mag es gut nachvollziehbar und logisch erscheinen. Gleichzeitig ist es in der Praxis ein langer, schwieriger und oftmals schmerzhafter Prozess, der viele Jahre benötigen kann. Akzeptanz ist dennoch eine notwendige Voraussetzung, um seine eigene Situation langfristig und dauerhaft verbessern zu können.',
			'Die damit verbundenen Gefühle sind sehr individuell und unterschiedlich und somit gibt es kein Rezept, um zu beschreiben, wie ein solcher Prozess ablaufen kann und wie viel Zeit er benötigt. Es ist herausfordernd und kann auch mit Trauer verbunden sein. Diese zuzulassen ist Teil des Prozesses, der verschiedene Phasen umfasst. Auf dem Weg der Verarbeitung kann eine psychotherapeutische Begleitung sinnvoll sein.',
			'Eine schwerwiegende Diagnose zu akzeptieren bedeutet auch, traumatische Erlebnisse zu verarbeiten und in sein eigenes Selbstbild zu integrieren. Damit zusammen hängt auch die Frage, was man Neues kreieren kann, anstatt am alten Selbstbild festzuhalten. Also den Blick auf die eigenen Fähigkeiten und Ressourcen zu lenken, die trotz Krankheit oder Krise noch immer da sind. Eine Krise kann auch eine Chance sein, wenn man bereit ist, etwas Altes loszulassen.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Akzeptanz']
	},
	{
		language: 'en',
		title: 'Acceptance',
		lastUpdate: '2023-08-11',
		content: [
			'Accepting a life with limitations and accepting a certain diagnosis is an important step on the road to recovery. This statement is easy to put into words, because in theory it may seem well understood and logical. At the same time, in practice, it is a long, difficult, and often painful process that can take many years. Acceptance is nevertheless a necessary prerequisite for being able to improve one\'s own situation in the long term and in the end permanently.',
			'The feelings involved are very individual and different and thus there is no recipe to describe how such a process can happen and how much time it takes. It is challenging and can also involve grief. Allowing this to happen is part of the process, which involves various stages. Psychotherapeutic support can be useful on the path of processing.',
			'Accepting a serious diagnosis also means coming to terms with traumatic experiences and integrating them into one\'s own self-image. Related to this is the question of what one can create new instead of holding on to the old self-image. In other words, by focusing on one\'s own abilities and resources, which are still there despite illness or crisis help support an integrated self-image. If you are willing, a crisis can even be an opportunity to let go of something old or burdening.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat', 'Acceptance']
	},
	{
		language: 'de',
		title: 'Angehörige',
		lastUpdate: '2023-08-11',
		content: [
			'Warum ist es wichtig, auch Angehörige in die Behandlung mit einzubeziehen? Aus einer systemischen Perspektive, einer bestimmten Form der Psychotherapie, heraus betrachtet, ist die erkrankte Person in ein Umfeld eingebettet. Bezugspersonen, wie z.B. Freunde und Familie spielen sowohl in Hinblick auf die Erkrankung, als auch bei der Genesung eine wichtige Rolle. Dies ist wichtig, in den Fokus zu rücken, denn gerade eine positiv erlebte Kommunikation mit dem eigenen Umfeld kann maßgeblich zur eigenen Genesung beitragen. Ihrer Mitwirkung kommt somit eine hohe Wirksamkeit zu.',
			'Leider werden Angehörige im psychiatrischen System oftmals nicht genügend gehört. Es ist weiterhin zu beachten, dass Freunde oder Familie selbst Gefahr laufen, ebenfalls zu erkranken. Abgrenzung und das Beachten eigener Bedürfnisse ist daher umso wichtiger. Manche Angehörige neigen zudem dazu, sich überaus stark emotional zu engagieren. Hierbei ist es wichtig, sich selbst immer wieder daran zu erinnern, dass man als Angehörige keine Ärztin oder Therapeutin ist und nicht die Profi-Rolle übernimmt.',
			'Manche Angehörige neigen zudem dazu, die Schuld für eine Erkrankung bei sich selbst zu suchen. Umso wichtiger ist es, sich zu vernetzen und Hilfe und Unterstützung zu organisieren. Hier kommen Angehörigenverbände ins Spiel. Oftmals ist der Austausch mit anderen Personen, die ähnliche Erlebnisse gemacht haben, im Rahmen von Selbsthilfegruppen oder durch Beratungsgespräche, sehr hilfreich.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Angehörige']
	},
	{
		language: 'en',
		title: 'Relatives',
		lastUpdate: '2023-08-11',
		content: [
			'Why is it important to include relatives within treatment? From a systemic perspective, which is important to a particular form of psychotherapy (Systemic Therapy), the focus of the therapy is on the social environment the affected person is embedded in. People like friends and family play an important role both in terms of the illness and in recovery. It is important to focus on this because positive communication with one\'s own environment can contribute significantly to one\'s own recovery. Their cooperation is therefore highly effective.',
			'Unfortunately, relatives are often not sufficiently heard in the psychiatric system. It should also be noted that friends or family themselves run the risk of also becoming ill. Distinction and attention to one\'s own needs are therefore all the more important. Some relatives even tend to become overly emotionally involved. Here it is important to remind oneself again and again that one is not a doctor or therapist and does not take on the role of a professional.',
			'Some relatives may turn to blame themselves for an illness. This makes it all the more important to network and organize help and support. This is where relatives\' associations come into play. Often, the exchange with other people who have had similar experiences is very helpful especially in the context of self-help groups or through counseling sessions.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat', 'Relatives']
	},
	{
		language: 'de',
		title: 'Antidepressiva',
		lastUpdate: '2023-08-11',
		content: [
			'Antidepressiva sind Medikamente, die in die Wirkung von Botenstoffen (Neurotransmittern) im Gehirn eingreifen und helfen sollen, Symptome einer Depression abzuschwächen. Anders als der Name “Antidepressiva” nahelegt, können Antidepressiva leider die Veränderungen im Stoffwechsel des Gehirns bei einer Depression nicht aufheben, sondern verändern den Stoffwechsel auf unterschiedliche Weise, je nach Art des Antidepressivums. Aus der Stoffwechselwirkung leiten sich auch die unterschiedlichen Gruppen von Antidepressiva ab (z.B. Serotonin-Wiederaufnahme-Hemmer, oft als SSRI abgekürzt). Je nach Art des Antidepressivums unterscheiden sich die möglichen Nebenwirkungen.',
			'Leider ist es aktuell nicht möglich, vorauszusagen, bei wem welches Antidepressivum gut wirksam ist oder bei wem es Nebenwirkungen hervorruft. Antidepressiva sind bezüglich ihrer Wirksamkeit umstritten. Es besteht jedoch Konsens, dass sie bei leichten Depressionen eher nicht eingesetzt werden sollten, sondern nur bei mittelschweren und schweren Depressionen. Üblicherweise werden Antidepressiva zur Behandlung einer akuten Depression begonnen und nach dem Abklingen der Depression für eine Zeit weiter eingenommen, um das Wiederauftreten von erneuten Depressionen zu vermeiden.',
			'Nach Beginn einer Therapie mit Antidepressiva tritt die Wirkung mit einer Verzögerung von 1-3 Wochen auf. Antidepressiva können -> unerwünschte Arzneimittelwirkungen haben und die verschreibende Ärzt:in muss dich hierüber aufklären. Werden Antidepressiva abgesetzt, kann es zu -> Absetzsymptomen kommen. Antidepressiva werden nicht nur bei Depressionen eingesetzt, sondern auch unter anderem bei Angststörungen, Panikattacken, Zwangsstörungen oder Schlafstörungen.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Antidepressiva']
	},
	{
		language: 'en',
		title: 'Antidepressants',
		lastUpdate: '2023-08-11',
		content: [
			'Antidepressants are drugs that modulate neurotransmitters in the brain and are intended to help reduce symptoms of depression. Contrary to what the name "antidepressants" suggests, antidepressants unfortunately cannot reverse the molecular changes in the brain affected by depression, but rather modulate neurotransmitter effects in different ways, depending on the type of antidepressant. The different groups of antidepressants (e.g. serotonin reuptake inhibitors, often abbreviated as SSRIs) are also derived from the effect on neurotransmitters. Depending on the type of antidepressant, the possible side effects differ.',
			'Unfortunately, it is currently not possible to predict which antidepressant will be effective for whom or if it will cause side effects. Antidepressants are controversial in terms of their effectiveness. However, there is consensus that they should rather not be used for mild depression, but only for moderate and severe depression. Typically, antidepressants are used to treat acute depression and use is continued for a period of time after the depression has subsided to prevent recurrence of depression.',
			'After starting a therapy plan with antidepressants, the effect appears with a delay of 1-3 weeks. Antidepressants can have adverse drug reactions and the prescribing physician must inform you about this. If the use of antidepressants is stopped, -> withdrawal symptoms may occur. Antidepressants are not only used for depression, but also for anxiety disorders, panic attacks, obsessive-compulsive disorders, and sleep disorders.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat', 'Antidepressants']
	},
	{
		language: 'de',
		title: 'Antipsychotika',
		lastUpdate: '2023-08-11',
		content: [
			'Unter Antipsychotika (synonym: Neuroleptika) versteht man Medikamente, die zur Behandlung von Psychosen eingesetzt werden. Einige der Medikamente sind auch zur Behandlung von bipolaren Erkrankungen und Depressionen zugelassen. Bei der Behandlung von Psychosen unterscheidet man die Akuttherapie (wenn der Betroffene in einer Psychose ist), von der Erhaltungstherapie (Stabilisierungsphase) und Rückfallvorbeugung (Rezidivprophylaxe). In der Akuttherapie werden häufig höhere Dosen von Antipsychotika eingesetzt als in der Rückfallvorbeugung.',
			'Man unterscheidet starke (hochpotente), mittelstarke (mittelpotent) und wenig starke (niedrigpotente) Antipsychotika. Außerdem werden die Medikamente danach unterschieden, an welcher Stelle (Rezeptor) sie im Gehirn wirken (zum Beispiel am Dopamin-Rezeptor). Üblicherweise werden Neuroleptika als Tabletten eingenommen, manche gibt es auch in flüssiger Form zum Schlucken oder als langwirksame Spritze (-> Depotmedikamente). Je nachdem, an welchem Rezeptor die Medikamente wirken, unterscheiden sich Antipsychotika auch in den möglichen Nebenwirkungen - hierüber muss dich dein:e Ärzt:in gut informieren.',
			'Leider ist es aktuell nicht möglich, vorauszusagen, bei wem welches Medikament gut wirksam ist oder bei wem es Nebenwirkungen hervorruft. Häufig gibt es auch eine gute Wirkung und gleichzeitig Nebenwirkungen - dann ist es wichtig, mit deiner verschreibenden Ärzt:in hierüber offen zu sprechen und gut gemeinsam abzuwägen (z.B. Ist es für mich okay, dass ich morgens noch müde vom Medikament bin, aber dafür weniger Ängste habe?).',
			'Achtung: Plötzliches Absetzen von Medikamenten kann einerseits zum Wiederauftreten von Psychosen, aber auch anderen unangenehmen Absetzsymptomen führen. Anpassungen der Medikamente sollten daher vorsichtig vorgenommen werden. Einige Medikamente erfordern außerdem regelmäßige Kontrollen von Blutwerten oder Überprüfung der Herzfunktion mittels EKG. Diese dienen deiner Sicherheit und es ist daher wichtig, diese Untersuchungen in Anspruch zu nehmen.',
			'Tipp: Bei der Einstellung der Medikamente kann es helfen, deine Symptome und auch Nebenwirkungen genau aufzuzeichnen und diese Informationen mit deiner Ärzt:in zu teilen. Insbesondere bei langsamen Anpassungen der Medikamente über einen langen Zeitraum kann das helfen, den Überblick zu behalten.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Antipsychotika']
	},
	{
		language: 'en',
		title: 'Antipsychotics',
		lastUpdate: '2023-08-11',
		content: [
			'Antipsychotics (synonym: neuroleptics) are drugs used to treat psychoses. Some of these drugs are also approved for the treatment of bipolar disorders and depression. In the treatment of psychoses, a distinction is made between acute therapy (when the person is in psychosis), maintenance therapy (stabilization phase), and relapse prevention (relapse prophylaxis). In acute therapy, higher doses of antipsychotics are often used than in relapse prevention.',
			'A distinction is made between strong (high-potency), medium-strong (medium-potency), and mild (low-potency) antipsychotics. The drugs are also classified according to the site (receptor) at which they act in the brain (for example, at the dopamine receptor). Neuroleptics are usually taken as tablets, but some are also available in liquid form for swallowing or as long-acting injections (-> depot medication). Depending on which receptor the drugs act on, antipsychotics also differ in their possible side effects - your doctor should inform you about this.',
			'Unfortunately, it is not currently possible to predict if a particular medication will be effective or if it will cause side effects for each individual. Often there is a good effect and at the same time side effects - then it is important to talk openly about this with your prescribing doctor and to weigh these things together (e.g. Is it okay for me that I am still tired in the morning from the medication, but have less anxiety?).',
			'Caution: sudden discontinuation of medication can lead to the recurrence of psychosis, but also to other unpleasant withdrawal symptoms. Adjustments to medication(s) should therefore be made carefully. Some medications also require regular blood checks or verification of heart function by ECG. These are for your safety, so it is important to take advantage of these checkups.',
			'Tip: During medication adjustments, it can be helpful to accurately record your symptoms as well as side effects and share this information with your doctor. This can help you keep track of your medications, especially if you are adjusting them slowly over a long period of time.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat', 'Antipsychotics']
	},
	{
		language: 'de',
		title: 'Behandler:innen und Berufsgruppen',
		lastUpdate: '2023-08-11',
		content: [
			'Natürlich spielen Psychiaterinnen und Psychiater eine wichtige Rolle bei der Krisenintervention und der Behandlung von psychischen Erkrankungen. Zu einer ganzheitlichen Behandlung gehören aber noch viele andere essentielle Berufsgruppen.',
			'Während aus Perspektive von Ärzt:innen Medikamente oftmals ein zentrales Thema in der Behandlung sind, haben weitere Berufsgruppen eine andere Sichtweise, die sie einfließen lassen können.',
			'Psychologische Psychotherapeut:innen schauen auf die inneren Prozesse. Das gesprochene Wort ist bei ihnen zentraler als die Beschäftigung mit Psychopharmaka.',
			'Sozialarbeitende wiederum haben ihre Kompetenz in der Vermittlung von passenden Anschlussbehandlungen und können bei schriftlicher Korrespondenz mit Behörden und der Organisation im Alltag behilflich sein.',
			'Pflegefachkräfte bringen eine andere Perspektive ein. Sie können die selbstständige Medikamenteneinnahme trainieren, in der Bewältigung der alltäglichen Tätigkeiten und achten auf die grundsätzliche Versorgung von Patient:innen.',
			'Ergotherapeutische Fachkräfte achten beispielsweise auf psychisches und körperliches Wohlbefinden und je nach Fachgebiet können sie viele Ressourcen reaktivieren.',
			'Genesungsbegleiter:innen fällt die zentrale Rolle zu, Hoffnung und Zuversicht zu vermitteln und ihre ganz spezifische Sicht auf die Behandlung auch ins Team einzubringen.',
			'Die Vorteile dieser vielen unterschiedlichen Perspektiven liegen in ihrer gegenseitigen Ergänzung. Die Vielstimmigkeit ermöglicht einen ganzheitlichen Ansatz, der verschiedene Aspekte anspricht. So wird die Zentrierung auf einen einzelnen Aspekt, wie z.B. die medikamentöse Therapie, vermieden und weitere Möglichkeiten werden aufgezeigt.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Treatment Providers', 'Professional Groups']
		},
		{
		language: 'en',
		title: 'Treatment Providers and Professional Groups',
		lastUpdate: '2023-08-11',
		content: [
			'Of course, psychiatrists play an important role in crisis intervention and the treatment of mental illness. However, many other professional groups are also involved and essential for a holistic treatment.',
			'From the perspective of many physicians, medications are often a central focus in treatment, while other professional groups have a different perspective that they can incorporate.',
			'Psychological psychotherapists look at the inner processes. The spoken word is more central to them than dealing with psychotropic drugs.',
			'Social workers, on the other hand, have their expertise in arranging appropriate follow-up treatment and can help with written correspondence with authorities and organization in everyday life.',
			'Nursing professionals bring a different perspective. They can teach patients to take their medications independently, deal with everyday tasks and they pay close attention to the basic care of patients.',
			'Occupational therapists, for example, pay attention to psychological and physical well-being and, depending on the specialty, can reactivate many resources.',
			'Experts with experience play a central role in conveying hope and confidence and in bringing their very specific view of treatment to the team.',
			'The advantages of these many different perspectives lie in their mutual complementarity. The polyphony enables a holistic approach that addresses different aspects. Thus, centering on a single aspect, such as drug therapy, is avoided and other approaches are revealed.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat', 'Treatment Providers', 'Professional Groups']
		},
		{
		language: 'de',
		title: 'Benzodiazepine',
		lastUpdate: '2023-08-11',
		content: [
			'Benzodiazepine sind häufig eingesetzte Medikamente zur Dämpfung von Angst und Anspannung. Sie werden außerdem bei schweren Depressionen, zur Behandlung von Alkoholentzugssymptomen und bei Menschen mit ausgeprägten Suizidgedanken eingesetzt. Benzodiazepine werden übrigens auch in der Neurologie und Anästhesie eingesetzt. Sie werden zu der Medikamentengruppe der Anxiolytika (griechisch: Angstlöser) oder auch Tranquilizer (Beruhigungsmittel) gezählt. Beispiele für Benzodiazepine sind Lorazepam (Handelsname Tavor), Diazepam (Valium), Alprazolam (Xanax).',
			'Der große Vorteil dieser Medikamente ist die sehr rasche und häufig als angenehm beschriebene Wirkung, vor allem wenn die Symptome sehr ausgeprägt sind. Kurzfristig können Benzodiazepine zu Konzentrations- und Merkfähigkeitsstörungen, Schläfrigkeit und Stürzen, vor allem bei älteren Menschen, führen. Bei längerer Einnahme kann es zu Gewöhnung oder sogar Abhängigkeit von Benzodiazepinen kommen (bei Menschen, die Benzodiazepine über einen Monat nehmen, zeigt die Hälfte eine Gewöhnung). Wenn man sich an die Wirkung gewöhnt hat, kann es bei plötzlichem Absetzen zu schweren Entzugssymptomen und sogar lebensbedrohlichen Delirien und Krampfanfällen kommen.',
			'Zusammenfassend kann man sagen, dass es kurzfristig sehr wirksame Medikamente sind, bei denen dein:e verschreibende:r Behandler:in und du sehr darauf achten müsst, dass du sie nicht zu lange einnimmst. Wichtig: Sollte bei dir bereits eine Gewöhnung an das Medikament bestehen, solltest du medizinische Hilfe in Anspruch nehmen, um zu prüfen, wie du schrittweise die Einnahme beenden kannst. Eine Gewöhnung kannst du z.B. daran erkennen, dass die gleiche Dosis nicht mehr die gleiche Wirkung entfaltet und Du bei Nicht-Einnahme Entzugssymptome wie Angst, Unruhe oder Kopfschmerzen verspürst. Dann solltest Du unbedingt die gleiche Dosis einnehmen und dringend medizinische Hilfe in Anspruch nehmen.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Benzodiazepines', 'Medication', 'Anxiety', 'Depression']
		},
		{
		language: 'en',
		title: 'Benzodiazepines',
		lastUpdate: '2023-08-11',
		content: [
			'Benzodiazepines are commonly used medications to dampen anxiety and tension. They are also used for severe depression, to treat alcohol withdrawal symptoms, and for people with pronounced suicidal ideation. Benzodiazepines, by the way, are also used in neurology and anesthesia. They belong to the drug group of anxiolytics (Greek: anxiety relievers) or tranquilizers (sedatives). Examples of benzodiazepines are lorazepam (trade name Tavor), diazepam (Valium), alprazolam (Xanax).',
			'The great advantage of these drugs is that they have a very rapid effect, often described as pleasant, especially when symptoms are very pronounced. In the short term, benzodiazepines can cause impaired concentration and memory, drowsiness, and falls, especially in the elderly. Prolonged use can lead to habituation or even dependence on benzodiazepines (among people taking benzodiazepines for more than a month, half show habituation). Once one has become accustomed to the effects, sudden discontinuation can result in severe withdrawal symptoms and even seizures.',
			'In summary, these are very effective short-term medications that your prescriber and you should be very careful not to take for too long. Important: If you have already become accustomed to the medication, you should seek medical help to see how you can gradually stop taking it. You can recognize habituation by the fact that the same dosage does not have the same effect anymore and left out dosages lead to withdrawal symptoms such as anxiety, restlessness, or headaches. If this happens you need to immediately seek medical help.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat', 'Benzodiazepines', 'Medication', 'Anxiety', 'Depression']
		},
		{
		language: 'de',
		title: 'Diagnose',
		lastUpdate: '2023-08-11',
		content: [
			'Ob es wirklich klar unterscheidbare Diagnosen gibt, ist wissenschaftlich umstritten. Dennoch sind sie aus ärztlicher Sicht für die Einordnung und Kommunikation sehr nützlich. Eine Diagnose zu erhalten kann unterschiedliche Reaktionen hervorrufen. Auf der einen Seite ist es denkbar, dass eine betroffene Person zunächst einmal geschockt reagiert. Schließlich kann dies eine schwerwiegende Beeinträchtigung für das weitere Leben bedeuten. Andererseits kann die Verkündung der Diagnose auch mit einer Erleichterung einhergehen. Schon allein deswegen, da viele Personen, die sich an eine Ärzt:in oder einen sonstigen Behandler wenden, es bereits mit starken Einschränkungen zu tun haben. Insofern hat die Diagnose den Effekt, dass die vorhandenen Symptome nun erklärbar werden.',
			'Einmal diagnostiziert, kann eine zielgerichtete Unterstützung bzw. die passende Art der Behandlung in die Wege geleitet werden. Diese Art der Einordnung in Kategorien ist in unserem Gesundheitssystem notwendig, damit die entsprechenden Leistungen vergütet werden. Zudem weiß eine Psychiater:in aufgrund der Studienlage und ihrer Erfahrungen, welche Art von Umgang bei anderen Patient:innen hilfreich ist. Wichtig ist jedoch, dass eine Person nicht auf ihre Diagnose reduziert wird und Verallgemeinerungen vorgenommen werden. Schließlich sind wir alle mehr als unsere Diagnosen und möchten ganzheitlich wahrgenommen werden.',
			'Damit eine kürzlich diagnostizierte Person besser nachvollziehen kann, welche nächsten Schritte für die Behandlung wichtig sind, ergibt es Sinn, sich selbst über die krisenhafte Situation zu informieren. Man spricht auch davon, zur Expertin bzw. zum Experten der eigenen Erkrankung zu werden. Dadurch wird ein besseres oder tieferes Verständnis der Krise möglich. Manche Belastungen oder Verhaltensweisen können somit gelindert oder verändert werden und ein positiver Umgang mit der eigenen Diagnose wird dadurch wahrscheinlicher. Nicht zuletzt kann jemand, der oder die über das notwendige Fachwissen verfügt, selbstbestimmt über die Wahl der angedachten Therapiemöglichkeiten entscheiden.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Diagnosis', 'Mental Health', 'Treatment', 'Holistic Care']
		},
		{
		language: 'en',
		title: 'Diagnosis',
		lastUpdate: '2023-08-11',
		content: [
			'Whether there are really clearly distinguishable diagnoses is scientifically disputed. Nevertheless, from a medical point of view, they are very useful for communication and classification. Receiving a diagnosis can cause different reactions. On the one hand, it is conceivable that an affected person will initially react in shock. After all, this can mean a serious impairment for their future life. On the other hand, the announcement of the diagnosis may also be accompanied by relief. This is due to the fact that many people who turn to a doctor or therapist are already dealing with severe limitations. In this respect, the diagnosis means that the existing symptoms can now be explained.',
			'Once diagnosed, targeted support or the appropriate type of treatment can be initiated. This kind of categorization is necessary in our healthcare system so that the appropriate treatment is reimbursed. In addition, a psychiatrist knows from experience what kind of treatment has been helpful for other patients. However, it is important not to reduce a person to their diagnosis and make generalizations. After all, we are all more than our diagnoses and want to be perceived holistically.',
			'In order for a recently diagnosed person to better understand which next steps are important for treatment, it makes sense to inform oneself about the crisis situation. This is also referred to as becoming an expert on one\'s own illness. This enables a better or deeper understanding of the crisis. Some stress or behaviors can thus be alleviated or changed, and a positive approach to one\'s own diagnosis becomes more likely. Last but not least, someone who has the necessary expertise can make a self-determined decision about the choice of treatment options to be considered.'
		],
		authors: [
			'Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)',
			'Dennis Stratmann (Peer Worker)'
		],
		tags: ['hcp', 'pat', 'diagnosis', 'medical', 'psychiatry', 'holistic approach']
		},
		{
		language: 'de',
		title: 'Einschränkungen',
		lastUpdate: '2023-08-11',
		content: [
			'Eine psychische Krise ist oftmals mit verschiedenen Einschränkungen im Alltag verbunden. Zum einen gibt es Nebenwirkungen, mit denen man zurechtkommen muss, insofern man sich für die Einnahme von Psychopharmaka entschieden hat. Zum anderen gibt es Einschränkungen durch die Symptome der jeweiligen Erkrankung, die den Alltag oftmals erschweren.',

			'Was ist konkret mit Einschränkungen gemeint? Es können körperliche Faktoren sein, wie zum Beispiel eine erhöhte Müdigkeit oder das Gefühl, weniger zu empfinden. Einschränkungen können auch die wahrgenommene eigene Leistungsfähigkeit umfassen. Womöglich ist jemand im Job nicht mehr genauso leistungsfähig wie vor der Krise. Häufig ist es gar nicht so einfach, diese beiden Arten von Einschränkungen voneinander zu trennen. In manchen Fällen hilft es bereits, über die jeweiligen Herausforderungen zu sprechen, um gemeinsam nach möglichen Lösungen zu suchen.',

			'Wenn es sich beispielsweise um finanzielle Einschränkungen handelt, da durch die Verhaltensweisen während einer Krise viel Geld ausgegeben wurde, kann eine Schuldnerberatung hilfreich sein. Wenn aufgrund mangelnden Antriebs die sozialen Kontakte stark eingeschränkt sind, kann beispielsweise mit einer Netzwerkkarte gearbeitet werden, um zu versuchen, aktivierende Verhaltensweisen einzuüben. In jedem Fall ist es sinnvoll, sich anzuvertrauen und die Einschränkungen, die man erlebt, zu thematisieren, denn nur auf diese Weise kann sich die Situation ändern.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'en',
		title: 'Restrictions',
		lastUpdate: '2023-08-11',
		content: [
			'A mental crisis is often associated with various restrictions in everyday life. On the one hand, there are side effects that one has to cope with, when one has decided to take psychotropic drugs. On the other hand, there are restrictions due to the symptoms of the respective illness, which often make everyday life more difficult.',

			'What specifically is meant by limitations? They can be physical factors, such as increased fatigue or dampened emotions. Limitations can also include one\'s perceived ability to perform. Someone may no longer be as capable at work as they were before the crisis. It is often not so easy to separate these two types of limitations. In some cases, it already helps to talk about the respective challenges in order to look for possible solutions together.',

			'For example, if it is a matter of financial limitations because a lot of money was spent due to the behaviors during a crisis, debt counseling can be helpful. If social contacts are severely limited due to a lack of drive, for example, a network map can be used to try to practice activating behaviors. No matter the situation, it makes sense to confide and address the limitations one is experiencing, because only in this way can the situation change.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'de',
		title: 'Gruppenpsychotherapie',
		lastUpdate: '2023-08-11',
		content: [
			'Psychotherapie kann neben der Einzeltherapie auch in Gruppen durchgeführt werden, was als Gruppenpsychotherapie bezeichnet wird. In der stationären (Klinik) oder teilstationären Behandlung (Tageskliniken) ist dies die häufigste Form der Therapie, während in der ambulanten Psychotherapie (in einer Psychotherapiepraxis) häufiger Einzelpsychotherapie angeboten wird.',

			'Gruppenpsychotherapie ist wahrscheinlich genauso wirksam wie Einzelpsychotherapie – somit nicht einfach eine billige Alternative. Die Gruppenpsychotherapie bietet den Vorteil, dass die Teilnehmer:innen von den Erfahrungen und Problemen anderer Menschen lernen und sich gegenseitig helfen können, mit Unterstützung und unter der Leitung einer Psychotherapeut:in.',

			'Wie bei der Einzelpsychotherapie gibt es sehr verschiedene Gruppenpsychotherapieformen: psychoanalytische, tiefenpsychologische, systemische oder verhaltenstherapeutische Gruppen. Man unterscheidet in der Gruppentherapie diagnosespezifische Gruppen (z.B. alle Teilnehmenden leiden an ähnlichen Problemen, z.B. Angststörungen) oder diagnoseübergreifende Gruppen (alle Teilnehmenden haben unterschiedliche Probleme).',

			'Bevor man in eine Psychotherapiegruppe aufgenommen wird, wird der/die Gruppenleiter:in mit dir alleine Vorgespräche führen. Tipp: Nutze diese Stunden, um dich gut zu informieren, warum die Therapeut:in dir eine Einzel- oder Gruppentherapie empfiehlt. Im Idealfall bist du dir beim Beginn einer Gruppentherapie bewusst, warum du an einer Gruppe teilnimmst, welche Ziele du hast, wie du diese in der Gruppe erreichen kannst und welche Schwierigkeiten oder Nebenwirkungen in deiner Gruppenpsychotherapie auftreten können. Solltest du dir gar nicht vorstellen können, über deine Schwierigkeiten in einer Gruppe zu sprechen, ist eine Gruppentherapie nicht sinnvoll.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'en',
		title: 'Group Therapy',
		lastUpdate: '2023-08-11',
		content: [
			'In addition to individual therapy, psychotherapy can also be conducted in groups, which is referred to as group psychotherapy. In inpatient (clinic) or day-care (day hospitals) treatment, this is the most common form of therapy, while in outpatient psychotherapy (in a psychotherapy practice), individual psychotherapy is offered more often.',

			'Group psychotherapy is probably as effective as individual psychotherapy - thus not simply a cheap alternative. Group psychotherapy offers the advantage that participants can learn from each other\'s experiences and problems and support each other, with the support and guidance of a psychotherapist.',

			'As with individual psychotherapy, there are very different forms of group psychotherapy: psychoanalytic, depth psychological, systemic, or behavioral therapy groups. A distinction is made in group therapy between diagnosis-specific groups (e.g. all participants suffer from similar problems, e.g. anxiety disorders) or cross-diagnosis groups (all participants have different problems).',

			'Before being accepted into a psychotherapy group, the group leader will have preliminary discussions with you alone. Tip: Use these hours to get a good idea of why the therapist is recommending individual or group therapy. Ideally, when you begin group therapy, you will be aware of why you are participating in a group, what your goals are, how you can achieve them in the group, and what difficulties or side effects may occur in your group psychotherapy. If you cannot even imagine talking about your difficulties in a group, group therapy is not useful.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist Psychiatry)', 'Dennis Stratmann (Recovery Companion)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'de',
		title: 'Krisenerfahrung',
		lastUpdate: '2023-08-11',
		content: [
			'Man spricht von Krisenerfahrung, um den Wert, den eine krisenhafte Phase auch beinhaltet, zu betonen. Hier lohnt ein genauer Blick auf die Wortwahl. Psychiatrisches Fachvokabular ist gelegentlich schwer verständlich und häufig auf eine Problemstellung fokussiert, sodass es selten positiv stimmende oder selbstbestimmte Begriffe mit einer positiven Konnotation beinhaltet. Im Gegenteil wird diese Art der Fachsprache von manchen Patient:innen als abwertend wahrgenommen. Hier geht es um eine Defizitorientierung. Dies ist erwähnenswert, da Sprache eine große Bedeutung im Hinblick auf die Bedeutung für den eigenen Selbstwert und die Identität einer Person spielt.',

			'Auch der Einsatz von Umgangssprache, die Patient:innen leichter fallen mag, als defizitorientiertes Fachvokabular, ist eine Möglichkeit. In jedem Fall ist es eine individuelle Entscheidung und jede krisenerfahrene Person wird sich für eine andere Auswahl von Begriffen entscheiden.',

			'Um abwertende oder diskriminierende Worte zu vermeiden, ist es möglich, ganz bewusst weniger geläufige und sehr ausgewählte Begrifflichkeiten zu nutzen, also eine Art von Fachsprache. Man spricht auch von Ressourcenorientierung. Dabei kann die eigene Krise bestenfalls als Chance wahrgenommen werden. Das eigene Erleben wird in Form von Erfahrungswissen zu einer Art Schatz, den es zu erkunden und zu bergen gilt. Es geht darum, die eigenen Erlebnisse zu verarbeiten und zu reflektieren, um gestärkt aus einer Krise hervorzugehen.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'en',
		title: 'Crisis Experience',
		lastUpdate: '2023-08-11',
		content: [
			'One speaks of crisis experience to emphasize the value that a crisis phase also contains in terms of potential learnings and growth. Here it is worth taking a closer look at the choice of words. Psychiatric vocabulary is sometimes difficult to understand and often focuses on a problem, so it rarely includes positive-voiced or self-determined terms. On the contrary, this type of technical language is perceived by some patients as pejorative. This is about deficit orientation. This is worth mentioning, since language plays a major role in terms of importance for a person\'s own self-worth and identity.',

			'The use of colloquial language, which patients may find easier than deficit-oriented technical vocabulary, is also a possibility. In each case, it is an individual decision and each person experiencing a crisis will choose a different set of terms for themselves.',

			'In order to avoid pejorative or discriminatory words, it is possible to consciously use less common and very selected terminology, i.e. a kind of specialized language. One also speaks of resource orientation. At best, one\'s own crisis can be perceived as an opportunity. One\'s own experience, in the form of experiential knowledge, becomes a kind of treasure to be explored and recovered. It is a matter of processing and reflecting on one\'s own experiences in order to emerge strengthened from a crisis.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'de',
		title: 'Lithium',
		lastUpdate: '2023-08-11',
		content: [
			'Lithium ist ein Alkalimetall und wird zur Behandlung von bipolaren Erkrankungen und Depressionen behandelt. Bei Menschen mit bipolarer Erkrankung wird Lithium vor allem für die Verhinderung erneuter manischer oder depressiver Phasen eingesetzt, d.h. es senkt das Risiko, erneut krank zu werden. Auch zur Behandlung einer manischen Phase kann Lithium eingesetzt werden. Bei Depressionen wird Lithium eingesetzt, wenn Patient:innen auf eine Therapie mit Antidepressiva nicht ausreichend ansprechen (sog. Augmentationsbehandlung). Lithium wird außerdem verschrieben, wenn Patient:innen starke Suizidgedanken im Rahmen einer der beiden Erkrankungen haben.',

			'Obwohl die Wirksamkeit von Lithium gut belegt ist, wird es in der Praxis seltener eingesetzt - das liegt vor allem daran, dass bei der Lithiumtherapie einiges an Vorsichtsmaßnahmen eingehalten werden muss. Im Vergleich zu anderen Medikamenten ist der therapeutische Bereich zwischen der Behandlungsdosis und einer giftigen Dosis nicht so groß. Daher muss gerade in der Neueinstellung auf Lithium häufig im Blut gemessen werden, wie viel Lithium im Körper ist. Es gibt bei Lithium keine Standarddosis, wie bei anderen Medikamenten, sondern die Dosis muss individuell eingestellt werden. Lithium wird über die Niere, also über den Urin, aus dem Körper wieder ausgeschieden. Die Ausscheidung kann auch stark schwanken, je nachdem, wie gut die Niere Urin produziert und wie viel Flüssigkeit man trinkt, daher muss auf eine ausreichende regelmäßige Flüssigkeitsaufnahme geachtet werden.',

			'Vor allem bei Flüssigkeitsverlust (Erbrechen, Durchfälle und starkes Schwitzen) muss auf ausreichenden Ausgleich geachtet werden, sodass kein zu hoher Anteil Lithium in Deinem Köper ist. Vergiftungswarnzeichen an denen Du bemerkst, dass Du zuviel Lithium im Körper hast sind:',

			'Schwindel',
			'Übelkeit, Erbrechen, Durchfall, Magenschmerzen, Appetitlosigkeit',
			'Zittern der Hände, Gangstörungen, Schwierigkeiten zu Sprechen',
			'Schläfrigkeit, Abgeschlagenheit, Verlangsamung',
			'Bei einem schweren Verlauf treten eine Muskelsteifigkeit, epileptische Anfälle, Verminderte Harnausscheidung mit Niereninsuffizienz hinzu.',

			'Es ist immer wichtig, gut über die Medikamente, die man nimmt, Bescheid zu wissen - bei Lithium ist es jedoch besonders wichtig! Sollte dir eine Behandlung mit Lithium vorgeschlagen werden, ist es daher besonders wichtig, dass dich deine behandelnde Ärzt:in umfassend informiert und du nachfragst! Du solltest genau über Dosierung, Gründe für die Behandlung, mögliche Nebenwirkungen, Vergiftungswarnzeichen und notwendige Sicherheitsmaßnahmen informiert sein. Stelle außerdem sicher, dass die Ärzt:in, die dir Lithium verschreibt, immer informiert ist, welche anderen Medikamente du noch nimmst, damit sie Wechselwirkungen mit anderen Medikamenten einschätzen kann. Im Falle einer geplanten oder bestehenden Schwangerschaft solltest du ebenfalls deine verschreibende Ärzt:in dringend hierüber informieren.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'en',
		title: 'Lithium',
		lastUpdate: '2023-08-11',
		content: [
			'Lithium is an alkali metal and is used to treat bipolar disorder and depression. In people with bipolar disorder, lithium is used primarily to prevent recurrence of manic or depressive episodes, i.e. it reduces the risk of becoming ill again. Lithium can also be used to treat a manic phase. In depression, lithium is used when patients do not respond adequately to antidepressant therapy (called augmentation treatment). Lithium is also prescribed when patients have strong suicidal thoughts in the context of both above mentioned disorders.',

			'Although the efficacy of lithium is well established, it is used less frequently in practice - this is mainly due to the fact that a number of precautions must be observed during lithium therapy. Compared to other medications, the dose difference between the treatment dose and a toxic dose is not that great. Therefore, especially when new to lithium, it is necessary to frequently measure in the blood how much lithium is in the body. There is no standard dose for lithium, as there is for other medications, but the dose must be adjusted individually. Lithium is excreted from the body via the kidneys, i.e. via the urine. The excretion can also vary greatly depending on how well the kidney produces urine and how much fluid one drinks.',

			'Especially in the case of fluid loss (vomiting, diarrhea and heavy sweating), care must be taken to ensure sufficient compensation so that there is not too much lithium in your body. Poisoning warning signs by which you notice that you have too much lithium in your body are:',

			'Dizziness',
			'nausea, vomiting, diarrhea, stomach pain, loss of appetite',
			'trembling of hands, gait disturbances, difficulty speaking',
			'drowsiness, fatigue, slowing down',
			'In a severe course, muscle stiffness, epileptic seizures, decreased urination with renal insufficiency can occur.',

			'It is always important to be informed about the medications you are taking - but it is especially important with lithium! If a treatment with lithium is suggested to you, it is therefore especially important that your treating doctor informs you comprehensively and that you ask! You should be fully informed about dosage, reasons for treatment, possible side effects, poisoning warning signs and necessary safety measures. Also, make sure that the doctor prescribing lithium is always aware of what other medications you are taking so that she can assess interactions with other medications. In case of a planned or existing pregnancy, you should also urgently inform your prescribing physician.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'de',
		title: 'Medikamentenreduktion und Absetzen von Psychopharmaka',
		lastUpdate: '2023-08-11',
		content: [
			'In der Vergangenheit wurden häufig relativ hohe Dosen an Psychopharmaka verschrieben, wobei der Blick auf die Nebenwirkungen fehlte. Mittlerweile hat sich eine Grundhaltung etabliert nach dem Motto: “So wenig wie möglich, so viel wie nötig.”',

			'Gründe für eine Reduktion oder sogar ein Absetzen von Psychopharmaka können Nebenwirkungen sein. Hinzu kommt für manche Personen, dass ihnen die regelmäßige Einnahme von Tabletten ein Gefühl vermittelt, krank zu sein. Auf der anderen Seite steigt mit dem Reduzieren bzw. Absetzen von Medikamenten die Möglichkeit, erneut in eine Krise zu geraten. Das Absetzen kann also ein Gefühl von weniger Stabilität vermitteln.',

			'Wenn man sich nach Abwägen der Vor- und Nachteile, gemeinsam mit der Behandler:in, schließlich für eine Reduktion entscheidet, gibt es sogenannte mögliche Rebound-Effekte zu beachten. Dies bedeutet, dass eine Person, die über einen langen Zeitraum Psychopharmaka eingenommen hat, Absetzsymptome entwickelt. Hierbei spielen biochemische Effekte eine wichtige Rolle. Ohne an dieser Stelle zu sehr ins Detail zu gehen, gibt es hier ein paar wichtige Punkte zu beachten, um einen solchen Absetzeffekt zu vermeiden. Ein sehr schnelles oder gar abruptes Absetzen der Medikamente sollte man daher vermeiden. Ein bewährtes Vorgehen ist es, stattdessen über einen längeren Zeitraum schrittweise Psychopharmaka zu reduzieren.',

			'Für viele Personen hat es sich zudem als hilfreich erwiesen, auf andere Therapieformen zu setzen. Auf diese Art kann ein Wegfall der medikamentösen Therapie womöglich besser aufgefangen werden. Hierzu zählen Psychotherapie, Ergotherapie oder auch Anpassungen des Lebensstils, die man vornehmen kann, um weiterhin stabil zu bleiben. Es ist außerdem gut, Vertrauenspersonen, mit denen man regelmäßig Kontakt hat, einzubeziehen. Diese können dir zurückmelden, wenn sie Veränderungen wahrnehmen, die dir vielleicht nicht auffallen.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'en',
		title: 'Medication reduction and discontinuation of psychotropic drugs',
		lastUpdate: '2023-08-11',
		content: [
			'In the past, relatively high doses of psychotropic drugs were often prescribed, with little regard for the side effects. In the meantime, a basic attitude has been established according to the motto: "As little as possible, as much as necessary."',

			'Reasons for reducing or even discontinuing psychotropic drugs can include side effects. In addition, for some people, taking pills regularly gives them a feeling of being sick. On the other hand, reducing or discontinuing medication increases the possibility of falling into a crisis again. So stopping medication can give a feeling of less stability.',

			'If, after weighing the pros and cons, one finally decides to reduce medication, together with one\'s treating physician, there are so-called rebound effects to consider. This means that a person who has taken psychotropic drugs over a long period of time develops discontinuation symptoms. Biochemical effects play an important role here. Without going into too much detail, here are a few important points to consider in order to avoid such a discontinuation effect. A very fast or even abrupt discontinuation of medication should definitely be avoided. Instead, a proven procedure is to gradually reduce psychotropic drugs over a longer period of time.',

			'For many people, it has also proven helpful to rely also on other forms of therapy. In this way, a discontinuation of drug therapy may be better executed. These include psychotherapy, occupational therapy, or lifestyle adjustments that can be made to remain stable. It is also good to involve trusted people with whom you have regular contact. They can report back to you if they notice changes that you may not notice.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'de',
		title: 'Netzwerkkarte als Methode des Open Dialogue',
		lastUpdate: '2023-08-11',
		content: [
			'Das Erstellen einer Netzwerkkarte ist ein wirkungsvolles Mittel, um Klarheit zu erhalten, welche Personen im Rahmen einer Krise oder auch bei der Vorbeugung einer erneuten krisenhaften Situation hilfreich sein können.',

			'Die Netzwerkkarte ist eine Methode aus dem Open Dialogue, also dem Offenen Dialog, einer Methode mit einem systemischen Ansatz mit Herkunft aus Finnland. Viele Studien weisen auf eine gute Wirksamkeit dieses Ansatzes hin.',

			'Man geht davon aus, dass das eigene Umfeld einer Person, die in eine Krise gerät, eine wichtige Rolle bei der Genesung spielt. Es handelt sich um eine Methode, die nicht in erster Linie auf die Einnahme von Medikamenten, sondern vielmehr auf Gespräche und Veränderungen im sozialen Miteinander setzt.',

			'Die Netzwerkkarte ist dabei eines von verschiedenen Instrumenten, die im Rahmen dieser Methode zum Einsatz kommen. Ganz grob lässt sich zwischen vier Personengruppen unterscheiden: Angehörige bzw. Familie, Freundeskreis, Kolleg:innen oder auch Bekannte und schließlich die professionellen Behandelnden wie Ärzt:innen oder Therapeut:innen.',

			'Das Erstellen einer Netzwerkkarte erleichtert es, ein sogenanntes Netzwerkgespräch vorzubereiten, bei dem nahestehende und wichtige Mitmenschen der erkrankten Person einbezogen werden. Auch für die Erstellung eines Krisenplanes ist eine Netzwerkkarte hilfreich, um zu entscheiden, welche Vertrauensperson im Falle einer Krise kontaktiert werden sollte.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'en',
		title: 'Network map as a method of Open Dialogue',
		lastUpdate: '2023-08-11',
		content: [
			'Creating a network map is an effective way to get clarity about which people can be helpful in the context of a crisis or even in preventing a new crisis-like situation.',

			'The network map is a method from the Open Dialogue, a method with a systemic approach with origin in Finland. Many studies indicate a good effectiveness of this approach.',

			'It is believed that the environment of a person who finds themselves in a crisis, plays an important role in the recovery. It is a method that does not rely primarily on the use of medication, but rather on conversations and changes in social interaction.',

			'The network map is one of several tools used in this method. Roughly speaking, a distinction can be made between four groups of people: Relatives or family, circle of friends, colleagues or acquaintances, and finally professional caregivers such as doctors or therapists.',

			'The creation of a network map makes it easier to prepare a so-called network discussion, in which close and important people of the ill person are included. A network map is also helpful for drawing up a crisis plan in order to decide which trusted person should be contacted in the event of a crisis.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'de',
		title: 'Open Dialogue',
		lastUpdate: '2023-08-11',
		content: [
			'Die Methode des Offenen Dialogs ist eine alternative Behandlungsmethode und setzt in stärkerem Maße auf soziale Interaktion, den Einbezug des Umfelds und Gespräche statt auf medikamentöse Therapieformen.',

			'Ursprünglich entwickelt wurde der Open Dialogue in Finnland und insbesondere in Lappland wurden damit in wissenschaftlichen Studien nachweislich gute Behandlungserfolge erzielt. Das Umfeld einer erkrankten Person einzubeziehen, zeigt bereits, dass hier vor allem auf die Wirksamkeit der Kommunikation gesetzt wird. Medikamente stehen hierbei nicht im Zentrum der Behandlung, können aber ebenfalls zum Einsatz kommen.',

			'Gerät eine Person in eine psychotische Krise, kommt zunächst das Umfeld zusammen und wird gemeinsam mit einem Team aus professionellen Behandelnden aktiv. Dies sollte sofort und unmittelbar erfolgen. Die erkrankte Person steht dabei im Zentrum. Dies bedeutet auch, dass der Grundsatz gilt, mit ihr anstatt über sie zu sprechen. Die in unserer Gesellschaft oftmals knappe Ressource Zeit spielt eine große Rolle, denn alle Personen nehmen sich ausreichend Zeit dafür. Wichtig ist zudem, auf die krisenerfahrende Person einzugehen und ihre Bedürfnisse ernst zu nehmen.',

			'Die Methode des Offenen Dialogs wird auch in Deutschland in zunehmendem Maße angewandt, sodass nicht wenige Menschen im sozialpsychiatrischen Bereich darin ausgebildet sind. Ein flächendeckender Einsatz dieser Methode ist allerdings noch in weiter Ferne.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat'],
		},
	{
		language: 'en',
		title: 'Open Dialogue',
		lastUpdate: '2023-08-11',
		content: [
			'The Open Dialogue method is an alternative treatment method and relies to a greater extent on social interaction and the inclusion of the environment and conversations instead of drug-based forms of therapy.',

			'Open Dialogue was originally developed in Finland, in Lapland in particular, and scientific studies have shown that it has achieved good treatment results. Involving the environment of a sick person already shows that the power of communication is the main focus here. Medication is not at the center of treatment, but can also be used.',
			'If a person enters a psychotic crisis, the environment first comes together and takes action together with a team of professional treatment providers. This should be done immediately and directly. The person with the illness is the focus of attention. This also focuses on applying the principle of talking to them rather than about them. Time, which is often a scarce resource in our society, plays a major role, as all persons take sufficient time to do this. It is also important to respond to the person in crisis and to take his or her needs seriously.',

			'The Open Dialogue method is also being used increasingly in Germany, so that quite a few people in the social psychiatric field are trained in it. However, widespread use of this method is still a long way off.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat'],
		},
		{
		language: 'de',
		title: 'Psychiater:in',
		lastUpdate: '2023-08-11',
		content: [
			'Psychiater:in ist der umgangssprachliche Ausdruck für eine:n Fachärzt:in für Psychiatrie und Psychotherapie. Psychiater:innen sind Ärzt:innen, die nach Abschluss des sechsjährigen Studiums der Medizin einen Facharzt für Psychiatrie und Psychotherapie erworben haben. Die Weiterbildung zur Fachärzt:in beinhaltet eine mindestens vierjährige Arbeit in einer psychiatrischen Klinik (oder teilweise auch in einer Praxis) und mindestens einjährige Arbeit in einer neurologischen Klinik. Nebenbei muss eine psychotherapeutische Ausbildung absolviert werden, entweder im Bereich der Verhaltenstherapie, tiefenpsychologisch fundierter Psychotherapie oder Psychoanalyse. Der Abschluss der Weiterbildung ist eine Facharztprüfung bei der Ärztekammer. Psychiatrisch:innen sind in Kliniken, Tageskliniken und in Arztpraxen tätig. Neben Hausärzt:innen verschreiben Psychiatrisch:innen die meisten Psychopharmaka wie Antidepressiva und Antipsychotika. Meist sind die Termine, vor allem bei niedergelassenen Psychiatrisch:innen deutlich kürzer und seltener als bei einem Psychotherapeuten. Tipp: Um bei kurzen Terminen nichts zu vergessen, kann es sehr nützlich sein, sich vorher Notizen zu machen, um keine wichtige Information oder Frage beim Termin zu vergessen.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat'],
		},
		{
		language: 'en',
		title: 'Psychiatrists',
		lastUpdate: '2023-08-11',
		content: [
			'Colloquial term for specialist in psychiatry and psychotherapy. Psychiatrists are physicians who, after completing a six-year course of study in medicine, have obtained a specialist qualification in psychiatry and psychotherapy. Further training as a specialist includes at least four years of work in a psychiatric clinic (or partly in a practice) and at least one year of work in a neurological clinic. Alongside this, psychotherapeutic training must be completed, either in behavior therapy or psychoanalysis. The completion of the further training is a specialist examination at the medical association. Psychiatrists work in clinics, day clinics and in medical practices. In addition to general practitioners, psychiatrists prescribe most psychotropic drugs such as antidepressants and antipsychotics. In most cases, the appointments are much shorter and less frequent than with a psychotherapist, especially with psychiatrists in private practice. Tip: in order not to forget anything during short appointments, it can be very useful to make notes beforehand so as not to forget any important information or question during the appointment.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat'],
	},
	{
		language: 'de',
		title: 'Psychoanalyse',
		lastUpdate: '2023-08-11',
		content: [
			'Psychoanalyse ist zunächst als Therapie von Sigmund Freud ab Ende des 19. Jhd. entwickelt worden und ist damit die älteste der modernen Psychotherapieformen. Im Laufe der Zeit entwickelte sich die Psychoanalyse zu einer breiten wissenschaftlichen Theorie, die auch Kultur-, Sozialwissenschaften und Philosophie beeinflusste. Im Folgenden beschränken wir uns auf die Psychoanalyse als Therapieform. Die Wirkung psychoanalytischer Psychotherapie ist wissenschaftlich gut belegt.',

			'Psychoanalytische Psychotherapie beruht auf einigen zentralen Annahmen:',
			'1. Das Unbewusste: jeder Mensch hat unbewusste Wünsche, Ängste, die - ohne dass wir das bemerken - unser Handeln beeinflussen.',
			'2. Wichtigkeit frühkindlicher Erfahrungen: frühe Beziehungserfahrungen (z.B. mit Vater und Mutter) prägen unsere Persönlichkeit.',
			'3. Konflikte: Viele psychische Probleme beruhen auf intrapsychischen Konflikten (z.B. großer Wut auf jemanden und gleichzeitiger Angst davor, wütend zu werden).',
			'4. Übertragung: Frühe Beziehungserfahrungen und Konflikte prägen unsere Beziehungen in der Gegenwart – auch die Beziehung zur Therapeut:in.',
			'Das Aufdecken von Unbewusstem in der Therapie hilft der Patient:in sich selbst besser zu verstehen, sich zu verändern und somit auch bei der Überwindung von psychischen Problemen.',

			'Wie läuft psychoanalytische Psychotherapie in der Praxis?',
			'Da psychoanalytische Psychotherapie nicht direkt auf Verhaltensveränderung abzielt, wird der/die Therapeut:in weniger konkrete Tipps geben, sondern eher durch Fragen versuchen, dein Verständnis für dich selbst zu fördern.',
			'Die Therapeut:in wird typischerweise keine klare Struktur vorgeben, sondern die Patient:in bitten, möglichst spontan zu berichten, was ihr/ihm durch den Kopf geht.',
			'Psychoanalytische Psychotherapie wird 2-4x/Woche und über eine längere Zeit (2-4 Jahre) durchgeführt. Dies ist deutlich länger und intensiver als in anderen Psychotherapieformen. Es ist üblich, dass Patient:innen zeitweise während der Therapie liegen. Wenn man eine Psychoanalytische Psychotherapie in Anspruch nehmen möchte, sollte man sich also auf eine längere Behandlungsdauer einstellen.',

			'Tipp: Bei jeder Psychotherapie gibt es Vorgespräche, die auch dafür da sind, dass du als Patient:in entscheiden kannst, ob eine bestimmte Therapie deinen Vorstellungen entspricht. Nutze Sie, um Fragen zu stellen. Zweifel solltest du ansprechen und mit der Therapeut:in klären.',

			'Hinweis: Aus der Psychoanalyse hat sich ein weiteres Therapieverfahren entwickelt: tiefenpsychologisch fundierte Psychotherapie. Diese beruht auf den gleichen Theorien, wird jedoch meist mit einer Frequenz von einmal pro Woche durchgeführt und dauert weniger lang.',
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat'],
	},
	{
		language: 'en',
		title: 'Psychoanalysis',
		lastUpdate: '2023-08-11',
		content: [
			'Psychoanalysis was first developed as a therapy by Sigmund Freud from the end of the 19th century and is thus the oldest of the modern forms of psychotherapy. Over time, psychoanalysis developed into a broad scientific theory that also influenced cultural studies, social sciences, and philosophy. In the following, we will limit ourselves to psychoanalysis as a form of therapy. The effect of psychoanalytic psychotherapy is scientifically well-proven.',

			'Psychoanalytic psychotherapy is based on some central assumptions:',
			'1. The unconscious: every person has unconscious desires, fears, which - without us noticing - influence our actions.',
			'2. Importance of early childhood experiences: early relationship experiences (e.g. with father and mother) shape our personality.',
			'3. Conflicts: many psychological problems are based on intrapsychic conflicts (e.g. great anger towards someone and simultaneous fear of becoming angry).',
			'4. Transference: early relationship experiences and conflicts shape our relationships in the present - also the relationship with the therapist.',
			'Uncovering the unconscious in therapy helps the patients to understand themselves better, to change, and thus also to overcome psychological problems.',

			'How does psychoanalytic psychotherapy work in practice?',
			'Since psychoanalytic psychotherapy is not directly aimed at behavior change, the therapist will not give as much concrete advice, but will rather try to further your understanding of yourself by asking questions. The therapist will typically not provide a clear structure, but will ask the patient to report what is on her/his mind as spontaneously as possible. Psychoanalytic psychotherapy is conducted 2-4x/week and over a longer period of time (2-4 years). This is much longer and more intensive than in other forms of psychotherapy. It is common for patients to lie down at times during therapy. Thus, if one wishes to engage in psychoanalytic psychotherapy, one should be prepared for a longer treatment period.',

			'Tip: In every psychotherapy there are preliminary discussions, which are also there so that you as a patient can decide whether a particular therapy meets your expectations. Use them to ask questions. You should raise doubts and clarify them with the therapist.',

			'Note: From psychoanalysis another therapy method has developed: depth psychology based psychotherapy. This is based on the same theories but is usually conducted with a frequency of once a week and lasts less time.',
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat']
	},
	{
		language: 'de',
		title: 'Psychose-Spektrum statt Schizophrenie',
		lastUpdate: '2023-08-11',
		content: [
			'Da man früher davon ausging, dass es keine Heilung für Menschen mit Schizophrenie geben kann, steht der Begriff auch heute noch stellvertretend für Konzepte, die Psychose-erfahrenen Menschen jede Hoffnung und Zuversicht auf einen günstigen Krankheitsverlauf nehmen. Diese Hoffnungslosigkeit ist somit das Gegenteil von einem Konzept wie Recovery.',

			'Der Begriff Schizophrenie ruft sehr leicht Missverständnisse und Vorurteile hervor und wird im Alltag oftmals falsch verwendet. Gerade im medialen Kontext ist häufig von Schizophrenie die Rede, wenn es um fragwürdige Haltungen bestimmter Personen geht. Sehr verbreitet ist zudem die Annahme, dass es sich bei Schizophrenie um eine dissoziative Identitätsstörung handelt. Also das, was im Sprachgebrauch oftmals als Multiple Persönlichkeit beschrieben wird.',

			'Generell gibt es eine sehr große Bandbreite an unterschiedlichen Symptomen, die von Person zu Person sehr individuell ausgeprägt sein können. Zwei Menschen mit derselben Diagnose haben nicht selten komplett unterschiedliche Symptome. Dies unterstreicht besonders, dass das Erleben einer psychotischen Episode hochgradig individuell und einzigartig ist.',

			'Aus diesen Gründen erscheint es vielen Forschenden sinnvoll, von einem Psychose-Spektrum zu sprechen und nicht länger das Wort Schizophrenie zu verwenden. Auf diese Art kann die Diagnose von vielen Personen wesentlich besser angenommen und akzeptiert werden. Zudem wird die Stigmatisierung und Selbststigmatisierung von Psychose-erfahrenen Personen deutlich reduziert. Dies beweist beispielsweise die wissenschaftliche Forschung, die sich mit der Abschaffung des Begriffs Schizophrenie in Ländern wie Japan und Südkorea befasst.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['Psychose', 'Schizophrenie', 'psychische Gesundheit']
	},
	{
		language: 'en',
		title: 'Psychosis Spectrum vs. Schizophrenia',
		lastUpdate: '2023-08-11',
		content: [
			'Because it was once assumed that there could be no cure for people with schizophrenia, the term is still representative of concepts that deprive people experiencing psychosis of any hope or confidence that the course of the illness will be favorable. This hopelessness is thus the opposite of a concept such as recovery.',

			'The term schizophrenia very easily evokes misunderstandings and prejudices and is often misused in everyday life. Especially in the media context, the term schizophrenia is often used to refer to questionable attitudes of certain individuals. It is commonly assumed that schizophrenia is a dissociative identity disorder. In other words, this is known as multiple personality.',

			'In general, there is a very wide range of different symptoms that can be very individualized from person to person. It is not uncommon for two people with the same diagnosis to have completely different symptoms. This particularly underscores that the experience of a psychotic episode is highly individual and unique.',

			'For these reasons, it seems useful to many researchers to speak of a psychosis spectrum and no longer use the word schizophrenia. In this way, the diagnosis can be much better accepted by many people. In addition, the stigmatization and self-stigmatization of persons with psychosis will be significantly reduced. This is proven, for example, by scientific research that deals with the abolition of the term schizophrenia in countries such as Japan and South Korea.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist Psychiatry)', 'Dennis Stratmann (Recovery Companion)'],
		tags: ['hcp', 'pat', 'psychosis', 'schizophrenia', 'mental health']
	},
	{
		language: 'de',
		title: 'Trauma',
		lastUpdate: '2023-08-11',
		content: [
			'Unter Trauma versteht man ein belastendes Ereignis oder eine Situation außergewöhnlicher Bedrohung oder katastrophenartigen Ausmaßes, die bei fast jedem eine tiefe Verzweiflung hervorrufen würde - so die Definition im Diagnosemanual ICD-10. Sehr unangenehme Situationen, die jemanden belasten, erfüllen nicht unbedingt die Definition eines Traumas, wie es umgangssprachlich manchmal ausgedrückt wird. Wörtlich übersetzt heißt das Wort auf griechisch “Wunde”. Im Bereich der Psychiatrie und Psychotherapie sind damit seelische Verletzungen gemeint. Oft “heilen” diese Verletzungen, ohne dass eine psychische Erkrankung entsteht.',

			'Seelische Traumata können jedoch unterschiedliche psychische Symptome hervorrufen: häufig sind Depressionen, Ängste, Schlafstörungen, Wiedererleben des Traumas in Träumen oder sich aufdrängenden Gedanken, Gereiztheit, Vermeiden von Situationen, die an das Trauma erinnern, aber auch körperlich wahrgenommene Schmerzen. Traumata sind somit ein Risikofaktor für viele psychische Erkrankungen. Mittlerweile gibt es in den unterschiedlichen psychotherapeutischen Verfahren spezielle Methoden, um die Folgen von Traumatisierungen zu behandeln, also sowohl in der kognitiven Verhaltenstherapie als auch in der tiefenpsychologisch fundierten Psychotherapie. Im Zentrum der Behandlung steht dabei immer eine Bearbeitung, also aktive Auseinandersetzung mit dem Trauma, mit dem Ziel, dass die Symptome der Traumatisierung abnehmen. Psychotherapeut:innen können sich im Bereich Traumatherapie weiterbilden, aber die Qualifikation als Traumatherapeut:in ist nicht einheitlich geregelt.'
		],
		authors: ['Dr. Caspar Wiegmann (Psychiater)', 'Dennis Stratmann (Genesungsbegleiter)'],
		tags: ['hcp', 'pat', 'Trauma', 'psychische Gesundheit']
	},
	{
		language: 'en',
		title: 'Trauma',
		lastUpdate: '2023-08-11',
		content: [
			'Trauma is defined as a stressful event or situation of extraordinary threat or catastrophic magnitude that would cause deep distress in almost anyone - according to the definition in the ICD-10 diagnostic manual. Very negative situations that stress someone, do not necessarily meet the definition of trauma, as it is sometimes colloquially expressed. Literally translated, the word means "wound" in Greek. In the field of psychiatry and psychotherapy, it refers to psychological injuries. Often these injuries "heal" without causing a mental illness.',

			'However, mental trauma can cause various psychological symptoms: common are depression, anxiety, sleep disturbances, reliving the trauma in dreams or imposing thoughts, irritability, avoidance of situations reminiscent of the trauma, but also physically perceived pain. Trauma is thus a risk factor for many mental illnesses. In the meantime, there are special methods in the different psychotherapeutic procedures to treat the consequences of trauma, i.e. in cognitive behavioral therapy as well as in deep psychologically based psychotherapy. The focus of the treatment is always on processing, i.e. active confrontation with the trauma with the goal that the symptoms of the traumatization decrease. Psychotherapists can continue their education in the field of trauma therapy, but the qualification as a trauma therapist is not uniformly regulated.'
		],
		authors: ['Dr. Caspar Wiegmann (M.D., Specialist in Psychiatry and Psychotherapy)', 'Dennis Stratmann (Peer Worker)'],
		tags: ['hcp', 'pat', 'trauma', 'mental health']
	}
]
