import { Component, Input } from '@angular/core'
import { RccModalController, RccTranslationService, SharedModule } from '@rcc/common'
import { GlossaryEntry } from '@rcc/core/src/items/glossary-entry'

@Component({
	selector	: 'rcc-glossary-entry-modal',
	templateUrl	: 'glossary-entry-modal.component.html',
	styleUrls	: ['./glossary-entry-modal.component.scss'],
	standalone	: true,
	imports		: [SharedModule],
})
export class RccGlossaryEntryModalComponent {
	@Input()
	public entry: GlossaryEntry

	public constructor(
		private readonly rccTranslationService: RccTranslationService,
		private readonly rccModalController: RccModalController,
	) {}

	protected get authors(): string[] {
		const authors: string[] = this.entry.authors ?? []

		if (authors.length === 0)
			return []

		const first: string = this.rccTranslationService.translate('GLOSSARY.AUTHORS.FIRST', { name: authors[0] })

		const last: string = authors.length > 1 ? this.rccTranslationService.translate('GLOSSARY.AUTHORS.LAST', { name: authors[authors.length - 1] }) : undefined

		const middle: string[] = authors.length > 2 ? [...authors].splice(1, authors.length - 2) : []

		return [first, ...middle, last].filter(value => !!value)
	}

	protected closeModal(): void {
		this.rccModalController.dismiss()
	}

	private _dateFormatOptions: Intl.DateTimeFormatOptions = {
		day: '2-digit',
		month: '2-digit',
		year: 'numeric',
	}
	protected dateFormatOptions: Record<string, string> = this._dateFormatOptions as Record<string, string>
}
