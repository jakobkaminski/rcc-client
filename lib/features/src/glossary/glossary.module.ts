import	{
			NgModule
		}											from '@angular/core'

import	{	RouterModule, Routes				}	from '@angular/router'
import	{
			Factory,
			HandlerAction,
			ItemAction,
			provideItemAction,
			provideTranslationMap,
			RccModalController,
			SharedModule,
		}											from '@rcc/common'
import	{	MedicalFeatureModule				}	from '@rcc/common/src/medical/medical-feature/medical-feature.module'
import	{	GlossaryPageComponent				}	from './page'
import	{	CuratedGlossaryEntryStoreService	}	from './store'
import	{	GlossaryEntry						}	from '@rcc/core/src/items/glossary-entry'
import	{	RccGlossaryEntryModalComponent		}	from './modal/glossary-entry-modal/glossary-entry-modal.component'


import en from './i18n/en.json'
import de from './i18n/de.json'

export const GlossaryMainMenuPath: string = '/backup'

const routes: Routes	=	[
								{
									path: 		'medical-glossary',
									title:		'BACKUP.PAGE_TITLE',
									component: 	GlossaryPageComponent
								}
							]

const itemAction: Factory<ItemAction<GlossaryEntry>> = {
	deps: [RccModalController],
	factory: (rccModalController: RccModalController) => ({
		role:		'details',
		itemClass:	GlossaryEntry,
		storeClass:	CuratedGlossaryEntryStoreService,

		getAction: (entry: GlossaryEntry): HandlerAction => ({
			icon: 'information',
			label: 'GLOSSARY.INFORMATION',
			handler: () => {
				void rccModalController.present(RccGlossaryEntryModalComponent, { entry }, { mini: true })
			}
		})
	})
}

@NgModule({
imports:	[
				SharedModule,
				RouterModule.forChild(routes),
				MedicalFeatureModule,
			],
providers:	[
				CuratedGlossaryEntryStoreService,
				provideTranslationMap('GLOSSARY',{ en,de }),
				provideItemAction(itemAction),
			],
declarations:	[
					GlossaryPageComponent,
				]
})
export class GlossaryModule{}
