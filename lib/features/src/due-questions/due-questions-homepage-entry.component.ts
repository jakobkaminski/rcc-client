
import	{	Component				}	from '@angular/core'
import	{	NotificationService		}	from '@rcc/common'

@Component({
	template: 	`
					{{
						(notificationService.notification$ | async) > 0
						?	('DUE_QUESTIONS.DESCRIPTION' | translate : ({count: (notificationService.notification$ | async)}) )
						:	('DUE_QUESTIONS.NOTHING_DUE' | translate)
					}}
				`
})
export class DueQuestionsHomePageEntryComponent {
	public constructor(public notificationService: NotificationService){}
}
