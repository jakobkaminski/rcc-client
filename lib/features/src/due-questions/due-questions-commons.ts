import	{
			SymptomCheck,
			Question
		}							from '@rcc/core'

export const DueQuestionsHomePath = 'due-questions'


export interface DueData {
	symptomChecks:	SymptomCheck[],
	questions:		Question[],
	startDate:		Date,
	endDate:		Date
}
