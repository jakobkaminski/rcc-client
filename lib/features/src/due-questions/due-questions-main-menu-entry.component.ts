import 	{
			Component,
			OnDestroy
		}								from '@angular/core'
import	{	Subscription			}	from 'rxjs'
import	{
			DueData,
			DueQuestionsHomePath
		}								from './due-questions-commons'
import	{	DueQuestionsService		}	from './due-questions.service'

@Component({
	template:	`
					<ion-item routerLink = "${DueQuestionsHomePath}">
						<ion-label [id]	= "'DUE_QUESTIONS.MENU_ENTRY' | toID:'rcc-e2e'">{{ "DUE_QUESTIONS.MENU_ENTRY" | translate }}</ion-label>
						<ion-badge color="secondary">{{questionCount}}</ion-badge>
						<ion-icon [name] = " 'notification' | rccIcon" slot = "end"></ion-icon>
					</ion-item>
				`
})
export class DueQuestionsMainMenuEntryComponent implements OnDestroy {

	public questionCount	: number

	private subscriptions	: Subscription[] = []

	public constructor(
		public dueQuestionsService	: DueQuestionsService
	){

		this.subscriptions.push(
			this.dueQuestionsService
			.subscribe( (result: DueData) => this.questionCount = result && result.questions.length || 0)
		)

	}


	public ngOnDestroy(): void{
		this.subscriptions.forEach( subscription => subscription.unsubscribe() )
	}

}
