import { assert, assertProperty } from '@rcc/core'

export const runtimeConfigPath: string = 'startup.contract-modal'

interface ModalConfig {
	content: {
		translations: Record<string, string>
	},
	userConfirmationKey: string,
	excludedRoutes: string,
}

export function assertContractModalConfig(config: unknown): asserts config is ModalConfig {
	assertProperty(config, 'content', 						'assertContractModalConfig: config is missing .content property.')
	assertProperty(config.content, 'translations', 			'assertContractModalConfig: translations are missing on content property')

	assert(typeof config.content.translations === 'object',	'assertContractModalConfig: translations must be a record')

	Object.entries(config.content.translations).map(([key, value]) => {
		assert(typeof value === 'string',					`assertContractModalConfig: Translation key: ${key} should have a string value, got instead ${value}`)
	})

	assertProperty(config, 'userConfirmationKey',			'assertContractModalConfig: config is missing .userConfirmationKey property.')
}
