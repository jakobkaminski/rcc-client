import { NgModule } from '@angular/core'
import { RccPublicRuntimeConfigModule, provideTranslationMap, requestPublicRuntimeConfigValue } from '@rcc/common'
import { runtimeConfigPath } from './runtime-startup-contract-modal.commons'
import { RccRuntimeStartupContractModalService } from './runtime-startup-contract-modal.service'
import { NavigationEnd, Router } from '@angular/router'
import { filter } from 'rxjs'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	imports: [
		RccPublicRuntimeConfigModule
	],
	providers: [
		RccRuntimeStartupContractModalService,
		provideTranslationMap('RUNTIME_STARTUP_CONTRACT_MODAL', { en, de }),
		requestPublicRuntimeConfigValue({
			path:			runtimeConfigPath,
			description: 	'Startup Contract Modal Config',
			type:			'ModalConfig',
		}),
	],
})
export class RccRuntimeStartupContractModalModule {
	public constructor(
		rccRuntimeStartupContractModalService	: RccRuntimeStartupContractModalService,
		router									: Router,
	) {
		router.events.pipe(
			filter((event: NavigationEnd) => event instanceof NavigationEnd),
		).subscribe(() => {
			void rccRuntimeStartupContractModalService.run()
		})
	}
}
