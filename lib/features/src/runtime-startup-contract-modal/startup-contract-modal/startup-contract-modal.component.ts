import	{
			Component,
			Input,
			ViewEncapsulation
		}										from '@angular/core'

import	{
			RccModalController,
			SharedModule
		} 										from '@rcc/common'

import	{
			RccMarkdownComponent
		}										from '@rcc/common/ui-components/markdown'

interface Content {
	translations: Record<string, string>
}

@Component({
	selector: 		'rcc-startup-contract-modal',
	templateUrl:	'./startup-contract-modal.component.html',
	styleUrls: 		['./startup-contract-modal.component.scss'],
	standalone: 	true,
	encapsulation:	ViewEncapsulation.None,
	imports: 		[SharedModule, RccMarkdownComponent],
})
export class RccStartupContractModalComponent {
	@Input()
	public content: Content

	public constructor(private readonly rccModalController: RccModalController) {}

	protected confirm(): void {
		this.rccModalController.dismiss(true)
	}
}
