import	{
			NgModule,
			Type
		}													from '@angular/core'
import	{
			Routes,
			RouterModule
		}													from '@angular/router'
import	{	Report										}	from '@rcc/core'
import	{
			Factory,
			ItemAction,
			RccCardComponent,
			RccModalController,
			SharedModule,
			WidgetsModule,
			provideItemAction,
			provideTranslationMap,
		}													from '@rcc/common'
import	{
			RccComputedValuesListComponent
		}													from '../computed-values'


import	{	QuestionnaireServiceModule					}	from '../questions'

import	{
			RccReportStackedViewFullPageComponent,
			RccReportStackedViewReducedPageComponent,
		}													from './page'
import	{	ReportStackedViewComponent					}	from './view/report-stacked-view.component'

import en from './i18n/en.json'
import de from './i18n/de.json'

export const REPORT_STACKED_VIEW_PATH: string = 'my-charts'
const routes: Routes	=	[
	{
		path: 		REPORT_STACKED_VIEW_PATH,
		title:		'REPORT_STACKED_VIEW.PAGE_TITLE',
		component:	RccReportStackedViewFullPageComponent,
	},
]

const components : Type<unknown>[] = [
	ReportStackedViewComponent,
	RccReportStackedViewFullPageComponent,
	RccReportStackedViewReducedPageComponent,
]

const reportDetailsItemActionFactory : Factory<ItemAction<Report>> = {

	deps: [RccModalController],

	factory(...args: unknown[]) {
		const rccModalController: RccModalController = args[0] as RccModalController
		return {
				itemClass:	 		Report,
				role: 				'details' as const,
				getAction: (report: Report) => ({
					label: 			'SESSIONS.HOME.INSPECT_DATA.LABEL' as const,
					description: 	'SESSIONS.HOME.INSPECT_DATA.DESCRIPTION' as const,
					icon: 			'view' as const,
					handler: 		() => rccModalController.present(RccReportStackedViewReducedPageComponent, { report }, { fullScreen: true }),
					position:		0,
					category:		'analyze' as const,
				})
			}
	}

}

@NgModule({
	providers:[
		provideTranslationMap('REPORT_STACKED_VIEW', { en, de }),
		provideItemAction(reportDetailsItemActionFactory),
	],
	imports: [
		RouterModule.forChild(routes),
		SharedModule,
		RccCardComponent,
		WidgetsModule,
		RccComputedValuesListComponent,
		QuestionnaireServiceModule,
	],
	declarations: [
		...components,
	],
	exports: [
		...components,
		RouterModule,
	]
})
export class ReportStackedViewModule {}
