import	{
			Component,
		}												from '@angular/core'
import	{	RccReportStackedViewPageBaseComponent	}	from '../base/report-stacked-view-page-base.component'

@Component({
	selector	:	'rcc-report-stacked-view-full-page',
	templateUrl	:	'./report-stacked-view-full-page.component.html',
	styleUrls	:	['../base/report-stacked-view-page-base.component.scss'],
})
export class RccReportStackedViewFullPageComponent extends RccReportStackedViewPageBaseComponent {}
