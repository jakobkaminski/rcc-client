import	{
			Component,
			OnInit,
			OnDestroy,
			Input
		}									from '@angular/core'
import	{	FormControl					}	from '@angular/forms'
import	{
			PathAction,
			RccTranslationService,
		}									from '@rcc/common'

import	{
			Subject,
			takeUntil
		}									from 'rxjs'

import	{
			Report,
			sub,
		}									from '@rcc/core'

import	{
			EntryMetaStoreService,
		}									from '../../../entries'

import	{
			DataViewScope
		}									from '../../../data-visualization'


type DataViewScopeOption	= 	{ value: DataViewScope, label: string }

type DataViewScopeConfig 	= 	{

									label				: string,
									startDate			: (startDate: Date) => Date
									highlightInterval	: number
								}

const dateFormat			:	Intl.DateTimeFormatOptions
							= 	{
									day: 	'2-digit',
									month: 	'2-digit',
									year: 	'numeric'
								}

const defaultDataViewScope	:	DataViewScope
							=	'week'

const dataViewScopes		: 	Record<DataViewScope, DataViewScopeConfig>
							=	{
									week: {
										label:		 		'REPORT_STACKED_VIEW.VIEW_OPTIONS.WEEK',
										startDate: 			(startDate) => sub(startDate, { days: 7 }),
										highlightInterval: 	-8,
									},
									month: {
										label: 				'REPORT_STACKED_VIEW.VIEW_OPTIONS.MONTH',
										startDate: 			(startDate) => sub(startDate, { weeks: 4 }),
										highlightInterval:	-31,
									},
									year: {
										label: 				'REPORT_STACKED_VIEW.VIEW_OPTIONS.YEAR',
										startDate: 			(startDate) => sub(startDate, { years: 1 }),
										highlightInterval: 	0,
									}
								}

@Component({
	selector	:	'',
	template	:	'',
})
export abstract class RccReportStackedViewPageBaseComponent implements OnInit, OnDestroy {

	@Input()
	public report						:	Report
										=	undefined

	// #region actions
	protected backAction				: 	PathAction
										=	{
												path: 		'/',
												label: 		'REPORT_STACKED_VIEW.BUTTON_BACK.LABEL',
												icon: 		'back',
												category: 	'create'  // TODO: shouldn't be 'create', but this is according to the figma design
											}
	// #endregion

	protected dataViewScopeControl		: 	FormControl<DataViewScope>
										= 	new FormControl(defaultDataViewScope)

	protected dataViewScopeOptions		: 	DataViewScopeOption[]
										= 	Object.entries(dataViewScopes).map(([dataViewScope, dataViewScopeOptions]) => ({
												value: dataViewScope as DataViewScope,
												label: this.translationService.translate(dataViewScopeOptions.label)
											}))

	protected initialEndDate			:	Date
										= 	new Date()

	protected startDate					: 	Date
										=	sub(this.initialEndDate, { days: 7 })

	protected dateControl				:	FormControl<Date>
										=	new FormControl(this.initialEndDate)

	protected dateFormat				:	Record<string, unknown>
										= 	dateFormat as Record<string, unknown>

	protected calendarHighlightInterval	: 	number
										= 	dataViewScopes[defaultDataViewScope].highlightInterval

	protected destroy$					: 	Subject<void>
										= 	new Subject<void>()




	public constructor(
		private readonly entryMetaStoreService	: EntryMetaStoreService,
		private readonly translationService		: RccTranslationService
	) {}

	public get endDate(): Date { return this.dateControl.value }

	public async ngOnInit(): Promise<void> {

		await this.entryMetaStoreService.ready

		this.report = this.report ?? Report.from(this.entryMetaStoreService.items)

		this.dateControl.valueChanges
		.pipe( takeUntil(this.destroy$) )
		.subscribe((endDate) => {

			const dataViewScope				: DataViewScope
											= this.dataViewScopeControl.value

			const startDate					: Date
											= dataViewScopes[dataViewScope].startDate(endDate)

			this.startDate 					= startDate
		})


		this.dataViewScopeControl.valueChanges
		.pipe( takeUntil(this.destroy$) )
		.subscribe((dataViewScope) => {

			const selectedEndDate			: Date
											= new Date(this.dateControl.value)

			const newStartDate				: Date
											= dataViewScopes[dataViewScope].startDate(selectedEndDate)

			this.startDate 					= newStartDate
			this.calendarHighlightInterval 	= dataViewScopes[dataViewScope].highlightInterval

		})
	}


	public ngOnDestroy() : void {

		this.destroy$.next()
		this.destroy$.complete()
	}

}
