import	{	InjectionToken	}	from '@angular/core'

export const matomoScriptUrlAppendix	: string = '/matomo.js'
export const matomoPHPUrlAppendix		: string = '/matomo.php'

export interface MatomoConfig {
	url: string
}

export const MATOMO_LOGGING_ENDPOINT : InjectionToken<string>
	= new InjectionToken<string>('url to which all logging data is sent')
