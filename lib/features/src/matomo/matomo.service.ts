import	{
			Inject,
			Injectable
		}							from '@angular/core'

import	{
			LoggingDataObject,
			RccUsageLoggingService
		} 							from '../usage-logging'

import	{
			MatomoTracker
		}							from 'ngx-matomo-client'

import	{
			uuidv4
		} 							from '@rcc/core'
import 	{
			matomoPHPUrlAppendix,
			matomoScriptUrlAppendix
		}							from './matomo.commons'

import	{
			RccBuildInfoService,
			RccPublicRuntimeConfigService,
		}							from '@rcc/common'

@Injectable()
export class MatomoService extends RccUsageLoggingService {

	private userId: string

	public constructor(
		private matomoTracker: MatomoTracker,
		buildInfoService: RccBuildInfoService,

		@Inject('MATOMO_LOGGING_ENDPOINT')
		protected usageLoggingEndpoint: string,
		private readonly rccPublicRuntimeConfigService: RccPublicRuntimeConfigService
	) {
		super(buildInfoService)

		void this.setup()
	}

	private async setup(): Promise<void> {
		await this.initializeMatomoTracker()
		this.createUserIdIfNeededAndSetMatomoUid()
	}

	private async initializeMatomoTracker(): Promise<void> {
		const siteId: string = await this.rccPublicRuntimeConfigService.get('matomo.site-id') as string
		const matomoScriptUrl: string = this.usageLoggingEndpoint + matomoScriptUrlAppendix
		const matomoPHPUrl: string = this.usageLoggingEndpoint + matomoPHPUrlAppendix

		const existingScript: HTMLElement = document.getElementById('matomo-script')

		if (existingScript) return undefined

		// configured matomo snippet
		type piwikAnalyticsQueueCommand = [string, string];
		type piwikAnalyticsQueueArray = piwikAnalyticsQueueCommand[];

		interface CustomWindow extends Window {
			_paq?: piwikAnalyticsQueueArray;
		}

		const customWindow : CustomWindow = window as CustomWindow
		const _paq: piwikAnalyticsQueueArray = customWindow._paq || []

		_paq.push(['setTrackerUrl', matomoPHPUrl])
		_paq.push(['setSiteId', siteId])

		const matomoScriptElement: HTMLScriptElement = document.createElement('script')
		const firstFoundScriptElement: HTMLScriptElement = document.getElementsByTagName('script')[0]

		matomoScriptElement.setAttribute('id', 'matomo-script')
		matomoScriptElement.async = true
		matomoScriptElement.src = matomoScriptUrl

		if (firstFoundScriptElement.parentNode)
			firstFoundScriptElement.parentNode.insertBefore(matomoScriptElement, firstFoundScriptElement)
		else
			console.error('Error: s.parentNode is null or undefined')

	}

	private createUserIdIfNeededAndSetMatomoUid() : void {
		this.userId = this.getUserId()

		if (!this.userId) {
			this.userId = this.generateUserId()
			this.storeUserId(this.userId)
		}

		this.matomoTracker.setUserId(this.userId)
	}

	private getUserId(): string {
		return localStorage.getItem('userId')
	}

	private storeUserId(userId: string): void {
		localStorage.setItem('userId', userId)
	}

	private generateUserId(): string {
		return uuidv4()
	}

	protected async logEvent(loggingData: LoggingDataObject, flavor: string, version: string): Promise<void> {

		if (loggingData.requiresUserConsent && await this.matomoTracker.isUserOptedOut())
			return


		if (loggingData.includeUserId) {

			let userId: string = this.getUserId()

			if (!userId) {
				userId = this.generateUserId()
				this.storeUserId(userId)
				this.matomoTracker.setUserId(userId)
			}
		}

		const categoryValue : string = loggingData.category + ' - ' + flavor + ' - ' + version

		this.matomoTracker.trackEvent(categoryValue, loggingData.key, loggingData.extraValue, loggingData.computableValue)

	}
}
