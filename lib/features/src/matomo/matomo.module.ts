import {
			ModuleWithProviders,
			NgModule
		}							from '@angular/core'

import {
			MatomoService
		}							from './matomo.service'

import {
			RccUsageLoggingModule,
			RccUsageLoggingService
		}							from '../usage-logging'

import {
			MatomoInitializationMode,
			NgxMatomoModule
		} 							from 'ngx-matomo-client'

import	{
			MatomoConfig
		}							from './matomo.commons'
import	{
			environment
		} 							from '../../../../app/generic/src/environments/environment'
import	{
			requestPublicRuntimeConfigValue
		}							from '@rcc/common'

@NgModule({
	providers: [
		MatomoService,
		{
			provide: RccUsageLoggingService,
			useExisting: MatomoService
		},
		requestPublicRuntimeConfigValue({
			description: 'Matomo site ID',
			path: 'matomo.site-id',
			type: 'string',
			required: true
		})
	],
	declarations: [],
	imports: [
		RccUsageLoggingModule,
		NgxMatomoModule.forRoot({
			mode				:	MatomoInitializationMode.MANUAL,
			enableLinkTracking	:	false,
			disabled			:	environment.matomo.disabled,
		}),
	]
})

export class MatomoModule {

	public static forRoot(config: MatomoConfig) : ModuleWithProviders<RccUsageLoggingModule> {

		if (config.url === '' || config.url === undefined)
			throw new Error('Logging endpoint is not defined')

		return 	{
			ngModule: 	MatomoModule,
			providers:	[
				{
					provide: 	'MATOMO_LOGGING_ENDPOINT',
					useValue: 	config.url
				}
			]
		}

	}


}
