import { Component, Inject, OnInit, Optional } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { CustomQuestionStoreService, CustomSymptomCheckStoreService, QUESTION_STORES, SymptomCheckMetaStoreService, SymptomCheckMetaStoreServiceHCPHomePath } from '../..'
import { QuestionConfig, QuestionRelationDto, QuestionStore, SymptomCheck } from '@rcc/core'
import { DAILY_NOTES_ID } from '@rcc/features/curated/curated-questions/curated-default-questions'
import { RccQuestionnaireEditorService } from '@rcc/features/questionnaire-editor/questionnaire-editor.service'
import { RccTitleService, RccTranslationService } from '@rcc/common'
import { ConfirmPayload } from '@rcc/features/questionnaire-editor/questionnaire-editor/questionnaire-editor.component'

export enum EditMode {
	NEW = 'new',
	FROM_TEMPLATE = 'from-template',
}

@Component({
	templateUrl: './monitoring-setup-custom-template-page.component.html'
})
export class MonitoringSetupCustomTemplatePageComponent implements OnInit {
	public constructor(
		activatedRoute: ActivatedRoute,
		private readonly symptomCheckMetaStoreService: SymptomCheckMetaStoreService,
		private readonly customSymptomCheckStoreService: CustomSymptomCheckStoreService,
		private readonly rccQuestionnaireEditorService: RccQuestionnaireEditorService,
		private readonly router: Router,
		private readonly rccTranslationService: RccTranslationService,
		private readonly rccTitleService: RccTitleService,
		private readonly customQuestionStoreService: CustomQuestionStoreService,
		@Optional() @Inject(QUESTION_STORES)
		private readonly stores: QuestionStore[],

	) {
		this.mode = activatedRoute.snapshot.data.mode as EditMode
		this.templateId = activatedRoute.snapshot.params['templateId'] as string

		if (this.mode === EditMode.FROM_TEMPLATE && !this.templateExists(this.templateId))
			void this.router.navigate([`/${SymptomCheckMetaStoreServiceHCPHomePath}`])

		void this.determineInitialName()
	}

	public ngOnInit(): void {
		this.rccTitleService.setTitle('MONITORING_SETUP_CUSTOM_TEMPLATE.PAGE_TITLE')
	}

	private mode: EditMode

	protected newTemplateInitialName: string

	protected templateId: string

	protected async saveTemplate(data: ConfirmPayload): Promise<void> {
		data.config.questions = data.config.questions.filter(
			question => !this.isDailyNotesQuestion(question)
		)
		await this.customSymptomCheckStoreService.addSymptomCheckConfig(data.config)
		await Promise.all([
			this.storeOnTheFlyQuestions(data.questions)
		])
		await this.rccQuestionnaireEditorService.reset()
		void this.router.navigate([SymptomCheckMetaStoreServiceHCPHomePath])
	}

	private async storeOnTheFlyQuestions(questions: QuestionConfig[]) : Promise<void> {
		await Promise.all(questions.map( async question => {
			if (this.questionExists(question.id)) return

			await this.customQuestionStoreService.addQuestionConfig(question)
		}))
	}

	private isDailyNotesQuestion(question: string | QuestionRelationDto): boolean {
		if (typeof question === 'string')
			return question === DAILY_NOTES_ID
		return question.id === DAILY_NOTES_ID
	}

	private templateExists(templateId: string): boolean {
		const storesContainingId: Array<unknown> = this.symptomCheckMetaStoreService.stores
			.map(store => store.items)
			.filter(items => items.findIndex(item => item.id === templateId) !== -1)
		return storesContainingId.length > 0
	}

	private _questionIndex: Set<string>
	private get questionIndex(): Set<string> {
		if (this._questionIndex) return this._questionIndex

		this._questionIndex = new Set()
		this.stores.forEach(store => {
			store.items.forEach(item => {
				this._questionIndex.add(item.id)
			})
		})
		return this._questionIndex
	}

	private questionExists(questionId: string): boolean {
		return this.questionIndex.has(questionId)
	}

	private async determineInitialName(): Promise<void> {
		if (this.mode === EditMode.NEW)
			this.newTemplateInitialName = this.rccTranslationService.translate(
				'MONITORING_SETUP_CUSTOM_TEMPLATE.NEW_TEMPLATE'
			)

		else {
			const template: SymptomCheck = await this.symptomCheckMetaStoreService.get(this.templateId)
			this.newTemplateInitialName = template.meta.label
		}
	}
}
