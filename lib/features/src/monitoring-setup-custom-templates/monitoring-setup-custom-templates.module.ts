import { Routes } from '@angular/router'
import { EditMode, MonitoringSetupCustomTemplatePageComponent } from './monitoring-setup-custom-template-page/monitoring-setup-custom-template-page.component'
import { NgModule } from '@angular/core'
import { SharedModule, overrideRoute, provideTranslationMap } from '@rcc/common'
import { QuestionnaireEditorModule } from '../questionnaire-editor/questionnaire-editor.module'
import { canDeactivateMonitoringSetupCustomTemplateFn } from './can-deactivate-monitoring-setup-custom-template-fn'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes: Routes = [
	{
		path: 'symptom-checks/custom',
		children: [
			{
				path: 'new',
				component: MonitoringSetupCustomTemplatePageComponent,
				canDeactivate: [canDeactivateMonitoringSetupCustomTemplateFn],
				data: {
					mode: EditMode.NEW
				}
			},
			{
				path: 'edit/:templateId',
				component: MonitoringSetupCustomTemplatePageComponent,
				canDeactivate: [canDeactivateMonitoringSetupCustomTemplateFn],
				data: {
					mode: EditMode.FROM_TEMPLATE
				}
			},
			{
				path: 'from/:templateId',
				component: MonitoringSetupCustomTemplatePageComponent,
				canDeactivate: [canDeactivateMonitoringSetupCustomTemplateFn],
				data: {
					mode: EditMode.FROM_TEMPLATE
				}
			}
		]
	}
]

@NgModule({
	imports: [
		SharedModule,
		QuestionnaireEditorModule,
	],
	declarations: [
		MonitoringSetupCustomTemplatePageComponent
	],
	providers: [
		...routes.map(overrideRoute),
		provideTranslationMap('MONITORING_SETUP_CUSTOM_TEMPLATE', { de, en })
	]
})
export class MonitoringSetupCustomTemplatesModule {}
