import	{
			Component,
		}								from '@angular/core'

import	{
			CalendarDateString,
			isFinite
		}								from '@rcc/core'

import	{
			MedicalFeatureModule,
		}								from '@rcc/common'

import	{
			DataViewControl,
			Datapoint,
			getPreviousRange
		}								from '../../data-visualization'

import	{
			ComputedValueComponent
		}								from '../computed-value-component.class'
import	{
			CommonComputedValueComponent
		}								from '../common-computed-value/common-computed-value.component'


/**
 * {@link ComputedValueComponent} that calculates the difference
 * of 'true' counts over a given period of time.
 */
@Component({
	template:	'<common-computed-value [icon]="icon" [value]="value" [displayValue]="displayValue"></common-computed-value>',
	selector:	'rcc-boolean-count-computed-value',
	standalone: true,
	imports:	[
					MedicalFeatureModule,
					CommonComputedValueComponent,
				]
})
export class BooleanCountComputedValueComponent extends ComputedValueComponent {
	public static description :	string = 'COMPUTED_BOOLEAN_COUNT.DESCRIPTION'

	/**
	 * Matches boolean type questions
	 */
	public static match(dataViewControl: DataViewControl): boolean {

		if(!dataViewControl.datasets) 		return false
		if(!dataViewControl.datasets[0]) 	return false

		return dataViewControl.datasets[0].question.type === 'boolean'

	}

	/**
	 * Calculates the difference between 'true' counts in the current date range
	 * vs the previous date range
	*/
	public static getRawValue(dataViewControl: DataViewControl) : unknown {

		const datapoints	: 	Datapoint[]
								= 	dataViewControl.datasets[0].datapoints

		const startDate		: 	string	= dataViewControl.startDate
		const endDate		: 	string	= dataViewControl.endDate

		const [previousStartDate, previousEndDate]	: [string, string]
													= getPreviousRange(startDate, endDate)

		const currentCount		:	number
								=	this.getRangeTrueCount(datapoints, startDate, endDate)

		const previousCount		:	number
								=	this.getRangeTrueCount(datapoints, previousStartDate, previousEndDate)

		if (!isFinite(currentCount))	return null
		if (!isFinite(previousCount))	return null

		return currentCount - previousCount
	}

	/**
	 * Count 'true' occurrences in datapoints over between startDate and endDate
	 */
	private static getRangeTrueCount(datapoints: Datapoint[], startDate: string, endDate: string): number {
		return	datapoints
				.filter(datapoint =>
							datapoint.value === true
							&& CalendarDateString.isWithinInterval(datapoint.date, startDate, endDate)
						)
				.length
	}
}
