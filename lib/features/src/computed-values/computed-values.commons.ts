import	{
			InjectionToken,
			Provider
		}								from '@angular/core'

import	{
			getProvider
		}								from '@rcc/common'

import	{
			ComputedValueComponent
		}								from './computed-value-component.class'


export const COMPUTED_VALUES 	: InjectionToken<ComputedValueComponent>
								= new InjectionToken<ComputedValueComponent>('Computed Values')

/**
 * A extension of a {@link ComputedValueComponent} provided with this method,
 * can be used in other parts of the app to render a derived value bases on some
 * {@link DataViewControl} that passes {@link ComputedValueComponent.match}.
 *
 */
export function provideComputedValue(computedValueComponent: typeof ComputedValueComponent): Provider {
	return getProvider(COMPUTED_VALUES, computedValueComponent, true)
}
