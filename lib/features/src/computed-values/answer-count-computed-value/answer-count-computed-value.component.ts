import	{ Component }						from '@angular/core'
import	{
			CalendarDateString,
			isFinite
		}									from '@rcc/core'
import	{
			DataViewControl,
			Datapoint,
			getPreviousRange,
		}									from '../../data-visualization'
import	{ ComputedValueComponent }			from '../computed-value-component.class'
import	{ CommonComputedValueComponent } 	from '../common-computed-value/common-computed-value.component'
import	{ MedicalFeatureModule }			from '@rcc/common'

/**
 * {@link ComputedValueComponent} that calculates the difference
 * in the number of answers over a given period of time.
 */
@Component({
	template:	'<common-computed-value [icon]="icon" [value]="value" [displayValue]="displayValue"></common-computed-value>',
	selector	: 'rcc-answer-count-computed-value',
	standalone	: true,
	imports		: [
		MedicalFeatureModule,
		CommonComputedValueComponent,
	]
})
export class AnswerCountComputedValueComponent extends ComputedValueComponent {
	public static description: string = 'COMPUTED_ANSWER_COUNT.DESCRIPTION'

	public static match(): boolean {
		return true
	}

	public static getRawValue(dataViewControl: DataViewControl): number {
		const datapoints		: 	Datapoint[]
								= 	dataViewControl.datasets[0].datapoints

		const currentStartDate	: 	string
								= 	dataViewControl.startDate

		const currentEndDate	: 	string
								= 	dataViewControl.endDate

		const [previousStartDate, previousEndDate]
								=	getPreviousRange(currentStartDate, currentEndDate)

		const currentCount		: number
								= this.getRangeCount(datapoints, currentStartDate, currentEndDate)
		
		const previousCount		: number
								= this.getRangeCount(datapoints, previousStartDate, previousEndDate)

		if(!isFinite(currentCount))		return null
		if(!isFinite(previousCount))	return null

		return currentCount - previousCount
	}

	private static getRangeCount(datapoints: Datapoint[], startDate: string, endDate: string): number {
		return	datapoints
				.filter(datapoint => CalendarDateString.isWithinInterval(datapoint.date, startDate, endDate))
				.length
	}
}
