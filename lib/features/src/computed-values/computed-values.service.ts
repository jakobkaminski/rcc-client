import	{
			Injectable,
			Inject,
			Optional
		}								from '@angular/core'

import	{
			DataViewControl
		}								from '../data-visualization'


import	{
			COMPUTED_VALUES
		}								from './computed-values.commons'

import	{
			ComputedValueComponent
		}								from './computed-value-component.class'


/**
 * This service manages {@link ComputedValueComponent}s.
 */
@Injectable({
	providedIn: 'root'
})
export class RccComputedValuesService {


	public constructor(
		@Optional() @Inject(COMPUTED_VALUES)
		protected computedValueComponents : (typeof ComputedValueComponent)[]

	){
		this.computedValueComponents = computedValueComponents || []
	}

	/**
	 * @returns all {@link ComputedValueComponent}s that are compatible with the
	 * provided {@link DataViewControl}. See {@link ComputedValueComponent.match}
	 */
	public getComputedValues(dataViewControl: DataViewControl) : (typeof ComputedValueComponent)[]  {

		return 	this.computedValueComponents
				.filter( computedValueComponent => computedValueComponent.match(dataViewControl) )
	}

}
