import	{
			Component,
			Input
		}									from '@angular/core'
import	{
			isFinite
		}									from '@rcc/core'
import	{
			DataViewControl,
			getAbsValueString,
			getIcon
		}									from '../data-visualization'


/** *
 *
 * These values are kind of arbitrary, but bare with me.
 * The idea with these three options is to sort all possible
 * values of a specific {@link ComputedValueComponent} into categories.
 *
 * These categories come with little meaning, except that values in each
 * category are somehow related to the other values in the same category
 * and somehow different from values of all the other categories.
 *
 * This allows the rest of the UI to display values of one category differently
 * from the values of another category, in an arbitrary but not random way.
 *
 * The categories come with a tiny bit of extra meaning:
 *
 * * Putting values into 'N' indicates to the UI, that these values give little
 * reason to be highlighted at all.
 * * Putting values in 'A' indicates to the UI, that if a group of values
 * needed to be highlighted more prominently, it was this one.
 * * Putting values in 'B' indicates to the UI, that if a group of values
 * needed to be highlighted more prominently , it was _NOT_ this one.
 *
 * Example:
 *
 * * 'A' : all positive values
 * * 'B' : all negative values
 * * 'N' : 0 values
 *
 * We are __not__ indicating to the UI that 'A' is good or 'B' is bad, instead
 * we tell the UI that A and B can be highlighted differently and if need be, A
 * should be the category to be highlighted more prominently. Also If the UI
 * had to pick some values to be rather non descriptive, it should be the 0
 * value.
 *
 */

export type ComputedValueCategory = 'A' | 'B' | 'N'



/**
 * This component represents a derived/computed value based on values provided
 * by answers (e.g. the most pick answer of a period of time or arithmetic mean
 * of a number valued answer).
 *
 * Components of this type are serving two closely connected purposes:
 *
 * 1. They calculate a derived value from a set of datapoints provided in a
 * {@link DataViewControl}, e.g. the average. (static side)
 * 2. They provide a way to render this value, e.g. as an icon, an image or
 * just text. (instance side)
 *
 * The second purpose is the reason why this a Component.
 *
 * The first purpose is built into the component, because the rendering part
 * is not meant to be standalone or used anywhere else besides rendering exactly
 * the value from the calculating part.
 *
 * Calculating the value happens in a static method, because the component will
 * be rendered dynamically, but we might need the value before the component is
 * rendered to enable the rest of the UI to take it into account, for e.g. a
 * border color around the box where the rendered component will be placed in.
 */
@Component({
	template: '{{value}}'
})
export class ComputedValueComponent {

	// STATIC:

	/**
	 * Meant to be overwritten by child class.
	 *
	 * A translation string serving as a description of what kind of computation
	 * this component performs.
	 */
	public static description	: string
								= undefined


	/**
	 * Meant to be overwritten by child class.
	 *
	 * Checks if the calculation performed below, make sense for the
	 * provided data.
	 *
	 * @returns	true, iff the output works for a data set and is supposed to be
	 * 			displayed.
	 */
	public static match(dataViewControl: DataViewControl): boolean {

		const params : string = JSON.stringify(dataViewControl)
		throw new Error(`Extension of ComputedValueComponent: missing implementation of static getRawValue(), ${params}`)

		return false
	}

	/**
	 * Meant to be overwritten by child class.
	 *
	 * Calculates a derived value from the data given in the provided
	 * {@link DataViewControl}.
	 *
	 * A return value of null means that the computation failed.
	 */
	public static getRawValue(dataViewControl: DataViewControl) : unknown {

		const params : string = JSON.stringify(dataViewControl)
		throw new Error(`Extension of ComputedValueComponent: missing implementation of static getRawValue(), ${params}`)
	}

	/**
	 * Meant to be overwritten by child class.
	 * Sets positive values as 'A', negative values as 'B' and 0 as 'N' by default.
	 *
	 * Expects a value from the above {@link getRawValue()} method.
	 * @returns the supposed category of a calculated value. (see {@link ComputedValueCategory})	 *
	 */
	public static getValueCategory(value: unknown) : ComputedValueCategory {

		if (!isFinite(value))			return null

		if(value > 0)	return 'A'
		if(value < 0)	return 'B'

		return 'N'
	}

	// INSTANCE:

	/**
	 * Expects the raw value back from the above {@link getRawValue()} method,
	 * possibly processed by some other part of the app in between.
	 *
	 */
	@Input()
	public value: unknown

	protected get icon(): string {
		return getIcon(this.value)
	}

	protected get displayValue(): string {
		return getAbsValueString(this.value)
	}
}
