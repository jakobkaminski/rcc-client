import { NgModule } from '@angular/core'
import { provideTranslationMap } from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	providers: [
		provideTranslationMap('COMPUTED_VALUES_EXPLANATION', { en, de }),
	],
})
export class ComputedValuesExplanationTranslationModule {}
