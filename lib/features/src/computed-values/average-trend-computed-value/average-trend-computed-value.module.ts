import	{	NgModule								}	from '@angular/core'
import	{
			MedicalFeatureModule
		}												from '@rcc/common/medical/medical-feature/medical-feature.module'
import	{	AverageTrendComputedValueComponent		}	from './average-trend-computed-value.component'
import	{
			provideComputedValue
		}												from '../computed-values.commons'
import	{	provideTranslationMap					}	from '@rcc/common'

const de: Record<string, string> = {
	'DESCRIPTION_LABEL': 'Durchschnitt dieses Zeitraums vs. dem des letzten Zeitraums'
}

const en: Record<string, string> = {
	'DESCRIPTION_LABEL': 'This timeframe\'s average vs. last timeframe\'s average'
}

@NgModule({
	imports:[
		MedicalFeatureModule
	],
	providers: [
		provideComputedValue(AverageTrendComputedValueComponent),
		provideTranslationMap('COMPUTED_VALUE_AVERAGE', { de, en }),
	]
})
export class AverageTrendComputedValueModule{}
