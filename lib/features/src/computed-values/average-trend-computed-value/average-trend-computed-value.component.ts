import	{
			Component,
		}								from '@angular/core'

import	{
			CalendarDateString,
			isFinite
		}								from '@rcc/core'

import	{
			MedicalFeatureModule
		}								from '@rcc/common/medical/medical-feature/medical-feature.module'

import	{
			DataViewControl,
			Dataset,
			Datapoint,
			getPreviousRange,
		}								from '../../data-visualization'

import	{
			ComputedValueComponent
		}								from '../computed-value-component.class'
import	{
			CommonComputedValueComponent
		}								from '../common-computed-value/common-computed-value.component'

/**
 * {@link ComputedValueComponent} that calculates the change of the
 * arithmetic mean of values over a given period of time.
 */
@Component({
	template:	'<common-computed-value [icon]="icon" [value]="value" [displayValue]="displayValue"></common-computed-value>',
	selector:	'rcc-average-trend-computed-value',
	standalone:	true,
	imports:[
		MedicalFeatureModule,
		CommonComputedValueComponent,
	]
})
export class AverageTrendComputedValueComponent extends ComputedValueComponent {
	public static description : string = 'COMPUTED_VALUE_AVERAGE.DESCRIPTION_LABEL'

	public static match(dataViewControl: DataViewControl): boolean {

		if(!dataViewControl.datasets) 		return false
		if(!dataViewControl.datasets[0]) 	return false

		const question 			: Dataset['question']
								= dataViewControl.datasets[0].question

		const isDecimal			: boolean
								= question.type === 'decimal'

		const isInteger			: boolean
								= question.type === 'integer'

		const isNumerical		: boolean
								= isDecimal || isInteger

		return isNumerical

	}

	public static getRawValue(dataViewControl: DataViewControl) : unknown {

		const datapoints		: 	Datapoint[]
								= 	dataViewControl.datasets[0].datapoints

		const currentStartDate	: 	string
								= 	dataViewControl.startDate

		const currentEndDate	: 	string
								= 	dataViewControl.endDate

		const [previousStartDate, previousEndDate]
								= getPreviousRange(currentStartDate, currentEndDate)

		const currentAverage	:	number
								=	this.getRangeAverage(datapoints, currentStartDate, currentEndDate)

		const previousAverage	:	number
								=	this.getRangeAverage(datapoints, previousStartDate, previousEndDate)

		if(!isFinite(currentAverage))	return null
		if(!isFinite(previousAverage))	return null

		if(previousAverage === 0)		return null

		return currentAverage - previousAverage
	}

	private static getRangeAverage(datapoints: Datapoint[], startDate: string, endDate: string) : number {

		const values	: 	number[]
						= 	datapoints
							.filter(dataPoint => CalendarDateString.isWithinInterval(dataPoint.date, startDate, endDate))
							.map(	datapoint	=> datapoint.value)
							.filter(isFinite)

		const sum		:	number
						=	values.reduce( (x, next) => x + next, 0)

		const average	:	number
						=	values.length > 0
							?	sum/values.length
							:	null

		return average
	}
}
