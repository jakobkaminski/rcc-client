import	{
			NgModule,
			ModuleWithProviders
		}										from '@angular/core'
import	{
			DevModule,
			TransmissionModule,
			IncomingDataServiceModule,
			provideTranslationMap
		}										from '@rcc/common'
import	{	WebSocketTransmissionService	}	from './websocket-transmission.service'
import	{	WEBSOCKET_DEFAULT_URL			}	from './websocket-transmission.commons'


import en from './i18n/en.json'
import de from './i18n/de.json'



@NgModule({
	providers: [
		WebSocketTransmissionService,
		provideTranslationMap('WEBSOCKET_TRANSMISSION_SERVICE', { en, de })
	],
	imports: [
		DevModule.note('WebsocketTransmissionModule'),
		TransmissionModule.forChild(WebSocketTransmissionService),
		IncomingDataServiceModule
	],
})
export class WebSocketTransmissionModule {


	public static forRoot(url:string): ModuleWithProviders<WebSocketTransmissionModule> {

		return 	{
					ngModule: 	WebSocketTransmissionModule,
					providers:	[
									{
										provide: 	WEBSOCKET_DEFAULT_URL,
										useValue: 	url
									}
								]
				}
	}
}
