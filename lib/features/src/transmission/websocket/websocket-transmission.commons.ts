import	{	InjectionToken		}	from '@angular/core'
import	{
			isErrorFree,
			assertProperty,
			assert
		}							from '@rcc/core'


export type WsTransmissionMeta = ['rcc-wst', string, string] // ["rcc-wst", channel, key]


export function assertWsTransmissionMeta(x: unknown) : asserts x is WsTransmissionMeta {
	assert(x instanceof Array, 			'assertWsTransmissionMeta: must be an array.')
	assert(x[0] === 'rcc-wst',			'assertWsTransmissionMeta: [0] must be \'rcc-wst.\'')
	assert(typeof x[1] === 'string',	'assertWsTransmissionMeta: [1] must be string.')
	assert(typeof x[2] === 'string', 	'assertWsTransmissionMeta: [2] must be string.')
}

export function isWsTransmissionMeta(x:unknown): x is WsTransmissionMeta {
	return isErrorFree( () => assertWsTransmissionMeta(x))
}



export interface WsTransmissionConfig {
	data	:	unknown,
	url		:	string
}

export function assertWsTransmissionConfig(x:unknown): asserts x is WsTransmissionConfig {

	assertProperty(x, 'url', 			'isWsTransmissionConfig: missing url.')

	assert(typeof x.url === 'string',	'isWsTransmissionConfig: url must be a string.')
	assert(x.url.match(/^wss:/),		'isWsTransmissionConfig: url must start with \'wss:\'.')

	assertProperty(x, 'data',			'isWsTransmissionConfig: missing data.')
}

export function isWsTransmissionConfig(x: unknown): x is WsTransmissionConfig {
	return isErrorFree( () => assertWsTransmissionConfig(x) )
}


export const WEBSOCKET_DEFAULT_URL	: InjectionToken<string>
									= new InjectionToken<string>('url of the websocket server')
