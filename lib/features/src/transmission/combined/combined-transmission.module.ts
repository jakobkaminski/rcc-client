import	{
			NgModule,
			ModuleWithProviders
		}										from '@angular/core'
import	{
			DevModule,
			TransmissionModule,
			IncomingDataServiceModule,
			provideTranslationMap
		}										from '@rcc/common'

import	{	CombinedTransmissionService	}		from './combined-transmission.service'

import	{
			CMB_SIGNAL_SERVER,
			CMB_STUN_SERVERS
		}										from './combined-transmission.commons'


import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	providers: [
		CombinedTransmissionService,
		provideTranslationMap('COMBINED_TRANSMISSION_SERVICE', { en, de }),
	],
	imports: [
		DevModule.note('CombinedTransmissionServiceModule'),
		TransmissionModule.forChild(CombinedTransmissionService),
		IncomingDataServiceModule
	],
})
export class CombinedTransmissionModule {


	public static forRoot(url:string, stunServers: string[]): ModuleWithProviders<CombinedTransmissionModule> {

		return 	{
					ngModule: 	CombinedTransmissionModule,
					providers:	[
									{
										provide: 	CMB_SIGNAL_SERVER,
										useValue: 	url
									},
									{
										provide: 	CMB_STUN_SERVERS,
										useValue: 	stunServers
									},

								]
				}
	}
}
