import 	{
			Component,
		} 							from '@angular/core'

import	{	FormControl 		}	from '@angular/forms'

import	{
			QuestionnaireService
		}							from '../questionnaire.service'


@Component({
	selector: 		'rcc-questionnaire.page',
	templateUrl: 	'./questionnaire.page.html',
	styleUrls: 		['./questionnaire.page.scss'],
})
export class QuestionnairePageComponent	 {

	public filterControl 	: FormControl					= new FormControl()

	public constructor(
		public questionnaireService: QuestionnaireService
	) { }

}
