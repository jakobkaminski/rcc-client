import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			provideMainMenuEntry,
			Action,
			WithPosition,
			getEffectivePosition
		}											from '@rcc/common'

import	{
			QuestionnaireServiceModule,
			QuestionnaireServiceHomePath
		}											from './questionnaire.module'

@NgModule({
	imports: [
	QuestionnaireServiceModule,
	],
})

export class QuestionnaireServiceMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `QuestionnaireServiceMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<QuestionnaireServiceMainMenuEntryModule> {

		const mainMenuEntry	: Action =	{
				icon: 			'question',
				label: 			'QUESTIONNAIRE.MENU_ENTRY',
				path:			QuestionnaireServiceHomePath,
				position:		getEffectivePosition(config, .5),
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	QuestionnaireServiceMainMenuEntryModule,
			providers
		}
	}

}
