import 	{
			NgModule,
			ModuleWithProviders,
			Type,
			PipeTransform
		} 								from '@angular/core'

import 	{
			RouterModule,
			Routes
		}								from '@angular/router'

import	{
			BaseMetaStoreModule,
			MetaAction,
			MetaStoreModule,
			provideItemRepresentation,
			provideTranslationMap,
			SharedModule,
			TranslationsModule,
			ProductOrFactory,
			MetaStoreConfig,
			ItemRepresentation,
		}										from '@rcc/common'

import	{
			Question,
			QuestionConfig,
			QuestionStore,
		}										from '@rcc/core'


import	{	QuestionnaireService			}	from './questionnaire.service'

import	{
			QUESTION_STORES,
			QUESTION_META_ACTIONS
		}										from './questionnaire.commons'

import	{	QuestionnairePageComponent		}	from './questionnaire.page/questionnaire.page'
import	{
			Id2QuestionPipe,
			AsAnswerToPipe,
			LabeledOptionsPipe
		}										from './questionnaire.pipes'
import 	{	QuestionLabelComponent	}			from './question-label/question-label.component'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes: Routes		=	[
									{ path: 'questionnaire',	component: QuestionnairePageComponent	},
								]

const questionnaireConfig: MetaStoreConfig<QuestionConfig, Question, QuestionStore>
							=  	{
									itemClass:			Question,
									serviceClass: 		QuestionnaireService,
								}

const itemRepresentation: ProductOrFactory<ItemRepresentation>
							=	{
									itemClass:			Question,
									icon:				'question',
									labelComponent:		QuestionLabelComponent,
								}

const pipes: Array<Type<PipeTransform>>
							=	[
									Id2QuestionPipe,
									AsAnswerToPipe,
									LabeledOptionsPipe
								]


export class MenuEntryQuestionnaireComponent {}



export const QuestionnaireServiceHomePath : string = '/questionnaire'


@NgModule({
	declarations: [
		QuestionnairePageComponent,
		QuestionLabelComponent,
		...pipes
	],
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		MetaStoreModule.forChild(questionnaireConfig),
		TranslationsModule
	],
	exports: [
		MetaStoreModule, // So other Module need not import this specifically
		...pipes
	],
	providers:[
		QuestionnaireService,
		provideTranslationMap('QUESTIONNAIRE', { en, de } ),
		provideItemRepresentation(itemRepresentation)
	]
})
export class QuestionnaireServiceModule extends BaseMetaStoreModule {

	public static 	forChild(
				stores			: Type<QuestionStore>[],
				metaActions?	: MetaAction<QuestionConfig, Question, QuestionStore>[]
			): ModuleWithProviders<QuestionnaireServiceModule> {


		const mwp: ModuleWithProviders<QuestionnaireServiceModule> = BaseMetaStoreModule.getModuleWithProviders<QuestionnaireServiceModule>(this, stores, metaActions, QUESTION_STORES, QUESTION_META_ACTIONS)
		return mwp

	}
}
