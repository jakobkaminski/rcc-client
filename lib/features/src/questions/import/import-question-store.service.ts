import 	{
			Injectable,
			OnDestroy,
			Injector
		} 							from '@angular/core'

import	{
			SubscriptionLike,
		}							from 'rxjs'
import	{
			filter,
			map,
			tap
		}							from 'rxjs/operators'

import	{
			Question,
			QuestionConfig,
			QuestionStore,
			assertQuestionConfig // remove
		}							from '@rcc/core'


import	{
			RccStorage,
			IncomingDataService,
		}							from '@rcc/common'

import	{
			QuestionnaireService
		}							from '../questionnaire'

@Injectable()
export class ImportQuestionStoreService extends QuestionStore implements OnDestroy {

	public readonly name = 'IMPORT_QUESTION_STORE.NAME'

	private subscriptions : SubscriptionLike[] = []

	public constructor(
		private incomingDataService	: IncomingDataService,
		private rccStorage			: RccStorage,
		private injector			: Injector
	){
		super(
			rccStorage.createItemStorage('rcc-import-questions'),
		)

		this.listenToIncomingDataService()
	}


	protected listenToIncomingDataService() : void {

		this.subscriptions.push(
			this.incomingDataService
			.pipe(
				tap( 	data => console.info('QUESTIONS IMPORT TAP', data)),
				map( 	(data		: unknown) 			=> Question.findConfigs(data) ),
				filter( (configs	: QuestionConfig[]) => configs.length > 0)
			)
			.subscribe( configs => void this.addQuestionConfig(configs) )
		)
	}


	public ngOnDestroy() : void {
		this.subscriptions.forEach( sub => sub.unsubscribe() )
	}



	public async addQuestionConfig(configs: QuestionConfig[])					: Promise<void>
	public async addQuestionConfig(config: 	QuestionConfig)						: Promise<void>
	public async addQuestionConfig(x: 		QuestionConfig | QuestionConfig[])	: Promise<void>
	{

		console.log('addQuestionConfig', x)
		console.log('assertQuestionConfig', assertQuestionConfig)

		if(!(x instanceof Array)) return await this.addQuestionConfig([x])

		const configs		= x
		const questionnaireService = this.injector.get(QuestionnaireService)

		await questionnaireService.ready

		const ids			= questionnaireService.items.map( question => question.id )

		configs
		.filter(	config => !ids.includes(config.id) )
		.forEach( 	config => this.addConfig(config) )

		return void await this.storeAll()

	}


	public async deleteQuestion(question: Question): Promise<void> {
		if(!this.removeItem(question)) throw new Error('CustomQuestionStoreService.delete: Unable to delete symptom check with id: ' + question.id)

		return void await this.storeAll()
	}


}
