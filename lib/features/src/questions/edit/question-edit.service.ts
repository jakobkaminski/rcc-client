import	{	Injectable					}	from '@angular/core'
import	{
			Question,
			QuestionConfig,
			uuidv4

		}									from '@rcc/core'
import	{
			RccTranslationService,
			RccModalController,
			ItemEditService,
			ItemEditResult,

		}									from '@rcc/common'
import	{	QuestionEditComponent		}	from './question-edit.component'


@Injectable()
export class QuestionEditService extends ItemEditService<Question> {


	public constructor(
		protected rccModalController 	: RccModalController,
		protected rccTranslationService	: RccTranslationService

	){
		super(
			Question,
			QuestionEditComponent,
			rccModalController,
		)
	}

	public async editCopy(item: Question, heading?: string) : Promise<ItemEditResult<Question>> {
		const editResult 	= await super.editCopy(item, heading)

		// Every change of a question has to result in a new ID.
		// The user can cancel the edit process to leave the original question unchanged and keep its ID.
		editResult.item.id 	= uuidv4()
		return editResult

	}

		/* This method is a copy of itemEditService.edit(). We cannot rely on super.edit() to invoke editCopy() (what it currently does, but  what could change in the future) So we have to ensure here that it uses the editCopy() we defined above. And with it assign a new ID  every time a question is edited. */

	public async edit(item: Question, heading?: string): Promise<ItemEditResult<Question>> {

		const result			= 	await this.editCopy(item, heading)

		if ( item ) item.config = result.item.config
		if (!item ) item 		= new this.itemClass(result.item.config)

		return { item, artifacts: result.artifacts }
	}

	public async create(heading? : string, startingConfig?: QuestionConfig): Promise<ItemEditResult<Question>> {

		const activeLanguage	=	this.rccTranslationService.activeLanguage
		const wording			=	this.rccTranslationService.translate('QUESTIONS.EDIT.CUSTOM_QUESTION_INITIAL_WORDING')

		startingConfig 			= 	startingConfig
									||
									{
										id: 			uuidv4(),
										type: 			'string',
										translations:	{
											[activeLanguage]: wording
										}
									}

		return await this.edit(null, heading)
	}

}
