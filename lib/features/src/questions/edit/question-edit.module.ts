import	{	NgModule					}	from '@angular/core'
import	{
			provideTranslationMap,
			SharedModule,
			WidgetsModule,
		}									from '@rcc/common'
import	{	QuestionEditComponent		}	from './question-edit.component'

import	{	QuestionEditService			}	from './question-edit.service'

import	{	QueriesModule				}	from '../../queries'

import	{	QuestionEditWidgetsModule	}	from '../question-edit-widgets'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	providers: [
		provideTranslationMap('QUESTIONS.EDIT', { en,de }),
		QuestionEditService,
	],
	imports: [
		SharedModule,
		QueriesModule,
		WidgetsModule,
		QuestionEditWidgetsModule
	],
	declarations: [
		QuestionEditComponent
	],

	exports: [
		QuestionEditComponent
	]
})
export class QuestionEditModule {}
