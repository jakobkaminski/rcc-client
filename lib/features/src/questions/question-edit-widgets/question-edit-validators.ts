import	{
			AbstractControl,
			ValidationErrors,
			FormGroup,
			FormArray
		}							from '@angular/forms'

import	{
			has,
			assert,
			assertProperty,
			AnswerType
		}							from '@rcc/core'


export function validateWording(control: AbstractControl<unknown>) : ValidationErrors | null {

	if(!has(control,'value') )		return { 'QUESTIONS.EDIT.VALIDATION.WORDING_EMPTY': true }

	const value = control.value

	if( typeof value !== 'string')	return { 'QUESTIONS.EDIT.VALIDATION.WORDING_EMPTY': true }
	if( value.trim() === '')		return { 'QUESTIONS.EDIT.VALIDATION.WORDING_EMPTY': true }

	return null

}


/**
 * Validator function factory.
 *
 * Validates the answer type. The Factory requires an array of valid answer types.
 *
 */
export function validateAnswerType(answerTypes: AnswerType[]) : (control: AbstractControl<unknown>) => ValidationErrors | null {

	return (control: AbstractControl<unknown>): ValidationErrors | null => {

		const value = control.value

		if( typeof value !== 'string')	return { 'QUESTIONS.EDIT.VALIDATION.ANSWER_TYPE_MISSING': true }
		if( value.trim() === '')		return { 'QUESTIONS.EDIT.VALIDATION.ANSWER_TYPE_MISSING': true }

		if( (answerTypes as string[]).includes(value) ) return null

		return { 'QUESTIONS.EDIT.VALIDATION.ANSWER_TYPE_UNKNOWN': true }

	}

}

/**
 * Validates FormControls for minimum and maximum answer values, when editing questions.
 * Controls for min and max must either be disabled or have a number value.
 * Expects a AbstractControl similar to this:
 *
 * ``FormGroup({
 * 		enableMin: 	FormControl<boolean>(),
 * 		min:		FormControl<number>(),
 * 		enableMax:	FormControl<boolean>(),
 * 		max:		FormControl<number>(),
 * })```
 */
export function validateMinMax(control : AbstractControl): ValidationErrors | null {

		assert(control 					instanceof FormGroup, 		`validateMinMax() expected ${FormGroup.name}, got: ${control.constructor.name}`)

		const subcontrols = control.controls

		assert(subcontrols.enableMin 	instanceof AbstractControl,	'validateMinMax() missing .enableMin control.', subcontrols)
		assert(subcontrols.min 			instanceof AbstractControl,	'validateMinMax() missing .min control.', 		subcontrols)
		assert(subcontrols.enableMax 	instanceof AbstractControl,	'validateMinMax() missing .enableMax control.', subcontrols)
		assert(subcontrols.max 			instanceof AbstractControl,	'validateMinMax() missing .max control.', 		subcontrols)

		subcontrols.min.updateValueAndValidity({ onlySelf: true, emitEvent: true })
		subcontrols.max.updateValueAndValidity({ onlySelf: true, emitEvent: true })

		const values	= control.value as Record<string, number|boolean>

		const enableMin	= values.enableMin
		const min 		= values.min

		const enableMax	= values.enableMax
		const max 		= values.max


		if(enableMin && (typeof min  !== 'number' || !Number.isFinite(min)) )	subcontrols.min.setErrors({ 'QUESTIONS.EDIT.VALIDATION.MIN_NAN' : true })
		if(enableMax && (typeof max  !== 'number' || !Number.isFinite(max)) )	subcontrols.max.setErrors({ 'QUESTIONS.EDIT.VALIDATION.MAX_NAN' : true })

		if(subcontrols.min.errors ||  subcontrols.max.errors) return null

		if(min >= max ){
			subcontrols.min.setErrors({ 'QUESTIONS.EDIT.VALIDATION.MIN_GE_MAX' : true })
			subcontrols.max.setErrors({ 'QUESTIONS.EDIT.VALIDATION.MIN_GE_MAX' : true })
		}

		return null

}

export interface ValidationOptions {
	/**
	 * When explicitly set, this option enforces a required label. When
	 * left as undefined, the validator using implicit rules for required
	 * labels (i.e. string types have required labels, numbers don't)
	 */
	labelRequired: boolean
}

/**
 * Validator function factory.
 *
 * Validates a FormGroup representing an answer Option. The Factory requires a
 * string or an AbstractControl representing the answer type.
 *
 * Can only be used on FormGroups similar to this:
 *
 * ``
 * 	FormGroup({
 * 		value: 	FormControl<string|number>(),
 * 		label:	FormControl<string>(),
 * 		//...
 * 	})
 * ```
 */
export function validateOption(answerType: AnswerType | AbstractControl<AnswerType>, options?: Partial<ValidationOptions>) : (control : AbstractControl) => (ValidationErrors | null) {

	// Asserts than the control this validation function is added to
	// has al the properties we need to perform the validation.
	function assertOptionControl(control : AbstractControl) : asserts control is FormGroup<{value: AbstractControl, label: AbstractControl}> {

		assert(control instanceof FormGroup, `validateOption() expected to be call with ${FormGroup.name}, got: ${control.constructor.name}`)

		const controls = control.controls

		assertProperty(controls, 'value', 'validateOption() missing .value')
		assertProperty(controls, 'label', 'validateOption() missing .label')

		const valueControl 	= controls.value
		const labelControl	= controls.label

		assert(valueControl	instanceof AbstractControl,	`validateOptions() expected .value on control to be a ${FormGroup.name}; got: ${valueControl.constructor.name}.`, controls)
		assert(labelControl	instanceof AbstractControl,	`validateOptions() expected .label on control to be a ${FormGroup.name}; got: ${labelControl.constructor.name}.`, controls)

	}

	// Checks the value of an option, depending on the answer type
	function isValidValue(type: AnswerType, value: unknown) : boolean {

		if(type === 'boolean') return typeof value === 'boolean'
		if(type === 'string') 	return typeof value === 'string'

		if(type === 'decimal') return Number.isFinite(value)
		if(type === 'integer') return Number.isInteger(value)

	}

	// Checks the label of an option; the result does not yet depend
	// on the answer type but might do so in the future.
	function isValidLabel(type: AnswerType, label: unknown) : boolean {
		const labelRequired: boolean | undefined = options?.labelRequired

		if (labelRequired === undefined) {
			if(type === 'integer')	return true
			if(type === 'decimal')	return true

			return typeof label === 'string' && label !== ''
		} else
			return labelRequired === false || label !== ''
	}


	// The actual validation function, varying with the answer type.
	return (control : AbstractControl) => {

		assertOptionControl(control)

		const answer_type 		= 	answerType instanceof AbstractControl
									?	answerType.value
									:	answerType

		const valueErrorMessage = `QUESTIONS.EDIT.VALIDATION.OPTIONS.BAD_VALUE.${answer_type.toUpperCase()}`
		const labelErrorMessage	= `QUESTIONS.EDIT.VALIDATION.OPTIONS.BAD_LABEL.${answer_type.toUpperCase()}`

		const controls			= control.controls

		controls.value.updateValueAndValidity({ onlySelf: true, emitEvent: true })
		controls.label.updateValueAndValidity({ onlySelf: true, emitEvent: true })

		if(!isValidValue(answer_type, controls.value.value)) controls.value.setErrors({ [valueErrorMessage]: true })
		if(!isValidLabel(answer_type, controls.label.value)) controls.label.setErrors({ [labelErrorMessage]: true })

		return null
	}
}


/**
 * Validator function factory.
 *
 * Validates FormArrays that contain controls for options, when editing questions.
 *
 * Checks if the number of options is correct depending on answer type and no duplicates exist.
 *
 * Can only be used on a FormArray like this:
 *
 * ``FormArray<FormGroup<{ value: FormControl<string|number>(), label: FormControl<string()}>>
 *
 * ```
 */
export function validateOptions(answerType: AnswerType | AbstractControl<AnswerType>) : (control : AbstractControl) => (ValidationErrors | null) {

	return (control : AbstractControl) : (ValidationErrors | null) => {


		assert(control instanceof FormArray, 							`validateOptions() expected ${FormArray.name}, got: ${control.constructor.name}`)

		assert(control.controls.every( c => c instanceof FormGroup), 	`validateOptions() all child conrollers must be instances of ${FormGroup.name}`)
		assert(control.controls.every( (c : FormGroup) => 'value' in c.controls), 	'validateOptions() all child conrollers must have a .value control')
		assert(control.controls.every( (c : FormGroup) => 'label' in c.controls), 	'validateOptions() all child conrollers must have a .label control')

		const optionControls = control.controls as FormGroup<{value: AbstractControl<unknown>, label:AbstractControl<string>}>[]

		optionControls.forEach( c => c.updateValueAndValidity({ onlySelf: true, emitEvent: true }) )

		const answer_type =	answerType instanceof AbstractControl
							?	answerType.value
							:	answerType


		// count:

		if(answer_type === 'boolean' && optionControls.length !== 2)	return { 'QUESTIONS.EDIT.VALIDATION.OPTIONS.BAD_COUNT_BOOLEAN' : true }
		if(answer_type === 'string'	 && optionControls.length < 2)		return { 'QUESTIONS.EDIT.VALIDATION.OPTIONS.BAD_COUNT' : true }
		if(answer_type === 'decimal' && optionControls.length < 2)		return { 'QUESTIONS.EDIT.VALIDATION.OPTIONS.BAD_COUNT' : true }
		if(answer_type === 'integer' && optionControls.length < 2)		return { 'QUESTIONS.EDIT.VALIDATION.OPTIONS.BAD_COUNT' : true }


		// duplicates:
		const values = optionControls.map( c => c.value.value)
		const labels = optionControls.map( c => c.value.label?.trim()).filter(c => !!c)

		optionControls.forEach( optionControl => {

			if(optionControl.invalid) return

			const valueControl = optionControl.controls.value
			const labelControl = optionControl.controls.label

			if( values.filter(v => v === valueControl.value ).length > 1) 			valueControl.setErrors({ 'QUESTIONS.EDIT.VALIDATION.OPTIONS.DUPLICATE_VALUE' : true })
			if( labels.filter(l => l === labelControl.value?.trim() ).length > 1)	labelControl.setErrors({ 'QUESTIONS.EDIT.VALIDATION.OPTIONS.DUPLICATE_LABEL' : true })
		})

	}

}
