import 	{	WidgetControl 				}	from '@rcc/common'
import	{
			AbstractControl,
			FormControl,
		}									from '@angular/forms'

import	{
			BehaviorSubject
		}									from 'rxjs'

import	{
			QuestionConfig,
		}									from '@rcc/core'


export class QuestionEditControl extends WidgetControl {



	/**
	 * The current edit of the question Config.
	 */
	public questionConfig$		: BehaviorSubject<QuestionConfig>

	/**
	 * Collection of forms or single form, the active widget uses to collect values.
	 * This object will be used to determine, if relvant forms have errors.
	 * So make sure to add all relevant forms from within your widget.
	 * */
	public editForm$			= new  BehaviorSubject<AbstractControl>(new FormControl<null>(null) )


	public constructor(
		/**
		 * The original question config if you edit an existing question or null
		 * if you create a new one.
		 */
		readonly	originalQuestionConfig 	: QuestionConfig | null,
	){

		super()

		this.questionConfig$ = new BehaviorSubject(originalQuestionConfig)
	}


	/**
	 * Emits the value on  {@link QuestionEditControl#questionConfig$}
	 */
	public set questionConfig(config: QuestionConfig)	{ this.questionConfig$.next(config) }

	/**
	 * Access {@link QuestionEditControl#questionConfig$} without the need to subscribe
	 */
	public get questionConfig(): QuestionConfig 		{ return this.questionConfig$.value }

	/**
	 * Emits the value on  {@link QuestionEditControl#editForm$}
	 */
	public set editForms(control: AbstractControl)		{ this.editForm$.next(control) }

	/**
	 *  Access {@link QuestionEditControl#editForm$} without the need to subscribe
	 */
	public get editForms(): AbstractControl			{ return this.editForm$.value  }

}
