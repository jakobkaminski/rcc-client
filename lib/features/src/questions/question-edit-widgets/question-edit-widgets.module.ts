import	{	NgModule							} from '@angular/core'
import	{	FallbackQuestionEditWidgetsModule	} from './fallback-question-edit-widgets'
import	{	provideTranslationMap				} from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	imports:[
		FallbackQuestionEditWidgetsModule
	],
	providers:[
		provideTranslationMap('QUESTIONS.EDIT', { en,de })
	]


})
export class QuestionEditWidgetsModule {

}
