import	{
			Component,
			ElementRef,
			OnDestroy
		} 											from '@angular/core'

import	{
			Subject,
			takeUntil,
			skip
		}											from 'rxjs'

import	{
			Question,
			CalendarDateString,
			hasOptions,
			indexArray,
			QuestionOptionConfig,
		}											from '@rcc/core'

import	{
			RccTranslationService
		}											from '@rcc/common'

import	{
			MultipleChoice,
			BaseChartConfig,
			OptionBasics,
			OptionInteger,
			ConsumableDataForGroupCharts,
			COLORS_DEFAULT
		}											from '@rcc/figures.cc'

import	{
			DataViewWidgetComponent,
			DataViewControl,
			Dataset,
			Datapoint,
		} 											from '../../data-visualization'

import	{
			locales,
			getConsumableChartData,
			getChartOptions,
			getChartDimensions
		}											from '../complex-data-view-widgets.commons'



/**
 * This DatiaView widget visualizes all questions that take single choice answers.
 * As second data set it can handle any question without options, that takes
 * string answers. This is intended to accommodate the daily notes question, but
 * the widget can handle any such question. A second data set is not required.
 */
@Component({
	templateUrl:'../complex-data-view-widgets.commons.html',
	styleUrls:	['../complex-data-view-widgets.commons.css'],
})
export class SingleChoiceDataViewWidgetComponent extends DataViewWidgetComponent implements OnDestroy {

	// STATIC:

	public static widgetMatch(dataViewControl: DataViewControl): number {
		const primaryDataset 	: Dataset
								= dataViewControl.datasets[0]

		if(!primaryDataset)
			return -1

		const primaryQuestion	: Question
								= primaryDataset.question

		if(primaryQuestion.type !== 'string')
			return -1
		if(!hasOptions(primaryQuestion.config))
			return -1

		const secondaryDataset 	: Dataset
								= dataViewControl.datasets[1]

		const secondaryQuestion	: Question
								= secondaryDataset?.question

		if(secondaryDataset)
			if(secondaryQuestion?.type === 'string')
				if(!hasOptions(secondaryQuestion.config))
					return 2
				else
					return 1
			else
				return -1

		return 1
	}




	// INSTANCE:

	private chartSvg			: SVGElement
								= undefined

	private destroy$			: Subject<void>
								= new Subject<void>()

	public constructor(
		public	dataViewControl			: DataViewControl,

		protected elementRef			: ElementRef<HTMLElement>,
		protected rccTranslationService	: RccTranslationService
	){
		super(dataViewControl)

		const startDate			: 	Date
								= 	CalendarDateString.toDate(dataViewControl.startDate)

		const endDate			: 	Date
								= 	CalendarDateString.toDate(dataViewControl.endDate)
		
		const integerDataView	:	DataViewControl
								=	this.convertDataViewControlToInteger(dataViewControl)

		const options			:	OptionInteger[]
								=	getChartOptions(integerDataView) as OptionInteger[]

		const data				:	ConsumableDataForGroupCharts[]
								=	getConsumableChartData(integerDataView, 'multipleChoice', options) as ConsumableDataForGroupCharts[]

		const dimensions		:	BaseChartConfig<ConsumableDataForGroupCharts>['dimensions']
								=	getChartDimensions(dataViewControl)

		const chart 			:	MultipleChoice
								= 	new MultipleChoice({
										...this.commonChartOptions,
										data,
										options,
										startDate,
										endDate,
										timePeriod: dataViewControl.scope,
										language: rccTranslationService.activeLanguage,
										dimensions,
										locales,
										styles: {
											color: {
												primary:	'var(--main)',
												inactive:	COLORS_DEFAULT.greyBack,
												scale:		new Map(options.map((value: OptionBasics) => [value.value, 'var(--main)']))
											},
											note: {
												backgroundColor: 'var(--supp-highlight-secondary)'
											}
										},
									})


		this.rccTranslationService.activeLanguageChange$
		.pipe(
			skip(1),
			takeUntil(this.destroy$)
		)
		.subscribe( (lang : string) => chart.updateLanguage(lang) )


		this.chartSvg				=	chart.svg.node()

		this.chartSvg.style.width 	= 	'100%'

		this.elementRef.nativeElement.append(this.chartSvg)

		chart.render()
	}

	public ngOnDestroy(): void {
		this.chartSvg.remove()
		this.destroy$.next()
		this.destroy$.complete()
	}

	private convertDataViewControlToInteger(dataViewControl: DataViewControl): DataViewControl {
		const mainDataset: Dataset = dataViewControl.datasets[0]
		const valueToIndexMap: Map<string | number | boolean, { option: QuestionOptionConfig, index: number }> = indexArray(mainDataset.question.options.map((option, index) => ({ option, index })), item => item.option.value)

		const dataPointsConvertedToIntegers: Datapoint[] = mainDataset.datapoints.map(
				(datapoint: Datapoint) => {
					const index: number = valueToIndexMap.get(datapoint.value as string | number | boolean).index
					return { ...datapoint, value: index }
				}
		)

		const convertedQuestion: Question = new Question({
			...mainDataset.question.config,
			options: mainDataset.question.options.map(option => ({
				...option,
				value: valueToIndexMap.get(option.value).index
			})),
			type: 'integer'
		})

		const convertedDataset: Dataset = {
			...mainDataset,
			datapoints: dataPointsConvertedToIntegers,
			question: convertedQuestion,
		}

		return ({
			...dataViewControl,
			datasets: [
				convertedDataset,
				...dataViewControl.datasets.slice(1)
			]
		}) as DataViewControl
	}

}

