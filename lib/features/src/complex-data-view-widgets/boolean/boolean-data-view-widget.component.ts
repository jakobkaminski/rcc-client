import	{
			Component,
			ElementRef,
			OnDestroy
		} 											from '@angular/core'

import	{
			Subject,
			takeUntil,
			skip
		}											from 'rxjs'

import	{
			Question,
			CalendarDateString,
		}											from '@rcc/core'

import	{
			RccTranslationService
		}											from '@rcc/common'

import	{
			YesNoChart,
			BaseChartConfig,
			COLORS_DEFAULT,
			ConsumableDataBase,
			ConsumableDataForYesNo,
			OptionBoolean
		}											from '@rcc/figures.cc'

import	{
			DataViewWidgetComponent,
			DataViewControl,
			Dataset,
		} 											from '../../data-visualization'

import	{
			locales,
			getConsumableChartData,
			getChartOptions,
			getChartDimensions
		}											from '../complex-data-view-widgets.commons'



/**
 * This DatView widget visualizes all questions that take boolean answers.
 * As second data set it can handle any question without options, that takes
 * string answers. This is intended to accommodate the daily notes question, but
 * the widget can handle any such question. A second data set is not required.
 */
@Component({
	templateUrl:'../complex-data-view-widgets.commons.html',
	styleUrls:	['../complex-data-view-widgets.commons.css'],
})
export class BooleanDataViewWidgetComponent extends DataViewWidgetComponent implements OnDestroy {

	// STATIC:

	public static widgetMatch(dataViewControl: DataViewControl): number {

		const primaryDataset 	: Dataset
								= dataViewControl.datasets[0]


		if(!primaryDataset) return -1

		const primaryQuestion	: Question
								= primaryDataset.question


		if(primaryQuestion.type !== 'boolean') return -1

		const secondaryDataset 	: Dataset
								= dataViewControl.datasets[1]

		const secondaryQuestion	: Question
								= secondaryDataset?.question

		if(secondaryDataset && secondaryQuestion?.type !== 'string') return -1


		return 1
	}




	// INSTANCE:

	private chartSvg			: SVGElement
								= undefined

	private destroy$			: Subject<void>
								= new Subject<void>()

	public constructor(

		public 	dataViewControl			: DataViewControl,

		protected elementRef			: ElementRef<HTMLElement>,
		protected rccTranslationService	: RccTranslationService
	){
		super(dataViewControl)

		const startDate			: 	Date
								= 	CalendarDateString.toDate(dataViewControl.startDate)

		const endDate			: 	Date
								= 	CalendarDateString.toDate(dataViewControl.endDate)

		const data 				:	ConsumableDataForYesNo[]
								=	getConsumableChartData(dataViewControl, 'yesNo') as ConsumableDataForYesNo[]

		const options			:	OptionBoolean[]
								=	getChartOptions(dataViewControl) as OptionBoolean[]

		const dimensions		:	BaseChartConfig<ConsumableDataBase>['dimensions']
								=	getChartDimensions(dataViewControl)

		const chart 			:	YesNoChart
								= 	new YesNoChart({
										...this.commonChartOptions,
										data,
										options,
										startDate,
										endDate,
										timePeriod: dataViewControl.scope,
										language: rccTranslationService.activeLanguage,
										dimensions,
										locales,
										styles: {
											color: {
												primary: 'var(--main)',
												inactive: COLORS_DEFAULT.greyBack,
											},
											note: {
												backgroundColor: 'var(--supp-highlight-secondary)'
											}
										}
									})

		this.rccTranslationService.activeLanguageChange$
		.pipe(
			skip(1),
			takeUntil(this.destroy$)
		)
		.subscribe( (lang : string) => chart.updateLanguage(lang) )


		this.chartSvg				=	chart.svg.node()

		this.chartSvg.style.width 	= 	'100%'

		this.elementRef.nativeElement.append(this.chartSvg)

		chart.render()
	}

	public ngOnDestroy(): void {
		this.chartSvg.remove()
		this.destroy$.next()
		this.destroy$.complete()
	}
}

