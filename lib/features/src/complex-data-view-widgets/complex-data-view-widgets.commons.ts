import	{
			TimeLocaleDefinition
		}											from 'd3-time-format'

import	{
			BaseChartConfig,
			BaseChartWithOptionsConfig,
			DatapointBasics,
			generateConsumableDataByTimePeriod,
			RawDataString,
			ChartType,
			OptionInteger,
			ConsumableDataBase,
		}											from '@rcc/figures.cc'

import	{
			assert,
			CalendarDateString,
			differenceInCalendarDays
		}											from '@rcc/core'

import	{
			DataViewControl,
			Dataset,
			Datapoint
		}											from '../data-visualization'


/**
 * Calculates the dimension for a figures.cc chart. Use this function to ensure
 * that all the charts align properly.
 */
export function getChartDimensions(dataViewControl?: DataViewControl, width: number = 400): BaseChartConfig<ConsumableDataBase>['dimensions']{
	return {
				width,
				margin: {
					top: 0,
					right: 0,
					bottom: Math.max(width*0.05, 32),
					left: Math.max(width*0.2, 54),
				},
			}
}


/**
 * Converts rcc data to data consumable by figures.cc charts
 */
export function getConsumableChartData(dataViewControl: DataViewControl, chartType: ChartType, options?: OptionInteger[]):	BaseChartConfig<any>['data']{

	if(!dataViewControl) return []

	assert(dataViewControl.datasets[1]?.question.type === 'string', 'getConsumableChartData: secondary dataset must refer to a question with string answers.')

	const mainDataPoints 	: 	Datapoint[]
							= 	dataViewControl.datasets[0].datapoints
								.map( datapoint => ({
									...datapoint,
									note: datapoint.note || ''		// Workaround: The charts expect note to always be a string (not null)
								}))

	const notesDataset		: 	Dataset
							= 	dataViewControl.datasets[1]

	const startDate			: 	Date
							= 	CalendarDateString.toDate(dataViewControl.startDate)

	const endDate			: 	Date
							= 	CalendarDateString.toDate(dataViewControl.endDate)

	const consumableData 	:	BaseChartConfig<any>['data']
							=	generateConsumableDataByTimePeriod({
									dataPoints:		mainDataPoints 	as DatapointBasics[],
									notes:			notesDataset 	as unknown as RawDataString,
									startDate:		startDate,
									daysAhead:		differenceInCalendarDays(endDate,startDate),
									timePeriod: 	dataViewControl.scope,
									chartType:		chartType,
									options,
								})

	return consumableData
}

/**
 * Options are meant to be used as y-Axis labels for certain figures.cc charts.
 * They are just a copy of rcc question options.
 */
export function getChartOptions(dataViewControl: DataViewControl) : BaseChartWithOptionsConfig<any,any>['options'] | undefined {
	if(!dataViewControl) return undefined
	return dataViewControl.datasets[0]?.question.options
}


export const localeDE: TimeLocaleDefinition = {

	dateTime:	'%A, der %e. %B %Y, %X',
	date:	'%d.%m.%Y',
	time:	'%H:%M:%S',
	periods:	[
				'AM', 'PM'
			],
	days: [
		'Sonntag',
		'Montag',
		'Dienstag',
		'Mittwoch',
		'Donnerstag',
		'Freitag',
		'Samstag',
	],
	shortDays: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
	months: [
		'Januar',
		'Februar',
		'März',
		'April',
		'Mai',
		'Juni',
		'Juli',
		'August',
		'September',
		'Oktober',
		'November',
		'Dezember',
	],
	shortMonths: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
}

export const localeEN : TimeLocaleDefinition = {

	dateTime:	'%A, %e. %B %Y, %X',
	date:	'%d.%m.%Y',
	time:	'%H:%M:%S',
	periods:	[
				'AM', 'PM'
			],
	days: [
		'Sunday',
		'Monday',
		'Tuesday',
		'Wednesday',
		'Thursday',
		'Friday',
		'Saturday',
	],
	shortDays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	months: [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'Juli',
		'August',
		'September',
		'October',
		'November',
		'December',
	],
	shortMonths: ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'],
}



/**
 * These objects are used for various chart labels.
 */
export const locales : BaseChartConfig<any>['locales'] = {
	de: localeDE,
	en: localeEN
}
