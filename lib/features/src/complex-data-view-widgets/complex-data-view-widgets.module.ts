import 	{
			NgModule,
			Type
		}												from '@angular/core'

import	{
			SharedModule,
			WidgetsModule,
			provideWidget,
		} 												from '@rcc/common'

import	{
			DataViewWidgetComponent
		}												from '../data-visualization'

import	{	BooleanDataViewWidgetComponent			}	from './boolean'
import	{	LabeledIntegersDataViewWidgetComponent	}	from './labeled-integers'
import	{	RawNumbersDataViewWidgetComponent		}	from './raw-numbers'
import	{	SingleChoiceDataViewWidgetComponent		}	from './single-choice'


const dataViewWidgets 	:	Type<DataViewWidgetComponent>[]
						=	[
								BooleanDataViewWidgetComponent,
								LabeledIntegersDataViewWidgetComponent,
								RawNumbersDataViewWidgetComponent,
								SingleChoiceDataViewWidgetComponent,
							]

@NgModule({
	imports: [
		SharedModule,
		WidgetsModule,
	],
	providers:[
		...dataViewWidgets.map(provideWidget),
	],
	declarations: [
		...dataViewWidgets,
	],
	exports: [
		...dataViewWidgets
	]
})
export class ComplexDataViewWidgetsModule { }
