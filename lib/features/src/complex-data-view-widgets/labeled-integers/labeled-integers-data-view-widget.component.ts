import	{
			Component,
			ElementRef,
			OnDestroy
		} 											from '@angular/core'

import	{
			Subject,
			takeUntil,
			skip
		}											from 'rxjs'

import	{
			Question,
			CalendarDateString,
			QuestionOptionConfig,
			hasOptions,
		}											from '@rcc/core'

import	{
			RccTranslationService
		}											from '@rcc/common'

import	{
			QuantitiesChart,
			BaseChartConfig,
			BaseChartWithOptionsConfig,
			OptionBasics,
			OptionInteger,
			ConsumableDataForSingleBars,
			COLORS_DEFAULT
		}											from '@rcc/figures.cc'

import	{
			DataViewWidgetComponent,
			DataViewControl,
			Dataset,
		} 											from '../../data-visualization'

import	{
			locales,
			getConsumableChartData,
			getChartOptions,
			getChartDimensions
		}											from '../complex-data-view-widgets.commons'



/**
 * This DatiaView widget visualizes all questions that take single choice answers.
 * As second data set it can handle any question without options, that takes
 * string answers. This is intended to accommodate the daily notes question, but
 * the widget can handle any such question. A second data set is not required.
 */
@Component({
	templateUrl:'../complex-data-view-widgets.commons.html',
	styleUrls:	['../complex-data-view-widgets.commons.css'],
})
export class LabeledIntegersDataViewWidgetComponent extends DataViewWidgetComponent implements OnDestroy {

	// STATIC:

	public static widgetMatch(dataViewControl: DataViewControl): number {

		const primaryDataset 	: Dataset
								= dataViewControl.datasets[0]

		if(!primaryDataset)
			return -1

		const primaryQuestion	: Question
								= primaryDataset.question

		if(primaryQuestion.type !== 'integer')
			return -1
		if(!hasOptions(primaryQuestion.config))
			return -1

		const min: number = primaryQuestion.min
		const max: number = primaryQuestion.max

		if(min && max) {
			if (min > max)
				return -1

			// create an array with all possible integer values
			const values: number[] = Array.from(Array(1+max-min).keys()).map((value: number) => value + min)

			values.forEach((value: number) => {
				if (primaryQuestion.options.find(
					(questionOptionConfig: QuestionOptionConfig) => questionOptionConfig.value === value
				) === undefined)
					return -1
			})
		}

		const secondaryDataset 	: Dataset
								= dataViewControl.datasets[1]

		const secondaryQuestion	: Question
								= secondaryDataset?.question

		if(secondaryDataset)
			if(secondaryQuestion?.type === 'string')
				if(!hasOptions(secondaryQuestion.config))
					return 2
				else
					return 1
			else
				return -1

		return 1
	}




	// INSTANCE:

	private chartSvg			: SVGElement
								= undefined

	private destroy$			: Subject<void>
								= new Subject<void>()

	public constructor(
		public 	dataViewControl			: DataViewControl,

		protected elementRef			: ElementRef<HTMLElement>,
		protected rccTranslationService	: RccTranslationService
	){
		super(dataViewControl)

		const startDate			: 	Date
								= 	CalendarDateString.toDate(dataViewControl.startDate)

		const endDate			: 	Date
								= 	CalendarDateString.toDate(dataViewControl.endDate)

		const options			:	BaseChartWithOptionsConfig<ConsumableDataForSingleBars, OptionBasics>['options']
								=	getChartOptions(dataViewControl) as BaseChartWithOptionsConfig<ConsumableDataForSingleBars, OptionBasics>['options']

		const data 				:	BaseChartConfig<ConsumableDataForSingleBars>['data']
								=	getConsumableChartData(dataViewControl, 'quantities', options as OptionInteger[]) as BaseChartConfig<ConsumableDataForSingleBars>['data']

		const dimensions		:	BaseChartConfig<ConsumableDataForSingleBars>['dimensions']
								=	getChartDimensions(dataViewControl)

		const chart 			:	QuantitiesChart
								= 	new QuantitiesChart({
										...this.commonChartOptions,
										data,
										options: options as OptionInteger[],
										startDate,
										endDate,
										timePeriod: dataViewControl.scope,
										language: rccTranslationService.activeLanguage,
										dimensions,
										locales,
										styles: {
											color: {
												primary:	'var(--main)',
												inactive:	COLORS_DEFAULT.greyBack,
											},
											note: {
												backgroundColor: 'var(--supp-highlight-secondary)'
											}
										},
									})

		this.rccTranslationService.activeLanguageChange$
		.pipe(
			skip(1),
			takeUntil(this.destroy$)
		)
		.subscribe( (lang : string) => chart.updateLanguage(lang) )


		this.chartSvg				=	chart.svg.node()

		this.chartSvg.style.width 	= 	'100%'

		this.elementRef.nativeElement.append(this.chartSvg)

		chart.render()
	}

	public ngOnDestroy(): void {
		this.chartSvg.remove()
		this.destroy$.next()
		this.destroy$.complete()
	}
}

