import { Component, ElementRef, OnDestroy } from '@angular/core'
import { DataViewControl, DataViewWidgetComponent, Dataset } from '../../data-visualization'
import { CalendarDateString, Question, hasOptions } from '@rcc/core'
import { SliderChart, ConsumableDataForSingleBars, COLORS_DEFAULT } from '@rcc/figures.cc'
import { getChartDimensions, getConsumableChartData, locales } from '../complex-data-view-widgets.commons'
import { RccTranslationService } from '@rcc/common'
import { Subject, takeUntil } from 'rxjs'

@Component({
	templateUrl	: '../complex-data-view-widgets.commons.html',
	styleUrls	: ['../complex-data-view-widgets.commons.css']
})
export class RawNumbersDataViewWidgetComponent extends DataViewWidgetComponent implements OnDestroy {
	public static widgetMatch(dataViewControl: DataViewControl): number {
		const primaryDataset 	: Dataset
								= dataViewControl.datasets[0]

		if(!primaryDataset) return -1

		const primaryQuestion	: Question
								= primaryDataset.question

		const correctQuestionType: boolean =
			primaryQuestion.type === 'integer' || primaryQuestion.type === 'decimal'
		if (!correctQuestionType) return -1

		if (hasOptions(primaryQuestion.config)) return -1

		const hasSecondaryDataset: boolean = dataViewControl.datasets[1] != null
		const secondaryDatasetType: string = dataViewControl.datasets[1]?.question.type

		if (!hasSecondaryDataset) return 1
		if (secondaryDatasetType === 'string') return 2

		return -1
	}

	private destroy$			: Subject<void>
								= new Subject<void>()

	public constructor(
		public dataViewControl			: DataViewControl,
		protected elementRef			: ElementRef<HTMLElement>,
		protected rccTranslationService	: RccTranslationService,
	) {
		super(dataViewControl)

		const { min, max } = this.determineMinMaxValues(dataViewControl)

		const chart: SliderChart = new SliderChart({
			...this.commonChartOptions,
			data: getConsumableChartData(dataViewControl, 'quantities') as ConsumableDataForSingleBars[],
			startDate: CalendarDateString.toDate(dataViewControl.startDate),
			endDate: CalendarDateString.toDate(dataViewControl.endDate),
			timePeriod: dataViewControl.scope,
			min,
			max,
			language: rccTranslationService.activeLanguage,
			dimensions: getChartDimensions(dataViewControl),
			locales,
			styles: {
				color: {
					primary: 'var(--main)',
					inactive:	COLORS_DEFAULT.greyBack,
				},
				note: {
					backgroundColor: 'var(--supp-highlight-secondary)'
				}
			}
		})

		const chartSvg: SVGSVGElement = chart.svg.node()
		chartSvg.style.width = '100%'

		this.elementRef.nativeElement.append(chartSvg)
		this.rccTranslationService.activeLanguageChange$
			.pipe(
				takeUntil(this.destroy$)
			)
			.subscribe( (lang : string) => chart.updateLanguage(lang))

		chart.render()
	}

	private determineMinMaxValues(dataViewControl: DataViewControl): { min: number, max: number } {
		const primaryDataSet: Dataset = dataViewControl.datasets[0]
		const minFromQuestion: number | undefined = primaryDataSet.question.min
		const maxFromQuestion: number | undefined = primaryDataSet.question.max

		if (minFromQuestion != null && maxFromQuestion != null)
			return { min: minFromQuestion, max: maxFromQuestion }

		const values: number[] = primaryDataSet.datapoints.map(datapoint => datapoint.value as number)

		const min: number = minFromQuestion ?? Math.min(...values)
		const max: number = maxFromQuestion ?? Math.max(...values)

		return { min, max }
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
