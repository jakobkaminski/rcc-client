import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			provideMainMenuEntry,
			Factory,
			HandlerAction,
			RccStorage,
			getEffectivePosition,
			WithPosition,
			provideTranslationMap,
			RccToastController
		}											from '@rcc/common'


import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({
	providers: [
		provideTranslationMap('DELETE_DATA', { en, de }),
		{ provide: Window, useValue: window }
	]
})

export class DeleteStorageMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `DeleteStorageMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<DeleteStorageMainMenuEntryModule> {

		const mainMenuEntry	: Factory<HandlerAction> = {
			deps: [RccStorage, Window, RccToastController],
			factory: (
				storage: RccStorage,
				window: Window,
				rccToastController: RccToastController
				) => ({
					label:				'DELETE_DATA.LABEL',
					icon: 				'trash',
					confirmMessage: 	'DELETE_DATA.CONFIRMATION',
					successMessage: 	'DELETE_DATA.SUCCESS',
					position: 			getEffectivePosition(config, -1),
					handler: 			async () => {
										const keys: string[] = await storage.getStorageNames()
										await Promise.all(keys.map(key => storage.clearItemStorage(key))).then( async () => await rccToastController.success('DELETE_DATA.SUCCESS'))
										window.location.reload()
					},
				})
			}
		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	DeleteStorageMainMenuEntryModule,
			providers
		}
	}

}
