import	{ 	Component,
			Inject,
			Optional
		} 										from '@angular/core'

import	{
			ComponentFixture,
			TestBed
		} 										from '@angular/core/testing'

import	{
			Action,
			isHandlerAction,
			MainMenuEntry,
			MAIN_MENU_ENTRIES,
			RccStorage,
			RccToastController
		}										from '@rcc/common'

import	{
			DeleteStorageMainMenuEntryModule
		}										from './delete-storage-main-menu-entry.module'



describe('DeleteStorageMainMenuEntryModule', () => {

	let windowSpy: jasmine.Spy<() => void> | undefined = undefined


	const mockWindow: Window = {
		location: {
			reload: () => undefined
		} as Location
	} as Window

	beforeEach(() => {
		windowSpy = spyOn(mockWindow.location, 'reload')
	})

	let clearCalls: number = 0
	const clearItemStorage: () => void = () => clearCalls++

	@Component({
		template: `
			<button (click)='onClick()'>Delete</button>
		`,
	})
	class WrapperComponent {
		public constructor(
			@Optional() @Inject(MAIN_MENU_ENTRIES)
			private readonly entries		: MainMenuEntry[],
		) {}

		protected onClick(): void {
			const deleteMenuEntry: Action = this.entries.find((entry) => 'label' in entry && entry.label === 'DELETE_DATA.LABEL') as Action

			if (isHandlerAction(deleteMenuEntry))
				deleteMenuEntry.handler()
		}
	}

	beforeEach(() => {
		clearCalls = 0
		const mockRccToastController: RccToastController = jasmine.createSpyObj('RccToastController', ['present', 'success', 'failure', 'info']) as RccToastController
		void TestBed.configureTestingModule({
			imports: [
				DeleteStorageMainMenuEntryModule.addEntry(),
			],
			providers: [
				{ provide: RccStorage, useValue: { clearItemStorage, getStorageNames: () => ['one', 'two'] } },
				{ provide: Window, useValue: mockWindow },
				{ provide: RccToastController, useValue: mockRccToastController },
			],
			declarations: [
				WrapperComponent
			],
		}).compileComponents()
	})

	it('Should provide a main menu entry', () => {

		const mainMenuEntries 	: MainMenuEntry[]	= TestBed.inject(MAIN_MENU_ENTRIES)
		const deleteEntry 		: MainMenuEntry | undefined		= mainMenuEntries.find((entry) => 'label' in entry && entry.label === 'DELETE_DATA.LABEL')

		expect(deleteEntry).toBeDefined()

	})

	it('Should try to clear item storage when clicked', async () => {
		const fixture: ComponentFixture<WrapperComponent> = TestBed.createComponent(WrapperComponent)
		const compiled: HTMLElement = fixture.debugElement.nativeElement as HTMLElement

		const button: HTMLButtonElement | null = compiled.querySelector('button')

		button?.click()

		await new Promise((resolve) => setTimeout(resolve, 1000))

		expect(clearCalls).toBe(2)
		expect(windowSpy).toHaveBeenCalledWith()
	})

})
