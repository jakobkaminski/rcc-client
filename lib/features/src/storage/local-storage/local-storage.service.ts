import 	{ 	Injectable 			} 	from '@angular/core'

import	{
			Item,
			ItemStorage,
			ConfigOf
		}							from '@rcc/core'



@Injectable()
export class LocalStorageService {

	protected namePrefix = 'rcc-storage-'


	public createItemStorage<I extends Item<any>, C extends ConfigOf<I> = ConfigOf<I> >(id:string): ItemStorage<I> {

		async function store(items: (I|ConfigOf<I>)[]): Promise<void>{

			const configs	=	items.map( (i: I|C ) =>
									i instanceof Item
									?	i.config
									:	i
								)



			return localStorage.setItem(this.namePrefix+id, JSON.stringify(configs))
		}

		const getAll 	=	async () =>	{

									const result 		= JSON.parse(localStorage.getItem(this.namePrefix+id) || '[]')
									const legacy_result = JSON.parse(localStorage.getItem(id) || '[]')

									const combined = [...legacy_result, ...result]

									await store(combined)

									localStorage.removeItem(id)

									return combined

							}



		return	{ getAll, store }

	}

	public async clearItemStorage(id:string): Promise<void> {
		localStorage.removeItem(this.namePrefix+id)
		localStorage.removeItem(id)

		console.log('ls: removed:', id)
	}

	public async getStorageNames(): Promise<string[]>{
		const regex = new RegExp(`^${this.namePrefix}`)
		return Object.keys(localStorage).filter( name => regex.exec(name) )
	}
}
