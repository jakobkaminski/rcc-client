import 	{	NgModule 				} 	from '@angular/core'
import	{
			DevModule,
			StorageProviderModule
		}								from '@rcc/common'

import	{	LocalStorageService		}	from './local-storage.service'

@NgModule({
	providers:[
		LocalStorageService
	],
	imports: [
		StorageProviderModule.forRoot(LocalStorageService),
		DevModule.note('LocalStorageModule')
	],
})
export class LocalStorageModule{}
