import 	{ 	Injectable 			} 	from '@angular/core'

import	{	RccStorage			}	from '@rcc/common'

import	{
			Item,
			ConfigOf,
			ItemStorage,
			assert
		}							from '@rcc/core'


export class IndexedDbItemStorage<I extends Item, C extends ConfigOf<I> = ConfigOf<I> > implements ItemStorage<I> {

	public static dbName	: string		= 'rccDatabase'
	public static storeName	: string		= 'rccStorage'

	public static async connect(): Promise<IDBDatabase> {

		return 	new Promise<IDBDatabase>( (resolve, reject) => {

					const request: IDBOpenDBRequest = indexedDB.open(IndexedDbItemStorage.dbName, 1)

					request.onsuccess 		= () 	=> resolve( request.result )
					request.onerror			= event => reject( event )

					request.onupgradeneeded	= event => {
						const db: IDBDatabase = request.result

						if(event.newVersion !== 1) return

						db.createObjectStore(IndexedDbItemStorage.storeName)
					}

				})
	}

	public static async request<T>(callback: (store: IDBObjectStore) => IDBRequest<T>, mode?: IDBTransactionMode): Promise<T> {

		const db			: IDBDatabase 		= 	await this.connect()

		const transaction	: IDBTransaction 	= 	db.transaction(IndexedDbItemStorage.storeName, mode)
		const completion	: Promise<unknown>	=	new Promise( resolve => { transaction.oncomplete = resolve })
		const store			: IDBObjectStore	=	transaction.objectStore(IndexedDbItemStorage.storeName)

		const rPromise: Promise<T>		=	new Promise<T>((resolve, reject) => {

									transaction.onerror = () => reject(transaction.error)
									let request: IDBRequest<T> = undefined

									try {
										request = callback(store)
									} catch (e) {
										console.error(e)
										return reject(e)
									}

									request.onerror		= () => reject(request.error)
									request.onsuccess	= () =>	resolve(request.result)
								})

		const data	: T			= 	await rPromise

		await completion

		db.close()

		return data

	}


	public constructor(
		protected id: string
	){}


	public async getAll(): Promise<C[]> {

		const data: string = await IndexedDbItemStorage.request( (store:IDBObjectStore) =>  (store.get(this.id) as  IDBRequest<string>) )

		return (JSON.parse(data || '[]') as C[])
	}

	public async store(items: (C|I)[]): Promise<void> {

		const configs: unknown[] = items.map(
			(i: C|I) =>	(i instanceof Item)
				?	i.config
				:	i
		)

		await IndexedDbItemStorage.request( store =>  store.put(JSON.stringify(configs), this.id), 'readwrite')

	}

	public async clear(): Promise<void>{
		return await IndexedDbItemStorage.request( store => store.delete(this.id), 'readwrite')
	}
}


@Injectable()
export class IndexedDbService extends RccStorage{

	public createItemStorage<I extends Item>(name:string): ItemStorage<I> {
		return new IndexedDbItemStorage<I>(name)
	}

	public async clearItemStorage(name:string): Promise<void>{
		await new IndexedDbItemStorage(name).clear()
	}

	public async getStorageNames(): Promise<string[]> {
		const keys: IDBValidKey[] 	= await IndexedDbItemStorage.request(store => store.getAllKeys())

		assert( keys.every(key => typeof key === 'string') ,'IndexedDbService.getStorageNames() non-string keys detected.')

		return keys as string[]
	}
}
