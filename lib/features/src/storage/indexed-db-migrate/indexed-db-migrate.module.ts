import 	{	NgModule 				} 	from '@angular/core'
import	{
			DevModule,
			StorageProviderModule
		}								from '@rcc/common'

import	{	IndexedDbMigrateService	}	from './indexed-db-migrate.service'

@NgModule({
	providers:[
		IndexedDbMigrateService
	],
	imports: [
		StorageProviderModule.forRoot(IndexedDbMigrateService),
		DevModule.note('IndexedDbMigrateService')
	],
})
export class IndexedDbMigrateModule{}
