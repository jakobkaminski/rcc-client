import 	{	NgModule						}	from '@angular/core'

import	{
			provideTranslationMap
		}										from '@rcc/common'

import	{	RccTransmissionRequestService	}	from './transmission-request.service'


import en from './i18n/en.json'
import de from './i18n/de.json'


/**
 * This module enables basic handling of transmission requests. Importing it on
 * its own will do nothing, but it serves as dependency for other
 * modules that will each actually manage transmission requests for a specific
 * type of data.
 */
@NgModule({
	providers:[
		RccTransmissionRequestService,
		provideTranslationMap('TRANSMISSION_REQUESTS', { en,de })
	]
})
export class RccTransmissionRequestModule {}
