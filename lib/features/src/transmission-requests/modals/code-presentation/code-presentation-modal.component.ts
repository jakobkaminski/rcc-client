import	{
			Component,
			Inject,
			OnDestroy
		}								from '@angular/core'

import	{
			FormControl
		}								from '@angular/forms'

import	{
			SharedModule,
			RccModalController,
			RELAY_PROPERTIES
		}								from '@rcc/common'

import	{
			UserCanceledError,
			assertProperty,
			assert
		}								from '@rcc/core'


/**
 * Modal shown showing the transmission code.
 */
@Component({
	selector: 		'rcc-code-presentation-modal',
	templateUrl: 	'./code-presentation-modal.component.html',
	styleUrls:		['./code-presentation-modal.component.scss'],
	standalone: 	true,

	imports:[
		SharedModule
	]
})
export class CodePresentationModalComponent implements OnDestroy {

	public codeControl 	: FormControl<string>
						= new FormControl<string>(null)

	public message		: string
						= undefined

	public code			: string
						= undefined

	public destroyed	: boolean
						= false

	public constructor(
		@Inject(RELAY_PROPERTIES)
		private relayProperties		: unknown,
		private rccModalController	: RccModalController
	){
		assertProperty(relayProperties, ['message', 'code'])
		assert(typeof relayProperties.message === 'string',	'message must be a string')
		assert(typeof relayProperties.code === 'string', 	'code must be a string')
		assert(relayProperties.done instanceof Promise, 	'code must be a instance of Promise')

		this.code 		= relayProperties.code
		this.message 	= relayProperties.message

		relayProperties.done
		.finally( () => this.destroyed || this.rccModalController.dismiss() )

	}

	public cancel() : void {
		this.rccModalController.dismiss(new UserCanceledError())
	}

	public submit(event: Event): void {
		event.preventDefault()
		this.rccModalController.dismiss(this.codeControl.value)
	}

	public ngOnDestroy() : void {
		this.destroyed = true
	}
}
