import	{	Component				}	from '@angular/core'

import	{
			FormControl
		}								from '@angular/forms'

import	{
			SharedModule,
			RccModalController,
		}								from '@rcc/common'

import	{
			UserCanceledError
		}								from '@rcc/core'


/**
 * Modal shown showing the transmission code.
 */
@Component({
	selector: 		'rcc-busy-modal',
	templateUrl: 	'./busy-modal.component.html',
	standalone: 	true,

	imports:[
		SharedModule
	]
})
export class BusyModalComponent{

	public codeControl 	: FormControl<string>
						= new FormControl<string>(null)

	public message		: string
						= undefined


	public constructor(
		private rccModalController: RccModalController
	){}

	public cancel() : void {
		this.rccModalController.dismiss(new UserCanceledError())
	}

}
