import	{	Injectable						}	from '@angular/core'

import	{
			filter
		}										from 'rxjs'

import	{
			RccTransmissionService,
			RccTransmission,
			RccOverlayService,
			RccAlertController
		}										from '@rcc/common'

import	{
			SessionConfig
		}										from '@rcc/core'

import	{
			EntryShareService
		}										from '../../../entries'

import	{
			RccTransmissionRequestService
		}										from '../../transmission-request.service'

import	{
			RequestData,
			assertRequestData
		}										from '../../transmission-request.commons'

/**
 * This service manages transmission requests for session data.
 */
@Injectable()
export class FulFillSessionRequestService {


	public constructor(
		private rccTransmissionRequestService	: RccTransmissionRequestService,
		private rccTransmissionService			: RccTransmissionService,
		private rccOverlayService				: RccOverlayService,
		private rccAlertController				: RccAlertController,
		private entryShareService				: EntryShareService
	){

		this.listenToSessionRequests()
	}


	/**
	 * Listen for transmission requests for session data.
	 */
	private listenToSessionRequests(): void {

		this.rccTransmissionRequestService.request$
		.pipe(
			filter( requestData => requestData[1] === 'session')
		)
		.subscribe( requestData => void this.handleSessionRequest( requestData ) )
	}


	/**
	 * Actually handle a specific transmission request for session data.
	 */
	private async handleSessionRequest(requestData: RequestData) : Promise<void> {

		assertRequestData(requestData)

		const message				: string
									= 'SESSION_REQUESTS.FULFILL.INFO'

		// Negotiate payload meta:
		const sessionMeta 			: unknown
									= await	this.rccTransmissionRequestService
											.negotiateMeta({
												role: 'fulfill',
												requestData,
												message
											})


		const sessionConfig			: SessionConfig
									= await this.entryShareService.getDataToShareAsSession()


		// Create new transmission for the requested session data.
		// Why not send the session using the original meta?
		// Because for the original meta, the requesting side chose the means of
		// transmission. For actual health data, the sending side should pick
		// the means, and that is what is happening here:

		const sessionTransmission	: RccTransmission
									= await	this.rccTransmissionService
											.setupWithExistingMeta(sessionConfig, sessionMeta) // payload, transmission meta

		await this.rccOverlayService.waitOrCancel(
			sessionTransmission.start(),
			{
				message: 'DATA_TRANSFER_SHARING_TEXT',
				contextText: 'DATA_TRANSFER_SHARING_CONTEXT_TEXT',
				icon: 'transmission_loading',
				direction: 'share'
			})

		void this.rccAlertController.present({ message: 'SESSION_REQUESTS.FULFILL.SUCCESS' })

	}



}
