import { Injectable } from '@angular/core'
import { FormControl } from '@angular/forms'
import { IncomingDataService, QrCodeService, RccModalController, RccSettingsService } from '@rcc/common'
import { RccTransmissionRequestService } from '@rcc/features'
import { FormData as SessionRequestOptionsFormData, RccSessionRequestPresentOptionsModalComponent, Choice } from './components/session-request-present-options-modal.component'
import { dataTransmissionPreferenceSettingsEntryId } from './combine-session-request.common'

@Injectable()
export class RccCombinedSessionRequestService {
	public constructor(
		private readonly rccModalController				: RccModalController,
		private readonly rccTransmissionRequestService	: RccTransmissionRequestService,
		private readonly qrCodeService					: QrCodeService,
		private readonly rccSettingsService				: RccSettingsService,
		private readonly incomingDataService			: IncomingDataService,
	) {}

	public async handleSessionRequestOptions(): Promise<void> {
		const setting: FormControl<Choice> = await this.rccSettingsService.get(dataTransmissionPreferenceSettingsEntryId)

		const preferenceSavedInUserSettings: boolean = setting.value != null

		const value: Choice = preferenceSavedInUserSettings ? setting.value : await this.promptForSessionPreference()

		if (value == null)
			return
		
		if (value === 'no-camera') {
			const transmissionMeta: unknown = await this.rccTransmissionRequestService.triggerRequest('session', 'SESSION_REQUESTS.TRIGGER.REQUEST_CONTENT')
			this.incomingDataService.next(transmissionMeta)
		}
				

		if (value === 'with-camera')
			await this.qrCodeService.scanAndAnnounce()
	}

	private async promptForSessionPreference(): Promise<Choice> {
		const result: SessionRequestOptionsFormData
			= await this.rccModalController.present(
				RccSessionRequestPresentOptionsModalComponent,
				{},
				{ fitContent: true }
			)

		if (result == null)
			return

		if (result.rememberChoice)
			await this.rccSettingsService.set(dataTransmissionPreferenceSettingsEntryId, result.choice)

		return result.choice
	}
}
