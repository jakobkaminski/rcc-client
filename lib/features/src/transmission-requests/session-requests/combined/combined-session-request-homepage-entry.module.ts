import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core'
import { Factory, HandlerAction, WithPosition, getEffectivePosition, provideHomePageEntry, provideSettingsEntry, provideTranslationMap } from '@rcc/common'
import { RccTransmissionRequestModule } from '../../transmission-request.module'
import { RccCombinedSessionRequestService } from './combined-session-request.service'
import { dataTransmissionPreferenceSettingsEntryId } from './combine-session-request.common'

import en from './i18n/en.json'
import de from './i18n/de.json'

const MODAL_PROMPT_SESSION_REQUEST_HOME_PAGE_ENTRY_CONFIG: InjectionToken<WithPosition>
	= new InjectionToken<WithPosition>('Configuration for home page entry')

const homePageEntry: Factory<HandlerAction> = {
	deps: [
		RccCombinedSessionRequestService,
		MODAL_PROMPT_SESSION_REQUEST_HOME_PAGE_ENTRY_CONFIG,
	],
	factory: (
		rccCombinedSessionRequestService: RccCombinedSessionRequestService,
		config?: WithPosition,
	) => ({
		position: getEffectivePosition(config, 2),
		label: 'COMBINED_SESSION_REQUEST.SESSION_SETTING',
		description: 'COMBINED_SESSION_REQUEST.SESSION_SETTING_DESCRIPTION',
		icon: 'receive_data',
		category: 'receive',
		handler: () => rccCombinedSessionRequestService.handleSessionRequestOptions()
	})
}

/**
 * This home page action will prompt the user to choose a session request
 * method, if they have not chosen one in the settings.
 *
 * Within the prompt, the user will have the option to save the option
 * they choose, which will have the same effect as if they had chosen
 * the preferred method in the settings.
 */
@NgModule({
	imports: [
		RccTransmissionRequestModule,
	],
	providers: [
		provideHomePageEntry(homePageEntry),
		provideSettingsEntry({
			id: dataTransmissionPreferenceSettingsEntryId,
			label: 'COMBINED_SESSION_REQUEST.SETTINGS_ENTRY.LABEL',
			description: 'COMBINED_SESSION_REQUEST.SETTINGS_ENTRY.DESCRIPTION',
			icon: '',
			defaultValue: undefined,
			type: 'select',
			options: [
				{
					value: 'with-camera',
					label: 'COMBINED_SESSION_REQUEST.SETTINGS_ENTRY.WITH_CAMERA',
				},
				{
					value: 'no-camera',
					label: 'COMBINED_SESSION_REQUEST.SETTINGS_ENTRY.NO_CAMERA',
				},
			],
			position: 0,
		}),
		provideTranslationMap('COMBINED_SESSION_REQUEST', { en, de }),
		RccCombinedSessionRequestService,
	]
})
export class RccCombinedSessionRequestHomepageEntryModule {
	public static addEntry(config?: WithPosition): ModuleWithProviders<RccCombinedSessionRequestHomepageEntryModule> {
		return {
			ngModule:	RccCombinedSessionRequestHomepageEntryModule,
			providers:	[{ provide: MODAL_PROMPT_SESSION_REQUEST_HOME_PAGE_ENTRY_CONFIG, useValue: config }]
		}
	}
}
