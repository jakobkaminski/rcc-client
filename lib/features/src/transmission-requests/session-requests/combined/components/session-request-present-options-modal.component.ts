import { Component } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { RccModalController, SharedModule } from '@rcc/common'
import { RccModalHeaderComponent } from '@rcc/common/modal-header/modal-header.component'
import { FormGroupOf } from '@rcc/core/types'
import { RccSelectOption } from '@rcc/themes/theming-mechanics'

export type Choice = 'with-camera' | 'no-camera'

export interface FormData {
	choice: Choice
	rememberChoice: boolean
}

type FormGroupType = FormGroupOf<FormData>

@Component({
	selector:		'rcc-session-request-present-options-modal',
	templateUrl:	'./session-request-present-options-modal.component.html',
	styleUrls:		['./session-request-present-options-modal.component.scss'],
	standalone:		true,
	imports:		[
		SharedModule,
		RccModalHeaderComponent,
	]
})
export class RccSessionRequestPresentOptionsModalComponent {
	public constructor(
		private readonly rccModalController: RccModalController
	) {}

	protected formGroup: FormGroup<FormGroupType> = new FormGroup<FormGroupType>({
		choice: new FormControl(undefined, Validators.required),
		rememberChoice: new FormControl(true),
	})

	protected preferenceOptions: RccSelectOption<Choice>[] = [
		{
			value: 'with-camera',
			label: 'COMBINED_SESSION_REQUEST.MODAL_OPTIONS.WITH_CAMERA',
		},
		{
			value: 'no-camera',
			label: 'COMBINED_SESSION_REQUEST.MODAL_OPTIONS.NO_CAMERA',
		},
	]

	protected onSubmit(): void {
		this.rccModalController.dismiss(this.formGroup.value)
	}

	protected closeModal(): void {
		this.rccModalController.dismiss()
	}

	protected get invalid(): boolean {
		return this.formGroup.invalid
	}
}
