import	{	Component 				}	from '@angular/core'
import	{
			Report,
		}								from '@rcc/core'

import	{	ExposeTemplateComponent	}	from '@rcc/common'


@Component({
	selector:		'rcc-basic-report-label',
	templateUrl:	'./basic-report-label.component.html',
	styleUrls:		['./basic-report-label.component.scss']
})
export class BasicReportLabelComponent extends ExposeTemplateComponent{

	public numberOfQuestions 	: number

	public constructor (public report: Report) {
		super()

		const questionIds: string[]	= 	report.entries.map( entry => entry.questionId )

		this.numberOfQuestions 		=	new Set(questionIds).size

	}

	private _dateFormat: Intl.DateTimeFormatOptions = {
		weekday: 'long',
		day: '2-digit',
		month: '2-digit',
		year: 'numeric',
		hour: '2-digit',
		minute: '2-digit',
	}
	protected dateFormat: Record<string, string> = this._dateFormat as Record<string, string>
}
