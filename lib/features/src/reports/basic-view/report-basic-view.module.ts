import 	{ 	NgModule 						}	from '@angular/core'
import 	{ 	RouterModule, Routes 			}	from '@angular/router'

import	{
			SharedModule,
			WidgetsModule,
			provideTranslationMap,
		}										from '@rcc/common'


import	{	DataVisualizationModule			}	from '../../data-visualization'
import	{	ReportMetaStoreServiceModule	}	from '../meta-store'
import	{	ReportViewComponent				}	from './view'
import	{	ReportBasicViewPageComponent	}	from './view-page'


import en from './i18n/en.json'
import de from './i18n/de.json'


const routes: Routes = [
	{ path: 'reports/:id',	component: ReportBasicViewPageComponent	},
]


@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		ReportMetaStoreServiceModule.forChild(null),
		DataVisualizationModule,
		WidgetsModule
	],
	providers:[
		provideTranslationMap('REPORT_BASIC_VIEW', { en, de }),
	],
	declarations: [
		ReportBasicViewPageComponent,
		ReportViewComponent
	],
})
export class ReportBasicViewModule {}
