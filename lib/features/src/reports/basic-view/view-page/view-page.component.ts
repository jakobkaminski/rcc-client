import	{
			Component,
			OnInit,
		}								from '@angular/core'
import	{
			Location
		}								from '@angular/common'
import	{
			ActivatedRoute,
			ParamMap
		}								from '@angular/router'
import	{	mergeMap				}	from 'rxjs/operators'

import	{	Report					}	from '@rcc/core'
import	{	ReportMetaStoreService	}	from '../../meta-store'


@Component({
	styleUrls:		['./view-page.component.css'],
	templateUrl:	'./view-page.component.html',
})
export class ReportBasicViewPageComponent  implements OnInit {

	public report			: Report

	public constructor(

		protected activatedRoute			: ActivatedRoute,
		protected reportMetaStoreService	: ReportMetaStoreService,
		protected location					: Location
	){}


	public ngOnInit() :void {

		this.activatedRoute.paramMap
		.pipe(
			mergeMap( 	(params	: ParamMap) 	=> this.reportMetaStoreService.get(params.get('id')) )
		)
		.subscribe(		(report	: Report)		=> this.report = report )

		// will unsubscribe automatically as part of route
	}

	public close(): void {
		this.location.back()
	}

}
