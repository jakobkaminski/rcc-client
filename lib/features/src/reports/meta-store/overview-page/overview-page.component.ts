import 	{
			Component,
		} 								from '@angular/core'

import	{	FormControl 			}	from '@angular/forms'

import	{
			ReportMetaStoreService
		}								from '../report-meta-store.service'

@Component({
	selector: 		'rcc-meta-store-page',
	templateUrl: 	'./overview-page.component.html',
	styleUrls: 		['./overview-page.component.scss'],
})
export class ReportMetaStorePageComponent {

	public filterControl 	: FormControl					= new FormControl()

	public constructor(
		public reportMetaStoreService: ReportMetaStoreService
	) { }

}
