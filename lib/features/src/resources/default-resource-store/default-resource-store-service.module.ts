import { NgModule } from '@angular/core'
import { RccDefaultResourceStoreService } from './default-resource-store.service'
import { provideTranslationMap } from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	providers: [
		RccDefaultResourceStoreService,
		provideTranslationMap('DEFAULT_RESOURCE_STORE', { en, de }),
	]
})
export class RccDefaultResourceStoreServiceModule {}
