import { Injectable } from '@angular/core'
import { ItemStorage } from '@rcc/core'
import { ResourceStore } from '@rcc/core/src/items/resources/resource-store.class'
import { Resource } from '@rcc/core/src/items/resources/resource.class'
import { ResourceConfig } from '@rcc/core/src/items/resources/resource.commons'

const TRANSLATION_KEY: string = 'DEFAULT_RESOURCE_STORE'

const defaultResources: ResourceConfig[] = [
	{
		id: 'rcc-default-resource-break',
		text: `${TRANSLATION_KEY}.TAKE_A_BREAK`,
	},
	{
		id: 'rcc-default-resource-music',
		text: `${TRANSLATION_KEY}.LISTEN_TO_MUSIC`,
	},
	{
		id: 'rcc-default-resource-walk',
		text: `${TRANSLATION_KEY}.TAKE_A_WALK`,
	},
	{
		id: 'rcc-default-resource-friend',
		text: `${TRANSLATION_KEY}.CONTACT_FRIEND_OR_RELATIVE`,
	},
	{
		id: 'rcc-default-resource-nature',
		text: `${TRANSLATION_KEY}.TIME_IN_NATURE`,
	},
	{
		id: 'rcc-default-resource-exercise',
		text: `${TRANSLATION_KEY}.EXERCISE`,
	},
	{
		id: 'rcc-default-resource-meal',
		text: `${TRANSLATION_KEY}.PREPARE_MEAL`,
	},
	{
		id: 'rcc-default-resource-meditate',
		text: `${TRANSLATION_KEY}.MEDITATE`,
	},
]

const staticStorage: ItemStorage<Resource> = {
	getAll: () => Promise.resolve(defaultResources)
}

@Injectable()
export class RccDefaultResourceStoreService extends ResourceStore {
	public constructor() {
		super(staticStorage)
	}
}
