import { Injectable } from '@angular/core'
import { RccStorage } from '@rcc/common'
import { ResourceStore } from '@rcc/core/src/items/resources/resource-store.class'
import { RccDefaultResourceStoreService } from '../default-resource-store/default-resource-store.service'
import { Resource } from '@rcc/core/src/items/resources/resource.class'
import { uuidv4 } from '@rcc/core'

@Injectable()
export class RccCustomResourceStoreService extends ResourceStore {
	public constructor(
		private readonly rccDefaultResourceStoreService: RccDefaultResourceStoreService,
		rccStorage: RccStorage,
	) {
		super(rccStorage.createItemStorage('rcc-custom-resources'))
		void this.setup()
	}

	private async setup(): Promise<void> {
		await this.ready
		// if there are no resources present add the default resources
		if(!this.items.length)
			await this.addDefaultResources()
	}

	private async addDefaultResources(): Promise<void> {
		await this.rccDefaultResourceStoreService.ready
		this.rccDefaultResourceStoreService.items.forEach((item: Resource) => {
			this.addItem(item)
		})
		await this.storeAll()
	}

	public createNewResource(): Resource {
		const resource: Resource = this.addConfig({
			id: uuidv4(),
			text: ''
		})
		void this.storeAll()
		return resource
	}

	public delete(resource: Resource): void {
		this.removeItem(resource)
		void this.storeAll()
	}

	public update(resource: Resource): void {
		this.addItem(resource)
		void this.storeAll()
	}

}
