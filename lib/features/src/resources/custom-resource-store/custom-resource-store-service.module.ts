import { NgModule } from '@angular/core'
import { RccCustomResourceStoreService } from './custom-resource-store.services'
import { provideTranslationMap } from '@rcc/common'

import	de from './i18n/de.json'
import	en from './i18n/en.json'

@NgModule({
	providers: [
		RccCustomResourceStoreService,
		provideTranslationMap('CUSTOM_RESOURCE_STORE', { de, en }),
	]
})
export class RccCustomResourceStoreServiceModule {
}
