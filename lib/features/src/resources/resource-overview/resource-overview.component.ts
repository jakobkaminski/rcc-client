import { Component, Input, OnDestroy } from '@angular/core'
import { RccCustomResourceStoreService } from '../custom-resource-store/custom-resource-store.services'
import { Resource } from '@rcc/core/src/items/resources/resource.class'
import { Action, RccTranslationService } from '@rcc/common'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { FormGroupOf } from '@rcc/core/types'
import { Subject, takeUntil } from 'rxjs'


export interface FormData {
	text: string,
}

type FormGroupType = FormGroupOf<FormData>



@Component({
	selector:		'rcc-resource-overview',
	templateUrl:	'./resource-overview.component.html',
	styleUrls:		['./resource-overview.component.scss'],
})
export class RccResourceOverviewComponent implements OnDestroy {
	public constructor(
		private readonly rccCustomResourceStoreService : RccCustomResourceStoreService,
		private readonly rccTranslationService: RccTranslationService,
	) {	}

	@Input()
	protected editMode: boolean = false

	private destroy$: Subject<void> = new Subject()

	protected get resources(): Resource[] {
		return this.rccCustomResourceStoreService.items.reverse()
	}

	private _formGroups: Map<string, FormGroup<FormGroupType>>
	protected get formGroups(): Map<string, FormGroup<FormGroupType>> {
		// initialize _formGroups if it does not exist
		this._formGroups = this._formGroups || new Map(this.resources.map((resource: Resource) => [resource.id, this.getFormGroup(resource)]))

		const mapKeys: string[] = Array.from(this._formGroups.keys())
		const resourceIds: string[] = this.resources.map((resource: Resource) => (resource.id))

		// add new resources and create formGroup
		const newResourceIds: string[] = resourceIds.filter((resourceId: string) => !(mapKeys.includes(resourceId)))
		for(const newResourceId of newResourceIds) {
			const newResource: Resource = this.resources.find((resource: Resource) => resource.id === newResourceId)
			this._formGroups.set(newResourceId, this.getFormGroup(newResource))
		}

		// remove deleted resources from map
		const removedResourceIds: string[] = mapKeys.filter((mapKey: string) => !resourceIds.includes(mapKey))
		for(const removedResourceId of removedResourceIds)
			this._formGroups.delete(removedResourceId)

		return this._formGroups
	}

	protected addResourceAction: Action = {
		icon: undefined,
		label: 'RESOURCES.ADD_RESOURCE',
		handler: () => {
			this.addResource()
		}
	}

	private addResource(): Resource {
		return this.rccCustomResourceStoreService.createNewResource()
	}

	protected getDeleteAction(resource: Resource): Action {
		return {
			label: 			'CUSTOM_RESOURCE_STORE.ACTIONS.DELETE.LABEL',
			successMessage:	'CUSTOM_RESOURCE_STORE.ACTIONS.DELETE.SUCCESS',
			failureMessage:	'CUSTOM_RESOURCE_STORE.ACTIONS.DELETE.FAILURE',
			confirmMessage:	'CUSTOM_RESOURCE_STORE.ACTIONS.DELETE.CONFIRM',
			handler: 	 	() => this.rccCustomResourceStoreService.delete(resource),
			icon:			'delete_circle',
		}
	}

	private getFormGroup(resource: Resource): FormGroup<FormGroupType> {
		const formGroup: FormGroup<FormGroupType> = new FormGroup<FormGroupType>({
			text: new FormControl(this.rccTranslationService.translate(resource.text), [Validators.required]),
		})
		formGroup.valueChanges
			.pipe(
				takeUntil(this.destroy$),
			)
			.subscribe((change: { text: string }) => {
				resource.text = change.text
				this.rccCustomResourceStoreService.update(resource)
			})
		return formGroup
	}

	public ngOnDestroy(): void {
		this.destroy$.next()
		this.destroy$.complete()
	}
}
