import 	{
			Component,
			ViewChild,
			OnDestroy,
		}     								from '@angular/core'

import	{
			FormControl
		}									from '@angular/forms'

import	{
			Router
		}									from '@angular/router'

import	{
			merge,
			Subscription,
			Observable,
			share,
			Subject,
			takeUntil
		}									from 'rxjs'

import	{
			CalendarDateString,
			SymptomCheck,
			format,
			isDayBefore,
		}									from '@rcc/core'
import	{
			QueryControl,
			QueryRunComponent
		}									from '../../queries'


import	{	DayQueryService					}	from '../day-query.service'
import	{	SymptomCheckMetaStoreService	}	from '../../symptom-checks/meta-store'
import	{	DayLabel						}	from '@rcc/themes/theming-mechanics/components/date-picker/date-picker.commons'

interface QuestionStats {
	answered: number
	total: number
}

@Component({
	templateUrl:   './day-query-run-page.component.html',
	styleUrls:     ['./day-query-run-page.component.scss']
})
export class DayQueryRunPageComponent implements OnDestroy{

	@ViewChild(QueryRunComponent)
	public 	queryRunComponent : QueryRunComponent

	public 	showCalendar			: boolean 							= false

	private	queryControlRecords		: Record<string, QueryControl[]>	= {}
	private statsRecords			: Record<string, QuestionStats>		= {}
	public	currentQueryControls	: QueryControl[]					= []

	private today					: Date								= new Date()
	protected startDate				: Date								= this.today
	protected endDate				: Date								= this.today
	public	dateControl				: FormControl<Date>					= new FormControl<Date>(this.today)

	private	answer$					: Observable<unknown>
	private	answerSubscription		: Subscription

	private destroy$				: Subject<void> = new Subject<void>()

	public allDoneSelectedDate		: boolean = false
	public doneSelectedDate			: number
	public totalSelectedDate		: number

	public constructor(
		public		dayQueryService					: DayQueryService,
		public		symptomCheckMetaStoreService	: SymptomCheckMetaStoreService,
		private		router							: Router
	){
		void this.setup()
	}

	private setup() : void {

		this.dateControl.valueChanges
		.pipe(takeUntil(this.destroy$))
		.subscribe( () => this.onDateChanges() )

		void this.updateStartDate().then(() => this.update())
	}


	private async update(): Promise<void> {
		await this.updateQueryControlRecord()
		this.updateCurrentQueryControls()
		this.updateStats()
	}


	private onDateChanges() : void {
		this.updateCurrentQueryControls()
		this.updateStats()
	}

	private async updateStartDate(): Promise<void> {
		const earliestDate: Date = await this.earliestDate()
		this.startDate = earliestDate
	}

	/**
	 * Gets the currently selected date: e.g. '2022-01-10'
	 */
	private get selectedDate() : Date { return this.dateControl.value }

	private get selectedDateString(): string { return format(this.selectedDate, 'yyyy-MM-dd') }

	private _earliestDate: Date
	private async earliestDate(): Promise<Date> {
		if (this._earliestDate) return this._earliestDate

		await this.symptomCheckMetaStoreService.ready


		this._earliestDate = this.symptomCheckMetaStoreService.items
			.map(symptomCheck => symptomCheck.config.meta.creationDate)
			.filter(creationDate => !!creationDate)
			.map(creationDateString => CalendarDateString.toDate(creationDateString))
			.reduce((previous, next) => {
				if (isDayBefore(next, previous))
					return next
				return previous
			}, this.startDate)
		return this._earliestDate
	}

	/**
	 * Updates queryControls for all dates
	 */
	private async updateQueryControlRecord() : Promise<void>  {
		await this.symptomCheckMetaStoreService.ready

		const symptomChecks 		:	SymptomCheck[]
									= 	this.symptomCheckMetaStoreService.items

		this.queryControlRecords 	= 	await this.dayQueryService.getQueryControls(symptomChecks, this.startDate, this.endDate)

		if(this.answerSubscription) this.answerSubscription.unsubscribe()

		const allQueryControls		:	QueryControl[]
									=	Object.values(this.queryControlRecords).flat()

		const allChanges			:	Observable<unknown>[]
									=	allQueryControls.map( queryControl => queryControl.change$)

		this.answer$				=	merge(...allChanges).pipe(share())

		this.answerSubscription		=	this.answer$
			.pipe(takeUntil(this.destroy$))
			.subscribe( () => this.updateStats() )
	}

	/**
	 * Updates to set of {@link QueryControl | QueryControls} currently used in the slider.
	 */
	private updateCurrentQueryControls() : void {
		this.currentQueryControls = this.queryControlRecords[this.selectedDateString] || undefined
	}

	/**
	 * Counts answers for all relevant questions for a given date (e.g. '2022-02-23').
	 */
	private getQuestionStats(dateString : string): QuestionStats {

		const queryControls	: QueryControl[]
							= this.queryControlRecords[dateString]

		if(!queryControls) return null

		const total			: number
							= queryControls.length

		const answered 			: number
							= queryControls.filter( queryControl => !!queryControl.entry ).length

		return { answered, total }
	}

	/**
	 * Calculates various stats used in the template, so they don't have to be recalculated on every change detection.
	 */
	private updateStats() : void {

		const dateStrings			: string[]
									= Object.keys(this.queryControlRecords)

		const questionStatsEntries	: [string, QuestionStats][]
									= dateStrings.map( dateString => [dateString, this.getQuestionStats(dateString)])

		const questionStats			: Record<string, QuestionStats>
									= Object.fromEntries(questionStatsEntries)
		this.statsRecords = questionStats

		const statsSelectedDate		: QuestionStats
									= questionStats[this.selectedDateString]

		this.doneSelectedDate		= statsSelectedDate?.answered
		this.totalSelectedDate		= statsSelectedDate?.total

		this.allDoneSelectedDate	= statsSelectedDate && (this.doneSelectedDate === this.totalSelectedDate)

	}

	protected get dayLabels(): DayLabel[] {
		return Object.entries(this.statsRecords).map(([key, value]) => ({
			date: new Date(key),
			content: `${value.answered}/${value.total}`,
			highlight: value.answered > 0,
		}))
	}

	public ngOnDestroy() : void {
		this.destroy$.next()
		this.destroy$.complete()
	}

	/**
	 * Called when query run is cancelled.
	 */
	public onCancel(): void {
		void this.router.navigate(['/'])
	}

	/**
	 * Called when query run is done.
	 */
	public onDone(): void {
		void this.router.navigate(['/'])
	}

	protected answerRemainingQuestions(): void {
		this.queryRunComponent.gotoFirstUnansweredQuery()
	}

	protected changeAnswers(): void {
		this.queryRunComponent.gotoFirstQuery()
	}
}
