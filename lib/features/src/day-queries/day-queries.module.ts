import	{	NgModule					}	from '@angular/core'

import	{
			provideRoutes,
			Route
		}									from '@angular/router'

import	{
			SharedModule,
			TranslationsModule,
			provideTranslationMap,
			overrideRoute,
		}									from '@rcc/common'

import	{	QueriesModule				}	from '../queries'
import	{	QuestionnaireServiceModule	}	from '../questions'
import	{	JournalServiceModule		}	from '../entries'
import	{	DayQueryService				}	from './day-query.service'
import	{	TodaysOpenQuestion$			}	from './todays-open-questions.observable'
import	{	DayQueryRunPageComponent	}	from './day-query-run-page'
import	{	DayQueryPageComponent		}	from './day-query-page'
import	{	dayQueryRunPath				}	from './day-queries.commons'
import	{	HasAnAnswerForTodayPipe		}	from './day-queries.pipes'
import	{	AllDoneSlideComponent		}	from './all-done-slide'

import en from './i18n/en.json'
import de from './i18n/de.json'



const routes : Route = {

	path:		dayQueryRunPath,
	title:		'DAY_QUERIES.PAGE_TITLE',
	component:	DayQueryRunPageComponent

}

const queryPageRoute : Route = 	{

	path: 		'query/:id',
	component: 	DayQueryPageComponent

}

/**
 * This Module handles questions that are due on a target day. (No momentary assessment)
 */
@NgModule({
	imports:[
		QueriesModule,
		SharedModule,
		TranslationsModule,
		QuestionnaireServiceModule,
		JournalServiceModule,
	],
	declarations:[
		DayQueryRunPageComponent,
		DayQueryPageComponent,
		HasAnAnswerForTodayPipe,
		AllDoneSlideComponent
	],
	exports:[
		DayQueryRunPageComponent,
		DayQueryPageComponent,
		HasAnAnswerForTodayPipe,
		AllDoneSlideComponent
	],
	providers:[
		DayQueryService,
		TodaysOpenQuestion$,
		provideTranslationMap('DAY_QUERIES', { en, de }),
		provideRoutes([routes]),
		overrideRoute(queryPageRoute)
	]
})
export class DayQueryModule{}
