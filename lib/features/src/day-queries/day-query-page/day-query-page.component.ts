import 	{
			Component,
			OnInit,
			ChangeDetectorRef
		} 								from '@angular/core'

import	{
			ActivatedRoute
		}								from '@angular/router'

import	{
			mergeMap,
			mergeAll,
			take,
			map
		}								from 'rxjs/operators'

import	{	from					}	from 'rxjs'

import	{
			Question,
		}								from '@rcc/core'

import	{	QuestionnaireService	}	from '../../questions'

import	{	QueryControl			}	from '../../queries'
import	{	DayQueryService			}	from '../day-query.service'



/**
 * This component renders a whole page with the UI to answer a single question
 * for the current day.
 * The question corresponds to the id in the path. Given answers will overwrite
 * an earlier answer of the same day.
 */
@Component({
	selector: 	'rcc-day-query-page',
	templateUrl: 	'./day-query-page.component.html',
})
export class DayQueryPageComponent implements OnInit {

	public queryControl: QueryControl

	public constructor(
		private	activatedRoute			: ActivatedRoute,
		private questionnaireService	: QuestionnaireService,
		private dayQueryService			: DayQueryService,
		private cd						: ChangeDetectorRef
	){}

	public ngOnInit() : void {

		this.activatedRoute.paramMap
		.pipe(
			map( 		(params) 					=> 	params.get('id') ),
			mergeMap( 	(id			: string )		=> 	from(this.questionnaireService.get([id]) ) ), // TODO: multiple matches?
			mergeAll(),
			take(1),
			mergeMap(	(question	: Question)		=> 	this.dayQueryService.getQueryControl(question) )

		)
		.subscribe({
			next: queryControl => 	{
				console.log('QUERYCONTROL', queryControl)
				this.queryControl = queryControl
				this.cd.detectChanges()
			}
		})

	}

}
