import	{
			Injectable,
			Inject,
			ApplicationRef
		}										from '@angular/core'

import	{
			DOCUMENT
		}										from '@angular/common'

import	{
			BehaviorSubject,
			Observable,
			merge,
			mergeMap,
			auditTime,
			fromEvent
		}										from 'rxjs'

import	{
			CalendarDateString,
			assert,
			isApplicationStable
		}										from '@rcc/core'

import	{	EntryMetaStoreService			}	from '../entries'
import	{	SymptomCheckMetaStoreService	}	from '../symptom-checks/meta-store'
import	{	QueryControl					}	from '../queries'
import	{	DayQueryService					}	from './day-query.service'


@Injectable()
export class TodaysOpenQuestion$ extends Observable<number> {


	public currentDate$ 			: BehaviorSubject<string>
									= new BehaviorSubject<string>(null)

	public numberOfOpenQuestion$ 	: BehaviorSubject<number>
									= new BehaviorSubject<number>(0)

	public constructor(
		@Inject(DOCUMENT)
		protected document							: Document,
		protected symptomCheckMetaStoreService		: SymptomCheckMetaStoreService,
		protected entryMetaStoreService				: EntryMetaStoreService,
		protected dayQueryService					: DayQueryService,
		private applicationRef								: ApplicationRef,
	){


		super( subscriber => this.numberOfOpenQuestion$.subscribe(subscriber) )

		const visibilityChange$ : Observable<Event>
								= fromEvent<Event>(this.document, 'visibilitychange')

		merge(
			this.symptomCheckMetaStoreService.change$,
			this.entryMetaStoreService.change$,
			this.currentDate$,
			visibilityChange$
		)
		.pipe(
			auditTime(1000),
			mergeMap( () => this.dayQueryService.getQueryControls(this.symptomCheckMetaStoreService.items) ),
		)
		.subscribe( queryControlRecord => {

			const dateString 			: string
										= Object.keys(queryControlRecord)[0]

			assert(dateString, 'TodaysOpenQuestion$: unable to get date from QueryControlRecord.', queryControlRecord)

			this.setDate(dateString)

			const queryControls			: QueryControl[]
										= queryControlRecord[dateString]

			const numberOfOpenQuestions : number
										= queryControls.filter( ({ entry }) => !entry ).length

			this.setNumberOfOpenQuestions(numberOfOpenQuestions)
		})

		void this.trackDate()
	}

	protected setNumberOfOpenQuestions( n : number) : void {
		if(this.numberOfOpenQuestion$.value !== n) this.numberOfOpenQuestion$.next(n)
	}

	protected setDate(dateString : string) : void {
		if(this.currentDate$.value !== dateString) this.currentDate$.next(dateString)
	}

	protected async trackDate() : Promise<void> {
		await isApplicationStable(this.applicationRef)

		this.setDate(CalendarDateString.today())

		setTimeout(
			() => { void this.trackDate() },
			1000*60
		)

	}


}
