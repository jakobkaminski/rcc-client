import	{	NgModule						}	from '@angular/core'


import	{
			provideSettingsEntry,
			provideTranslationMap,
			ScheduledNotificationModule,
		}										from '@rcc/common'

import	{	DayQueryModule					}	from '../day-queries.module'

import	{
			DayQueryReminderService,
			settingsEntry
		}										from './day-query-reminder.service'

import en from './i18n/en.json'
import de from './i18n/de.json'


@NgModule({

	imports : [
		DayQueryModule,
		ScheduledNotificationModule
	],

	providers: [
		DayQueryReminderService,
		provideSettingsEntry<string>(settingsEntry),
		provideTranslationMap('DAY_QUERIES', { en,de })
	]

})
export class DayQueryRemindersModule {

	public constructor(
		dayQueryReminderService: DayQueryReminderService	// ensure the service is instantiated whenever the module is loaded
	){}

}
