import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			provideHomePageEntry,
			Factory,
			Action,
			WithPosition,
			getEffectivePosition
		}											from '@rcc/common'

import	{	TodaysOpenQuestion$					}	from './todays-open-questions.observable'

import	{
			DayQueryModule,
		}											from './day-queries.module'

@NgModule({
	imports: [
		DayQueryModule,
	],

})

export class DayQueryHomePageEntryModule {

	/**
	* This method can add an entry to the home page.
	*
	* Calling it without parameter, will add an entry to home
	* page automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                   |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | undefined, false, null | adds no home page entry                                   |
	* |            | true                   | adds home page entry at a reasonably adequate position    |
	* |            | positive number        | adds home page entry at position counting from the top    |
	* |            | negative number        | adds home page entry at position counting from the bottom |
	* Example: 	`SessionHomePageEntryModule.addEntry({ position: 1 })`,

	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<DayQueryHomePageEntryModule> {

		const homePageEntry	: Factory<Action> = {

			deps: [TodaysOpenQuestion$],
			factory: (todaysOpenQuestion$:TodaysOpenQuestion$) =>({
				position:		getEffectivePosition(config, 2),
				label:			'DAY_QUERIES.HOME.LABEL',
				description:	'DAY_QUERIES.HOME.DESCRIPTION',
				icon:			'answer_question',
				path:			'day-query-run',
				category:		'create',
				notification:	todaysOpenQuestion$
			})

		}

		const providers : Provider[] = [provideHomePageEntry(homePageEntry)]

		return {
			ngModule:	DayQueryHomePageEntryModule,
			providers
		}
	}
}
