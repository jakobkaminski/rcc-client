import 	{
			Component,
			EventEmitter,
			Input,
			Output
		}     								from '@angular/core'

import	{
			Router
		}									from '@angular/router'

import	{
			Action
		}									from '@rcc/common'

@Component({
	selector:   'all-done-slide',
	templateUrl:   './all-done-slide.component.html',
	styleUrls:     ['./all-done-slide.component.css'],
})
export class AllDoneSlideComponent{

	public constructor(
		private	router	: Router)
	{}

	@Input()
	public allDoneSelectedDate		: boolean
	@Input()
	public doneSelectedDate			: number
	@Input()
	public totalSelectedDate		: number

	@Output()
	public answerQuestions			: EventEmitter<void> = new EventEmitter<void>()
	@Output()
	public changeAnswers			: EventEmitter<void> = new EventEmitter<void>()

	public answerRemainingQuestionsAction: Action = {
		label: 			'DAY_QUERIES.QUERY_RUN.ANSWER_QUESTONS',
		description: 	'DAY_QUERIES.QUERY_RUN.ANSWER_QUESTONS',
		category:		'create',
		icon: 			'create',
		handler: 		() => this.answerQuestions.emit()
	}

	/**
	 * Called when query run is done.
	 */
	public doneAction: Action = {
		label: 			'DAY_QUERIES.QUERY_RUN.DONE',
		description: 	'DAY_QUERIES.QUERY_RUN.DONE',
		category:		'analyze',
		icon: 			'analyze',
		handler: 		() => this.onDone()
	}

	public onDone(): void {
		void this.router.navigate(['/'])
	}
}
