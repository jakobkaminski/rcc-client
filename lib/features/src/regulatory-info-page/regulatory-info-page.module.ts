import	{	NgModule						}	from '@angular/core'
import	{	RouterModule,
			Routes							}	from '@angular/router'
import	{	RegulatoryInfoPageComponent		}	from './regulatory-info-page.component'
import	{	MainMenuModule,
			SharedModule,
			provideTranslationMap,
			MedicalFeatureModule			}	from '@rcc/common'

import en from './i18n/en.json'
import de from './i18n/de.json'

const routes: Routes = [
	{
		path: 'about',
		title: 'ABOUT.TITLE',
		component: RegulatoryInfoPageComponent,
	},
]

export const aboutPagePath: string = '/about'

@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(routes),
		MainMenuModule,
		MedicalFeatureModule
	],
	providers: [
		provideTranslationMap('ABOUT', { en, de }),
	],
	declarations: [
		RegulatoryInfoPageComponent,
	]
})
export class RegulatoryInfoPageModule {}
