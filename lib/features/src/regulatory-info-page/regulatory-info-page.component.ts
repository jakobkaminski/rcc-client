import	{	Component			}	from '@angular/core'
import	{	RccBuildInfoService	}	from '@rcc/common'

@Component({
	selector		:	'rcc-about-page',
	templateUrl	:	'./regulatory-info-page.component.html',
	styleUrls		:	['./regulatory-info-page.component.scss']
})
export class RegulatoryInfoPageComponent {

	protected version	: string
	protected date		: Date

	private _dateFormatOptions: Intl.DateTimeFormatOptions = {
		month	: '2-digit',
		year	: 'numeric',
		day		: '2-digit',
		weekday	: 'long',
	}

	protected dateFormatOptions: Record<string, unknown> = this._dateFormatOptions as Record<string, unknown>

	public constructor(private readonly buildInfoService: RccBuildInfoService) {

		const regex : RegExp = /(\d+\.\d+\.\d+)/

		if (buildInfoService.buildInfos.tag.startsWith('v'))
			this.version = buildInfoService.buildInfos.tag.match(regex)[0]
		else
			this.version = buildInfoService.buildInfos.tag

		this.date = new Date(buildInfoService.buildInfos.tagDate)
	}

}
