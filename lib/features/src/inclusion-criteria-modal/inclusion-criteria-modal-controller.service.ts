import	{	Injectable						}	from '@angular/core'
import	{	RccModalController				}	from '@rcc/common/src/modals-provider'
import	{	NavigationEnd, Router			}	from '@angular/router'
import	{	RccKeyValueStorageService		}	from '@rcc/common/src/key-value-storage'
import	{	filter,	map						}	from 'rxjs'
import	{	InclusionCriteriaModalComponent		}	from './inclusion-criteria-modal.component'
import	{
			inclusionCriteriaConfirmedKey
		}										from './inclusion-criteria-modal.commons'

@Injectable({
	providedIn: 'root',
})
export class InclusionCriteriaModalControllerService {

	public constructor(
		private readonly router: Router,
		private rccKeyValueStorageService: RccKeyValueStorageService,
		private rccModalController: RccModalController,
	){
		this.router.events
			.pipe(
				filter((event: NavigationEnd) => event instanceof NavigationEnd),
				map((event: NavigationEnd) => event.urlAfterRedirects),
			)
			.subscribe(() => {
				void this.showConfirmationModalIfNeeded()
			})
	}

	private async showConfirmationModalIfNeeded(): Promise<void> {
		const confirmationNeeded: boolean = await this.confirmationNeeded()
		if (confirmationNeeded)
			await this.confirm()
		
	}

	private async confirmationNeeded(): Promise<boolean> {
		const confirmed 		: boolean
								= 	(await this.rccKeyValueStorageService
									.get<boolean>(inclusionCriteriaConfirmedKey))?.value ?? false
		return !confirmed
	}

	private async confirm(): Promise<void> {
		const result : unknown = await this.rccModalController.present(InclusionCriteriaModalComponent, undefined , { mini: true, canDismiss: false })
		if (result === true)
			await this.rccKeyValueStorageService.set(inclusionCriteriaConfirmedKey, true)
	}
}
