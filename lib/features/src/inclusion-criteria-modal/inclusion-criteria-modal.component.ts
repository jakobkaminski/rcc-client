import	{
			Component,
		}											from '@angular/core'
import	{
			RccModalController					}	from '@rcc/common'

@Component({
	selector	: 'rcc-inclusion-criteria-modal',
	templateUrl	: './inclusion-criteria-modal.component.html',
	styleUrls	: ['./inclusion-criteria-modal.component.scss'],
})
export class InclusionCriteriaModalComponent {

	public constructor(
		private readonly rccModalController: RccModalController,
	) {}

	public acceptInclusionCriteria(): void {
		this.rccModalController.dismiss(true)
	}

}
