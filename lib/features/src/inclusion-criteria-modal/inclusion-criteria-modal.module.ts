import	{
			NgModule,
		}										from '@angular/core'
import	{	MedicalFeatureModule			}	from '@rcc/common/medical/medical-feature/medical-feature.module'
import	{	provideTranslationMap			}	from '@rcc/common/src/translations'
import	{	SharedModule 					}	from '@rcc/common/src/shared-module'
import	{
			InclusionCriteriaModalComponent,
		}										from './inclusion-criteria-modal.component'
import	{
			InclusionCriteriaModalControllerService,
		}										from './inclusion-criteria-modal-controller.service'

import	de from './i18n/de.json'
import	en from './i18n/en.json'

@NgModule({
	imports: [
		SharedModule,
		MedicalFeatureModule
	],
	providers: [
		provideTranslationMap('INCLUSION_CRITERIA', { en, de }),
	],
	declarations: [
		InclusionCriteriaModalComponent,
	],

})
export class InclusionCriteriaModalModule {
	public constructor(private readonly inclusionCriteriaModalControllerService: InclusionCriteriaModalControllerService) {
	}
}
