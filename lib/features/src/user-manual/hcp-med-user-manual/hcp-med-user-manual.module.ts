import { NgModule } from '@angular/core'
import { MedicalFeatureModule } from '@rcc/common/src/medical/medical-feature/medical-feature.module'
import { RccUserManualSharedModule } from '../user-manual-shared/user-manual-shared.module'
import { USER_MANUAL_MAP } from '../user-manual-shared/user-manual-shared.common'

import deManual from './files/de-user-manual.pdf'
import enManual from './files/en-user-manual.pdf'

const languageManualMap: Record<string, string> = {
	de: deManual,
	en: enManual,
}

@NgModule({
	imports: [
		MedicalFeatureModule,
		RccUserManualSharedModule,
	],
	providers: [
		{ provide: USER_MANUAL_MAP, useValue: languageManualMap },
	],
})
export class RccHcpMedUserManualModule {}
