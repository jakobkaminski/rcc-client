import { InjectionToken } from '@angular/core'

/** Index of user manual file locations, indexed by language */
export type UserManualMap = Record<string, string>

export const USER_MANUAL_MAP: InjectionToken<UserManualMap> = new InjectionToken('User manual map')
