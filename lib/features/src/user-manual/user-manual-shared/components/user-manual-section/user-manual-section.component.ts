import { Component, Inject } from '@angular/core'
import { RccSettingsService } from '@rcc/common'
import { USER_MANUAL_MAP, UserManualMap } from '@rcc/features/user-manual/user-manual-shared/user-manual-shared.common'
import { Observable, from, map, merge, mergeMap } from 'rxjs'

@Component({
	selector	: 'rcc-user-manual-section',
	templateUrl	: './user-manual-section.component.html',
	styleUrls	: ['./user-manual-section.component.scss']
})
export class RccUserManualSectionComponent {
	public constructor(
		@Inject(USER_MANUAL_MAP)
		private readonly userManualMap		: UserManualMap,
		private readonly rccSettingsService	: RccSettingsService
	) {}

	protected hasUserManual: boolean = this.userManualMap != null

	protected userManualLink: Observable<string> = merge(
		from(this.rccSettingsService.get('activeLanguage')).pipe(map(formControl => formControl.value as string)),
		from(this.rccSettingsService.get('activeLanguage').then(formControl => formControl.valueChanges as Observable<string>)).pipe(mergeMap(val => val))
	).pipe(map(language => this.userManualMap[language]))
}
