import { Injectable } from '@angular/core'
import { Entry, Question, R4, Report, report2fhir } from '@rcc/core'
import { JournalService, QuestionnaireService } from '@rcc/features'

/**
 * This service exports questions + user entries from app storage to some other format
 */
@Injectable()
export class FhirService {
	public constructor(
		private journalService: JournalService,
		private questionnaireService: QuestionnaireService
	) {}

	// Returns all entries from the journal
	private async getEntries(): Promise<Entry[]> {
		await this.journalService.ready
		return this.journalService.items
	}

	// Returns all questions from the questionnaire
	private async getQuestions(): Promise<Question[]> {
		await this.questionnaireService.ready
		return this.questionnaireService.items
	}

	/**
	 * Converts a Report to FHIR format
	 * @param {string}	lang	Language code for export, defaults to 'en' (English)
	 * @param {string}	id		User pseudonym/patient ID, defaults to '0000'
	 * @param {Report}	report	Report to be converted to FHIR, defaults to undefined.
	 * In that case, a new Report of all answered items will be generated
	 * @returns The Report in FHIR format
	 */
	public async exportToFHIR(lang: string = 'en', id: string = '0000', report: Report = undefined): Promise<R4.IQuestionnaireResponse> {
		const questions : Question[] = await this.getQuestions()
		if (!report) {
			const entries : Entry[] = await this.getEntries()
			const fullReport : Report = Report.from(entries)
			return report2fhir(fullReport, questions, lang, id)
		}
		return report2fhir(report, questions, lang, id)
	}
}
