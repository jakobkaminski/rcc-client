import	{
			Injectable,
			NgModule
		}								from '@angular/core'
import	{
			ExportConfig,
			ExportMetadata,
			ExportResult,
			ExportService,
			provideExportService,
			provideTranslationMap,
			RccSettingsService,
		}								from '@rcc/common'
import	{
			assert,
			R4,
			Report
		}								from '@rcc/core'
import	{	FhirService				}	from './fhir.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

@Injectable()
export class ReportFhirExportService extends ExportService<Report,R4.IQuestionnaireResponse> {
	public label	 	: string	= 'FHIR_MODULE.EXPORT.LABEL'
	public description	: string	= 'FHIR_MODULE.EXPORT.DESCRIPTION'

	public constructor(
		public fhirService: FhirService,
		public rccSettingsService: RccSettingsService
	) {
		super()
	}

	public canExport(x: unknown): x is Report {
		return x instanceof Report
	}

	public async export(x: ExportConfig<Report>): Promise<ExportResult<R4.IQuestionnaireResponse>> {
		assert(this.canExport(x.data), 'ReportFhirExportService.export: x.data must be of type Report')

		const language	: string = (await this.rccSettingsService.get<string>('activeLanguage')).value
		const pseudonym : string = (await this.rccSettingsService.get<string>('repose-pseudonym'))?.value

		const fhirReport : R4.IQuestionnaireResponse = await this.fhirService.exportToFHIR(language, pseudonym, x.data)
		return {
			toString: () => JSON.stringify(fhirReport),
			raw: () => fhirReport
		}
	}

	public getMetadata(x: ExportConfig<Report>): ExportMetadata {
		assert(this.canExport(x.data), 'ReportFhirExportService.export: x.data must be of type Report')
		return {
			suggestedFilename	: `RCC-Report-${x.data.date.toISOString().split('T')[0]}.json`,
			mimeType			: 'application/json'
		}
	}
}

@NgModule({
	providers: [
		FhirService,
		provideTranslationMap('FHIR_MODULE', { en, de }),
		...provideExportService(ReportFhirExportService)
	]
})
export class FhirModule {}
