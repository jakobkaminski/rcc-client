import {
	Component,
	EventEmitter,
	Input,
	Output
}					from '@angular/core'
import {
	ControlContainer,
	FormGroupDirective
}					from '@angular/forms'

@Component({
	selector    : 'rcc-choice-option-item[optionValueType]',
	templateUrl : './choice-option-item.component.html',
	viewProviders: [
		{ provide: ControlContainer, useExisting: FormGroupDirective }
	]
})
export class ChoiceOptionItemComponent {
	@Input()
	public optionValueType: string

	@Input()
	public showRemoveButton = true

	@Input()
	public valueReadonly = false

	@Output()
	public removeItem = new EventEmitter<void>()
}
