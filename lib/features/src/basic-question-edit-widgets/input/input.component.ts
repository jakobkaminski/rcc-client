import 	{
			Component,
		}										from '@angular/core'

import	{
			RccTranslationService
		}	                            		from '@rcc/common'

import	{
			takeUntil,
			startWith
		}										from 'rxjs'

import	{
			QuestionEditControl,
			GenericQuestionEditWidgetComponent
		}										from '../../questions/question-edit-widgets'


@Component({
	templateUrl : './input.component.html'
})
export class InputQuestionEditWidgetComponent extends GenericQuestionEditWidgetComponent {


	// STATIC

	public static label 		= 'BASIC_QUESTION_EDIT_WIDGETS.TEXT_INPUT.LABEL'

	public static controlType 	= QuestionEditControl

	public placeholder  = 'BASIC_QUESTION_EDIT_WIDGET.TEXT_INPUT.PLACEHOLDER'

	public static widgetMatch(questionEditControl: QuestionEditControl): number {

		const config			= questionEditControl.questionConfig

		// New question, can still be handled:
		if(!config)				return 2


		const hasNoOptions 	= 	! (config.options?.length > 0)
		const typeMatch		=	['string', 'integer', 'decimal'].includes(config.type)

		// Made to handle questions like this:
		if(hasNoOptions && typeMatch) return 2

		// Can handle any other question if need be:
		return 	0
	}



	// INSTANCE

	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
	){
		super(
			questionEditControl,
			rccTranslationService
		)

		this.answerTypes = ['string', 'integer', 'decimal']
		this.setAnswerTypeAsSelectedOrDefault()

		this.answerTypeControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.answerTypeControl.value)
		)
		.subscribe(
			answerType => 	answerType === 'string'
							?	this.limitationControl.setValue('none')
							:	this.limitationControl.setValue('minmax')
		)

		this.tagControl.disable()
	}
}
