import 	{
			Component,
		}										from '@angular/core'

import	{
			FormControl
		}										from '@angular/forms'
import	{
			takeUntil,
			startWith
		}										from 'rxjs'
import	{
			RccTranslationService
		}	                            		from '@rcc/common'

import	{
			QuestionEditControl,
			GenericQuestionEditWidgetComponent
		}										from '../../questions/question-edit-widgets'

@Component({
	templateUrl : './yes-no.component.html'
})
export class YesNoQuestionEditWidgetComponent extends GenericQuestionEditWidgetComponent {


	// STATIC

	public static label 		= 'BASIC_QUESTION_EDIT_WIDGETS.YES_NO.LABEL'

	public static controlType 	= QuestionEditControl

	public static widgetMatch(questionEditControl: QuestionEditControl): number {

		const config			= questionEditControl.questionConfig

		// New question, can still be handled:
		if(!config)				return 0

		// Made to handle questions like this:
		if(config.type === 'boolean') return 2

		// Can handle any other question if need be:
		return 	0
	}



	// INSTANCE

	public useCustomLabelControl = new FormControl<boolean>(true)

	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
	){
		super(
			questionEditControl,
			rccTranslationService,
		)
		this.answerTypes = ['boolean']
		this.answerTypeControl.setValue('boolean')
		this.limitationControl.setValue('options')
		this.tagControl.disable()

		this.allForms.push(this.useCustomLabelControl)

		this.useCustomLabelControl.setValue(questionEditControl.questionConfig.options?.length > 0)

		this.useCustomLabelControl.valueChanges
		.pipe(
			takeUntil(this.destroy$),
			startWith(this.useCustomLabelControl.value)
		)
		.subscribe(
			useCustomLabel 	=> 	useCustomLabel
								?	this.limitationControl.setValue('options')
								:	this.limitationControl.setValue('none')
		)
	}

}
