import 	{
			Component,
		}										from '@angular/core'

import	{
			RccTranslationService
		}	                            		from '@rcc/common'

import	{
			QuestionEditControl,
			GenericQuestionEditWidgetComponent
		}										from '../../questions/question-edit-widgets'


@Component({
	templateUrl : './choice.component.html'
})
export class ChoiceQuestionEditWidgetComponent extends GenericQuestionEditWidgetComponent {

	public static label 		= 'BASIC_QUESTION_EDIT_WIDGETS.CHOICE.LABEL'

	public static controlType 	= QuestionEditControl

	public static widgetMatch(questionEditControl: QuestionEditControl): number {

		const config			= questionEditControl.questionConfig

		// New question, can still be handled:
		if(!config)				return 0

		const hasOptions	= config.options?.length > 0
		const typeMatch 	= ['string', 'integer', 'decimal'].includes(config.type)

		// Made to handle questions like this:
		if(typeMatch && hasOptions) return 2

		// Can handle any other question if need be:
		return 	0
	}


	// INSTANCE"


	public constructor(
		protected questionEditControl	: QuestionEditControl,
		protected rccTranslationService	: RccTranslationService,
	){
		super(
			questionEditControl,
			rccTranslationService,
		)

		this.answerTypes = ['string', 'integer', 'decimal']
		this.setAnswerTypeAsSelectedOrDefault()

		this.limitationControl.setValue('options')
		this.tagControl.disable()

	}

}
