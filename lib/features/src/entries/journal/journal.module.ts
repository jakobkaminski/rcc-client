import	{	NgModule				}		from '@angular/core'

import	{
			Entry,
		}									from '@rcc/core'

import	{
			StorageProviderModule,
			provideItemAction,
			provideTranslationMap,
			ItemAction,
		}								from '@rcc/common'

import	{
			ReportBasicViewModule,
		}									from '../../reports/basic-view'
import	{	EntryMetaStoreServiceModule	}	from '../meta-store'
import	{	JournalService				}	from './journal.service'

import en from './i18n/en.json'
import de from './i18n/de.json'


export const JournalServiceHomePath : string = '/journal'

const itemAction: ItemAction<Entry> = 	{
								role:		'destructive' as const,
								itemClass:	Entry,
								storeClass: JournalService,

								getAction:	(entry: Entry, journalService: JournalService) => ({
									label: 			'JOURNAL.ACTIONS.DELETE',
									icon:			'delete',
									successMessage:	'JOURNAL.ACTIONS.DELETE_SUCCESS',
									failureMessage:	'JOURNAL.ACTIONS.DELETE_FAILURE',
									confirmMessage:	'JOURNAL.ACTIONS.DELETE_CONFIRM',
									handler: 		() => journalService.removeEntry(entry)
								}),
							}


@NgModule({
	imports:[
		ReportBasicViewModule,
		EntryMetaStoreServiceModule.forChild([JournalService]),
		StorageProviderModule
	],
	providers: [
		JournalService,
		provideItemAction(itemAction),
		provideTranslationMap('JOURNAL', { en, de }),
		// provideHomePageEntry(homePageEntry)
	],
})
export class JournalServiceModule { }
