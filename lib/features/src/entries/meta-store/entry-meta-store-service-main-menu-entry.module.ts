
import	{
			NgModule,
			Provider,
			ModuleWithProviders,
		}											from '@angular/core'

import	{
			provideMainMenuEntry,
			Action,
			WithPosition,
			getEffectivePosition
		}											from '@rcc/common'
import	{
			EntryMetaStoreServiceModule,
		}											from './entry-meta-store.module'
import	{
			EntryMetaStoreServiceHomePath
		}											from './entry-meta-store.commons'


@NgModule({
	imports: [
		EntryMetaStoreServiceModule,
	],

})

export class EntryMetaStoreServiceMainMenuEntryModule {

	/**
	* This method can add entries to the main menu.
	*
	* Calling it without parameter, will add entries to the main menu
	* automatically at reasonably adequate positions.
	*
	* If you want to determine the positions yourself provide a `config` parameter :
	*
	* | .position  | value                  | effect                                                    |
	* |------------|------------------------|-----------------------------------------------------------|
	* | .position  | true                   | adds menu entry at a reasonably adequate position         |
	* |            | positive number        | adds menu entry at position counting from the top         |
	* |            | negative number        | adds menu entry at position counting from the bottom      |
	*
	* Example:     `EntryMetaStoreServiceMainMenuEntryModule.addEntry({ position: 1 })`,
	*/

	public static addEntry(config?: WithPosition): ModuleWithProviders<EntryMetaStoreServiceMainMenuEntryModule> {

		const mainMenuEntry	: Action =	{
				path:			EntryMetaStoreServiceHomePath,
				position:		getEffectivePosition(config, 7),
				label: 			'ENTRIES_META_STORE.MENU_ENTRY',
				icon: 			'entry',
		}

		const providers : Provider[] = [provideMainMenuEntry(mainMenuEntry)]

		return {
			ngModule:	EntryMetaStoreServiceMainMenuEntryModule,
			providers
		}
	}

}
