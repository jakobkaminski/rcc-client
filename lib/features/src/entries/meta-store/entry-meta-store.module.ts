import 	{
			NgModule,
			ModuleWithProviders,
			Type
		} 									from '@angular/core'

import 	{
			RouterModule,
			Routes
		}									from '@angular/router'

import	{
			BaseMetaStoreModule,
			ExportModule,
			RccExportService,
			ItemRepresentation,
			MetaAction,
			MetaStoreConfig,
			MetaStoreModule,
			provideItemRepresentation,
			provideTranslationMap,
			SharedModule,
		}									from '@rcc/common'

import	{
			Entry,
			EntryConfig,
			EntryStore,
			Question,
			Report,
		}									from '@rcc/core'

import	{
			QuestionnaireServiceModule,
			QuestionnaireService
		}									from '../../questions/questionnaire'

import	{	EntryMetaStoreService		}	from './entry-meta-store.service'

import	{
			ENTRY_STORES,
			ENTRY_META_ACTIONS
		}									from './entry-meta-store.commons'

import	{	EntryMetaStorePageComponent }	from './overview-page/overview-page.component'
import	{	EntryLabelComponent			}	from './item-label/item-label.component'


import en from './i18n/en.json'
import de from './i18n/de.json'

const routes: Routes = [
		{ path: 'entries',	component: EntryMetaStorePageComponent	},
	]

const metaStoreConfig: MetaStoreConfig<EntryConfig, Entry, EntryStore> = {
		itemClass:		Entry,
		serviceClass:	EntryMetaStoreService
	}

const itemRepresentation: ItemRepresentation = {
		itemClass:		Entry,
		icon:			'entry',
		labelComponent:	EntryLabelComponent,
	}

const metaAction: MetaAction<EntryConfig, Entry, EntryStore> =
	{
		label:			'ENTRIES_META_STORE.ACTION.EXPORT',
		role:			'share' as const,
		icon:			'export',
		position:		-1,
		deps:			[RccExportService, QuestionnaireService],
		handlerFactory:	(rccExportService: RccExportService, questionnaireService: QuestionnaireService) =>
							async (entryMetaStoreService: EntryMetaStoreService) => {
								const 	report		: Report		 	= Report.from(entryMetaStoreService.items)
								const 	questions	: Question[]		= await questionnaireService.get(report.questionIds)

								await rccExportService.runExport({
									data: 		report,
									context:	[questions],
								})
							}
	}


@NgModule({
	imports: [
		SharedModule,
		ExportModule,
		RouterModule.forChild(routes),
		MetaStoreModule.forChild(metaStoreConfig),
		QuestionnaireServiceModule
	],
	providers:[
		provideItemRepresentation(itemRepresentation),
		EntryMetaStoreService,
		{ provide: ENTRY_META_ACTIONS, multi:true, useValue: metaAction },
		provideTranslationMap('ENTRIES_META_STORE', { en, de })
	],
	declarations: [
		EntryMetaStorePageComponent,
		EntryLabelComponent
	],
	exports: [
		EntryMetaStorePageComponent,
		MetaStoreModule					// this is important so anything importing EntryMetaStoreModule can use the components of MetaStoreModule too
	],
})
export class EntryMetaStoreServiceModule extends BaseMetaStoreModule {

	public static forChild(
		stores?			: Type<EntryStore>[],
		metaActions?	: MetaAction<EntryConfig, Entry, EntryStore>[]
	): ModuleWithProviders<EntryMetaStoreServiceModule> {

		const mwp: ModuleWithProviders<EntryMetaStoreServiceModule>
			= BaseMetaStoreModule.getModuleWithProviders<EntryMetaStoreServiceModule>(this, stores, metaActions, ENTRY_STORES, ENTRY_META_ACTIONS)
		return mwp
	}

}
