import 	{
			Component,
		} 								from '@angular/core'

import	{	FormControl 			}	from '@angular/forms'

import	{
			EntryMetaStoreService
		}								from '../entry-meta-store.service'


@Component({
	selector: 		'rcc-meta-store-page',
	templateUrl: 	'./overview-page.component.html',
	styleUrls: 		['./overview-page.component.scss'],
})
export class EntryMetaStorePageComponent {

	public filterControl 	: FormControl	= new FormControl()

	public constructor(
		public symptomCheckMetaStoreService: EntryMetaStoreService
	) { }


}
