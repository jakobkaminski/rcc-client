import 	{
			Injectable,
			Inject,
			Optional
		} 								from '@angular/core'


import	{
			MetaStore,
			MetaAction
		}								from '@rcc/common'

import	{
			Entry,
			EntryConfig,
			EntryStore
		}								from '@rcc/core'

import	{
			ENTRY_STORES,
			ENTRY_META_ACTIONS
		}								from './entry-meta-store.commons'

@Injectable()
export class EntryMetaStoreService extends MetaStore<EntryConfig, Entry, EntryStore>{

	public readonly name = 'ENTRIES_META_STORE.NAME'

	public constructor(
		@Optional() @Inject(ENTRY_STORES)
		stores		: EntryStore[],

		@Optional() @Inject(ENTRY_META_ACTIONS)
		metaActions	: MetaAction<EntryConfig, Entry, EntryStore>[]

	) {
		super(stores, metaActions)
	}


}
