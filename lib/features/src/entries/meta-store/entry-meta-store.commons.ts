import	{	InjectionToken 	}		from '@angular/core'
import 	{
			Entry,
			EntryConfig,
			EntryStore,
		}							from '@rcc/core'

import	{	MetaAction		}		from '@rcc/common'



export const ENTRY_STORES 						: InjectionToken<EntryStore>= new InjectionToken<EntryStore>('Entry Stores')
export const ENTRY_META_ACTIONS 				: InjectionToken<MetaAction<EntryConfig, Entry, EntryStore>>= new InjectionToken<MetaAction<EntryConfig, Entry, EntryStore>>('Meta actions for entries.')
export const EntryMetaStoreServiceHomePath		: string = '/entries'
