import	{
			NgModule,
		}									from '@angular/core'
import	{
			HomePageModule,
			MainMenuModule,
			SharedModule,
			ShareDataModule,
			provideTranslationMap,
		}									from '@rcc/common'

import	{	EntryShareService			}	from './entry-share.service'
import	{	EntryShareModalComponent	}	from './modal/entry-share-modal.component'


import en from './i18n/en.json'
import de from './i18n/de.json'


export class EntryShareMenuEntry {
	public constructor(public entryShareService: EntryShareService){}
}


@NgModule({
	imports: [
		SharedModule,
		ShareDataModule,
		MainMenuModule,
		HomePageModule,
	],
	providers: [
		EntryShareService,
		provideTranslationMap('ENTRY_SHARE', { en, de }),
	],
	declarations: [
		EntryShareModalComponent
	]
})
export class EntryShareServiceModule { }
