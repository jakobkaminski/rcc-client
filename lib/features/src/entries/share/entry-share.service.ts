
import	{	Injectable						}	from '@angular/core'
import	{
			RccModalController,
			RccAlertController,
			ShareDataService,
			ButtonConfig,
			RejectButtonConfig,
			ResolveButtonConfig
		}										from '@rcc/common'

import	{
			Report,
			SessionConfig,
			SymptomCheck,
			UserCanceledError,
			Entry,
			Question,
			QuestionConfig,
			SymptomCheckStore
		}										from '@rcc/core'
import	{	ItemSelectService, 				}	from '@rcc/common'

import	{	QuestionnaireService			}	from '../../questions/questionnaire'
import	{	SymptomCheckMetaStoreService	}	from '../../symptom-checks/meta-store'
import	{	EntryMetaStoreService			}	from '../meta-store'


/**
 * TODO: This whole service should be extendable, with sharing option that can be provided by other modules.
 */

@Injectable()
export class EntryShareService {


	public constructor(
		protected rccModalController				: RccModalController,
		protected rccAlertController				: RccAlertController,
		protected shareDataService					: ShareDataService,
		protected questionnaireService				: QuestionnaireService,
		protected entryMetaStoreService				: EntryMetaStoreService,
		protected itemSelectService					: ItemSelectService,
		protected symptomCheckMetaStoreService		: SymptomCheckMetaStoreService

	){}


	public async getSessionConfigFromRawAnswers() : Promise<SessionConfig> {

		await this.entryMetaStoreService.ready

		const entries			: Entry[]			=	this.entryMetaStoreService.items

		const questionIds 		: string[]			= 	entries
														.map( entry => entry.questionId )
														.flat()

		const uniqueIds	 		: string[]			=	Array.from( new Set<string>(questionIds))

		const questions 	 	: Question[]		=  	await this.questionnaireService.get(uniqueIds)
		const questionConfigs	: QuestionConfig[]	=	questions.map(q => q.config)

		const report 		 	: Report			= 	Report.from(entries)

		return	{
					symptomCheck: 	null,
					report: 		report.config,
					questions:		questionConfigs
				}
	}

	public async getSessionConfigFromSymptomCheck(symptomCheck: SymptomCheck) : Promise<SessionConfig> {

		await this.entryMetaStoreService.ready

		const	questionIds 	: string[]				= 	symptomCheck.questionIds
		const	questions		: Question[]			= 	await this.questionnaireService.get(questionIds)
		const	questionConfigs	: QuestionConfig[]		=	questions.map( q => q.config )

		const	entries			: Entry[]				=	this.entryMetaStoreService.items.filter( entry => questionIds.includes(entry.questionId) )

		const	report			: Report				= 	Report.from(entries)

		return 	{
					symptomCheck: 	symptomCheck.config,
					report:			report.config,
					questions:		questionConfigs
				}
	}

	/**
	 * Constructs a {@link SessionConfig} from the saved Symptom Checks.
	 *
	 * If no Symptom Checks are present, use only the saved answers.
	 *
	 * If no answers (Entries) are present, show an error message.
	 */
	public async getDataToShareAsSession(): Promise<SessionConfig> {

		await this.symptomCheckMetaStoreService.ready
		await this.entryMetaStoreService.ready

		const noSymptomChecks	: boolean	= this.symptomCheckMetaStoreService.items.length === 0
		const noAnswers			: boolean	= this.entryMetaStoreService.items.length === 0


		// No Symptom Checks, no answers:

		if(noSymptomChecks && noAnswers){

			const sc_message	: string	= 	'{{ENTRY_SHARE.NO_SYMPTOM_CHECKS}}'
			const an_message	: string	= 	'{{ENTRY_SHARE.NO_ENTRIES}}'

			const message		: string	= 	[sc_message, an_message].join(' ')

			await this.rccAlertController.present({ message })

			throw new Error('no data to share')
		}


		// No Symptom Checks, some answers:
		if(noSymptomChecks && !noAnswers){

			const sc_message	: string	= 	'{{ENTRY_SHARE.NO_SYMPTOM_CHECKS}}'
			const an_message	: string	=	'{{ENTRY_SHARE.REQUEST_SHARE_RAW}}'

			const message		: string	=	[sc_message, an_message].join(' ')

			const cancelButton	: RejectButtonConfig	=	{
																label: 		'CANCEL',
																rejectAs: 	'cancel'
															}

			const answerButton	: ResolveButtonConfig	=	{
																label: 		'ENTRY_SHARE.CONTINUE_WITHOUT_SYMPTOM_CHECK',
																resolveAs: 	'raw'
															}

			const buttons		: ButtonConfig[]		=	[cancelButton, answerButton]


			await 	this.rccAlertController.present({ message, buttons })
					.catch( (reason : string) => {
						if(['dismiss', 'cancel'].includes(reason)) throw new UserCanceledError(reason)
					})


			return await this.getSessionConfigFromRawAnswers()

		}

		// // Don't show selection modal if only 1 Symptom Check is present
		// // Commented out until refactoring
		// if (this.symptomCheckMetaStoreService.items.length === 1)
		// 	return await this.getSessionConfigFromSymptomCheck(this.symptomCheckMetaStoreService.items[0])

		// Open symptom check select modal
		const stores		: SymptomCheckStore[]	= this.symptomCheckMetaStoreService.stores
		const heading		: string				= 'ENTRY_SHARE.SELECT_SYMPTOM_CHECK.HEADING'
		const subHeading	: string				= 'ENTRY_SHARE.SELECT_SYMPTOM_CHECK.SUB_HEADING'
		const message		: string				= 'ENTRY_SHARE.SELECT_SYMPTOM_CHECK.MESSAGE'
		const singleSelect	: boolean				= false
		const preselect		: SymptomCheck[]		= stores[0].items
		const symptomChecks	: SymptomCheck[]		= await this.itemSelectService.select({ stores, preselect, singleSelect, heading, subHeading, message })
		const sessionConfig : SessionConfig			= await this.getSessionConfigFromSymptomCheck(symptomChecks[0])

		return sessionConfig
	}

	public async shareSymptomCheckAsSession() : Promise<void> {

		const sessionConfig : SessionConfig
							= await this.getDataToShareAsSession()

		await this.shareDataService.share(sessionConfig, 'send', 'ENTRY_SHARE.DISCLAIMER')
	}




}
