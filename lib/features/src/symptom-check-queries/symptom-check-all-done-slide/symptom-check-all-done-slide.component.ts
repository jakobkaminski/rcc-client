import 	{
			Component,
			Input,
		}     								from '@angular/core'
import	{
			Action
		}									from '@rcc/common'

@Component({
	selector:   	'symptom-check-all-done-slide',
	templateUrl:	'./symptom-check-all-done-slide.component.html',
	styleUrls:		['./symptom-check-all-done-slide.component.css'],
})
export class SymptomCheckAllDoneSlideComponent{

	@Input()
	public allDoneToday		: boolean
	@Input()
	public doneToday		: number
	@Input()
	public totalToday		: number
	@Input()
	public doneAction		: Action
}
