import { MonitoringSetupControl } from '../monitoring-setup-control.class'

export const fourChoiceQuestion: MonitoringSetupControl = {
	label: 'MONITORING_SETUP_QUESTIONS.4_CHOICE',
	options: ['A', 'B', 'C', 'D'].map((value) => ({
		value,
		translations: {}
	})),
	answerType: 'string',

	questionMatch: (question) => {
		const hasOptions: boolean = question.options?.length === 4
		return hasOptions && question.type === 'string'
	}
}
