import { hasOptions } from '@rcc/core'
import { MonitoringSetupControl } from '../monitoring-setup-control.class'

export const numericalQuestion: MonitoringSetupControl = {
	label: 'MONITORING_SETUP_QUESTIONS.NUMERICAL',
	options: [],
	answerType: 'decimal',
	min: 0,
	showWidget: true,
	unitInput: true,

	questionMatch: (question) => {
		const isInteger		: boolean = question.type === 'decimal'
		const hasNoOptions	: boolean = !hasOptions(question.config)
		const isScale		: boolean = question.tags.includes('rcc-scale')

		return isInteger && hasNoOptions && !isScale
	}
}
