import { MonitoringSetupControl } from '../monitoring-setup-control.class'

export const quantityQuestion: MonitoringSetupControl = {
	label: 'MONITORING_SETUP_QUESTIONS.QUANTITY',
	options: [
		{ value: 0, meaning: 'no', 			translations: { en: 'no', 		de: 'nein'	} },
		{ value: 1, meaning: 'somewhat',	translations: { en: 'somewhat', 	de: 'etwas'	} },
		{ value: 2, meaning: 'moderately',	translations: { en: 'moderately', de: 'mäßig'	} },
		{ value: 3, meaning: 'strongly', 	translations: { en: 'strongly', 	de: 'stark'	} },
	],
	answerType: 'integer',

	questionMatch: (question) => {
		const hasOptions: boolean = question.options?.length === 4
		return hasOptions && question.type === 'integer'
	}
}
