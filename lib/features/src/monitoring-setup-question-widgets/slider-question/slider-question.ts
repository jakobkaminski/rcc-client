import { MonitoringSetupControl } from '../monitoring-setup-control.class'

export const sliderQuestion: MonitoringSetupControl = {
	label: 'MONITORING_SETUP_QUESTIONS.SLIDER',
	options: [],
	answerType: 'integer',
	min: 0,
	max: 100,
	tags: ['rcc-scale'],
	showWidget: true,

	questionMatch: (question) => {
		const hasNoOptions: boolean = !(question.options?.length !== 0)
		return hasNoOptions && question.type === 'integer' && question.tags.includes('rcc-scale')
	}
}
