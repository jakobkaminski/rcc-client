import	{
			Injectable
		}								from '@angular/core'
import	{
			RccTranslationService
		}								from '@rcc/common'
import	{
			Question,
			QuestionOptionConfig,
			Report,
		}								from '@rcc/core'
import	{
			Dataset,
			DatasetsService
		}								from '@rcc/features'
import	{
			PDFDocument,
			PDFFont,
			PDFPage,
			StandardFonts,
		}								from 'pdf-lib'
import	*	as windows1252				from 'windows-1252'


export type PDFData = {	questions: Question[]	}

type PageOptions = {
	pageMargin: number
}

type LineOptions = {
	lineMargin?: number
	fontSize: number
}

type LineWriter = {
	writeLine: (text: string, options: LineOptions) => void,
	newLine: (newLineHeight: number) => void,
}

@Injectable()
export class RccPdfService {

	public constructor(
		private readonly datasetsService: DatasetsService,
		private readonly translationService: RccTranslationService
	) {}


	public async createPDFOfReport(report: Report): Promise<string> {
		const pdf					: PDFDocument = await PDFDocument.create()
		const margin				: number = 20
		const fontSizeH1			: number = 28
		const headingMargin			: number = 20
		const fontSize1				: number = 16
		const fontSize2				: number = 14
		const questionMargin		: number = 16
		const answerMargin			: number = 8
		const questionSectionMargin	: number = 20

		const writer : LineWriter = this.createWriter(pdf, { pageMargin: margin })

		writer.writeLine(
			this.translationService.translate('PDF_MODULE.REPORT_TITLE'),
			{ lineMargin: headingMargin, fontSize: fontSizeH1 }
		)

		const datasets : Dataset[] = await this.datasetsService.getDatasets(report)

		datasets.forEach((dataset) => {
			const question		: Question	= dataset.question
			const questionText	: string	= this.translationService.translate(question)

			writer.writeLine(questionText, { lineMargin: questionMargin, fontSize: fontSize1 })

			dataset.datapoints.forEach(datapoint => {
				const answerOption : QuestionOptionConfig = question.options?.find(
					option => option.value === datapoint.value
				)

				const translated : string = this.translationService.translate(answerOption)
				const answerText : string = translated === '[undefined]' ? '' : `${translated} `

				const answerLineText : string = `${datapoint.value as string} - ${answerText}(${datapoint.date})`
				writer.writeLine(answerLineText, { lineMargin: answerMargin, fontSize: fontSize2 })
			})

			writer.newLine(questionSectionMargin)
		})

		return await pdf.save() as unknown as string
	}

	/**
	 * @returns A function to write a new line to the PDF, this handles the tracking
	 * of the current line position
	 */
	private createWriter(pdf: PDFDocument, options: PageOptions): LineWriter {
		let currentPage			: PDFPage	= pdf.addPage()
		const { width, height }	= currentPage.getSize()
		const allowedWidth		: number	= width - options.pageMargin * 2
		const font				: PDFFont	= pdf.embedStandardFont(StandardFonts.Helvetica)

		// The y coordinates of the page go from 0 (bottom of the page), to [height]
		// (top of the page) - so to write top down, we set the y coordinates
		// to the height of the page, - minus the margin
		const initialYPosition	: () => number  = () => height - options.pageMargin
		let currentYPosition	: number = initialYPosition()

		const writeLine : (lineText: string, fontSize: number) => void = (lineText, fontSize) => {
			// To determine the y position that we actually write at, we
			// subtract the font size of the text we are writing divided
			// by 2, as the y position refers to the vertical center of
			// the text
			const y : () => number = () => currentYPosition - fontSize / 2

			const needToAddNewPage : boolean = currentYPosition - fontSize < options.pageMargin
			if (needToAddNewPage) {
				currentPage = pdf.addPage()
				currentYPosition = initialYPosition()
			}
			currentPage.drawText(lineText, { x: options.pageMargin, y: y(), size: fontSize, font })
			currentYPosition -= fontSize
		}

		return {
			writeLine: (text, lineOptions) => {
				const printText				: string	= this.replaceInvalidCharactersWithQuestionMarks(text, font)
				const lineWidth				: number	= font.widthOfTextAtSize(printText, lineOptions.fontSize)
				const textTooWideForPage	: boolean	= lineWidth > allowedWidth

				if (textTooWideForPage) {
					const lines : string[] = this.calculateLineBreaks(printText, font, lineOptions.fontSize, allowedWidth)
					lines.forEach((line) => writeLine(line, lineOptions.fontSize))
				} else
					writeLine(printText, lineOptions.fontSize)

				currentYPosition -= (lineOptions.lineMargin || 0)
			},
			newLine: (newLineHeight) => {
				currentYPosition -= newLineHeight
			}
		}
	}

	private replaceInvalidCharactersWithQuestionMarks(text: string, font: PDFFont): string {
		// allow only windows1252 chars as the current font only supports those
		// remove windows-1252 dependency once font changes
		const win1252: Uint16Array = windows1252.encode(text, { mode: 'replacement' })
		// replace windows1252 chars not supported by the font with ?
		const filteredText: Uint16Array = win1252.map((char) => font.getCharacterSet().includes(char) ? char : 63)
		return windows1252.decode(filteredText)
	}

	/**
	 * Uses bisection search to determine the maximum length of words that can fit on a line
	 *
	 * @returns a list of lines, each of which is short enough to fit on the allowed width of the page
	 */
	private calculateLineBreaks(text: string, font: PDFFont, fontSize: number, allowedWidth: number): string[] {
		const words 		: string[]	= text.split(' ')
		const wordLength 	: number	= words.length

		/** Used to track the start of the current line - starting with the first word */
		let start	: number = 0
		/**
		 * Used to track the current lowest current guess of the bisection search, will
		 * be updated when the current guess is too short
		 */
		let lowest	: number = start
		/**
		 * Used to track the current highest guess of the bisection search, will be updated
		 * when the current guess is too long
		 */
		let end		: number = wordLength

		/** Used to track the middle point between the lower and upper markers of the bisection search */
		let middle	: number = 0
		const determineMiddle : () => number = () => Math.floor((end - lowest) / 2) + lowest
		middle = determineMiddle()

		let finished	: boolean	= false
		const lines 	: string[]	= []

		while (!finished) {
			const line	: string	= words.slice(start, middle).join(' ')
			const width	: number	= font.widthOfTextAtSize(line, fontSize)

			if (width > allowedWidth) {
				end		= middle
				middle	= determineMiddle()
			}

			if (width <= allowedWidth) {
				lowest	= middle
				middle	= determineMiddle()
			}

			if (middle === lowest) {
				lines.push(words.slice(start, lowest).join(' '))

				start	= lowest
				lowest	= start
				end		= wordLength
				middle	= determineMiddle()
				const remainingLine : string = words.slice(start, end).join(' ')

				if (font.widthOfTextAtSize(words.slice(start, end).join(' '), fontSize) < allowedWidth) {
					lines.push(remainingLine)
					finished = true
				}
			}

		}

		return lines
	}
}
