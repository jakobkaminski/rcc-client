
import	{	Injectable						}	from '@angular/core'
import	{
			RccModalController,
		}										from '@rcc/common/modals-provider'
import	{
			QrCodeService,
		}										from '@rcc/common/qr-codes'
import	{
			RccAlertController,
		}										from '@rcc/common/modals-provider'
import	{	CameraSwitchSendModalComponent	}	from './modals/camera-switch-send-modal.component'
import	{	EntryShareService				}	from '../entries'

/**
 * TODO: This whole service should be extendable, with sharing option that can be provided by other modules.
 */

@Injectable()
export class CameraSwitchService {

	public constructor(
		protected rccModalController				: RccModalController,
		protected rccAlertController				: RccAlertController,
		protected entryShareService					: EntryShareService,
		protected qrCodeService						: QrCodeService

	){}

	public async send(): Promise<void> {
		const result: string = await this.rccModalController.present(CameraSwitchSendModalComponent)

		if(result === 'recipient-has-camera') 		return void this.entryShareService.shareSymptomCheckAsSession()
		if(result === 'recipient-has-no-camera')	return void this.qrCodeService.scanAndAnnounce()

		throw(new Error('Unexpected result from camera switch modal:'+ result))

	}
}
