import	{
			NgModule,
		}												from '@angular/core'
import	{	CameraSwitchService						}	from './camera-switch.service'
import	{	CameraSwitchSendModalComponent			}	from './modals/camera-switch-send-modal.component'
import	{
			SharedModule,
			provideTranslationMap,
		}												from '@rcc/common'
import	{	RccCardComponent						}	from '@rcc/common/ui-components/card/card.component'
import	{	RccColorCategoryDirective				}	from '@rcc/common/ui-components/color-category/color-category.directive'
import	{	RccFabComponent							}	from '@rcc/common/ui-components/fab/fab.component'
import	{	TranslationsModule 						}	from '@rcc/common/translations'
import	{
			RccRadioSelectComponent,
			RccReducedFullPageComponent
		}												from '@rcc/themes/active'

import en from './i18n/en.json'
import de from './i18n/de.json'

export class CameraSwitchMenuEntry {
	public constructor(public cameraSwitchService: CameraSwitchService){}
}

@NgModule({
	imports: [
		RccCardComponent,
		RccColorCategoryDirective,
		RccFabComponent,
		RccRadioSelectComponent,
		RccReducedFullPageComponent,
		TranslationsModule,
		SharedModule,
	],
	providers: [
		CameraSwitchService,
		provideTranslationMap('CAMERA_SWITCH', { en, de })
	],
	declarations: [
		CameraSwitchSendModalComponent,
	]
})
export class CameraSwitchServiceModule { }
