import	{	Component						}	from '@angular/core'
import	{	FormControl						}	from '@angular/forms'
import	{
			UserCanceledError
		}										from '@rcc/core'
import	{
			Action,
		}										from '@rcc/common/actions'
import	{
			RccModalController,
		}										from '@rcc/common/modals-provider'
import	{	RccSelectOption					}	from '@rcc/themes/theming-mechanics'


type Options = 'recipient-has-camera' | 'recipient-has-no-camera'

@Component({
	templateUrl:	'./camera-switch-send-modal.component.html',
	styleUrls:		['./camera-switch-send-modal.component.scss'],

})
export class CameraSwitchSendModalComponent {

	public constructor(
		private rccModalController		: RccModalController,
	){}

	public HEADING						: string = 'CAMERA_SWITCH.SEND.TITLE'
	public SUBHEADING					: string = 'CAMERA_SWITCH.SEND.DESCRIPTION'
	public EXPLANATION					: string = 'CAMERA_SWITCH.SEND.EXPLANATION'
	public QUESTION						: string = 'CAMERA_SWITCH.SEND.QUESTION'

	protected options: RccSelectOption<Options>[] = [
		{ value: 'recipient-has-camera', label: 'CAMERA_SWITCH.SEND.CAMERA' },
		{ value: 'recipient-has-no-camera', label: 'CAMERA_SWITCH.SEND.NO_CAMERA' }
	]

	protected switchCameraControl: FormControl<Options> = new FormControl<Options>('recipient-has-camera')

	protected cancelAction: Action = {
		icon: 'close',
		label: 'CANCEL',
		handler: () => this.cancel()
	}

	public cancel() : void {
		this.rccModalController.dismiss(new UserCanceledError() )
	}

	protected submit(): void {
		this.rccModalController.dismiss(this.switchCameraControl.value)
	}

}
