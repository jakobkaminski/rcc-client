import	{
			Component,
			Input,
			OnInit,
			OnDestroy,
		}								from '@angular/core'

import	{
			FormControl,
			FormGroup,
		}								from '@angular/forms'

import	{
			SubscriptionLike
		}								from 'rxjs'

import	{
			assert,
			assertProperty,
			Schedule				}	from '@rcc/core'

import	{
			ModalWithResultComponent,
			RccAlertController,
			RccModalController,
			ItemEditResult
		}								from '@rcc/common'

interface ScheduleControlGroupValues {
	restrictTimes	: boolean
	restrictDays	: boolean
	daysOfWeek		: { [index:string] : boolean }
	timesOfDay		: { [index:string] : boolean }
}



/**
 * Takes a schedule and emits and edited copy of it.
 */
@Component({
	selector: 		'rcc-schedule-edit',
	templateUrl: 	'./schedule-edit.component.html',
	styleUrls: 		['./schedule-edit.component.scss'],
})
export class ScheduleEditComponent extends ModalWithResultComponent<ItemEditResult<Schedule>> implements OnInit, OnDestroy {

	@Input()
	public set item(schedule: Schedule) { this.updateFromSchedule(schedule) }

	@Input()
	public editTimes				: 	boolean
									=	false


	public 	days					: 	string[]
									=	['su', 'mo', 'tu', 'we', 'th', 'fr', 'sa']

	public	times					: 	string[]
									=	['morning', 'afternoon', 'evening', 'night']

	public 	restrictDaysControl		: 	FormControl<boolean>
									= 	new FormControl<boolean>(false)

	public 	restrictTimesControl	: 	FormControl<boolean>
									= 	new FormControl<boolean>(false)

	public 	dayControlGroup			: 	FormGroup<{[index:string] : FormControl<boolean> }>
									= 	new FormGroup<{[index:string] : FormControl<boolean> }>({
											'mo': new FormControl<boolean>(false),
											'tu': new FormControl<boolean>(false),
											'we': new FormControl<boolean>(false),
											'th': new FormControl<boolean>(false),
											'fr': new FormControl<boolean>(false),
											'sa': new FormControl<boolean>(false),
											'su': new FormControl<boolean>(false)
										})

	public timeControlGroup			: 	FormGroup<{[index:string] : FormControl<boolean> }>
									=	new FormGroup<{[index:string] : FormControl<boolean> }>({
											'morning'	: new FormControl<boolean>(false),
											'afternoon'	: new FormControl<boolean>(false),
											'evening' 	: new FormControl<boolean>(false),
											'night' 	: new FormControl<boolean>(false)
										})

	public scheduleControlGroup		:	FormGroup
									=	new FormGroup({
											restrictTimes:	this.restrictTimesControl,
											restrictDays:	this.restrictDaysControl,
											daysOfWeek:		this.dayControlGroup,
											timesOfDay: 	this.timeControlGroup
										})

	public scheduleEdit				: 	Schedule
									=	new Schedule()

	private subscription 			: 	SubscriptionLike

	public constructor(
		protected rccAlertController	: RccAlertController,
		protected rccModalController	: RccModalController,

	){
		super()
	}


	public ngOnInit(): void {

		this.subscription = this.scheduleControlGroup.valueChanges
							.subscribe( (result : ScheduleControlGroupValues) => this.updateFromInput(result) )

	}

	protected updateFromSchedule(schedule: Schedule) : void {

		this.scheduleEdit = Schedule.copy(schedule)

		this.restrictDaysControl.setValue(!this.scheduleEdit.everyDay)
		this.restrictTimesControl.setValue(!this.scheduleEdit.allDay)

		this.scheduleEdit.daysOfWeek.forEach(

			(day:number)	=> 	this.dayControlGroup
								.controls[this.days[day]]
								.setValue(true)
		)

		this.scheduleEdit.timesOfDay.forEach(

			(time:string)	=> 	this.timeControlGroup
								.controls[time]
								.setValue(true)
		)
	}

	protected updateFromInput(result: ScheduleControlGroupValues) : void {


		const daysOfWeek 	:	number[]
							= 	Object.keys(result.daysOfWeek)
								.filter(	key => result.daysOfWeek[key] )
								.map(		day => this.days.indexOf(day) )

		const timesOfDay 	:	string[]
							=	Object.keys(result.timesOfDay)
								.filter(	key => result.timesOfDay[key] )


		const config		:	[number[], string[]]
							=	[
									result.restrictDays
									?	daysOfWeek
									:	[],

									this.editTimes
									?	result.restrictTimes
										?	timesOfDay
										:	[]
									:	[...(this.scheduleEdit.timesOfDay || [])]
								]

		Schedule.assertData(config)

		this.scheduleEdit.config = config
	}


	public assertResult(x: unknown) : asserts x is ItemEditResult<Schedule> {
		assertProperty(x, 'item', 			'ItemSelectModalComponent.assertResult()')
		assert(x.item instanceof Schedule,	'ItemSelectModalComponent.assertResult() .item is not and instance of Schedule')
	}


	public getResult(): ItemEditResult<Schedule> {
		return { item: this.scheduleEdit }
	}

	public async cancel(): Promise<void> {

		await this.rccAlertController.confirm(
				'CONFIRM_CANCEL',
				'YES',
				'NO'
				)

		super.cancel()
	}

	public ngOnDestroy() : void {
		this.subscription.unsubscribe()
	}


}
