import	{	Injectable				}	from '@angular/core'
import	{
			RccModalController,
			ItemEditService
		}								from '@rcc/common'
import	{
			Schedule,
		} 								from '@rcc/core'
import	{	ScheduleEditComponent	}	from './schedule-edit.component'


/**
 * TODO: Improve documentation.
 *
 * This service triggers the UI to edit or create a {@link Schedule}.
 */
@Injectable()
export class ScheduleEditService extends ItemEditService<Schedule>{

	public constructor(
		public rccModalController : RccModalController
	){
		super(
			Schedule,
			ScheduleEditComponent,
			rccModalController
		)
	}

}
