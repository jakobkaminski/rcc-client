import	{ NgModule 						} 	from '@angular/core'
import	{
			provideSettingsEntry,
			provideTranslationMap
		} 									from '@rcc/common'

@NgModule({
	providers:[
		provideTranslationMap('SETTINGS_ENTRY_GROUPS.REMINDERS', {
			en: {
				LABEL: 			'Reminders',
				DESCRIPTION:	'Everything concerning reminders.'
			},

			de: {
				LABEL: 			'Erinnerungen',
				DESCRIPTION:	'Alles, was Erinnerungen betrifft.'
			}
		}),

		provideSettingsEntry({
			id: 			'reminder_group',
			type:			'group',
			label:			'SETTINGS_ENTRY_GROUPS.REMINDERS.LABEL',
			description:	'SETTINGS_ENTRY_GROUPS.REMINDERS.DESCRIPTION',
			subSettingIds:	['day-query-reminder', 'ics-reminder'],
			icon:			'settings',
			position:		3

		})
	]
})
export class RccReminderSettingsEntryGroupModule {}
