import	{
			Injectable,
			OnDestroy
		}						from '@angular/core'

import	{
			Observable,
			Subscription
		}						from 'rxjs'
import	{
			RccBuildInfoService
		}						from '@rcc/common'

export interface RccUsageLoggingJob {
	tick$				: Observable<unknown>
	description			: string
	label				: string
	loggingData			: LoggingDataObject
}

export interface LoggingDataObject {
	/** category that describes the scope of the logging job, eg. the module that it is located in, will be appended with the version and flavor  */
	category			: string,
	/** describes the action that happened */
	key					: string,
	/** can be used to log extra information that can be used to group log entries */
	extraValue?			: string,
	/** numeric value that can be used to calculate median values or sums for a specific action */
	computableValue?	: number,
	includeUserId		: boolean,
	requiresUserConsent	: boolean,
}

@Injectable()
export class RccUsageLoggingService implements OnDestroy{

	private subscriptions	:	Subscription[]			=	[]
	private jobs			:	RccUsageLoggingJob[]	=	[]

	public constructor(private buildInfoService: RccBuildInfoService) {
	}

	public registerLoggingJob(job: RccUsageLoggingJob): void {

		const sub: Subscription = job.tick$.subscribe( () => {
			const version : string = this.buildInfoService.buildInfos.tag
			const flavor : string = this.buildInfoService.buildInfos.flavor

			this.logEvent(job.loggingData, flavor, version)
		})

		this.subscriptions.push(sub)
		this.jobs.push(job)

	}

	protected logEvent(loggingDataObject: LoggingDataObject, flavor: string, version: string): void {
		console.warn('no logging implementation used, tried to log' + loggingDataObject.key + ' - ' + flavor + ' - ' + version)
	}

	public ngOnDestroy() : void {
		this.subscriptions.forEach(sub => sub.unsubscribe())
	}

}
