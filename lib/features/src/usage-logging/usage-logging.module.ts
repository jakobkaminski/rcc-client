import	{
			NgModule
		} 							from '@angular/core'

import	{
			provideTranslationMap
		} 							from '@rcc/common'
import	{
			RccUsageLoggingService
		}							from './usage-logging.service'

import en from './i18n/en.json'
import de from './i18n/de.json'

@NgModule({
	providers: [
		RccUsageLoggingService,
		provideTranslationMap('DATA_LOGGING', { en, de })
	],
	declarations: [],
})
export class RccUsageLoggingModule {


}
