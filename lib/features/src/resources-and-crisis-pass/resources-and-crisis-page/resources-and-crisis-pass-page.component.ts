import { Component } from '@angular/core'
import { FormControl } from '@angular/forms'
import { Action } from '@rcc/common'
import { Option } from '@rcc/themes/theming-mechanics/components/segment/segment-base.component'

enum Mode {
	resources = 'resources',
	crisisPass = 'crisis-pass',
}

@Component({
	selector:		'rcc-resources-and-crisis-pass-page',
	templateUrl:	'./resources-and-crisis-pass-page.component.html',
	styleUrls:		['resources-and-crisis-pass-page.component.scss'],
})
export class RccResourcesAndCrisisPassPageComponent {
	protected segmentOptions: Option[] = [
		{
			label: 'RESOURCES_AND_CRISIS_PASS.RESOURCES',
			value: Mode.resources,
		},
		{
			label: 'RESOURCES_AND_CRISIS_PASS.CRISIS_PASS',
			value: Mode.crisisPass,
		}
	]

	protected formControl: FormControl<Mode> = new FormControl<Mode>(Mode.resources)

	protected get resourceMode(): boolean {
		return this.formControl.value === Mode.resources
	}

	protected get crisisPassMode(): boolean {
		return this.formControl.value === Mode.crisisPass
	}
	
	protected editMode: boolean
	protected get switchEditModeAction(): Action {
		return {
			icon: undefined,
			label: this.editMode ? 'RESOURCES.EDIT.DONE' : 'RESOURCES.EDIT.START',
			handler: () => { this.editMode = !this.editMode }
		}
	}
}
