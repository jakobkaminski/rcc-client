import { NgModule } from '@angular/core'
import { SharedModule, provideTranslationMap } from '@rcc/common'
import { RccResourcesAndCrisisPassPageComponent } from './resources-and-crisis-page/resources-and-crisis-pass-page.component'
import { RouterModule, Routes } from '@angular/router'
import { RccResourcesModule } from '../resources/resources.module'
import { RccCrisisPassModule } from '../crisis-pass/crisis-pass.module'

import en from './i18n/en.json'
import de from './i18n/de.json'

export const RESOURCES_AND_CRISIS_PASS_PATH: string = 'resources-crisis'

const routes: Routes = [
	{
		path:		RESOURCES_AND_CRISIS_PASS_PATH,
		title:		'RESOURCES_AND_CRISIS_PASS.PAGE_TITLE',
		component:	RccResourcesAndCrisisPassPageComponent,
	}
]

@NgModule({
	providers: [
		provideTranslationMap('RESOURCES_AND_CRISIS_PASS', { en, de }),
	],
	imports: [
		RouterModule.forChild(routes),
		SharedModule,
		RccResourcesModule,
		RccCrisisPassModule,
	],
	declarations: [
		RccResourcesAndCrisisPassPageComponent,
	]
})
export class RccResourcesAndCrisisPlanModule {
}
