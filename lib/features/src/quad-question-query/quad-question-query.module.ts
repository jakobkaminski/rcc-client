import { 	NgModule 						}	from '@angular/core'

import {
			provideWidget,
			SharedModule,
			WidgetsModule,
}												from '@rcc/common'

import 	{
			QuadQuestionQueryWidgetComponent
		} 										from './quad-question-query-widget/quad-question-query-widget.component'


@NgModule({
	imports: [
		SharedModule,
		WidgetsModule,
	],
	providers: [
		provideWidget(QuadQuestionQueryWidgetComponent)
	],
	declarations: [
		QuadQuestionQueryWidgetComponent
	],
	exports: [
		QuadQuestionQueryWidgetComponent
	]
})
export class QuadQuestionQueryModule {}
