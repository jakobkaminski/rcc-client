import	{
			Injectable,
			Type
		}								from '@angular/core'

import	{
			ModalController,
		}								from '@ionic/angular'
import 	{	OverlayEventDetail		}	from '@ionic/core'
import	{
			ModalPresentationOptions,
			RccModalController,
		}								from '@rcc/common'
import	{	PropertiesOf			}	from '@rcc/core/types'

@Injectable()
export class IonicModalController implements RccModalController {

	public constructor(
		public modalController: ModalController
	){}

	private modalIds	: string[] = []
	/** Stores modal references based on their ID */
	private modals		: Map<string, HTMLIonModalElement> = new Map()
	private lastId		: number = 0

	public async present<T, R = unknown>(component: Type<T>, data?: PropertiesOf<T>, options?: ModalPresentationOptions) : Promise<R>{

		this.lastId++

		const id	: string				= 'modal-'+ this.lastId.toString()
		const modal	: HTMLIonModalElement	= await this.modalController.create({
			component,
			componentProps: data,
			id,
			cssClass: this.cssClasses({
				'modal-fullscreen': options?.fullScreen,
				'modal-mini': options?.mini,
				'modal-fit-content': options?.fitContent,
			}),
			showBackdrop: !options?.fullScreen,
			canDismiss: options?.canDismiss ?? true,
		})

		this.modalIds.push(id)
		this.modals.set(id, modal)

		const result: unknown = await this.modalDismissed(modal, id)

		if(result instanceof Error) throw result

		return result as R
	}

	private async modalDismissed(modal: HTMLIonModalElement, id: string): Promise<unknown> {
		await modal.present()
		const result: OverlayEventDetail = await modal.onDidDismiss()

		this.modalIds 	= this.modalIds.filter(_modal => _modal !== id)
		return result.data as unknown
	}

	public async dismiss(data:unknown): Promise<void>{
		const modalIdToDismiss: string = this.modalIds[this.modalIds.length - 1]
		const modal: HTMLIonModalElement = this.modals.get(modalIdToDismiss)
		modal.canDismiss = true
		await this.modalController.dismiss(data, '', modalIdToDismiss)
		this.modals.delete(modalIdToDismiss)
	}

	/**
	 * @param options A record of potential css classes, with a value
	 * indicating whether they should be shown or not
	 *
	 * @returns A filtered list of css classes
	 */
	private cssClasses(options: Record<string, unknown>): string[] {
		return Object.entries(options)
			.filter(([, condition]) => !!condition)
			.map(([cssClass]) => cssClass)
	}
}
