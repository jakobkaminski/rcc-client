import	{	NgModule		}	from '@angular/core'
import	{	IconsModule		}	from '@rcc/common'



@NgModule({
	declarations: [],
	imports: [
		IconsModule.forChild(new Map<string, string>([
			['back',				'arrow-back'],
			['add',					'add-outline'],
			['new',					'add-outline'],
			['search',				'search-outline'],
			['filter',				'search-outline'],
			['create-from',			'duplicate-outline'],

			['actions',				'ellipsis-vertical'],
			['active',				'checkmark'],
			['apply',				'checkmark'],
			['backup',				'exit-outline'],
			['chart',				'analytics-outline'],
			['calendar',			'calendar-outline'],
			['close',				'close'],
			['delete',				'trash-outline'],
			['dev',					'bug-outline'],
			['edit',				'pencil'],
			['end',					'play-skip-forward-outline'],
			['export',				'open-outline'],
			['fill',				'create-outline'],
			['home',				'home-outline'],
			['language',			'language-outline'],
			['menu',				'menu-outline'],
			['negative_feedback',	'close'],
			['next',				'caret-forward-outline'],
			['notification',		'notifications-outline'],
			['pause',				'pause'],
			['positive_feedback',	'checkmark'],
			['previous',			'caret-back-outline'],
			['qr-code',				'qr-code-outline'],
			['query',				'pencil'],
			['reminder',			'alarm-outline'],
			['remove',				'remove-circle-outline'],
			['restore',				'enter-outline'],
			['revert',				'refresh-outline'],
			['scan',				'qr-code-outline'],
			['schedule',			'time-outline'],
			['select',				'albums-outline'],
			['select-items',		'list-outline'],
			['settings',			'settings-outline'],
			['share',				'share-outline'],
			['start',				'caret-forward-outline'],
			['time',				'time-outline'],
			['unknown',				'alert-outline'],
			['update',				'arrow-up-circle-outline'],
			['view',				'eye-outline'],

			// Items,
			['entry',				'reader-outline'],
			['question',			'help-circle-outline'],
			['symptom-check', 		'list-circle-outline'],
			['report',				'reader-outline'],
			['session',				'people-outline']
		]))
	]
})
export class IonicIconsModule { }
