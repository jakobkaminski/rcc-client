# Guide zum Bau und zur Eingliederung neuer Pages in die Application

*Für weitere Informationen besuche diese [Page](https://docs.recoverycat.de/modules/ExamplePageModule.html#source)*

1. Erstelle einen Branch.([Guide](guide-zur-benutzung-von-terminal-und-git-befehlen.html))
2. Generiere ein Modul im Zielordner.

```console
$ ng g m MODULNAME
```

3. Importiere die Grundmodule.

```ts

import 	{
			Component,
			NgModule
		} 								from '@angular/core'

import	{	RouterModule			}	from '@angular/router'

import	{
			SharedModule,
			MainMenuModule,
			MainMenuEntry,
			TranslationsModule,
			provideMainMenuEntry
		}								from '@rcc/common'
```

Dazu gehören für die RCC App alle oben genannten.

## Page mit Content erstellen

Erstelle und eine Class mit *export* und ein dazugehöriges Template in @Component({})

```ts
@Component({
	template:  	`
					<ion-content>

						<h1> My example page </h1>

						<p> My example content </p>

						<p> {{'EXAMPLES.EXAMPLE_TRANSLATION' | translate }} </p>

					</ion-content>
				`,
})
export class MyExamplePage{}
```

Das Template kann ebenso ausgelagert werden in eine externe HTML Datei mit Verlinkung auf folgende Weise.
```ts
templateUrl:	'./example.html',
```

## Menüeintrag erstellen

Im Module der neuen Page für den Menüeintrag eine neue Class mit *export* erstellen

```ts
@Component({
	template:	`
					<ion-item routerLink = "examples/example-page">
						<ion-label>{{ "EXAMPLES.EXAMPLE_PAGE_MENU_ENTRY" | translate }}</ion-label>
					</ion-item>
				`
})
export class MyExampleMenuEntry {}


const routes 				=	[
									{
										path: 		'examples/example-page',
										component: 	MyExamplePage
									}
								]
```
Entsprechenden Pfad im Objekt unter dem Schlüssel *path* angeben.

## Übersetzungen implementieren

Übersetzungen können mit entsprechenden Sprachenobjekten (en, de etc.) eingebaut werden.

```ts
const	en =	{
					'EXAMPLE_PAGE_MENU_ENTRY':	'ExamplePage',
					'EXAMPLE_TRANSLATION': 		'Translated text example (en)'
				}

const	de =	{
					'EXAMPLE_PAGE_MENU_ENTRY':	'Beispielseite',
					'EXAMPLE_TRANSLATION': 		'Übersetztes Textbeispiel (de)'
				}
```

## Einrichtung von NgModule

Unter *declarations* die beiden Classes der Page und des Menüeintrags einbinden. Abschließend das Page Module exportieren.

```ts
@NgModule({
	imports: [
		SharedModule,
		RouterModule.forChild(routes),

	],
	providers: [
		provideMainMenuEntry({position: -1, component: MyExampleMenuEntry}),
		provideTranslationMap('EXAMPLES', { en, de })
	],
	declarations:[
		MyExamplePage,
		MyExampleMenuEntry
	]
})
export class ExamplePageModule {}
```

Schließlich mit folgendem Befehl den lokalen Server starten:

```console
ng serve @rcc/app/WORKSPACE
```

Bei WORKSPACE den jeweiligen Workspace einfügen.

