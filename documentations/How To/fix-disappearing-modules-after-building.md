# Fix Disappearing Modules after Building - Tree-Shaking Issue and Fix

## Problem Description

Recently, we encountered a problem where certain modules,
specifically the ```MedicalFeatureModule``` and ```MedicalProductModule```, stopped functioning.
Our first debug session suggested that these modules were being tree-shaken out or removed from the final build.
This behavior is expected when a module isn't being used anywhere within the application.
However, in our scenario, these modules were very much in use.

Here we used the ```MedicalProductModule```
```typescript
[...]
import	{	MedicalProductModule			}	from '@rcc/common/src/medical/medical-product/medical-product.module'
import	{	RccHcpUserManualModule			}	from '@rcc/features/user-manual/hcp-user-manual/hcp-user-manual.module'

@NgModule({
	imports: [
		[...]
		MedicalProductModule,
	],
})

export class RccFeaturesModule { }

```

and the ```MedicalFeatureModule```
```typescript
[...]
import	{	MedicalFeatureModule	}	from '@rcc/common/src/medical/medical-feature/medical-feature.module'
[...]
@NgModule({
	imports:	[
		SharedModule,
		RouterModule.forChild(routes),
		MedicalFeatureModule,
	],
	[...]
})
export class GlossaryModule{}

```

After we investigated some more, we noticed that unlike our other modules,
the problematic modules weren't exported via ```index.ts``` files.
We suspected that this lack of an export might have prompted Angular to exclude these modules from the build.

## Solution

### 1. Export Modules Using index.ts Files:

To address this issue, we made sure that both ```MedicalFeatureModule``` and ```MedicalProductModule``` were exported through ```index.ts``` files, similar to our other modules.

```lib/common/src/medical/index.ts```
```typescript
export * from './medical-product/medical-product.module'
export * from './medical-feature/medical-feature.module'
```

```lib/common/index.ts```
```typescript
[...]
export * from './main-menu'
export * from './medical'
export * from './meta-store'
[...]
```


### 2. Addressing Angular's Build Cache:

An unusual aspect of this problem was that, despite making the above changes, the modules remained absent from the build. In the end we figured out that angular seems to
cache some information about what to include in the build, which was unaffected by the ```index.ts``` changes.

to fix this:
 - Delete the ```.angular``` folder. This will clear Angular's build cache.
 - rebuild the application

this should fix any "stuck" tree-shaking issue.
