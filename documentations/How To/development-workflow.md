# Development Workflow

This document lays down a guide for a small team working with GitLab in parallel. It emphasizes a frictionless work
flow that allows all developers to work on the project on their own schedule while ensuring code quality and
improving general knowledge about the code base and project state for all team members.

## Protected branches and feature branches

The workflow relies on a specific branch structure. In general, we differentiate between protected branches and
feature branches. Feature branches are tied to issues and contain work in progress features. No more than two team
members should work on one feature branch at the same time. Feature branches will be deleted after merge. Protected
branches combine completed features and represent the current state of the product in development. They should only
contain changes merged from feature branches - team members should never make or push changes directly onto protected
branches.

#### Before release

As long as the product has not gone public there is one protected branch (`main`). All feature branches will be
merged into this branch eventually until the product is ready to be published.

When the product has gone public another protected branch (`production`) is introduced. From now on `production` is always
identical with the current state of the publicly available product, while improvements, new features and fixes are
compiled in `main` until they are ready to be released - completed feature branches will merge into `main`. `production`
will only ever receive changes in two ways: Fixes for critical problems that have to be fast tracked, and deployment
ready states of `main`. Changes to `production` should be published as soon as possible so the branch stays in sync with
the published product.

## Issues

All development is based on issues. For the sake of transparency team members should only work on features/fixes that
are defined by an issue. If something comes up that is not defined by an issue please create an issue before starting
work on the code. Issues should be

-   **defined**: all necessary information is in the issue or linked to from the issue
-   **self-contained**: issues can depend on each other but should not require to be worked on in parallel
-   **iterable**: features should be broken down into iterations that build on top of each other to ensure progress
    and parallel working
-   **limited**: issues should be broken down into actionable pieces that can be worked on in a couple of days
-   **testable**: preferably the changes of the issue are exposed, i.e. if metadata should be changed the issue
    should also contain exposing it in the frontend or a request payload

Please make sure that issue titles are descriptive.

#### Labels

Use labels to allow team members to gain information about an issue at a glance.

-   Priority labels (like `Priority::High`) inform team members which issues should be prioritized.
-   Labels for running epics
-   Labels for issues that are particularly suited for onboarding of new team members
-   If team members specialize in certain kind of tasks (like documentation or layout/styling), labels should be used to allow them to find fitting issues
-   Blocking labels (like `Blocked::content` for missing content) can be used to signal that issues can not be worked on at the moment
-   Bug fix and maintenance issues should be labeled accordingly
-   Labels that fell out of use should be deleted

#### Milestones

Use milestones to group issues that belong to the same domain, a feature set or a time sensitive development goal. If
possible, set due dates for milestones.

## Commits

Try to avoid big commits with lots of changes.

Commit messages can be a helpful tool to explore a projects change history when they are written well. Commit messages
should at least entail a short (try to stay under 50 characters), but conclusive description of the changes made. If a
commit needs explanation or information about context please provide a body to the commit message, separated from the
subject line by a single blank line:

```
Wipe astro droid memory

Recently bought astro droid looped the same holographic message. Only wiping the internal memory
```

Try to avoid single word or non-descriptive messages like `cleanup` or `wip`. For more information on well written commit
messages please refer to [How to Write a Git Commit Message](https://cbea.ms/git-commit)

## Feature branches

Feature branches should be created out of and stay linked to an issue. The GitLab GUI can be used to create feature branches
and merge requests (MR), but we want to keep the number of MRs in `Draft` state to a minimum. The following steps are recommended:

-   Pick an issue and assign yourself to it.
-   Create a feature branch _###-title-of-the-issue_; the title can be adjusted as long as the branch name starts with the issue
    number
-   Once your changes are ready for review, create a MR from the branch and make sure the tile refers to the issue and that the MR
    includes the `Closes ###` text to automatically close the issue once the MR is merged. Assign yourself to the MR as well.
-   Make sure the MR is in `Draft` state as long as it is work in progress

#### When you are done

When you deem the feature ready set the MR to ready (this will remove the `Draft:` prefix) to signal the MR is ready for
code review. Before you do that please make sure that

-   Your code is clean and well-structured (remove code that has been commented out, `console.log` statements, unused vars
    and private methods and so on),
-   linting does not produce errors or warnings in the context of your feature,
-   there are no failing tests in the context of your feature (please create an issue when tests fail that are not within
    the scope of the issue),
-   projected merge conflicts are solved, and
-   your code is documented as agreed upon.

_Optional:_ If there is a team member that should review the MR (because they wrote the issue, are well versed with the
specific technology or other reasons) you can request a review from this person by assigning them as reviewer.

## Code reviews

Every team member is allowed and encouraged to do code reviews. This is a good way to learn about the code base and get a more
holistic picture of the project. Just check the open MRs and pick one that is not in draft state anymore. Assign yourself as
reviewer. Code reviews are not only a way to ensure code quality but should allow team members to learn how to write better
code and understand the intent behind a certain implementation. Please make sure to explain _why_ you think a certain
implementation is not optimal and _how_ it can be improved. Needless to say, be respectful and constructive in your criticism.

When reviewing code please look for the following

-   Does the MR solve the issue?
-   Is the implementation sound?
-   Does it compile? Are there warnings?
-   Do all tests succeed?
-   Does it break existing features?
-   Does the code conform to agreed upon guidelines (code style, naming conventions, architectural patterns...)?
-   Is the code clean and well readable?
-   Are there any leftovers from development (like `console.log` statements, code that has been commented out, unused variables or imports...)?

Use inline comments for feedback on specific lines or blocks of code, directly comment on the merge request for more general feedback. Use the `Insert suggestion` feature to suggest changes. In general the assignee is responsible for reacting to and/or including the reviewers suggestions.

#### Approval

Reviewers should revisit an MR when changes have been made to resolve comment threads they opened. Additionally, the author
of the MR can request a re-review when relevant changes have been made. When all their comment threads are resolved reviewers
can approve the MR.

#### Merging

An MR can only be merged when it has been approved by at least two other team members - one from the _Recovery Cat_ team, one
from the _ctrl.alt.coop_ team. The team members working on an MR are responsible for the merge.

When you merge an MR please make sure that the feature branch is deleted and the linked issue closed afterwards.
