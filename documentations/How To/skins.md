# Skins (Coloring part of themes)

Skins consist of a set of css variables, that's all.

Take a look at `/lib/themes/theming-mechanics/colors/example-colors.css` for a
full list of css variables and a bit of explanation.

Set the variables as you like it and you're ready to go.

```

//example-colors.css may be out of date
:root {

	/**
	 * Every color (the base color) comes with a supplementary color.
	 * Color and supplementary color can be highlighted.
	 * All of the four colors up to this point have a contrast color, so
	 * that all in all we get 8 color variants for each base color.
	 *
	 * All colors are stored as RGB triples: 32,156,255
	 * This way they can be used in rgba(..., 0.5). Main purpose of this format
	 * is to stay compatible with ionic though.
	 */



	/**
	 * Hierarchical colors:
	 *
	 * - primary
	 * - secondary
	 * - tertiary
	 * - quaternary (more common than 'quartary')
	 */

	--rcc-primary-rgb-main:							0, 108, 170;
	--rcc-primary-rgb-main-highlight:				0, 81, 128;
	--rcc-primary-rgb-main-contrast:				255, 255, 255;
	--rcc-primary-rgb-main-highlight-contrast:		255, 255, 255;

	--rcc-primary-rgb-supp:							151, 182, 200;
	--rcc-primary-rgb-supp-highlight:				102, 155, 185;
	--rcc-primary-rgb-supp-contrast:				0, 0, 0;
	--rcc-primary-rgb-supp-highlight-contrast:		0, 0, 0;



	--rcc-secondary-rgb-main:						112, 188, 35;
	--rcc-secondary-rgb-main-highlight:				80, 124, 35;
	--rcc-secondary-rgb-main-contrast:				255, 255, 255;
	--rcc-secondary-rgb-main-highlight-contrast:	255, 255, 255;

	--rcc-secondary-rgb-supp:						192, 235, 148;
	--rcc-secondary-rgb-supp-highlight:				160, 219, 100;
	--rcc-secondary-rgb-supp-contrast:				0, 0, 0;
	--rcc-secondary-rgb-supp-highlight-contrast:	0, 0, 0;



	--rcc-tertiary-rgb-main:						223, 104, 32;
	--rcc-tertiary-rgb-main-highlight:				154, 61, 5;
	--rcc-tertiary-rgb-main-contrast:				255, 255, 255;
	--rcc-tertiary-rgb-main-highlight-contrast:		255, 255, 255;

	--rcc-tertiary-rgb-supp:						239, 179, 143;
	--rcc-tertiary-rgb-supp-highlight:				255, 155, 96;
	--rcc-tertiary-rgb-supp-contrast:				0, 0, 0;
	--rcc-tertiary-rgb-supp-highlight-contrast:		0, 0, 0;



	--rcc-quaternary-rgb-main:						117, 129, 30;
	--rcc-quaternary-rgb-main-highlight:			140, 159, 0;
	--rcc-quaternary-rgb-main-contrast:				255, 255, 255;
	--rcc-quaternary-rgb-main-highlight-contrast:	255, 255, 255;

	--rcc-quaternary-rgb-supp:						239, 255, 128;
	--rcc-quaternary-rgb-supp-highlight:			255, 255, 64;
	--rcc-quaternary-rgb-supp-contrast:				0, 0, 0;
	--rcc-quaternary-rgb-supp-highlight-contrast:	0, 0, 0;


	--rcc-quinary-rgb-main:							48, 56, 79;
	--rcc-quinary-rgb-main-highlight:				32, 49, 96;
	--rcc-quinary-rgb-main-contrast:				255, 255, 255;
	--rcc-quinary-rgb-main-highlight-contrast:		255, 255, 255;

	--rcc-quinary-rgb-supp:							130, 145, 189;
	--rcc-quinary-rgb-supp-highlight:				100, 131, 219;
	--rcc-quinary-rgb-supp-contrast:				0, 0, 0;
	--rcc-quinary-rgb-supp-highlight-contrast:		0, 0, 0;





	/**
	 * Categorical colors:
	 *
	 * | category | meaning |
	 * |----------|---------|
	 * | create   | Everything connected to content creation and editing |
	 * | share    | Everything connected to sending data                 |
	 * | receive  | Everything connected to receiving data               |
	 * | analyze  | Everything connected to analyzing or evaluating data |
	 * | default  | Everything that does not fall into any category      |
	 *
	 * Add/remove categorical colors as you please.
	 */


	--rcc-category-create-rgb-main:							var(--rcc-primary-rgb-main);
	--rcc-category-create-rgb-main-highlight:				var(--rcc-primary-rgb-main-highlight);
	--rcc-category-create-rgb-main-contrast:				var(--rcc-primary-rgb-main-contrast);
	--rcc-category-create-rgb-main-highlight-contrast:		var(--rcc-primary-rgb-main-highlight-contrast);

	--rcc-category-create-rgb-supp:							var(--rcc-primary-rgb-supp);
	--rcc-category-create-rgb-supp-highlight:				var(--rcc-primary-rgb-supp-highlight);
	--rcc-category-create-rgb-supp-contrast:				var(--rcc-primary-rgb-supp-contrast);
	--rcc-category-create-rgb-supp-highlight-contrast:		var(--rcc-primary-rgb-supp-highlight-contrast);



	--rcc-category-share-rgb-main:							var(--rcc-secondary-rgb-main);
	--rcc-category-share-rgb-main-highlight:				var(--rcc-secondary-rgb-main-highlight);
	--rcc-category-share-rgb-main-contrast:					var(--rcc-secondary-rgb-main-contrast);
	--rcc-category-share-rgb-main-highlight-contrast:		var(--rcc-secondary-rgb-main-highlight-contrast);

	--rcc-category-share-rgb-supp:							var(--rcc-secondary-rgb-supp);
	--rcc-category-share-rgb-supp-highlight:				var(--rcc-secondary-rgb-supp-highlight);
	--rcc-category-share-rgb-supp-contrast:					var(--rcc-secondary-rgb-supp-contrast);
	--rcc-category-share-rgb-supp-highlight-contrast:		var(--rcc-secondary-rgb-supp-highlight-contrast);



	--rcc-category-receive-rgb-main:						var(--rcc-tertiary-rgb-main);
	--rcc-category-receive-rgb-main-highlight:				var(--rcc-tertiary-rgb-main-highlight);
	--rcc-category-receive-rgb-main-contrast:				var(--rcc-tertiary-rgb-main-contrast);
	--rcc-category-receive-rgb-main-highlight-contrast:		var(--rcc-tertiary-rgb-main-highlight-contrast);

	--rcc-category-receive-rgb-supp:						var(--rcc-tertiary-rgb-supp);
	--rcc-category-receive-rgb-supp-highlight:				var(--rcc-tertiary-rgb-supp-highlight);
	--rcc-category-receive-rgb-supp-contrast:				var(--rcc-tertiary-rgb-supp-contrast);
	--rcc-category-receive-rgb-supp-highlight-contrast:		var(--rcc-tertiary-rgb-supp-highlight-contrast);



	--rcc-category-analyze-rgb-main:						var(--rcc-quaternary-rgb-main);
	--rcc-category-analyze-rgb-main-highlight:				var(--rcc-quaternary-rgb-main-highlight);
	--rcc-category-analyze-rgb-main-contrast:				var(--rcc-quaternary-rgb-main-contrast);
	--rcc-category-analyze-rgb-main-highlight-contrast:		var(--rcc-quaternary-rgb-main-highlight-contrast);

	--rcc-category-analyze-rgb-supp:						var(--rcc-quaternary-rgb-supp);
	--rcc-category-analyze-rgb-supp-highlight:				var(--rcc-quaternary-rgb-supp-highlight);
	--rcc-category-analyze-rgb-supp-contrast:				var(--rcc-quaternary-rgb-supp-contrast);
	--rcc-category-analyze-rgb-supp-highlight-contrast:		var(--rcc-quaternary-rgb-supp-highlight-contrast);

}

```

# RccColorService

See {@link RccColorService}
