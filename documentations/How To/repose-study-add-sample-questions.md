# REPOSE study information

The following diagram illustrates the entire study workflow:

![Diagram depicting REPOSE workflow](../../Screenshots/repose-workflow.jpg "REPOSE workflow")

1. An Excel sheet containing the pseudonym and the passphrase for each participant is set up. The passphrase is generated randomly and will be used to encrypt the data later. This file is only accessible to the study team.
1. A QR code is generated from each pair of pseudonym and passphrase. The QR code is printed on paper together with the link to the app and handed to the participant.
1. The participant scans the QR code using the app. Pseudonym and passphrase are saved to the app settings. The participant uses the app as instructed by the study team, i.e. answers the questionnaire.
1. After answering all questions on a given day, the answers are converted to JSON format and then added to a ZIP archive that is encrypted with the passphrase. The filename of the ZIP archive is the pseudonym.
1. The ZIP archive is securely transmitted to the Nextcloud server hosted by Charité via HTTPS.
1. The study team downloads (and deletes) the data from the Nextcloud server at their own discretion. They refer to the Excel sheet to decrypt the ZIP archive with the corresponding passphrase. All data that cannot be decrypted may be discarded as fraudulent/fake.

# Adding default questions to the app

This guide shows how to download the app,
get it running on a local machine (your own computer)
and add your own sample questions directly to the source code.
It is aimed at non-programmers and does not presume any previous experience with coding.

## System prerequisites

This section shows how to install the necessary software on your computer.

### Windows

The following steps were tested on Windows 10.
If you use a different version of Windows and notice any inconsistencies,
please create a [GitLab issue](https://gitlab.com/recoverycat/rcc-client/-/issues).

1. [Download](https://git-scm.com/) and install Git.
Git is a version control system that allows us to work on the same files
without getting in each other's way.
1. [Download](https://code.visualstudio.com/download) and install Visual Studio Code.
VS Code is a code editor. You can use any code editor or IDE, but this tutorial assumes VS Code.
1. [Download](https://github.com/coreybutler/nvm-windows/releases)
and install the Node version manager for Windows:
Scroll down the page and download `nvm-setup.exe`.
This is for the programming part of the app.
1. Open the Windows Command Prompt (*Eingabeaufforderung*) by clicking on the Windows symbol
in the task bar and typing `cmd`. Right click on the app and select "Run as administrator".
This will be referred to as the *Terminal* from now on.
    1. Verify that Git is installed correctly by typing `git version` in the Terminal.
    Hit enter and you should see output similar to `git version 2.27.0.windows.1`
    1. Do the same with `nvm version`. It should output a version number.
    1. Install the latest version of Node.js with `nvm install latest`.
    1. Follow the instructions in the output to use the latest version,
    i.e. type `nvm use 18.3.0` (replace with the displayed version number). If you get an error like `exit status 1`, you probably did not run cmd as administrator.

### Mac

The following steps were tested on macOS 12.0 Monterey.
If you use a different version of macOS and notice any inconsistencies,
please create a [GitLab issue](https://gitlab.com/recoverycat/rcc-client/-/issues).

1. Open the Terminal app.
2. Install Xcode, Apple's own development environment for macOS, by typing (hit enter in the end):
```console
xcode-select --install
```
3. This should automatically install Git. Check with this command, it should output something like `git version 2.27.0.1`.
```console
git version
```

4. Install Homebrew, a package manager that lets you install software directly from the Terminal:
```console
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```
⚠️ You will be prompted to enter your password and won't see any input as you're typing.
This is for security reasons. Just type your password and confirm with Enter.

5. Install the command line interpreter [Oh My Zsh](https://ohmyz.sh/)
```console
$ sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```
6. Install Visual Studio Code.
```console
brew install --cask visual-studio-code
```

## Downloading the source code

Follow the steps in [this guide](https://docs.recoverycat.de/additional-documentation/use-git-and-gitlab.html)
to get started with Git and GitLab.

Finally, follow the "Setup" part of the [README](https://docs.recoverycat.de/index.html)
to install the app.

⚠️ Serve the REPOSE version of the app with `npm run start:repose`

If the instructions are not clear, please file a
[GitLab issue](https://gitlab.com/recoverycat/rcc-client/-/issues).


## Working with the code

Open VS Code and open the `rcc` folder (File → Open Folder...) within it.
You should now see all subfolders and files to the left.

Before making any changes, create a new Git branch (see tutorial above).
Never make changes to the main branch directly, as this would impact all other developers!

Open this file: ``lib/repose/src/repose-questions/repose-question-store.service.ts`` (not `module`!)
This is where new questions are added.

When you're done, push your changes to the GitLab repository.

### Code structure

```configs``` is an array (list) that contains all the questions in the app.
One question is surrounded by curly braces.

```js
const configs:QuestionConfig[] = [

   {
     id:       'repose-a-daily',
     type:     'integer',
     meaning:  "Haben Sie sich heute deprimiert gefühlt?",  // todo
     translations: {
               'en': "Have you felt depressed today?",      // mit deepl übersetzt
               'de': "Haben Sie sich heute deprimiert gefühlt?"
             },
     options:    [
               // engl. antworten benötigen noch professionelle übersetzung
               { value:0  , translations: { en: 'no', /*todo*/  de: 'nein' } },
               { value:1  , translations: { en: 'slightly', /*vorläufig übersetzt*/  de: 'gering' } },
               { value:2  , translations: { en: 'moderately', /*mit deepl übersetzt*/  de: 'mäßig' } },
               { value:3  , translations: { en: 'strongly', /*mit deepl übersetzt*/  de: 'stark' } },


             ],
     tags:     ['repose-category-depression',"icd-10-f32","icd-10-f33", "daily"]
   },

   // ...
]
```

### Step-by-step instructions

```js
     id:       'repose-a-daily',
```
Give the question a unique ID. Preferably proceed alphabetically.

``'repose-a-daily'  -> 'repose-b-daily' -> 'repose-c-daily'`` etc.

If the question is shown weekly, add the tag `'weekly'` instead.
``'repose-a-weekly' -> 'repose-b-weekly'`` etc.

```js
     type:     'integer',
```
The type `'integer'` describes numbers and they are used to give the user different answering options, which can express a degree or not.

Another type would be `'boolean'`, which is used to give the user two options: `true` or `false` i. e. `yes` or `no`.

If a complex yes/no question with multiple answering options is needed, use the type `'integer'` instead.

```js
     meaning:  "Haben Sie sich heute deprimiert gefühlt?",  // todo
```
With the key 'meaning', you can add a short description of the question. This is used to give the editor a hint about what the question is about but it won't be shown in the app.

```js
     translations: {
               'en': "Have you felt depressed today?",      // mit deepl übersetzt
               'de': "Haben Sie sich heute deprimiert gefühlt?"
             },
```
Please enter the translations for the question in the language you want to use. If it's not the final translation, you can add a comment with ``//`` or in-line comment with `/* ... */`.

```js
     options:    [
                                                            // englische antworten benötigen noch professionelle übersetzung
               { value:0  , translations: { en: 'no', /*todo*/  de: 'nein' } },
               { value:1  , translations: { en: 'slightly', /*vorläufig übersetzt*/  de: 'gering' } },
               { value:2  , translations: { en: 'moderately', /*mit deepl übersetzt*/  de: 'mäßig' } },
               { value:3  , translations: { en: 'strongly', /*mit deepl übersetzt*/  de: 'stark' } },
```
Please enter the translations for the answering options in the language you want to use. If it's not the final translation, you can add a comment with ``//``.

```js
     tags:     ['repose-category-depression',"icd-10-f32","icd-10-f33", "daily"]
```
Here, please enter the tags for the question separated by commas in quotes inside the brackets. Tags are optional but useful since they can be used to filter questions.

For example:
``'repose-category-bipolar-disorder'`` or ``'repose-category-depression'``

Further tags could be:

- Entry of ICD-10:
``'icd-10-f32'``

- Time intervals:
``'daily', 'weekly'``

- Medication:
``'meds'``

Afterwards save the file with ``Ctrl+S`` (Mac: ``Cmd+S``). If the file was saved correctly, the white circle should disappear in the top right corner of the tab.

Type in your terminal:

```console
$ git add .
$ git commit -m "HIER KOMMENTAR EINFÜGEN"
$ git push origin BRANCH
```

In line 2 you can add a comment to the commit inside the quotes.
For example:
"I added a new question from line X to Y. Final translations are missing"
In line 3 instead of `BRANCH` you type the name of the branch you are working on and which you want to push to.

### Best Practice:
- Categorize questions by reasonable tags and put them together in the file directly.
- Uniform usage of single or double quotes.
- Add 3 empty lines after each newly added question
- If you encounter technical problems, feel free to reach out to  **Andreas Pittrich** (andreas.pittrich@posteo.de), **Noël Simmel** (noel.simmel@charite.de) or **Hanhoan Truong** (tqh@posteo.net).
