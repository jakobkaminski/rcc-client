# Software Bill of Materials

This table provides an overview of all external software components (software of unknown provenance, SOUPs) used in Recovery Cat. It allows developers to keep track of SOUP-related information and to evaluate the risks associated with SOUPs. Moreover, it allows auditors or authorities to quickly get an overview of the external components used.

This document is to be updated whenever a new package is installed or an installed package is updated or removed.

For further information, consult the [SOP_3.11 SOUP Surveillance](https://docs.google.com/document/d/1nvsvpKwG4h5833sY_K2CIcSbZW1qE4nuAaiDXMK6S2E/edit)

## Direct Dependencies

Direct dependencies are SOUPs that are used by Recovery Cat directly, i.e. imported directly into the source code.

| ID | App Variant | Package Name | Version | License | Programming Language | Website | Requirements | Date Last Verified | Replaceability | Risk of this package needing replacement | Will phase out over time | Verification Reasoning |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
D_01 | Both | tslib | 2.5.0 | 0BSD | TypeScript | [link](https://github.com/Microsoft/tslib.git) | Runtime library for TypeScript helper functions, essential to TypeScript projects | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Commonly used, maintained by a large organisation (Microsoft) |
D_02 | Both | tslib | 1.14.1 | 0BSD | TypeScript | [link](https://github.com/Microsoft/tslib.git) | Runtime library for TypeScript helper functions. This version is required by pdf-lib | 07.06.2023 | unreplaceable | marginal risk | replacement planned at some point | Commonly used, maintained by a large organisation (Microsoft) |
D_03 | Both | @angular/core | 15.2.9 | MIT | TypeScript | [link](https://github.com/angular/angular.git#packages/core) | Framework used to create single-page web applications, the basis of this SaMD | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Commonly used, maintained by a large organisation (Google) |
D_04 | Both | @angular/common | 15.2.9 | MIT | TypeScript | [link](https://github.com/angular/angular.git#packages/common) | Needed for Angular to function, contains commonly needed directives and services | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Commonly used, maintained by a large organisation (Google) |
D_05 | Both | @angular/forms | 15.2.9 | MIT | TypeScript | [link](https://github.com/angular/angular.git#packages/forms) | Forms for Angular | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Commonly used, maintained by a large organisation (Google) |
D_06 | Both | @angular/platform-browser | 15.2.9 | MIT | TypeScript | [link](https://github.com/angular/angular.git#packages/platform-browser) | Library for using Angular in a web browser | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Commonly used, maintained by a large organisation (Google) |
D_07 | Both | @angular/router | 15.2.9 | MIT | TypeScript | [link](https://github.com/angular/angular.git#packages/router) | Routing library for Angular | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Commonly used, maintained by a large organisation (Google) |
D_08 | Both | @angular/service-worker | 15.2.9 | MIT | TypeScript | [link](https://github.com/angular/angular.git#packages/service-worker) | Service worker library for Angular | 07.06.2023 | major feature loss or substantial coding effort | low risk | replacement not anticipated | Commonly used, maintained by a large organisation (Google) |
D_09 | Both | zone.js | 0.12.0 | MIT | JavaScript | [link](https://github.com/angular/angular.git#packages/zone.js) | Zones for JavaScript | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Part of Angular |
D_10 | Both | @ionic/core | 6.7.4 | MIT | TypeScript | [link](https://github.com/ionic-team/ionic.git) | UI toolkit for web apps | 07.06.2023 | major feature loss or substantial coding effort | marginal risk | replacement planned at some point | Commonly used, actively maintained |
D_11 | Both | @ionic/angular | 6.7.4 | MIT | TypeScript | [link](https://github.com/ionic-team/ionic.git) | Angular-specific wrappers for @ionic/core | 07.06.2023 | major feature loss or substantial coding effort | marginal risk | replacement planned at some point | Commonly used, actively maintained |
D_12 | Both | pdf-lib | 1.17.1 | MIT | TypeScript | [link](https://github.com/Hopding/pdf-lib.git) | PDF creation, needed to export user data from the app | 07.06.2023 | Equivalent package available | high risk | replacement planned at some point | Commonly used |
D_13 | Both | @pdf-lib/standard-fonts | 1.0.0 | MIT | TypeScript | [link](https://github.com/Hopding/standard-fonts.git) | Standard fonts for pdf-lib | 07.06.2023 | Equivalent package available | high risk | replacement planned at some point | Created by the same developer as pdf-lib |
D_14 | Both | @pdf-lib/upng | 1.0.1 | MIT | JavaScript | [link](https://github.com/Hopding/upng.git) | PNG decoder/encoder for pdf-lib | 07.06.2023 | Equivalent package available | high risk | replacement planned at some point | Created by the same developer as pdf-lib |
D_15 | Both | @zip.js/zip.js | 2.7.6 | BSD-3-Clause | JavaScript | [link](https://github.com/gildas-lormeau/zip.js.git) | ZIP archive functionality, needed to export user data from the app. Only library that supports encryption at the moment | 07.06.2023 | major feature loss or substantial coding effort | moderate risk | replacement not anticipated | Actively maintained, 2.9k stars on GitHub. Only package that supports zip encryption |
D_16 | Both | @ahryman40k/ts-fhir-types | 4.0.39 | MIT | TypeScript | [link](https://github.com/Ahryman40k/typescript-fhir-types.git) | TypeScript model for FHIR (standard for exchanging electronic health care data), used to represent data in a standardized format | 07.06.2023 | minor feature loss / manageble coding effort | high risk | replacement planned at some point | No alternative available at the start of development, now planned to be replaced by @types/fhir |
D_17 | Both | chart.js | 3.9.1 | MIT | JavaScript | [link](https://github.com/chartjs/Chart.js.git) | [link](hTML5 charts, needed to visualize user data in the app | 07.06.2023 | major feature loss or substantial coding effort | marginal risk | in process of being replaced | Commonly used, actively maintained, sufficient test coverage |
D_18 | Both | date-fns | 2.30.0 | MIT | TypeScript | [link](https://github.com/date-fns/date-fns.git) | Date utility library, used to correctly display times and dates in the app | 07.06.2023 | minor feature loss / manageble coding effort | marginal risk | replacement not anticipated | Commonly used, actively maintained |
D_19 | Both | qrcode | 1.5.3 | MIT | JavaScript | [link](https://github.com/soldair/node-qrcode.git) | QR code generator. User data is transmitted between patients and HCPs using QR codes | 07.06.2023 | Equivalent package available | high risk | replacement not anticipated | Commonly used |
D_20 | Both | qr-scanner | 1.4.2 | MIT | TypeScript | [link](https://github.com/nimiq/qr-scanner.git) | Very lightweight QR scanner, used until QR functionality is implemented natively in browsers. Based on a port of the popular zxing-js, which is no longer maintained | 07.06.2023 | Equivalent package available | moderate risk | replacement not anticipated | Most lightweight package available |
D_21 | Both | rxjs | 7.8.1 | Apache-2.0 | TypeScript | [link](https://github.com/reactivex/rxjs.git) | Reactive programming for JavaScript, used to handle real-time content changes | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Commonly used, actively maintained, originally developed by a large organization (Microsoft) |
D_22 | Both | webpack | 5.76.1 | MIT | JavaScript | [link](https://github.com/webpack/webpack.git) | Module bundler, currently needed to use CycloneDX for the Software Bill of Materials | 07.06.2023 | unreplaceable | marginal risk | replacement not anticipated | Commonly used, actively maintained, sufficient test coverage |

## Transitive Dependencies

Transitive dependencies are dependencies of the direct dependencies. They are needed for the app to run and function correctly, but are not imported directly.

| ID | App Variant | Package Name | Version | License | Programming Language | Website | Required by | Date Last Verified | Special consideration needed, because package is part of TS/JS canon |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| T_01 | Both | pako | 1.0.11 | MIT | JavaScript | [link](https://github.com/nodeca/pako.git) | pdf-lib | 30.06.2023 | no |
| T_02 | Both | fp-ts | 2.14.0 | MIT | TypeScript | [link](https://github.com/gcanti/fp-ts.git) | @ahryman40k/ts-fhir-types | 30.06.2023 | no |
| T_03 | Both | io-ts | 2.2.20 | MIT | TypeScript | [link](https://github.com/gcanti/io-ts.git) | @ahryman40k/ts-fhir-types | 30.06.2023 | no |
| T_04 | Both | dijkstrajs | 1.0.3 | MIT | JavaScript | [link](https://github.com/tcort/dijkstrajs.git) | qrcode | 30.06.2023 | no |
| T_05 | Both | encode-utf8 | 1.0.3 | MIT | JavaScript | [link](https://github.com/LinusU/encode-utf8.git) | qrcode | 30.06.2023 | no |
| T_06 | Both | events | 3.3.0 | MIT | JavaScript | [link](https://github.com/Gozala/events.git) | webpack | 30.06.2023 | no |