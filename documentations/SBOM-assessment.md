# Periodic assessment of SBOM
According to [SOP_3.11](https://drive.google.com/file/d/1bdNGC-GuE23IdzC3LisbYiMcAcgwp904/view)

Create new GitLab issue and branch. Keep note of all steps taken (console commands).

Example: https://gitlab.com/recoverycat/rcc-client/-/issues/866

## Vulnerability scan
1. `yarn upgrade` (fixes many vulnerabilities already)
1. `yarn audit`, save result as `yarn-audit.md`. Don't use `npm audit` since npm is not used for build.
1. Go through vulnerabilities and eliminate as many as possible:
    1. Look at top-level packages and upgrade if possible.
    1. Look at top-level packages and remove if possible, using `yarn remove`.
1. Handle leftover vulnerabilities:
    1. Keep justifiable vulnerabilities and note the reasoning in the assessment summary:
        1. Severity "low", according to GitHub Advisories.
        1. Severity "moderate" or higher, but not applicable to our use of the package.
    1. Escalate unjustifiable vulnerabilities with process owner.
1. Run e2e tests.
1. Add final `yarn audit` output to `yarn-audit.md`.
1. Commit all changes. Commit message includes the list of console commands.

## SBOM assessment
Regulatory file: RC1_SW_04

1. Check direct dependencies against `package.json` of root and all workspaces.
1. Check version numbers against `yarn.lock`.
1. Remove unused packages.
1. Convert Google Sheets file to Markdown and save as `SBOM.md` in this directory.

Put list of console commands and a summary of what was done in the merge request description.
