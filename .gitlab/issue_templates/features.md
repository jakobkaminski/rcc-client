# ​🆕​ Feature

Related software requirement: RK_SWR_01

Related Epic ClickUp ID: #1234567

Affected app variants: PAT, HCP, Study

## Summary
*Describe the gist of the feature in prose.*

## Background
*Describe the technical aspects of the feature.*

## Acceptance Criteria
*List the acceptance criteria that must be met before this issue may be closed.*

- [ ] Criterion 1
- [ ] Criterion 2

## Ideas
*Discuss possible implementations.*

## Tasks
*Define specific tasks in the order they need to be done in. Reference files or specific lines of code that need to be changed.*

- [ ] Task 1
- [ ] Task 2
- [ ] Task 3

## Design
*Present or link to any existing designs, screens or wireframes.*
