# End-to-end testing

This document is supposed to provide some guidelines on how to write E2E-tests. Please feel free to comment, discuss, change, ammend, complain :)

Over time this document is supposed to become a complete guide of how we want to handle the E2E-tests.

## Infrastrucure

All E2E test related files lie in `./cypress`, except `cypress.config`, which lies in the project root.
* folder structure of cypress folder
	* downloads - holds files cypress downloads during tests, like he backups
	* e2e - holds the actual tests
		* each version has its own folder with a regulatory sub folder
		* if we want additional non-regulatory tests, we can add a folder for them
	* fixtures - holds files that are used in tests
	* reports - holds the pdf reports of the tests, if the report is enabled
	* screenshots - holds screenshots of failed tests
	* support - holds additional configuration files
	* videos - holds videos of failed tests

Where should this document go?

## What is the goal?

The E2E should ...

* check if the application flow works as we intened (check if a to be tested aspect should rather be part of a unit test only, or should have an additional unit test that runs earlier)
	* for regulatory purposes
		* https://miro.com/app/board/uXjVMxExJ0M=/?share_link_id=165022668580
		* each box with an identifier represents a node in the application flow and needs to be tested by an e2e test
	* in general
		* functional tests with a user interaction which is caused by a users intent to do something specific in the app
		* not focused on the system but the users behaviour when using the main functionalities of the app
* check the fullfillment of accessibility standards
* (check if everything is doable by keyboard)
* be agnostic of the layout
* be a corrective to the app (if we have difficulties writing a test, we should consider improving the app in the respective regard, rather than tailor the test)
* be application-flow based, multi-device compatible (at least a mobile view, or different resolutions),
* run against deployed versions of the app (for example QA or Production).
* be multi-browser compatible (at least the more relevant browsers)
* be properly integrated into the deployment pipeline.

What do you want the E2E to do?

## E2E vs unit test (Needs discussion, example cases?)

Indicators for a unit test:
* the focus of the test is internal code structures that can be tested in isolation
	* classes, functions, constructors, validations, ...
* when dependencies like modules or components can be mocked
* examples:
	* testing the functionality of our translation pipe

Indicators for an E2E test:
* tests that need the browser and user interaction
* focus is on interaction of the apps interface and not the internal code
* focuses on one use of a certain functionality and not every variation that the underlying code is capable of
* examples:
	* testing that the language dropdown changes the labels on screen

## When should the tests run with which consequences?

* At will by developer locally
	* if tests fail:
		* investigate if tests or code is broken
		* fix either before merging

* Before a merge to main (branch should be in sync with main)
	* run after every push
	* if tests fail:
		* cannot merge
		* branch needs to be fixed

* After deployment to QA
	* need to run against deployed version
	* if tests fail:
		* cannot deploy to production
		* investigate if error is due to environment
			* document source ef error
			* check if prod could have the same problem


## Challenges

* multi language
* multi variants
* accessibility
* same test for different input methods (keyboard vs mouse)
* variant interaction
* themes
* length of test runs
* QA test environment and PROD environment should be as similar as possible (more general concern)


## Suggested principles (sometimes no matter how hard you try you have to cheat on E2E tests to get them to run :D):

* Tests should be human readable and meaningful: Wrap portions of the tests into functions with meaningful names.
* ~~Do not repeat yourself: Wrap portions of tests into reusable functions.~~ [^2]
* Build hierarchie of testing utils: findElementWithAriaLabel -> clickButtonWithAriaLabel -> clickMainMenuButton (This way we can later improve the ClickButtonWithAriaLabel function to e.g. check if it has a [role="button"] attribute in case the button is not actually a button element.)
* When locating elements, do not use knowlege of the code, instead only use what the user can see or what can be interpreted by screen readers. (If we cannot write a test this way, we should restructure the code we are testing)
* When elements are hard to detect, do not add/refer to ids or classes, instead find a way to add useful data to the elements a screen reader could read or are useful to the user in some other way: (add [aria-label] where appropriate, add [title] attribute where appropriatre (shows a tooltip -> useful), add [placeholder] on inputs )
* Failure states need to be tested the same as successful states

## Running the tests with and without reporting

The e2e tests can be run in several ways. The two main ways are either with the GUI or in headless mode. For both there are scripts in the package.json.
You can also run the tests in headless mode and generate a PDF report of the test run.


* GUI - ```e2e:gui``` or ```cypress open```
  * you have to make sure to set the ```base url``` of the variant you want to test in the ```cypress.config.ts```
* Headless - ```e2e:hcp:run``` or ```e2e:pat:run```
  * here the base url is already specified
* headless with report generation - ```e2e:hcp:report``` or ```e2e:pat:report```
  * This will use the ```cypress/e2e-report.sh``` script to generate the report
  * The finished reports are saved in the ```cypress/reports``` folder

[^2] needs discussion

